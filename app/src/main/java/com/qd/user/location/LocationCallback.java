package com.qd.user.location;

import android.location.Location;

interface LocationCallback {

    void setLocation(Location currentLocation);

    void disconnect();
}
