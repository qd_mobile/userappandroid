package com.qd.user.location;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

class Location extends com.google.android.gms.location.LocationCallback implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private final int REQUEST_CHECK_SETTINGS = 0x002;
    private final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;//10 seconds
    private final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;//10 seconds
    private Activity mActivity;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private com.qd.user.location.LocationCallback mLocationCallback;


    private void startLocation() {
        if (mGoogleApiClient == null || mLocationRequest == null) {
            createLocationRequest();
            buildGoogleApiClient();
            buildLocationSettingsRequest();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    /**
     * Sets up the rcLocation request. Android has two rcLocation request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current rcLocation. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused RCLocation Provider API returns rcLocation updates that are
     * accurate to within a few feet.
     * <p>
     * These settings are appropriate for mapping applications that show real-time rcLocation
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                // Sets the desired interval for active rcLocation updates. This interval is
                // inexact. You may not receive updates at all if no rcLocation sources are available, or
                // you may receive them slower than requested. You may also receive updates faster than
                // requested if other applications are requesting rcLocation at a faster interval.
                .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
                // Sets the fastest rate for active rcLocation updates. This interval is exact, and your
                // application will never receive updates faster than this value.
                .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
                .setSmallestDisplacement(0);//set smallest displacement to change lat long
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed rcLocation settings.
     */
    private void buildLocationSettingsRequest() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);//this make sure dialog is always visible

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(locationSettingsResult -> {
            final Status status = locationSettingsResult.getStatus();

            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    startLocationUpdates();
                    break;

                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // RCLocation settings are not satisfied. But could be fixed by showing the user a dialog.
                    try {
                        status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);//this prompts a user with a dialog, we get its call in onActivityResult.
                    } catch (IntentSender.SendIntentException ignored) {
                        //do nothing
                    }
                    break;

                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    // RCLocation settings are not satisfied. However, we have no way to fix the
                    // settings so we won't show the dialog.
                    break;
            }
        });
    }


    /**
     * Requests rcLocation updates from the FusedLocationApi.
     */
    @SuppressWarnings({"MissingPermission"})//because permission case has been handled perfectly
    private void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.getFusedLocationProviderClient(mActivity).requestLocationUpdates(mLocationRequest, this, null);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //reconnecting google api client
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    /**
     * This method is called when google api client connection fails
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(mActivity, 1);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * this method is called whenever there is location update
     */
    @Override
    public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        if (locationResult.getLastLocation()!=null && mLocationCallback!=null)
            mLocationCallback.setLocation(locationResult.getLastLocation());
    }

    /*
     * Method to disconnect mGoogleApiClient
     * */
    public void disconnect() {
        if (mGoogleApiClient != null && mLocationCallback!=null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mLocationCallback.disconnect();
        }
    }


    /**
     * method to connect google api client
     */
    public void connect() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
        else startLocation();

    }


    /**
     * method to initialize context
     */
    public void setActivity(FragmentActivity activity) {
        this.mActivity = activity;
    }

    /**
     * method to initialize location callback listener
     */
    public void setCallback(com.qd.user.location.LocationCallback locationCallback) {
        this.mLocationCallback = locationCallback;
    }

    @Override
    public void onLocationChanged(android.location.Location location) {

    }
}
