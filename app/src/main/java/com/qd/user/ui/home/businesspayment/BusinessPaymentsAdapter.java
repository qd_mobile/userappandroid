package com.qd.user.ui.home.businesspayment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.payments.invoice.Datum;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.ProgressLoader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

class BusinessPaymentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;
    private ArrayList<Object> mBusinessPaymentsList;
    private BusinessPaymentsAdapterInterface mInterface;


    BusinessPaymentsAdapter(ArrayList<Object> ordersList, BusinessPaymentsAdapterInterface iBusinessPayments) {
        mInterface = iBusinessPayments;
        mBusinessPaymentsList = ordersList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_vendor_payment_history, viewGroup, false);
            return new BusinessPaymentsAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mBusinessPaymentsList.get(position) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mBusinessPaymentsList.get(position) instanceof Datum) {
            ((BusinessPaymentsAdapterViewHolder) holder).tvOrderId.setText(((Datum) mBusinessPaymentsList.get(position)).getInvoiceNumber());
            ((BusinessPaymentsAdapterViewHolder) holder).tvBillAmount.setText(String.format(Locale.getDefault(), "%.3f KWD", ((Datum) mBusinessPaymentsList.get(position)).getTotalAmount()));
            ((BusinessPaymentsAdapterViewHolder) holder).tvDay.setText(DateFormatter.getInstance().getFormattedDay(((Datum) mBusinessPaymentsList.get(position)).getCreatedAt()));
            ((BusinessPaymentsAdapterViewHolder) holder).tvDate.setText(DateFormatter.getInstance().getFormattedDate(((Datum) mBusinessPaymentsList.get(position)).getCreatedAt()));
            ((BusinessPaymentsAdapterViewHolder) holder).tvTime.setText(DateFormatter.getInstance().getFormattedTime(((Datum) mBusinessPaymentsList.get(position)).getCreatedAt()));
            ((BusinessPaymentsAdapterViewHolder) holder).tvBillDuration.setText(String.format("%s - %s", DateFormatter.getInstance().getFormattedDate(((Datum) mBusinessPaymentsList.get(position)).getInvoiceStartDate()),
                    DateFormatter.getInstance().getFormattedDate(((Datum) mBusinessPaymentsList.get(position)).getInvoiceEndDate())));


        }
    }

    @Override
    public int getItemCount() {
        return mBusinessPaymentsList.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (mBusinessPaymentsList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }


    //hide progress loader after getting paginated data
    void hideProgress() {
        mBusinessPaymentsList.remove(mBusinessPaymentsList.size() - 1);
        notifyDataSetChanged();
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mBusinessPaymentsList.add(new ProgressLoader());
        notifyItemInserted(mBusinessPaymentsList.size());
    }

    public interface BusinessPaymentsAdapterInterface {
        void onRowClicked(String id);

        void onPayNowClicked(String id, BigDecimal totalAmount);
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;


        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    class BusinessPaymentsAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_label_order_id)
        TextView tvLabelOrderId;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.view_date_and_time)
        View viewDateAndTime;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.view_my_order)
        View viewMyOrder;
        @BindView(R.id.tv_bill_amount)
        TextView tvBillAmount;
        @BindView(R.id.tv_label_bill_amount)
        TextView tvLabelBillAmount;
        @BindView(R.id.view_bill)
        View viewBill;
        @BindView(R.id.tv_bill_duration)
        TextView tvBillDuration;
        @BindView(R.id.tv_label_bill_duration)
        TextView tvLabelBillDuration;
        @BindView(R.id.view_bill_amount)
        View viewBillAmount;
        @BindView(R.id.btn_pay_now)
        TextView btnPayNow;
        @BindView(R.id.guidelineStart)
        Guideline guidelineStart;
        @BindView(R.id.guidelineEnd)
        Guideline guidelineEnd;
        BusinessPaymentsAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> mInterface.onRowClicked(((Datum) mBusinessPaymentsList.get(getAdapterPosition())).getId()));

            btnPayNow.setOnClickListener(v -> mInterface.onPayNowClicked(((Datum) mBusinessPaymentsList.get(getAdapterPosition())).getId(),
                    ((Datum) mBusinessPaymentsList.get(getAdapterPosition())).getTotalAmount()));
        }
    }
}
