package com.qd.user.ui.createorder.selectvehicle;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.createorder.vehicle.VehicleCategoryResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.ui.createorder.CreateOrderFragment;
import com.qd.user.ui.createorder.CreateOrderRepository;

public class SelectVehicleViewModel extends ViewModel {
    private RichMediatorLiveData<VehicleCategoryResponse> mVehicleCategoryLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;


    private CreateOrderRepository mRepo = new CreateOrderRepository();
    private RichMediatorLiveData<OptimizedDeliveryResponse> mOptimizedDeliveryLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mVehicleCategoryLiveData == null) {
            mVehicleCategoryLiveData = new RichMediatorLiveData<VehicleCategoryResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if (mOptimizedDeliveryLiveData == null) {
                mOptimizedDeliveryLiveData = new RichMediatorLiveData<OptimizedDeliveryResponse>() {
                    @Override
                    protected Observer<FailureResponse> getFailureObserver() {
                        return failureResponseObserver;
                    }

                    @Override
                    protected Observer<Throwable> getErrorObserver() {
                        return errorObserver;
                    }

                    @Override
                    protected Observer<Boolean> getLoadingStateObserver() {
                        return mLoadingStateObserver;
                    }
                };
            }

        }
    }


    /**
     * This method gives the CreateOrder live data object to {@link CreateOrderFragment}
     *
     * @return {@link #mVehicleCategoryLiveData}
     */
    RichMediatorLiveData<VehicleCategoryResponse> getVehicleCategoryLiveData() {
        return mVehicleCategoryLiveData;
    }

    /**
     * This method gives the delivery Charges live data object to {@link SelectVehicleFragment}
     *
     * @return {@link #mOptimizedDeliveryLiveData}
     */
    RichMediatorLiveData<OptimizedDeliveryResponse> getOptimizedDeliveryLiveData() {
        return mOptimizedDeliveryLiveData;
    }


    void getVehicleCategories() {
        //  mRepo.getVehicleCategories(mVehicleCategoryLiveData, orderType);
    }


    void getChangedDeliveryVehicle(OptimizedDeliveryRequest dataForOptimizedRoute) {
        mRepo.getDeliveryCharges(mOptimizedDeliveryLiveData, dataForOptimizedRoute);
    }
}
