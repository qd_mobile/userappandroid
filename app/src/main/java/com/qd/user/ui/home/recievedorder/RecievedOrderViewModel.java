package com.qd.user.ui.home.recievedorder;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.model.failureresponse.FailureResponse;

public class RecievedOrderViewModel extends ViewModel {
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private ReceivedOrderRepository mRepo = new ReceivedOrderRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;

    }

    public String getRegistrationDate() {
        return mRepo.getRegistrationDate();
    }

    public void getRecievedOrdersList(int mNextPage, String mFromDate, String mToDate) {
    }
}
