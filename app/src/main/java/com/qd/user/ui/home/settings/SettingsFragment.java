package com.qd.user.ui.home.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.utils.LocaleManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingsFragment extends BaseFragment {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_language)
    TextView tvLanguage;
    @BindView(R.id.tv_English)
    TextView tvEnglish;
    @BindView(R.id.tv_arabic)
    TextView tvArabic;
    @BindView(R.id.view_language)
    View viewLanguage;
    @BindView(R.id.tv_about_us)
    TextView tvAboutUs;
    @BindView(R.id.view_about_us)
    View viewAboutUs;
    @BindView(R.id.tv_privacy_policy)
    TextView tvPrivacyPolicy;
    @BindView(R.id.view_privacy_policy)
    View viewPrivacyPolicy;
    @BindView(R.id.tv_terms_conditions)
    TextView tvTermsConditions;
    private Unbinder unbinder;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.switch_notifications)
    Switch switchNotifications;

    /**
     * A {@link ISettingsHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private ISettingsHost mSettingsHost;

    /**
     * A {@link SettingsViewModel} object to handle all the actions and business logic of orders listing.
     */
    private SettingsViewModel mSettingsViewModel;
    private boolean isLanguageOpen;

    public static SettingsFragment getInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ISettingsHost) {
            mSettingsHost = (ISettingsHost) context;
        } else
            throw new IllegalStateException("host must implement ISettingsHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        mSettingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        mSettingsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        super.onViewCreated(view, savedInstanceState);

        observeLiveData();
    }

    private void observeLiveData() {
        mSettingsViewModel.getNotificationStatusLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            if (commonResponse != null)
                switchNotifications.setChecked(commonResponse.getResult().isEnableNotification());
        });
    }

    @Override
    protected void initViewsAndVariables() {
        //mSettingsHost.setDrawerLocked(false);
        tvTitle.setText(R.string.s_settings);
        switchNotifications.setChecked(mSettingsViewModel.getNotificationStatus());
        getSelectedLanguage();
        setListeners();
    }

    private void setListeners() {
        switchNotifications.setOnCheckedChangeListener((buttonView, isChecked) -> mSettingsViewModel.enableOrDisableNotifications(isChecked));
    }

    private void getSelectedLanguage() {
        if (LocaleManager.getLanguage(getContext()).equals("en")) {
            changeLanguages(getString(R.string.s_english));
        } else changeLanguages(getString(R.string.s_arabic));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        if (switchNotifications.isChecked())
            switchNotifications.setChecked(false);
        else switchNotifications.setChecked(true);
    }

    @OnClick({R.id.iv_back, R.id.tv_language, R.id.tv_English, R.id.tv_arabic, R.id.tv_about_us, R.id.tv_privacy_policy, R.id.tv_terms_conditions})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_language:
                openLanguages();
                break;
            case R.id.tv_English:
                changeLanguages(getString(R.string.s_english));
                mSettingsHost.setLocale("en");
                break;
            case R.id.tv_arabic:
                changeLanguages(getString(R.string.s_arabic));
                mSettingsHost.setLocale("ar");
                break;
            case R.id.tv_about_us:
                mSettingsHost.openStaticPage(AppConstants.StaticPages.ABOUT_US);
                break;
            case R.id.tv_privacy_policy:
                mSettingsHost.openStaticPage(AppConstants.StaticPages.PRIVACY_POLICY);
                break;
            case R.id.tv_terms_conditions:
                mSettingsHost.openStaticPage(AppConstants.StaticPages.TERMS_AND_CONDITIONS);
                break;
            case R.id.iv_back:
                mSettingsHost.onBackPressed();
                break;
        }
    }

    private void changeLanguages(String language) {
        if (language.equals(getString(R.string.s_english))) {
            tvEnglish.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_settings_check_box_active, 0);
            tvArabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_settings_check_box_inactive, 0);
        } else {
            tvArabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_settings_check_box_active, 0);
            tvEnglish.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_settings_check_box_inactive, 0);
        }
    }

    private void openLanguages() {
        if (!isLanguageOpen) {
            tvLanguage.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_settings_down_arrow, 0);
            tvEnglish.setVisibility(View.VISIBLE);
            tvArabic.setVisibility(View.VISIBLE);
            isLanguageOpen = true;
        } else {
            tvLanguage.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_settings_down_forward_arrow, 0);
            tvEnglish.setVisibility(View.GONE);
            tvArabic.setVisibility(View.GONE);
            isLanguageOpen = false;
        }
    }


    public interface ISettingsHost {

        void onBackPressed();

        void openStaticPage(String title);

        void setLocale(String en);
    }

}
