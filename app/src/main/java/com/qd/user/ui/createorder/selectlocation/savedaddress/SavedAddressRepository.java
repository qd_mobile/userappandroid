package com.qd.user.ui.createorder.selectlocation.savedaddress;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.failureresponse.FailureResponse;

class SavedAddressRepository {

    void getSavedAddresses(final RichMediatorLiveData<SavedAddressResponse> savedAddressLiveData, String searchString) {
        DataManager.getInstance().getSavedAddresses(DataManager.getInstance().getUserId(), searchString).enqueue(new NetworkCallback<SavedAddressResponse>() {
            @Override
            public void onSuccess(SavedAddressResponse savedAddressResponse) {
                savedAddressLiveData.setValue(savedAddressResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                savedAddressLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                savedAddressLiveData.setError(t);
            }
        });
    }
}
