package com.qd.user.ui.approveReturn;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.Result;
import com.qd.user.model.myorders.report.ReportNotificationResponse;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.DateFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ApproveReturnFragment extends BaseFragment implements AppUtils.ISuccess {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_label_order_id)
    TextView tvLabelOrderId;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.view_date_and_time)
    View viewDateAndTime;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.view_my_order)
    View viewMyOrder;
    @BindView(R.id.iv_order_type)
    ImageView ivOrderType;
    @BindView(R.id.tv_order_type)
    TextView tvOrderType;
    @BindView(R.id.iv_pickup_location)
    ImageView ivPickupLocation;
    @BindView(R.id.tv_label_pickup_location)
    TextView tvLabelPickupLocation;
    @BindView(R.id.tv_pickup_location)
    TextView tvPickupLocation;
    @BindView(R.id.view_dotted_line)
    View viewDottedLine;
    @BindView(R.id.iv_drop_off_location)
    ImageView ivDropOffLocation;
    @BindView(R.id.tv_label_drop_off_location)
    TextView tvLabelDropOffLocation;
    @BindView(R.id.tv_drop_off_location)
    TextView tvDropOffLocation;
    @BindView(R.id.rl_location_info)
    RelativeLayout rlLocationInfo;
    @BindView(R.id.view_payment_mode_divider)
    View viewPaymentModeDivider;
    @BindView(R.id.tv_payment_mode)
    TextView tvPaymentMode;
    @BindView(R.id.tv_label_payment_mode)
    TextView tvLabelPaymentMode;
    @BindView(R.id.tv_label_item_price)
    TextView tvLabelItemPrice;
    @BindView(R.id.tv_label_delivery_charges)
    TextView tvLabelDeliveryCharges;
    @BindView(R.id.view_item_price_divider)
    View viewItemPriceDivider;
    @BindView(R.id.tv_item_price)
    TextView tvItemPrice;
    @BindView(R.id.view_delivery_charges_divider)
    View viewDeliveryChargesDivider;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.view_reason_for_report)
    View viewReasonForReport;
    @BindView(R.id.iv_report_reason)
    ImageView ivReportReason;
    @BindView(R.id.tv_report_reason)
    TextView tvReportReason;
    @BindView(R.id.btn_return_order)
    Button btnReturnOrder;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;

    private IApproveReturnHost mHost;
    private ApproveReturnViewModel mViewModel;
    private Unbinder unbinder;
    private String mElementId;
    private boolean isForPickup;
    private ReportNotificationResponse mNotificationResponse;

    public static ApproveReturnFragment getInstance(Parcelable notificationData) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.NOTIFICATION_DATA, notificationData);
        ApproveReturnFragment approveReturnFragment = new ApproveReturnFragment();
        approveReturnFragment.setArguments(bundle);
        return approveReturnFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_approve_return, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(this).get(ApproveReturnViewModel.class);
        mViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

    }

    private void observeLiveData() {
        mViewModel.getApproveReturnLiveData().observe(getViewLifecycleOwner(), orderDetailResponse -> {
            if (orderDetailResponse != null) {
                mHost.onBackPressed();
            }
        });

        mViewModel.getOrderCancellationLiveData().observe(getViewLifecycleOwner(), orderCancellationResponse -> {
            if (orderCancellationResponse != null) {
                mHost.onBackPressed();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IApproveReturnHost) {
            mHost = (IApproveReturnHost) context;
        } else
            throw new IllegalStateException("Host must implement IApproveReturnHost");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_delivery_failed);
        getArgumentsData();
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getParcelable(AppConstants.NOTIFICATION_DATA) != null) {
            setViews(Objects.requireNonNull(getArguments().getParcelable(AppConstants.NOTIFICATION_DATA)));
        }
    }

    private void setViews(ReportNotificationResponse notificationResponse) {
        mNotificationResponse = notificationResponse;
        tvPickupLocation.setText(notificationResponse.getPickupLocation());
        tvDropOffLocation.setText(notificationResponse.getDropLocation());
        tvDay.setText(DateFormatter.getInstance().getFormattedDay(notificationResponse.getScheduledStartTime()));
        tvDate.setText(DateFormatter.getInstance().getFormattedDate(notificationResponse.getScheduledStartTime()));
        tvTime.setText(DateFormatter.getInstance().getFormattedTime(notificationResponse.getScheduledStartTime()));
        tvOrderId.setText(notificationResponse.getOrderUniqueId());
        tvItemPrice.setText(String.format(Locale.getDefault(), "%.3f KWD", notificationResponse.getItemPrice()));
        tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", notificationResponse.getDeliveryCharge()));
        tvReportReason.setText(notificationResponse.getReason());

        switch ((int) notificationResponse.getFromIssueReport()) {
            case 2:
                isForPickup = true;
                btnReturnOrder.setText(R.string.s_cancel_order);
                btnReturnOrder.setBackgroundResource(R.drawable.drawable_round_cornered_red_stroke);
                btnReturnOrder.setTextColor(getResources().getColor(R.color.colorDarkRed));
                break;
            case 1:
                btnReturnOrder.setText(R.string.s_return_order);
                btnReturnOrder.setBackgroundResource(R.color.colorGreenishTeal);
                btnReturnOrder.setTextColor(getResources().getColor(R.color.colorWhite));
        }

        mElementId = notificationResponse.getElementId();

    }

    @OnClick(R.id.btn_return_order)
    public void onViewClicked() {
        if (isForPickup) {
            openOrderCancellationDialog();
        } else
            AppUtils.getInstance().openSuccessDialog(getContext(),
                    String.format(Locale.getDefault(), "You will be charged %.3f KWD",
                            mNotificationResponse.getReturnCharges() != 0 ? mNotificationResponse.getReturnCharges() : mNotificationResponse.getCancellationCharges()),
                    this);

    }

    /**
     * Opens dialog for listing the reason to cancel the particular order.
     * Only one of the given reasons can be selected at a time and an additional edit text is provided for description.
     */
    private void openOrderCancellationDialog() {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.layout_cancel_order);
            AppUtils.getInstance().dimDialogBackground(dialog);

            final RadioGroup rgReasons = dialog.findViewById(R.id.rg_reasons);
            final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
            final Button btnSubmit = dialog.findViewById(R.id.btn_submit);
            final EditText etDescription = dialog.findViewById(R.id.et_message);
            final TextView tvError = dialog.findViewById(R.id.tv_error);

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            List<Result> reasons = DataManager.getInstance().getCancelReasons();

            for (int i = 0; i < reasons.size(); i++) {
                RadioButton button = new RadioButton(getContext());
                button.setId(i);
                button.setText(reasons.get(i).getReason());
                rgReasons.addView(button);
            }

            btnSubmit.setOnClickListener(v -> {
                if (rgReasons.getCheckedRadioButtonId() != -1) {
                    if (reasons.get(rgReasons.getCheckedRadioButtonId()).getDescriptionMandatory() && etDescription.getText().toString().isEmpty()) {
                        tvError.setText(R.string.s_please_describe_reason_for_cancellation);
                        tvError.setVisibility(View.VISIBLE);
                        shakeLayout(tvError);
                    } else {
                        String reportIssue = null;
                        tvError.setVisibility(View.GONE);
                        reportIssue = reasons.get(rgReasons.getCheckedRadioButtonId()).getReason();

                        mViewModel.cancelOrder(requestPayloadForOrderCancellation(reportIssue, etDescription.getText().toString(), false));
                        dialog.dismiss();
                    }
                } else {
                    tvError.setText(R.string.s_please_select_a_reason_for_cancellation);
                    tvError.setVisibility(View.VISIBLE);
                    shakeLayout(tvError);
                }
            });

            dialog.show();
        }
    }

    private JSONObject requestPayloadForOrderCancellation(String reportIssue, String description, boolean isApproved) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("requestFrom", mNotificationResponse.getOrderType().equals(getString(R.string.business)) ?
                    getString(R.string.business) : getString(R.string.cash_client));
            jsonObject.put("orderId", mNotificationResponse.getId());

            jsonObject.put("reason", reportIssue);
            if (description != null)
                jsonObject.put("description", description);

            jsonObject.put("driverId", mNotificationResponse.getDriverId());

            if (isApproved) {
                jsonObject.put("isApproved", true);
                jsonObject.put("isElement", true);
                jsonObject.put("elementId", mElementId);
            } else {
                jsonObject.put("isElement", false);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public void onOkViewClicked() {
        //mViewModel.cancelOrder(requestPayloadForOrderCancellation(mNotificationResponse.getReason(), "", true));
        mViewModel.approveOrderReturn(mElementId);
    }


    public interface IApproveReturnHost {

        void onBackPressed();
    }
}
