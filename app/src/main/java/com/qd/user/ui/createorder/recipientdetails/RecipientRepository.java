package com.qd.user.ui.createorder.recipientdetails;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import org.json.JSONException;
import org.json.JSONObject;

class RecipientRepository {

    String getUserName() {
        return DataManager.getInstance().getUserName();
    }

    public String getEmail() {
        return DataManager.getInstance().getEmail();
    }

    public String getMobileNumber() {
        return DataManager.getInstance().getCountryCode() + DataManager.getInstance().getPhoneNumber();
    }

    public String getContact() {
        return DataManager.getInstance().getPhoneNumber();
    }

    public void getDeliveryCharges(final RichMediatorLiveData<OptimizedDeliveryResponse> optimizedDeliveryLiveData, OptimizedDeliveryRequest dataForOptimizedRoute) {
        dataForOptimizedRoute.setVendorId(DataManager.getInstance().getUserId());
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().getDeliveryCharges(new JSONObject(gson.toJson(dataForOptimizedRoute))).enqueue(new NetworkCallback<OptimizedDeliveryResponse>() {
                @Override
                public void onSuccess(OptimizedDeliveryResponse optimizedDeliveryResponse) {
                    optimizedDeliveryLiveData.setValue(optimizedDeliveryResponse);
                    optimizedDeliveryLiveData.setLoadingState(false);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    optimizedDeliveryLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    optimizedDeliveryLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
