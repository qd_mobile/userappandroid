package com.qd.user.ui.home.contactus;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.contactus.ContactUsResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.HashMap;

class ContactUsRepository {

    void hitContactUsApi(final RichMediatorLiveData<ContactUsResponse> contactUsLiveData, String name, Object selectedItem, String message, String email, String phone) {
        DataManager.getInstance().hitContactUsApi(createRequestPayloadForContactUs(name, selectedItem, message, email, phone)).enqueue(new NetworkCallback<ContactUsResponse>() {
            @Override
            public void onSuccess(ContactUsResponse contactUsResponse) {
                contactUsLiveData.setLoadingState(false);
                contactUsLiveData.setValue(contactUsResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                contactUsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                contactUsLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> createRequestPayloadForContactUs(String name, Object selectedItem, String message, String email, String phone) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put("name", name);
        mRequestMap.put("expectedOrder", "");
        mRequestMap.put("enquiryType", selectedItem.toString());
        mRequestMap.put("queryType", "CONTACT_US");
        mRequestMap.put("message", message);

        if (DataManager.getInstance().getUserType() != null) {
            mRequestMap.put("isRegistered", true);
            mRequestMap.put("mobileNo", "");
            mRequestMap.put("email", "");
            mRequestMap.put("registeredUserId", DataManager.getInstance().getUserId());
        } else {
            mRequestMap.put("mobileNo", phone);
            mRequestMap.put("email", email);
            mRequestMap.put("isRegistered", false);
        }


        return mRequestMap;
    }

    String getUsername() {
        return DataManager.getInstance().getUserName();
    }
}
