package com.qd.user.ui.createorder.selectlocation;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.model.createorder.savedaddress.SavedLocation;
import com.qd.user.ui.createorder.recipientdetails.RecipientDetailsFragment;
import com.qd.user.ui.createorder.selectlocation.advancedsearch.AdvancedSearchFragment;
import com.qd.user.ui.createorder.selectlocation.advancedsearch.AdvancedSearchViewModel;
import com.qd.user.ui.createorder.selectlocation.map.MapFragment;
import com.qd.user.ui.createorder.selectlocation.savedaddress.SavedAddressFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SelectLocationFragment extends BaseFragment implements ViewPager.OnPageChangeListener {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tl_location)
    TabLayout tlLocation;
    @BindView(R.id.vp_select_location)
    ViewPager vpSelectLocation;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_selected_location)
    TextView tvSelectedLocation;
    @BindView(R.id.ll_selected_location)
    LinearLayout llSelectedLocation;

    private Unbinder unbinder;
    private ISelectLocationHost mHost;
    private AdvancedSearchViewModel mSelectLocationViewModel;
    private SelectLocationViewPagerAdapter mViewPagerAdapter;
    private boolean mIsForPickup;
    private Result mSavedAddress;
    private ArrayList<Fragment> mFragmentList;
    private DropOffArr mSelectedDropLocation;
    private PickupLocation mSelectedPickupLocation;
    private boolean mIsFromRecipientDetails;

    public SelectLocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_location, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static SelectLocationFragment getInstance(boolean isForPickup, Parcelable pickUp, Parcelable dropOffArr, boolean isFromRecipientDetails) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(NetworkConstants.KEY_IS_FOR_PICKUP, isForPickup);
        if (isForPickup) {
            bundle.putParcelable(AppConstants.SELECTED_ADDRESS, pickUp);
        } else
            bundle.putParcelable(AppConstants.SELECTED_ADDRESS, dropOffArr);
        bundle.putBoolean(AppConstants.IS_FROM_RECIPIENT_DETAILS, isFromRecipientDetails);
        SelectLocationFragment selectLocationFragment = new SelectLocationFragment();
        selectLocationFragment.setArguments(bundle);
        return selectLocationFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ISelectLocationHost) {
            mHost = (ISelectLocationHost) context;
        } else throw new IllegalStateException("Host must implement ISelectLocationHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mSelectLocationViewModel = new ViewModelProvider(this).get(AdvancedSearchViewModel.class);
        mSelectLocationViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        super.onViewCreated(view, savedInstanceState);
        observeLiveData();


    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            mIsFromRecipientDetails = getArguments().getBoolean(AppConstants.IS_FROM_RECIPIENT_DETAILS);
            mIsForPickup = getArguments().getBoolean(NetworkConstants.KEY_IS_FOR_PICKUP);
            if (getArguments().getParcelable(AppConstants.SELECTED_ADDRESS) != null) {
                if (mIsForPickup)
                    mSelectedPickupLocation = getArguments().getParcelable(AppConstants.SELECTED_ADDRESS);
                else
                    mSelectedDropLocation = getArguments().getParcelable(AppConstants.SELECTED_ADDRESS);
            }
        }
    }

    private void observeLiveData() {
        //observe validations errors

    }

    @Override
    protected void initViewsAndVariables() {
        getArgumentsData();
        if (mIsForPickup) {
            tvTitle.setText(getString(R.string.s_pick_location));
        } else {
            tvTitle.setText(getString(R.string.s_drop_location));
        }
        vpSelectLocation.addOnPageChangeListener(this);

        mFragmentList = new ArrayList<>();

        if (mIsFromRecipientDetails) {
            if (mSelectedDropLocation != null) {
                llSelectedLocation.setVisibility(View.VISIBLE);
                tvSelectedLocation.setText(mSelectedDropLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            } else if (mSelectedPickupLocation != null) {
                llSelectedLocation.setVisibility(View.VISIBLE);
                tvSelectedLocation.setText(mSelectedPickupLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            }
            mFragmentList.add(MapFragment.getInstance(mIsForPickup, mSelectedPickupLocation, mSelectedDropLocation, RecipientDetailsFragment.class.getName()));
            mFragmentList.add(AdvancedSearchFragment.getInstance(mIsForPickup, getSelectedAddress(), RecipientDetailsFragment.class.getName()));
        } else {
            llSelectedLocation.setVisibility(View.GONE);
            mFragmentList.add(MapFragment.getInstance(mIsForPickup, mSelectedPickupLocation, mSelectedDropLocation, ""));
            mFragmentList.add(AdvancedSearchFragment.getInstance(mIsForPickup, getSelectedAddress(), ""));
        }


        //if user is not guest
        if (mSelectLocationViewModel.getUserType() != null) {
            mFragmentList.add(SavedAddressFragment.getInstance(mIsForPickup, getSelectedAddress(), mIsFromRecipientDetails));
        }

        setUpViewPager();
    }

    private Result getSelectedAddress() {
        if (!mIsFromRecipientDetails) {
            return null;
        } else if (mSelectedDropLocation != null) {
            Result result = new Result();
            SavedLocation savedLocation = new SavedLocation();
            savedLocation.setGovernorate(mSelectedDropLocation.getGovernorate());
            savedLocation.setArea(mSelectedDropLocation.getArea());
            savedLocation.setBlockNumber(mSelectedDropLocation.getBlockNumber());
            savedLocation.setStreet(mSelectedDropLocation.getStreet());
            savedLocation.setAvenue(mSelectedDropLocation.getAvenue());
            savedLocation.setHouseOrbuilding(mSelectedDropLocation.getHouseOrbuilding());
            savedLocation.setApartmentOrOffice(mSelectedDropLocation.getApartmentOrOffice());
            savedLocation.setFloor(mSelectedDropLocation.getFloor());
            savedLocation.setFullAddress(mSelectedDropLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            savedLocation.setGeometry(mSelectedDropLocation.getGeometry());

            result.setSavedLocation(savedLocation);
            result.setAlreadySelected(true);

            return result;
        } else if (mSelectedPickupLocation != null) {
            Result result = new Result();
            SavedLocation savedLocation = new SavedLocation();
            savedLocation.setGovernorate(mSelectedPickupLocation.getGovernorate());
            savedLocation.setArea(mSelectedPickupLocation.getArea());
            savedLocation.setBlockNumber(mSelectedPickupLocation.getBlockNumber());
            savedLocation.setStreet(mSelectedPickupLocation.getStreet());
            savedLocation.setAvenue(mSelectedPickupLocation.getAvenue());
            savedLocation.setHouseOrbuilding(mSelectedPickupLocation.getHouseOrbuilding());
            savedLocation.setApartmentOrOffice(mSelectedPickupLocation.getApartmentOrOffice());
            savedLocation.setFloor(mSelectedPickupLocation.getFloor());
            savedLocation.setFullAddress(mSelectedPickupLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            savedLocation.setGeometry(mSelectedPickupLocation.getGeometry());

            result.setSavedLocation(savedLocation);
            result.setAlreadySelected(true);
            return result;
        } else
            return null;
    }

    private void setUpViewPager() {
        mViewPagerAdapter = new SelectLocationViewPagerAdapter(getChildFragmentManager(), mFragmentList, getTitleList(), mSelectLocationViewModel.getUserType());
        vpSelectLocation.setAdapter(mViewPagerAdapter);
        tlLocation.setupWithViewPager(vpSelectLocation);
        if (mSelectLocationViewModel.getUserType() != null)
            vpSelectLocation.setOffscreenPageLimit(3);
        else
            vpSelectLocation.setOffscreenPageLimit(2);

    }

    private String[] getTitleList() {
        return new String[]{getString(R.string.s_map), getString(R.string.s_advanced_search), getString(R.string.s_saved_address)};
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.iv_back) {
            mHost.onBackPressed();
        }

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        if (mViewPagerAdapter.getItem(i) instanceof SavedAddressFragment) {
            ((SavedAddressFragment) mViewPagerAdapter.getItem(i)).updateSavedAddress(mSavedAddress);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    public void updateSavedAddress(Result result) {
        mSavedAddress = result;
    }


    public interface ISelectLocationHost {
        void onBackPressed();
    }
}
