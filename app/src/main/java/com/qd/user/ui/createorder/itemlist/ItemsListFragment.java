package com.qd.user.ui.createorder.itemlist;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.additem.ItemDetails;
import com.qd.user.model.createorder.request.CreateOrderRequest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ItemsListFragment extends BaseFragment implements ItemsListAdapter.ItemsListAdapterInterface {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_toolbar)
    View viewToolbar;
    @BindView(R.id.rv_items_list)
    RecyclerView rvItemsList;
    @BindView(R.id.fab_add_item)
    FloatingActionButton fabAddItem;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    private Unbinder unbinder;
    private IItemsListHost mHost;
    private ItemsListViewModel mItemsListViewModel;
    private ItemsListAdapter mAdapter;

    private ArrayList<ItemDetails> mItemsList;
    private CreateOrderRequest mCreateOrderDetails;

    public ItemsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_items_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static ItemsListFragment getInstance(ItemDetails item, CreateOrderRequest createOrderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.ITEM_DETAILS, item);
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, createOrderDetails);
        ItemsListFragment itemsListFragment = new ItemsListFragment();
        itemsListFragment.setArguments(bundle);
        return itemsListFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IItemsListHost) {
            mHost = (IItemsListHost) context;
        } else throw new IllegalStateException("Host must implement IItemsListHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mItemsListViewModel = new ViewModelProvider(this).get(ItemsListViewModel.class);
        mItemsListViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

    }

    private void observeLiveData() {
        //observe validations errors

    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(getString(R.string.s_add_item));
        mItemsList = new ArrayList<>();
        getArgumentsData();
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            if (getArguments().containsKey(AppConstants.ITEM_DETAILS)) {
                mItemsList.add((ItemDetails) getArguments().getParcelable(AppConstants.ITEM_DETAILS));
                setUpAdapter();
            }

            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
        }
    }

    private void setUpAdapter() {
        mAdapter = new ItemsListAdapter(mItemsList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rvItemsList.getContext(), LinearLayoutManager.VERTICAL, false);
        rvItemsList.setLayoutManager(linearLayoutManager);
        rvItemsList.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_continue, R.id.fab_add_item})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                break;
            case R.id.btn_continue:
                mHost.navigateToRecipientDetailsFragment(getOrderDetails(), -1);
                break;
            case R.id.fab_add_item:
                mHost.openAddItemFragment(mCreateOrderDetails);
                break;
        }

    }

    private CreateOrderRequest getOrderDetails() {
        mCreateOrderDetails.getDropOffArr().get(0).setItem(mItemsList);
        return mCreateOrderDetails;
    }

    @Override
    public void onEditItemClicked(int adapterPosition) {
        mItemsList.get(adapterPosition).setPos(adapterPosition);
        mHost.backToAddItemFragment(mItemsList.get(adapterPosition), -1, mCreateOrderDetails);
    }

    public void updateItemsList(ItemDetails item) {
        if (item.isForEdit()) {
            mItemsList.set(item.getPos(), item);
            mAdapter.notifyItemChanged(item.getPos());
        } else {
            mItemsList.add(item);
            mAdapter.notifyItemInserted(mItemsList.size() - 1);
        }
    }


    public interface IItemsListHost {
        void onBackPressed();

        void navigateToRecipientDetailsFragment(CreateOrderRequest orderDetails, int i);

        void openAddItemFragment(CreateOrderRequest mCreateOrderDetails);

        void backToAddItemFragment(ItemDetails details, int position, CreateOrderRequest mCreateOrderDetails);
    }
}
