package com.qd.user.ui.trackorder.orderdetail;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;

import org.json.JSONObject;

class OrderDetailRepository {
    void getOrderDetails(String orderId, final RichMediatorLiveData<OrderDetailResponse> orderDetailsLiveData) {
        DataManager.getInstance().hitOrderDetailApi(orderId).enqueue(new NetworkCallback<OrderDetailResponse>() {
            @Override
            public void onSuccess(OrderDetailResponse orderDetailResponse) {
                orderDetailsLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderDetailsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderDetailsLiveData.setError(t);
            }
        });
    }

    void cancelOrder(JSONObject request, final RichMediatorLiveData<OrderCancellationResponse> orderCancellationLiveData) {

        DataManager.getInstance().cancelOrder(request).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse orderDetailResponse) {
                orderCancellationLiveData.setLoadingState(false);
                orderCancellationLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderCancellationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderCancellationLiveData.setError(t);
            }
        });
    }

    void getReceivedOrderDetails(String orderId, final RichMediatorLiveData<OrderDetailResponse> orderDetailsLiveData) {
        DataManager.getInstance().hitReceivedOrderDetailApi(orderId, DataManager.getInstance().getPhoneNumber()).enqueue(new NetworkCallback<OrderDetailResponse>() {
            @Override
            public void onSuccess(OrderDetailResponse orderDetailResponse) {
                orderDetailsLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderDetailsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderDetailsLiveData.setError(t);
            }
        });
    }

    void autoCancelOrder(String id, RichMediatorLiveData<OrderCancellationResponse> autoCancellationLiveData) {
        DataManager.getInstance().autoCancelOrder(id).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse commonResponse) {
                autoCancellationLiveData.setValue(commonResponse);
                autoCancellationLiveData.setLoadingState(false);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                autoCancellationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                autoCancellationLiveData.setError(t);
            }
        });
    }
}
