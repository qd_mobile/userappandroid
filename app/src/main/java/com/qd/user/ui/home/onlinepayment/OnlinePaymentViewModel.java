package com.qd.user.ui.home.onlinepayment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;

public class OnlinePaymentViewModel extends ViewModel {

    private RichMediatorLiveData<WalletRechargeResponse> mInitializePaymentLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private OnlinePaymentRepository mRepo = new OnlinePaymentRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {

        if (mInitializePaymentLiveData == null) {
            mInitializePaymentLiveData = new RichMediatorLiveData<WalletRechargeResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }


    /**
     * This method gives the wallet recharge live data object to {@link OnlinePaymentFragment}
     *
     * @return {@link #mInitializePaymentLiveData}
     */
    RichMediatorLiveData<WalletRechargeResponse> getInitializePaymentLiveData() {
        return mInitializePaymentLiveData;
    }


    void initializePayment(InitializePaymentRequest paymentRequest) {
        mRepo.initializePayment(mInitializePaymentLiveData, paymentRequest);
    }
}

