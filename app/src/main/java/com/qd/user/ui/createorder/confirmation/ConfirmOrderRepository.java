package com.qd.user.ui.createorder.confirmation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import org.json.JSONException;
import org.json.JSONObject;

class ConfirmOrderRepository {

    void getDirectionsApiResponse(final RichMediatorLiveData<DirectionsApiResponse> directionsApiLiveData, String url) {
        DataManager.getInstance().getDirectionsApiResponse(url).enqueue(new NetworkCallback<DirectionsApiResponse>() {
            @Override
            public void onSuccess(DirectionsApiResponse directionsApiResponse) {
                directionsApiLiveData.setValue(directionsApiResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                directionsApiLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                directionsApiLiveData.setError(t);
            }
        });
    }

    void getEta(final RichMediatorLiveData<OptimizedDeliveryResponse> optimizedDeliveryLiveData, OptimizedDeliveryRequest dataForOptimizedRoute) {
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().getEta(new JSONObject(gson.toJson(dataForOptimizedRoute))).enqueue(new NetworkCallback<OptimizedDeliveryResponse>() {
                @Override
                public void onSuccess(OptimizedDeliveryResponse optimizedDeliveryResponse) {
                    optimizedDeliveryLiveData.setValue(optimizedDeliveryResponse);
                    optimizedDeliveryLiveData.setLoadingState(false);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    optimizedDeliveryLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    optimizedDeliveryLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
