package com.qd.user.ui.home.createdorder;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.myorders.Datum;
import com.qd.user.model.myorders.DropLocation;
import com.qd.user.model.myorders.PickupLocation;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.PaginationListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderCreatedFragment extends BaseFragment implements PaginationListener.PaginationListenerCallbacks, DatePickerDialog.OnDateSetListener, OrderCreatedAdapter.IMyOrders {
    private static final int HOUR_OF_DAY = 23;
    private static final int MINUTE = 59;
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.rv_orders_listing)
    RecyclerView rvOrdersListing;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    private Unbinder unbinder;
    @BindView(R.id.tv_label_order_id)
    View tvLabelOrderId;
    @BindView(R.id.tv_day)
    View tvDay;
    @BindView(R.id.tv_order_id)
    View tvOrderId;
    @BindView(R.id.tv_date)
    View tvDate;
    @BindView(R.id.view_my_order)
    View viewMyOrder;
    @BindView(R.id.iv_order_type)
    View ivOrderType;
    @BindView(R.id.tv_order_type)
    View tvOrderType;
    @BindView(R.id.tv_order_status)
    View tvOrderStatus;
    @BindView(R.id.iv_pickup_location)
    View ivPickupLocation;
    @BindView(R.id.tv_label_pickup_location)
    View tvLabelPickupLocation;
    @BindView(R.id.tv_pickup_location)
    View tvPickupLocation;
    @BindView(R.id.view_dotted_line)
    View viewDottedLine;
    @BindView(R.id.iv_drop_off_location)
    View ivDropOffLocation;
    @BindView(R.id.tv_label_drop_off_location)
    View tvLabelDropOffLocation;
    @BindView(R.id.tv_drop_off_location)
    View tvDropOffLocation;
    @BindView(R.id.rl_location_info)
    RelativeLayout rlLocationInfo;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerOrders;

    private OrderCreatedViewModel mCreateOrderViewModel;
    private ArrayList<Object> mOrdersList;

    private OrderCreatedAdapter mOrdersAdapter;
    private int mNextPage;
    private PaginationListener mPaginationListener;
    private boolean isRefreshing;
    private String mFromDate;
    private String mToDate;

    private ICreatedOrderHost mHost;

    private Date mStartDate;
    private Date mEndDate;
    private Calendar mCalendar;
    private String selectDateFor;
    private LinearLayoutManager layoutManager;

    public static OrderCreatedFragment getInstance() {
        return new OrderCreatedFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCreateOrderViewModel = new ViewModelProvider(this).get(OrderCreatedViewModel.class);
        mCreateOrderViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

        clearFilters();

        showShimmerEffect();
        mCreateOrderViewModel.getCreatedOrdersList(mNextPage, mFromDate, mToDate);

        mCreateOrderViewModel.getCancellationReasons();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    protected void initViewsAndVariables() {
        mOrdersList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mCalendar = Calendar.getInstance();
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);

        setRefreshLayoutListener();
        setUpAdapter();
    }

    private void observeLiveData() {
        mCreateOrderViewModel.getOrdersLiveData().observe(getViewLifecycleOwner(), ordersResponse -> {
            hideShimmerEffect();
            hideProgressDialog();
            if (ordersResponse != null) {

                if (ordersResponse.getResult().getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (ordersResponse.getResult().getData() != null && ordersResponse.getResult().getData().size() > 0) {
                    if (mOrdersList != null && mOrdersList.size() > 0) {
                        mOrdersAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mOrdersList.clear();
                        }
                        setResponseToAdapter(ordersResponse.getResult().getData());
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mOrdersList.clear();
                        }

                        setResponseToAdapter(ordersResponse.getResult().getData());
                        rvOrdersListing.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }

            }
        });
    }

    /**
     * hide recycler view and and show no data found  message if data is not available
     */
    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        rvOrdersListing.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    /**
     * Sets orders from response to adapter and calls {@code mOrdersAdapter.notifyDataSetChanged()}
     * to reflect the changes
     *
     * @param ordersList Response containing all the orders, it can't be null and its size will
     *                       always be greater than 0, as it's always called after checking for null
     *                       on its contents.
     */
    private void setResponseToAdapter(Collection<Datum> ordersList) {
        tvNoList.setVisibility(View.GONE);
        rvOrdersListing.setVisibility(View.VISIBLE);
        mOrdersList.addAll(ordersList);
        mOrdersAdapter.notifyDataSetChanged();
    }


    /**
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        swipeRefresh.setOnRefreshListener(
                () -> {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    refreshList();
                    mPaginationListener.setFetchingStatus(false);

                }
        );
    }


    public boolean showOrHideFilter() {
        if (llFilter.getVisibility() == View.VISIBLE) {
            llFilter.setVisibility(View.GONE);
            TransitionManager.beginDelayedTransition(llFilter);
            clearFilters();
            mCreateOrderViewModel.getCreatedOrdersList(mNextPage, mFromDate, mToDate);
            return false;
        } else {
            llFilter.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(llFilter);
            return true;
        }
    }


    private void openDatePicker() {
        Date minDate;
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.setTime(mStartDate);
            minDate = DateFormatter.getInstance().getParsedDate(mCreateOrderViewModel.getRegistrationDate());
        } else {
            mCalendar.setTime(mEndDate);
            minDate = mStartDate;
        }

        if (getContext() != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(minDate.getTime());
            datePickerDialog.show();
        }
    }


    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrdersListing.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrdersListing.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);

    }

    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerOrders.stopShimmer();
        shimmerOrders.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);

    }

    private void showShimmerEffect() {
        tvNoList.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.GONE);
        shimmerOrders.setVisibility(View.VISIBLE);
        shimmerOrders.startShimmer();
    }


    @Override
    public void loadMoreItems() {
        mOrdersAdapter.showProgress();
        mCreateOrderViewModel.getCreatedOrdersList(mNextPage, mFromDate, mToDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.set(year, month, dayOfMonth, 0, 0);
            mFromDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mStartDate = DateFormatter.getInstance().getParsedDate(mFromDate);
            tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        } else {
            mCalendar.set(year, month, dayOfMonth, HOUR_OF_DAY, MINUTE);
            mToDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mEndDate = DateFormatter.getInstance().getParsedDate(mToDate);
            tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(mToDate));
        }
    }


    public boolean isFilterApplied() {
        return llFilter.getVisibility() == View.VISIBLE;
    }

    

    private void setUpAdapter() {
        mOrdersAdapter = new OrderCreatedAdapter(mOrdersList, this);
        rvOrdersListing.setLayoutManager(layoutManager);
        rvOrdersListing.setAdapter(mOrdersAdapter);
        rvOrdersListing.scheduleLayoutAnimation();
        rvOrdersListing.addOnScrollListener(mPaginationListener);
    }

    @Override
    public void onTrackOrderClicked(String id) {
        mHost.trackCurrentOrder(id, false);
    }

    @Override
    public void onRowOrderViewClick(String orderId, String orderType) {
        if (orderType.equals(getString(R.string.business))) {
            mHost.openBusinessOrderDetails(orderId);
        } else
            mHost.openOrderDetailFragment(orderId, false);

    }

    @Override
    public void showPickupAddress(PickupLocation pickupLocation) {
        showFullAddressAlertDialog(pickupLocation.getArea(), pickupLocation.getBlockNumber(), pickupLocation.getStreet(), pickupLocation.getHouseOrbuilding(),
                pickupLocation.getHouseOrbuilding(), pickupLocation.getFloor(), pickupLocation.getPickupInstructions());
    }

    @Override
    public void showDropAddress(DropLocation dropLocation) {
        showFullAddressAlertDialog(dropLocation.getArea(), dropLocation.getBlockNumber(), dropLocation.getStreet(), dropLocation.getHouseOrbuilding(),
                dropLocation.getHouseOrbuilding(), dropLocation.getFloor(), dropLocation.getDriverNote());
    }

    private void showFullAddressAlertDialog(CharSequence area, CharSequence blockNumber, String street, String houseOrBuilding, String apartmentOrOffice, String floor, String driverNote) {
        if (getContext() != null) {
            final Dialog addressDialog = new Dialog(getContext(), R.style.customDialog);
            addressDialog.setContentView(R.layout.dialog_view_address);
            addressDialog.setCancelable(true);

            ((TextView) addressDialog.findViewById(R.id.tv_area)).setText(area);
            ((TextView) addressDialog.findViewById(R.id.tv_block)).setText(blockNumber);

            if (street != null && !street.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_street)).setText(street);
            else {
                addressDialog.findViewById(R.id.tv_street).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_street).setVisibility(View.GONE);
            }

            if (houseOrBuilding != null && !houseOrBuilding.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_house_building)).setText(houseOrBuilding);
            else {
                addressDialog.findViewById(R.id.tv_house_building).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_house_building).setVisibility(View.GONE);
            }

            if (floor != null && !floor.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_floor)).setText(floor);
            else {
                addressDialog.findViewById(R.id.tv_floor).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_floor).setVisibility(View.GONE);
            }

            if (apartmentOrOffice != null && !apartmentOrOffice.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_apartment)).setText(apartmentOrOffice);
            else {
                addressDialog.findViewById(R.id.tv_apartment).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_apartment).setVisibility(View.GONE);
            }

            if (driverNote != null && !driverNote.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_note)).setText(driverNote);
            else {
                addressDialog.findViewById(R.id.tv_label_note).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_note).setVisibility(View.GONE);
            }

            AppUtils.getInstance().dimDialogBackground(addressDialog);
            addressDialog.show();
        }
    }


    @OnClick({R.id.tv_filter, R.id.tv_start_date, R.id.tv_end_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_filter:
                mNextPage = 1;
                isRefreshing = true;
                showShimmerEffect();
                mCreateOrderViewModel.getCreatedOrdersList(mNextPage, mFromDate, mToDate);
                break;
            case R.id.tv_start_date:
                selectDateFor = getString(R.string.s_from_date);
                openDatePicker();
                break;
            case R.id.tv_end_date:
                selectDateFor = getString(R.string.s_to_date);
                openDatePicker();
                break;
        }

    }


    public void refreshList() {
        isRefreshing = true;
        mNextPage = 1;
        mCreateOrderViewModel.getCreatedOrdersList(mNextPage, mFromDate, mToDate);
    }


    private void clearFilters() {
        mNextPage = 1;
        isRefreshing = true;
        mFromDate = mCreateOrderViewModel.getRegistrationDate();
        mStartDate = DateFormatter.getInstance().getParsedDate(mFromDate);
        //mToDate = DateFormatter.getInstance().getFormattedDateInUtc(Calendar.getInstance().getTime());
        mEndDate = Calendar.getInstance().getTime();
        mToDate = null;

        tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(DateFormatter.getInstance().getFormattedDateInUtc(Calendar.getInstance().getTime())));
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof ICreatedOrderHost) {
            mHost = (ICreatedOrderHost) getParentFragment();
        } else throw new IllegalStateException("Host must implement ICreatedOrderHost");
    }


    public interface ICreatedOrderHost {

        void openOrderDetailFragment(String orderId, boolean isOrderReceived);

        void trackCurrentOrder(String id, boolean isFromReceived);

        void openBusinessOrderDetails(String orderId);
    }
}
