package com.qd.user.ui.home.personalpayment;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.personal.PaymentHistoryResponse;

import java.util.HashMap;

class PersonalPaymentsRepository {
    void getPaymentHistory(final RichMediatorLiveData<PaymentHistoryResponse> paymentHistoryResponseLiveData, int page, String fromDate, String toDate) {
        DataManager.getInstance().getPersonalPaymentHistory(createPayloadForPaymentHistory(page, fromDate, toDate)).enqueue(new NetworkCallback<PaymentHistoryResponse>() {
            @Override
            public void onSuccess(PaymentHistoryResponse PaymentHistoryResponse) {
                paymentHistoryResponseLiveData.setValue(PaymentHistoryResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                paymentHistoryResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                paymentHistoryResponseLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> createPayloadForPaymentHistory(int page, String fromDate, String toDate) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put("cashClientId", DataManager.getInstance().getUserId());
        mRequestMap.put("page", page);
        mRequestMap.put("limit", "10");
        mRequestMap.put("fromDate", fromDate);
        mRequestMap.put("toDate", toDate);

        return mRequestMap;
    }

    public String getRegistrationDate() {
        return DataManager.getInstance().getRegistrationDateAndTime();
    }
}
