package com.qd.user.ui.home.personalpayment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.personal.Datum;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.PaginationListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PersonalPaymentsFragment extends BaseFragment implements
        PaginationListener.PaginationListenerCallbacks, DatePickerDialog.OnDateSetListener {

    private static final int MINUTE = 59;
    private static final int HOUR_OF_DAY = 23;
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.rv_orders_listing)
    RecyclerView rvOrdersListing;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;

    private Unbinder unbinder;
    private ArrayList<Object> mPersonalPaymentsList;
    private PersonalPaymentsAdapter mAdapter;
    private boolean isRefreshing;
    private String mToDate, mFromDate;
    private Date mStartDate, mEndDate;
    private String selectDateFor;
    private Calendar mCalendar;
    /**
     * A {@link IPersonalPaymentsHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IPersonalPaymentsHost mPersonalPaymentsHost;
    private PersonalPaymentsViewModel mPersonalPaymentsViewModel;
    private int mNextPage;
    private PaginationListener mPaginationListener;
    private LinearLayoutManager layoutManager;

    public PersonalPaymentsFragment() {
        // Required empty public constructor
    }

    public static PersonalPaymentsFragment getInstance() {
        return new PersonalPaymentsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof IPersonalPaymentsHost) {
            mPersonalPaymentsHost = (IPersonalPaymentsHost) getParentFragment();
        } else
            throw new IllegalStateException("host must implement IPersonalPaymentsHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_orders, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPersonalPaymentsViewModel = new ViewModelProvider(this).get(PersonalPaymentsViewModel.class);
        mPersonalPaymentsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        observeData();
        clearFilters();

        showShimmerEffect();
        mPersonalPaymentsViewModel.getPersonalPaymentsHistory(mNextPage, mFromDate, mToDate);


    }

    private void observeData() {

        mPersonalPaymentsViewModel.getPaymentHistoryLiveData().observe(getViewLifecycleOwner(), paymentHistoryResponse -> {
            hideShimmerEffect();
            hideProgressDialog();
            if (paymentHistoryResponse != null) {

                if (paymentHistoryResponse.getResult().getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (paymentHistoryResponse.getResult().getData() != null && paymentHistoryResponse.getResult().getData().size() > 0) {
                    if (mPersonalPaymentsList != null && mPersonalPaymentsList.size() > 0) {
                        mAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mPersonalPaymentsList.clear();
                        }
                        setResponseToAdapter(paymentHistoryResponse.getResult().getData());
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mPersonalPaymentsList.clear();
                        }

                        setResponseToAdapter(paymentHistoryResponse.getResult().getData());
                        rvOrdersListing.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }

            }
        });
    }

    private void setResponseToAdapter(Collection<Datum> paymentHistoryResponse) {
        tvNoList.setVisibility(View.GONE);
        rvOrdersListing.setVisibility(View.VISIBLE);
        mPersonalPaymentsList.addAll(paymentHistoryResponse);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    protected void initViewsAndVariables() {
        mPersonalPaymentsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mCalendar = Calendar.getInstance();
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);

        setRefreshLayoutListener();
        setPersonalPaymentsAdapter();
    }

    private void setPersonalPaymentsAdapter() {
        mAdapter = new PersonalPaymentsAdapter(mPersonalPaymentsList);
        rvOrdersListing.setLayoutManager(layoutManager);
        rvOrdersListing.setAdapter(mAdapter);
        rvOrdersListing.scheduleLayoutAnimation();
        rvOrdersListing.addOnScrollListener(mPaginationListener);
    }

    private void clearFilters() {
        mNextPage = 1;
        isRefreshing = true;
        mFromDate = mPersonalPaymentsViewModel.getRegistrationDate();
        mStartDate = DateFormatter.getInstance().getParsedDate(mPersonalPaymentsViewModel.getRegistrationDate());
        mToDate = DateFormatter.getInstance().getFormattedDateInUtc(Calendar.getInstance().getTime());
        mEndDate = Calendar.getInstance().getTime();

        tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(mToDate));
    }

    /**
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        swipeRefresh.setOnRefreshListener(
                () -> {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    refreshList();
                    mPaginationListener.setFetchingStatus(false);

                }
        );
    }

    private void refreshList() {
        isRefreshing = true;
        mNextPage = 1;
        mPersonalPaymentsViewModel.getPersonalPaymentsHistory(mNextPage, mFromDate, mToDate);
    }

    /**
     * Hides recycler view and and show no data found  message if data is not available
     */
    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        rvOrdersListing.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        hideShimmerEffect();
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    public boolean showOrHideFilter() {
        if (llFilter.getVisibility() == View.VISIBLE) {
            llFilter.setVisibility(View.GONE);
            TransitionManager.beginDelayedTransition(llFilter);
            clearFilters();
            mPersonalPaymentsViewModel.getPersonalPaymentsHistory(mNextPage, mFromDate, mToDate);
            return false;
        } else {
            llFilter.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(llFilter);
            return true;
        }
    }

    @OnClick({R.id.tv_filter, R.id.tv_start_date, R.id.tv_end_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_filter:
                mNextPage = 1;
                isRefreshing = true;
                showShimmerEffect();
                mPersonalPaymentsViewModel.getPersonalPaymentsHistory(mNextPage, mFromDate, mToDate);
                break;
            case R.id.tv_start_date:
                selectDateFor = getString(R.string.s_from_date);
                openDatePicker();
                break;
            case R.id.tv_end_date:
                selectDateFor = getString(R.string.s_to_date);
                openDatePicker();
                break;
        }

    }

    private void openDatePicker() {
        Date minDate;
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.setTime(mStartDate);
            minDate = DateFormatter.getInstance().getParsedDate(mPersonalPaymentsViewModel.getRegistrationDate());
        } else {
            mCalendar.setTime(mEndDate);
            minDate = mStartDate;
        }

        if (getContext() != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(minDate.getTime());
            datePickerDialog.show();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrdersListing.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrdersListing.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);

    }

    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerViewContainer.stopShimmer();
        shimmerViewContainer.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);

    }

    private void showShimmerEffect() {
        swipeRefresh.setVisibility(View.GONE);
        shimmerViewContainer.setVisibility(View.VISIBLE);
        shimmerViewContainer.startShimmer();
    }


    @Override
    public void loadMoreItems() {
        mAdapter.showProgress();
        mPersonalPaymentsViewModel.getPersonalPaymentsHistory(mNextPage, mFromDate, mToDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.set(year, month, dayOfMonth, 0, 0);
            mFromDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mStartDate = DateFormatter.getInstance().getParsedDate(mFromDate);
            tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        } else {
            mCalendar.set(year, month, dayOfMonth, HOUR_OF_DAY, MINUTE);
            mToDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mEndDate = DateFormatter.getInstance().getParsedDate(mToDate);
            tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(mToDate));
        }
    }


    public boolean isFilterApplied() {
        return llFilter.getVisibility() == View.VISIBLE;
    }


    public interface IPersonalPaymentsHost {

    }
}
