package com.qd.user.ui.createorder.additem;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.createorder.additem.itemstype.Result;

import java.util.ArrayList;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemTypeAdapter extends RecyclerView.Adapter<ItemTypeAdapter.ItemTypeAdapterViewHolder> {

    private final ArrayList<Result> mAddressList;
    private AdvanceSearchAdapterInterface mInterface;


    ItemTypeAdapter(TreeSet<Result> addressList, AdvanceSearchAdapterInterface advanceSearchAdapterInterface) {
        mAddressList = new ArrayList<>(addressList);
        mInterface = advanceSearchAdapterInterface;
    }

    @NonNull
    @Override
    public ItemTypeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_dialog, viewGroup, false);
        return new ItemTypeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemTypeAdapterViewHolder ItemTypeAdapterViewHolder, int i) {

        if (!mAddressList.get(i).getSuggestedVehicleCategory().isEmpty()) {
            ItemTypeAdapterViewHolder.tvFavAddress.setText(String.format("%s (%s)", mAddressList.get(i).getKeyword(),
                    mAddressList.get(i).getSuggestedVehicleCategory()));
        } else {
            ItemTypeAdapterViewHolder.tvFavAddress.setText(mAddressList.get(i).getKeyword());
        }

    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }

    public interface AdvanceSearchAdapterInterface {

        void onItemSelected(Result item, String title);
    }

    class ItemTypeAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_fav_address)
        TextView tvFavAddress;

        ItemTypeAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> mInterface.onItemSelected(mAddressList.get(getAdapterPosition()), tvFavAddress.getText().toString()));
        }
    }
}
