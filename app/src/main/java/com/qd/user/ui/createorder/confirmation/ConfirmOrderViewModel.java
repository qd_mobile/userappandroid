package com.qd.user.ui.createorder.confirmation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

public class ConfirmOrderViewModel extends ViewModel {
    private RichMediatorLiveData<DirectionsApiResponse> mDirectionsApiLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private ConfirmOrderRepository mRepo = new ConfirmOrderRepository();
    private RichMediatorLiveData<OptimizedDeliveryResponse> mOptimizedDeliveryLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mDirectionsApiLiveData == null) {
            mDirectionsApiLiveData = new RichMediatorLiveData<DirectionsApiResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mOptimizedDeliveryLiveData == null) {
            mOptimizedDeliveryLiveData = new RichMediatorLiveData<OptimizedDeliveryResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }


        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the ConfirmOrder live data object to {@link ConfirmOrderFragment}
     *
     * @return {@link #mDirectionsApiLiveData}
     */
    RichMediatorLiveData<DirectionsApiResponse> getDirectionsApiLiveData() {
        return mDirectionsApiLiveData;
    }

    /**
     * This method gives the delivery Charges live data object to {@link ConfirmOrderFragment}
     *
     * @return {@link #mOptimizedDeliveryLiveData}
     */
    RichMediatorLiveData<OptimizedDeliveryResponse> getOptimizedDeliveryLiveData() {
        return mOptimizedDeliveryLiveData;
    }

    void getDirectionsResponse(String url) {
        mRepo.getDirectionsApiResponse(mDirectionsApiLiveData, url);
    }

    void getEta(OptimizedDeliveryRequest data) {
        mRepo.getEta(mOptimizedDeliveryLiveData, data);
    }
}
