package com.qd.user.ui.onboard.resetpassword;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.HashMap;

class ResetPasswordRepository {
    public void hitResetPasswordApi(String newPassword, final RichMediatorLiveData<CommonResponse> mResetPasswordLiveData, String token) {
        DataManager.getInstance().hitResetPasswordApi(createResetPasswordObject(newPassword, token)).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                mResetPasswordLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mResetPasswordLiveData.setFailure(failureResponse);

            }

            @Override
            public void onError(Throwable t) {
                mResetPasswordLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> createResetPasswordObject(String newPassword, String token) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_TOKEN, token);
        hashMap.put(NetworkConstants.KEY_NEW_PASSWORD, newPassword);
        return hashMap;
    }
}
