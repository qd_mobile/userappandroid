package com.qd.user.ui.onboard.login;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.HashMap;

class LoginRepository {


    private void hitLoginApi(final RichMediatorLiveData<CreateAccountResponse> loginLiveData, String email, String password, String deviceId) {
        DataManager.getInstance().hitLoginApi(createRequestPayloadForLogin(email, password, deviceId)).enqueue(new NetworkCallback<CreateAccountResponse>() {

            @Override
            public void onSuccess(CreateAccountResponse createAccountResponse) {
                setUserDataInPreferences(createAccountResponse);
                loginLiveData.setValue(createAccountResponse);
                loginLiveData.setLoadingState(false);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                loginLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                loginLiveData.setError(t);
            }

            private void setUserDataInPreferences(CreateAccountResponse loginResponse) {
                DataManager.getInstance().setUserName(loginResponse.getResult().getUserDetails().getName());
                DataManager.getInstance().setAccessToken(loginResponse.getResult().getAuthenticationToken());
                DataManager.getInstance().setCountryCode(loginResponse.getResult().getUserDetails().getContact().getCcPersonal());
                DataManager.getInstance().setPhoneNumber(loginResponse.getResult().getUserDetails().getContact().getPersonal());
                DataManager.getInstance().setEmailAddress(loginResponse.getResult().getUserDetails().getEmail());
                DataManager.getInstance().setUserLoggedIn(true);

                DataManager.getInstance().setServiceType(loginResponse.getResult().getUserDetails().getServicesOffered());
                DataManager.getInstance().setRegistrationDateAndTime(loginResponse.getResult().getUserDetails().getCreatedAt());
                DataManager.getInstance().setUserId(loginResponse.getResult().getUserDetails().getId());
                DataManager.getInstance().setUserType(loginResponse.getResult().getUserDetails().getCashClientStatus());
                DataManager.getInstance().setCurrentStatus(loginResponse.getResult().getUserDetails().getCurrentStatus());
                DataManager.getInstance().setVendorStatus(loginResponse.getResult().getUserDetails().getStatus());
                DataManager.getInstance().setCashClientCurrentStatus(loginResponse.getResult().getUserDetails().getCashClientCurrentStatus());
                DataManager.getInstance().setSignUpType(loginResponse.getResult().getUserDetails().getSignupType());
                DataManager.getInstance().setBusinessName(loginResponse.getResult().getUserDetails().getBusinessName());
                DataManager.getInstance().setCompanyProfile(loginResponse.getResult().getUserDetails().getCompanyProfile());

                if (loginResponse.getResult().getUserDetails().getCompany() != null)
                    DataManager.getInstance().setCompanyContact(loginResponse.getResult().getUserDetails().getCompany().getContact().getCcPersonal() +
                            loginResponse.getResult().getUserDetails().getCompany().getContact().getPersonal());

                String fullBusinessAddress = new GsonBuilder().create().toJson(loginResponse.getResult().getUserDetails().getFullBusinessAddress());
                DataManager.getInstance().setBusinessAddress(fullBusinessAddress);
                DataManager.getInstance().setBusinessFullAddress(loginResponse.getResult().getUserDetails().getFullBusinessAddress().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));


            }
        });


    }

    void getFireBaseToken(final RichMediatorLiveData<CreateAccountResponse> loginLiveData, final String username, final String password, final String deviceId) {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("Failed", "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    if (task.getResult() != null && !task.getResult().getToken().isEmpty()) {
                        DataManager.getInstance().setDeviceToken(task.getResult().getToken());
                        hitLoginApi(loginLiveData, username, password, deviceId);
                    }
                });
    }



    private HashMap<String, String> createRequestPayloadForLogin(String email, String password, String deviceId) {

        final HashMap<String, String> loginMap = new HashMap<>();

        loginMap.put(NetworkConstants.KEY_EMAIL,email);
        loginMap.put(NetworkConstants.KEY_USERNAME,"");
        loginMap.put(NetworkConstants.KEY_PASSWORD,password);
        loginMap.put(NetworkConstants.KEY_DEVICE_ID,deviceId);
        loginMap.put(NetworkConstants.KEY_DEVICE_TOKEN, DataManager.getInstance().getDeviceToken());
        loginMap.put(NetworkConstants.KEY_TYPE, AppConstants.userType.CASH_CLIENT);
        loginMap.put(NetworkConstants.KEY_PLATFORM,AppConstants.ANDROID);

        return loginMap;
    }


    void setUserVerified(boolean userVerified) {
        DataManager.getInstance().setUserVerified(userVerified);
    }
}
