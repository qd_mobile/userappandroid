package com.qd.user.ui.createorder.selectlocation.map;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;

import com.google.android.libraries.places.api.model.AutocompletePrediction;

import java.util.ArrayList;
import java.util.List;

class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    private ArrayList<AutocompletePrediction> resultList;
    private final Object lock;
    private List<AutocompletePrediction> mPredictionList;


    PlacesAutoCompleteAdapter(Context context, int resource, ArrayList<AutocompletePrediction> placesAutoCompleteList) {
        super(context, resource);
        resultList = placesAutoCompleteList;
        lock = new Object();
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position).getFullText(null).toString();
    }


    @NonNull
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults filterResults = new FilterResults();

                if (mPredictionList == null) {
                    synchronized (lock) {
                        mPredictionList = new ArrayList<>(resultList);
                    }
                }


                if (constraint != null) {

                    synchronized (lock) {
                        filterResults.values = mPredictionList;
                        filterResults.count = mPredictionList.size();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

        };
    }

    public ArrayList<AutocompletePrediction> getList() {
        return resultList;
    }

    interface PlaceAutoCompleteAdapterInterface {

    }


}
