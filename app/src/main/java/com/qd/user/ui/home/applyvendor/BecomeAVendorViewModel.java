package com.qd.user.ui.home.applyvendor;

import android.text.TextUtils;
import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;
import com.qd.user.model.vendor.becomeavendor.BecomeAVendorInfo;
import com.qd.user.model.vendor.uniquevendor.UniqueEmailResponse;
import com.qd.user.model.vendor.vendorsignup.VendorSignUpResponse;

public class BecomeAVendorViewModel extends ViewModel {
    private RichMediatorLiveData<VendorSignUpResponse> mBecomeAVendorLiveData;
    private RichMediatorLiveData<UniqueEmailResponse> mUniquVendorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;
    private BecomeAVendorRepository mRepo = new BecomeAVendorRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mBecomeAVendorLiveData == null) {
            mBecomeAVendorLiveData = new RichMediatorLiveData<VendorSignUpResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if (mValidateLiveData == null) {
                mValidateLiveData = new MutableLiveData<>();
            }
        }
        if (mUniquVendorLiveData == null) {
            mUniquVendorLiveData = new RichMediatorLiveData<UniqueEmailResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };


        }
    }

    /**
     * This method gives the login live data object to {@link BecomeAVendorFragment}
     *
     * @return {@link #mBecomeAVendorLiveData}
     */
    RichMediatorLiveData<VendorSignUpResponse> getBecomeAVendorLiveData() {
        return mBecomeAVendorLiveData;
    }

    /**
     * This method gives the login live data object to {@link BecomeAVendorFragment}
     *
     * @return {@link #mBecomeAVendorLiveData}
     */
    RichMediatorLiveData<UniqueEmailResponse> getUniqueVendorLiveData() {
        return mUniquVendorLiveData;
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }


    private boolean validateInputFields(BecomeAVendorInfo vendorInfo) {
        if (TextUtils.isEmpty(vendorInfo.getCompanyName())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_COMPANY_NAME, R.string.s_please_enter_company_name));
            return false;
        } /*else if (!Pattern.matches("[a-zA-Z ]+", vendorInfo.getCompanyName())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_COMPANY_NAME, R.string.s_please_enter_valid_company_name));
            return false;
        }*/ else if (!vendorInfo.getPhoneNumber().matches("[0-9]{7,9}$")) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER, R.string.s_please_enter_valid_phone_number));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(vendorInfo.getEmailAddress()).matches()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_invalid_email));
            return false;
        } /*else if (!Pattern.matches("[a-zA-Z ]+", vendorInfo.getName())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_NAME, R.string.s_please_enter_a_valid_name));
            return false;
        }*/ else if (vendorInfo.getAddress().isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ADDRESS, R.string.s_please_enter_valid_address));
        }

        return true;
    }

    void onSubmitViewClicked(BecomeAVendorInfo vendorInfo, String deviceId) {
        if (validateInputFields(vendorInfo)) {
            mRepo.hitVendorSignUpApi(vendorInfo, mBecomeAVendorLiveData, deviceId);
        }
    }

    public String getUserType() {
        return mRepo.getUserType();
    }

    void checkForUniqueEmail(String email) {
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_invalid_email));
        } else mRepo.checkForUniqueEmail(email, mUniquVendorLiveData);
    }

    String getEmailId() {
        return mRepo.getEmailId();
    }

    public String getPhoneNumber() {
        return mRepo.getPhoneNumber();

    }

    public String getName() {
        return mRepo.getName();
    }
}
