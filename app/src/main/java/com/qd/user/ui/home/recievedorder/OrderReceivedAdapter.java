package com.qd.user.ui.home.recievedorder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.recievedorders.Datum;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.ProgressLoader;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class
OrderReceivedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;

    private ArrayList<Object> mReceivedOrdersList;
    private IReceivedOrders mIReceivedOrders;


    OrderReceivedAdapter(ArrayList<Object> ordersList, IReceivedOrders iReceivedOrders) {
        mIReceivedOrders = iReceivedOrders;
        mReceivedOrdersList = ordersList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_my_orders, viewGroup, false);
            return new OrderReceivedAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mReceivedOrdersList.get(position) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mReceivedOrdersList.get(position) instanceof Datum) {
            Datum datum = (Datum) mReceivedOrdersList.get(position);
            ((OrderReceivedAdapterViewHolder) holder).tvOrderId.setText(String.format("ID - #%s", datum.getOrderUniqueId()));
            ((OrderReceivedAdapterViewHolder) holder).tvCreatorName.setText(String.format("(%s)", datum.getPickupLocation().get(0).getRecipientName()));

            if (datum.getPickupLocation().size() > 0 &&
                    (datum.getOrderType().equals(AppConstants.OrderType.GET) || datum.getOrderType().equals(AppConstants.OrderType.SPECIAL_GET))) {
                ((OrderReceivedAdapterViewHolder) holder).tvPickupLocation.setText(datum.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            } else {
                ((OrderReceivedAdapterViewHolder) holder).tvPickupLocation.setVisibility(View.GONE);
            }

            if (datum.getDropLocation().size() > 0) {
                ((OrderReceivedAdapterViewHolder) holder).tvDropOffLocation.setText(datum.getDropLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
                ((OrderReceivedAdapterViewHolder) holder).tvReceiverName.setText(String.format("(%s)", datum.getDropLocation().get(0).getRecipientName()));

            }
            if (datum.getScheduledStartTime() != null) {
                ((OrderReceivedAdapterViewHolder) holder).tvDate.setText(DateFormatter.getInstance().getFormattedDate(datum.getScheduledStartTime()));
                ((OrderReceivedAdapterViewHolder) holder).tvTime.setText(DateFormatter.getInstance().getFormattedTime(datum.getScheduledStartTime()));
                ((OrderReceivedAdapterViewHolder) holder).tvDay.setText(DateFormatter.getInstance().getFormattedDay(datum.getScheduledStartTime()));

            }


            ((OrderReceivedAdapterViewHolder) holder).tvOrderType.setText(datum.getOrderType());
            switch (datum.getOrderType()) {
                case AppConstants.OrderType.SEND:
                case AppConstants.OrderType.SPECIAL_SEND:
                    ((OrderReceivedAdapterViewHolder) holder).ivOrderType.setImageDrawable(((OrderReceivedAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_send));
                    break;
                case AppConstants.OrderType.GET:
                case AppConstants.OrderType.SPECIAL_GET:
                    ((OrderReceivedAdapterViewHolder) holder).ivOrderType.setImageDrawable(((OrderReceivedAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_get));
                    break;
                case AppConstants.OrderType.BUSINESS:
                    ((OrderReceivedAdapterViewHolder) holder).ivOrderType.setImageDrawable(((OrderReceivedAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_business));
                    break;
            }

            ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());

            switch (datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus()) {
                case AppConstants.OrderStatus.ORDER_CREATED:
                case AppConstants.OrderStatus.CASHBACK_INITIATED:
                case AppConstants.OrderStatus.RETURN_INITIATED:
                case AppConstants.OrderStatus.DRIVER_ALIGNED:
                case AppConstants.OrderStatus.DELIVERED:
                    if (datum.getDropLocation().size() == 1 &&
                            (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                    .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).
                                getDriverTrackingStatus().size() - 1).getStatus());
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorBlue));
                    } else {
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorGreenishTeal));
                    }
                    break;
                case AppConstants.OrderStatus.RETURNED:
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                    break;
                case AppConstants.OrderStatus.CANCELLED:
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_reddish));
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getColor(R.color.colorDarkRed));
                    break;
                case AppConstants.OrderStatus.CASHBACK_RETURNED:
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                    break;

                case AppConstants.OrderStatus.OUT_FOR_DELIVERY:
                    if (datum.getDropLocation().size() == 1 &&
                            (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                    .equals(AppConstants.DriverTrackingStatus.DRIVE_TO_DROP_LOCATION)) ||
                            (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                    .equals(AppConstants.DriverTrackingStatus.REACHED_TO_DROP_LOCATION))) {
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setText(R.string.s_track);
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_rectangle_stroke_gun_metal));
                        ((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrderReceivedAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorTextBlack));
                    }
                    break;
            }


        }

    }

    @Override
    public int getItemCount() {
        return mReceivedOrdersList.size();
    }

    //hide progress loader after getting paginated data
    public void hideProgress() {
        mReceivedOrdersList.remove(mReceivedOrdersList.size() - 1);
        notifyDataSetChanged();
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mReceivedOrdersList.add(new ProgressLoader());
        notifyItemInserted(mReceivedOrdersList.size());
    }

    @Override
    public int getItemViewType(int position) {
        if (mReceivedOrdersList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }

    public interface IReceivedOrders {
        void onRowOrderViewClick(String id);

        void onTrackOrderClicked(String id, boolean b);
    }

    class OrderReceivedAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_label_order_id)
        TextView tvLabelOrderId;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.view_date_and_time)
        View viewDateAndTime;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.view_my_order)
        View viewMyOrder;
        @BindView(R.id.iv_order_type)
        ImageView ivOrderType;
        @BindView(R.id.tv_order_type)
        TextView tvOrderType;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.iv_pickup_location)
        ImageView ivPickupLocation;
        @BindView(R.id.tv_label_pickup_location)
        TextView tvLabelPickupLocation;
        @BindView(R.id.tv_creator_name)
        TextView tvCreatorName;
        @BindView(R.id.tv_pickup_location)
        TextView tvPickupLocation;
        @BindView(R.id.view_dotted_line)
        View viewDottedLine;
        @BindView(R.id.iv_drop_off_location)
        ImageView ivDropOffLocation;
        @BindView(R.id.tv_label_drop_off_location)
        TextView tvLabelDropOffLocation;
        @BindView(R.id.tv_receiver_name)
        TextView tvReceiverName;
        @BindView(R.id.tv_drop_off_location)
        TextView tvDropOffLocation;
        @BindView(R.id.rl_location_info)
        RelativeLayout rlLocationInfo;

        OrderReceivedAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(v -> {
                Datum datum = (Datum) mReceivedOrdersList.get(getAdapterPosition());
                mIReceivedOrders.onRowOrderViewClick(datum.getId());
            });

            tvOrderStatus.setOnClickListener(v -> {
                switch (((Datum) mReceivedOrdersList.get(getAdapterPosition())).getDropLocation().get(0).getDriverTrackingStatus().
                        get(((Datum) mReceivedOrdersList.get(getAdapterPosition())).getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()) {
                    case AppConstants.DriverTrackingStatus.DRIVE_TO_DROP_LOCATION:
                    case AppConstants.DriverTrackingStatus.REACHED_TO_DROP_LOCATION:
                        mIReceivedOrders.onTrackOrderClicked(((Datum) mReceivedOrdersList.get(getAdapterPosition())).getId(), false);
                }
            });
        }
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;


        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

