package com.qd.user.ui.createorder.selectlocation.advancedsearch;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.utils.LocaleManager;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdvancedSearchAdapter extends RecyclerView.Adapter<AdvancedSearchAdapter.AdvancedSearchAdapterViewHolder> implements Filterable {

    private static final int DEGREES_OF_ROTATION = 180;
    private final List<String> mAddressList;
    private final String mTitle;
    private AdvanceSearchAdapterInterface mInterface;
    private List<String> mFilteredList;


    AdvancedSearchAdapter(TreeSet<String> addressList, String title, AdvanceSearchAdapterInterface advanceSearchAdapterInterface) {
        mAddressList = new ArrayList<>(addressList);
        mFilteredList = new ArrayList<>(addressList);
        mInterface = advanceSearchAdapterInterface;
        mTitle = title;
    }

    @NonNull
    @Override
    public AdvancedSearchAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_dialog, viewGroup, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new AdvancedSearchAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdvancedSearchAdapterViewHolder advancedSearchAdapterViewHolder, int i) {
        advancedSearchAdapterViewHolder.tvFavAddress.setText(mFilteredList.get(i));

        if (mFilteredList.get(i).equals(advancedSearchAdapterViewHolder.tvFavAddress.getResources().getString(R.string.s_no_data_found))) {
            advancedSearchAdapterViewHolder.tvFavAddress.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else if (LocaleManager.getLanguage(advancedSearchAdapterViewHolder.tvFavAddress.getContext()).equals("en")) {
            advancedSearchAdapterViewHolder.tvFavAddress.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_send_popup_forward_arrow, 0);
        } else if (LocaleManager.getLanguage(advancedSearchAdapterViewHolder.tvFavAddress.getContext()).equals("ar")) {
            Drawable drawable = advancedSearchAdapterViewHolder.tvFavAddress.getResources().getDrawable(R.drawable.ic_send_popup_forward_arrow);
            Bitmap iconBitmap = ((BitmapDrawable) drawable).getBitmap();
            Matrix matrix = new Matrix();
            matrix.postRotate(DEGREES_OF_ROTATION);
            Bitmap targetBitmap = Bitmap.createBitmap(iconBitmap, 0, 0, iconBitmap.getWidth(), iconBitmap.getHeight(), matrix, true);
            Drawable result = new BitmapDrawable(advancedSearchAdapterViewHolder.tvFavAddress.getResources(), targetBitmap);
            advancedSearchAdapterViewHolder.tvFavAddress.setCompoundDrawablesWithIntrinsicBounds(result, null, null, null);
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint.toString().isEmpty()) {
                    mFilteredList = mAddressList;
                } else {

                    List<String> filteredList = new ArrayList<>();
                    for (String row : mAddressList) {

                        // match condition. this might differ depending on your requirement
                        if (row.toLowerCase().contains(constraint)) {
                            filteredList.add(row);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredList = (List<String>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    class AdvancedSearchAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_fav_address)
        TextView tvFavAddress;

        AdvancedSearchAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            //itemView.invalidate();
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> mInterface.onItemSelected(mFilteredList.get(getAdapterPosition()), mTitle));
        }
    }

    public interface AdvanceSearchAdapterInterface {

        void onItemSelected(String item, String title);
    }
}
