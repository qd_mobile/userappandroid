package com.qd.user.ui.createorder.ordersummary;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.paymentmode.PaymentModeType;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentModeAdapter extends RecyclerView.Adapter<PaymentModeAdapter.PaymentModeAdapterViewHolder> {
    private ArrayList<PaymentModeType> mPaymentModeList;
    private IPaymentMode mIPaymentMode;

    public PaymentModeAdapter(ArrayList<PaymentModeType> paymentModeList, IPaymentMode iPaymentMode) {
        mPaymentModeList = paymentModeList;
        mIPaymentMode = iPaymentMode;

    }

    @NonNull
    @Override
    public PaymentModeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_payment_mode, viewGroup, false);
        return new PaymentModeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentModeAdapterViewHolder paymentModeAdapterViewHolder, int i) {
        paymentModeAdapterViewHolder.tvPaymentType.setText(mPaymentModeList.get(i).getPaymentType());
        paymentModeAdapterViewHolder.llPaymentType.setBackground(
                paymentModeAdapterViewHolder.llPaymentType.getResources().getDrawable(R.drawable.drawable_grey_round_stroke));

        if (!mPaymentModeList.get(i).isActive()) {
            paymentModeAdapterViewHolder.llPaymentType.setBackground(
                    paymentModeAdapterViewHolder.llPaymentType.getResources().getDrawable(R.drawable.drawable_grey_round_stroke));
            paymentModeAdapterViewHolder.tvPaymentType.setTextColor(
                    paymentModeAdapterViewHolder.llPaymentType.getResources().getColor(R.color.colorBrownishGrey));
            paymentModeAdapterViewHolder.ivPaymentType.setBackground(
                    paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.drawable_ci_pale_grey));

            switch (mPaymentModeList.get(i).getPaymentIcon()) {
                case 1:
                    paymentModeAdapterViewHolder.ivPaymentType.setImageDrawable(
                            paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.ic_order_summary_cash_inactive));
                    break;
                case 2:
                    paymentModeAdapterViewHolder.ivPaymentType.setImageDrawable(
                            paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.ic_order_summary_qd_wallet_inactive));
                    break;
                case 3:
                case 4:
                    paymentModeAdapterViewHolder.ivPaymentType.setImageDrawable(
                            paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.ic_order_summary_online_inactive));
                    break;
            }

        } else {
            paymentModeAdapterViewHolder.llPaymentType.setBackground(
                    paymentModeAdapterViewHolder.llPaymentType.getResources().getDrawable(R.drawable.drawable_green_stroke_less_cornered));
            paymentModeAdapterViewHolder.tvPaymentType.setTextColor(
                    paymentModeAdapterViewHolder.llPaymentType.getResources().getColor(R.color.colorGreenishTeal));
            paymentModeAdapterViewHolder.ivPaymentType.setBackground(
                    paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.drawable_transparent_green_circle));

            switch (mPaymentModeList.get(i).getPaymentIcon()) {
                case 1:
                    paymentModeAdapterViewHolder.ivPaymentType.setImageDrawable(
                            paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.ic_order_summary_cash_active));
                    break;
                case 2:
                    paymentModeAdapterViewHolder.ivPaymentType.setImageDrawable(
                            paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.ic_order_summary_qd_wallet_active));
                    break;
                case 3:
                case 4:
                    paymentModeAdapterViewHolder.ivPaymentType.setImageDrawable(
                            paymentModeAdapterViewHolder.ivPaymentType.getResources().getDrawable(R.drawable.ic_order_summary_online_active));
                    break;

            }

        }

    }


    @Override
    public int getItemCount() {
        return mPaymentModeList.size();
    }

    public interface IPaymentMode {

        void onPaymentModeSelected(PaymentModeType paymentModeType);
    }

    class PaymentModeAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_payment_type)
        ImageView ivPaymentType;
        @BindView(R.id.tv_payment_type)
        TextView tvPaymentType;
        @BindView(R.id.ll_payment_type)
        LinearLayout llPaymentType;

        PaymentModeAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                if (!mPaymentModeList.get(getAdapterPosition()).isActive()) {
                    makeAllInactive();
                    mIPaymentMode.onPaymentModeSelected(mPaymentModeList.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });
        }

        private void makeAllInactive() {
            for (int i = 0; i < mPaymentModeList.size(); i++) {
                mPaymentModeList.get(i).setActive(false);
            }
            mPaymentModeList.get(getAdapterPosition()).setActive(true);

        }

    }
}
