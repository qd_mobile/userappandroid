package com.qd.user.ui.approveReturn;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;

import org.json.JSONObject;

class ApproveReturnRepository {
    void approveOrderReturn(String elementId, final RichMediatorLiveData<OrderCancellationResponse> approveReturnLiveData) {
        DataManager.getInstance().approveOrderReturn(elementId, "VENDOR").enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse orderDetailResponse) {
                approveReturnLiveData.setLoadingState(false);
                approveReturnLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                approveReturnLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                approveReturnLiveData.setError(t);
            }
        });
    }

    public void cancelOrder(JSONObject request, final RichMediatorLiveData<OrderCancellationResponse> orderCancellationLiveData) {
        DataManager.getInstance().cancelOrder(request).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse orderDetailResponse) {
                orderCancellationLiveData.setLoadingState(false);
                orderCancellationLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderCancellationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderCancellationLiveData.setError(t);
            }
        });
    }
}
