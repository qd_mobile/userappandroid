package com.qd.user.ui.createorder.findingdriver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.Result;
import com.qd.user.socket.SocketConstants;
import com.qd.user.socket.SocketManager;

import org.json.JSONException;
import org.json.JSONObject;

class FindingDriverRepository {


    /**
     * emit for finding a driver for the order just being created whose id is passed to
     * the event {@literal SocketConstants.EVENT_ORDER_STATUS}
     *
     * @param id Order id of the order created
     */
    void emitForDriverAvailability(String id) {
        SocketManager.getInstance().sendDataOnSocket(SocketConstants.EVENT_ORDER_STATUS, createJsonObject(id));
    }

    private JSONObject createJsonObject(String id) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(SocketConstants.KEY_TYPE, SocketConstants.EVENT_ORDER_ACCEPTED_BY_DRIVER);
            jsonObject.put(SocketConstants.KEY_ORDER_ID, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    /**
     * Listens to if the order is accepted by any driver or has been assigned by the admin
     * to any driver
     *
     * @param driverAcceptanceLiveData Live data object that will contain the details of the driver assigned
     */
    void listenForDriverAcceptance(final RichMediatorLiveData<Result> driverAcceptanceLiveData) {
        SocketManager.getInstance().listenForDriverAcceptance(args -> {
            final String s = args[0].toString();
            Result result = new GsonBuilder().create().fromJson(s, Result.class);
            driverAcceptanceLiveData.postValue(result);
        });
    }


    void disconnectSocketListeners() {
        SocketManager.getInstance().disconnectSocketListeners();
    }

    void retryForDriver(String id, final RichMediatorLiveData<CommonResponse> retryLiveData) {
        DataManager.getInstance().retryForDriver(id).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                retryLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                retryLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                retryLiveData.setError(t);
            }
        });
    }

    public void cancelOrder(JSONObject requestPayloadForOrderCancellation, final RichMediatorLiveData<OrderCancellationResponse> orderCancellationLiveData) {
        DataManager.getInstance().cancelOrder(requestPayloadForOrderCancellation).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse orderDetailResponse) {
                orderCancellationLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderCancellationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderCancellationLiveData.setError(t);
            }
        });
    }

    int getWaitingTimeForRetry() {
        return DataManager.getInstance().getWaitingTimeForRetry();
    }

    void checkForOrderStatus(String id, RichMediatorLiveData<CommonResponse> currentStatusLiveData) {
        DataManager.getInstance().checkForOrderStatus(id).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                currentStatusLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                currentStatusLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                currentStatusLiveData.setError(t);
            }
        });
    }

    void autoCancelOrder(String id, RichMediatorLiveData<OrderCancellationResponse> autoCancellationLiveData) {
        DataManager.getInstance().autoCancelOrder(id).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse commonResponse) {
                autoCancellationLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                autoCancellationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                autoCancellationLiveData.setError(t);
            }
        });
    }

    public void getCancellationReasons(RichMediatorLiveData<CommonListResponse> cancellationReasonsLiveData) {
        DataManager.getInstance().getCancellationReasons().enqueue(new NetworkCallback<CommonListResponse>() {
            @Override
            public void onSuccess(CommonListResponse commonResponse) {
                cancellationReasonsLiveData.setValue(commonResponse);
                DataManager.getInstance().setCancellationReasons(new Gson().toJson(commonResponse.getResult()));
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                cancellationReasonsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                cancellationReasonsLiveData.setError(t);
            }
        });
    }
}
