package com.qd.user.ui.home.editprofile;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.delete.DeleteSavedPlaceResponse;
import com.qd.user.model.editprofile.EditProfileResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.profile.ProfileResponse;
import com.qd.user.socket.SocketManager;

import java.util.HashMap;

class EditProfileRepository {
    public String getUserName() {
        return DataManager.getInstance().getUserName();
    }

    public String getPhoneNumber() {
        return DataManager.getInstance().getPhoneNumber();
    }

    public String getEmail() {
        return DataManager.getInstance().getEmail();
    }

    public String getCountryCode() {
        return DataManager.getInstance().getCountryCode();

    }

    void hitEditProfileApi(CreateAccountInfo editUserInfo, final RichMediatorLiveData<EditProfileResponse> mEditProfileLiveData) {
        DataManager.getInstance().hitEditProfileApi(createPayLoad(editUserInfo)).enqueue(new NetworkCallback<EditProfileResponse>() {
            @Override
            public void onSuccess(EditProfileResponse editProfileResponse) {
                setUserDataInPreference(editProfileResponse);
                mEditProfileLiveData.setValue(editProfileResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mEditProfileLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mEditProfileLiveData.setError(t);
            }
        });
    }

    private void setUserDataInPreference(EditProfileResponse editProfileResponse) {
        DataManager.getInstance().setUserName(editProfileResponse.getResult().getName());
        DataManager.getInstance().setCountryCode(editProfileResponse.getResult().getContact().getCcPersonal());
        DataManager.getInstance().setPhoneNumber(editProfileResponse.getResult().getContact().getPersonal());
        DataManager.getInstance().setEmailAddress(editProfileResponse.getResult().getEmail());
    }

    private HashMap<String, String> createPayLoad(CreateAccountInfo editUserInfo) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_NAME, editUserInfo.getName());
        hashMap.put(NetworkConstants.KEY_CC_PERSONAL, editUserInfo.getCountryCode());
        hashMap.put(NetworkConstants.KEY_PERSONAL, editUserInfo.getPhoneNumber());
        hashMap.put(NetworkConstants.KEY_CASH_CLIENT_ID, DataManager.getInstance().getUserId());
        return hashMap;
    }

    void hitSavedPlacesApi(final RichMediatorLiveData<SavedAddressResponse> mSavedPlacesLiveData) {
        DataManager.getInstance().getSavedAddresses(DataManager.getInstance().getUserId(), "").enqueue(new NetworkCallback<SavedAddressResponse>() {
            @Override
            public void onSuccess(SavedAddressResponse savedAddressResponse) {
                mSavedPlacesLiveData.setValue(savedAddressResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mSavedPlacesLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mSavedPlacesLiveData.setError(t);
            }
        });
    }

    void deleteSavedAddress(String id, final RichMediatorLiveData<DeleteSavedPlaceResponse> mDeleteSavedLiveData) {
        DataManager.getInstance().hitDeleteSavedPlacesApi(id).enqueue(new NetworkCallback<DeleteSavedPlaceResponse>() {
            @Override
            public void onSuccess(DeleteSavedPlaceResponse deleteSavedPlaceResponse) {
                mDeleteSavedLiveData.setValue(deleteSavedPlaceResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mDeleteSavedLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mDeleteSavedLiveData.setError(t);
            }
        });
    }

    void clearPreference() {
        DataManager.getInstance().clearPreference();
    }

    String getSignUpType() {
        return DataManager.getInstance().getUserSignupType();
    }

    void connectSocket() {
        SocketManager.getInstance().connectSocket();
    }

    void initializeSocket() {
        SocketManager.initializeSocket();
    }

    public String getUserType() {
        return DataManager.getInstance().getUserType();
    }

    public String getBusinessName() {
        return DataManager.getInstance().getBusinessName();
    }

    public String getCompanyProfile() {
        return DataManager.getInstance().getCompanyProfile();
    }

    public String getBusinessAddress() {
        return DataManager.getInstance().getBusinessFullAddress();
    }

    String getCompanyContact() {
        return DataManager.getInstance().getCompanyContact();
    }

    void disconnectSocket() {
        SocketManager.getInstance().disconnectSocket();
    }

    void logOutUser(final RichMediatorLiveData<CommonResponse> logOutLiveData, String deviceId) {
        DataManager.getInstance().logOutUser(AppConstants.userType.CASH_CLIENT, DataManager.getInstance().getUserId(), deviceId).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                logOutLiveData.setLoadingState(false);
                logOutLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                logOutLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                logOutLiveData.setError(t);
            }
        });
    }

    void getProfileInfo(final RichMediatorLiveData<ProfileResponse> profileLiveData) {
        DataManager.getInstance().getProfileInfo().enqueue(new NetworkCallback<ProfileResponse>() {
            @Override
            public void onSuccess(ProfileResponse profileResponse) {
                profileLiveData.setValue(profileResponse);
                setDataInPreference(profileResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                profileLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                profileLiveData.setError(t);
            }
        });
    }

    private void setDataInPreference(ProfileResponse profileResponse) {
        DataManager.getInstance().setUserName(profileResponse.getResult().getVendorList().getName());
        DataManager.getInstance().setCountryCode(profileResponse.getResult().getVendorList().getContact().getCcPersonal());
        DataManager.getInstance().setPhoneNumber(profileResponse.getResult().getVendorList().getContact().getPersonal());
        DataManager.getInstance().setEmailAddress(profileResponse.getResult().getVendorList().getEmail());

        if (!profileResponse.getResult().getVendorList().getCashClientStatus().equals(AppConstants.userType.ONLY_CASH_CLIENT)) {
            DataManager.getInstance().setServiceType(profileResponse.getResult().getVendorList().getServicesOffered());
            DataManager.getInstance().setCompanyContact(profileResponse.getResult().getVendorList().getCompany().getContact().getCcPersonal() +
                    profileResponse.getResult().getVendorList().getCompany().getContact().getPersonal());
            DataManager.getInstance().setBusinessFullAddress(profileResponse.getResult().getVendorList().getBusinessAddress());
            DataManager.getInstance().setBusinessName(profileResponse.getResult().getVendorList().getBusinessName());
            DataManager.getInstance().setCompanyProfile(profileResponse.getResult().getVendorList().getCompanyProfile());
        }

        DataManager.getInstance().setUserId(profileResponse.getResult().getVendorList().getId());
        DataManager.getInstance().setUserType(profileResponse.getResult().getVendorList().getCashClientStatus());
        DataManager.getInstance().setCurrentStatus(profileResponse.getResult().getVendorList().getCurrentStatus());
        DataManager.getInstance().setVendorStatus(profileResponse.getResult().getVendorList().getStatus());
        DataManager.getInstance().setCashClientCurrentStatus(profileResponse.getResult().getVendorList().getCashClientCurrentStatus());
        DataManager.getInstance().setSignUpType(profileResponse.getResult().getVendorList().getSignupType());

    }
}
