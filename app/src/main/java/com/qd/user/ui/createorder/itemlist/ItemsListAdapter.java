package com.qd.user.ui.createorder.itemlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.qd.user.R;
import com.qd.user.model.createorder.additem.ItemDetails;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.ItemsListAdapterViewHolder> {

    private final ArrayList<ItemDetails> mItemsList;
    private ItemsListAdapterInterface mInterface;


    ItemsListAdapter(ArrayList<ItemDetails> itemsList, ItemsListAdapterInterface itemsListAdapterInterface) {
        mItemsList = itemsList;
        mInterface = itemsListAdapterInterface;
    }


    @NonNull
    @Override
    public ItemsListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_items_list, viewGroup, false);
        return new ItemsListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsListAdapterViewHolder itemsListAdapterViewHolder, int i) {
        itemsListAdapterViewHolder.tvItemName.setText(mItemsList.get(i).getItemType());
        itemsListAdapterViewHolder.tvLocation.setText(mItemsList.get(i).getItemDescription());

        Glide.with(itemsListAdapterViewHolder.itemView.getContext())
                .load(mItemsList.get(i).getItemImage().get(0))
                .into((itemsListAdapterViewHolder).ivItemImage);
    }

    @Override
    public int getItemCount() {
        return mItemsList.size();
    }

    private void inflatePopupMenu(Context context, ImageView options, final int adapterPosition) {
        final PopupMenu popupMenu = new PopupMenu(context, options);
        popupMenu.inflate(R.menu.menu_item_options);

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_edit:
                    mInterface.onEditItemClicked(adapterPosition);
                    break;
                case R.id.menu_delete:
                    mItemsList.remove(adapterPosition);
                    notifyItemRemoved(adapterPosition);
                    break;
            }
            return false;
        });

        popupMenu.show();
    }

    public interface ItemsListAdapterInterface {

        void onEditItemClicked(int adapterPosition);

    }

    class ItemsListAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.cv_item_image)
        CardView cvItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.tv_location)
        TextView tvLocation;
        @BindView(R.id.iv_item_options)
        ImageView ivItemOptions;
        @BindView(R.id.view_item_list)
        View viewItemList;

        ItemsListAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ivItemOptions.setOnClickListener(v -> inflatePopupMenu(ivItemOptions.getContext(), ivItemOptions, getAdapterPosition()));
        }
    }
}
