package com.qd.user.ui.createorder.selectlocation.savedaddress;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.createorder.savedaddress.Result;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SavedAddressAdapter extends RecyclerView.Adapter<SavedAddressAdapter.SavedAddressAdapterViewHolder> {

    private ArrayList<Result> mFavAddressList;
    private SavedAddressAdapterInterface mInterface;

    SavedAddressAdapter(ArrayList<Result> favAddressList, SavedAddressAdapterInterface savedAddressAdapterInterface) {
        mFavAddressList = favAddressList;
        mInterface = savedAddressAdapterInterface;
    }

    @NonNull
    @Override
    public SavedAddressAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_fav_address, viewGroup, false);
        return new SavedAddressAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SavedAddressAdapterViewHolder savedAddressAdapterViewHolder, int i) {
        savedAddressAdapterViewHolder.tvTitleFavAddress.setText(mFavAddressList.get(i).getSavedLocation().getName());
        savedAddressAdapterViewHolder.tvFavAddress.setText(mFavAddressList.get(i).getSavedLocation().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
    }

    @Override
    public int getItemCount() {
        return mFavAddressList.size();
    }

    interface SavedAddressAdapterInterface {
        void onSavedAddressClicked(Result result);
    }

    class SavedAddressAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title_fav_address)
        TextView tvTitleFavAddress;
        @BindView(R.id.tv_fav_address)
        TextView tvFavAddress;

        SavedAddressAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> mInterface.onSavedAddressClicked(mFavAddressList.get(getAdapterPosition())));
        }
    }
}
