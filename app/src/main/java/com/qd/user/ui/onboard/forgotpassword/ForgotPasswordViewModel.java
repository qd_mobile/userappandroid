package com.qd.user.ui.onboard.forgotpassword;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;
import com.qd.user.ui.onboard.login.LoginFragment;

public class ForgotPasswordViewModel extends ViewModel {
    private RichMediatorLiveData<CommonResponse> mForgotasswordLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private ForgotPasswordRepository mRepo = new ForgotPasswordRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver,Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver=loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if(mForgotasswordLiveData ==null){
            mForgotasswordLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if(mValidateLiveData == null){
                mValidateLiveData = new MutableLiveData<>();
            }
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the login live data object to {@link LoginFragment}
     *
     * @return {@link #mForgotasswordLiveData}
     */
    public RichMediatorLiveData<CommonResponse> getForgotPasswordLiveData() {
        return mForgotasswordLiveData;
    }


    public void onSendLinkViewClicked(CreateAccountInfo forgotPasswordData, boolean isEmailAddressSelected) {
        if (validateInputFields(forgotPasswordData,isEmailAddressSelected)) {
         mRepo.hitForgotPasswordApi(mForgotasswordLiveData,forgotPasswordData,isEmailAddressSelected);
        }
    }

    private boolean validateInputFields(CreateAccountInfo forgotPasswordData, boolean isEmailAddressSelected) {
        if (isEmailAddressSelected) {
            if (!Patterns.EMAIL_ADDRESS.matcher(forgotPasswordData.getEmail()).matches()) {
                mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, "Invalid email address"));
                return false;
            }
        }else {
            if (!forgotPasswordData.getPhoneNumber().matches("[0-9]{7,9}$")) {
                mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER, "Please enter a valid phone number"));
                return false;
            }
        }
        return true;
    }
}
