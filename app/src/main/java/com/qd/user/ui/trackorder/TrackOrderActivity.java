package com.qd.user.ui.trackorder;

import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.qd.user.QuickDelivery;
import com.qd.user.R;
import com.qd.user.base.BaseActivity;
import com.qd.user.constants.AppConstants;
import com.qd.user.ui.trackorder.businessorder.BusinessOrderDetailsFragment;
import com.qd.user.ui.trackorder.orderdetail.OrderDetailFragment;
import com.qd.user.ui.trackorder.orderstatus.OrderStatusFragment;
import com.qd.user.ui.trackorder.tracking.OrderTrackingFragment;

public class TrackOrderActivity extends BaseActivity implements OrderDetailFragment.IOrderDetailHost, OrderTrackingFragment.IOrderTrackingHost,
        OrderStatusFragment.IOrderStatusHost, BusinessOrderDetailsFragment.IBusinessOrderDetailHost {
    private String mCurrentOrderStatus;
    private String mOrderId;

    @Override
    protected void initViewsAndVariables() {

    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_on_board;
    }

    /*@Override
    public void navigateToSelectAddressFragment() {
        addFragmentWithBackStack(R.id.cl_root_view, SelectLocationFragment.getInstance(true, null, false), SelectLocationFragment.class.getName());
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntentData();
    }

    private void getIntentData() {
        switch (getIntent().getStringExtra(AppConstants.ACTION_NAVIGATION)) {
            case AppConstants.Navigation.TRACK_ORDER:
                openTrackOrderFragment(getIntent().getStringExtra(AppConstants.ORDER_ID), getIntent().getBooleanExtra(AppConstants.IS_ORDER_RECEIVED, false));
                break;
            case AppConstants.Navigation.ORDER_DETAIL:
                openOrderDetailsFragment(getIntent().getStringExtra(AppConstants.ORDER_ID), getIntent().getBooleanExtra(AppConstants.IS_ORDER_RECEIVED, false));
                break;
            case AppConstants.Navigation.BUSINESS_ORDER_DETAIL:
                openBusinessOrderDetailsFragment(getIntent().getStringExtra(AppConstants.ORDER_ID));
        }
    }

    private void openBusinessOrderDetailsFragment(String orderId) {
        addFragment(R.id.cl_root_view, BusinessOrderDetailsFragment.getInstance(orderId), BusinessOrderDetailsFragment.class.getName());
    }

    @Override
    public void openOrderStatusFragment(String orderUniqueId) {
        addFragmentWithBackStack(R.id.cl_root_view, OrderStatusFragment.getInstance(orderUniqueId), OrderStatusFragment.class.getName());
    }

    private void openOrderDetailsFragment(String orderId, boolean isOrderReceived) {
        addFragment(R.id.cl_root_view, OrderDetailFragment.getInstance(orderId, isOrderReceived), OrderDetailFragment.class.getName());
    }

    @Override
    public void openTrackOrderFragment(String orderId, boolean isFromReceived) {
        addFragment(R.id.cl_root_view, OrderTrackingFragment.getInstance(orderId, isFromReceived), OrderTrackingFragment.class.getName());
    }

    @Override
    public void refreshOrders(String id) {
        finishAfterTransition();
    }

    @Override
    public void backToOrderDetailsFragment(String orderId) {
        exitFullScreenMode();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(OrderDetailFragment.class.getName());
        if (fragment != null) {
            while (getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof OrderDetailFragment) {
                getSupportFragmentManager().popBackStackImmediate();
            }

            if (getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof OrderDetailFragment) {
                ((OrderDetailFragment) fragment).updateOrder(orderId);
            }

        } else {
            openOrderDetailsFragment(orderId, getIntent().getBooleanExtra(AppConstants.IS_ORDER_RECEIVED, false));
        }
    }

    @Override
    public void enterFullScreen() {
        super.enterFullScreenMode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuickDelivery.getInstance().setCurrentActivity(this);
    }

    private void clearReferences() {
        Activity currActivity = QuickDelivery.getInstance().getCurrentActivity();
        if (this.equals(currActivity))
            QuickDelivery.getInstance().setCurrentActivity(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        clearReferences();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearReferences();
    }

    @Override
    public void setCurrentOrderStatus(String orderId, String orderStatus) {
        mOrderId = orderId;
        mCurrentOrderStatus = orderStatus;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof OrderTrackingFragment) {
            if (mCurrentOrderStatus != null && !(mCurrentOrderStatus.equals(AppConstants.OrderStatus.DELIVERED) ||
                    mCurrentOrderStatus.equals(AppConstants.OrderStatus.CANCELLED) ||
                    mCurrentOrderStatus.equals(AppConstants.OrderStatus.RETURNED) ||
                    mCurrentOrderStatus.equals(AppConstants.OrderStatus.CASHBACK_RETURNED))) {
                ((OrderTrackingFragment) fragment).updateOrderStatus(mOrderId);
            } else if (mOrderId != null) {
                backToOrderDetailsFragment(mOrderId);
            }
        }
    }
}
