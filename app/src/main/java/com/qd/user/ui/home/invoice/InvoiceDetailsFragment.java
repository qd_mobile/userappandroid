package com.qd.user.ui.home.invoice;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.invoicedetails.OrderObject;
import com.qd.user.model.payments.invoicedetails.Result;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.PaginationListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class InvoiceDetailsFragment extends BaseFragment implements PaginationListener.PaginationListenerCallbacks {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.ll_invoice)
    LinearLayout llInvoice;
    @BindView(R.id.tv_label_total_number_of_orders)
    TextView tvLabelTotalNumberOfOrders;
    @BindView(R.id.tv_driver_id)
    TextView tvTotalNoOfOrders;
    @BindView(R.id.tv_label_bill_amount)
    TextView tvLabelBillAmount;
    @BindView(R.id.tv_bill_amount)
    TextView tvBillAmount;
    @BindView(R.id.tv_label_date_generated)
    TextView tvLabelDateGenerated;
    @BindView(R.id.tv_date_generated)
    TextView tvDateGenerated;
    @BindView(R.id.tv_label_due_date)
    TextView tvLabelDueDate;
    @BindView(R.id.tv_due_date)
    TextView tvDueDate;
    @BindView(R.id.tv_label_bill_duration)
    TextView tvLabelBillDuration;
    @BindView(R.id.tv_bill_duration)
    TextView tvBillDuration;
    @BindView(R.id.cl_total_order_details)
    ConstraintLayout clTotalOrderDetails;
    @BindView(R.id.rv_order_details)
    RecyclerView rvOrderDetails;
    @BindView(R.id.btn_pay_now)
    Button btnPayNow;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;

    private Unbinder unbinder;
    private ArrayList<Object> mOrdersList;
    private String mInvoiceId;
    private int mNextPage;
    private InvoiceOrdersAdapter mAdapter;
    private PaginationListener mPaginationListener;
    private boolean isRefreshing;
    /**
     * A {@link IInvoiceDetailsHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IInvoiceDetailsHost mInvoiceDetailsHost;
    private InvoiceDetailsViewModel mInvoiceDetailsViewModel;
    private BigDecimal mAmount;

    public InvoiceDetailsFragment() {
        // Required empty public constructor
    }

    public static InvoiceDetailsFragment getInstance(String id) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.INVOICE_ID, id);
        InvoiceDetailsFragment invoiceDetailsFragment = new InvoiceDetailsFragment();
        invoiceDetailsFragment.setArguments(bundle);
        return invoiceDetailsFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IInvoiceDetailsHost) {
            mInvoiceDetailsHost = (IInvoiceDetailsHost) context;
        } else
            throw new IllegalStateException("host must implement IInvoiceDetailsHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invoice, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mInvoiceDetailsViewModel = new ViewModelProvider(this).get(InvoiceDetailsViewModel.class);
        mInvoiceDetailsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        showProgressDialog();
        mInvoiceDetailsViewModel.getInvoiceDetails(mInvoiceId, mNextPage);
        setUpAdapter();
        observeLiveData();
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_invoice_details);
        ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_invoice_details_download));
        ivNotification.setVisibility(View.GONE);
        clTotalOrderDetails.setVisibility(View.GONE);


        mOrdersList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);

        getArgumentsData();

    }

    private void observeLiveData() {
        mInvoiceDetailsViewModel.getInvoiceDetailsLiveData().observe(getViewLifecycleOwner(), invoiceDetailsResponse -> {
            hideProgressDialog();
            if (invoiceDetailsResponse != null) {
                if (invoiceDetailsResponse.getResult().get(0).getOrderObjects().get(0).getMetadata().get(0).getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (invoiceDetailsResponse.getResult().get(0).getOrderObjects().get(0).getData() != null &&
                        invoiceDetailsResponse.getResult().get(0).getOrderObjects().get(0).getData().size() > 0) {
                    clTotalOrderDetails.setVisibility(View.VISIBLE);
                    if (mOrdersList.size() > 0) {
                        mAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            // swipeRefresh.setRefreshing(false);
                            mOrdersList.clear();
                        }
                        setViews(invoiceDetailsResponse.getResult().get(0));
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            // swipeRefresh.setRefreshing(false);
                            mOrdersList.clear();
                        }

                        setViews(invoiceDetailsResponse.getResult().get(0));
                        rvOrderDetails.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }

            }
        });
    }

    private void setViews(Result result) {
        setResponseToAdapter(result.getOrderObjects().get(0));

        mAmount = result.getTotalDeliveryCharges().subtract(result.getTotalItemCost());

        if (mAmount.compareTo(BigDecimal.valueOf(0)) > 0) {
            btnPayNow.setVisibility(View.VISIBLE);
        }

        tvOrderId.setText(String.format("#%s", result.getInvoiceNumber()));
        tvTotalNoOfOrders.setText(String.valueOf(result.getTotalNoOfOrders()));
        tvBillAmount.setText(String.format(Locale.getDefault(), "%.3f KWD", result.getTotalAmount()));
        tvDateGenerated.setText(DateFormatter.getInstance().getFormattedDate(result.getCreatedAt()));
        tvDueDate.setText(DateFormatter.getInstance().getFormattedDate(result.getInvoiceDueDate()));
        tvBillDuration.setText(String.format("%s - %s", DateFormatter.getInstance().getFormattedDate(result.getInvoiceStartDate()), DateFormatter.getInstance().getFormattedDate(result.getInvoiceEndDate())));

    }

    private void setResponseToAdapter(OrderObject invoiceDetailsResponse) {
        mOrdersList.addAll(invoiceDetailsResponse.getData());
        mAdapter.notifyDataSetChanged();
    }

    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        //swipeRefresh.setRefreshing(false);
        clTotalOrderDetails.setVisibility(View.GONE);
        rvOrderDetails.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        //swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrderDetails.setVisibility(View.GONE);
        clTotalOrderDetails.setVisibility(View.GONE);
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getString(AppConstants.INVOICE_ID) != null) {
            mInvoiceId = getArguments().getString(AppConstants.INVOICE_ID);
        }
    }

    private void setUpAdapter() {
        mAdapter = new InvoiceOrdersAdapter(mOrdersList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvOrderDetails.setLayoutManager(linearLayoutManager);
        rvOrderDetails.setAdapter(mAdapter);
        rvOrderDetails.scheduleLayoutAnimation();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.iv_back, R.id.btn_pay_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mInvoiceDetailsHost.onBackPressed();
                break;
            case R.id.btn_pay_now:
                mInvoiceDetailsHost.openOnlinePaymentsFragment(createRequestForInitializingPayment());
        }
    }

    private InitializePaymentRequest createRequestForInitializingPayment() {
        InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
        initializePaymentRequest.setAmount(mAmount);
        initializePaymentRequest.setTransactionFor(AppConstants.TransactionFor.INVOICE);
        initializePaymentRequest.setInvoiceId(mInvoiceId);

        return initializePaymentRequest;
    }

    /**
     * Hits a network call for data on next page, if present
     */
    @Override
    public void loadMoreItems() {
        mAdapter.showProgress();
        mInvoiceDetailsViewModel.getInvoiceDetails(mInvoiceId, mNextPage);
    }


    public interface IInvoiceDetailsHost {

        void onBackPressed();

        void openOnlinePaymentsFragment(InitializePaymentRequest requestForInitializingPayment);
    }
}
