package com.qd.user.ui.createorder.selectlocation;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.model.failureresponse.FailureResponse;

class SelectLocationViewModel extends ViewModel {
    private SelectLocationRepo mRepo = new SelectLocationRepo();
    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {

    }

    public String getUserType() {
        return mRepo.getUserType();
    }

    void getTokenForKuwaitFinder() {

    }
}
