package com.qd.user.ui.onboard.createaccount;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.socialresponse.FbLoginResponse;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CreateAccountFragment extends BaseFragment implements TextWatcher {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_subtittle)
    TextView tvSubtitle;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.tv_first_name)
    TextInputLayout tvFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.tv_last_name)
    TextInputLayout tvLastName;
    @BindView(R.id.view_first_name)
    View viewFirstName;
    @BindView(R.id.view_last_name)
    View viewLastName;
    @BindView(R.id.tv_error_firstname)
    TextView tvErrorFirstName;
    @BindView(R.id.tv_error_lastname)
    TextView tvErrorLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.tv_email)
    TextInputLayout tvEmail;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tv_password)
    TextInputLayout tvPassword;
    @BindView(R.id.tv_password_visibility)
    TextView tvPasswordVisibility;
    @BindView(R.id.view_password)
    View viewPassword;
    @BindView(R.id.tv_error_password)
    TextView tvErrorPassword;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.iv_terms_conditions)
    ImageView ivTermsConditions;
    private Unbinder unbinder;
    @BindView(R.id.et_country_code)
    EditText etCountryCode;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.tv_phone_number)
    TextInputLayout tvPhoneNumber;
    @BindView(R.id.view_country_code)
    View viewCountryCode;
    @BindView(R.id.view_phone_number)
    View viewPhoneNumber;
    @BindView(R.id.tv_error_phone_number)
    TextView tvErrorPhoneNumber;
    @BindView(R.id.tv_terms_And_condition)
    TextView tvTermsAndCondition;
    @BindView(R.id.btn_create_account)
    Button btnCreateAccount;
    @BindView(R.id.tv_error_confirm_password)
    TextView tvErrorConfirmPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.til_confirm_password)
    TextInputLayout tilConfirmNewPassword;
    @BindView(R.id.tv_confirm_password_visibility)
    TextView tvConfirmPasswordVisibility;
    @BindView(R.id.view_confirm_password)
    View viewConfirmPassword;
    private ICreateAccountHost mHost;
    private CreateAccountViewModel mCreateAccountViewModel;
    private boolean isForSocialLogin;
    private boolean isConfirmNewPasswordVisible;
    private boolean isNewPasswordVisible;

    private FbLoginResponse mFbLoginResponse;
    private GoogleSignInAccount mGoogleSignInAccount;
    private CreateOrderRequest mCreateOrderDetail;
    private boolean isTCAgreed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_account, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCreateAccountViewModel = new ViewModelProvider(this).get(CreateAccountViewModel.class);
        mCreateAccountViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setListeners();
        getArgumentsData();
    }

    public static CreateAccountFragment getInstance(Parcelable fbLoginResponse, Parcelable googleSignInAccount, Parcelable mCreateOrderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.FB_LOGIN_RESPONSE, fbLoginResponse);
        bundle.putParcelable(AppConstants.GOOGLE_RESPONSE, googleSignInAccount);
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, mCreateOrderDetails);
        CreateAccountFragment createAccountFragment = new CreateAccountFragment();
        createAccountFragment.setArguments(bundle);
        return createAccountFragment;
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.CREATE_ORDER_DETAILS) && getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS) != null) {
            mCreateOrderDetail = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
        }
        if (getArguments() != null && getArguments().containsKey(AppConstants.FB_LOGIN_RESPONSE) && getArguments().getParcelable(AppConstants.FB_LOGIN_RESPONSE) != null) {
            mFbLoginResponse = getArguments().getParcelable(AppConstants.FB_LOGIN_RESPONSE);
            setViewsForSocialSignUp();
            isForSocialLogin = true;
        } else if (getArguments() != null && getArguments().containsKey(AppConstants.GOOGLE_RESPONSE) && getArguments().getParcelable(AppConstants.GOOGLE_RESPONSE) != null) {
            mGoogleSignInAccount = getArguments().getParcelable(AppConstants.GOOGLE_RESPONSE);
            setViewsForSocialSignUp();
            isForSocialLogin = true;
        }

    }


    /**
     * show available user data on the fields of create account screen
     */
    private void setViewsForSocialSignUp() {
        tvPassword.setVisibility(View.GONE);
        tilConfirmNewPassword.setVisibility(View.GONE);
        if (mFbLoginResponse != null) {
            if (mFbLoginResponse.getEmailId() != null) {
                etEmail.setText(mFbLoginResponse.getEmailId());
                etEmail.setEnabled(false);
            }
            if (mFbLoginResponse.getPhoneNumber() != null) {
                etPhoneNumber.setText(mFbLoginResponse.getPhoneNumber());
                etPhoneNumber.setEnabled(false);
            }

            if (mFbLoginResponse.getFirstName() != null) {
                etFirstName.setText(mFbLoginResponse.getFirstName());
                etFirstName.setEnabled(false);
            }
            if (mFbLoginResponse.getLastName() != null) {
                etLastName.setText(mFbLoginResponse.getLastName());
                etLastName.setEnabled(false);
            }

        } else if (mGoogleSignInAccount != null) {
            if (mGoogleSignInAccount.getEmail() != null) {
                etEmail.setText(mGoogleSignInAccount.getEmail());
                etEmail.setEnabled(false);
            }

            if (mGoogleSignInAccount.getGivenName() != null) {
                etFirstName.setText(mGoogleSignInAccount.getGivenName());
                etFirstName.setEnabled(false);
            }
            if (mGoogleSignInAccount.getFamilyName() != null) {
                etLastName.setText(mGoogleSignInAccount.getFamilyName());
                etLastName.setEnabled(false);
            }

        }

    }

    /**
     * Method to observe all live data objects set on the view
     */
    private void observeLiveData() {

        //observing validation live data
        mCreateAccountViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_FIRST_NAME:
                        showFirstNameValidationError(validationErrors.getValidationMessage());
                        focusFirstNameInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_LAST_NAME:
                        showLastNameValidationError(validationErrors.getValidationMessage());
                        focusLastNameInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_EMAIL:
                        showEmailValidationError(validationErrors.getValidationMessage());
                        focusEmailInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PASSWORD:
                        showPasswordValidationError(validationErrors.getValidationMessage());
                        focusPasswordInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_CONFIRM_PASSWORD:
                        showConfirmPasswordValidationError(validationErrors.getValidationMessage());
                        focusConfirmPasswordInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER:
                        showPhoneNumberValidationError(validationErrors.getValidationMessage());
                        focusPhoneNumberInputField();
                        break;
                }
            }
        });

        mCreateAccountViewModel.getSignUpLiveData().observe(getViewLifecycleOwner(), createAccountResponse -> {
            if (createAccountResponse != null) {
                if (mCreateOrderDetail != null) {
                    mHost.backToOrderSummary();
                } else {
                    if (createAccountResponse.getResult().getUserDetails().isOtpVerified()) {
                        mCreateAccountViewModel.setIsUserVerified(true);
                        steerToHomeOnSignUpSuccess();
                    } else {
                        if (!isForSocialLogin)
                            openOtpSentDialog();
                    }
                }

            }

        });
        mCreateAccountViewModel.getSocialSignUpLiveData().observe(getViewLifecycleOwner(), socialSignUpResponse -> {
            if (socialSignUpResponse != null) {
                hideProgressDialog();
                if (mCreateOrderDetail != null) {
                    mHost.backToOrderSummary();
                } else {
                    if (socialSignUpResponse.getResult().getUserDetails().isOtpVerified()) {
                        mCreateAccountViewModel.setIsUserVerified(true);
                        steerToHomeOnSignUpSuccess();
                    } else {
                        if (isForSocialLogin)
                            openOtpSentDialog();

                    }
                }
            }

        });
    }

    private void openOtpSentDialog() {
        if (getActivity() != null) {
            final Dialog otpSentDialog = new Dialog(getActivity(), R.style.customDialog);
            otpSentDialog.setContentView(R.layout.dialog_forgot_password);
            AppUtils.getInstance().dimDialogBackground(otpSentDialog);
            Button btnOk = otpSentDialog.findViewById(R.id.btn_ok);
            ImageView ivCodeSend = otpSentDialog.findViewById(R.id.iv_resent_link);
            TextView tvMessage = otpSentDialog.findViewById(R.id.tv_message);
            tvMessage.setText(R.string.s_verification_code_send_to_your_phone_number);
            ivCodeSend.setImageDrawable(getResources().getDrawable(R.drawable.ic_login_code_sent));
            otpSentDialog.show();

            btnOk.setOnClickListener(v -> {
                otpSentDialog.dismiss();
                mHost.openVerifyOtpScreen(null);
            });
        }

    }

    private void focusPhoneNumberInputField() {
        etPhoneNumber.setSelection(etPhoneNumber.getText().toString().length());
        etPhoneNumber.requestFocus();
    }

    private void showPhoneNumberValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorPhoneNumber);
        tvErrorPhoneNumber.setText(errorMessage);
        tvErrorPhoneNumber.setVisibility(View.VISIBLE);
    }

    /**
     * move to home after user successfully login via social login or create a new account
     */
    private void steerToHomeOnSignUpSuccess() {
        hideProgressDialog();
        mHost.openHomeScreen();
    }

    //show error message of mismatch or empty confirm  password
    private void showConfirmPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorConfirmPassword);
        tvErrorConfirmPassword.setText(errorMessage);
        tvErrorConfirmPassword.setVisibility(View.VISIBLE);
    }

    //focus confirm password field
    private void focusConfirmPasswordInputField() {
        etConfirmPassword.setSelection(etConfirmPassword.getText().toString().length());
        etConfirmPassword.requestFocus();
    }

    //focus last name field
    private void focusLastNameInputField() {
        etLastName.setSelection(etLastName.getText().toString().length());
        etLastName.requestFocus();
    }

    //show error message of invalid or empty last name
    private void showLastNameValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorLastName);
        tvErrorLastName.setText(errorMessage);
        tvErrorLastName.setVisibility(View.VISIBLE);
    }

    //focus first name field
    private void focusFirstNameInputField() {
        etFirstName.setSelection(etFirstName.getText().toString().length());
        etFirstName.requestFocus();
    }
    //show error message of invalid or empty first name

    private void showFirstNameValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorFirstName);
        tvErrorFirstName.setText(errorMessage);
        tvErrorFirstName.setVisibility(View.VISIBLE);
    }

    //show error message of invalid or empty email address
    private void showEmailValidationError(int message) {
        AppUtils.getInstance().shakeLayout(tvErrorEmail);
        tvErrorEmail.setText(message);
        tvErrorEmail.setVisibility(View.VISIBLE);
    }

    //focus email address field
    private void focusEmailInputField() {
        etEmail.setSelection(etEmail.getText().toString().length());
        etEmail.requestFocus();
    }

    //show error message for invalid and empty password
    private void showPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorPassword);
        tvErrorPassword.setText(errorMessage);
        tvErrorPassword.setVisibility(View.VISIBLE);
    }

    //focus password field
    private void focusPasswordInputField() {
        etPassword.setSelection(etPassword.getText().toString().length());
        etPassword.requestFocus();
    }


    private void setListeners() {
        etEmail.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
        etFirstName.addTextChangedListener(this);
        etConfirmPassword.addTextChangedListener(this);
        etLastName.addTextChangedListener(this);
        etPhoneNumber.addTextChangedListener(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_create_account, R.id.tv_password_visibility, R.id.tv_confirm_password_visibility, R.id.iv_terms_conditions})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_create_account:
                hideErrorFields();
                showProgressDialog();
                mCreateAccountViewModel.onCreateAccountViewClicked(getCreateAccountData(), isForSocialLogin);
                break;
            case R.id.tv_confirm_password_visibility:
                if (isConfirmNewPasswordVisible) {
                    tvConfirmPasswordVisibility.setText(R.string.s_hide);
                    etConfirmPassword.setTransformationMethod(null);
                    etConfirmPassword.setSelection(etConfirmPassword.getText().length());
                    isConfirmNewPasswordVisible = false;
                } else {
                    tvConfirmPasswordVisibility.setText(R.string.s_show);
                    etConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etConfirmPassword.setSelection(etConfirmPassword.getText().length());
                    isConfirmNewPasswordVisible = true;
                }
                break;
            case R.id.iv_terms_conditions:
                if (isTCAgreed) {
                    ivTermsConditions.setImageResource(R.drawable.ic_login_tick_inactive);
                } else ivTermsConditions.setImageResource(R.drawable.ic_login_tick_active);
                isTCAgreed = !isTCAgreed;
                checkForEnablingButton();
                break;
            case R.id.tv_password_visibility:
                if (isNewPasswordVisible) {
                    tvPasswordVisibility.setText(R.string.s_hide);
                    etPassword.setTransformationMethod(null);
                    etPassword.setSelection(etPassword.getText().length());
                    isNewPasswordVisible = false;
                } else {
                    tvPasswordVisibility.setText(R.string.s_show);
                    etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etPassword.setSelection(etPassword.getText().length());
                    isNewPasswordVisible = true;
                }
                break;

        }
    }

    /**
     * @return createAccountInfo which contains all fields info
     */
    private CreateAccountInfo getCreateAccountData() {

        CreateAccountInfo createAccountInfo = new CreateAccountInfo();
        if (isForSocialLogin) {
            if (mFbLoginResponse != null) {
                createAccountInfo.setFirstName(mFbLoginResponse.getFirstName());
                createAccountInfo.setLastName(mFbLoginResponse.getLastName());
                createAccountInfo.setEmail(etEmail.getText().toString());
                createAccountInfo.setPhoneNumber(etPhoneNumber.getText().toString());
                createAccountInfo.setDeviceToken(mFbLoginResponse.getToken());
                createAccountInfo.setAppId(mFbLoginResponse.getFbId());
                createAccountInfo.setPlatform(AppConstants.FACEBOOK);
            }
            if (mGoogleSignInAccount != null) {
                createAccountInfo.setFirstName(etFirstName.getText().toString().trim());
                createAccountInfo.setLastName(etLastName.getText().toString().trim());
                createAccountInfo.setEmail(etEmail.getText().toString().trim());
                createAccountInfo.setPhoneNumber(etPhoneNumber.getText().toString());
                createAccountInfo.setDeviceToken("");
                createAccountInfo.setAppId(mGoogleSignInAccount.getId());
                createAccountInfo.setPlatform(AppConstants.GMAIL);
            }
        } else {
            createAccountInfo.setFirstName(etFirstName.getText().toString().trim());
            createAccountInfo.setLastName(etLastName.getText().toString().trim());
            createAccountInfo.setEmail(etEmail.getText().toString().trim().toLowerCase());
            createAccountInfo.setPassword(etPassword.getText().toString().trim());
            createAccountInfo.setCountryCode(etCountryCode.getText().toString().trim());
            createAccountInfo.setConfirmPassword(etConfirmPassword.getText().toString().trim());
            createAccountInfo.setPhoneNumber(etPhoneNumber.getText().toString().trim());
        }
        createAccountInfo.setDeviceId(getDeviceId());
        createAccountInfo.setCountryCode(etCountryCode.getText().toString());

        return createAccountInfo;

    }

    private void hideErrorFields() {
        tvErrorEmail.setVisibility(View.GONE);
        tvErrorFirstName.setVisibility(View.GONE);
        tvErrorLastName.setVisibility(View.GONE);
        tvErrorPassword.setVisibility(View.GONE);
        tvErrorConfirmPassword.setVisibility(View.GONE);
        tvErrorPhoneNumber.setVisibility(View.GONE);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ICreateAccountHost) {

            mHost = (ICreateAccountHost) context;
        } else
            throw new IllegalStateException("Host must implement ICreateAccountHost");
    }

    @Override
    protected void initViewsAndVariables() {
        super.initViewsAndVariables();
        etCountryCode.setEnabled(false);
        tvTittle.setText(R.string.s_create_account);
        btnCreateAccount.setEnabled(false);
        setSpannableStringForConditionsAndPrivacyPolicy();
    }

    private void setSpannableStringForConditionsAndPrivacyPolicy() {
        SpannableString termsNConditions = new SpannableString(getString(R.string.s_terms_conditions));
        SpannableString privacyPolicy = new SpannableString(getString(R.string.s_privacy_policy));

        termsNConditions.setSpan(new StyleSpan(Typeface.BOLD), 0, getString(R.string.s_terms_conditions).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        privacyPolicy.setSpan(new StyleSpan(Typeface.BOLD), 0, getString(R.string.s_privacy_policy).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);


        ClickableSpan privacyPolicyClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.setBackground(null);
                mHost.showStaticPage(AppConstants.StaticPages.PRIVACY_POLICY);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };

        ClickableSpan termsAndConditionClickableSan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.setBackground(null);
                mHost.showStaticPage(AppConstants.StaticPages.TERMS_AND_CONDITIONS);


            }


            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };
        termsNConditions.setSpan(new ForegroundColorSpan(ContextCompat.getColor(tvTermsAndCondition.getContext(), R.color.colorGreenishTeal)), 0, termsNConditions.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        termsNConditions.setSpan(termsAndConditionClickableSan, 0, termsNConditions.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        privacyPolicy.setSpan(new ForegroundColorSpan(ContextCompat.getColor(tvTermsAndCondition.getContext(), R.color.colorGreenishTeal)), 0, privacyPolicy.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        privacyPolicy.setSpan(privacyPolicyClickableSpan, 0, privacyPolicy.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        tvTermsAndCondition.setText(TextUtils.concat(getString(R.string.s_by_signing_up_you_agree_to_our_terms_amp_conditions_and_privacy_policy), " ",
                termsNConditions, " and ", privacyPolicy));
        tvTermsAndCondition.setHighlightColor(Color.TRANSPARENT);
        tvTermsAndCondition.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        checkForEnablingButton();
        if (etPassword.getText().toString().trim().length() > 0) {
            tvPasswordVisibility.setVisibility(View.VISIBLE);
        } else tvPasswordVisibility.setVisibility(View.GONE);

        if (etConfirmPassword.getText().toString().trim().length() > 0) {
            tvConfirmPasswordVisibility.setVisibility(View.VISIBLE);
        } else tvConfirmPasswordVisibility.setVisibility(View.GONE);
    }

    private void checkForEnablingButton() {
        if (isTCAgreed && isForSocialLogin && (!etFirstName.getText().toString().isEmpty() && !etLastName.getText().toString().isEmpty() && !etEmail.getText().toString().isEmpty()
                && !etPhoneNumber.getText().toString().isEmpty())) {
            btnCreateAccount.setEnabled(true);
            btnCreateAccount.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        } else if (isTCAgreed && !etFirstName.getText().toString().isEmpty() && !etLastName.getText().toString().isEmpty() && !etEmail.getText().toString().isEmpty() && !etPassword.getText().toString().isEmpty() &&
                !etConfirmPassword.getText().toString().isEmpty() && !etPhoneNumber.getText().toString().isEmpty()) {
            btnCreateAccount.setEnabled(true);
            btnCreateAccount.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        } else {
            btnCreateAccount.setEnabled(false);
            btnCreateAccount.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
        }
    }


    public interface ICreateAccountHost {

        void openHomeScreen();

        void onBackPressed();

        void backToOrderSummary();

        void openVerifyOtpScreen(String phoneNumber);

        void showStaticPage(String string);
    }
}
