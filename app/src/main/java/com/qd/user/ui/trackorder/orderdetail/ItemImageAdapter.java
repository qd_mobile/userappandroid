package com.qd.user.ui.trackorder.orderdetail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.qd.user.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemImageAdapter extends RecyclerView.Adapter<ItemImageAdapter.ItemImageAdapterViewHolder> {
    private ArrayList<String> mItemsList;

    public ItemImageAdapter(ArrayList<String> itemsList) {
        mItemsList = itemsList;
    }

    @NonNull
    @Override
    public ItemImageAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_image, viewGroup, false);
        return new ItemImageAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemImageAdapterViewHolder itemImageAdapterViewHolder, int i) {
        itemImageAdapterViewHolder.ivCancel.setVisibility(View.GONE);
        if (mItemsList.get(i) != null)
            Glide.with(itemImageAdapterViewHolder.ivItemImage)
                    .load(mItemsList.get(i))
                    .into(itemImageAdapterViewHolder.ivItemImage);
    }


    @Override
    public int getItemCount() {
        return mItemsList.size();
    }

    class ItemImageAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.iv_cancel)
        ImageView ivCancel;

        ItemImageAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
