package com.qd.user.ui.home.dashboard;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;
import com.qd.user.model.vendor.vendorstatus.VendorStatusResponse;
import com.qd.user.ui.home.changepassword.ChangePasswordFragment;

public class DashboardViewModel extends ViewModel {
    private RichMediatorLiveData<VendorStatusResponse> mVendorStatusLiveData;
    private RichMediatorLiveData<VendorInfoResponse> mVendorInfoResponseLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private DashboardRepository mRepo = new DashboardRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mVendorStatusLiveData == null) {
            mVendorStatusLiveData = new RichMediatorLiveData<VendorStatusResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mVendorInfoResponseLiveData == null) {
            mVendorInfoResponseLiveData = new RichMediatorLiveData<VendorInfoResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }

    /**
     * This method gives the login live data object to {@link ChangePasswordFragment}
     *
     * @return {@link #mVendorStatusLiveData}
     */
    RichMediatorLiveData<VendorStatusResponse> getVendorStatusLiveData() {
        return mVendorStatusLiveData;
    }

    /**
     * This method gives the vendor info live data object to {@link DashBoardFragment}
     *
     * @return {@link #mVendorInfoResponseLiveData}
     */
    RichMediatorLiveData<VendorInfoResponse> getVendorInfoResponseLiveData() {
        return mVendorInfoResponseLiveData;
    }

    public String getUserType() {
        return DataManager.getInstance().getUserType();
    }

    public String getCurrentStatus() {
        return DataManager.getInstance().getCurrentStatus();
    }

    void getStatusOfVendor(boolean loadingState, String deviceId, String platform) {
        if (loadingState)
        mVendorStatusLiveData.setLoadingState(true);
        mRepo.hitVendorStatusApi(mVendorStatusLiveData, deviceId, platform);
    }

    public String getVendorStatus() {
        return mRepo.getVendorStatus();
    }

    public String getCashClientCurrentStatus() {
        return mRepo.getCashClientCurrentStatus();
    }

    void getVendorInfo() {
        mVendorInfoResponseLiveData.setLoadingState(true);
        mRepo.getVendorInfo(mVendorInfoResponseLiveData);
    }
}
