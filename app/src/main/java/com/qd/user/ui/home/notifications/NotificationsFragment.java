package com.qd.user.ui.home.notifications;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.notifications.Datum;
import com.qd.user.utils.PaginationListener;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class NotificationsFragment extends BaseFragment implements PaginationListener.PaginationListenerCallbacks, NotificationsAdapter.INotificationInterface {

    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;
    @BindView(R.id.rv_notification_listing)
    RecyclerView rvNotificationListing;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_notification)
    TextView tvNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;


    private NotificationsAdapter mAdapter;
    private ArrayList<Object> mNotificationsList;
    private INotificationsHost mHost;
    private NotificationsViewModel mNotificationsViewModel;
    private int mNextPage;
    private PaginationListener mPaginationListener;
    private boolean isRefreshing;
    private LinearLayoutManager layoutManager;
    private Unbinder unbinder;
    private Datum mNotificationData;

    public static NotificationsFragment getInstance() {
        return new NotificationsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNotificationsViewModel = new ViewModelProvider(this).get(NotificationsViewModel.class);
        mNotificationsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        showShimmerEffect();
        mNotificationsViewModel.getNotificationsList(mNextPage, getDeviceId());
        observeLiveData();
    }

    private void observeLiveData() {
        mNotificationsViewModel.getNotificationsLiveData().observe(getViewLifecycleOwner(), notificationListingResponse -> {
            hideShimmerEffect();
            hideProgressDialog();

            if (notificationListingResponse != null) {

                if (notificationListingResponse.getResult().getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (notificationListingResponse.getResult().getData() != null && notificationListingResponse.getResult().getData().size() > 0) {
                    if (mNotificationsList != null && mNotificationsList.size() > 0) {
                        mAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mNotificationsList.clear();
                        }
                        setResponseToAdapter(notificationListingResponse.getResult().getData());
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mNotificationsList.clear();
                        }

                        setResponseToAdapter(notificationListingResponse.getResult().getData());
                        rvNotificationListing.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }
            }

        });

        mNotificationsViewModel.getMarkReadLiveData().observe(getViewLifecycleOwner(), notificationResponse -> {
            if (notificationResponse != null) {
                if (mNotificationData != null) {
                    mNotificationData.setIsRead(true);
                    mHost.decreaseNotificationCount();
                    mAdapter.notifyDataSetChanged();
                    navigateToNotificationDetail();
                } else refreshList();

            }
        });
    }

    private void navigateToNotificationDetail() {
        switch (mNotificationData.getType()) {

            case AppConstants.NotificationType.ORDER_ACCEPTED_BY_DRIVER:
            case AppConstants.NotificationType.ORDER_STATUS_SENDER:
                if (mNotificationData.getOrderData().get(0).getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
                    mHost.navigateToBusinessOrderDetail(mNotificationData.getOrderId());
                } else mHost.navigateToOrderDetail(mNotificationData.getOrderId(), false);

                break;
            case AppConstants.NotificationType.ORDER_STATUS_RECEIVER:
                mHost.navigateToOrderDetail(mNotificationData.getOrderId(), true);
        }
    }

    private void setResponseToAdapter(Collection<Datum> datum) {
        tvNotification.setVisibility(View.VISIBLE);
        mNotificationsList.addAll(datum);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_notification);
        tvNotification.setVisibility(View.GONE);
        mNotificationsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);
        rvNotificationListing.addOnScrollListener(mPaginationListener);

        setRefreshLayoutListener();

        setUpAdapter();
    }

    /**
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        swipeRefresh.setOnRefreshListener(
                () -> {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    refreshList();
                    mPaginationListener.setFetchingStatus(false);

                }
        );
    }

    private void refreshList() {
        isRefreshing = true;
        mNextPage = 1;
        mNotificationsViewModel.getNotificationsList(mNextPage, getDeviceId());
    }


    private void setUpAdapter() {
        mAdapter = new NotificationsAdapter(mNotificationsList, this);
        rvNotificationListing.setLayoutManager(layoutManager);
        rvNotificationListing.setAdapter(mAdapter);
        rvNotificationListing.scheduleLayoutAnimation();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * Hides recycler view and and show no data found  message if data is not available
     */
    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        rvNotificationListing.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        hideShimmerEffect();
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        hideProgressDialog();

        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvNotificationListing.setVisibility(View.GONE);

    }

    /**
     * Hides shimmer view and set views accordingly
     */
    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerViewContainer.stopShimmer();
        shimmerViewContainer.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);

    }

    /**
     * Shows shimmer view and set views accordingly
     */
    private void showShimmerEffect() {
        swipeRefresh.setVisibility(View.GONE);
        shimmerViewContainer.setVisibility(View.VISIBLE);
        shimmerViewContainer.startShimmer();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof INotificationsHost) {
            mHost = (INotificationsHost) context;
        } else throw new IllegalStateException("Host must implement INotificationsHost");
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        mHost.onBackPressed();
    }

    @OnClick({R.id.iv_back, R.id.tv_notification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                break;
            case R.id.tv_notification:
                mNotificationData = null;
                mNotificationsViewModel.markAllNotificationAsRead();
        }
    }


    /**
     * Hits a network call for data on next page, if present
     */
    @Override
    public void loadMoreItems() {
        mAdapter.showProgress();
        mNotificationsViewModel.getNotificationsList(mNextPage, getDeviceId());
    }

    @Override
    public void onNotificationClick(Datum datum) {
        mNotificationData = datum;
        if (datum.isIsRead()) {
            navigateToNotificationDetail();
        } else {
            mNotificationsViewModel.markNotificationRead(datum.getId());
        }

    }

    public interface INotificationsHost {

        void onBackPressed();

        void navigateToBusinessOrderDetail(String orderId);

        void navigateToOrderDetail(String orderId, boolean isOrderReceived);

        void decreaseNotificationCount();
    }
}

