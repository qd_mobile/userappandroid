package com.qd.user.ui.createorder.droplocation;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.additem.ItemDetails;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.ui.createorder.itemlist.ItemsListViewModel;
import com.qd.user.ui.createorder.ordersummary.OrderSummaryFragment;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.dragdrop.SimpleItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DropLocationsListFragment extends BaseFragment implements DropLocationsListAdapter.DropLocationsListAdapterInterface, OnStartDragListener, AppUtils.IConfirmationDialog, DialogInterface.OnClickListener {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_toolbar)
    View viewToolbar;
    @BindView(R.id.rv_items_list)
    RecyclerView rvDropLocationsList;
    @BindView(R.id.fab_add_item)
    FloatingActionButton fabAddItem;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    private Unbinder unbinder;
    private DropLocationsListFragment.IDropLocationsListHost mHost;
    private DropLocationsListAdapter mAdapter;

    private ArrayList<DropOffArr> mDropLocationsList;
    private CreateOrderRequest mCreateOrderDetails;
    private String mNavigation;
    private ItemTouchHelper mItemTouchHelper;
    private int mPosition;

    public DropLocationsListFragment() {
        // Required empty public constructor
    }

    public static DropLocationsListFragment getInstance(Parcelable createOrderDetails, String navigation) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, createOrderDetails);
        bundle.putString(AppConstants.ACTION_NAVIGATION, navigation);
        DropLocationsListFragment DropLocationsListFragment = new DropLocationsListFragment();
        DropLocationsListFragment.setArguments(bundle);
        return DropLocationsListFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_items_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof DropLocationsListFragment.IDropLocationsListHost) {
            mHost = (DropLocationsListFragment.IDropLocationsListHost) context;
        } else throw new IllegalStateException("Host must implement IDropLocationsListHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ItemsListViewModel mDropLocationsListViewModel = new ViewModelProvider(this).get(ItemsListViewModel.class);
        mDropLocationsListViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

    }

    private void observeLiveData() {
        //observe validations errors

    }

    @Override
    protected void initViewsAndVariables() {
        mDropLocationsList = new ArrayList<>();
        getArgumentsData();
        setViews();
    }

    private void setViews() {
        if (mNavigation.equals(OrderSummaryFragment.class.getName())) {
            tvTitle.setText(getString(R.string.s_drop_location));
            btnContinue.setVisibility(View.GONE);
            fabAddItem.hide();
        } else {
            tvTitle.setText(getString(R.string.s_rearrange_delivery));
        }
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.CREATE_ORDER_DETAILS)) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
            if (mCreateOrderDetails != null && mCreateOrderDetails.getDropOffArr().size() > 0)
                mDropLocationsList.addAll(mCreateOrderDetails.getDropOffArr());
            mNavigation = getArguments().getString(AppConstants.ACTION_NAVIGATION);
            setUpAdapter();
        }
    }


    private void setUpAdapter() {
        if (mNavigation.equals(OrderSummaryFragment.class.getName())) {
            mDropLocationsList.remove(0);
            MoreDropsAdapter mMoreDropsAdapter = new MoreDropsAdapter(mDropLocationsList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rvDropLocationsList.getContext(), LinearLayoutManager.VERTICAL, false);
            rvDropLocationsList.setLayoutManager(linearLayoutManager);
            rvDropLocationsList.setAdapter(mMoreDropsAdapter);

        } else {
            mAdapter = new DropLocationsListAdapter(mDropLocationsList, this,this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rvDropLocationsList.getContext(), LinearLayoutManager.VERTICAL, false);
            rvDropLocationsList.setLayoutManager(linearLayoutManager);
            rvDropLocationsList.setAdapter(mAdapter);

            ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
            mItemTouchHelper = new ItemTouchHelper(callback);
            mItemTouchHelper.attachToRecyclerView(rvDropLocationsList);
        }

        rvDropLocationsList.scheduleLayoutAnimation();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_continue, R.id.fab_add_item})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_continue:
                AppUtils.getInstance().showAlertDialog(getContext(),
                        getString(R.string.s_do_you_want_the_system_to_optimize_route_for_you),
                        this, this);
                break;
            case R.id.fab_add_item:
                if (mCreateOrderDetails.getDropOffArr().size() >= 5) {
                    showSnackBar(R.string.s_you_can_add_5_drops_at_max);
                } else
                mHost.editRecipientAddress(null, null);
                break;
        }

    }

    private CreateOrderRequest getOrderDetails() {
        mCreateOrderDetails.setDropOffArr(mAdapter.getDropOffArr());
        return mCreateOrderDetails;
    }

    public CreateOrderRequest getOrderRequest() {
        return mCreateOrderDetails;
    }

    @Override
    public void onEditItemClicked(int adapterPosition) {
        mHost.backToAddItemFragment(null, adapterPosition, getOrderDetails());
    }

    @Override
    public void onDeleteItemClicked(int adapterPosition) {
        mPosition = adapterPosition;
        AppUtils.getInstance().openConfirmationDialog(getContext(), getString(R.string.s_do_you_want_to_delete_this_drop), this);
    }

    public void updateDropLocationsList(ArrayList<DropOffArr> dropOffArr) {
        Collection<DropOffArr> list = new ArrayList<>(dropOffArr);
        mDropLocationsList.clear();
        mDropLocationsList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
       mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onYesViewClicked() {
        mAdapter.removeDrop(mPosition);
        mCreateOrderDetails.getDropOffArr().remove(mPosition);
        if (mCreateOrderDetails.getDropOffArr().size() == 0) {
            mHost.onBackPressed();
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int id) {
        switch (id) {
            case DialogInterface.BUTTON_POSITIVE:
                    mHost.navigateToConfirmOrderFragment(getOrderDetails(), true);
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                    mHost.navigateToConfirmOrderFragment(getOrderDetails(), false);
                break;
        }
    }

    public String getActionNavigation() {
        return mNavigation;
    }


    public interface IDropLocationsListHost {
        void onBackPressed();

        void backToAddItemFragment(ItemDetails itemDetails, int position, CreateOrderRequest mCreateOrderDetails);

        void navigateToConfirmOrderFragment(CreateOrderRequest orderDetails, boolean isToOptimize);

        void editRecipientAddress(PickupLocation offArr, DropOffArr dropOffArr);
    }
}
