package com.qd.user.ui.home.notifications;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.notifications.NotificationsListingResponse;

public class NotificationsViewModel extends ViewModel {
    private RichMediatorLiveData<NotificationsListingResponse> mNotificationsLiveData;
    private RichMediatorLiveData<CommonResponse> mMarkReadLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> loadingStateObserver;
    private NotificationsRepository mRepo = new NotificationsRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.loadingStateObserver = loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mNotificationsLiveData == null) {
            mNotificationsLiveData = new RichMediatorLiveData<NotificationsListingResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return loadingStateObserver;
                }
            };
        }
        if (mMarkReadLiveData == null) {
            mMarkReadLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return loadingStateObserver;
                }
            };
        }

    }

    /**
     * This method gives the notifications live data object to {@link NotificationsFragment}
     *
     * @return {@link #mNotificationsLiveData}
     */
    RichMediatorLiveData<NotificationsListingResponse> getNotificationsLiveData() {
        return mNotificationsLiveData;
    }

    /**
     * This method gives the mark notification live data object to {@link NotificationsFragment}
     *
     * @return {@link #mMarkReadLiveData}
     */
    RichMediatorLiveData<CommonResponse> getMarkReadLiveData() {
        return mMarkReadLiveData;
    }


    void getNotificationsList(int page, String deviceId) {
        mRepo.hitNotificationsApi(mNotificationsLiveData, page, deviceId);
    }


    void markAllNotificationAsRead() {
        mMarkReadLiveData.setLoadingState(true);
        mRepo.markNotificationsAsRead(null, true, mMarkReadLiveData);
    }

    void markNotificationRead(String id) {
        mMarkReadLiveData.setLoadingState(true);
        mRepo.markNotificationsAsRead(id, false, mMarkReadLiveData);
    }
}
