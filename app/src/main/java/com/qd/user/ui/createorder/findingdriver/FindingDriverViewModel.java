package com.qd.user.ui.createorder.findingdriver;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.Result;
import com.qd.user.ui.trackorder.orderdetail.OrderDetailFragment;

import org.json.JSONObject;

public class FindingDriverViewModel extends ViewModel {

    private RichMediatorLiveData<Result> mDriverAcceptanceLiveData;
    private RichMediatorLiveData<CommonResponse> mRetryLiveData;
    private RichMediatorLiveData<CommonListResponse> mCancellationReasonsLiveData;
    private RichMediatorLiveData<OrderCancellationResponse> mAutoCancellationLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private RichMediatorLiveData<OrderCancellationResponse> mOrderCancellationLiveData;

    private FindingDriverRepository mRepo = new FindingDriverRepository();


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mDriverAcceptanceLiveData == null) {
            mDriverAcceptanceLiveData = new RichMediatorLiveData<Result>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mRetryLiveData == null) {
            mRetryLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mCancellationReasonsLiveData == null) {
            mCancellationReasonsLiveData = new RichMediatorLiveData<CommonListResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mAutoCancellationLiveData == null) {
            mAutoCancellationLiveData = new RichMediatorLiveData<OrderCancellationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mOrderCancellationLiveData == null) {
            mOrderCancellationLiveData = new RichMediatorLiveData<OrderCancellationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }


    /**
     * This method gives the DriverAvailability live data object to {@link FindingDriverFragment}
     *
     * @return {@link #mDriverAcceptanceLiveData}
     */
    RichMediatorLiveData<Result> getDriverAvailabilityLiveData() {
        return mDriverAcceptanceLiveData;
    }

    /**
     * This method gives the cancellation live data object to {@link OrderDetailFragment}
     *
     * @return {@link #mAutoCancellationLiveData}
     */
    RichMediatorLiveData<OrderCancellationResponse> getAutoCancellationLiveData() {
        return mAutoCancellationLiveData;
    }

    /**
     * This method gives the cancellation live data object to {@link OrderDetailFragment}
     *
     * @return {@link #mAutoCancellationLiveData}
     */
    RichMediatorLiveData<OrderCancellationResponse> getOrderCancellationLiveData() {
        return mOrderCancellationLiveData;
    }


    /**
     * This method gives the Retry live data object to {@link FindingDriverFragment}
     *
     * @return {@link #mRetryLiveData}
     */
    RichMediatorLiveData<CommonResponse> getRetryLiveData() {
        return mRetryLiveData;
    }


    void emitForDriverAvailability(String id) {
        mRepo.emitForDriverAvailability(id);
        // listenForDriverAcceptance();
    }

    private void listenForDriverAcceptance() {
        mRepo.listenForDriverAcceptance(mDriverAcceptanceLiveData);
    }

    void disconnectSocketListeners() {
        mRepo.disconnectSocketListeners();
    }

    void retryForFindingDriver(String id) {
        mRepo.retryForDriver(id, mRetryLiveData);

    }

    public void cancelOrder(JSONObject requestPayloadForOrderCancellation) {
        mRepo.cancelOrder(requestPayloadForOrderCancellation, mOrderCancellationLiveData);
    }

    int getWaitingTimeForRetry() {
        return mRepo.getWaitingTimeForRetry();
    }

    void checkForOrderStatus(String id) {
        mRepo.checkForOrderStatus(id, mRetryLiveData);
    }

    void autoCancelOrder(String id) {
        mRepo.autoCancelOrder(id, mAutoCancellationLiveData);
    }

    public void getCancellationReasons() {
        mRepo.getCancellationReasons(mCancellationReasonsLiveData);
    }
}
