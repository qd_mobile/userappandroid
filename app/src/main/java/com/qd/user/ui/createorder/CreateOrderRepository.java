package com.qd.user.ui.createorder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CreateOrderRepository {

    public void getVehicleCategories(final RichMediatorLiveData<VehicleEtaResponse> vehicleCategoryLiveData, String orderType, double latitude, double longitude) {
        DataManager.getInstance().getAvailableVehicles(orderType, latitude, longitude, DataManager.getInstance().getUserId()).enqueue(new NetworkCallback<VehicleEtaResponse>() {
            @Override
            public void onSuccess(VehicleEtaResponse vehicleCategoryResponse) {
                vehicleCategoryLiveData.setValue(vehicleCategoryResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                vehicleCategoryLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                vehicleCategoryLiveData.setError(t);
            }
        });
       /* DataManager.getInstance().getVehicleCategories().enqueue(new NetworkCallback<VehicleCategoryResponse>() {
            @Override
            public void onSuccess(VehicleCategoryResponse vehicleCategoryResponse) {
                vehicleCategoryLiveData.setValue(vehicleCategoryResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                vehicleCategoryLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                vehicleCategoryLiveData.setError(t);
            }
        });*/
    }

    public String getBusinessAddress() {
        return DataManager.getInstance().getBusinessAddress();
    }


    public void getDeliveryCharges(final RichMediatorLiveData<OptimizedDeliveryResponse> optimizedDeliveryLiveData, OptimizedDeliveryRequest dataForOptimizedRoute) {
        dataForOptimizedRoute.setVendorId(DataManager.getInstance().getUserId());
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().getDeliveryCharges(new JSONObject(gson.toJson(dataForOptimizedRoute))).enqueue(new NetworkCallback<OptimizedDeliveryResponse>() {
                @Override
                public void onSuccess(OptimizedDeliveryResponse optimizedDeliveryResponse) {
                    optimizedDeliveryLiveData.setValue(optimizedDeliveryResponse);
                    optimizedDeliveryLiveData.setLoadingState(false);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    optimizedDeliveryLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    optimizedDeliveryLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void getVendorInfo(final RichMediatorLiveData<VendorInfoResponse> vendorInfoResponseLiveData) {
        DataManager.getInstance().getVendorInfo(DataManager.getInstance().getUserId()).enqueue(new NetworkCallback<VendorInfoResponse>() {
            @Override
            public void onSuccess(VendorInfoResponse vendorInfoResponse) {
                vendorInfoResponseLiveData.setValue(vendorInfoResponse);
                vendorInfoResponseLiveData.setLoadingState(false);
                String fullBusinessAddress = new GsonBuilder().create().toJson(vendorInfoResponse.getResult().getFullBusinessAddress());
                DataManager.getInstance().setBusinessAddress(fullBusinessAddress);
                DataManager.getInstance().setVendorPaymentMode(vendorInfoResponse.getResult().getPaymentModes());
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                vendorInfoResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                vendorInfoResponseLiveData.setError(t);
            }
        });
    }

    void checkDriverAvailability(final RichMediatorLiveData<DriverAvailabilityResponse> driverAvailabilityLiveData, List<Double> coordinates, String vehicleType) {
        DataManager.getInstance().checkDriverAvailability(createRequestPayload(coordinates, vehicleType)).enqueue(new NetworkCallback<DriverAvailabilityResponse>() {
            @Override
            public void onSuccess(DriverAvailabilityResponse driverAvailabilityResponse) {
                driverAvailabilityLiveData.setValue(driverAvailabilityResponse);
                driverAvailabilityLiveData.setLoadingState(false);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                driverAvailabilityLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                driverAvailabilityLiveData.setError(t);
            }
        });
    }

    private JSONObject createRequestPayload(List<Double> coordinates, String vehicleType) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("sourceLatitude", coordinates.get(1));
            jsonObject.put("sourceLongitude", coordinates.get(0));
            jsonObject.put("vehicleType", vehicleType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public String getVendorPaymentMode() {
        return DataManager.getInstance().getVendorPaymentMode();
    }
}
