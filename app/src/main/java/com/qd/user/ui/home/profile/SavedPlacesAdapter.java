package com.qd.user.ui.home.profile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.createorder.savedaddress.Result;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SavedPlacesAdapter extends RecyclerView.Adapter<SavedPlacesAdapter.SavedPlacesAdapterViewHolder> {

    private ISavedPlaces iSavedPlaces;
    private ArrayList<Result> mSavedPlacesList;

    public SavedPlacesAdapter(ArrayList<Result> savedPlacesList, ISavedPlaces iSavedPlaces) {
        this.iSavedPlaces = iSavedPlaces;
        mSavedPlacesList = savedPlacesList;
    }

    @NonNull
    @Override
    public SavedPlacesAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_saved_places, viewGroup, false);
        return new SavedPlacesAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SavedPlacesAdapterViewHolder holder, int position) {
        if (mSavedPlacesList.get(position).getSavedLocation().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", "") != null)
            holder.tvPlaceAddress.setText(mSavedPlacesList.get(position).getSavedLocation().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        if (mSavedPlacesList.get(position).getSavedLocation().getName()!= null)
            holder.tvPlaceName.setText(mSavedPlacesList.get(position).getSavedLocation().getName());
    }

    @Override
    public int getItemCount() {
        return mSavedPlacesList.size();
    }

    class SavedPlacesAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_place_name)
        TextView tvPlaceName;
        @BindView(R.id.tv_place_address)
        TextView tvPlaceAddress;
        @BindView(R.id.iv_delete)
        ImageView ivDelete;
        @BindView(R.id.iv_edit)
        ImageView ivEdit;

        SavedPlacesAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ivDelete.setOnClickListener(v -> iSavedPlaces.onDeleteSavedPlace(mSavedPlacesList.get(getAdapterPosition()).getId()));
            ivEdit.setOnClickListener(v -> iSavedPlaces.onEditSavedPlaces(mSavedPlacesList.get(getAdapterPosition())));

        }
    }

    public interface ISavedPlaces {

        void onDeleteSavedPlace(String id);

        void onEditSavedPlaces(Result result);
    }
}
