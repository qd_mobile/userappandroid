package com.qd.user.ui.onboard.forgotpassword;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends BaseFragment  implements TextWatcher{


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_subtittle)
    TextView tvSubtittle;
    @BindView(R.id.rb_email)
    RadioButton rbEmail;
    @BindView(R.id.rb_phone)
    RadioButton rbPhone;
    @BindView(R.id.rg_contact_option)
    RadioGroup rgContactOption;
    @BindView(R.id.et_email_address)
    EditText etEmailAddress;
    @BindView(R.id.til_email_address)
    TextInputLayout tvEmailAddress;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.et_country_code)
    EditText etCountryCode;
    @BindView(R.id.view_country_code)
    View viewCountryCode;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.tv_phone_number)
    TextInputLayout tvPhoneNumber;
    @BindView(R.id.view_phone_number)
    View viewPhoneNumber;
    @BindView(R.id.btn_send_link)
    Button btnSendLink;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.tv_error_phone_number)
    TextView tvErrorPhoneNumber;
    private Unbinder unbinder;
    private IForgotPasswordHost mHost;
    private ForgotPasswordViewModel mForgotPasswordViewModel;
    private boolean isEmailAddressSelected;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    public static ForgotPasswordFragment getInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment_order_status
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IForgotPasswordHost) {
            mHost = (IForgotPasswordHost) context;
        } else throw new IllegalStateException("Host must implement IForgotPasswordHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mForgotPasswordViewModel = new ViewModelProvider(this).get(ForgotPasswordViewModel.class);
        mForgotPasswordViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setListeners();
        rgContactOption.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rb_email) {
                isEmailAddressSelected = true;
                showEmailAddressField();
            } else {
                isEmailAddressSelected = false;
                showPhoneNumberField();
            }
        });
    }

    private void setListeners() {
        etPhoneNumber.addTextChangedListener(this);
        etEmailAddress.addTextChangedListener(this);
    }

    private void showPhoneNumberField() {
        tvPhoneNumber.setVisibility(View.VISIBLE);
        viewPhoneNumber.setVisibility(View.VISIBLE);
        etCountryCode.setVisibility(View.VISIBLE);
        viewCountryCode.setVisibility(View.VISIBLE);
        //hide email address  field
        tvEmailAddress.setVisibility(View.GONE);
        viewEmail.setVisibility(View.GONE);
        tvErrorEmail.setVisibility(View.GONE);
           if (!etPhoneNumber.getText().toString().trim().equals(""))
        {           setButtonEnable();
        }
        else {
            setButtonDisable();
        }

    }

    private void showEmailAddressField() {
        tvEmailAddress.setVisibility(View.VISIBLE);
        viewEmail.setVisibility(View.VISIBLE);
        //hide phone number field
        tvErrorPhoneNumber.setVisibility(View.GONE);
        tvPhoneNumber.setVisibility(View.GONE);
        viewPhoneNumber.setVisibility(View.GONE);
        etCountryCode.setVisibility(View.GONE);
        viewCountryCode.setVisibility(View.GONE);
   if (!etEmailAddress.getText().toString().trim().equals(""))
        {           setButtonEnable();
        }
        else {
            setButtonDisable();
        }

    }

    @Override
    protected void initViewsAndVariables() {
        tvTittle.setText(R.string.forgot_password);
        tvSubtittle.setText(R.string.s_choose_a_method_to_reset_password);
        showEmailAddressField();
        etCountryCode.setEnabled(false);
        isEmailAddressSelected=true;
    }

    private void observeLiveData() {
        //observe validations errors
        mForgotPasswordViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_EMAIL:
                        showEmailValidationError(validationErrors.getErrorMessage());
                        focusEmailInputField();

                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER:
                        showPhoneNumberValidationError(validationErrors.getErrorMessage());
                        focusPhoneNumberInputField();
                        break;
                }
            }
        });
        mForgotPasswordViewModel.getForgotPasswordLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            if (commonResponse != null) {
                hideProgressDialog();
                if (isEmailAddressSelected)
                    showSuccessDialog();
                else
                    mHost.openVerifyOtpScreen(etPhoneNumber.getText().toString());
            }
        });

    }

    private void showSuccessDialog() {
        if (getActivity() != null) {
            final Dialog otpSentDialog = new Dialog(getActivity(), R.style.customDialog);
            otpSentDialog.setContentView(R.layout.dialog_forgot_password);
            TextView tvMessage = otpSentDialog.findViewById(R.id.tv_message);
            tvMessage.setText(getString(R.string.s_reset_password_link_sent_to_your_email));
            AppUtils.getInstance().dimDialogBackground(otpSentDialog);
            Button btnOk = otpSentDialog.findViewById(R.id.btn_ok);
            otpSentDialog.show();

            btnOk.setOnClickListener(v -> {
                otpSentDialog.dismiss();
                mHost.onBackPressed();

            });

        }
    }

    //focus email address field
    private void focusEmailInputField() {
        etEmailAddress.setSelection(etEmailAddress.getText().toString().length());
        etEmailAddress.requestFocus();
    }

    private void focusPhoneNumberInputField() {
        etPhoneNumber.setSelection(etPhoneNumber.getText().toString().length());
        etPhoneNumber.requestFocus();
    }

    private void showPhoneNumberValidationError(CharSequence errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorPhoneNumber);
        tvErrorPhoneNumber.setText(errorMessage);
        tvErrorPhoneNumber.setVisibility(View.VISIBLE);
    }

    private void showEmailValidationError(CharSequence errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorEmail);
        tvErrorEmail.setText(errorMessage);
        tvErrorEmail.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_send_link})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_send_link:
                showProgressDialog();
                mForgotPasswordViewModel.onSendLinkViewClicked(getForgotPasswordData(),isEmailAddressSelected);
                break;
        }
    }

    private CreateAccountInfo getForgotPasswordData() {
        CreateAccountInfo createAccountInfo = new CreateAccountInfo();
        if (isEmailAddressSelected) {
            if (!etEmailAddress.getText().toString().trim().equals(""))
                createAccountInfo.setEmail(etEmailAddress.getText().toString().trim());
        }
        else {
            if (!etPhoneNumber.getText().toString().trim().equals(""))
                createAccountInfo.setPhoneNumber(String.valueOf(etPhoneNumber.getText()));
            createAccountInfo.setCountryCode(etCountryCode.getText().toString());
        }
        return createAccountInfo;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
      enableOrDisableButton();
    }

    private void enableOrDisableButton() {
        if (isEmailAddressSelected&&!etEmailAddress.getText().toString().trim().equals("")){
            setButtonEnable();

        }
          else if (!etPhoneNumber.getText().toString().trim().equals(""))
        {           setButtonEnable();
        }
        else {
           setButtonDisable();
        }
    }

    private void setButtonDisable() {
        btnSendLink.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
        btnSendLink.setClickable(false);
    }

    private void setButtonEnable() {
        btnSendLink.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        btnSendLink.setClickable(true);
    }


    public interface IForgotPasswordHost {
        void onBackPressed();

        void openVerifyOtpScreen(String phoneNumber);
    }
}
