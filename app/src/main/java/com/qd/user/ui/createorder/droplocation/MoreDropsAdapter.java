package com.qd.user.ui.createorder.droplocation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qd.user.R;
import com.qd.user.model.createorder.request.DropOffArr;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoreDropsAdapter extends RecyclerView.Adapter<MoreDropsAdapter.MoreDropsAdapterViewHolder>  {

    private final List<DropOffArr> mMoreDrops;

    MoreDropsAdapter(List<DropOffArr> moreDrops) {
        mMoreDrops = moreDrops;
    }


    @NonNull
    @Override
    public MoreDropsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_multiple_drops, viewGroup, false);
        return new MoreDropsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoreDropsAdapterViewHolder moreDropsAdapterViewHolder, int i) {
        moreDropsAdapterViewHolder.tvName.setText(mMoreDrops.get(i).getRecipientName());
        moreDropsAdapterViewHolder.tvItemName.setText(mMoreDrops.get(i).getItem().get(0).getItemDescription());
        moreDropsAdapterViewHolder.tvPhoneNumber.setText(mMoreDrops.get(i).getRecipientMobileNo());

        moreDropsAdapterViewHolder.tvLabelItem.setText(String.format(Locale.getDefault(), "Item %d", i + 2));

        if (mMoreDrops.get(i).getRecipientEmail() != null && !mMoreDrops.get(i).getRecipientEmail().isEmpty()) {
            moreDropsAdapterViewHolder.tvEmail.setText(mMoreDrops.get(i).getRecipientEmail());
        } else {
            moreDropsAdapterViewHolder.tvLabelEmail.setVisibility(View.GONE);
            moreDropsAdapterViewHolder.tvEmail.setVisibility(View.GONE);
        }

        moreDropsAdapterViewHolder.tvDropLocation.setText(mMoreDrops.get(i).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (mMoreDrops.get(i).getDriverNote() != null && !mMoreDrops.get(i).getDriverNote().isEmpty()) {
            moreDropsAdapterViewHolder.tvNoteForDriver.setText(mMoreDrops.get(i).getDriverNote());
        } else {
            moreDropsAdapterViewHolder.ivNoteForDriver.setVisibility(View.GONE);
            moreDropsAdapterViewHolder.tvNoteForDriver.setVisibility(View.GONE);
            moreDropsAdapterViewHolder.tvLabelNoteForDriver.setVisibility(View.GONE);
        }

        if (mMoreDrops.get(i).getItem().get(0).getItemImage().size() > 0) {
            Glide.with(moreDropsAdapterViewHolder.ivItem.getContext())
                    .load(mMoreDrops.get(i).getItem().get(0).getItemImage().get(0))
                    .apply(new RequestOptions().placeholder(R.drawable.ic_delivery_type_single_inactive))
                    .into(moreDropsAdapterViewHolder.ivItem);
        }
    }

    @Override
    public int getItemCount() {
        return mMoreDrops.size();
    }

    class MoreDropsAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_item)
        ImageView ivItem;
        @BindView(R.id.tv_label_item)
        TextView tvLabelItem;
        @BindView(R.id.tv_label_eta)
        TextView tvLabelEta;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.iv_drop_location)
        ImageView ivDropLocation;
        @BindView(R.id.tv_label_drop_location)
        TextView tvLabelDropLocation;
        @BindView(R.id.tv_drop_location)
        TextView tvDropLocation;
        @BindView(R.id.iv_note_for_driver)
        ImageView ivNoteForDriver;
        @BindView(R.id.tv_label_note_for_driver)
        TextView tvLabelNoteForDriver;
        @BindView(R.id.tv_note_for_driver)
        TextView tvNoteForDriver;
        @BindView(R.id.tv_label_recipient_details)
        TextView tvLabelRecipientDetails;
        @BindView(R.id.tv_label_name)
        TextView tvLabelName;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_label_phone_number)
        TextView tvLabelPhoneNumber;
        @BindView(R.id.tv_phone_number)
        TextView tvPhoneNumber;
        @BindView(R.id.tv_label_email)
        TextView tvLabelEmail;
        @BindView(R.id.tv_email)
        TextView tvEmail;
        @BindView(R.id.cl_recipient_details)
        ConstraintLayout clRecipientDetails;

        MoreDropsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            tvLabelRecipientDetails.setOnClickListener(v -> {
                if (clRecipientDetails.getVisibility() == View.VISIBLE) {
                    clRecipientDetails.setVisibility(View.GONE);
                } else {
                    clRecipientDetails.setVisibility(View.VISIBLE);
                }
            });

        }
    }
}
