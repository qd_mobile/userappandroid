package com.qd.user.ui.home.createdorder;

import com.google.gson.Gson;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.myorders.OrdersResponse;
import com.qd.user.model.recievedorders.RecievedOrdersResponse;

import java.util.HashMap;

class OrderCreatedRepository {
    void getOrdersList(int mNexPage, final RichMediatorLiveData<OrdersResponse> mOrdersLiveData, String fromDate, String toDate) {
        DataManager.getInstance().hitOrderHistoryApi(createOrderHistoryObject(mNexPage, fromDate, toDate)).enqueue(new NetworkCallback<OrdersResponse>() {
            @Override
            public void onSuccess(OrdersResponse ordersResponse) {
                mOrdersLiveData.setValue(ordersResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mOrdersLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mOrdersLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> createOrderHistoryObject(int mNexPage, String fromDate, String toDate) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_PAGE, mNexPage);
        hashMap.put(NetworkConstants.KEY_CASH_CLIENT_ID, DataManager.getInstance().getUserId());
        hashMap.put(NetworkConstants.KEY_LIMIT, 10);
        if (fromDate != null && toDate != null) {
            hashMap.put(NetworkConstants.KEY_REGISTER_FROM, fromDate);
            hashMap.put(NetworkConstants.KEY_REGISTER_TO, toDate);
        }

        return hashMap;
    }

    private HashMap<String, Object> createOrderReceivedObject(int mNexPage, String fromDate, String toDate) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_PAGE, mNexPage);
        hashMap.put(NetworkConstants.KEY_CASH_CLIENT_ID, DataManager.getInstance().getUserId());
        hashMap.put(NetworkConstants.KEY_PHONE_NUMBER, DataManager.getInstance().getPhoneNumber());
        hashMap.put(NetworkConstants.KEY_LIMIT, 10);

        if (fromDate != null && !fromDate.isEmpty()) {
            hashMap.put(NetworkConstants.KEY_REGISTER_FROM, fromDate);
        }
        if (toDate != null && !toDate.isEmpty()) {
            hashMap.put(NetworkConstants.KEY_REGISTER_TO, toDate);
        }

        return hashMap;
    }


    public String getRegistrationDate() {
        return DataManager.getInstance().getRegistrationDateAndTime();
    }

    void getReceivedOrdersList(int mNextPage, final RichMediatorLiveData<RecievedOrdersResponse> receivedOrdersLiveData, String fromDate, String toDate) {
        DataManager.getInstance().hitRecievedOrderApi(createOrderReceivedObject(mNextPage, fromDate, toDate)).enqueue(new NetworkCallback<RecievedOrdersResponse>() {
            @Override
            public void onSuccess(RecievedOrdersResponse ordersResponse) {
                receivedOrdersLiveData.setValue(ordersResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                receivedOrdersLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                receivedOrdersLiveData.setError(t);
            }
        });
    }


    void getCancellationReasons(RichMediatorLiveData<CommonListResponse> cancellationReasonsLiveData) {
        DataManager.getInstance().getCancellationReasons().enqueue(new NetworkCallback<CommonListResponse>() {
            @Override
            public void onSuccess(CommonListResponse commonResponse) {
                cancellationReasonsLiveData.setValue(commonResponse);
                DataManager.getInstance().setCancellationReasons(new Gson().toJson(commonResponse.getResult()));
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                cancellationReasonsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                cancellationReasonsLiveData.setError(t);
            }
        });
    }
}
