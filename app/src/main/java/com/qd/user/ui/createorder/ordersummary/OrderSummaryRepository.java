package com.qd.user.ui.createorder.ordersummary;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.balance.PendingBalanceResponse;
import com.qd.user.model.createorder.flexible.FlexibleOrdersResponse;
import com.qd.user.model.createorder.promo.PromoCodeResponse;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.response.CreateOrderResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

class OrderSummaryRepository {

    void createOrder(final CreateOrderRequest createOrderDetails, final RichMediatorLiveData<CreateOrderResponse> createOrderResponseLiveData) {
        createOrderDetails.setPlatform("ANDROID_" + DataManager.getInstance().getVersionName());
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().createOrder(new JSONObject(gson.toJson(createOrderDetails))).enqueue(new NetworkCallback<CreateOrderResponse>() {
                @Override
                public void onSuccess(CreateOrderResponse createOrderResponse) {
                    createOrderResponseLiveData.setValue(createOrderResponse);
                    createOrderResponseLiveData.setLoadingState(false);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    createOrderResponseLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    createOrderResponseLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getUserType() {
        return DataManager.getInstance().getUserType();
    }

    void checkDriverAvailability(final RichMediatorLiveData<DriverAvailabilityResponse> driverAvailabilityLiveData, List<Double> coordinates, String vehicleType) {
        DataManager.getInstance().checkDriverAvailability(createRequestPayload(coordinates, vehicleType)).enqueue(new NetworkCallback<DriverAvailabilityResponse>() {
            @Override
            public void onSuccess(DriverAvailabilityResponse driverAvailabilityResponse) {
                driverAvailabilityLiveData.setValue(driverAvailabilityResponse);
                driverAvailabilityLiveData.setLoadingState(false);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                driverAvailabilityLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                driverAvailabilityLiveData.setError(t);
            }
        });
    }

    private JSONObject createRequestPayload(List<Double> coordinates, String vehicleType) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("sourceLatitude", coordinates.get(1));
            jsonObject.put("sourceLongitude", coordinates.get(0));
            jsonObject.put("vehicleType", vehicleType);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    void applyPromo(String code, float amount, final RichMediatorLiveData<PromoCodeResponse> promoCodeLiveData) {
        DataManager.getInstance().applyPromoCode(createRequestPayloadForPromo(code, amount)).enqueue(new NetworkCallback<PromoCodeResponse>() {
            @Override
            public void onSuccess(PromoCodeResponse promoCodeResponse) {
                promoCodeLiveData.setLoadingState(false);
                promoCodeLiveData.setValue(promoCodeResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                promoCodeLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                promoCodeLiveData.setError(t);
            }
        });
    }


    private HashMap<String, Object> createRequestPayloadForPromo(String code, float amount) {
        HashMap<String, Object> requestMap = new HashMap<>();

        requestMap.put("couponCode", code);
        requestMap.put("userId", DataManager.getInstance().getUserId());
        requestMap.put("platform", "ANDROID");
        requestMap.put("amount", amount);

        return requestMap;
    }

    void validateWalletAmount(BigDecimal totalDeliveryCharges, final RichMediatorLiveData<CommonResponse> validateWalletLiveData) {
        DataManager.getInstance().validateWalletAmount(DataManager.getInstance().getUserId(), totalDeliveryCharges).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                validateWalletLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                validateWalletLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                validateWalletLiveData.setError(t);
            }
        });
    }

    String getVendorPaymentMode() {
        return DataManager.getInstance().getVendorPaymentMode();
    }

    void createFlexibleOrder(CreateOrderRequest createOrderDetails, final RichMediatorLiveData<FlexibleOrdersResponse> createOrderResponseLiveData) {
        createOrderDetails.setPlatform("ANDROID");
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().createFlexibleOrder(new JSONObject(gson.toJson(createOrderDetails))).enqueue(new NetworkCallback<FlexibleOrdersResponse>() {
                @Override
                public void onSuccess(FlexibleOrdersResponse createOrderResponse) {
                    createOrderResponseLiveData.setValue(createOrderResponse);
                    createOrderResponseLiveData.setLoadingState(false);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    createOrderResponseLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    createOrderResponseLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    void getPendingBalance(final RichMediatorLiveData<PendingBalanceResponse> pendingBalanceLiveData) {
        DataManager.getInstance().getPendingBalance(DataManager.getInstance().getUserId()).enqueue(new NetworkCallback<PendingBalanceResponse>() {
            @Override
            public void onSuccess(PendingBalanceResponse pendingBalanceResponse) {
                pendingBalanceLiveData.setValue(pendingBalanceResponse);
                setWaitingTimeForRetry(pendingBalanceResponse.getResult().get(0).getConfigurations().getFindingDriverWaitTime());
                setWaitingTimeForCancellation(pendingBalanceResponse.getResult().get(0).getConfigurations().getAutoCancellation());

            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                pendingBalanceLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                pendingBalanceLiveData.setError(t);
            }

            private void setWaitingTimeForRetry(int findingDriverWaitTime) {
                DataManager.getInstance().setWaitingTimeForRetry(findingDriverWaitTime);
            }

            private void setWaitingTimeForCancellation(int findingDriverWaitTime) {
                DataManager.getInstance().setWaitingTimeForCancellation(findingDriverWaitTime);
            }
        });
    }


}
