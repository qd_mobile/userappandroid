package com.qd.user.ui.home.settings;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;

public class SettingsViewModel extends ViewModel {

    private RichMediatorLiveData<CommonResponse> mNotificationStatusLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private SettingsRepo mRepo = new SettingsRepo();


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mNotificationStatusLiveData == null) {
            mNotificationStatusLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }

    String getBaseUrl() {
        return mRepo.getBaseUrl();
    }

    boolean getNotificationStatus() {
        return mRepo.getNotificationStatus();
    }

    RichMediatorLiveData<CommonResponse> getNotificationStatusLiveData() {
        return mNotificationStatusLiveData;
    }

    void enableOrDisableNotifications(boolean isChecked) {
        mNotificationStatusLiveData.setLoadingState(true);
        mRepo.enableOrDisableNotifications(mNotificationStatusLiveData, isChecked);

    }
}
