package com.qd.user.ui.home.invoice;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.invoicedetails.InvoiceDetailsResponse;
import com.qd.user.ui.home.businesspayment.BusinessPaymentsFragment;

public class InvoiceDetailsViewModel extends ViewModel {
    private RichMediatorLiveData<InvoiceDetailsResponse> mInvoiceDetailsLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private InvoiceRepository mRepo = new InvoiceRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mInvoiceDetailsLiveData == null) {
            mInvoiceDetailsLiveData = new RichMediatorLiveData<InvoiceDetailsResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }


    /**
     * This method gives the Invoice listing live data object to {@link BusinessPaymentsFragment}
     *
     * @return {@link #mInvoiceDetailsLiveData}
     */
    RichMediatorLiveData<InvoiceDetailsResponse> getInvoiceDetailsLiveData() {
        return mInvoiceDetailsLiveData;
    }


    void getInvoiceDetails(String invoiceId, int page) {
        mRepo.getInvoiceDetails(mInvoiceDetailsLiveData, invoiceId, page);
    }

}
