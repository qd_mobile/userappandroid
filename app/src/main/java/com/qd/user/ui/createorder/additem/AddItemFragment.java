package com.qd.user.ui.createorder.additem;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.ImageBean;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.additem.AddImage;
import com.qd.user.model.createorder.additem.ItemDetails;
import com.qd.user.model.createorder.additem.itemstype.Result;
import com.qd.user.model.createorder.distancetime.eta.ETA;
import com.qd.user.model.createorder.distancetime.eta.VehicleCategory;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.paymentmode.PaymentModeType;
import com.qd.user.ui.createorder.CreateOrderFragment;
import com.qd.user.ui.createorder.itemlist.ItemsListFragment;
import com.qd.user.ui.createorder.ordersummary.PaymentModeAdapter;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.PermissionHelper;
import com.qd.user.utils.customviews.DecimalDigitsInputFilter;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

public class AddItemFragment extends BaseFragment implements AddItemImageAdapter.IAddItemImageInterface, PermissionHelper.IGetPermissionListener, AmazonCallback,
        ItemTypeAdapter.AdvanceSearchAdapterInterface, PaymentModeAdapter.IPaymentMode, DialogInterface.OnClickListener {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.view_toolbar)
    View viewToolbar;
    @BindView(R.id.tv_label_item_details)
    TextView tvLabelItemDetails;
    @BindView(R.id.tv_label_item_description)
    TextView tvLabelItemDescription;
    @BindView(R.id.et_item_description)
    EditText etItemDescription;
    @BindView(R.id.view_item_description)
    View viewItemDescription;
    @BindView(R.id.tv_error_item_description)
    TextView tvErrorItemDescription;
    @BindView(R.id.tv_label_item_type)
    TextView tvLabelItemType;
    @BindView(R.id.tv_item_type)
    TextView tvItemType;
    @BindView(R.id.view_item_type)
    View viewItemType;
    @BindView(R.id.tv_error_item_type)
    TextView tvErrorItemType;
    @BindView(R.id.switch_want_to_sell)
    Switch switchWantToSell;
    @BindView(R.id.tv_label_item_price)
    TextView tvLabelItemPrice;
    @BindView(R.id.et_item_price)
    EditText etItemPrice;
    @BindView(R.id.tv_item_price)
    TextView tvItemPrice;
    @BindView(R.id.ll_item_price)
    LinearLayout llItemPrice;
    @BindView(R.id.tv_item_price_text)
    TextView tvItemPriceText;
    @BindView(R.id.tv_label_add_image)
    TextView tvLabelAddImage;
    @BindView(R.id.rv_item_images)
    RecyclerView rvItemImages;
    @BindView(R.id.tv_error_item_image)
    TextView tvErrorItemImage;
    @BindView(R.id.tv_label_delivery_vehicle)
    TextView tvLabelDeliveryVehicle;
    @BindView(R.id.iv_vehicle_name)
    ImageView ivVehicleName;
    @BindView(R.id.tv_vehicle_name)
    TextView tvVehicleName;
    @BindView(R.id.btn_change)
    Button btnChange;
    @BindView(R.id.tv_label_delivery_charges)
    TextView tvLabelDeliveryCharges;
    @BindView(R.id.tv_return_charges)
    TextView tvReturnCharges;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.cl_charges)
    ConstraintLayout clCharges;
    @BindView(R.id.tv_label_payment_mode)
    TextView tvLabelPaymentMode;
    @BindView(R.id.rv_payment_type)
    RecyclerView rvPaymentType;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.sv_advanced_search)
    ScrollView svAdvancedSearch;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.cb_item_default)
    CheckBox cbItemDefault;
    private Unbinder unbinder;
    private IAddItemHost mHost;
    private AddItemViewModel mAddItemViewModel;

    private int mAddedImageListCount;
    private Dialog mItemTypeDialog;
    private TreeSet<Result> mItemsList;
    private ArrayList<Object> mImagesList;
    private AddItemImageAdapter mAdapter;
    private PermissionHelper mPermissionHelper;
    private Uri photoUri;
    private String mCurrentPhotoPath;
    private AmazonS3 mAmazonS3;
    private List<String> mServerUrlList;
    private boolean mIsForEdit;
    private int mPosition;
    private CreateOrderRequest mCreateOrderDetails;
    private ArrayList<VehicleCategory> mVehiclesList;
    //private boolean isWantMoneyBack;
    //private boolean isTimeForPaymentSelected;
    private int mSelectedVehicle;
    private int mSuggestedVehicle;
    private boolean isForMultipleDelivery;
    private int mDropPosition;
    private ArrayList<PaymentModeType> mPaymentModeList;
    private String mPaymentMode;

    public AddItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_item, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static AddItemFragment getInstance(Parcelable itemDetails, int position, Parcelable createOrderData) {
        Bundle bundle = new Bundle();
        if (itemDetails != null)
            bundle.putParcelable(AppConstants.ITEM_DETAILS, itemDetails);

        if (position != -1)
            bundle.putInt(AppConstants.EDIT_INDEX, position);
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, createOrderData);
        AddItemFragment addItemFragment = new AddItemFragment();
        addItemFragment.setArguments(bundle);
        return addItemFragment;

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IAddItemHost) {
            mHost = (IAddItemHost) context;
        } else throw new IllegalStateException("Host must implement IAddItemHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAddItemViewModel = new ViewModelProvider(this).get(AddItemViewModel.class);
        mAddItemViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        initializeAmazonS3();

        mAddItemViewModel.getVehicleTypes(mCreateOrderDetails.getOrderType()
                , mCreateOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                mCreateOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));

        mAddItemViewModel.getItemTypes();
    }

    /**
     * Initializes amazon S3 to upload item images
     */
    private void initializeAmazonS3() {
        mAmazonS3 = new AmazonS3();
        mAmazonS3.setActivity(getActivity());
        mAmazonS3.setCallback(this);

    }

    /**
     * Gets data from arguments
     */
    private void getArgumentsData() {
        if (getArguments() != null) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
            tvTitle.setText(String.format("%s (%s)", getString(R.string.s_add_item), mCreateOrderDetails.getOrderType()));

            if (mCreateOrderDetails.getOrderType().equals(getString(R.string.business))) {
                switchWantToSell.setVisibility(View.GONE);
                tvLabelItemPrice.setVisibility(View.VISIBLE);
                llItemPrice.setVisibility(View.VISIBLE);
                tvLabelPaymentMode.setVisibility(View.VISIBLE);
                rvPaymentType.setVisibility(View.VISIBLE);
                tvLabelItemDescription.setText(getString(R.string.s_item_description_optional));
                setPaymentModeList();
            }

            if (mCreateOrderDetails.getDropOffArr().size() > 1) {
                isForMultipleDelivery = true;
                btnChange.setVisibility(View.GONE);
            }

            setViews(mCreateOrderDetails.getVehicleType());

            if (getArguments().containsKey(AppConstants.ITEM_DETAILS)) {
                setItemDetailsForEdit(getArguments().getParcelable(AppConstants.ITEM_DETAILS));
            }
            if (getArguments().containsKey(AppConstants.EDIT_INDEX)) {
                mDropPosition = getArguments().getInt(AppConstants.EDIT_INDEX);
                if (mCreateOrderDetails.getDropOffArr().get(mDropPosition).getItem() != null)
                    setItemDetailsForEdit(mCreateOrderDetails.getDropOffArr().get(mDropPosition).getItem().get(0));
            }

            /*if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.GET)) {
                switchWantToSell.setVisibility(View.GONE);
            }*/

            setCharges(mDropPosition != -1 ? mDropPosition : 0);
        }
    }

    private void setCharges(int dropPosition) {
        tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", mCreateOrderDetails.getDropOffArr().get(dropPosition).getDeliveryCharge()));
        if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
            if (mCreateOrderDetails.getPaymentType().equals(AppConstants.PaymentMode.WALLET)) {
                tvReturnCharges.setText(String.format(Locale.getDefault(), String.format("%s", getString(R.string.s_delivery_charges_will_be_charged_on_wallet_and_return_charges)), mCreateOrderDetails.getDropOffArr().get(dropPosition).getDeliveryCharge()));
            } else {
                tvReturnCharges.setText(String.format(Locale.getDefault(), String.format("%s", getString(R.string.s_delivery_charges_will_be_settled_in_invoice_and_return_charges)), mCreateOrderDetails.getDropOffArr().get(dropPosition).getDeliveryCharge()));
            }
        } else {
            // tvReturnCharges.setVisibility(View.GONE);
            tvReturnCharges.setText(String.format(Locale.getDefault(), "Extra %.3f kwd will be charged for return", mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount()));
        }
    }


    private void setPaymentModeList() {
        PaymentModeType paymentModeType1 = new PaymentModeType(AppConstants.PaymentMode.CASH, 1, false);
        PaymentModeType paymentModeType2 = new PaymentModeType(AppConstants.PaymentMode.PREPAID, 2, false);
        mPaymentModeList.add(paymentModeType1);
        mPaymentModeList.add(paymentModeType2);
        setUpPaymentModeAdapter();
    }

    private void setUpPaymentModeAdapter() {
        PaymentModeAdapter paymentModeAdapter = new PaymentModeAdapter(mPaymentModeList, this);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 3);
        rvPaymentType.setLayoutManager(linearLayoutManager);
        rvPaymentType.setAdapter(paymentModeAdapter);
    }

    /**
     * Sets views based on previous data
     *
     * @param vehicleType Vehicle type selected
     */
    private void setViews(CharSequence vehicleType) {
        tvVehicleName.setText(vehicleType);

        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.send))) {
            switchWantToSell.setText(getString(R.string.s_do_you_want_us_to_sell_an_item));
            tvLabelItemPrice.setText(getString(R.string.s_amount_to_take_from_your_customer));
        } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get))) {
            switchWantToSell.setText(getString(R.string.s_do_you_want_us_to_buy_something_for_you));
            tvLabelItemPrice.setText(getString(R.string.s_expected_amount_to_be_paid));
        }

    }

    /**
     * Sets item details for edit, in case user wants to edit item details of existing drop
     *
     * @param parcelable Item details which are to be shown pre filled for editing
     */
    private void setItemDetailsForEdit(Parcelable parcelable) {
        ItemDetails itemDetails = (ItemDetails) parcelable;
        mIsForEdit = true;
        mPosition = itemDetails.getPos();


        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.send))) {
            switchWantToSell.setText(getString(R.string.s_do_you_want_us_to_sell_an_item));
        } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get))) {
            switchWantToSell.setText(getString(R.string.s_do_you_want_us_to_buy_something_for_you));
        } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.business))) {
            etItemPrice.setText(String.format(Locale.getDefault(), "%.3f", itemDetails.getItemPrice()));
        }
        etItemDescription.setText(itemDetails.getItemDescription());
        tvItemType.setText(itemDetails.getItemType());
        mImagesList.addAll(itemDetails.getItemImage());
        mAdapter.notifyDataSetChanged();
    }

    private void observeLiveData() {

        mAddItemViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_ITEM_DESCRIPTION:
                        etItemDescription.requestFocus();
                        shakeItemDescriptionErrorField(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_ITEM_TYPE:
                        tvItemType.setText(R.string.s_other);
                        mSuggestedVehicle = 5;
                        showAlertDialog();
                        tvItemType.requestFocus();
                        //shakeItemTypeErrorField(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_ITEM_IMAGES:
                        shakeItemImagesErrorField(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_ITEM_PRICE:
                        showItemPriceValidationError(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_TIME_OF_PAYMENT:
                        //openConfirmationDialog();
                    case AppConstants.UI_VALIDATIONS.INVALID_PAYMENT_MODE:
                        showSnackBar(getString(validationErrors.getValidationMessage()));
                }
            }
        });

        mAddItemViewModel.getVehicleCategoryLiveData().observe(getViewLifecycleOwner(), vehicleCategoryResponse -> {
            if (vehicleCategoryResponse != null && vehicleCategoryResponse.getResult().getVehicleCategory().size() > 0) {
                mVehiclesList.addAll(vehicleCategoryResponse.getResult().getVehicleCategory());
                setEtaForVehicles(vehicleCategoryResponse.getResult().getETA());
                setVehicleIcon();
            }
        });

        mAddItemViewModel.getItemsListLiveData().observe(getViewLifecycleOwner(), itemsTypeListResponse -> {
            if (itemsTypeListResponse != null && itemsTypeListResponse.getResult().size() > 0) {
                mItemsList.clear();
                if (isForMultipleDelivery) {
                    for (int i = 0; i < itemsTypeListResponse.getResult().size(); i++) {
                        if (itemsTypeListResponse.getResult().get(i).getWeightage() >= mSelectedVehicle) {
                            mItemsList.add(itemsTypeListResponse.getResult().get(i));
                        }
                    }
                } else mItemsList.addAll(itemsTypeListResponse.getResult());
            }
        });

    }

    /**
     * Show validation error in case Item price is empty for business or special orders
     *
     * @param errorMessage Error message to be shown
     */
    private void showItemPriceValidationError(int errorMessage) {
        shakeLayout(tvItemPriceText);
        tvItemPriceText.setText(getString(errorMessage));
        tvItemPriceText.setTextColor(getResources().getColor(R.color.colorDarkRed));
        tvItemPriceText.setVisibility(View.VISIBLE);
    }

    /**
     * Sets the appropriate vehicle icon according to the one he has selected
     * on {@link CreateOrderFragment} or if the vehicle has been changed from
     * {@link ItemsListFragment}
     */
    private void setVehicleIcon() {
        for (int i = 0; i < mVehiclesList.size(); i++) {
            if (mVehiclesList.get(i).getCategoryName().equals(mCreateOrderDetails.getVehicleType())) {
                Glide.with(this)
                        .load(mVehiclesList.get(i).getIconLink().getAddItem())
                        .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.drawable_ci_stroke_silver))
                        .into(ivVehicleName);

                mVehiclesList.get(i).setSelected(true);
                mSelectedVehicle = mVehiclesList.get(i).getWeightage();
            }
        }
    }

    private void setEtaForVehicles(ETA eta) {
        for (int i = 0; i < mVehiclesList.size(); i++) {
            switch (mVehiclesList.get(i).getCategoryName()) {
                case AppConstants.DeliveryVehicles.BIKE:
                    if (eta.getBike().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getBike().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;

                case AppConstants.DeliveryVehicles.CAR:
                    if (eta.getCar().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getCar().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;

                case AppConstants.DeliveryVehicles.VAN:
                    if (eta.getVan().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getVan().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;
                case AppConstants.DeliveryVehicles.COOLER:
                    if (eta.getCooler().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getCooler().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;
            }
        }
    }


    /**
     * Show validation error in case Item image is not there
     *
     * @param errorMessage Error message to be shown
     */
    private void shakeItemImagesErrorField(int errorMessage) {
        tvErrorItemImage.setVisibility(View.VISIBLE);
        tvErrorItemImage.setText(getString(errorMessage));
        shakeLayout(tvErrorItemImage);
    }

    /*
     * Show validation error in case Item type is not selected
     *
     * @param errorMessage Error message to be shown
     *//*
    private void shakeItemTypeErrorField(int errorMessage) {
        tvErrorItemType.setVisibility(View.VISIBLE);
        tvErrorItemType.setText(getString(errorMessage));
        shakeLayout(tvErrorItemType);
    }*/

    /**
     * Show validation error in case Item description is empty
     *
     * @param errorMessage Error message to be shown
     */
    private void shakeItemDescriptionErrorField(int errorMessage) {
        tvErrorItemDescription.setVisibility(View.VISIBLE);
        tvErrorItemDescription.setText(getString(errorMessage));
        shakeLayout(tvErrorItemDescription);
    }


    @Override
    protected void initViewsAndVariables() {
        mServerUrlList = new ArrayList<>();
        mItemsList = new TreeSet<>();
        mImagesList = new ArrayList<>();
        mVehiclesList = new ArrayList<>();
        mPaymentModeList = new ArrayList<>();
        mDropPosition = -1;

        etItemPrice.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(3, 3)});

        setUpAdapter();
        getArgumentsData();
        setListeners();
    }

    private void setListeners() {
        switchWantToSell.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                TransitionManager.beginDelayedTransition(svAdvancedSearch);
                llItemPrice.setVisibility(View.VISIBLE);
                tvLabelItemPrice.setVisibility(View.VISIBLE);

                if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SEND) || mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SPECIAL_SEND))
                    AppUtils.getInstance().openSuccessDialog(getContext(), getString(R.string.s_the_maximum_amount_can_be_paid_30_kd_as_cash_upfront_at_pickup_if_the_amount_is_greater_than_30_kd_then_driver_will_deliver_the_order_and_return_back_to_pay_money_extra_charge_apply), () -> {
                    });
                else {
                    AppUtils.getInstance().openSuccessDialog(getContext(), getString(R.string.s_driver_will_buy_up_to_30_kd_if_total_item_price_is_greater_than_30_kd_then_driver_will_collect_the_money_from_pickup_and_then_purchase_extra_charge_apply), () -> {
                    });
                }
               /* if (mCreateOrderDetails.getOrderType().equals(getString(R.string.send))) {
                      mCreateOrderDetails.setOrderType(getString(R.string.s_special_send));
                    tvReturnCharges.setVisibility(View.VISIBLE);
                    tvReturnCharges.setText(String.format(Locale.getDefault(), getString(R.string.s_extra_return_charges), mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount()));
                }
                else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get))) {
                      mCreateOrderDetails.setOrderType(getString(R.string.s_special_get));
                }*/
            } else {
                TransitionManager.beginDelayedTransition(svAdvancedSearch);
                llItemPrice.setVisibility(View.GONE);
                tvLabelItemPrice.setVisibility(View.GONE);
                tvItemPriceText.setVisibility(View.GONE);
                etItemPrice.setText("");

                /*if (mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_send))) {
                     mCreateOrderDetails.setOrderType(getString(R.string.send));
                      tvReturnCharges.setVisibility(View.GONE);
                } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get))) {
                    mCreateOrderDetails.setOrderType(getString(R.string.get));
                }*/

            }
        });

        cbItemDefault.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                mCreateOrderDetails.setItemDefault(tvItemType.getText().toString());
            } else mCreateOrderDetails.setItemDefault(null);
        });

        etItemPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("StringFormatInvalid")
            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    tvItemPriceText.setTextColor(getResources().getColor(R.color.colorGreenishTeal));
                    tvItemPriceText.setVisibility(View.VISIBLE);
                    if (mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get)) ||
                            (switchWantToSell.isChecked() && mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.GET))) {
                        if (Double.parseDouble(s.toString()) > AppConstants.MIN_ITEM_PRICE_FOR_SPECIAL_ORDERS) {
                            //tvItemPriceText.setTextColor(getResources().getColor(R.color.colorDarkRed));
                            //tvItemPriceText.setText(R.string.s_service_not_available);
                            tvItemPriceText.setText(getString(R.string.s_driver_will_collect_amount_before_pickup));
                        } else {
                            //tvItemPrice.setTextColor(getResources().getColor(R.color.colorGreenishTeal));
                            tvItemPriceText.setText(getString(R.string.s_this_amount_has_to_be_paid_only_in_cash_when_you_receive_your_order));
                        }
                    } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_send)) ||
                            (switchWantToSell.isChecked() && mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SEND))) {
                        if (Double.parseDouble(s.toString()) <= AppConstants.MIN_ITEM_PRICE_FOR_SPECIAL_ORDERS) {
                            tvItemPriceText.setText(getString(R.string.s_the_amount_will_be_paid_by_driver_at_time_of_pickup));
                            //  tvReturnCharges.setVisibility(View.GONE);
                        } else {
                            tvItemPriceText.setText(getString(R.string.s_the_amount_will_be_paid_after_item_has_delivered));
                            tvReturnCharges.setVisibility(View.VISIBLE);
//                            tvReturnCharges.setText(String.format(Locale.getDefault(), getString(R.string.s_extra_return_charges), mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount()));
                        }
                    } /*else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.business))) {
                        if (Integer.valueOf(s.toString()) > 60) {
                            openConfirmationDialog();
                        }
                        tvItemPriceText.setVisibility(View.GONE);
                    }*/ else {
                        tvItemPriceText.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void setUpAdapter() {
        mImagesList.add(0, new AddImage());
        mAdapter = new AddItemImageAdapter(mImagesList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvItemImages.getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvItemImages.setLayoutManager(layoutManager);
        rvItemImages.setAdapter(mAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tv_item_type, R.id.btn_submit, R.id.iv_back, R.id.btn_change})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_item_type:
                if (mItemsList != null && mItemsList.size() > 0)
                    openItemSelectionDialog(mItemsList, getString(R.string.s_item_category));
                else {
                    showProgressDialog();
                    mAddItemViewModel.getItemTypes();
                }
                break;
            case R.id.btn_submit:
                hideValidationFields();
                if (mAddItemViewModel.checkValidations(getItem(), mPaymentMode, mImagesList.size() - 1, mCreateOrderDetails.getOrderType())) {
                    if (mDropPosition == -1 && mSelectedVehicle > mSuggestedVehicle) {
                        showChangeVehicleDialog();
                    } else {
                        for (int i = 0; i < mImagesList.size(); i++) {
                            if (mImagesList.get(i) instanceof String && (((String) mImagesList.get(i)).contains("s3"))) {
                                mServerUrlList.add((String) mImagesList.get(i));
                                mImagesList.remove(i);
                                i--;
                            }
                        }
                        if (mImagesList.size() > 1) {
                            showProgressDialog();
                            uploadImagesToS3Server(mImagesList);
                        } else {
                            mHost.navigateToRecipientDetailsFragment(getOrderDetails(), mDropPosition != -1 ? mDropPosition : 0);
                            // mHost.navigateToItemsListFragment(getItem(), mCreateOrderDetails);
                        }
                    }
                }
                break;
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_change:
                mHost.openSelectVehicleType(mVehiclesList, mCreateOrderDetails);
                break;
        }
    }


    private void hideValidationFields() {
        tvErrorItemType.setVisibility(View.GONE);
        tvErrorItemDescription.setVisibility(View.GONE);
        tvErrorItemImage.setVisibility(View.GONE);
    }

    private void uploadImagesToS3Server(ArrayList<Object> mImagesList) {
        mAddedImageListCount = mImagesList.size() - 1 + mServerUrlList.size();
        for (int i = 1; i < mImagesList.size(); i++) {
            String image = (String) mImagesList.get(i);
            ImageBean bean = addDataInBean(String.valueOf(image));
            mAmazonS3.uploadImage(bean);
        }

    }

    private ImageBean addDataInBean(String path) {
        ImageBean bean = new ImageBean();
        bean.setId("1");
        bean.setName("sample");
        bean.setImagePath(path);
        return bean;
    }


    private void openItemSelectionDialog(TreeSet<Result> itemsList, CharSequence title) {
        if (getActivity() != null) {

            if (mItemTypeDialog == null) {
                mItemTypeDialog = new Dialog(getActivity(), R.style.customDialog);
                mItemTypeDialog.setContentView(R.layout.dialog_address);
                mItemTypeDialog.setCanceledOnTouchOutside(true);
                mItemTypeDialog.setCancelable(true);
                if (mItemTypeDialog.getWindow() != null)
                    mItemTypeDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                AppUtils.getInstance().dimDialogBackground(mItemTypeDialog);
            }

            TextView tvTitle = mItemTypeDialog.findViewById(R.id.tv_address_title);
            mItemTypeDialog.findViewById(R.id.sv_area).setVisibility(View.GONE);

            tvTitle.setText(title);

            RecyclerView rvAddress = mItemTypeDialog.findViewById(R.id.rv_address);

            ItemTypeAdapter itemTypeAdapter = new ItemTypeAdapter(itemsList, this);
            rvAddress.setLayoutManager(new LinearLayoutManager(rvAddress.getContext(), LinearLayoutManager.VERTICAL, false));
            rvAddress.setAdapter(itemTypeAdapter);

            mItemTypeDialog.show();
        }
    }

    @Override
    public void onItemSelected(Result item, String itemName) {
        tvItemType.setText(itemName);
        mItemTypeDialog.dismiss();

        /*TransitionManager.beginDelayedTransition(svAdvancedSearch);
        cbItemDefault.setVisibility(View.VISIBLE);*/

        mSuggestedVehicle = item.getWeightage();
        if (mSuggestedVehicle < mSelectedVehicle) {
            showChangeVehicleDialog();
        } else if (mSuggestedVehicle == 5) {
            showAlertDialog();
            /*TransitionManager.beginDelayedTransition(svAdvancedSearch);
            cbItemDefault.setVisibility(View.GONE);*/
        }

    }

    /**
     * method to create action Alert Dialog
     */
    private void showAlertDialog() {
        if (getContext() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(R.string.s_you_have_selected_others_as_item_category_make_sure_to_select_right_vehicle);
            builder.setCancelable(true);
            builder.setPositiveButton(getContext().getString(R.string.s_ok), this);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    /**
     * Opens dialog to show that vehicle is not suitable for the item type selected,
     * hence gives the option to change the vehicle through {@code openSelectVehicleType()}
     */
    private void showChangeVehicleDialog() {
        if (getActivity() != null) {
            final Dialog otpSentDialog = new Dialog(getActivity(), R.style.customDialog);
            otpSentDialog.setContentView(R.layout.dialog_forgot_password);
            TextView tvMessage = otpSentDialog.findViewById(R.id.tv_message);
            tvMessage.setText(getString(R.string.s_vehicle_type_not_suitable));
            ImageView ivTick = otpSentDialog.findViewById(R.id.iv_resent_link);
            ivTick.setImageDrawable(ivTick.getResources().getDrawable(R.drawable.ic_order_failed_exclamation));
            AppUtils.getInstance().dimDialogBackground(otpSentDialog);
            Button btnOk = otpSentDialog.findViewById(R.id.btn_ok);
            otpSentDialog.show();

            btnOk.setOnClickListener(v -> {
                otpSentDialog.dismiss();
                if (!isForMultipleDelivery) {
                    if (mVehiclesList != null && mVehiclesList.size() > 0) {
                        mHost.openSelectVehicleType(mVehiclesList, mCreateOrderDetails);
                    } else
                        mAddItemViewModel.getVehicleTypes(mCreateOrderDetails.getOrderType(), mCreateOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates().get(1), mCreateOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
                }
            });
        }

    }


    @Override
    public void onAddImagesClicked() {
        mPermissionHelper = new PermissionHelper(this);
        if (mPermissionHelper.hasPermission(getActivity(), new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.PermissionConstants.REQUEST_IMAGE_PERMISSIONS)) {
            permissionsGiven(AppConstants.PermissionConstants.REQUEST_IMAGE_PERMISSIONS);
        }

    }

    @Override
    public void showSnackBar() {
        showSnackBar(getString(R.string.s_max_5_images));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionHelper.setPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void permissionsGiven(int requestCode) {
        openSelectImageDialog();
    }

    /**
     * Opens dialog to choose whether the image is to selected from camera or gallery for uploading
     */
    private void openSelectImageDialog() {
        if (getActivity() != null) {
            final Dialog selectImageDialog = new Dialog(getActivity(), R.style.customDialog);
            selectImageDialog.setContentView(R.layout.dialog_select_image);
            selectImageDialog.setCanceledOnTouchOutside(true);
            selectImageDialog.setCancelable(true);
            AppUtils.getInstance().dimDialogBackgroundWithBottomGravity(selectImageDialog);

            ImageView ivCamera = selectImageDialog.findViewById(R.id.iv_camera);
            ImageView ivGallery = selectImageDialog.findViewById(R.id.iv_gallery);

            ivCamera.setOnClickListener(v -> {
                openCamera();
                selectImageDialog.dismiss();
            });

            ivGallery.setOnClickListener(v -> {
                openGallery();
                selectImageDialog.dismiss();
            });
            selectImageDialog.show();
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppConstants.PermissionConstants.GALLERY_PICK_IMAGE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.PermissionConstants.CAMERA_PIC_REQUEST:
                    if (photoUri != null) {
                        Uri image;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            image = Uri.parse(mCurrentPhotoPath);
                        } else {
                            image = photoUri;
                        }
                        addPickedImage(image.getPath());

                    }
                    break;
                case AppConstants.PermissionConstants.GALLERY_PICK_IMAGE:
                    if (data != null && getActivity() != null) {
                        photoUri = data.getData();
                        String result = null;
                        String[] projection = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getActivity().getContentResolver().query(photoUri, projection, null, null, null);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                int column_index = cursor.getColumnIndexOrThrow(projection[0]);
                                result = cursor.getString(column_index);
                            }
                            cursor.close();
                        }
                        if (result != null)
                            addPickedImage(result);
                    }
            }
        }

    }

    //add uploaded image on server in image list and notify adapter to show it on fragment
    private void addPickedImage(String imagePath) {
        tvErrorItemImage.setVisibility(View.GONE);
        mImagesList.add(imagePath);
        mAdapter.notifyDataSetChanged();
    }

    private void openCamera() {
        if (getActivity() != null) {
            Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                try {
                    photoUri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", createImageFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                photoUri = Uri.fromFile(new File(AppUtils.getInstance().getFilePath()));
            }
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            try {
                intentCamera.putExtra("return-data", true);
                startActivityForResult(intentCamera, AppConstants.PermissionConstants.CAMERA_PIC_REQUEST);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HH:mm:ss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void uploadSuccess(ImageBean bean) {
        hideProgressDialog();
        mServerUrlList.add(bean.getServerUrl());
        if (mServerUrlList.size() == mAddedImageListCount) {

           /* if (mCreateOrderDetails.getOrderType().equals(getString(R.string.business))) {
                mHost.navigateToItemsListFragment(getItem(), mCreateOrderDetails);
            } else*/
            if (mDropPosition != -1) {
                mHost.navigateToRecipientDetailsFragment(getOrderDetails(), mDropPosition);
            } else
                mHost.navigateToRecipientDetailsFragment(getOrderDetails(), 0);
        }
    }

    private CreateOrderRequest getOrderDetails() {
        List<ItemDetails> itemsList = new ArrayList<>();
        itemsList.add(getItem());

        if (isForMultipleDelivery) {
            if (mDropPosition != -1) {
                mCreateOrderDetails.getDropOffArr().get(mDropPosition).setItem(itemsList);
                mCreateOrderDetails.getDropOffArr().get(mDropPosition).setPaymentMode(mPaymentMode);
            } else {
                DropOffArr dropOffArr = new DropOffArr();
                dropOffArr.setItem(itemsList);
                dropOffArr.setPaymentMode(mPaymentMode);
                mCreateOrderDetails.getDropOffArr().add(dropOffArr);
            }
        } else if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
            mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(mPaymentMode);
            mCreateOrderDetails.getDropOffArr().get(0).setItem(itemsList);
        } else {
            mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.COD));
            mCreateOrderDetails.getDropOffArr().get(0).setItem(itemsList);
        }

        switch (mCreateOrderDetails.getOrderType()) {
            case AppConstants.OrderType.SEND:
            case AppConstants.OrderType.SPECIAL_SEND:
                if (switchWantToSell.isChecked()) {
                    mCreateOrderDetails.setOrderType(AppConstants.OrderType.SPECIAL_SEND);
                } else {
                    mCreateOrderDetails.setOrderType(AppConstants.OrderType.SEND);
                }
                break;
            case AppConstants.OrderType.GET:
            case AppConstants.OrderType.SPECIAL_GET:
                if (switchWantToSell.isChecked()) {
                    mCreateOrderDetails.setOrderType(AppConstants.OrderType.SPECIAL_GET);
                } else {
                    mCreateOrderDetails.setOrderType(AppConstants.OrderType.GET);
                }
                break;

        }

        return mCreateOrderDetails;
    }

    private ItemDetails getItem() {
        ItemDetails itemDetails = new ItemDetails();

        itemDetails.setItemDescription(etItemDescription.getText().toString().trim());
        itemDetails.setItemType(tvItemType.getText().toString().split(" ")[0]);


        if (mServerUrlList != null) {
            itemDetails.setItemImage(mServerUrlList);
        }

        itemDetails.setForEdit(mIsForEdit);

        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.business)) ||
                mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SPECIAL_SEND) ||
                mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SPECIAL_GET) ||
                (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.GET) && switchWantToSell.isChecked()) ||
                (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SEND) && switchWantToSell.isChecked())) {
            if (!etItemPrice.getText().toString().trim().isEmpty())
                itemDetails.setItemPrice(new BigDecimal(etItemPrice.getText().toString()));
            else if (llItemPrice.getVisibility() == View.GONE) {
                itemDetails.setItemPrice(BigDecimal.valueOf(0));
            }
        } else itemDetails.setItemPrice(BigDecimal.valueOf(0));
        itemDetails.setPos(mPosition);

        return itemDetails;
    }


    @Override
    public void uploadFailed(ImageBean bean) {
        hideProgressDialog();
        showSnackBar(getString(R.string.s_image_not_uploaded_successfully));
    }

    @Override
    public void uploadProgress(ImageBean bean) {

    }

    @Override
    public void uploadError(Exception e, ImageBean imageBean) {
        hideProgressDialog();
        showSnackBar(getString(R.string.s_image_not_uploaded_successfully));
    }

    public void updateVehicleType(VehicleCategory vehicle, BigDecimal deliveryCharges) {
        tvVehicleName.setText(vehicle.getKeyword());
        Glide.with(this)
                .load(vehicle.getIconLink().getAddItem())
                .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.drawable_ci_stroke_silver))
                .into(ivVehicleName);

        mSelectedVehicle = vehicle.getWeightage();

        mCreateOrderDetails.setVehicleType(vehicle.getCategoryName());
        mCreateOrderDetails.setEta(vehicle.getEta());

        if (deliveryCharges != null) {
            mCreateOrderDetails.getDropOffArr().get(mDropPosition != -1 ? mDropPosition : 0).setDeliveryCharge(deliveryCharges);
            setCharges(mDropPosition != -1 ? mDropPosition : 0);
        }

    }

    /*public void openConfirmationDialog() {
        if (getContext() != null) {
            final Dialog confirmationDialog = new Dialog(getContext(), R.style.customDialog);
            confirmationDialog.setContentView(R.layout.dialog_log_out);
            final TextView tvMessage = confirmationDialog.findViewById(R.id.tv_message);
            tvMessage.setText(getString(R.string.s_when_you_want_money_back));
            AppUtils.getInstance().dimDialogBackground(confirmationDialog);
            final Button btnLogout = confirmationDialog.findViewById(R.id.btn_logout);
            final Button btnCancel = confirmationDialog.findViewById(R.id.btn_cancel);
            btnLogout.setText(R.string.s_later);
            btnCancel.setText(R.string.s_now);
            confirmationDialog.show();

            btnLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isWantMoneyBack) {
                        confirmationDialog.dismiss();
                        openCancellationDialog();
                    } else {
                        confirmationDialog.dismiss();
                        isWantMoneyBack = false;
                        isTimeForPaymentSelected = true;
                    }
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isWantMoneyBack) {
                        isWantMoneyBack = false;
                        isTimeForPaymentSelected = true;
                        confirmationDialog.dismiss();
                    } else {
                        tvMessage.setText(String.format(Locale.getDefault(), "You will be charged additional %d KWD", 2));
                        btnCancel.setText(getString(R.string.s_cancel));
                        btnLogout.setText(getString(R.string.s_confirm));
                        isWantMoneyBack = true;
                    }
                }
            });
        }
    }

    public void openCancellationDialog() {

        if (getContext() != null) {

            final Dialog otpSentDialog = new Dialog(getContext(), R.style.customDialog);
            otpSentDialog.setContentView(R.layout.dialog_forgot_password);
            TextView tvMessage = otpSentDialog.findViewById(R.id.tv_message);
            tvMessage.setText(getString(R.string.s_qd_will_return_as_per_agreement));
            ImageView ivTick = otpSentDialog.findViewById(R.id.iv_resent_link);
            ivTick.setVisibility(View.GONE);
            AppUtils.getInstance().dimDialogBackground(otpSentDialog);
            Button btnOk = otpSentDialog.findViewById(R.id.btn_ok);
            otpSentDialog.show();

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    otpSentDialog.dismiss();
                    isTimeForPaymentSelected = true;
                }
            });
        }
    }
*/
    @Override
    public void onPaymentModeSelected(PaymentModeType paymentModeType) {
        if (paymentModeType.getPaymentIcon() == 1) {
            mPaymentMode = getString(R.string.COD);
            tvLabelItemPrice.setText(getString(R.string.s_amount_needs_to_be_collected_from_customer));
        } else {
            mPaymentMode = AppConstants.PaymentMode.PREPAID;
            tvLabelItemPrice.setText(getString(R.string.s_item_price));
            etItemPrice.setText("0");
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }


    public interface IAddItemHost {
        void onBackPressed();

        //void navigateToItemsListFragment(ItemDetails item, CreateOrderRequest createOrderDetails);

        void navigateToRecipientDetailsFragment(CreateOrderRequest item, int i);

        void openSelectVehicleType(ArrayList<VehicleCategory> vehicleType, CreateOrderRequest mCreateOrderDetails);
    }
}
