package com.qd.user.ui.onboard.forgotpassword;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.HashMap;

class ForgotPasswordRepository {
    public void hitForgotPasswordApi(final RichMediatorLiveData<CommonResponse> mForgotasswordLiveData, final CreateAccountInfo forgotPasswordData, boolean isEmailAddressSelected) {
        DataManager.getInstance().hitForgotPasswordApi(createResponsePayload(forgotPasswordData,isEmailAddressSelected)).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                mForgotasswordLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mForgotasswordLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mForgotasswordLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> createResponsePayload(CreateAccountInfo forgotPasswordData, boolean isEmailAddressSelected) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", "cashClient");
        if (isEmailAddressSelected){
            if (forgotPasswordData.getEmail() != null)
                hashMap.put(NetworkConstants.KEY_EMAIL, forgotPasswordData.getEmail());
        }
        else {
            if (forgotPasswordData.getPhoneNumber() != null)
                hashMap.put("phoneNumber", forgotPasswordData.getPhoneNumber());
            hashMap.put("countryCode", AppConstants.COUNTRY_CODE);
        }

        return hashMap;
    }
}
