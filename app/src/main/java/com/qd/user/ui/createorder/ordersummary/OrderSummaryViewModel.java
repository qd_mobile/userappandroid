package com.qd.user.ui.createorder.ordersummary;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.balance.PendingBalanceResponse;
import com.qd.user.model.createorder.flexible.FlexibleOrdersResponse;
import com.qd.user.model.createorder.promo.PromoCodeResponse;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.response.CreateOrderResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

import java.math.BigDecimal;
import java.util.List;


public class OrderSummaryViewModel extends ViewModel {
    private RichMediatorLiveData<CreateOrderResponse> mCreateOrderResponseLiveData;
    private RichMediatorLiveData<CommonResponse> mValidateWalletLiveData;
    private RichMediatorLiveData<DriverAvailabilityResponse> mDriverAvailabilityLiveData;
    private RichMediatorLiveData<FlexibleOrdersResponse> mFlexibleOrderLiveData;
    private RichMediatorLiveData<PromoCodeResponse> mPromoCodeLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;
    private RichMediatorLiveData<PendingBalanceResponse> mPendingBalanceLiveData;

    private OrderSummaryRepository mRepo = new OrderSummaryRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mCreateOrderResponseLiveData == null) {
            mCreateOrderResponseLiveData = new RichMediatorLiveData<CreateOrderResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateWalletLiveData == null) {
            mValidateWalletLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mDriverAvailabilityLiveData == null) {
            mDriverAvailabilityLiveData = new RichMediatorLiveData<DriverAvailabilityResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mPromoCodeLiveData == null) {
            mPromoCodeLiveData = new RichMediatorLiveData<PromoCodeResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mPendingBalanceLiveData == null) {
            mPendingBalanceLiveData = new RichMediatorLiveData<PendingBalanceResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mFlexibleOrderLiveData == null) {
            mFlexibleOrderLiveData = new RichMediatorLiveData<FlexibleOrdersResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the CreateOrder live data object to {@link OrderSummaryFragment}
     *
     * @return {@link #mCreateOrderResponseLiveData}
     */
    RichMediatorLiveData<CreateOrderResponse> getCreateOrderResponseLiveData() {
        return mCreateOrderResponseLiveData;
    }


    /**
     * This method gives the flexible order live data object to {@link OrderSummaryFragment}
     *
     * @return {@link #mFlexibleOrderLiveData}
     */
    RichMediatorLiveData<FlexibleOrdersResponse> getFlexibleOrderLiveData() {
        return mFlexibleOrderLiveData;
    }

    /**
     * This method gives the promo code live data object to {@link OrderSummaryFragment}
     *
     * @return {@link #mPromoCodeLiveData}
     */
    RichMediatorLiveData<PromoCodeResponse> getPromoCodeLiveData() {
        return mPromoCodeLiveData;
    }

    /**
     * This method gives the CreateOrder live data object to {@link OrderSummaryFragment}
     *
     * @return {@link #mDriverAvailabilityLiveData}
     */
    RichMediatorLiveData<DriverAvailabilityResponse> getDriverAvailabiltyLiveData() {
        return mDriverAvailabilityLiveData;
    }

    /**
     * This method gives the validate Wallet live data object to {@link OrderSummaryFragment}
     *
     * @return {@link #mValidateWalletLiveData}
     */
    RichMediatorLiveData<CommonResponse> getValidateWalletLiveData() {
        return mValidateWalletLiveData;
    }

    /**
     * This method gives the pending balance live data object to {@link OrderSummaryFragment}
     *
     * @return {@link #mPendingBalanceLiveData}
     */
    RichMediatorLiveData<PendingBalanceResponse> getPendingBalanceLiveData() {
        return mPendingBalanceLiveData;
    }

    boolean validateOrderDetails(String paymentType, String orderType, String mode) {
        if (orderType.equals(AppConstants.OrderType.BUSINESS) && (mode == null || mode.equals("N/A"))) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PAYMENT_MODE, R.string.s_please_select_payment_mode));
            return false;
        } else if (paymentType == null) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PAYMENT_MODE, R.string.s_please_select_payment_mode));
            return false;
        }
        return true;
    }

    void createOrder(CreateOrderRequest createOrderDetails) {
        createOrderDetails.setOrdererId(DataManager.getInstance().getUserId());
        mCreateOrderResponseLiveData.setLoadingState(true);

        if (createOrderDetails.getServiceType().equals(AppConstants.serviceType.FLEXIBLE)) {
            mRepo.createFlexibleOrder(createOrderDetails, mFlexibleOrderLiveData);
        } else mRepo.createOrder(createOrderDetails, mCreateOrderResponseLiveData);
    }

    public String getUserType() {
        return mRepo.getUserType();
    }

    void checkAvailabilityStatus(List<Double> coordinates, String vehicleType) {
        mDriverAvailabilityLiveData.setLoadingState(true);
        mRepo.checkDriverAvailability(mDriverAvailabilityLiveData, coordinates, vehicleType);
    }

    void applyPromo(String promoCode, float amount) {
        if (promoValid(promoCode)) {
            mPromoCodeLiveData.setLoadingState(true);
            mRepo.applyPromo(promoCode, amount, mPromoCodeLiveData);
        }
    }

    private boolean promoValid(String promoCode) {
        if (promoCode.isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PROMO_CODE, R.string.s_invalid_promo_code));
            return false;
        }
        return true;
    }

    void validateWalletAmount(BigDecimal totalDeliveryCharges) {
        mRepo.validateWalletAmount(totalDeliveryCharges, mValidateWalletLiveData);
    }

    String getVendorPaymentMode() {
        return mRepo.getVendorPaymentMode();
    }

    void getPendingBalance() {
        mRepo.getPendingBalance(mPendingBalanceLiveData);
    }


    /*public void setWaitingTimeForRetry(long findingDriverWaitTime) {
        mRepo.setWaitingTimeForRetry((int)findingDriverWaitTime);

    }

    public void setWaitingTimeForCancellation(long findingDriverWaitTime) {
        mRepo.setWaitingTimeForCancellation((int)findingDriverWaitTime);

    }*/
}
