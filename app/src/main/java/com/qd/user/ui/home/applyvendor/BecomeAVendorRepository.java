package com.qd.user.ui.home.applyvendor;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.vendor.becomeavendor.BecomeAVendorInfo;
import com.qd.user.model.vendor.uniquevendor.UniqueEmailResponse;
import com.qd.user.model.vendor.vendorsignup.Result;
import com.qd.user.model.vendor.vendorsignup.VendorSignUpResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

class BecomeAVendorRepository {

    public void hitVendorSignUpApi(final BecomeAVendorInfo vendorInfo, final RichMediatorLiveData<VendorSignUpResponse> mBecomeAVendorLiveData, String deviceId) {
        DataManager.getInstance().hitVendorSignUpApi(createBecomeAVendorObject(vendorInfo, deviceId)).enqueue(new NetworkCallback<VendorSignUpResponse>() {
            @Override
            public void onSuccess(VendorSignUpResponse vendorSignUpResponse) {
                if (DataManager.getInstance().getUserType() != null) {
                    setDataInPreference(vendorSignUpResponse.getResult());
                }
                mBecomeAVendorLiveData.setValue(vendorSignUpResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mBecomeAVendorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mBecomeAVendorLiveData.setError(t);
            }
        });
    }

    private void setDataInPreference(Result result) {
        DataManager.getInstance().setUserType(result.getCashClientStatus());
        DataManager.getInstance().setCurrentStatus(result.getCurrentStatus());
        DataManager.getInstance().setVendorStatus(result.getStatus());
        DataManager.getInstance().setCashClientCurrentStatus(result.getCashClientCurrentStatus());

    }


    private JSONObject createBecomeAVendorObject(BecomeAVendorInfo vendorInfo, String deviceId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(NetworkConstants.KEY_EMAIL, vendorInfo.getEmailAddress());
            if (DataManager.getInstance().getUserType() != null) {
                jsonObject.put(NetworkConstants.KEY_IS_CASH_CLIENT, true);
                jsonObject.put(NetworkConstants.KEY_VENDOR_ID, DataManager.getInstance().getUserId());
            }
            jsonObject.put(NetworkConstants.KEY_NAME, vendorInfo.getName());
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(NetworkConstants.KEY_CC_PERSONAL, vendorInfo.getCountryCode());
            jsonObject1.put(NetworkConstants.KEY_PERSONAL, vendorInfo.getPhoneNumber());
            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put(NetworkConstants.KEY_CONTACT, jsonObject1);
            jsonObject2.put(NetworkConstants.KEY_NAME_ENGLISH, vendorInfo.getCompanyName());
            jsonObject.put(NetworkConstants.KEY_COMPANY, jsonObject2);
            jsonObject.put(NetworkConstants.KEY_COMPANY_PROFILE, vendorInfo.getCompanyProfile());
            jsonObject.put(NetworkConstants.KEY_BUSINESS_ADDRESS, vendorInfo.getAddress());
            jsonObject.put(NetworkConstants.KEY_BUSINESS_TYPE, vendorInfo.getBusinessType());
            jsonObject.put(NetworkConstants.KEY_DEVICE_ID, deviceId);
            jsonObject.put(NetworkConstants.KEY_PLATFORM, AppConstants.ANDROID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    public String getUserType() {
        return DataManager.getInstance().getUserType();
    }

    public void checkForUniqueEmail(String email, final RichMediatorLiveData<UniqueEmailResponse> mUniqueVendorLiveData) {
        DataManager.getInstance().hitIsVendorUniqueApi(createObjet(email)).enqueue(new NetworkCallback<UniqueEmailResponse>() {
            @Override
            public void onSuccess(UniqueEmailResponse uniqueEmailResponse) {
                mUniqueVendorLiveData.setValue(uniqueEmailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mUniqueVendorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mUniqueVendorLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> createObjet(String email) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_TYPE, AppConstants.VENDOR_SIGNUP);
        hashMap.put(NetworkConstants.KEY_PARAM_TYPE, NetworkConstants.KEY_EMAIL);
        hashMap.put(NetworkConstants.KEY_PARAM_VALUE, email);
        return hashMap;
    }

    String getEmailId() {
        return DataManager.getInstance().getEmail();
    }

    public String getPhoneNumber() {
        return DataManager.getInstance().getPhoneNumber();
    }

    public String getName() {
        return DataManager.getInstance().getUserName();
    }
}
