package com.qd.user.ui.trackorder.orderdetail;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;

import org.json.JSONObject;

public class OrderDetailViewModel extends ViewModel {
    private RichMediatorLiveData<OrderDetailResponse> mOrderDetailsLiveData;
    private RichMediatorLiveData<OrderCancellationResponse> mOrderCancellationLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private OrderDetailRepository mRepo = new OrderDetailRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mOrderDetailsLiveData == null) {
            mOrderDetailsLiveData = new RichMediatorLiveData<OrderDetailResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mOrderCancellationLiveData == null) {
            mOrderCancellationLiveData = new RichMediatorLiveData<OrderCancellationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

    }

    /**
     * This method gives the login live data object to {@link OrderDetailFragment}
     *
     * @return {@link #mOrderDetailsLiveData}
     */
    public RichMediatorLiveData<OrderDetailResponse> getOrderDetailsLiveData() {
        return mOrderDetailsLiveData;
    }

    /**
     * This method gives the login live data object to {@link OrderDetailFragment}
     *
     * @return {@link #mOrderCancellationLiveData}
     */
    public RichMediatorLiveData<OrderCancellationResponse> getOrderCancellationLiveData() {
        return mOrderCancellationLiveData;
    }


    public void getOrderDetail(String orderId) {
        mRepo.getOrderDetails(orderId, mOrderDetailsLiveData);
    }

    public void cancelOrder(JSONObject request) {
        mOrderCancellationLiveData.setLoadingState(true);
        mRepo.cancelOrder(request, mOrderCancellationLiveData);
    }

    void getReceivedOrderDetail(String orderId) {
        mRepo.getReceivedOrderDetails(orderId, mOrderDetailsLiveData);
    }

    void autoCancel(String id) {
        mOrderCancellationLiveData.setLoadingState(true);
        mRepo.autoCancelOrder(id, mOrderCancellationLiveData);
    }
}
