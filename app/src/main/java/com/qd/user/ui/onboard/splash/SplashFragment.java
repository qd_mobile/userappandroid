package com.qd.user.ui.onboard.splash;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.QuickDelivery;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.qd.user.base.NetworkCallback.NO_INTERNET;

public class SplashFragment extends BaseFragment implements AppUtils.ISuccess {

    private static final int FORCE_UPDATE_AVAILABLE = 42208;
    private static final int NORMAL_UPDATE_AVAILABLE = 42210;
    private static final int APP_VERSION_GREATER = 42211;

    private Unbinder unbinder;
    @BindView(R.id.iv_app_logo)
    ImageView ivAppLogo;
    @BindView(R.id.tv_ver_no)
    TextView tvVerNo;

    /**
     * A {@link SplashViewModel} object to handle all the actions and business logic of splash
     */
    private SplashViewModel mSplashViewModel;
    private ISplashHost mHost;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onOkViewClicked() {

    }

    public interface ISplashHost {

        void enterFullScreenMode();

        void openWelcomeFragment(Parcelable parcelable);

        void openHomeScreen();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ISplashHost) {
            mHost = (ISplashHost) context;
        } else
            throw new IllegalStateException("host must implement ISplashHost");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mHost.enterFullScreenMode();

        //Inflate the layout for this fragment_order_status
        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Initialize view model
        mSplashViewModel = new ViewModelProvider(this).get(SplashViewModel.class);
        mSplashViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        checkForUpdates();

        //animate logo
        animateLogo();
        observeLiveData();

    }


    private void animateLogo() {
        Animation startAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
        ivAppLogo.startAnimation(startAnimation);
    }

    private void observeLiveData() {
        mSplashViewModel.getVersionLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            if (commonResponse != null) {
                navigateNextFromSplash();
            }
        });
    }


    private void checkForUpdates() {
        try {
            DataManager.getInstance().setVersionName(QuickDelivery.getInstance().getPackageManager()
                    .getPackageInfo(QuickDelivery.getInstance().getPackageName(), 0).versionName);
            tvVerNo.setText(String.format("V_%s", AppUtils.getInstance().getAppVersionName()));

            mSplashViewModel.checkForUpdates(
                    DataManager.getInstance().getVersionName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onFailure(FailureResponse failureResponse) {
        switch (failureResponse.getErrorCode()) {
            case FORCE_UPDATE_AVAILABLE:
                openUpdateDialog(false);
                break;
            case NORMAL_UPDATE_AVAILABLE:
            case APP_VERSION_GREATER:
                openUpdateDialog(true);
                break;
            case NO_INTERNET:
                checkForUpdates();
        }

    }


    private void openUpdateDialog(boolean isNormal) {
        if (getContext() != null) {

            final Dialog successDialog = new Dialog(getContext(), R.style.customDialog);
            successDialog.setContentView(R.layout.dialog_forgot_password);
            TextView tvMessage = successDialog.findViewById(R.id.tv_message);
            Button btnOk = successDialog.findViewById(R.id.btn_ok);
            Button btnCancel = successDialog.findViewById(R.id.btn_cancel);

            btnCancel.setText(R.string.s_skip);
            btnOk.setText(R.string.s_update);

            tvMessage.setText(getString(R.string.s_newer_version_is_available_please_update));
            successDialog.setCancelable(false);

            if (isNormal) {
                successDialog.findViewById(R.id.btn_cancel).setVisibility(View.VISIBLE);
            }

            ImageView ivTick = successDialog.findViewById(R.id.iv_resent_link);
            ivTick.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_password_changed));
            AppUtils.getInstance().dimDialogBackground(successDialog);
            successDialog.show();

            btnOk.setOnClickListener(view -> {
                successDialog.dismiss();
                openPlayStore();
            });

            btnCancel.setOnClickListener(view -> {
                successDialog.dismiss();
                navigateNextFromSplash();
            });
        }

    }

    private void openPlayStore() {
        final String appPackageName = QuickDelivery.getInstance().getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        checkForUpdates();
    }


    private void navigateNextFromSplash() {
        switch (mSplashViewModel.proceedFromSplash()) {
            case AppConstants.NAVIGATE_TO_HOME:
                mHost.openHomeScreen();
                break;
            case AppConstants.NAVIGATE_TO_WELCOME:
                mHost.openWelcomeFragment(null);

        }

    }
}
