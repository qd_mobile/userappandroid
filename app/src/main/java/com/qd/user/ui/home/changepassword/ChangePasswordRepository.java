package com.qd.user.ui.home.changepassword;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.HashMap;

class ChangePasswordRepository {
    public void hitChangePasswordApi(String oldPassword, String newPassword, String confirmNewPassword, final RichMediatorLiveData<CommonResponse> mChangePasswordLiveData) {
        DataManager.getInstance().hitChangePasswordApi(createChangePasswordObject(oldPassword, newPassword, confirmNewPassword)).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                mChangePasswordLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mChangePasswordLiveData.setFailure(failureResponse);

            }

            @Override
            public void onError(Throwable t) {
                mChangePasswordLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> createChangePasswordObject(String oldPassword, String newPassword, String confirmNewPassword) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_TYPE, AppConstants.CASH_CLIENT);
        hashMap.put(NetworkConstants.KEY_OLD_PASSWORD, oldPassword);
        hashMap.put(NetworkConstants.KEY_NEW_PASSWORD, newPassword);
        hashMap.put(NetworkConstants.KEY_CONFIRM_NEW_PASSWORD, confirmNewPassword);
        return hashMap;

    }


}
