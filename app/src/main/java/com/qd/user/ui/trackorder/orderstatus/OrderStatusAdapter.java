package com.qd.user.ui.trackorder.orderstatus;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.orderdetail.DriverTrackingstatus;
import com.qd.user.utils.DateFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderStatusAdapter extends RecyclerView.Adapter<OrderStatusAdapter.OrderStatusAdapterViewHolder> {

    private List<com.qd.user.model.orderdetail.DriverTrackingstatus> mStatusList;
    private ArrayList<String> mCompleteStatusList;


    OrderStatusAdapter(List<DriverTrackingstatus> statusList, ArrayList<String> completeStatusList) {
        mStatusList = statusList;
        mCompleteStatusList = completeStatusList;
    }

    @NonNull
    @Override
    public OrderStatusAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_order_status, viewGroup, false);
        return new OrderStatusAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderStatusAdapterViewHolder viewHolder, int i) {

        viewHolder.tvOrderStatus.setText(mCompleteStatusList.get(i));
        viewHolder.viewLineOrderStatus.setVisibility(View.VISIBLE);


        if (mStatusList.size() <= mCompleteStatusList.size() && mStatusList.size() > i) {
            switch (mStatusList.get(i).getStatus()) {
                case AppConstants.DriverTrackingStatus.UNALIGNED:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.ORDER_CREATED)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.ACCEPT_ORDER:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.DRIVER_ACEEPTED)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.DRIVE_TO_PICKEDUP:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.DRIVING_TO_PICKUP)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.REACHED_AT_PICKUP:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.ARRIVED_AT_PICKUP)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.ORDER_PICKEDUP:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.ORDER_PICKEDUP)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.DRIVE_TO_DROP_LOCATION:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.DRIVING_TO_DROP_OFF)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.REACHED_TO_DROP_LOCATION:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.ARRIVED_AT_DROP_OFF)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.DROP_COMPLETE:
                    if (mCompleteStatusList.get(i).equals(AppConstants.OrderTrackingStatus.ORDER_DELIVERED)) {
                        markStatusDone(viewHolder, i);
                    }
                    break;
                case AppConstants.DriverTrackingStatus.DELIVERY_ATTEMPT_FAILED:
                case AppConstants.DriverTrackingStatus.CANCELLED:
                    viewHolder.ivOrderStatus.setImageDrawable(viewHolder.ivOrderStatus.getResources().getDrawable(R.drawable.ic_order_failed_cross));
                    viewHolder.ivOrderStatus.setBackground(viewHolder.ivOrderStatus.getResources().getDrawable(R.drawable.drawable_ci_red));
                    viewHolder.tvOrderStatus.setText(mStatusList.get(i).getStatus());
                    viewHolder.tvOrderStatusTime.setText(String.format("%s, %s", DateFormatter.getInstance().getFormattedTime(mStatusList.get(i).getTime()),
                            DateFormatter.getInstance().getFormattedDate(mStatusList.get(i).getTime())));

                    if (mCompleteStatusList.size() > i) {
                        mCompleteStatusList.subList(i, mCompleteStatusList.size()).clear();
                        notifyDataSetChanged();
                    }
                    break;
                case AppConstants.DriverTrackingStatus.CASHBACK_INITIATED:
                    mCompleteStatusList.add(AppConstants.DriverTrackingStatus.CASHBACK_INITIATED);
                    mCompleteStatusList.add(AppConstants.DriverTrackingStatus.CASHBACK_RETURNED);
                    notifyDataSetChanged();
                case AppConstants.DriverTrackingStatus.CASHBACK_RETURNED:
                    viewHolder.tvOrderStatus.setText(mStatusList.get(i).getStatus());
                    markStatusDone(viewHolder, i);
                    break;
                case AppConstants.OrderStatus.RETURN_INITIATED:
                    mCompleteStatusList.add(AppConstants.OrderStatus.RETURN_INITIATED);
                    mCompleteStatusList.add(AppConstants.OrderStatus.RETURNED);
                    notifyDataSetChanged();
                case AppConstants.OrderStatus.RETURNED:
                    viewHolder.tvOrderStatus.setText(mStatusList.get(i).getStatus());
                    markStatusDone(viewHolder, i);
                    break;



            }
        }

        if (mCompleteStatusList.size() == i + 1) {
            viewHolder.viewLineOrderStatus.setVisibility(View.GONE);
        }
    }

    private void markStatusDone(OrderStatusAdapterViewHolder viewHolder, int i) {
        viewHolder.tvOrderStatus.setTextColor(viewHolder.tvOrderStatus.getResources().getColor(R.color.colorTextBlack));
        viewHolder.ivOrderStatus.setImageDrawable(viewHolder.tvOrderStatus.getResources().getDrawable(R.drawable.ic_welcome_language_select));
        viewHolder.ivOrderStatus.setScaleType(ImageView.ScaleType.CENTER);
        viewHolder.tvOrderStatusTime.setText(String.format("%s, %s", DateFormatter.getInstance().getFormattedTime(mStatusList.get(i).getTime()),
                DateFormatter.getInstance().getFormattedDate(mStatusList.get(i).getTime())));
    }


    @Override
    public int getItemCount() {
        return mCompleteStatusList.size();
    }

    class OrderStatusAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_order_status)
        ImageView ivOrderStatus;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_order_status_time)
        TextView tvOrderStatusTime;
        @BindView(R.id.view_line_order_status)
        View viewLineOrderStatus;

        OrderStatusAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

