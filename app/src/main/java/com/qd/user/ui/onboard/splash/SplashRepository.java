package com.qd.user.ui.onboard.splash;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.HashMap;

class SplashRepository {
    String navigateFromSplash() {
        if (DataManager.getInstance().isUserVerified()) {
            return AppConstants.NAVIGATE_TO_HOME;
        } else
            return AppConstants.NAVIGATE_TO_WELCOME;

    }

    void hitVersionApi(final RichMediatorLiveData<CommonResponse> versionLiveData, String versionName) {
        DataManager.getInstance().hitVersionApi(createRequestPayload(versionName)).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                versionLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                versionLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                versionLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> createRequestPayload(String versionName) {
        HashMap<String, String> requestMap = new HashMap<>();
        requestMap.put(NetworkConstants.VERSION_NUMBER, versionName);
        requestMap.put(NetworkConstants.APP_TYPE, NetworkConstants.USER);
        requestMap.put(NetworkConstants.KEY_PLATFORM, NetworkConstants.ANDROID);
        return requestMap;
    }
}
