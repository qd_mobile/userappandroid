package com.qd.user.ui.home.invoice;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.invoicedetails.InvoiceDetailsResponse;

import java.util.HashMap;

class InvoiceRepository {

    void getInvoiceDetails(final RichMediatorLiveData<InvoiceDetailsResponse> invoiceDetailsLiveData, String invoiceId, int page) {
        DataManager.getInstance().getInvoiceDetails(createRequestPayload(invoiceId, page)).enqueue(new NetworkCallback<InvoiceDetailsResponse>() {
            @Override
            public void onSuccess(InvoiceDetailsResponse invoiceDetailsResponse) {
                invoiceDetailsLiveData.setValue(invoiceDetailsResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                invoiceDetailsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                invoiceDetailsLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> createRequestPayload(String invoiceId, int page) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put("invoiceId", invoiceId);
        mRequestMap.put("page", page);
        mRequestMap.put("limit", 10);

        return mRequestMap;
    }
}
