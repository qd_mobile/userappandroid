package com.qd.user.ui.home.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.ui.home.editprofile.EditProfileViewModel;
import com.qd.user.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProfileFragment extends BaseFragment implements SavedPlacesAdapter.ISavedPlaces, AppUtils.IConfirmationDialog {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_edit_info)
    ImageView ivEditInfo;
    @BindView(R.id.tv_phone_number)
    TextView tvPhoneNumber;
    @BindView(R.id.tv_label_contact_info)
    TextView tvLabelContactInfo;
    @BindView(R.id.tv_label_email)
    TextView tvLabelEmail;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_label_company_contact)
    TextView tvLabelCompanyContact;
    @BindView(R.id.tv_company_contact)
    TextView tvCompanyContact;
    @BindView(R.id.tv_label_personal_contact)
    TextView tvLabelPersonalContact;
    @BindView(R.id.tv_personal_contact)
    TextView tvPersonalContact;
    @BindView(R.id.view_email_divider)
    View viewEmailDivider;
    @BindView(R.id.tv_label_saved_places)
    TextView tvLabelSavedPlaces;
    @BindView(R.id.tv_error_no_saved_places)
    TextView tvErrorNoSavedPlaces;
    @BindView(R.id.rv_saved_places)
    RecyclerView rvSavedPlaces;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.tv_change)
    TextView tvChange;
    @BindView(R.id.tv_label_password)
    TextView tvLabelPassword;
    @BindView(R.id.nsv_profile)
    NestedScrollView nsvProfile;


    private IProfileHost mHost;
    private EditProfileViewModel mEditProfileViewModel;
    private ArrayList<Result> mSavedPlacesList;
    private SavedPlacesAdapter mAdapter;
    private String mSavedAddressId;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditProfileViewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);
        mEditProfileViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

        showProgressDialog();
        mEditProfileViewModel.getProfileInfo();
        mEditProfileViewModel.getSavedPlaces();

    }

    private void observeLiveData() {
        mEditProfileViewModel.getSavedPlacesLiveData().observe(getViewLifecycleOwner(), savedAddressResponse -> {
            hideProgressDialog();
            if (savedAddressResponse != null) {
                if (savedAddressResponse.getResult().size() > 0)
                    showSavedAddressList(savedAddressResponse.getResult());
                else {
                    showNoDataFoundData();
                }
            }
        });

        mEditProfileViewModel.getDeleteSavedPlaceLiveData().observe(getViewLifecycleOwner(), deleteSavedPlaceResponse -> {
            if (deleteSavedPlaceResponse != null) {
                hideProgressDialog();
                removeDeletedAddress(deleteSavedPlaceResponse.getResult().getId());
            }
        });

        mEditProfileViewModel.getProfileLiveData().observe(getViewLifecycleOwner(), profileResponse -> {
            hideProgressDialog();
            if (profileResponse != null) {
                nsvProfile.setVisibility(View.VISIBLE);
                setUserProfileInfo(profileResponse.getResult());
            }
        });
    }


    private void showNoDataFoundData() {
        tvErrorNoSavedPlaces.setVisibility(View.VISIBLE);
        rvSavedPlaces.setVisibility(View.GONE);
    }

    private void showSavedAddressList(List<Result> result) {

        mSavedPlacesList.clear();
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).getIsFavourite()) {
                mSavedPlacesList.add(result.get(i));
            }
        }
        if (mSavedPlacesList.size() > 0) {
            rvSavedPlaces.setVisibility(View.VISIBLE);
            tvErrorNoSavedPlaces.setVisibility(View.GONE);
        } else {
            rvSavedPlaces.setVisibility(View.GONE);
            tvErrorNoSavedPlaces.setVisibility(View.VISIBLE);
        }
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Remove selected saved places from saved places listing
     *
     * @param id of saved place that user wants to remove from the listing..
     */
    private void removeDeletedAddress(String id) {
        for (int i = 0; i < mSavedPlacesList.size(); i++) {
            if (id.equals(mSavedPlacesList.get(i).getId())) {
                mSavedPlacesList.remove(i);
                mAdapter.notifyItemRemoved(i);
                break;
            }
        }
        if (mSavedPlacesList.size() < 1) {
            tvErrorNoSavedPlaces.setVisibility(View.VISIBLE);
            rvSavedPlaces.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_profile);
        mSavedPlacesList = new ArrayList<>();

        setUpAdapter();
    }

    private void setUpAdapter() {
        mAdapter = new SavedPlacesAdapter(mSavedPlacesList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvSavedPlaces.setLayoutManager(layoutManager);
        rvSavedPlaces.setAdapter(mAdapter);
    }


    private void setUserProfileInfo(com.qd.user.model.profile.Result result) {

        if (mEditProfileViewModel.getUserType().equals(AppConstants.userType.ONLY_CASH_CLIENT)) {
            tvName.setText(result.getVendorList().getName());
            tvPhoneNumber.setText(String.format("%s %s", mEditProfileViewModel.getCountryCode(), mEditProfileViewModel.getPhoneNumber()));
            tvEmail.setText(result.getVendorList().getEmail());

            tvLabelCompanyContact.setVisibility(View.GONE);
            tvLabelPersonalContact.setVisibility(View.GONE);
            tvCompanyContact.setVisibility(View.GONE);
            tvPersonalContact.setVisibility(View.GONE);
            tvLabelPassword.setVisibility(View.GONE);
            tvChange.setVisibility(View.GONE);
        } else {
            tvName.setText(result.getVendorList().getBusinessName());
            tvPhoneNumber.setText(result.getVendorList().getCompanyProfile());
            tvPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            ivEditInfo.setVisibility(View.GONE);

            tvLabelEmail.setText(R.string.s_address);
            tvLabelEmail.setAllCaps(true);
            tvEmail.setText(result.getVendorList().getFullBusinessAddress().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

            tvCompanyContact.setText(String.format("%s %s", result.getVendorList().getCompany().getContact().getCcPersonal(), result.getVendorList().getCompany().getContact().getPersonal()));
            tvPersonalContact.setText(String.format("%s %s", result.getVendorList().getContact().getCcPersonal(), result.getVendorList().getContact().getPersonal()));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_edit_info, R.id.iv_back, R.id.tv_change})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_edit_info:
                mHost.moveToEditProfileFragment();
                break;
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.tv_change:
                mHost.openChangePasswordFragment();

        }
    }


    public static ProfileFragment getInstance() {
        return new ProfileFragment();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IProfileHost) {

            mHost = (IProfileHost) context;
        } else
            throw new IllegalStateException("Host must implement IProfileHost");
    }

    @Override
    public void onDeleteSavedPlace(String savedAddressId) {
        mSavedAddressId = savedAddressId;
        AppUtils.getInstance().openConfirmationDialog(getActivity(), getString(R.string.s_are_you_sure_you_want_to_delete_saved_place), this);
    }

    @Override
    public void onEditSavedPlaces(Result result) {
        mHost.openAdvanceSearchFragment(result);
    }

    public void updateSavedAddressList(Result result) {
        for (int i = 0; i < mSavedPlacesList.size(); i++) {
            if (result.getId().equals(mSavedPlacesList.get(i).getId())) {
                mSavedPlacesList.set(i, result);
                mAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    @Override
    public void onYesViewClicked() {
        showProgressDialog();
        mEditProfileViewModel.onDeleteSavedAddress(mSavedAddressId);
    }

    public void updateProfileFragment() {
        tvName.setText(mEditProfileViewModel.getUserName());
        tvPhoneNumber.setText(String.format("%s %s", mEditProfileViewModel.getCountryCode(), mEditProfileViewModel.getPhoneNumber()));
        tvEmail.setText(mEditProfileViewModel.getEmail());
    }

    public interface IProfileHost {

        void moveToEditProfileFragment();

        void onBackPressed();

        void openAdvanceSearchFragment(Result result);

        void openChangePasswordFragment();
    }

}
