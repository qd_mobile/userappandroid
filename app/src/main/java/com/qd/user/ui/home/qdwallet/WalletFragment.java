package com.qd.user.ui.home.qdwallet;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.wallet.Datum;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.PaginationListener;
import com.qd.user.utils.customviews.DecimalDigitsInputFilter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WalletFragment extends BaseFragment implements PaginationListener.PaginationListenerCallbacks {


    private Unbinder unbinder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_balance_amount)
    TextView tvBalanceAmount;
    @BindView(R.id.tv_label_available_balance)
    TextView tvLabelAvailableBalance;
    @BindView(R.id.add_money)
    Button addMoney;
    @BindView(R.id.cl_add_amount)
    ConstraintLayout clAddAmount;
    @BindView(R.id.tv_label_recent_transactions)
    TextView tvLabelRecentTransactions;
    @BindView(R.id.rv_transactions)
    RecyclerView rvTransactions;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;

    private WalletTransactionAdapter mAdapter;
    private ArrayList<Object> mWalletTransactionsList;
    private IWalletHost mHost;
    private WalletViewModel mWalletViewModel;
    private int mNextPage;
    private PaginationListener mPaginationListener;
    private boolean isRefreshing;
    private LinearLayoutManager layoutManager;

    public static WalletFragment getInstance() {
        return new WalletFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_qd_wallet, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWalletViewModel = new ViewModelProvider(this).get(WalletViewModel.class);
        mWalletViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        showShimmerEffect();
        mWalletViewModel.getWalletTransactionHistory(mNextPage);
        observeLiveData();
    }

    private void observeLiveData() {
        mWalletViewModel.getWalletLiveData().observe(getViewLifecycleOwner(), walletTransactionHistoryResponse -> {
            hideShimmerEffect();
            hideProgressDialog();

            if (walletTransactionHistoryResponse != null) {

                if (walletTransactionHistoryResponse.getResult().getVendorWalletAmount() != null)
                    tvBalanceAmount.setText(String.format(Locale.getDefault(), "%6.3f KWD", walletTransactionHistoryResponse.getResult().getVendorWalletAmount()));
                else {
                    tvBalanceAmount.setText(String.format(Locale.getDefault(), "%d KWD", 0));
                }


                if (walletTransactionHistoryResponse.getResult().getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (walletTransactionHistoryResponse.getResult().getData() != null && walletTransactionHistoryResponse.getResult().getData().size() > 0) {
                    if (mWalletTransactionsList != null && mWalletTransactionsList.size() > 0) {
                        mAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mWalletTransactionsList.clear();
                        }
                        setResponseToAdapter(walletTransactionHistoryResponse.getResult().getData());
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mWalletTransactionsList.clear();
                        }

                        setResponseToAdapter(walletTransactionHistoryResponse.getResult().getData());
                        rvTransactions.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }
            }

        });

        /*mWalletViewModel.getWalletRechargeLiveData().observe(getViewLifecycleOwner(), new Observer<WalletRechargeResponse>() {
            @Override
            public void onChanged(@Nullable WalletRechargeResponse walletRechargeResponse) {
                if (walletRechargeResponse != null) {
                    mHost.openOnlinePaymentsFragment(walletRechargeResponse.getResult());
                }
            }
        });*/
    }

    private void setResponseToAdapter(Collection<Datum> datum) {
        mWalletTransactionsList.addAll(datum);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_qd_wallet);
        mWalletTransactionsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);
        rvTransactions.addOnScrollListener(mPaginationListener);

        setRefreshLayoutListener();

        setUpAdapter();
    }

    /**
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        swipeRefresh.setOnRefreshListener(
                () -> {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    refreshList();
                    mPaginationListener.setFetchingStatus(false);

                }
        );
    }

    private void refreshList() {
        isRefreshing = true;
        mNextPage = 1;
        mWalletViewModel.getWalletTransactionHistory(mNextPage);
    }


    private void setUpAdapter() {
        mAdapter = new WalletTransactionAdapter(mWalletTransactionsList);
        rvTransactions.setLayoutManager(layoutManager);
        rvTransactions.setAdapter(mAdapter);
        rvTransactions.scheduleLayoutAnimation();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * Hides recycler view and and show no data found  message if data is not available
     */
    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        rvTransactions.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        hideShimmerEffect();
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        hideProgressDialog();
        clAddAmount.setVisibility(View.GONE);
        tvLabelRecentTransactions.setVisibility(View.GONE);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvTransactions.setVisibility(View.GONE);

    }

    /**
     * Hides shimmer view and set views accordingly
     */
    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerViewContainer.stopShimmer();
        shimmerViewContainer.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);

    }

    /**
     * Shows shimmer view and set views accordingly
     */
    private void showShimmerEffect() {
        swipeRefresh.setVisibility(View.GONE);
        shimmerViewContainer.setVisibility(View.VISIBLE);
        shimmerViewContainer.startShimmer();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IWalletHost) {
            mHost = (IWalletHost) context;
        } else throw new IllegalStateException("Host must implement IWalletHost");
    }

    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        mHost.onBackPressed();
    }

    @OnClick({R.id.iv_back, R.id.add_money})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                break;
            case R.id.add_money:
                openDialogToAddMoney();
                break;
        }
    }

    private void openDialogToAddMoney() {
        if (getContext() != null) {
            final Dialog addMoneyDialog = new Dialog(getContext(), R.style.customDialog);
            addMoneyDialog.setContentView(R.layout.layout_add_money);

            AppUtils.getInstance().dimDialogBackground(addMoneyDialog);

            final EditText etMoney = addMoneyDialog.findViewById(R.id.et_money);
            final TextView tvError = addMoneyDialog.findViewById(R.id.tv_error_money);
            final Button btnSubmit = addMoneyDialog.findViewById(R.id.btn_submit);
            final Button btnCancel = addMoneyDialog.findViewById(R.id.btn_cancel);

            etMoney.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(4, 3)});

            addMoneyDialog.show();

            btnSubmit.setOnClickListener(v -> {
                if (etMoney.getText().toString().isEmpty()) {
                    tvError.setVisibility(View.VISIBLE);
                    shakeLayout(tvError);
                    tvError.setText(R.string.s_enter_the_money_to_be_added);
                } else {
                    tvError.setVisibility(View.GONE);
                    hideKeyboard();
                    mHost.openOnlinePaymentsFragment(createRequestForWalletRecharge(etMoney.getText().toString()));
                    //mWalletViewModel.rechargeWallet(etMoney.getText().toString());
                }
                addMoneyDialog.dismiss();

            });

            btnCancel.setOnClickListener(v -> addMoneyDialog.dismiss());
        }
    }

    private InitializePaymentRequest createRequestForWalletRecharge(String amount) {
        InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
        initializePaymentRequest.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
        initializePaymentRequest.setTransactionFor(AppConstants.TransactionFor.WALLET_RECHARGE);

        return initializePaymentRequest;
    }

    /**
     * Hits a network call for data on next page, if present
     */
    @Override
    public void loadMoreItems() {
        mAdapter.showProgress();
        mWalletViewModel.getWalletTransactionHistory(mNextPage);
    }

    public void refreshWalletAmount() {
        showProgressDialog();
        refreshList();
    }

    public interface IWalletHost {

        void onBackPressed();

        void openOnlinePaymentsFragment(InitializePaymentRequest result);
    }
}

