package com.qd.user.ui.home.myorders;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.ui.home.createdorder.OrderCreatedFragment;
import com.qd.user.ui.home.recievedorder.OrderReceivedFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyOrdersFragment extends BaseFragment implements OrderCreatedFragment.ICreatedOrderHost, OrderReceivedFragment.IReceivedOrderHost, ViewPager.OnPageChangeListener {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tl_location)
    TabLayout tlLocation;
    @BindView(R.id.vp_select_location)
    ViewPager vpSelectLocation;
    private Unbinder unbinder;
    private IMyOrdersHost mHost;
    private OrderCreatedFragment mCreatedOrderFragment;
    private OrderReceivedFragment mOrderReceivedFragment;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_location, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected void initViewsAndVariables() {
        mOrderReceivedFragment = OrderReceivedFragment.getInstance();
        mCreatedOrderFragment = OrderCreatedFragment.getInstance();
        tvTitle.setText(R.string.s_my_orders);
        ivNotification.setVisibility(View.VISIBLE);
        ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
        setUpViewPager();
    }

    private void setUpViewPager() {
        MyOrdersViewPagerAdapter mAdapter = new MyOrdersViewPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(mCreatedOrderFragment, getString(R.string.s_created));
        mAdapter.addFragment(mOrderReceivedFragment, getString(R.string.s_received));
        vpSelectLocation.setAdapter(mAdapter);
        tlLocation.setupWithViewPager(vpSelectLocation);
        vpSelectLocation.setOffscreenPageLimit(2);

        vpSelectLocation.addOnPageChangeListener(this);
    }

    public static MyOrdersFragment getInstance() {
        return new MyOrdersFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IMyOrdersHost) {
            mHost = (IMyOrdersHost) context;
        } else throw new IllegalStateException("Host must implement IMyOrdersHost");
    }

    @OnClick({R.id.iv_back, R.id.iv_notification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.iv_notification:
                setFilters();
                break;


        }
    }


    @Override
    public void trackCurrentOrder(String id, boolean isFromReceived) {
        mHost.trackCurrentOrder(id, isFromReceived);
    }

    @Override
    public void openBusinessOrderDetails(String orderId) {
        mHost.openBusinessOrderDetail(orderId);
    }

    @Override
    public void openOrderDetailFragment(String orderId, boolean isOrderReceived) {
        mHost.openOrderDetailFragment(orderId, isOrderReceived);
    }

    public void updateOrders() {
        mCreatedOrderFragment.refreshList();
    }

    /**
     * set views for filters, based on which fragment is currently visible and switches the
     * filter and cross icon accordingly
     */
    private void setFilters() {
        switch (vpSelectLocation.getCurrentItem()) {
            case 0:
                if (mCreatedOrderFragment.showOrHideFilter()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;
            case 1:
                if (mOrderReceivedFragment.showOrHideFilter()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;

        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        switch (i) {
            case 0:
                if (mCreatedOrderFragment.isFilterApplied()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;
            case 1:
                if (mOrderReceivedFragment.isFilterApplied()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }


    public interface IMyOrdersHost {

        void onBackPressed();

        void setDrawerLocked(boolean b);

        void openOrderDetailFragment(String orderId, boolean isOrderReceived);

        void trackCurrentOrder(String id, boolean isFromReceived);

        void openBusinessOrderDetail(String orderId);
    }

}
