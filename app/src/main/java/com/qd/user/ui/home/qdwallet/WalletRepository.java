package com.qd.user.ui.home.qdwallet;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;
import com.qd.user.model.payments.wallet.WalletTransactionHistoryResponse;

import java.util.HashMap;

class WalletRepository {

    void getWalletTransactionHistory(final RichMediatorLiveData<WalletTransactionHistoryResponse> walletLiveData, int nextPage) {
        DataManager.getInstance().getWalletTransactionHistory(createRequestPayload(nextPage)).enqueue(new NetworkCallback<WalletTransactionHistoryResponse>() {
            @Override
            public void onSuccess(WalletTransactionHistoryResponse walletTransactionHistoryResponse) {
                walletLiveData.setLoadingState(false);
                walletLiveData.setValue(walletTransactionHistoryResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                walletLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                walletLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> createRequestPayload(int nextPage) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put("vendorId", DataManager.getInstance().getUserId());
        mRequestMap.put("transactionFrom", AppConstants.PaymentMode.WALLET);
        mRequestMap.put("page", nextPage);
        mRequestMap.put("limit", 10);

        return mRequestMap;
    }

    void rechargeWallet(final RichMediatorLiveData<WalletRechargeResponse> walletRechargeLiveData, String amount) {
       /* DataManager.getInstance().rechargeWallet(createRequestPayloadForRecharge(amount)).enqueue(new NetworkCallback<WalletRechargeResponse>() {
            @Override
            public void onSuccess(WalletRechargeResponse walletRechargeResponse) {
                walletRechargeLiveData.setValue(walletRechargeResponse);
                walletRechargeLiveData.setLoadingState(false);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                walletRechargeLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                walletRechargeLiveData.setError(t);
            }
        });*/
    }

    private HashMap<String, Object> createRequestPayloadForRecharge(String amount) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put(NetworkConstants.VENDOR_ID, DataManager.getInstance().getUserId());
        mRequestMap.put(NetworkConstants.TRANSACTION_FOR, AppConstants.TransactionFor.WALLET_RECHARGE);
        mRequestMap.put(NetworkConstants.AMOUNT, Double.valueOf(amount));

        return mRequestMap;
    }
}
