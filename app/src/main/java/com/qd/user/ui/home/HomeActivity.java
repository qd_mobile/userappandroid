package com.qd.user.ui.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.navigation.NavigationView;
import com.qd.user.QuickDelivery;
import com.qd.user.R;
import com.qd.user.base.BaseActivity;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.ui.createorder.CreateOrderActivity;
import com.qd.user.ui.createorder.CreateOrderFragment;
import com.qd.user.ui.createorder.selectlocation.advancedsearch.AdvancedSearchFragment;
import com.qd.user.ui.home.applyvendor.BecomeAVendorFragment;
import com.qd.user.ui.home.changepassword.ChangePasswordFragment;
import com.qd.user.ui.home.contactus.ContactUsFragment;
import com.qd.user.ui.home.dashboard.DashBoardFragment;
import com.qd.user.ui.home.editprofile.EditProfileFragment;
import com.qd.user.ui.home.editprofile.EditProfileViewModel;
import com.qd.user.ui.home.invoice.InvoiceDetailsFragment;
import com.qd.user.ui.home.myorders.MyOrdersFragment;
import com.qd.user.ui.home.notifications.NotificationsFragment;
import com.qd.user.ui.home.onlinepayment.OnlinePaymentFragment;
import com.qd.user.ui.home.paymenthistory.PaymentHistoryFragment;
import com.qd.user.ui.home.profile.ProfileFragment;
import com.qd.user.ui.home.qdwallet.WalletFragment;
import com.qd.user.ui.home.settings.SettingsFragment;
import com.qd.user.ui.home.settings.StaticPageFragment;
import com.qd.user.ui.onboard.OnBoardActivity;
import com.qd.user.ui.trackorder.TrackOrderActivity;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.LocaleManager;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

import static com.qd.user.constants.AppConstants.Timings.MAX_HOUR_LIMIT;
import static com.qd.user.constants.AppConstants.Timings.MAX_MIN_LIMIT;
import static com.qd.user.constants.AppConstants.Timings.MIN_HOUR_LIMIT;
import static com.qd.user.constants.AppConstants.Timings.MIN_MIN_LIMIT;


public class
HomeActivity extends BaseActivity implements DashBoardFragment.IDashboardHost, NavigationView.OnNavigationItemSelectedListener, ProfileFragment.IProfileHost,
        DrawerLocked, EditProfileFragment.IEditProfileHost, ChangePasswordFragment.IChangePasswordHost, AdvancedSearchFragment.IAdvancedSearchHost,
        MyOrdersFragment.IMyOrdersHost, BecomeAVendorFragment.IBecomeAVendorHost, AppUtils.IConfirmationDialog,
        SettingsFragment.ISettingsHost, StaticPageFragment.IStaticPageHost, ContactUsFragment.IContactUsHost, WalletFragment.IWalletHost, PaymentHistoryFragment.IPaymentHistoryHost,
        AppUtils.ISuccess, InvoiceDetailsFragment.IInvoiceDetailsHost, OnlinePaymentFragment.IOnlinePaymentHost, NotificationsFragment.INotificationsHost {


    @BindView(R.id.fl_container)
    FrameLayout flContainer;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_subtittle)
    TextView tvSubTitle;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_email_address)
    TextView tvEmailAddress;
    @BindView(R.id.tv_app_version)
    TextView tvAppVersion;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.tv_dashboard)
    TextView tvDashboard;
    @BindView(R.id.tv_my_orders)
    TextView tvMyOrders;
    @BindView(R.id.tv_qd_wallet)
    TextView tvQdWallet;
    @BindView(R.id.tv_notification)
    TextView tvNotification;
    @BindView(R.id.tv_unread_notifications)
    TextView tvUnreadNotifications;
    @BindView(R.id.cl_notification)
    ConstraintLayout clNotification;
    @BindView(R.id.tv_become_a_vendor)
    TextView tvBecomeAVendor;
    @BindView(R.id.tv_payment_history)
    TextView tvPaymentHistory;
    @BindView(R.id.tv_contact_us)
    TextView tvContactUs;
    @BindView(R.id.tv_settings)
    TextView tvSettings;
    @BindView(R.id.tv_logout)
    TextView tvLogout;
    public static int mNotificationCount;
    @BindView(R.id.tv_wallet_balance)
    TextView tvWalletBalance;

    private EditProfileViewModel mEditProfileViewModel;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;

    @Override
    protected void initViewsAndVariables() {
        tvTittle.setText(R.string.s_choose_service_type);
        ivBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_dashoboard_menu));
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_home;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEditProfileViewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);
        mEditProfileViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        initializeSocket();
        getIntentData();
        observeLiveData();
        setUserDetailOnSideMenu();
    }

    private void observeLiveData() {
        mEditProfileViewModel.getLogOutLiveData().observe(this, commonResponse -> {
            if (commonResponse != null) {
                mEditProfileViewModel.clearPreference();
                moveToOnBoard(AppConstants.ACTION_WELCOME);
            }
        });
    }

    @Override
    public void setLocale(String localeName) {
        LocaleManager.persist(this, localeName);
        recreate();
    }

    //set user name and email from preference to side menu
    private void setUserDetailOnSideMenu() {
        if (mEditProfileViewModel.getEmail() != null) {
            tvName.setText(mEditProfileViewModel.getUserName());
            tvEmailAddress.setText(mEditProfileViewModel.getEmail());
            tvLogout.setVisibility(View.VISIBLE);
        } else {
            tvLogout.setVisibility(View.GONE);
            tvName.setText(R.string.s_hii_guest);
            tvEmailAddress.setText(R.string.s_tap_to_login);
        }
        tvAppVersion.setText(String.format("V_%s", AppUtils.getInstance().getAppVersionName()));
    }

    private void initializeSocket() {
        mEditProfileViewModel.initializeSocket();
        mEditProfileViewModel.connectSocket();
    }

    /**
     * Get data from intent
     */
    private void getIntentData() {
        if (getSupportFragmentManager().findFragmentByTag(SettingsFragment.class.getName()) != null) {
            hideToolbar();
        }
        if (getIntent() != null && getIntent().hasExtra(AppConstants.ACTION_NAVIGATION)) {
            switch (getIntent().getStringExtra(AppConstants.ACTION_NAVIGATION)) {
                case AppConstants.ACTION_DASHBOARD:
                    openDashBoardFragment();
                    break;
                case AppConstants.Navigation.ONLINE_PAYMENT:
                    hideToolbar();
                    InitializePaymentRequest initializePaymentRequest = getIntent().getParcelableExtra(AppConstants.PAYMENT_REQUEST);
                    if (initializePaymentRequest != null) {
                        initializePaymentRequest.setAmount(BigDecimal.valueOf(initializePaymentRequest.getPrice()));
                    }

                    if (getIntent().getStringExtra(AppConstants.PAYMENT_URL) != null)
                        addFragment(R.id.fl_container, OnlinePaymentFragment.getInstance(initializePaymentRequest, getIntent().getStringExtra(AppConstants.PAYMENT_URL)), OnlinePaymentFragment.class.getName());
                    else
                        addFragment(R.id.fl_container, OnlinePaymentFragment.getInstance(initializePaymentRequest, null), OnlinePaymentFragment.class.getName());

                    break;
                case AppConstants.Navigation.ORDERS_LIST:
                    hideToolbar();
                    addFragment(R.id.fl_container, MyOrdersFragment.getInstance(), MyOrdersFragment.class.getName());
                    break;
                case AppConstants.Navigation.NOTIFICATION:
                    if (getSupportFragmentManager().getFragments().isEmpty()) {
                        hideToolbar();
                        addFragment(R.id.fl_container, NotificationsFragment.getInstance(), NotificationsFragment.class.getName());
                    } else
                        openNotifications();
                    break;
            }
        }
    }


    private void openDashBoardFragment() {
        ivNotification.setVisibility(View.VISIBLE);
        addFragment(R.id.fl_container, DashBoardFragment.getInstance(), DashBoardFragment.class.getName());
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            } else {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
        }
        return false;
       /* switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

        }
        return super.onOptionsItemSelected(item);*/
    }

    @Override
    public void updateStatusBarColour(int color) {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(color));
    }

    @Override
    public void navigateToBusinessOrderDetail(String orderId) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.BUSINESS_ORDER_DETAIL);
        startActivity(intent);
    }

    @Override
    public void navigateToOrderDetail(String orderId, boolean isOrderReceived) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        intent.putExtra(AppConstants.IS_ORDER_RECEIVED, isOrderReceived);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.ORDER_DETAIL);
        startActivity(intent);
    }

    @Override
    public void decreaseNotificationCount() {
        mNotificationCount--;
        setCounts(mNotificationCount, null);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
            if (fragment instanceof DashBoardFragment) {
                finishAffinity();
            } else {
                super.onBackPressed();
                setDrawerLocked(true);
                fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
                if (fragment instanceof DashBoardFragment) {
                    showToolbar();
                } else if (fragment instanceof WalletFragment) {
                    ((WalletFragment) fragment).refreshWalletAmount();
                } else {
                    hideToolbar();
                }
            }
        }
    }

    /**
     * Opens static page fragment for rendering urls like Terms & Conditions, About us
     * Based on title passed
     *
     * @param title String based on which url is to be rendered
     */
    @Override
    public void openStaticPage(String title) {
        addFragmentWithBackStack(R.id.fl_container, StaticPageFragment.getInstance(title), StaticPageFragment.class.getName());
    }


    /**
     * Opens {@link CreateOrderFragment} after
     * user has selected the order type on {@link DashBoardFragment}
     *
     * @param orderType Type of order user wants to create
     */
    @Override
    public void openCreateOrderFragment(String orderType) {
            Intent intent = new Intent(this, CreateOrderActivity.class);
            intent.putExtra(AppConstants.ORDER_TYPE, orderType);
            startActivity(intent);
    }

    private boolean validateTime() {
        Calendar calendar = Calendar.getInstance();
        Calendar minStartCalendar = Calendar.getInstance();
        Calendar maxStartCalendar = Calendar.getInstance();

        minStartCalendar.set(Calendar.HOUR_OF_DAY, MIN_HOUR_LIMIT);
        minStartCalendar.set(Calendar.MINUTE, MIN_MIN_LIMIT);
        maxStartCalendar.set(Calendar.HOUR_OF_DAY, MAX_HOUR_LIMIT);
        maxStartCalendar.set(Calendar.MINUTE, MAX_MIN_LIMIT);

        if (calendar.getTime().before(minStartCalendar.getTime()) ||
                calendar.getTime().after(maxStartCalendar.getTime())) {
            AppUtils.getInstance().openNoServiceDialog(this, getString(R.string.s_you_can_only_create_orders_between_8_00am_to_10_30pm));
            return false;
        }
        return true;
    }

    @Override
    public void navigateToCreateOrderScreen(boolean isForPickup, SaveAddressRequest completeAddress) {

    }

    @Override
    public void updateSavedAddress(Result result) {

    }


    @Override
    public void backToProfileFragment(Result result) {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (fragment instanceof ProfileFragment) {
            if (result != null)
                ((ProfileFragment) fragment).updateSavedAddressList(result);
            else
                ((ProfileFragment) fragment).updateProfileFragment();

        }
    }

    @Override
    public void backToRecipientDetails(SaveAddressRequest completeAddress) {

    }

    @Override
    public void openAddItemFragment(SaveAddressRequest createOrderRequest) {

    }


    @Override
    public void openAdvanceSearchFragment(Result result) {
        addFragmentWithBackStack(R.id.fl_container, AdvancedSearchFragment.getInstance(false, result, ProfileFragment.class.getName()), AdvancedSearchFragment.class.getName());
    }


    @Override
    public void setCounts(int notificationCount, BigDecimal walletAmount) {
        tvUnreadNotifications.setVisibility(View.VISIBLE);
        mNotificationCount = notificationCount;
        if (notificationCount > 0 && notificationCount < 10)
            tvUnreadNotifications.setText(String.valueOf(notificationCount));
        else if (notificationCount > 9)
            tvUnreadNotifications.setText(getString(R.string.s_not_count));
        else tvUnreadNotifications.setVisibility(View.GONE);

        if (walletAmount != null && walletAmount.compareTo(BigDecimal.valueOf(0d)) > 0) {
            tvWalletBalance.setText(String.format(Locale.getDefault(), "%.3f KWD", walletAmount));
        } else tvWalletBalance.setVisibility(View.GONE);

    }

    @Override
    public void navigateToFindingDriverFragment(String orderId) {
        Intent intent = new Intent(this, CreateOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.FINDING_DRIVER);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        startActivity(intent);
    }

    @Override
    public void navigateToOrderTrackingFragment(String orderId) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.TRACK_ORDER);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        startActivity(intent);
    }

    @Override
    public void navigateToBecomeAVendor() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, BecomeAVendorFragment.getInstance(), BecomeAVendorFragment.class.getName());
    }


    @Override
    public void openChangePasswordFragment() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, ChangePasswordFragment.getInstance(), ChangePasswordFragment.class.getName());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void openOnlinePaymentsFragment(InitializePaymentRequest result) {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, OnlinePaymentFragment.getInstance(result, getIntent().getStringExtra(AppConstants.PAYMENT_URL)), OnlinePaymentFragment.class.getName());
    }

    @Override
    public void onOrderPayment(String orderId, boolean paymentStatus) {
        Intent intent = new Intent(this, CreateOrderActivity.class);
        intent.putExtra(AppConstants.PAYMENT_STATUS, paymentStatus);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        setResult(RESULT_OK, intent);
        finishAfterTransition();
    }

    @OnClick({R.id.iv_back, R.id.tv_name, R.id.tv_email_address, R.id.tv_dashboard, R.id.tv_my_orders, R.id.tv_logout, R.id.tv_notification, R.id.tv_qd_wallet,
            R.id.tv_settings, R.id.tv_contact_us, R.id.tv_become_a_vendor, R.id.tv_payment_history, R.id.iv_notification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if (getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
                    if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        drawerLayout.closeDrawer(Gravity.RIGHT);
                    } else {
                        drawerLayout.openDrawer(Gravity.RIGHT);
                    }
                } else {
                    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        drawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        drawerLayout.openDrawer(GravityCompat.START);
                    }
                }
                break;
            case R.id.tv_name:
            case R.id.tv_email_address:
                if (mEditProfileViewModel.getEmail() != null)
                    openProfileScreen();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_dashboard:
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.tv_my_orders:
                if (mEditProfileViewModel.getEmail() != null)
                    openOrdersFragment();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_logout:
                openLogOutDialog();
                break;

            case R.id.tv_notification:
                if (mEditProfileViewModel.getEmail() != null)
                    openNotifications();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_payment_history:
                if (mEditProfileViewModel.getEmail() != null)
                    navigateToPaymentsHistory();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_become_a_vendor:
                if (mEditProfileViewModel.getEmail() != null && !mEditProfileViewModel.getUserType().equals(AppConstants.userType.ONLY_CASH_CLIENT)) {
                    AppUtils.getInstance().openSuccessDialog(this, getString(R.string.s_already_registered_as_vendor), this);
                } else if (mEditProfileViewModel.getEmail() != null)
                    navigateToBecomeAVendor();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_settings:
                if (mEditProfileViewModel.getEmail() != null)
                    openSettingsFragment();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_contact_us:
                openContactUsFragment();
                break;
            case R.id.tv_qd_wallet:
                if (mEditProfileViewModel.getEmail() != null)
                    openWalletFragment();
                else
                    AppUtils.getInstance().openConfirmationDialog(this, getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.iv_notification:
                openNotifications();

        }
    }

    private void openNotifications() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, NotificationsFragment.getInstance(), NotificationsFragment.class.getName());
    }

    private void navigateToPaymentsHistory() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, PaymentHistoryFragment.getInstance(), PaymentHistoryFragment.class.getName());
    }

    private void openWalletFragment() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, WalletFragment.getInstance(), WalletFragment.class.getName());
    }

    private void openContactUsFragment() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, ContactUsFragment.getInstance(), ContactUsFragment.class.getName());
    }

    private void openSettingsFragment() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, SettingsFragment.getInstance(), SettingsFragment.class.getName());
    }


    /**
     * open logout dialog
     */
    private void openLogOutDialog() {
        final Dialog logOutDialog = new Dialog(this, R.style.customDialog);
        logOutDialog.setContentView(R.layout.dialog_log_out);
        TextView tvMessage = logOutDialog.findViewById(R.id.tv_message);
        tvMessage.setText(getString(R.string.s_are_you_sure_you_want_to_logout));
        AppUtils.getInstance().dimDialogBackground(logOutDialog);
        Button btnLogout = logOutDialog.findViewById(R.id.btn_logout);
        Button btnCancel = logOutDialog.findViewById(R.id.btn_cancel);

        logOutDialog.show();

        btnLogout.setOnClickListener(v -> {
            logOutDialog.dismiss();
            mEditProfileViewModel.logOutUser(getDeviceId());

        });
        btnCancel.setOnClickListener(v -> logOutDialog.dismiss());
    }

    private void moveToOnBoard(String actionLogin) {
        Intent intent = new Intent(this, OnBoardActivity.class);
        switch (actionLogin) {
            case AppConstants.ACTION_WELCOME:
                intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_WELCOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                break;
            case AppConstants.ACTION_LOGIN:
                intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_LOGIN);

        }
        startActivity(intent);
        finishAfterTransition();
    }

    private void openOrdersFragment() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, MyOrdersFragment.getInstance(), MyOrdersFragment.class.getName());
    }

    private void openProfileScreen() {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, ProfileFragment.getInstance(), ProfileFragment.class.getName());
    }

    private void hideToolbar() {
        setDrawerLocked(false);
        updateStatusBarColour(R.color.colorWhite);
        toolbar.setVisibility(View.GONE);
    }

    private void showToolbar() {
        updateStatusBarColour(R.color.colorGreenishTeal);
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void moveToEditProfileFragment() {
        addFragmentWithBackStack(R.id.fl_container, EditProfileFragment.getInstance(), EditProfileFragment.class.getName());
    }

    @Override
    public void setDrawerLocked(boolean enabled) {
        if (!enabled) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void openBusinessOrderDetail(String orderId) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.BUSINESS_ORDER_DETAIL);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        startActivity(intent);
    }

    @Override
    public void openInvoiceDetails(String id) {
        hideToolbar();
        addFragmentWithBackStack(R.id.fl_container, InvoiceDetailsFragment.getInstance(id), InvoiceDetailsFragment.class.getName());
    }

    @Override
    public void openOrderDetailFragment(String orderId, boolean isOrderReceived) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.ORDER_DETAIL);
        intent.putExtra(AppConstants.IS_ORDER_RECEIVED, isOrderReceived);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        startActivity(intent);
    }

    @Override
    public void trackCurrentOrder(String id, boolean isFromReceived) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.TRACK_ORDER);
        intent.putExtra(AppConstants.IS_ORDER_RECEIVED, isFromReceived);
        intent.putExtra(AppConstants.ORDER_ID, id);
        startActivity(intent);

    }


   /* @Override
    public void updateDashBoardFragment() {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (fragment instanceof DashBoardFragment) {
            showToolbar();
            //((DashBoardFragment) fragment).updateDashBoardFragment();
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        setUserDetailOnSideMenu();
        QuickDelivery.getInstance().setCurrentActivity(this);
    }

    private void clearReferences() {
        Activity currActivity = QuickDelivery.getInstance().getCurrentActivity();
        if (this.equals(currActivity))
            QuickDelivery.getInstance().setCurrentActivity(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        clearReferences();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (fragment instanceof MyOrdersFragment) {
            ((MyOrdersFragment) fragment).updateOrders();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mEditProfileViewModel.disconnectSocket();
        clearReferences();
    }

    @Override
    public void onYesViewClicked() {
        moveToOnBoard(AppConstants.ACTION_LOGIN);
    }

    @Override
    public void onOkViewClicked() {
        setDrawerLocked(false);
    }
}
