package com.qd.user.ui.createorder.servicetype;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.ui.createorder.ordersummary.OrderSummaryFragment;
import com.qd.user.utils.AppUtils;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ServiceTypeDialogFragment extends DialogFragment implements PremiumServiceFragment.IPremiumServiceHost
        , FlexibleServiceFragment.IFlexibleServiceHost {

    @BindView(R.id.tl_service_type)
    TabLayout tlServiceType;
    @BindView(R.id.vp_service_type)
    ViewPager vpServiceType;
    @BindView(R.id.view_service_type)
    View viewServiceType;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_proceed)
    Button btnProceed;
    private Unbinder unbinder;

    private IServiceTypeHost mHost;
    private PremiumServiceFragment mPremiumServiceFragment;
    private FlexibleServiceFragment mFlexibleServiceFragment;
    private ServiceTypeViewModel mServiceTypeViewModel;
    private CreateOrderRequest mCreateOrderDetails;
    private String mNavigation;


    public static ServiceTypeDialogFragment getInstance(CreateOrderRequest createOrderData, String navigation) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, createOrderData);
        bundle.putString(AppConstants.ACTION_NAVIGATION, navigation);
        ServiceTypeDialogFragment serviceTypeDialogFragment = new ServiceTypeDialogFragment();
        serviceTypeDialogFragment.setArguments(bundle);
        return serviceTypeDialogFragment;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getActivity() instanceof IServiceTypeHost) {
            mHost = (IServiceTypeHost) getActivity();
        } else throw new IllegalStateException("Host must implement IServiceTypeHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_service_type, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()), R.style.customDialog);
        dialog.setContentView(root);
        AppUtils.getInstance().dimDialogBackgroundWithBottomGravity(dialog);

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mServiceTypeViewModel = new ViewModelProvider(this).get(ServiceTypeViewModel.class);
        getArgumentsData();
        initViews();
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.CREATE_ORDER_DETAILS)) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
            mNavigation = getArguments().getString(AppConstants.ACTION_NAVIGATION);
        }
    }

    private void initViews() {
        mPremiumServiceFragment = PremiumServiceFragment.getInstance(mCreateOrderDetails);
        mFlexibleServiceFragment = FlexibleServiceFragment.getInstance(mCreateOrderDetails);

        setUpViewPager();
    }

    private void setUpViewPager() {
        ArrayList<Fragment> fragmentArrayList = new ArrayList<>();

        if (mServiceTypeViewModel.getServiceType() != null && mServiceTypeViewModel.getServiceType().equals(getString(R.string.flexible))) {
            fragmentArrayList.add(mFlexibleServiceFragment);
        }
        fragmentArrayList.add(mPremiumServiceFragment);

        ServiceTypeViewPagerAdapter serviceTypeViewPagerAdapter = new ServiceTypeViewPagerAdapter(getChildFragmentManager(), fragmentArrayList);
        vpServiceType.setAdapter(serviceTypeViewPagerAdapter);
        tlServiceType.setupWithViewPager(vpServiceType);
    }



    @OnClick({R.id.btn_cancel, R.id.btn_proceed})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                mHost.onBackPressedOnDataEntered();
                break;
            case R.id.btn_proceed:
                getDateAndTimeForOrder();
                break;
        }
    }

    private void getDateAndTimeForOrder() {
        switch (vpServiceType.getCurrentItem()) {
            case 0:
                if (mServiceTypeViewModel.getServiceType().equals(getString(R.string.s_premium))) {
                    if (mPremiumServiceFragment.validateTime()) {
                        mCreateOrderDetails.setScheduledStartTime(mPremiumServiceFragment.getDateAndTime());
                        mCreateOrderDetails.setServiceType(getString(R.string.s_premium));
                        mCreateOrderDetails.setIslater(mPremiumServiceFragment.isForLater());

                        mHost.onBackPressedOnDataEntered();
                        if (mNavigation.equals(OrderSummaryFragment.class.getName())) {
                            mHost.updateOrderSummaryFragment(mCreateOrderDetails);
                        } else {
                            mHost.getDeliveryCharges(mCreateOrderDetails);
                            // mHost.openAddItemFragment(mCreateOrderDetails);
                        }
                    }
                } else {
                    mCreateOrderDetails.setScheduledStartTime(mFlexibleServiceFragment.getScheduledStartTime());
                    if (mFlexibleServiceFragment.validateTime() && mFlexibleServiceFragment.getTimeSlotId() != null) {
                        mCreateOrderDetails.setServiceType(getString(R.string.flexible));
                        mCreateOrderDetails.setIslater(true);
                        mCreateOrderDetails.setTimeSlotId(mFlexibleServiceFragment.getTimeSlotId());
                        mHost.onBackPressedOnDataEntered();
                        if (mNavigation.equals(OrderSummaryFragment.class.getName())) {
                            mHost.updateOrderSummaryFragment(mCreateOrderDetails);
                        } else {
                            mHost.getDeliveryCharges(mCreateOrderDetails);
                            //  mHost.openAddItemFragment(mCreateOrderDetails);
                        }
                    }
                }
                break;
            case 1:
                if (mPremiumServiceFragment.validateTime()) {
                    mCreateOrderDetails.setScheduledStartTime(mPremiumServiceFragment.getDateAndTime());
                    mCreateOrderDetails.setServiceType(getString(R.string.s_premium));
                    mCreateOrderDetails.setIslater(mPremiumServiceFragment.isForLater());
                    mHost.onBackPressedOnDataEntered();
                    if (mNavigation.equals(OrderSummaryFragment.class.getName())) {
                        mHost.updateOrderSummaryFragment(mCreateOrderDetails);
                    } else {
                        mHost.getDeliveryCharges(mCreateOrderDetails);
                        // mHost.openAddItemFragment(mCreateOrderDetails);
                    }
                    break;
                }
        }
    }

    public interface IServiceTypeHost {

        void onBackPressed();

        void openAddItemFragment(CreateOrderRequest createOrderDetails);

        void updateOrderSummaryFragment(CreateOrderRequest createOrderDetails);

        void getDeliveryCharges(CreateOrderRequest createOrderRequest);

        void onBackPressedOnDataEntered();
    }
}
