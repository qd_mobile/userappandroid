package com.qd.user.ui.createorder.servicetype;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.utils.BoundTimePickerDialog;
import com.qd.user.utils.DateFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PremiumServiceFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    @BindView(R.id.tv_now)
    TextView tvNow;
    @BindView(R.id.tv_later)
    TextView tvLater;
    @BindView(R.id.ll_now_later)
    LinearLayout llNowLater;
    @BindView(R.id.tv_label_pickup_date)
    TextView tvLabelPickupDate;
    @BindView(R.id.tv_order_date)
    TextView tvOrderDate;
    @BindView(R.id.tv_label_time)
    TextView tvLabelTime;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_error_time_slot)
    TextView tvErrorTimeSlot;
    private Unbinder unbinder;

    private Calendar mCalendar;
    private CreateOrderRequest mCreateOrderDetails;

    public PremiumServiceFragment() {
        // Required empty public constructor
    }

    public static PremiumServiceFragment getInstance(Parcelable createOrderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.ORDER_DATA, createOrderDetails);
        PremiumServiceFragment premiumServiceFragment = new PremiumServiceFragment();
        premiumServiceFragment.setArguments(bundle);
        return premiumServiceFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_service_type_premium, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (!(getParentFragment() instanceof IPremiumServiceHost)) {
            throw new IllegalStateException("Host must implement IPremiumServiceHost");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void initViewsAndVariables() {
        getArgumentsData();

        mCalendar = Calendar.getInstance();
        tvOrderDate.setText(getFormattedDate(mCalendar.getTime()));
        setViewsForNow();
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getParcelable(AppConstants.ORDER_DATA) != null) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.ORDER_DATA);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tv_now, R.id.tv_later, R.id.tv_order_date, R.id.tv_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_now:
                setViewsForNow();
                break;
            case R.id.tv_later:
                setViewsForLater();
                break;
            case R.id.tv_order_date:
                if (tvLabelPickupDate.getText().toString().equals(getString(R.string.s_pickup_date)))
                    openDatePicker();
                break;
            case R.id.tv_time:
                openTimePicker();
                break;
        }
    }

    private void setViewsForNow() {
        selectTimeSlot(tvNow, tvLater);
        tvLabelPickupDate.setText(getString(R.string.s_pickup_date_time));
        tvOrderDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        mCalendar.setTime(Calendar.getInstance().getTime());
        tvOrderDate.setText(getCurrentDateTime(mCalendar.getTime()));
        tvLabelTime.setVisibility(View.GONE);
        tvTime.setVisibility(View.GONE);
        tvErrorTimeSlot.setVisibility(View.GONE);
    }

    private void setViewsForLater() {
        if (!isForLater()) {
            selectTimeSlot(tvLater, tvNow);

            if (mCreateOrderDetails.getServiceType() != null && mCreateOrderDetails.getIslater()) {
                tvTime.setText(DateFormatter.getInstance().getFormattedTime(mCreateOrderDetails.getScheduledStartTime()));
                tvOrderDate.setText(DateFormatter.getInstance().getFormattedDate(mCreateOrderDetails.getScheduledStartTime()));
            }

            /*if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) <= MIN_HOUR_LIMIT) {
                mCalendar.set(Calendar.HOUR_OF_DAY, MIN_HOUR_LIMIT + 1);
                mCalendar.set(Calendar.MINUTE, 0);
            } else {
                mCalendar.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1);
                mCalendar.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE) + 1);
            }*/

            mCalendar.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1);
            mCalendar.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE) + 1);

            tvLabelPickupDate.setText(getString(R.string.s_pickup_date));
            tvTime.setText(getFormattedTime(mCalendar.getTime()));
            tvOrderDate.setText(getFormattedDate(mCalendar.getTime()));
            tvOrderDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_service_type_date, 0);
            tvLabelTime.setVisibility(View.VISIBLE);
            tvTime.setVisibility(View.VISIBLE);
        }
    }

    boolean isForLater() {
        return tvTime.getVisibility() == View.VISIBLE;
    }

    private String getCurrentDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy 'at' hh:mm a", Locale.getDefault());
        return sdf.format(date);
    }

    private void openTimePicker() {
        BoundTimePickerDialog boundTimePickerDialog = new BoundTimePickerDialog(getContext(), this, mCalendar.get(Calendar.HOUR_OF_DAY),
                mCalendar.get(Calendar.MINUTE), true);


        if (mCalendar.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
            /*if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) <= MIN_HOUR_LIMIT) {
                boundTimePickerDialog.setMin(MIN_HOUR_LIMIT + 1, 0);
            } else */
                boundTimePickerDialog.setMin(Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1, Calendar.getInstance().get(Calendar.MINUTE) + 1);

            //boundTimePickerDialog.setMax(MAX_HOUR_LIMIT_LATER, MAX_MIN_LIMIT);
        } /*else {
            boundTimePickerDialog.setMin(MIN_HOUR_LIMIT, MIN_MIN_LIMIT);
            boundTimePickerDialog.setMax(MAX_HOUR_LIMIT, MAX_MIN_LIMIT);
        }*/

        boundTimePickerDialog.show();
    }

    private void openDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        if (getContext() != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
            datePickerDialog.show();
        }
    }

    private void selectTimeSlot(TextView selected, TextView unselected) {
        tvErrorTimeSlot.setVisibility(View.GONE);
        selected.setBackgroundResource(R.color.colorBlack);
        selected.setTextColor(getResources().getColor(R.color.colorWhite));
        unselected.setTextColor(getResources().getColor(R.color.colorBlack));
        unselected.setBackgroundResource(0);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mCalendar.set(year, month, dayOfMonth);
        Date date = mCalendar.getTime();
        tvOrderDate.setText(getFormattedDate(date));
        tvErrorTimeSlot.setVisibility(View.GONE);
    }

    /**
     * Formats the date in required format
     *
     * @param date Date to be formatted
     * @return String containing formatted date
     */
    private String getFormattedDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        return sdf.format(date);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
        Date date = calendar.getTime();

        mCalendar.setTime(date);
        tvTime.setText(getFormattedTime(date));
        tvErrorTimeSlot.setVisibility(View.GONE);
    }

    private String getFormattedTime(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        return sdf.format(time);
    }

    String getDateAndTime() {
        return getScheduledDateTime(mCalendar.getTime());
    }

    private String getScheduledDateTime(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(time);
    }

    boolean validateTime() {
        Calendar minStartCalendar = Calendar.getInstance();
       // Calendar maxStartCalendar = Calendar.getInstance();

      //  maxStartCalendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        minStartCalendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

        if (mCalendar.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
            if (tvTime.getVisibility() == View.VISIBLE) {
                /*if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) <= MIN_HOUR_LIMIT) {
                    minStartCalendar.set(Calendar.HOUR_OF_DAY, MIN_HOUR_LIMIT);
                    minStartCalendar.set(Calendar.MINUTE, MIN_MIN_LIMIT);
                } else {
                    minStartCalendar.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1);
                    minStartCalendar.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));
                }
                maxStartCalendar.set(Calendar.HOUR_OF_DAY, MAX_HOUR_LIMIT_LATER);
                maxStartCalendar.set(Calendar.MINUTE, MAX_MIN_LIMIT);*/

                minStartCalendar.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1);
                minStartCalendar.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));
                tvErrorTimeSlot.setText(getString(R.string.s_you_can_only_create_orders_for_two_hours_after_now_till_9_in_the_evening));
            } else {
                mCalendar.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE) + 1);
                /*minStartCalendar.set(Calendar.HOUR_OF_DAY, MIN_HOUR_LIMIT);
                minStartCalendar.set(Calendar.MINUTE, MIN_MIN_LIMIT);
                maxStartCalendar.set(Calendar.HOUR_OF_DAY, MAX_HOUR_LIMIT);
                maxStartCalendar.set(Calendar.MINUTE, MAX_MIN_LIMIT);
                tvErrorTimeSlot.setText(getString(R.string.s_you_can_only_create_orders_between_8_00am_to_10_30pm));*/
            }
        } else {
            /*minStartCalendar.set(Calendar.HOUR_OF_DAY, MIN_HOUR_LIMIT);
            minStartCalendar.set(Calendar.MINUTE, MIN_MIN_LIMIT);
            maxStartCalendar.set(Calendar.HOUR_OF_DAY, MAX_HOUR_LIMIT);
            maxStartCalendar.set(Calendar.MINUTE, MAX_MIN_LIMIT);*/

            minStartCalendar.set(Calendar.HOUR_OF_DAY, 0);
            minStartCalendar.set(Calendar.MINUTE, 0);
            tvErrorTimeSlot.setText(getString(R.string.s_you_can_only_create_orders_for_two_hours_after_now_till_10_30_in_the_evening));

        }


        if (mCalendar.getTime().before(minStartCalendar.getTime()) /*||
                mCalendar.getTime().after(maxStartCalendar.getTime())*/) {
            tvErrorTimeSlot.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    interface IPremiumServiceHost {

    }
}
