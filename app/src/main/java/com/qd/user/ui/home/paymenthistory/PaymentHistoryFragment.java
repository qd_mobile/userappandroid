package com.qd.user.ui.home.paymenthistory;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.ui.home.businesspayment.BusinessPaymentsFragment;
import com.qd.user.ui.home.personalpayment.PersonalPaymentsFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentHistoryFragment extends BaseFragment implements PersonalPaymentsFragment.IPersonalPaymentsHost, BusinessPaymentsFragment.IBusinessPaymentsHost, ViewPager.OnPageChangeListener {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tl_payments)
    TabLayout tlPayments;
    @BindView(R.id.vp_payments)
    ViewPager vpPayments;
    private Unbinder unbinder;
    private ArrayList<Fragment> mFragmentList;
    private PersonalPaymentsFragment mPersonalPaymentFragment;
    private BusinessPaymentsFragment mBusinessPaymentFragment;

    public PaymentHistoryFragment() {
        // Required empty public constructor
    }


    /**
     * A {@link IPaymentHistoryHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IPaymentHistoryHost mPaymentHistoryHost;
    private PaymentHistoryViewModel mPaymentHistoryViewModel;


    public static PaymentHistoryFragment getInstance() {
        return new PaymentHistoryFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IPaymentHistoryHost) {
            mPaymentHistoryHost = (IPaymentHistoryHost) context;
        } else
            throw new IllegalStateException("host must implement IPaymentHistoryHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_history, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPaymentHistoryViewModel = new ViewModelProvider(this).get(PaymentHistoryViewModel.class);
        mPaymentHistoryViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        setUpPaymentViewPagerAdapter();
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_payment_history);
        ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
        ivNotification.setVisibility(View.VISIBLE);

        mPersonalPaymentFragment = PersonalPaymentsFragment.getInstance();
        mBusinessPaymentFragment = BusinessPaymentsFragment.getInstance();

        mFragmentList = new ArrayList<>();
    }

    private void setUpPaymentViewPagerAdapter() {
        mFragmentList.add(mPersonalPaymentFragment);
        tlPayments.setVisibility(View.GONE);

        if (mPaymentHistoryViewModel.getUserType().equals(AppConstants.userType.ONLY_VENDOR) ||
                mPaymentHistoryViewModel.getUserType().equals(AppConstants.userType.BOTH)) {
            mFragmentList.add(mBusinessPaymentFragment);
            tlPayments.setVisibility(View.VISIBLE);
        }

        PaymentHistoryViewPagerAdapter mViewPagerAdapter = new PaymentHistoryViewPagerAdapter(getChildFragmentManager(), mFragmentList);
        vpPayments.setAdapter(mViewPagerAdapter);
        tlPayments.setupWithViewPager(vpPayments);
        vpPayments.addOnPageChangeListener(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void openInvoiceDetails(String id) {
        mPaymentHistoryHost.openInvoiceDetails(id);
    }

    @Override
    public void openOnlinePaymentsFragment(InitializePaymentRequest requestForInitializingPayment) {
        mPaymentHistoryHost.openOnlinePaymentsFragment(requestForInitializingPayment);
    }

    @OnClick({R.id.iv_back, R.id.iv_notification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mPaymentHistoryHost.onBackPressed();
                break;
            case R.id.iv_notification:
                setFilters();
        }
    }

    /**
     * set views for filters, based on which fragment is currently visible and switches the
     * filter and cross icon accordingly
     */
    private void setFilters() {
        switch (vpPayments.getCurrentItem()) {
            case 0:
                if (mPersonalPaymentFragment.showOrHideFilter()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;
            case 1:
                if (mBusinessPaymentFragment.showOrHideFilter()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;

        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        switch (i) {
            case 0:
                if (mPersonalPaymentFragment.isFilterApplied()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;
            case 1:
                if (mBusinessPaymentFragment.isFilterApplied()) {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filter_cross));
                } else {
                    ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_orders_filters));
                }
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }


    public interface IPaymentHistoryHost {

        void onBackPressed();

        void openInvoiceDetails(String id);

        void openOnlinePaymentsFragment(InitializePaymentRequest requestForInitializingPayment);
    }
}
