package com.qd.user.ui.onboard.welcome;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.facebook.CallbackManager;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.socialresponse.FbLoginResponse;
import com.qd.user.model.socialresponse.socialsigninresponse.SocialLoginResponse;

public class WelcomeViewModel extends ViewModel {
    private RichMediatorLiveData<FbLoginResponse> mFacebookLoginLiveData;
    private RichMediatorLiveData<SocialLoginResponse> mSocialLogInLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private WelcomeRepository mRepo=new WelcomeRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver=loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if(mFacebookLoginLiveData ==null){
            mFacebookLoginLiveData = new RichMediatorLiveData<FbLoginResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }
        if(mSocialLogInLiveData ==null){
            mSocialLogInLiveData = new RichMediatorLiveData<SocialLoginResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }
    }


    void initializeFbLogin() {
        mRepo.initializeFbLogin(mFacebookLoginLiveData);
    }
    /**
     * This method gives the login live data object to {@link WelcomeFragment}
     *
     * @return {@link #mFacebookLoginLiveData}
     */
    RichMediatorLiveData<FbLoginResponse> getFacebookLoginLiveData() {
        return mFacebookLoginLiveData;
    }


    /**
     * This method gives the login live data object to {@link WelcomeFragment}
     *
     * @return {@link #mSocialLogInLiveData}
     */
    RichMediatorLiveData<SocialLoginResponse> getSocialLoginLiveData() {
        return mSocialLogInLiveData;
    }


    CallbackManager getCallBackManager() {
       return mRepo.getCallBackManager();
    }

    void signUpThroughFacebookOrGoogle(CreateAccountInfo accountInfo) {
        mRepo.getFireBaseToken(mSocialLogInLiveData, accountInfo);
    }

    void setIsUserVerified() {
        mRepo.setIsUserVerified();
    }


}
