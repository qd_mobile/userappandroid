package com.qd.user.ui.home.businesspayment;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.invoice.InvoiceListingResponse;

import java.util.HashMap;

class BusinessPaymentsRepository {

    void getInvoiceListing(final RichMediatorLiveData<InvoiceListingResponse> invoiceListingLiveData, int page, String fromDate, String toDate) {
        DataManager.getInstance().getInvoiceListing(createPayloadForInvoice(page, fromDate, toDate)).enqueue(new NetworkCallback<InvoiceListingResponse>() {
            @Override
            public void onSuccess(InvoiceListingResponse invoiceListingResponse) {
                invoiceListingLiveData.setValue(invoiceListingResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                invoiceListingLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                invoiceListingLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> createPayloadForInvoice(int page, String fromDate, String toDate) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put("vendorId", DataManager.getInstance().getUserId());
        mRequestMap.put("page", page);
        mRequestMap.put("limit", "10");
        mRequestMap.put("fromDate", fromDate);
        mRequestMap.put("toDate", toDate);

        return mRequestMap;
    }

    public String getRegistrationDate() {
        return DataManager.getInstance().getRegistrationDateAndTime();
    }
}
