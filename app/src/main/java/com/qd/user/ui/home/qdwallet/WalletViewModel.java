package com.qd.user.ui.home.qdwallet;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;
import com.qd.user.model.payments.wallet.WalletTransactionHistoryResponse;

public class WalletViewModel extends ViewModel {
    private RichMediatorLiveData<WalletTransactionHistoryResponse> mWalletLiveData;
    private RichMediatorLiveData<WalletRechargeResponse> mWalletRechargeLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private WalletRepository mRepo = new WalletRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mWalletLiveData == null) {
            mWalletLiveData = new RichMediatorLiveData<WalletTransactionHistoryResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }

        if (mWalletRechargeLiveData == null) {
            mWalletRechargeLiveData = new RichMediatorLiveData<WalletRechargeResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }

    /**
     * This method gives the wallet transaction history live data object to {@link WalletFragment}
     *
     * @return {@link #mWalletLiveData}
     */
    RichMediatorLiveData<WalletTransactionHistoryResponse> getWalletLiveData() {
        return mWalletLiveData;
    }

    /**
     * This method gives the wallet recharge live data object to {@link WalletFragment}
     *
     * @return {@link #mWalletRechargeLiveData}
     */
    RichMediatorLiveData<WalletRechargeResponse> getWalletRechargeLiveData() {
        return mWalletRechargeLiveData;
    }

    /**
     * Method used to get wallet transaction history
     *
     * @param nextPage next page whose data is to be fetched
     */
    void getWalletTransactionHistory(int nextPage) {
        mRepo.getWalletTransactionHistory(mWalletLiveData, nextPage);
    }

    void rechargeWallet(String amount) {
        mWalletRechargeLiveData.setLoadingState(true);
        mRepo.rechargeWallet(mWalletRechargeLiveData, amount);
    }
}
