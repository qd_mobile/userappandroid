package com.qd.user.ui.trackorder.businessorder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.orderdetail.DropLocation;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DropLocationAdapter extends RecyclerView.Adapter<DropLocationAdapter.DropLocationAdapterViewHolder> {


    private final List<DropLocation> mDropLocationList;
    private DropLocationAdapterInterface mInterface;

    DropLocationAdapter(List<DropLocation> dropLocationList, DropLocationAdapterInterface dropLocationAdapterInterface) {
        mDropLocationList = dropLocationList;
        mInterface = dropLocationAdapterInterface;
    }

    @NonNull
    @Override
    public DropLocationAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_drop_details, viewGroup, false);
        return new DropLocationAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DropLocationAdapterViewHolder dropLocationAdapterViewHolder, int i) {
        dropLocationAdapterViewHolder.tvDropOffLocation.setText(mDropLocationList.get(i).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (mDropLocationList.get(i).getDriverNote() != null && !mDropLocationList.get(i).getDriverNote().isEmpty())
            dropLocationAdapterViewHolder.tvNoteForDriver.setText(mDropLocationList.get(i).getDriverNote());
        else {
            dropLocationAdapterViewHolder.tvNoteForDriver.setText("N/A");
        }


        dropLocationAdapterViewHolder.tvLabelDropOffLocation.setText(String.format(Locale.getDefault(), "%s %d",
                dropLocationAdapterViewHolder.tvLabelDropOffLocation.getResources().getString(R.string.s_drop_location), i + 1));

        dropLocationAdapterViewHolder.tvPaymentMode.setText(mDropLocationList.get(i).getPaymentMode());
        dropLocationAdapterViewHolder.tvName.setText(mDropLocationList.get(i).getRecipientName());
        dropLocationAdapterViewHolder.tvPhoneNumber.setText(mDropLocationList.get(i).getRecipientMobileNo());
        dropLocationAdapterViewHolder.tvItemAmount.setText(String.format(Locale.getDefault(), "%.3f KWD", mDropLocationList.get(i).getItem().get(0).getItemPrice()));
        dropLocationAdapterViewHolder.tvDeliveryAmount.setText(String.format(Locale.getDefault(), "%.3f KWD", mDropLocationList.get(i).getDeliveryCharges()));
        dropLocationAdapterViewHolder.tvReturnAmount.setText(String.format(Locale.getDefault(), "%.3f KWD", mDropLocationList.get(i).getReturnCharge()));

        switch (mDropLocationList.get(i).getDriverTrackingStatus().get(mDropLocationList.get(i).getDriverTrackingStatus().size() - 1).getStatus()) {
            case AppConstants.DriverTrackingStatus.UNALIGNED:
                dropLocationAdapterViewHolder.tvDriverName.setVisibility(View.GONE);
                dropLocationAdapterViewHolder.ivDriver.setVisibility(View.GONE);
                dropLocationAdapterViewHolder.ivVehicleType.setVisibility(View.GONE);

                dropLocationAdapterViewHolder.tvLabelReturnCharges.setVisibility(View.GONE);
                dropLocationAdapterViewHolder.tvReturnAmount.setVisibility(View.GONE);
                dropLocationAdapterViewHolder.tvOrderStatus.setVisibility(View.GONE);
                dropLocationAdapterViewHolder.ivCancel.setVisibility(View.VISIBLE);
                break;

            case AppConstants.DriverTrackingStatus.DRIVE_TO_DROP_LOCATION:
                dropLocationAdapterViewHolder.tvOrderStatus.setVisibility(View.VISIBLE);
                dropLocationAdapterViewHolder.tvOrderStatus.setText(R.string.s_in_progress);
                dropLocationAdapterViewHolder.ivCancel.setVisibility(View.VISIBLE);
                showDeliveryDriverDetails(dropLocationAdapterViewHolder, i);
                break;

            case AppConstants.DriverTrackingStatus.REACHED_TO_DROP_LOCATION:
                dropLocationAdapterViewHolder.tvOrderStatus.setVisibility(View.VISIBLE);
                dropLocationAdapterViewHolder.tvOrderStatus.setText(R.string.s_reached);
                dropLocationAdapterViewHolder.ivCancel.setVisibility(View.VISIBLE);
                showDeliveryDriverDetails(dropLocationAdapterViewHolder, i);

                break;

            case AppConstants.DriverTrackingStatus.ACCEPT_ORDER:
            case AppConstants.DriverTrackingStatus.DRIVE_TO_PICKEDUP:
            case AppConstants.DriverTrackingStatus.REACHED_AT_PICKUP:
            case AppConstants.DriverTrackingStatus.ORDER_PICKEDUP:
                dropLocationAdapterViewHolder.ivCancel.setVisibility(View.VISIBLE);
                dropLocationAdapterViewHolder.tvOrderStatus.setVisibility(View.GONE);
                if(mDropLocationList.get(i).getDriverTrackingStatus().get(mDropLocationList.get(i).getDriverTrackingStatus().size() - 1).getStatus().equals(AppConstants.DriverTrackingStatus.ORDER_PICKEDUP)&&
                        mInterface.getServiceType().equals(AppConstants.serviceType.FLEXIBLE)) {
                    dropLocationAdapterViewHolder.tvDriverName.setVisibility(View.GONE);
                    dropLocationAdapterViewHolder.ivDriver.setVisibility(View.GONE);
                    dropLocationAdapterViewHolder.ivVehicleType.setVisibility(View.GONE);
                }else {
                    showDeliveryDriverDetails(dropLocationAdapterViewHolder, i);
                }

                break;

            case AppConstants.DriverTrackingStatus.DROP_COMPLETE:
                dropLocationAdapterViewHolder.tvLabelReturnCharges.setVisibility(View.GONE);
                dropLocationAdapterViewHolder.tvReturnAmount.setVisibility(View.GONE);
                showDeliveryDriverDetails(dropLocationAdapterViewHolder, i);

                    dropLocationAdapterViewHolder.tvOrderStatus.setText(R.string.s_delivered);
                    dropLocationAdapterViewHolder.ivCancel.setVisibility(View.GONE);

                break;
            case AppConstants.DriverTrackingStatus.CANCELLED:
                if (mDropLocationList.get(i).getDriverTrackingStatus().get(mDropLocationList.get(i).getDriverTrackingStatus().size() - 2).getStatus()
                        .equals(AppConstants.DriverTrackingStatus.UNALIGNED)) {
                    dropLocationAdapterViewHolder.tvDriverName.setVisibility(View.GONE);
                    dropLocationAdapterViewHolder.ivDriver.setVisibility(View.GONE);
                    dropLocationAdapterViewHolder.ivVehicleType.setVisibility(View.GONE);

                    dropLocationAdapterViewHolder.tvLabelReturnCharges.setVisibility(View.GONE);
                    dropLocationAdapterViewHolder.tvReturnAmount.setVisibility(View.GONE);
                    dropLocationAdapterViewHolder.tvOrderStatus.setVisibility(View.GONE);
                } else {
                    dropLocationAdapterViewHolder.tvOrderStatus.setText(R.string.s_cancelled);
                    showDeliveryDriverDetails(dropLocationAdapterViewHolder, i);
                }
                break;
            case AppConstants.DriverTrackingStatus.RETURN_INITIATED:
            case AppConstants.DriverTrackingStatus.RETURNED:
                dropLocationAdapterViewHolder.tvOrderStatus.setVisibility(View.VISIBLE);
                dropLocationAdapterViewHolder.tvOrderStatus.setText(R.string.s_returned);
                showDeliveryDriverDetails(dropLocationAdapterViewHolder, i);
        }

        if (i + 1 == mDropLocationList.size()) {
            dropLocationAdapterViewHolder.viewDottedLine.setVisibility(View.GONE);
        }

    }

    private void showDeliveryDriverDetails(DropLocationAdapterViewHolder dropLocationAdapterViewHolder, int position) {
        Glide.with(dropLocationAdapterViewHolder.ivDriver.getContext())
                .load(mDropLocationList.get(position).getDeliveryDriverImage())
                .apply(new RequestOptions().placeholder(R.drawable.ic_profile_placeholder))
                .into(dropLocationAdapterViewHolder.ivDriver);
        dropLocationAdapterViewHolder.tvDriverName.setText(mDropLocationList.get(position).getDeliveryDriverName());
        dropLocationAdapterViewHolder.tvDriverPhoneNumber.setText(mDropLocationList.get(position).getDeliveryDriverMobileNo());


        switch (mDropLocationList.get(position).getDeliveryDriverVehicle()) {
            case AppConstants.DeliveryVehicles.BIKE:
                Glide.with(dropLocationAdapterViewHolder.ivVehicleType.getContext())
                        .load(R.drawable.ic_choose_vehicle_bike_inactive)
                        .into(dropLocationAdapterViewHolder.ivVehicleType);
                break;
            case AppConstants.DeliveryVehicles.CAR:
                Glide.with(dropLocationAdapterViewHolder.ivVehicleType.getContext())
                        .load(R.drawable.ic_choose_vehicle_car_inactive)
                        .into(dropLocationAdapterViewHolder.ivVehicleType);
                break;
            case AppConstants.DeliveryVehicles.VAN:
                Glide.with(dropLocationAdapterViewHolder.ivVehicleType.getContext())
                        .load(R.drawable.ic_choose_vehicle_van_inactive)
                        .into(dropLocationAdapterViewHolder.ivVehicleType);
                break;
            case AppConstants.DeliveryVehicles.COOLER:
                Glide.with(dropLocationAdapterViewHolder.ivVehicleType.getContext())
                        .load(R.drawable.ic_choose_vehicle_cooler_inactive)
                        .into(dropLocationAdapterViewHolder.ivVehicleType);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDropLocationList.size();
    }

    interface DropLocationAdapterInterface {

        void onDropOffCancelled(String id, String driverId);

        String getServiceType();
    }

    class DropLocationAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_drop_off_location)
        ImageView ivDropOffLocation;
        @BindView(R.id.view_dotted_line)
        View viewDottedLine;
        @BindView(R.id.tv_label_drop_off_location)
        TextView tvLabelDropOffLocation;
        @BindView(R.id.tv_drop_off_location)
        TextView tvDropOffLocation;
        @BindView(R.id.iv_dropoff_arrow)
        ImageView ivDropoffArrow;
        @BindView(R.id.view_drop_location)
        View viewDropLocation;
        @BindView(R.id.tv_label_note_for_driver)
        TextView tvLabelNoteForDriver;
        @BindView(R.id.tv_note_for_driver)
        TextView tvNoteForDriver;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_label_recipient_details)
        TextView tvLabelRecipientDetails;
        @BindView(R.id.tv_label_name)
        TextView tvLabelName;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_label_phone_number)
        TextView tvLabelPhoneNumber;
        @BindView(R.id.tv_phone_number)
        TextView tvPhoneNumber;
        @BindView(R.id.view_recepient_details)
        View viewRecepientDetails;
        @BindView(R.id.tv_item_amount)
        TextView tvItemAmount;
        @BindView(R.id.tv_delivery_amount)
        TextView tvDeliveryAmount;
        @BindView(R.id.tv_return_amount)
        TextView tvReturnAmount;
        @BindView(R.id.tv_label_item_price)
        TextView tvLabelItemPrice;
        @BindView(R.id.tv_label_delivery_charges)
        TextView tvLabelDeliveryCharges;
        @BindView(R.id.tv_label_return_charges)
        TextView tvLabelReturnCharges;
        @BindView(R.id.iv_driver)
        ImageView ivDriver;
        @BindView(R.id.iv_cancel)
        ImageView ivCancel;
        @BindView(R.id.tv_driver_name)
        TextView tvDriverName;
        @BindView(R.id.tv_driver_phone_number)
        TextView tvDriverPhoneNumber;
        @BindView(R.id.iv_vehicle_type)
        ImageView ivVehicleType;
        @BindView(R.id.tv_label_payment_mode)
        TextView tvLabelPaymentMode;
        @BindView(R.id.tv_payment_mode)
        TextView tvPaymentMode;
        @BindView(R.id.cl_drop_details)
        ConstraintLayout clDropDetails;
        @BindView(R.id.cl_drop_location)
        ConstraintLayout clDropLocation;

        DropLocationAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ivDropoffArrow.setOnClickListener(v -> {
                if (clDropDetails.getVisibility() == View.VISIBLE) {
                    clDropDetails.setVisibility(View.GONE);
                    ivDropoffArrow.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_my_orders_arrow_down));

                } else {
                    clDropDetails.setVisibility(View.VISIBLE);
                    ivDropoffArrow.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ic_my_oreders_arrow_up));
                }
            });

            ivCancel.setOnClickListener(v -> mInterface.onDropOffCancelled(mDropLocationList.get(getAdapterPosition()).getId(), mDropLocationList.get(getAdapterPosition()).getDeliveryDriverId()));

        }
    }
}
