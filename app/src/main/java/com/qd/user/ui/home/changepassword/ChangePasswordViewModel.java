package com.qd.user.ui.home.changepassword;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

public class ChangePasswordViewModel extends ViewModel {
    private RichMediatorLiveData<CommonResponse> mChangePasswordLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;
    private ChangePasswordRepository mRepo=new ChangePasswordRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver=loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if(mChangePasswordLiveData ==null){
            mChangePasswordLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if(mValidateLiveData == null){
                mValidateLiveData = new MutableLiveData<>();
            }
        }
    }

    /**
     * This method gives the login live data object to {@link ChangePasswordFragment}
     *
     * @return {@link #mChangePasswordLiveData}
     */
    RichMediatorLiveData<CommonResponse> getChangePasswordLiveData() {
        return mChangePasswordLiveData;
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    void onSaveViewClicked(String oldPassword, String newPassword, String confirmNewPassword) {
        if (validateInputFields(oldPassword,newPassword,confirmNewPassword)){
         mRepo.hitChangePasswordApi(oldPassword,newPassword,confirmNewPassword,mChangePasswordLiveData);
        }
    }

    private boolean validateInputFields(CharSequence oldPassword, CharSequence newPassword, String confirmNewPassword) {
        if (oldPassword.length()<8||confirmNewPassword.length()>16) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_OLD_PASSWORD, R.string.s_password_should_be_between_characters));
            return false;
        } else if (newPassword.length()<8||newPassword.length()>16) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_NEW_PASSWORD, R.string.s_password_should_be_between_characters));
            return false;
        } else if (!confirmNewPassword.contentEquals(newPassword)) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_CONFIRM_NEW_PASSWORD, R.string.s_password_does_not_match));
            return false;
        }
        return true;
    }


}
