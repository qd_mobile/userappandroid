package com.qd.user.ui.createorder.selectlocation.savedaddress;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.createorder.savedaddress.Result;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentAddressAdapter extends RecyclerView.Adapter<RecentAddressAdapter.RecentAddressAdapterViewHolder> {


    private ArrayList<Result> mRecentAddressList;
    private RecentAddressAdapterInterface mInterface;

    RecentAddressAdapter(ArrayList<Result> recentAddressResult, RecentAddressAdapterInterface recentAddressAdapterInterface) {
        mRecentAddressList = recentAddressResult;
        mInterface = recentAddressAdapterInterface;
    }

    @NonNull
    @Override
    public RecentAddressAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_recent_address, viewGroup, false);
        return new RecentAddressAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentAddressAdapterViewHolder recentAddressAdapterViewHolder, int i) {
        recentAddressAdapterViewHolder.tvRecentAddress.setText(mRecentAddressList.get(i).getSavedLocation().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
    }

    @Override
    public int getItemCount() {
        return mRecentAddressList.size();
    }


    interface RecentAddressAdapterInterface {

        void onRecentAddressClicked(Result result);
    }

    class RecentAddressAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_recent_address)
        TextView tvRecentAddress;

        RecentAddressAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> mInterface.onRecentAddressClicked(mRecentAddressList.get(getAdapterPosition())));
        }
    }
}


