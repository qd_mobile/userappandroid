package com.qd.user.ui.createorder.droplocation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qd.user.R;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.utils.dragdrop.ItemTouchHelperAdapter;
import com.qd.user.utils.dragdrop.ItemTouchHelperViewHolder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DropLocationsListAdapter extends RecyclerView.Adapter<DropLocationsListAdapter.DropLocationsListAdapterViewHolder>
        implements ItemTouchHelperAdapter {

    private final ArrayList<DropOffArr> mDropLocationsList;
    private DropLocationsListAdapterInterface mInterface;
    private OnStartDragListener mDragListener;

    DropLocationsListAdapter(ArrayList<DropOffArr> DropLocationsList, DropLocationsListAdapterInterface DropLocationsListAdapterInterface,
                             OnStartDragListener onStartDragListener) {
        mDropLocationsList = DropLocationsList;
        mInterface = DropLocationsListAdapterInterface;
        mDragListener = onStartDragListener;
    }


    @NonNull
    @Override
    public DropLocationsListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_rearrange_deliveries, viewGroup, false);
        return new DropLocationsListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DropLocationsListAdapterViewHolder dropLocationsListAdapterViewHolder, int i) {
        if (mDropLocationsList.get(i).getRecipientName() != null) {
            dropLocationsListAdapterViewHolder.tvRecipientName.setText(mDropLocationsList.get(i).getRecipientName());
            dropLocationsListAdapterViewHolder.tvItemName.setText(mDropLocationsList.get(i).getItem().get(0).getItemDescription());
            dropLocationsListAdapterViewHolder.tvLocation.setText(mDropLocationsList.get(i).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

            if (mDropLocationsList.get(i).getItem().get(0).getItemImage().size() > 0) {
                Glide.with(dropLocationsListAdapterViewHolder.ivItemImage.getContext())
                        .load(mDropLocationsList.get(i).getItem().get(0).getItemImage().get(0))
                        .apply(new RequestOptions().placeholder(R.drawable.ic_delivery_type_single_inactive))
                        .into(dropLocationsListAdapterViewHolder.ivItemImage);

            } else Glide.with(dropLocationsListAdapterViewHolder.ivItemImage.getContext())
                    .load(R.drawable.ic_delivery_type_single_inactive)
                    .into(dropLocationsListAdapterViewHolder.ivItemImage);

            dropLocationsListAdapterViewHolder.itemView.setOnTouchListener((v, event) -> {
                v.performClick();
                if (event.getActionMasked() ==
                        MotionEvent.ACTION_DOWN) {
                    mDragListener.onStartDrag(dropLocationsListAdapterViewHolder);
                }
                return false;
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDropLocationsList.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        mDropLocationsList.add(toPosition, mDropLocationsList.remove(fromPosition));
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        mDropLocationsList.remove(position);
        notifyItemRemoved(position);
    }

    public ArrayList<DropOffArr> getDropOffArr() {
        return mDropLocationsList;
    }

    void removeDrop(int position) {
        mDropLocationsList.remove(position);
        notifyItemRemoved(position);
    }

    public interface DropLocationsListAdapterInterface {

        void onEditItemClicked(int adapterPosition);

        void onDeleteItemClicked(int adapterPosition);
    }

    class DropLocationsListAdapterViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        @BindView(R.id.tv_recipient_name)
        TextView tvRecipientName;
        @BindView(R.id.tv_location)
        TextView tvLocation;
        @BindView(R.id.iv_item_options)
        ImageView ivItemOptions;
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.cv_item_image)
        CardView cvItemImage;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.view_item_list)
        View viewItemList;

        DropLocationsListAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            ivItemOptions.setOnClickListener(v -> inflatePopupMenu(ivItemOptions.getContext(), ivItemOptions, getAdapterPosition()));

        }

        private void inflatePopupMenu(Context context, ImageView options, final int adapterPosition) {
            final PopupMenu popupMenu = new PopupMenu(context, options);
            popupMenu.inflate(R.menu.menu_item_options);

            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.menu_edit:
                        mInterface.onEditItemClicked(adapterPosition);
                        break;
                    case R.id.menu_delete:
                        mInterface.onDeleteItemClicked(adapterPosition);
                        break;
                }
                return false;
            });

            popupMenu.show();
        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
    }
}

