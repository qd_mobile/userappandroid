package com.qd.user.ui.home.onlinepayment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;

import org.json.JSONException;
import org.json.JSONObject;

class OnlinePaymentRepository {

    void initializePayment(final RichMediatorLiveData<WalletRechargeResponse> walletRechargeLiveData, InitializePaymentRequest paymentRequest) {
        paymentRequest.setVendorId(DataManager.getInstance().getUserId());
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().initializePayment(new JSONObject(gson.toJson(paymentRequest))).enqueue(new NetworkCallback<WalletRechargeResponse>() {
                @Override
                public void onSuccess(WalletRechargeResponse walletRechargeResponse) {
                    walletRechargeLiveData.setValue(walletRechargeResponse);
                    walletRechargeLiveData.setLoadingState(false);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    walletRechargeLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    walletRechargeLiveData.setError(t);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
