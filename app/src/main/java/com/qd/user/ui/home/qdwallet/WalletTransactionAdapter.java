package com.qd.user.ui.home.qdwallet;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.wallet.Datum;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.ProgressLoader;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

class WalletTransactionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;
    private ArrayList<Object> mWalletTransactionList;


    WalletTransactionAdapter(ArrayList<Object> walletTransactionsList) {
        mWalletTransactionList = walletTransactionsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_qd_wallet, viewGroup, false);
            return new WalletTransactionAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (mWalletTransactionList.get(i) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mWalletTransactionList.get(i) instanceof Datum) {

            switch (((Datum) mWalletTransactionList.get(i)).getTransactionFor()) {
                case AppConstants.TransactionFor.ORDER:
                    ((WalletTransactionAdapterViewHolder) holder).tvOrderId.setText(String.format("#ID - %s", ((Datum) mWalletTransactionList.get(i)).getOrderData().get(0).getOrderUniqueId()));
                    ((WalletTransactionAdapterViewHolder) holder).tvTransactionType.setText(((Datum) mWalletTransactionList.get(i)).getOrderData().get(0).getOrderType());
                    break;
                case AppConstants.TransactionFor.WALLET_RECHARGE:
                    ((WalletTransactionAdapterViewHolder) holder).tvLabelOrderId.setText(R.string.s_transaction_id);
                    ((WalletTransactionAdapterViewHolder) holder).tvOrderId.setText(String.format("#ID - %s", ((Datum) mWalletTransactionList.get(i)).getTransactionId()));
                    ((WalletTransactionAdapterViewHolder) holder).tvTransactionType.setText(R.string.s_money_added);

                    break;
            }

            ((WalletTransactionAdapterViewHolder) holder).tvDay.setText(DateFormatter.getInstance().getFormattedDay(((Datum) mWalletTransactionList.get(i)).getCreatedAt()));
            ((WalletTransactionAdapterViewHolder) holder).tvDateTime.setText(String.format("%s, %s", DateFormatter.getInstance().getFormattedTime(((Datum) mWalletTransactionList.get(i)).getCreatedAt()), DateFormatter.getInstance().getFormattedDate(((Datum) mWalletTransactionList.get(i)).getCreatedAt())));

            if (((Datum) mWalletTransactionList.get(i)).getTransactionStatus().equals(AppConstants.TransactionStatus.SUCCESS)) {
                if (((Datum) mWalletTransactionList.get(i)).getTransactionType().equals(AppConstants.TransactionType.CREDIT)) {

                    ((WalletTransactionAdapterViewHolder) holder).tvAmount.setTextColor(((WalletTransactionAdapterViewHolder) holder).itemView.getResources()
                            .getColor(R.color.colorGreenishTeal));
                    ((WalletTransactionAdapterViewHolder) holder).tvAmount.setText(String.format(Locale.getDefault(), "+%.3f KWD", ((Datum) mWalletTransactionList.get(i)).getAmount()));
                } else {

                    ((WalletTransactionAdapterViewHolder) holder).tvAmount.setTextColor(((WalletTransactionAdapterViewHolder) holder).itemView.getResources()
                            .getColor(R.color.colorBlack));
                    ((WalletTransactionAdapterViewHolder) holder).tvAmount.setText(String.format(Locale.getDefault(), "-%.3f KWD", ((Datum) mWalletTransactionList.get(i)).getAmount()));

                }
            } else {
                ((WalletTransactionAdapterViewHolder) holder).tvAmount.setTextColor(((WalletTransactionAdapterViewHolder) holder).itemView.getResources()
                        .getColor(R.color.Red));
                ((WalletTransactionAdapterViewHolder) holder).tvAmount.setText(String.format(Locale.getDefault(), "%.3f KWD", ((Datum) mWalletTransactionList.get(i)).getAmount()));
            }

        }
    }

    @Override
    public int getItemCount() {
        return mWalletTransactionList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mWalletTransactionList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }

    //hide progress loader after getting paginated data
    void hideProgress() {
        mWalletTransactionList.remove(mWalletTransactionList.size() - 1);
        notifyDataSetChanged();
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mWalletTransactionList.add(new ProgressLoader());
        notifyItemInserted(mWalletTransactionList.size());
    }

    class WalletTransactionAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_label_order_id)
        TextView tvLabelOrderId;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_date_time)
        TextView tvDateTime;
        @BindView(R.id.tv_transaction_type)
        TextView tvTransactionType;
        @BindView(R.id.tv_amount)
        TextView tvAmount;

        WalletTransactionAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;


        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
