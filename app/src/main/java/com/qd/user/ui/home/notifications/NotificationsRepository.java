package com.qd.user.ui.home.notifications;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.notifications.NotificationsListingResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

class NotificationsRepository {

    void hitNotificationsApi(final RichMediatorLiveData<NotificationsListingResponse> notificationsLiveData, int page, String deviceId) {
        DataManager.getInstance().getNotificationsListing(createRequestPayload(page, deviceId)).enqueue(new NetworkCallback<NotificationsListingResponse>() {
            @Override
            public void onSuccess(NotificationsListingResponse notificationsListingResponse) {
                notificationsLiveData.setValue(notificationsListingResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                notificationsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                notificationsLiveData.setError(t);
            }
        });

    }

    private HashMap<String, Object> createRequestPayload(int page, String deviceId) {
        HashMap<String, Object> mRequestMap = new HashMap<>();

        mRequestMap.put(NetworkConstants.VENDOR_ID, DataManager.getInstance().getUserId());
        mRequestMap.put(NetworkConstants.KEY_PAGE, page);
        mRequestMap.put(NetworkConstants.KEY_LIMIT, 10);
        mRequestMap.put(NetworkConstants.KEY_DEVICE_ID, deviceId);
        mRequestMap.put(NetworkConstants.KEY_PLATFORM, AppConstants.ANDROID);

        return mRequestMap;
    }

    void markNotificationsAsRead(String id, boolean allRead, RichMediatorLiveData<CommonResponse> markReadLiveData) {
        DataManager.getInstance().markNotificationAsRead(createPayload(DataManager.getInstance().getUserId(), id, allRead)).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                markReadLiveData.setLoadingState(false);
                markReadLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                markReadLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                markReadLiveData.setError(t);
            }
        });
    }

    private JSONObject createPayload(String userId, String id, boolean allRead) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("vendorId", userId);
            jsonObject.put("notificationIds", id);
            jsonObject.put("allRead", allRead);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
