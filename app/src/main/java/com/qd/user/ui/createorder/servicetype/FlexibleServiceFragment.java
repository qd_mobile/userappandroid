package com.qd.user.ui.createorder.servicetype;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.servicetype.TimeSlot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FlexibleServiceFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {


    @BindView(R.id.tv_label_pickup_date)
    TextView tvLabelPickupDate;
    @BindView(R.id.tv_order_date)
    TextView tvOrderDate;
    @BindView(R.id.tv_morning_slot)
    TextView tvMorningSlot;
    @BindView(R.id.tv_evening_slot)
    TextView tvEveningSlot;
    @BindView(R.id.tv_error_time_slot)
    TextView tvErrorTimeSlot;
    @BindView(R.id.shimmer_slots)
    ShimmerFrameLayout shimmerSlots;
    private Unbinder unbinder;
    private IFlexibleServiceHost mHost;
    private ServiceTypeViewModel mServiceTypeViewModel;

    private List<TimeSlot> mSlotsList;
    private String mSlotId;
    private Calendar mCalendar;

    public FlexibleServiceFragment() {
        // Required empty public constructor
    }

    public static FlexibleServiceFragment getInstance(Parcelable createOrderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.ORDER_DATA, createOrderDetails);
        FlexibleServiceFragment flexibleServiceFragment = new FlexibleServiceFragment();
        flexibleServiceFragment.setArguments(bundle);

        return flexibleServiceFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_service_type_flexible, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof IFlexibleServiceHost) {
            mHost = (IFlexibleServiceHost) getParentFragment();
        } else throw new IllegalStateException("Host must implement IPremiumServiceHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mServiceTypeViewModel = new ViewModelProvider(this).get(ServiceTypeViewModel.class);
        mServiceTypeViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        showShimmerEffect();
        mServiceTypeViewModel.getFlexibleTimeSlots();
        observeLiveData();

    }

    private void observeLiveData() {
        //observe validations errors
        mServiceTypeViewModel.getFlexibleSlotsLiveData().observe(getViewLifecycleOwner(), flexibleSlotsResponse -> {
            hideShimmerEffect();
            if (flexibleSlotsResponse != null) {
                mSlotsList.addAll(flexibleSlotsResponse.getResult().get(0).getTimeSlots());
                setSlots();
            }
        });
    }

    private void setSlots() {
        if (mSlotsList.get(0).getIsActive() && mSlotsList.get(1).getIsActive()) {
            tvMorningSlot.setText(String.format("%s - %s", getFormattedTime(mSlotsList.get(0).getStartTime()), getFormattedTime(mSlotsList.get(0).getEndTime())));
            tvEveningSlot.setText(String.format("%s - %s", getFormattedTime(mSlotsList.get(1).getStartTime()), getFormattedTime(mSlotsList.get(1).getEndTime())));
        } else if (mSlotsList.get(0).getIsActive()) {
            tvMorningSlot.setText(String.format("%s - %s", getFormattedTime(mSlotsList.get(0).getStartTime()), getFormattedTime(mSlotsList.get(0).getEndTime())));
            tvEveningSlot.setVisibility(View.GONE);
        } else {
            tvEveningSlot.setText(String.format("%s - %s", getFormattedTime(mSlotsList.get(1).getStartTime()), getFormattedTime(mSlotsList.get(1).getEndTime())));
            tvMorningSlot.setVisibility(View.GONE);
        }

    }

    private String getFormattedTime(String date) {
        try {
            SimpleDateFormat spf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            spf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date newDate = spf.parse(date);
            spf = new SimpleDateFormat("h:mm a", Locale.getDefault());
            return spf.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void initViewsAndVariables() {
        mSlotsList = new ArrayList<>();
        mCalendar = Calendar.getInstance();
        mCalendar.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        tvOrderDate.setText(getFormattedDate(mCalendar.getTime()));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void openDatePicker() {
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        if (getContext() != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(calendar.getTime().getTime());
            datePickerDialog.show();
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mCalendar.set(year, month, dayOfMonth);
        Date date = mCalendar.getTime();
        tvOrderDate.setText(getFormattedDate(date));
    }

    /**
     * Formats the date in required format
     *
     * @param date Date to be formatted
     * @return String containing formatted date
     */
    private String getFormattedDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        return sdf.format(date);
    }


    @OnClick({R.id.tv_order_date, R.id.tv_morning_slot, R.id.tv_evening_slot})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_order_date:
                openDatePicker();
                break;
            case R.id.tv_morning_slot:
                selectTimeSlot(tvMorningSlot, tvEveningSlot);

                for (int i = 0; i < mSlotsList.size(); i++) {
                    mSlotsList.get(i).setSelected(i == 0);
                }

                if (validateTime()) {
                    if (mSlotsList.size() > 0)
                        mSlotId = mSlotsList.get(0).getId();
                }
                break;
            case R.id.tv_evening_slot:
                selectTimeSlot(tvEveningSlot, tvMorningSlot);

                for (int i = 0; i < mSlotsList.size(); i++) {
                    mSlotsList.get(i).setSelected(i == 1);
                }

                if (validateTime()) {
                    if (mSlotsList.size() > 1)
                        mSlotId = mSlotsList.get(1).getId();
                }
                break;
        }
    }


    private void selectTimeSlot(TextView selected, TextView unselected) {
        tvErrorTimeSlot.setVisibility(View.GONE);
        selected.setBackgroundResource(R.drawable.drawable_rectangle_circular_corner_solid_green);
        unselected.setBackgroundResource(R.drawable.drawable_grey_round_cornered_stroke);

        selected.setTextColor(getResources().getColor(R.color.colorWhite));
        unselected.setTextColor(getResources().getColor(R.color.colorBlack));

    }

    public String getTimeSlotId() {
        if (mSlotId != null)
            return mSlotId;
        else {
            tvErrorTimeSlot.setVisibility(View.VISIBLE);
            shakeLayout(tvErrorTimeSlot);
            return null;
        }
    }

    public String getScheduledStartTime() {
        return getScheduledDateTime(mCalendar.getTime());
    }

    private String getScheduledDateTime(Date time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(time);
    }


    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerSlots.stopShimmer();
        shimmerSlots.setVisibility(View.GONE);
        tvMorningSlot.setVisibility(View.VISIBLE);
        tvEveningSlot.setVisibility(View.VISIBLE);
    }

    private void showShimmerEffect() {
        tvMorningSlot.setVisibility(View.GONE);
        tvEveningSlot.setVisibility(View.GONE);
        shimmerSlots.setVisibility(View.VISIBLE);
        shimmerSlots.startShimmer();

    }

    public boolean validateTime() {

        Calendar calendar = Calendar.getInstance();

        for (int i = 0; i < mSlotsList.size(); i++) {
            if (mSlotsList.get(i).isSelected()) {
                calendar.setTime(getParsedDate(mSlotsList.get(i).getStartTime()));
                calendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
                mSlotId = mSlotsList.get(i).getId();
            }
        }

        if (calendar.getTime().before(new Date())) {
            tvErrorTimeSlot.setText(R.string.s_select_valid_time);
            tvErrorTimeSlot.setVisibility(View.VISIBLE);
            mSlotId = null;
            shakeLayout(tvErrorTimeSlot);
            return false;
        }

        return true;
    }

    private Date getParsedDate(String date) {
        Date dateTime = null;

        SimpleDateFormat spf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            dateTime = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateTime;
    }


    interface IFlexibleServiceHost {

    }

}
