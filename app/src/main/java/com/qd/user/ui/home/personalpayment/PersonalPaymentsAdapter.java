package com.qd.user.ui.home.personalpayment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.personal.Datum;
import com.qd.user.utils.ProgressLoader;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

class PersonalPaymentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;
    private ArrayList<Object> mPersonalPaymentsList;

    PersonalPaymentsAdapter(ArrayList<Object> ordersList) {
        mPersonalPaymentsList = ordersList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_payment_history, viewGroup, false);
            return new PersonalPaymentsAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mPersonalPaymentsList.get(position) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mPersonalPaymentsList.get(position) instanceof Datum) {
            Datum datum = (Datum) mPersonalPaymentsList.get(position);
            ((PersonalPaymentsAdapterViewHolder) holder).tvOrderId.setText(String.format("ID - #%s", datum.getOrderUniqueId()));
            if (datum.getPickupLocation().size() > 0) {
                ((PersonalPaymentsAdapterViewHolder) holder).tvPickupLocation.setText(datum.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

            }
            if (datum.getDropLocation().size() > 0) {
                ((PersonalPaymentsAdapterViewHolder) holder).tvDropoffLocation.setText(datum.getDropLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

            }
            if (datum.getScheduledStartTime() != null) {
                try {

                    SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
                    Date newDay = spf.parse(datum.getScheduledStartTime());
                    SimpleDateFormat dayFormatter = new SimpleDateFormat("EEEE", Locale.getDefault());
                    Date newDate = spf.parse(datum.getScheduledStartTime());
                    spf = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
                    SimpleDateFormat timeformatter = new SimpleDateFormat("h:mm a", Locale.getDefault());
                    ((PersonalPaymentsAdapterViewHolder) holder).tvDate.setText(spf.format(newDate));
                    ((PersonalPaymentsAdapterViewHolder) holder).tvTime.setText(timeformatter.format(newDate));
                    ((PersonalPaymentsAdapterViewHolder) holder).tvDay.setText(dayFormatter.format(newDay));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            ((PersonalPaymentsAdapterViewHolder) holder).tvOrderType.setText(datum.getOrderType());
            switch (datum.getOrderType()) {
                case AppConstants.OrderType.SEND:
                case AppConstants.OrderType.SPECIAL_SEND:
                    ((PersonalPaymentsAdapterViewHolder) holder).ivOrderType.setImageDrawable(((PersonalPaymentsAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_send));
                    break;
                case AppConstants.OrderType.GET:
                case AppConstants.OrderType.SPECIAL_GET:
                    ((PersonalPaymentsAdapterViewHolder) holder).ivOrderType.setImageDrawable(((PersonalPaymentsAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_get));
                    break;
                case AppConstants.OrderType.BUSINESS:
                    ((PersonalPaymentsAdapterViewHolder) holder).ivOrderType.setImageDrawable(((PersonalPaymentsAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_business));
                    break;
            }

            switch (datum.getPaymentDetails().getPaymentMode()) {
                case AppConstants.PaymentMode.WALLET:
                    ((PersonalPaymentsAdapterViewHolder) holder).tvPaymentMode.setText(datum.getPaymentDetails().getPaymentMode());
                    ((PersonalPaymentsAdapterViewHolder) holder).tvPaymentMode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_order_summary_qd_wallet_active, 0, 0, 0);
                    break;

                case AppConstants.PaymentMode.COD:
                    ((PersonalPaymentsAdapterViewHolder) holder).tvPaymentMode.setText(datum.getPaymentDetails().getPaymentMode());
                    ((PersonalPaymentsAdapterViewHolder) holder).tvPaymentMode.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_order_summary_cash, 0, 0, 0);
                    break;

            }

            ((PersonalPaymentsAdapterViewHolder) holder).tvOrderStatus.setText(R.string.s_payment_successful);

            if (datum.getPaymentDetails().getTotalItemCost() != null && !datum.getPaymentDetails().getTotalItemCost().equals(BigDecimal.valueOf(0))) {
                ((PersonalPaymentsAdapterViewHolder) holder).tvItemPrice.setText(String.format(Locale.getDefault(), "%.3f KWD", datum.getPaymentDetails().getTotalItemCost()));
            } else {
                ((PersonalPaymentsAdapterViewHolder) holder).tvItemPrice.setVisibility(View.GONE);
                ((PersonalPaymentsAdapterViewHolder) holder).tvLabelItemPrice.setVisibility(View.GONE);
                ((PersonalPaymentsAdapterViewHolder) holder).viewItemPriceDivider.setVisibility(View.GONE);

            }
            ((PersonalPaymentsAdapterViewHolder) holder).tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", datum.getPaymentDetails().getDeliveryCharge()));

        }

    }

    @Override
    public int getItemCount() {
        return mPersonalPaymentsList.size();
    }

    //hide progress loader after getting paginated data
    public void hideProgress() {
        mPersonalPaymentsList.remove(mPersonalPaymentsList.size() - 1);
        notifyDataSetChanged();
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mPersonalPaymentsList.add(new ProgressLoader());
        notifyItemInserted(mPersonalPaymentsList.size());
    }

    @Override
    public int getItemViewType(int position) {
        if (mPersonalPaymentsList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }


    class PersonalPaymentsAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_label_order_id)
        TextView tvLabelOrderId;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.view_date_and_time)
        View viewDateAndTime;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.view_my_order)
        View viewMyOrder;
        @BindView(R.id.iv_order_type)
        ImageView ivOrderType;
        @BindView(R.id.tv_order_type)
        TextView tvOrderType;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.iv_pickup_location)
        ImageView ivPickupLocation;
        @BindView(R.id.tv_label_pickup_location)
        TextView tvLabelPickupLocation;
        @BindView(R.id.tv_pickup_location)
        TextView tvPickupLocation;
        @BindView(R.id.view_dotted_line)
        View viewDottedLine;
        @BindView(R.id.iv_drop_off_location)
        ImageView ivDropoffLocation;
        @BindView(R.id.tv_label_drop_off_location)
        TextView tvLabelDropoffLocation;
        @BindView(R.id.tv_drop_off_location)
        TextView tvDropoffLocation;
        @BindView(R.id.rl_location_info)
        RelativeLayout rlLocationInfo;
        @BindView(R.id.view_payment_mode_divider)
        View viewPaymentModeDivider;
        @BindView(R.id.tv_payment_mode)
        TextView tvPaymentMode;
        @BindView(R.id.tv_label_payment_mode)
        TextView tvLabelPaymentMode;
        @BindView(R.id.tv_label_item_price)
        TextView tvLabelItemPrice;
        @BindView(R.id.tv_label_delivery_charges)
        TextView tvLabelDeliveryCharges;
        @BindView(R.id.view_item_price_divider)
        View viewItemPriceDivider;
        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;
        @BindView(R.id.view_delivery_charges_divider)
        View viewDeliveryChargesDivider;
        @BindView(R.id.tv_delivery_charges)
        TextView tvDeliveryCharges;
        @BindView(R.id.guidelineStart)
        Guideline guidelineStart;
        @BindView(R.id.guidelineEnd)
        Guideline guidelineEnd;

        PersonalPaymentsAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {

            });
        }
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @NonNull
        private final View itemView;
        @BindView(R.id.progress)
        ProgressBar progress;


        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);

        }
    }
}

