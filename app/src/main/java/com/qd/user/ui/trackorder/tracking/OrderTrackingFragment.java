package com.qd.user.ui.trackorder.tracking;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.orderdetail.DropLocation;
import com.qd.user.model.orderdetail.PickupLocation;
import com.qd.user.model.orderdetail.Result;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.maps.DirectionsApi;
import com.qd.user.utils.maps.IDirectionsParserInterface;
import com.qd.user.utils.maps.LatLngInterpolator;
import com.qd.user.utils.maps.ParserTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderTrackingFragment extends BaseFragment implements OnMapReadyCallback, IDirectionsParserInterface, AppUtils.ISuccess {

    private static final int LAT_LNG_ZOOM_LEVEL = 15;
    private static final int ANIMATE_DURATION = 2000;
    private static final float MARKER_POSITION = 0.5f;
    private static final int ANIM_DELAY_DURATION = 3000;
    private static final float ZOOM_LEVEL = 12;
    private static final int MAP_PADDING = 150;

    @BindView(R.id.fl_map_container)
    FrameLayout flMapContainer;
    @BindView(R.id.iv_driver_image)
    ImageView ivDriverImage;
    @BindView(R.id.tv_driver_name)
    TextView tvDriverName;
    @BindView(R.id.tv_driver_vehicle)
    TextView tvDriverVehicle;
    @BindView(R.id.tv_pickup_location)
    TextView tvPickupLocation;
    @BindView(R.id.view_driver_details)
    View viewDriverDetails;
    @BindView(R.id.tv_label_order_status)
    TextView tvLabelOrderStatus;
    @BindView(R.id.tv_order_status)
    Button tvOrderStatus;
    @BindView(R.id.view_order_status)
    View viewOrderStatus;
    @BindView(R.id.tv_in_progress)
    TextView tvCancelOrder;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.cl_tracking_info)
    ConstraintLayout clTrackingInfo;
    private Unbinder unbinder;
    @BindView(R.id.iv_call)
    ImageView ivCall;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.tv_eta)
    TextView tvEta;


    /**
     * A {@link IOrderTrackingHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IOrderTrackingHost mOrderTrackingHost;
    private OrderTrackingViewModel mOrderTrackingViewModel;
    private GoogleMap mGoogleMap;
    private Result mResult;
    private DirectionsApi mDirectionsApiReq;
    private LatLng mOrigin;
    private LatLng mDestination;
    private Polyline mPolyLine;
    private MarkerOptions mStartMarkerOptions;
    private MarkerOptions mDropMarkerOptions;
    private Marker mStartMarker;
    private Marker mDestinationMarker;
    private String mOrderId;
    private Animation clickAnimation;
    private long mTimeStamp;


    public static OrderTrackingFragment getInstance(String orderId, boolean isFromReceived) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.ORDER_ID, orderId);
        bundle.putBoolean(AppConstants.IS_ORDER_RECEIVED, isFromReceived);
        OrderTrackingFragment orderTrackingFragment = new OrderTrackingFragment();
        orderTrackingFragment.setArguments(bundle);
        return orderTrackingFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IOrderTrackingHost) {
            mOrderTrackingHost = (IOrderTrackingHost) context;
        } else
            throw new IllegalStateException("host must implement IOrderTrackingHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tracking, container, false);
        mOrderTrackingHost.enterFullScreen();

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mOrderTrackingViewModel = new ViewModelProvider(this).get(OrderTrackingViewModel.class);
        mOrderTrackingViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        getArgumentsData();
        observeLiveData();
    }

    private void observeLiveData() {
        mOrderTrackingViewModel.getOrderDetailsLiveData().observe(getViewLifecycleOwner(), orderDetailResponse -> {
            if (orderDetailResponse != null) {
                clTrackingInfo.setVisibility(View.VISIBLE);
                ivCall.setVisibility(View.VISIBLE);
                setDetails(orderDetailResponse.getResult().get(0));
            }
            hideProgressDialog();
        });

        mOrderTrackingViewModel.getDirectionsApiLiveData().observe(getViewLifecycleOwner(), directionsApiResponse -> {
            if (directionsApiResponse != null) {
                new ParserTask(OrderTrackingFragment.this).execute(new Gson().toJson(directionsApiResponse));
            }
        });

        mOrderTrackingViewModel.getOrderCancellationLiveData().observe(getViewLifecycleOwner(), orderCancellationResponse -> {
            if (orderCancellationResponse != null) {
                showSnackBar(getString(R.string.s_order_cancelled));
                mOrderTrackingHost.onBackPressed();
            }
        });

        mOrderTrackingViewModel.getCurrentLocationResponseLiveData().observe(getViewLifecycleOwner(), currentLocationResponse -> {
            if (currentLocationResponse != null && mDestination != null && currentLocationResponse.getOrderId().equals(mOrderId)) {
                mOrigin = new LatLng(currentLocationResponse.getCurrentLocationViaSocket().getLatitude(), currentLocationResponse.getCurrentLocationViaSocket().getLongitude());
                List<LatLng> dropsList = new ArrayList<>();
                dropsList.add(mDestination);

                if (currentLocationResponse.getTimeStamp() > mTimeStamp) {
                    mOrderTrackingViewModel.getDirectionsResponse(mDirectionsApiReq.directionRequestUrl(mOrigin, dropsList, false));
                    if (!currentLocationResponse.getETA().isEmpty())
                        tvEta.setText(String.format("%s away", currentLocationResponse.getETA()));
                }
                tvOrderStatus.setText(setUpdatedStatus(currentLocationResponse.getCurrentOrderStatus()));
                mTimeStamp = currentLocationResponse.getTimeStamp();

                switch (currentLocationResponse.getCurrentOrderStatus()) {
                    case AppConstants.OrderStatus.DELIVERED:
                        tvEta.setVisibility(View.GONE);
                        mOrderTrackingViewModel.disconnectSocketListenerForCurrentLocation();
                        AppUtils.getInstance().openSuccessDialog(getContext(), "Your order has been delivered.", this);
                        break;
                    case AppConstants.OrderStatus.PICKEDUP:
                    case AppConstants.DriverTrackingStatus.ORDER_PICKEDUP:
                        tvEta.setVisibility(View.GONE);
                        break;
                    default:
                        tvEta.setVisibility(View.VISIBLE);
                }


                if (currentLocationResponse.getDropLocation() != null) {
                    mResult.setDropLocation(currentLocationResponse.getDropLocation());
                    mResult.getDropLocation().get(0).setIsElementInprogress(true);
                    changeViews(currentLocationResponse.getDropLocation().get(0));
                }
            }
        });
    }

    private void changeViews(DropLocation dropLocation) {
        setDriverDetailsForDrop(dropLocation);
        mDestination = new LatLng(dropLocation.getGeometry().getCoordinates().get(1),
                dropLocation.getGeometry().getCoordinates().get(0));
    }


    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ORDER_ID) && getArguments().getString(AppConstants.ORDER_ID) != null) {
            mOrderId = getArguments().getString(AppConstants.ORDER_ID);
            if (getArguments().getBoolean(AppConstants.IS_ORDER_RECEIVED)) {
                tvCancelOrder.setVisibility(View.GONE);
            }
            showProgressDialog();
            mOrderTrackingViewModel.getOrderDetail(mOrderId);
        }
    }

    @Override
    protected void initViewsAndVariables() {
        mDirectionsApiReq = new DirectionsApi();
        clickAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.scale);
        clTrackingInfo.setVisibility(View.GONE);
        ivCall.setVisibility(View.GONE);

        mTimeStamp = new Date().getTime();


        setMap();
    }

    @Override
    public void onStart() {
        super.onStart();
        listenToDriverCurrentLocation();
    }

    @Override
    public void onPause() {
        super.onPause();
        mOrderTrackingViewModel.disconnectSocketListenerForCurrentLocation();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mResult != null) {
            if (currentElementInProgress() != -1 && !mResult.getOrderStatus().get(mResult.getOrderStatus().size() - 1).getStatus().equals(AppConstants.OrderStatus.CASHBACK_INITIATED)) {
                mOrderTrackingViewModel.emitToListenDriverCurrentLocation(mResult.getId(), mResult.getDropLocation().get(currentElementInProgress()).getDeliveryDriverId());
                setDriverDetailsForDrop(mResult.getDropLocation().get(currentElementInProgress()));
            } else {
                mOrderTrackingViewModel.emitToListenDriverCurrentLocation(mResult.getId(), mResult.getPickupLocation().get(0).getPickupDriverId());
                setDriverDetailsForPickup(mResult.getPickupLocation().get(0));
            }

            listenToDriverCurrentLocation();
        }
    }

    /**
     * Sets order details corresponding to the current order
     *
     * @param result Data of current order
     */
    private void setDetails(Result result) {
        mResult = result;
        listenToDriverCurrentLocation();

        if (result.getETAstring() != null && !result.getETAstring().isEmpty())
            tvEta.setText(String.format("%s away", result.getETAstring()));
        if (currentElementInProgress() != -1 && !mResult.getOrderStatus().get(mResult.getOrderStatus().size() - 1).getStatus().equals(AppConstants.OrderStatus.CASHBACK_INITIATED)) {
            mDestination = new LatLng(mResult.getDropLocation().get(currentElementInProgress()).getGeometry().getCoordinates().get(1),
                    mResult.getDropLocation().get(currentElementInProgress()).getGeometry().getCoordinates().get(0));
            mOrderTrackingViewModel.emitToListenDriverCurrentLocation(result.getId(), result.getDropLocation().get(currentElementInProgress()).getDeliveryDriverId());
            setDriverDetailsForDrop(result.getDropLocation().get(currentElementInProgress()));
        } else {
            mDestination = new LatLng(mResult.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                    mResult.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
            mOrderTrackingViewModel.emitToListenDriverCurrentLocation(result.getId(), result.getPickupLocation().get(0).getPickupDriverId());
            setDriverDetailsForPickup(result.getPickupLocation().get(0));
        }

        List<LatLng> dropsList = new ArrayList<>();
        dropsList.add(mDestination);
        mOrigin = new LatLng(mResult.getDriverObj().get(0).getCurrentLocationViaSocket().getLatitude(), mResult.getDriverObj().get(0).getCurrentLocationViaSocket().getLongitude());
        mOrderTrackingViewModel.getDirectionsResponse(mDirectionsApiReq.directionRequestUrl(mOrigin, dropsList, false));

    }

    private String setUpdatedStatus(String status) {
        switch (status) {
            case AppConstants.DriverTrackingStatus.UNALIGNED:
                return AppConstants.OrderTrackingStatus.ORDER_CREATED;

            case AppConstants.DriverTrackingStatus.ACCEPT_ORDER:
                return AppConstants.OrderTrackingStatus.DRIVER_ACEEPTED;

            case AppConstants.DriverTrackingStatus.DRIVE_TO_PICKEDUP:
                return AppConstants.OrderTrackingStatus.DRIVING_TO_PICKUP;

            case AppConstants.DriverTrackingStatus.REACHED_AT_PICKUP:
                return AppConstants.OrderTrackingStatus.ARRIVED_AT_PICKUP;

            case AppConstants.DriverTrackingStatus.ORDER_PICKEDUP:
                return AppConstants.OrderTrackingStatus.ORDER_PICKEDUP;

            case AppConstants.DriverTrackingStatus.DRIVE_TO_DROP_LOCATION:
                return AppConstants.OrderTrackingStatus.DRIVING_TO_DROP_OFF;

            case AppConstants.DriverTrackingStatus.REACHED_TO_DROP_LOCATION:
                return AppConstants.OrderTrackingStatus.ARRIVED_AT_DROP_OFF;

            case AppConstants.DriverTrackingStatus.DROP_COMPLETE:
                return AppConstants.OrderTrackingStatus.ORDER_DELIVERED;

            default:
                return status;

        }
    }

   /* private void manageAddressUpdate(List<OrderStatus> orderStatus) {
        switch (orderStatus.get(orderStatus.size()-1).getStatus()){
            case AppConstants.OrderStatus.DRIVER_ALIGNED:
            case AppConstants.OrderStatus.OUT_FOR_PICKUP:
                openConfirmationDialogForAddressUpdate();
                break;
                default:
                    tvChange.setVisibility(View.GONE);
        }
    }

    private void openConfirmationDialogForAddressUpdate() {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.dialog_log_out);
            AppUtils.getInstance().dimDialogBackground(dialog);

            final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
            final Button btnSubmit = dialog.findViewById(R.id.btn_logout);
            final TextView tvMessage = dialog.findViewById(R.id.tv_message);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  mOrderTrackingHost.navigateToSelectAddressFragment();
                }
            });

            dialog.show();
        }
    }*/

    /**
     * Determines the current element in progress, returns -1 if pickup is pending,
     * otherwise returns the position of current drop in progress.
     *
     * @return position of current drop or -1, if pickup is pending
     */
    private int currentElementInProgress() {
        int currentElement = -1;

        if (mResult.getDropLocation().get(0).getIsElementInprogress()) {
            return 0;
        }

        for (int i = 0; i < mResult.getOrderStatus().size(); i++) {
            if (mResult.getOrderStatus().get(i).getStatus().equals(AppConstants.OrderStatus.PICKEDUP)) {
                for (int j = 0; j < mResult.getDropLocation().size(); j++) {
                    if (mResult.getDropLocation().get(j).getIsElementInprogress()) {
                        currentElement = j;
                    }
                }
                break;
            }
        }

        return currentElement;
    }

    private void setDriverDetailsForPickup(PickupLocation pickupLocation) {
        if (getContext() != null)
            Glide.with(getContext()).load(pickupLocation.getPickupDriverImage())
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_profile_placeholder))
                    .into(ivDriverImage);

        tvOrderStatus.setText(setUpdatedStatus(pickupLocation.getDriverTrackingStatus().get(pickupLocation.getDriverTrackingStatus().size() - 1).getStatus()));

        tvDriverName.setText(pickupLocation.getPickupDriverName());
        tvDriverVehicle.setText(pickupLocation.getPickupDriverVehicle());
        tvPickupLocation.setText(pickupLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
    }


    private void setDriverDetailsForDrop(DropLocation dropLocation) {
        if (getContext() != null)
            Glide.with(getContext()).load(dropLocation.getDeliveryDriverImage())
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_profile_placeholder))
                    .into(ivDriverImage);

        tvDriverName.setText(dropLocation.getDeliveryDriverName());
        tvDriverVehicle.setText(dropLocation.getDeliveryDriverVehicle());
        tvPickupLocation.setText(dropLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (dropLocation.getDriverTrackingStatus() != null) {
            tvOrderStatus.setText(setUpdatedStatus(dropLocation.getDriverTrackingStatus().get(dropLocation.getDriverTrackingStatus().size() - 1).getStatus()));
            if (dropLocation.getDriverTrackingStatus().get(dropLocation.getDriverTrackingStatus().size() - 1).getStatus().equals(AppConstants.DriverTrackingStatus.DROP_COMPLETE)) {
                mOrderTrackingViewModel.disconnectSocketListenerForCurrentLocation();
                AppUtils.getInstance().openSuccessDialog(getContext(), "Your order has been delivered.", this);
            }
        }
    }


    /**
     * method to setup map initially
     */
    private void setMap() {
        final SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        addFragmentWithoutAnimation(R.id.fl_map_container, mapFragment, null);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        moveToCurrentLocation(new LatLng(AppConstants.KUWAIT_CENTER_LAT, AppConstants.KUWAIT_CENTER_LONG));


        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.setPadding(MAP_PADDING, MAP_PADDING, MAP_PADDING, MAP_PADDING);

    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        if (mGoogleMap != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, LAT_LNG_ZOOM_LEVEL));
            // Zoom in, animating the camera.
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
            // Zoom out to zoom level 12, animating with a duration of 2 seconds.
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL), ANIMATE_DURATION, null);
        }

    }


    @OnClick({R.id.tv_order_status, R.id.tv_in_progress, R.id.iv_call, R.id.tv_pickup_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
          /*  case R.id.tv_change:
                opensDialogToChangeDropLocation();
                break;*/
            case R.id.tv_order_status:
                clickAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mOrderTrackingHost.openOrderStatusFragment(mResult.getOrderUniqueId());
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                tvOrderStatus.startAnimation(clickAnimation);
                mOrderTrackingViewModel.disconnectSocketListenerForCurrentLocation();

                break;
            case R.id.tv_in_progress:
                if (tvOrderStatus.getText().toString().equals(AppConstants.OrderStatus.ORDER_CREATED) && !mResult.getIsLater()) {
                    mOrderTrackingViewModel.autoCancelOrder(mResult.getId());
                } else
                    openOrderCancellationDialog();
                break;
            case R.id.tv_pickup_location:
                showFullAddress();
                break;
            case R.id.iv_call:
                callDriver();
                break;
        }
    }

    private void showFullAddress() {
        if (getContext() != null) {
            final Dialog addressDialog = new Dialog(getContext(), R.style.customDialog);
            addressDialog.setContentView(R.layout.dialog_view_address);
            addressDialog.setCancelable(true);

            if (currentElementInProgress() != -1)
                ((TextView) addressDialog.findViewById(R.id.tv_area)).setText(mResult.getDropLocation().get(currentElementInProgress()).getArea());
            else
                ((TextView) addressDialog.findViewById(R.id.tv_area)).setText(mResult.getPickupLocation().get(0).getArea());

            if (currentElementInProgress() != -1)
                ((TextView) addressDialog.findViewById(R.id.tv_block)).setText(mResult.getDropLocation().get(currentElementInProgress()).getBlockNumber());
            else
                ((TextView) addressDialog.findViewById(R.id.tv_block)).setText(mResult.getPickupLocation().get(0).getBlockNumber());

            String street;
            if (currentElementInProgress() != -1)
                street = mResult.getDropLocation().get(currentElementInProgress()).getStreet();
            else street = mResult.getPickupLocation().get(0).getStreet();

            if (street != null && !street.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_street)).setText(street);
            else {
                addressDialog.findViewById(R.id.tv_street).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_street).setVisibility(View.GONE);
            }

            String houseOrBuilding;
            if (currentElementInProgress() != -1)
                houseOrBuilding = mResult.getDropLocation().get(currentElementInProgress()).getHouseOrbuilding();
            else houseOrBuilding = mResult.getPickupLocation().get(0).getHouseOrbuilding();
            if (houseOrBuilding != null && !houseOrBuilding.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_house_building)).setText(houseOrBuilding);
            else {
                addressDialog.findViewById(R.id.tv_house_building).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_house_building).setVisibility(View.GONE);
            }

            String floor;
            if (currentElementInProgress() != -1)
                floor = mResult.getDropLocation().get(currentElementInProgress()).getFloor();
            else floor = mResult.getPickupLocation().get(0).getFloor();
            if (floor != null && !floor.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_floor)).setText(floor);
            else {
                addressDialog.findViewById(R.id.tv_floor).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_floor).setVisibility(View.GONE);
            }

            String apartmentOrOffice;
            if (currentElementInProgress() != -1)
                apartmentOrOffice = mResult.getDropLocation().get(currentElementInProgress()).getApartmentOrOffice();
            else apartmentOrOffice = mResult.getPickupLocation().get(0).getApartmentOrOffice();
            if (apartmentOrOffice != null && !apartmentOrOffice.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_apartment)).setText(apartmentOrOffice);
            else {
                addressDialog.findViewById(R.id.tv_apartment).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_apartment).setVisibility(View.GONE);
            }

            String driverNote;
            if (currentElementInProgress() != -1)
                driverNote = mResult.getDropLocation().get(currentElementInProgress()).getDriverNote();
            else driverNote = mResult.getPickupLocation().get(0).getPickupInstructions();
            if (driverNote != null && !driverNote.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_note)).setText(driverNote);
            else {
                addressDialog.findViewById(R.id.tv_label_note).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_note).setVisibility(View.GONE);
            }

            AppUtils.getInstance().dimDialogBackground(addressDialog);
            addressDialog.show();
        }
    }

    private void callDriver() {
        if (currentElementInProgress() != -1) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mResult.getDropLocation().get(currentElementInProgress()).getDeliveryDriverMobileNo(), null));
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mResult.getPickupLocation().get(0).getPickupDriverMobileNo(), null));
            startActivity(intent);
        }
    }

   /* private void opensDialogToChangeDropLocation() {
        AppUtils.getInstance().openConfirmationDialog(getContext(), getString(R.string.s_are_you_sure_you_want_to_change_drop_location), this);
    }*/

    /**
     * Opens dialog for listing the reason to cancel the particular order.
     * Only one of the given reasons can be selected at a time and an additional edit text is provided for description.
     */
    private void openOrderCancellationDialog() {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.layout_cancel_order);
            AppUtils.getInstance().dimDialogBackground(dialog);

            final RadioGroup rgReasons = dialog.findViewById(R.id.rg_reasons);
            final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
            final Button btnSubmit = dialog.findViewById(R.id.btn_submit);
            final EditText etDescription = dialog.findViewById(R.id.et_message);
            final TextView tvError = dialog.findViewById(R.id.tv_error);

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            List<com.qd.user.model.commomresponse.Result> reasons = DataManager.getInstance().getCancelReasons();

            for (int i = 0; i < reasons.size(); i++) {
                RadioButton button = new RadioButton(getContext());
                button.setId(i);
                button.setText(reasons.get(i).getReason());
                rgReasons.addView(button);
            }

            btnSubmit.setOnClickListener(v -> {
                if (rgReasons.getCheckedRadioButtonId() != -1) {
                    if (reasons.get(rgReasons.getCheckedRadioButtonId()).getDescriptionMandatory() && etDescription.getText().toString().isEmpty()) {
                        tvError.setText(R.string.s_please_describe_reason_for_cancellation);
                        tvError.setVisibility(View.VISIBLE);
                        shakeLayout(tvError);
                    } else {
                        String reportIssue = null;
                        tvError.setVisibility(View.GONE);
                        reportIssue = reasons.get(rgReasons.getCheckedRadioButtonId()).getReason();

                        mOrderTrackingViewModel.cancelOrder(requestPayloadForOrderCancellation(reportIssue, etDescription.getText().toString()));
                        dialog.dismiss();
                    }
                } else {
                    tvError.setText(R.string.s_please_select_a_reason_for_cancellation);
                    tvError.setVisibility(View.VISIBLE);
                    shakeLayout(tvError);
                }
            });

            dialog.show();
        }
    }

    private JSONObject requestPayloadForOrderCancellation(String reportIssue, String description) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("requestFrom", mResult.getOrderType().equals(getString(R.string.business)) ? getString(R.string.business) : getString(R.string.cash_client));
            jsonObject.put("orderId", mResult.getId());
            jsonObject.put("elementId", currentElementInProgress() == -1 ? mResult.getPickupLocation().get(0).getId() : mResult.getDropLocation().get(currentElementInProgress()).getId());
            jsonObject.put("reason", reportIssue);
            if (description != null)
                jsonObject.put("description", description);
            jsonObject.put("isElement", currentElementInProgress() != -1);

            if (currentElementInProgress() != -1) {
                jsonObject.put("driverId", mResult.getDropLocation().get(currentElementInProgress()).getDeliveryDriverId());
            } else if (mResult.getPickupLocation().get(0).getPickupDriverId() != null) {
                jsonObject.put("driverId", mResult.getPickupLocation().get(0).getPickupDriverId());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }


    @Override
    public void setPolyLineOptions(PolylineOptions polyLineOptions) {
        if (mGoogleMap != null) {

            if (mStartMarkerOptions == null) {
                switch (mResult.getVehicleType()) {
                    case AppConstants.DeliveryVehicles.CAR:
                        mStartMarkerOptions = new MarkerOptions().position(mOrigin).anchor(MARKER_POSITION, MARKER_POSITION).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_vehicle_car));
                        break;
                    case AppConstants.DeliveryVehicles.BIKE:
                        mStartMarkerOptions = new MarkerOptions().position(mOrigin).anchor(MARKER_POSITION, MARKER_POSITION).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_vehicle_bike));
                        break;
                    case AppConstants.DeliveryVehicles.COOLER:
                        mStartMarkerOptions = new MarkerOptions().position(mOrigin).anchor(MARKER_POSITION, MARKER_POSITION).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_vehicle_cooler));
                        break;
                    case AppConstants.DeliveryVehicles.VAN:
                        mStartMarkerOptions = new MarkerOptions().position(mOrigin).anchor(MARKER_POSITION, MARKER_POSITION).
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_vehicle_van));
                        break;
                }

                mStartMarker = mGoogleMap.addMarker(mStartMarkerOptions);
                mStartMarker.showInfoWindow();
            }

            if (mDropMarkerOptions == null) {
                mDropMarkerOptions = new MarkerOptions().position(mDestination).anchor(MARKER_POSITION, MARKER_POSITION).
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_drop));
                mDestinationMarker = mGoogleMap.addMarker(mDropMarkerOptions);
                mDestinationMarker.showInfoWindow();
            }
            if (mPolyLine != null)
                mPolyLine.remove();

            mPolyLine = mGoogleMap.addPolyline(polyLineOptions);

            animateMarker(polyLineOptions.getPoints().get(0), mStartMarker);
            animateMarker(polyLineOptions.getPoints().get(polyLineOptions.getPoints().size() - 1), mDestinationMarker);

            //fit to bound
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(mOrigin);


            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(new CameraPosition.Builder().target(mOrigin).zoom(ZOOM_LEVEL).build()));


        }
    }

    /**
     * Animates marker on change in start and end locations
     * Also, rotates marker {@code marker.setRotation(start.bearingTo(end))}
     *
     * @param destination Location where marker is to set
     * @param marker      Marker
     */
    private void animateMarker(final LatLng destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(ANIM_DELAY_DURATION); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(animation -> {
                try {
                    float v = animation.getAnimatedFraction();
                    LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, destination);
                    marker.setPosition(newPosition);
                    Location start = new Location("");
                    Location end = new Location("");
                    start.setLatitude(startPosition.latitude);
                    start.setLongitude(startPosition.longitude);
                    end.setLatitude(destination.latitude);
                    end.setLongitude(destination.longitude);
                    marker.setRotation(start.bearingTo(end));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            valueAnimator.start();
        }
    }


    @Override
    public void showNoRouteFound() {
        Log.e(OrderTrackingFragment.class.getName(), "");
        //showSnackBar(getString(R.string.s_no_route_found));
    }

    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
    }

    private void listenToDriverCurrentLocation() {
        mOrderTrackingViewModel.listenForDriverCurrentLocation();
    }

    public void updateOrderStatus(String orderId) {
        showProgressDialog();
        mOrderTrackingViewModel.getOrderDetail(orderId);
        listenToDriverCurrentLocation();
    }

    @Override
    public void onOkViewClicked() {
        mOrderTrackingHost.backToOrderDetailsFragment(mOrderId);
    }

    public interface IOrderTrackingHost {

        void onBackPressed();

        void enterFullScreen();

        void openOrderStatusFragment(String orderUniqueId);

        void backToOrderDetailsFragment(String mOrderId);
    }
}


