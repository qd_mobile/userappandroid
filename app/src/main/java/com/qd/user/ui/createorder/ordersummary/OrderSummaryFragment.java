package com.qd.user.ui.createorder.ordersummary;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.response.PickupLocation;
import com.qd.user.model.createorder.response.Result;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.paymentmode.PaymentModeType;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.DateFormatter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderSummaryFragment extends BaseFragment implements PaymentModeAdapter.IPaymentMode, AppUtils.ISuccess, AppUtils.IConfirmationDialog {
    private static final int IMAGE_PADDING = 26;
    private static final int INSUFFICIENT_WALLET_FAILURE = 42215;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.iv_item)
    ImageView ivItem;
    @BindView(R.id.tv_label_item)
    TextView tvLabelItem;
    @BindView(R.id.tv_label_eta)
    TextView tvLabelEta;
    @BindView(R.id.iv_drop_location)
    ImageView ivDropLocation;
    @BindView(R.id.tv_label_drop_location)
    TextView tvLabelDropLocation;
    @BindView(R.id.tv_drop_location)
    TextView tvDropLocation;
    @BindView(R.id.iv_note_for_driver)
    ImageView ivNoteForDriver;
    @BindView(R.id.tv_label_note_for_driver)
    TextView tvLabelNoteForDriver;
    @BindView(R.id.tv_note_for_driver)
    TextView tvNoteForDriver;
    @BindView(R.id.tv_label_recipient_details)
    TextView tvLabelRecipientDetails;
    @BindView(R.id.tv_label_payment_mode)
    TextView tvLabelPaymentMode;
    @BindView(R.id.tv_apply)
    TextView tvApply;
    @BindView(R.id.tv_label_promo_code)
    EditText tvLabelPromoCode;
    @BindView(R.id.tv_label_promo_code_applied)
    TextView tvLabelPromoCodeApplied;
    @BindView(R.id.tv_promo_code)
    TextView tvPromoCode;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.cl_promo_code_info)
    ConstraintLayout clPromoCodeInfo;
    @BindView(R.id.tv_label_delivery_charges)
    TextView tvLabelDeliveryCharges;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.tv_label_promo_applied)
    TextView tvLabelPromoApplied;
    @BindView(R.id.tv_promo_applied)
    TextView tvPromoApplied;
    @BindView(R.id.view_dotted_line)
    View viewDottedLine;
    @BindView(R.id.tv_label_amount_to_be_paid)
    TextView tvLabelAmountToBePaid;
    @BindView(R.id.tv_amount_to_be_paid)
    TextView tvAmountToBePaid;
    @BindView(R.id.btn_confirm_order)
    Button btnConfirmOrder;
    @BindView(R.id.tv_item_name)
    TextView tvItemName;
    @BindView(R.id.tv_label_name)
    TextView tvLabelName;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_label_phone_number)
    TextView tvLabelPhoneNumber;
    @BindView(R.id.tv_phone_number)
    TextView tvPhoneNumber;
    @BindView(R.id.tv_label_email)
    TextView tvLabelEmail;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.cl_recipient_details)
    ConstraintLayout clRecipientDetails;
    @BindView(R.id.rv_payment_type)
    RecyclerView rvPaymentType;
    @BindView(R.id.view_apply_promo)
    View viewApplyPromo;
    @BindView(R.id.tv_label_return_charges)
    TextView tvLabelReturnCharges;
    @BindView(R.id.tv_return_charges)
    TextView tvReturnCharges;
    @BindView(R.id.tv_more_drops)
    TextView tvMoreDrops;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.iv_item_price)
    ImageView ivItemPrice;
    @BindView(R.id.tv_label_item_price)
    TextView tvLabelItemPrice;
    @BindView(R.id.tv_item_price)
    TextView tvItemPrice;
    @BindView(R.id.switch_bear_delivery_charges)
    Switch switchBearDeliveryCharges;
    @BindView(R.id.tv_error_insufficient_balance)
    TextView tvErrorInsufficientBalance;
    @BindView(R.id.cl_schedule)
    ConstraintLayout clSchedule;
    @BindView(R.id.tv_schedule_start_time)
    TextView tvScheduleStartTime;
    @BindView(R.id.tv_label_scheduled)
    TextView tvLabelScheduled;
    @BindView(R.id.btn_edit_schedule)
    Button btnEditSchedule;
    @BindView(R.id.tv_label_pending_charges)
    TextView tvLabelPendingCharges;
    @BindView(R.id.tv_pending_charges)
    TextView tvPendingCharges;
    @BindView(R.id.iv_pick_location)
    ImageView ivPickLocation;
    @BindView(R.id.tv_label_pick_location)
    TextView tvLabelPickLocation;
    @BindView(R.id.tv_pick_location)
    TextView tvPickLocation;
    private Unbinder unbinder;
    private OrderSummaryViewModel mOrderSummaryViewModel;
    private CreateOrderRequest mCreateOrderDetails;
    private IOrderSummaryHost mHost;
    private ArrayList<PaymentModeType> mPaymentModeList;
    private BigDecimal mDiscountedDeliveryCharges;
    private BigDecimal mWalletAmount;
    private double mPreviousPendingBal;
    private boolean isFromViewOrder;
    private PaymentModeAdapter mPaymentModeAdapter;

    public static OrderSummaryFragment getInstance(Parcelable orderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, orderDetails);
        OrderSummaryFragment orderSummaryFragment = new OrderSummaryFragment();
        orderSummaryFragment.setArguments(bundle);
        return orderSummaryFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_summary, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IOrderSummaryHost) {
            mHost = (IOrderSummaryHost) context;
        } else throw new IllegalStateException("Host must implement IOrderSummaryHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mOrderSummaryViewModel = new ViewModelProvider(this).get(OrderSummaryViewModel.class);
        mOrderSummaryViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        super.onViewCreated(view, savedInstanceState);
        observeLiveData();

        if (mOrderSummaryViewModel.getUserType() != null)
            mOrderSummaryViewModel.getPendingBalance();

    }


    private void observeLiveData() {

        mOrderSummaryViewModel.getDriverAvailabiltyLiveData().observe(getViewLifecycleOwner(), driverAvailabilityResponse -> {
            if (driverAvailabilityResponse != null) {
                mOrderSummaryViewModel.createOrder(mCreateOrderDetails);
            }
        });

        mOrderSummaryViewModel.getCreateOrderResponseLiveData().observe(getViewLifecycleOwner(), createOrderResponse -> {
            if (createOrderResponse != null) {
                if (createOrderResponse.getResult().getPaymentUrl() == null)
                    openOrderCreatedDialog(createOrderResponse.getResult());
                else {
                    btnConfirmOrder.setClickable(true);
                    mHost.navigateForOnlinePayment(getTotalDeliveryCharges(), null, createOrderResponse.getResult().getId(), createOrderResponse.getResult().getPaymentUrl());
                }
            }
        });

        mOrderSummaryViewModel.getFlexibleOrderLiveData().observe(getViewLifecycleOwner(), flexibleOrdersResponse -> {
            if (flexibleOrdersResponse != null) {
                openOrderCreatedDialog(null);
            }
        });

        mOrderSummaryViewModel.getValidateWalletLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            hideProgressDialog();
            if (commonResponse != null) {
                if (commonResponse.getSuccess()) {
                    tvErrorInsufficientBalance.setVisibility(View.GONE);
                    mCreateOrderDetails.setPaymentType(getString(R.string.wallet));
                    switch (mCreateOrderDetails.getOrderType()) {
                        case AppConstants.OrderType.SEND:
                        case AppConstants.OrderType.GET:
                            mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.not_available));
                            break;
                        case AppConstants.OrderType.SPECIAL_GET:
                        case AppConstants.OrderType.SPECIAL_SEND:
                            mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.COD));
                    }

                    btnConfirmOrder.setText(R.string.s_confirm_order);

                } else {
                    mCreateOrderDetails.setPaymentType(null);
                    tvErrorInsufficientBalance.setVisibility(View.VISIBLE);
                    shakeLayout(tvErrorInsufficientBalance);
                    mWalletAmount = commonResponse.getResult().getWalletAmount();
                    tvErrorInsufficientBalance.setText(String.format(Locale.getDefault(), "%.3f kwd (Insufficient balance for delivery charges)", mWalletAmount));
                    btnConfirmOrder.setText(getString(R.string.add_money));
                }
            }

        });

        mOrderSummaryViewModel.getPromoCodeLiveData().observe(getViewLifecycleOwner(), promoCodeResponse -> {
            if (promoCodeResponse != null) {
                if (!tvLabelPromoCode.getText().toString().isEmpty()) {
                    setViewsForPromoCode();
                }

                if (promoCodeResponse.getResult().get(0).getDiscountType().equals(AppConstants.FLAT))
                    tvPromoApplied.setText(String.format(Locale.getDefault(), "- %.3f KWD", promoCodeResponse.getResult().get(0).getDiscount()));
                else {
                    tvPromoApplied.setText(String.format(Locale.getDefault(), "- %.3f KWD", promoCodeResponse.getResult().get(0).getDiscount().doubleValue() * getTotalDeliveryCharges().doubleValue() / 100));
                }
                mDiscountedDeliveryCharges = promoCodeResponse.getResult().get(0).getDiscountedAmount();
                setTotalAmountToBePaid();
                mCreateOrderDetails.setPromoCodeId(promoCodeResponse.getResult().get(0).getId());
                mCreateOrderDetails.setPromoCode(promoCodeResponse.getResult().get(0).getPromocodeKeyword());
            }
        });

        mOrderSummaryViewModel.getPendingBalanceLiveData().observe(getViewLifecycleOwner(), pendingBalanceResponse -> {
            if (pendingBalanceResponse != null) {
                if (pendingBalanceResponse.getResult().get(0).getCancellationCharge() + pendingBalanceResponse.getResult().get(0).getReturnCharges() > 0) {
                    tvLabelPendingCharges.setVisibility(View.VISIBLE);
                    tvPendingCharges.setVisibility(View.VISIBLE);
                    tvPendingCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", pendingBalanceResponse.getResult().get(0).getCancellationCharge() +
                            pendingBalanceResponse.getResult().get(0).getReturnCharges() + pendingBalanceResponse.getResult().get(0).getItemCharges()));
                    mPreviousPendingBal = pendingBalanceResponse.getResult().get(0).getCancellationCharge() + pendingBalanceResponse.getResult().get(0).getReturnCharges()
                            + pendingBalanceResponse.getResult().get(0).getItemCharges();
                    /*mOrderSummaryViewModel.setWaitingTimeForRetry(pendingBalanceResponse.getResult().get(0).getConfigurations().getFindingDriverWaitTime());
                    mOrderSummaryViewModel.setWaitingTimeForCancellation(pendingBalanceResponse.getResult().get(0).getConfigurations().getFindingDriverWaitTime());*/
                    setTotalAmountToBePaid();
                }
            }
        });

        mOrderSummaryViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_PAYMENT_MODE:
                    case AppConstants.UI_VALIDATIONS.INVALID_PROMO_CODE:
                        showSnackBar(validationErrors.getValidationMessage());
                        break;
                }
            }
        });

    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_order_summary);

        tvLabelReturnCharges.setVisibility(View.GONE);
        tvReturnCharges.setVisibility(View.GONE);
        tvLabelPromoApplied.setVisibility(View.GONE);
        tvPromoApplied.setVisibility(View.GONE);

        switchBearDeliveryCharges.setChecked(true);
        getArgumentsData();

        switchBearDeliveryCharges.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mCreateOrderDetails.setDeliveryBySender(isChecked);
            mPaymentModeList.clear();
            setPaymentModeList();
            mCreateOrderDetails.setPaymentType(null);
            mPaymentModeAdapter.notifyDataSetChanged();
           /* if (isChecked) {
                tvLabelPromoCode.setVisibility(View.VISIBLE);
                tvApply.setVisibility(View.VISIBLE);
            }else{
                hidePromoCodeViews();
            }*/
            if (!isChecked) {
                mCreateOrderDetails.setPaymentType(AppConstants.PaymentMode.COD);
                setActivePaymentMode();
                mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(AppConstants.PaymentMode.COD);
            }
            setTotalAmountToBePaid();
        });


        tvLabelPromoCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    tvApply.setBackgroundColor(getResources().getColor(R.color.colorGreenishTeal));
                } else {
                    tvApply.setBackgroundColor(getResources().getColor(R.color.colorWhiteTwo));
                }
            }
        });


        mPaymentModeList = new ArrayList<>();
        setPaymentModeList();
        setUpPaymentModeAdapter();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void setActivePaymentMode() {
        mPaymentModeList.clear();
        setPaymentModeList();
        for (int i = 0; i < mPaymentModeList.size(); i++) {
            if (i == 0) {
                mPaymentModeList.get(i).setActive(true);
            } else {
                mPaymentModeList.get(i).setActive(false);
            }
        }

        mPaymentModeAdapter.notifyDataSetChanged();
    }

    /**
     * Set up list for payment modes
     */
    private void setPaymentModeList() {
        PaymentModeType paymentModeType1 = new PaymentModeType(AppConstants.PaymentMode.CASH, 1, false);
        PaymentModeType paymentModeType2 = new PaymentModeType(AppConstants.PaymentMode.WALLET, 2, false);
        PaymentModeType paymentModeType3 = new PaymentModeType(AppConstants.PaymentMode.KNET, 3, false);

       /* if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
            tvLabelPaymentMode.setText(R.string.s_payment_mode_for_item_cost);
            mPaymentModeList.add(paymentModeType1);
            mPaymentModeList.add(paymentModeType4);

            if (mOrderSummaryViewModel.getVendorPaymentMode().equals(AppConstants.PaymentMode.WALLET)) {
                validateWalletPayment();
                mCreateOrderDetails.setPaymentType(null);
            }
           *//* switch (mOrderSummaryViewModel.getVendorPaymentMode()){
                case AppConstants.PaymentMode.COD:
                    paymentModeType1.setActive(true);
                    mPaymentModeList.add(paymentModeType1);
                    mCreateOrderDetails.setPaymentType(getString(R.string.COD));
                    mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.not_available));
                    break;
                case AppConstants.PaymentMode.WALLET:
                    paymentModeType2.setActive(true);
                    mPaymentModeList.add(paymentModeType2);
                    mOrderSummaryViewModel.validateWalletAmount(getTotalDeliveryCharges());
                    break;
            }*//*
        } else {
            mPaymentModeList.add(paymentModeType1);
            mPaymentModeList.add(paymentModeType2);
            mPaymentModeList.add(paymentModeType3);

        }*/

        switch (mCreateOrderDetails.getOrderType()) {
            case AppConstants.OrderType.BUSINESS:
                if (mOrderSummaryViewModel.getVendorPaymentMode().equals(AppConstants.PaymentMode.WALLET) &&
                        mCreateOrderDetails.getServiceType().equals(AppConstants.serviceType.PREMIUM)) {
                    validateWalletPayment();
                    mCreateOrderDetails.setPaymentType(null);
                }
                break;


            case AppConstants.OrderType.SPECIAL_SEND:
                if (!switchBearDeliveryCharges.isChecked()) {
                    mPaymentModeList.add(paymentModeType1);
                    break;
                }
            case AppConstants.OrderType.SEND:
            case AppConstants.OrderType.GET:
                mPaymentModeList.add(paymentModeType1);
            case AppConstants.OrderType.SPECIAL_GET:
                mPaymentModeList.add(paymentModeType2);
                mPaymentModeList.add(paymentModeType3);
                break;
        }
    }


    /**
     * Set up adapter for payment modes
     */
    private void setUpPaymentModeAdapter() {
        mPaymentModeAdapter = new PaymentModeAdapter(mPaymentModeList, this);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 3);
        rvPaymentType.setLayoutManager(linearLayoutManager);
        rvPaymentType.setAdapter(mPaymentModeAdapter);

    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS) != null) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
            assert mCreateOrderDetails != null;
            setViews();
        }
    }

    private void setViews() {

        setScheduleTimings();

        if (mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemImage() != null &&
                mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemImage().size() > 0) {
            Glide.with(ivItem.getContext())
                    .load(mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemImage().get(0))
                    .apply(new RequestOptions().placeholder(R.drawable.ic_delivery_type_single_inactive))
                    .into((ivItem));
        } else Glide.with(ivItem.getContext())
                .load(R.drawable.ic_delivery_type_single_inactive)
                .apply(new RequestOptions().placeholder(R.drawable.ic_delivery_type_single_inactive))
                .into((ivItem));

        if (mCreateOrderDetails.getEta() != null)
            tvLabelEta.setText(String.format("ETA %s", mCreateOrderDetails.getEta()));

        tvItemName.setText(mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemDescription());
        tvDropLocation.setText(mCreateOrderDetails.getDropOffArr().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        tvPickLocation.setText(mCreateOrderDetails.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (mCreateOrderDetails.getDropOffArr().size() > 1) {
            tvMoreDrops.setVisibility(View.VISIBLE);
            tvMoreDrops.setText(String.format(Locale.getDefault(), "+ %d more", mCreateOrderDetails.getDropOffArr().size() - 1));
        }

        if ((mCreateOrderDetails.getOrderType().equals(getString(R.string.send)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.business)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_send))) &&
                !mCreateOrderDetails.getPickupLocation().get(0).getDriverNote().isEmpty()) {
            tvNoteForDriver.setText(mCreateOrderDetails.getPickupLocation().get(0).getDriverNote());

        } else if ((mCreateOrderDetails.getOrderType().equals(getString(R.string.get)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get))) &&
                !mCreateOrderDetails.getDropOffArr().get(0).getDriverNote().isEmpty()) {
            tvNoteForDriver.setText(mCreateOrderDetails.getDropOffArr().get(0).getDriverNote());
        } else {
            tvLabelNoteForDriver.setVisibility(View.GONE);
            ivNoteForDriver.setVisibility(View.GONE);
        }

        tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f kWD", getTotalDeliveryCharges()));

        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.send)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.get))) {

            ivItemPrice.setVisibility(View.GONE);
            tvLabelItemPrice.setVisibility(View.GONE);
            tvItemPrice.setVisibility(View.GONE);
        } else {
            ivItemPrice.setVisibility(View.VISIBLE);
            tvLabelItemPrice.setVisibility(View.VISIBLE);
            tvItemPrice.setText(String.format(Locale.getDefault(), "%.3f KWD", mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemPrice()));
        }


        switch (mCreateOrderDetails.getOrderType()) {

            case AppConstants.OrderType.BUSINESS:
                tvLabelPaymentMode.setVisibility(View.GONE);
                rvPaymentType.setVisibility(View.GONE);

                tvLabelPromoCode.setVisibility(View.GONE);
                tvApply.setVisibility(View.GONE);
                switchBearDeliveryCharges.setVisibility(View.GONE);

                break;
            case AppConstants.OrderType.SPECIAL_SEND:
                /*tvLabelPromoCode.setVisibility(View.GONE);
                tvApply.setVisibility(View.GONE);
                mCreateOrderDetails.setDeliveryBySender(false);*/
                switchBearDeliveryCharges.setVisibility(View.VISIBLE);

                if (mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemPrice().compareTo(BigDecimal.valueOf(AppConstants.MIN_ITEM_PRICE_FOR_SPECIAL_ORDERS)) <= 0) {
                    mCreateOrderDetails.getDropOffArr().get(0).setCashBackAmount(BigDecimal.valueOf(0));
                } else {
                    tvLabelReturnCharges.setVisibility(View.VISIBLE);
                    tvReturnCharges.setVisibility(View.VISIBLE);
                    tvReturnCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount()));
                }
                break;
            default:
                switchBearDeliveryCharges.setVisibility(View.GONE);

        }

        setTotalAmountToBePaid();


        if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SPECIAL_GET) || mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.GET)) {
            tvLabelRecipientDetails.setText(R.string.s_pickup_details);
            tvName.setText(mCreateOrderDetails.getPickupLocation().get(0).getRecipientName());
            tvPhoneNumber.setText(mCreateOrderDetails.getPickupLocation().get(0).getRecipientMobileNo());
            tvLabelEmail.setVisibility(View.GONE);
            tvEmail.setVisibility(View.GONE);
        } else {
            tvName.setText(mCreateOrderDetails.getDropOffArr().get(0).getRecipientName());
            tvPhoneNumber.setText(mCreateOrderDetails.getDropOffArr().get(0).getRecipientMobileNo());

            if (mCreateOrderDetails.getDropOffArr().get(0).getRecipientEmail() != null && !mCreateOrderDetails.getDropOffArr().get(0).getRecipientEmail().isEmpty()) {
                tvEmail.setText(mCreateOrderDetails.getDropOffArr().get(0).getRecipientEmail());
            } else {
                tvLabelEmail.setVisibility(View.GONE);
                tvEmail.setVisibility(View.GONE);
            }
        }

        /*if (mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_send)) ||
                (mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get)) &&
                        mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemPrice().compareTo(BigDecimal.valueOf(AppConstants.MIN_ITEM_PRICE_FOR_SPECIAL_ORDERS)) > 0)) {

            tvLabelReturnCharges.setVisibility(View.VISIBLE);
            tvReturnCharges.setVisibility(View.VISIBLE);
        } else {
            tvLabelReturnCharges.setVisibility(View.GONE);
            tvReturnCharges.setVisibility(View.GONE);
        }*/


    }

    private BigDecimal getTotalDeliveryCharges() {

        BigDecimal amount = BigDecimal.valueOf(0);
        for (int i = 0; i < mCreateOrderDetails.getDropOffArr().size(); i++) {
            amount = amount.add(mCreateOrderDetails.getDropOffArr().get(i).getDeliveryCharge());
        }
        return amount;
    }


    private void openOrderCreatedDialog(final Result result) {

        if (getActivity() != null && !getActivity().isFinishing()) {
            final Dialog successDialog = new Dialog(getActivity(), R.style.customDialogWithBottomAnim);
            successDialog.setContentView(R.layout.dialog_order_created_successfully);
            AppUtils.getInstance().dimDialogBackground(successDialog);
            successDialog.setCancelable(false);

            Button btnViewOrder = successDialog.findViewById(R.id.btn_view_order);
            ImageView ivTick = successDialog.findViewById(R.id.iv_tick);

            ivTick.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in));
            ivTick.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in));


            // Hide after some seconds
            final Handler handler = new Handler();
            final Runnable runnable = () -> {
                if (successDialog.isShowing()) {
                    successDialog.dismiss();
                }
            };

            successDialog.setOnDismissListener(dialog -> {
                handler.removeCallbacks(runnable);

                if (result != null) {

                    if (!isFromViewOrder && getActivity() != null && !getActivity().isFinishing()) {
                        if (result.getPaymentUrl() == null) {
                            if (result.getServiceType().equals(getString(R.string.s_premium)) && !result.getIsLater())
                                mHost.navigateToFindingDriverScreen(result);
                            else {
                                mHost.navigateToOrderDetails(result.getId());
                            }
                        } else {
                            mHost.navigateForOnlinePayment(getTotalDeliveryCharges(), null, result.getId(), null);
                        }
                    }
                } else mHost.openCreatedOrders();
            });


            btnViewOrder.setOnClickListener(v -> {
                isFromViewOrder = true;
                successDialog.dismiss();
                if (result != null) {
                    if (result.getPaymentUrl() == null) {
                        if (result.getServiceType().equals(getString(R.string.s_premium)) && !result.getIsLater())
                            mHost.navigateToFindingDriverScreen(result);
                        else {
                            mHost.navigateToOrderDetails(result.getId());
                        }
                    } else {
                        mHost.navigateForOnlinePayment(getTotalDeliveryCharges(), null, result.getId(), null);
                    }
                } else {
                    mHost.openCreatedOrders();
                }
            });

            successDialog.show();
            btnConfirmOrder.setClickable(true);
            handler.postDelayed(runnable, AppConstants.DIALOG_DISMISS_TIME_IN_MILLIS);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_confirm_order, R.id.tv_apply, R.id.tv_label_recipient_details, R.id.iv_cross, R.id.tv_more_drops, R.id.btn_edit_schedule})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_confirm_order:
                if (mOrderSummaryViewModel.getUserType() != null) {
                    if (mOrderSummaryViewModel.validateOrderDetails(mCreateOrderDetails.getPaymentType(), mCreateOrderDetails.getOrderType(),
                            mCreateOrderDetails.getDropOffArr().get(0).getPaymentMode())) {

                        if (!mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
                            mCreateOrderDetails.setScheduledStartTime(getCurrentTime());
                            mOrderSummaryViewModel.checkAvailabilityStatus(mCreateOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates(),
                                    mCreateOrderDetails.getVehicleType());
                        } else mOrderSummaryViewModel.createOrder(mCreateOrderDetails);

                        btnConfirmOrder.setClickable(false);

                    } else if (btnConfirmOrder.getText().equals(getString(R.string.add_money))) {
                        mHost.navigateForOnlinePayment(getTotalDeliveryCharges(), mWalletAmount, null, null);
                    }
                } else
                    AppUtils.getInstance().openConfirmationDialog(getActivity(), getString(R.string.s_please_login_to_continue), this);
                break;
            case R.id.tv_apply:
                mOrderSummaryViewModel.applyPromo(tvLabelPromoCode.getText().toString(), mCreateOrderDetails.getDropOffArr().get(0).getDeliveryCharge().floatValue());
                break;
            case R.id.btn_edit_schedule:
                mHost.openTimeSelectionFragment(mCreateOrderDetails, OrderSummaryFragment.class.getName());
                break;
            case R.id.tv_label_recipient_details:
                if (clRecipientDetails.getVisibility() == View.VISIBLE) {
                    clRecipientDetails.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(clRecipientDetails);
                    tvLabelRecipientDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_my_orders_arrow_down, 0);
                } else {
                    clRecipientDetails.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(clRecipientDetails);
                    tvLabelRecipientDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_my_oreders_arrow_up, 0);
                }
                break;
            case R.id.iv_cross:
                tvLabelPromoCode.setText("");
                hidePromoCodeViews();
                mDiscountedDeliveryCharges = null;
                setTotalAmountToBePaid();
                break;
            case R.id.tv_more_drops:
                mHost.navigateToDropLocationList(mCreateOrderDetails);
        }

    }


    private void setTotalAmountToBePaid() {
        switch (mCreateOrderDetails.getOrderType()) {
            case AppConstants.OrderType.SEND:
            case AppConstants.OrderType.GET:
            case AppConstants.OrderType.SPECIAL_GET:
                if (mDiscountedDeliveryCharges != null)
                    tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", mDiscountedDeliveryCharges.add(BigDecimal.valueOf(mPreviousPendingBal))));
                else
                    tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", getTotalDeliveryCharges().add(BigDecimal.valueOf(mPreviousPendingBal))));
                break;
           /* case AppConstants.OrderType.SPECIAL_GET:
                if (mDiscountedDeliveryCharges != null)
                    tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", mDiscountedDeliveryCharges
                            .add(mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemPrice().add(BigDecimal.valueOf(mPreviousPendingBal)))));
                else
                    tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", getTotalDeliveryCharges()
                            .add(mCreateOrderDetails.getDropOffArr().get(0).getItem().get(0).getItemPrice().add(BigDecimal.valueOf(mPreviousPendingBal)))));
                break;*/

            case AppConstants.OrderType.SPECIAL_SEND:
                if (mCreateOrderDetails.getDeliveryBySender()) {
                    if (mDiscountedDeliveryCharges != null)
                        tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", mDiscountedDeliveryCharges.add(BigDecimal.valueOf(mPreviousPendingBal))
                                .add(mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount())));
                    else
                        tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", getTotalDeliveryCharges().add(BigDecimal.valueOf(mPreviousPendingBal))
                                .add(mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount())));
                } else
                    tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD",/* mCreateOrderDetails.getDropOffArr().get(0).getCashBackAmount()
                            .add*/(BigDecimal.valueOf(mPreviousPendingBal))));
                break;
            case AppConstants.OrderType.BUSINESS:
                tvAmountToBePaid.setText(String.format(Locale.getDefault(), "%.3f kWD", getTotalDeliveryCharges().add(BigDecimal.valueOf(mPreviousPendingBal))));

        }

        if (tvAmountToBePaid.getText().toString().split(" kWD")[0].equals("0.000")) {
            mCreateOrderDetails.setPaymentType(AppConstants.PaymentMode.COD);
            setActivePaymentMode();
        } else if ((mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SPECIAL_GET) && mDiscountedDeliveryCharges != null &&
                mDiscountedDeliveryCharges.equals(BigDecimal.valueOf(0)))) {
            mCreateOrderDetails.setPaymentType(AppConstants.PaymentMode.WALLET);
            setActivePaymentMode();
        }

    }

    private String getCurrentTime() {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return spf.format(new Date());
    }

    /**
     * Sets view based on service type and schedule order start timing if order is scheduled for later
     */
    private void setScheduleTimings() {
        if (mCreateOrderDetails.getServiceType().equals(AppConstants.serviceType.PREMIUM) && mCreateOrderDetails.getIslater()) {
            clSchedule.setVisibility(View.VISIBLE);
            tvScheduleStartTime.setText(String.format("%s | %s", DateFormatter.getInstance().getFormattedDate(mCreateOrderDetails.getScheduledStartTime()), DateFormatter.getInstance().getFormattedTime(mCreateOrderDetails.getScheduledStartTime())));
        }
    }

    /**
     * hides views that relates to promo code, if no promo
     * code is applied
     */
    private void hidePromoCodeViews() {
        mDiscountedDeliveryCharges = null;
        clPromoCodeInfo.setVisibility(View.GONE);
        tvLabelPromoApplied.setVisibility(View.GONE);
        tvPromoApplied.setVisibility(View.GONE);
        mCreateOrderDetails.setPromoCode(null);
        mCreateOrderDetails.setPromoCodeId(null);
        mDiscountedDeliveryCharges = null;
    }

    /**
     * Sets views for promo code, if any promo code is applied
     */
    private void setViewsForPromoCode() {
        clPromoCodeInfo.setVisibility(View.VISIBLE);
        tvLabelPromoApplied.setVisibility(View.VISIBLE);
        tvPromoApplied.setVisibility(View.VISIBLE);
        tvPromoCode.setText(tvLabelPromoCode.getText().toString());
    }


    public void updateScheduleTiming(CreateOrderRequest createOrderDetails) {
        mCreateOrderDetails = createOrderDetails;
        setScheduleTimings();
    }

    /**
     * Callback when any of the payment mode is selected from {@link PaymentModeAdapter}
     *
     * @param paymentModeType Payment mode selected
     */
    @Override
    public void onPaymentModeSelected(PaymentModeType paymentModeType) {
        if (!mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
            mCreateOrderDetails.setPaymentType(null);
            tvErrorInsufficientBalance.setVisibility(View.GONE);
            btnConfirmOrder.setText(R.string.s_confirm_order);
        }

        switch (mCreateOrderDetails.getOrderType()) {
            case AppConstants.OrderType.SPECIAL_SEND:
                if (switchBearDeliveryCharges.isChecked()) {
                    setPaymentMode(paymentModeType);
                } else {
                    if (paymentModeType.getPaymentIcon() != 1)
                        showSnackBar(getString(R.string.s_only_cash_option_is_available_for_recipient));
                    setActivePaymentMode();
                    mCreateOrderDetails.setPaymentType(getString(R.string.COD));
                }
                mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.COD));
                break;
            case AppConstants.OrderType.BUSINESS:
                setItemPaymentMode(paymentModeType);
                break;
            case AppConstants.OrderType.SPECIAL_GET:
                setPaymentMode(paymentModeType);
                mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.COD));
                break;
            default:
                setPaymentMode(paymentModeType);
        }
    }

    private void setItemPaymentMode(PaymentModeType paymentModeType) {
        for (int i = 0; i < mCreateOrderDetails.getDropOffArr().size(); i++) {
            switch (paymentModeType.getPaymentIcon()) {
                case 1:
                    mCreateOrderDetails.getDropOffArr().get(i).setPaymentMode(AppConstants.PaymentMode.COD);
                    break;
                case 4:
                    mCreateOrderDetails.getDropOffArr().get(i).setPaymentMode(AppConstants.PaymentMode.PREPAID);
                    break;
            }
        }
    }

    private void setPaymentMode(PaymentModeType paymentMode) {
        switch (paymentMode.getPaymentIcon()) {
            case 1:
                mCreateOrderDetails.setPaymentType(getString(R.string.COD));
                mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.not_available));
                break;
            case 2:
                validateWalletPayment();
                break;
            case 3:
                /*  mHost.navigateForOnlinePayment(getTotalDeliveryCharges(), mWalletAmount);*/
                mCreateOrderDetails.setPaymentType(AppConstants.PaymentMode.ONLINE);
                mCreateOrderDetails.getDropOffArr().get(0).setPaymentMode(getString(R.string.not_available));
        }
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        btnConfirmOrder.setClickable(true);

        if (failureResponse.getErrorCode() == INSUFFICIENT_WALLET_FAILURE) {
            tvErrorInsufficientBalance.setVisibility(View.VISIBLE);
            tvErrorInsufficientBalance.setText(failureResponse.getErrorMessage());
            shakeLayout(tvErrorInsufficientBalance);
        }

    }

    @Override
    protected void onErrorOccurred(Throwable throwable) {
        super.onErrorOccurred(throwable);
        btnConfirmOrder.setClickable(true);
    }

    @Override
    public void onOkViewClicked() {
        if (getActivity() != null)
            getActivity().finishAfterTransition();
    }

    @Override
    public void onYesViewClicked() {
        mHost.moveToOnBoard(mCreateOrderDetails);
    }

    private void validateWalletPayment() {

        showProgressDialog();
        mOrderSummaryViewModel.validateWalletAmount(getTotalDeliveryCharges());

        /*if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS) && mOrderSummaryViewModel.getVendorPaymentMode().equals(AppConstants.PaymentMode.WALLET)) {
            showProgressDialog();
            mOrderSummaryViewModel.validateWalletAmount(getTotalDeliveryCharges());
        }*/
    }

    public void checkForPaymentMode() {
        if (mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.BUSINESS) &&
                mCreateOrderDetails.getServiceType().equals(AppConstants.serviceType.PREMIUM) && mCreateOrderDetails.getPaymentType() == null) {
            validateWalletPayment();
        }

        if (mOrderSummaryViewModel.getUserType() != null) {
            mOrderSummaryViewModel.getPendingBalance();
        }
    }

    public void updatePaymentStatus(boolean paymentStatus, String id) {
        if (paymentStatus) {
            Result result = new Result();
            result.setId(id);
            result.setServiceType(AppConstants.serviceType.PREMIUM);
            PickupLocation pickupLocation = new GsonBuilder().create().fromJson(new GsonBuilder().create().toJson(mCreateOrderDetails.getPickupLocation().get(0)), PickupLocation.class);
            List<PickupLocation> pickupLocations = new ArrayList<>();
            pickupLocations.add(pickupLocation);
            result.setPickupLocation(pickupLocations);
            result.setIsLater(false);
            openOrderCreatedDialog(result);
        } else {
            showErrorDialog();
        }
    }

    private void showErrorDialog() {
        if (getContext() != null) {
            final Dialog successDialog = new Dialog(getContext(), R.style.customDialog);
            successDialog.setContentView(R.layout.dialog_forgot_password);
            TextView tvMessage = successDialog.findViewById(R.id.tv_message);
            tvMessage.setText(R.string.s_payment_failed);
            ImageView ivTick = successDialog.findViewById(R.id.iv_resent_link);
            ivTick.setBackground(getResources().getDrawable(R.drawable.drawable_ci_red));
            ivTick.setPadding(IMAGE_PADDING, IMAGE_PADDING, IMAGE_PADDING, IMAGE_PADDING);
            ivTick.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_order_failed_cross));
            AppUtils.getInstance().dimDialogBackground(successDialog);
            Button btnOk = successDialog.findViewById(R.id.btn_ok);
            successDialog.show();

            btnOk.setOnClickListener(v -> successDialog.dismiss());
        }
    }


    public interface IOrderSummaryHost {

        void onBackPressed();

        void moveToOnBoard(CreateOrderRequest createOrderDetails);

        void navigateToDropLocationList(CreateOrderRequest createOrderDetails);

        void navigateToFindingDriverScreen(Result result);

        void navigateToOrderDetails(String id);

        void navigateForOnlinePayment(BigDecimal deliveryCharges, BigDecimal walletAmount, String id, String paymentUrl);

        void openTimeSelectionFragment(CreateOrderRequest createOrderDetails, String navigation);

        void openCreatedOrders();
    }

}
