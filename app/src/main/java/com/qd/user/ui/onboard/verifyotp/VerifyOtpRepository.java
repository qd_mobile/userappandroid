package com.qd.user.ui.onboard.verifyotp;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.otpverification.OtpVerificationResponse;

import java.util.HashMap;

class VerifyOtpRepository {
    void hitValidateOtpApi(String completeOtp, String phoneNumber, final RichMediatorLiveData<OtpVerificationResponse> mVerificationLiveData) {

        DataManager.getInstance().hitValidateOtpApi(createRequestPayloadForVerification(completeOtp, phoneNumber)).enqueue(new NetworkCallback<OtpVerificationResponse>() {
            @Override
            public void onSuccess(OtpVerificationResponse otpVerificationResponse) {
                DataManager.getInstance().setUserVerified(true);
                mVerificationLiveData.setValue(otpVerificationResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mVerificationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mVerificationLiveData.setError(t);
            }
        });
    }


    private HashMap<String, String> createRequestPayloadForVerification(String otp, String phoneNumber) {
        HashMap<String,String> requestMap = new HashMap<>();
        requestMap.put(NetworkConstants.KEY_TYPE, AppConstants.userType.CASH_CLIENT);
        requestMap.put(NetworkConstants.KEY_OTP, otp);
        requestMap.put(NetworkConstants.KEY_PHONE_NUMBER, phoneNumber);
        requestMap.put(NetworkConstants.KEY_COUNTRY_CODE, AppConstants.COUNTRY_CODE);
        return requestMap;
    }

    public String getPhoneNumber() {
        return DataManager.getInstance().getPhoneNumber();
    }

    void resendOtp(RichMediatorLiveData<CommonResponse> resendOtpLiveData) {
        DataManager.getInstance().resendOtp(AppConstants.CASH_CLIENT, AppConstants.COUNTRY_CODE, DataManager.getInstance().getPhoneNumber()).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                DataManager.getInstance().setUserVerified(true);
                resendOtpLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                resendOtpLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                resendOtpLiveData.setError(t);
            }
        });
    }
}
