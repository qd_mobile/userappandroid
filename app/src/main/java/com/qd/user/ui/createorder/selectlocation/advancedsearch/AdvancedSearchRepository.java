package com.qd.user.ui.createorder.selectlocation.advancedsearch;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.advancedsearch.SaveAddressResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.kuwaitfinder.blocks.BlocksResponse;
import com.qd.user.model.kuwaitfinder.geocode.GeoCodeLocationResponse;
import com.qd.user.model.kuwaitfinder.governorate.GovernorateResponse;
import com.qd.user.model.kuwaitfinder.neighbourhood.NeighbourhoodResponse;
import com.qd.user.model.kuwaitfinder.streets.StreetsResponse;
import com.qd.user.model.kuwaitfinder.token.TokenResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

class AdvancedSearchRepository {

    void getTokenForKuwaitFinder(final RichMediatorLiveData<TokenResponse> tokenLiveData) {
        DataManager.getInstance().getTokenForKuwaitFinderApi(createRequestPayload()).enqueue(new NetworkCallback<TokenResponse>() {
            @Override
            public void onSuccess(TokenResponse tokenResponse) {
                tokenLiveData.setValue(tokenResponse);
                DataManager.getInstance().setKuwaitFinderAccessToken(tokenResponse.getToken());
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                tokenLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                tokenLiveData.setError(t);
            }
        });

    }

    private HashMap<String, String> createRequestPayload() {
        HashMap<String, String> requestMap = new HashMap<>();

        requestMap.put("username", "QuickDelUser");
        requestMap.put("password", "Q!k#i2k@h*");
        requestMap.put("referer", "requestip");
        requestMap.put("expiration", "99999");
        requestMap.put("f", "pjson");

        return requestMap;
    }

    void getGovernorate(final RichMediatorLiveData<GovernorateResponse> governorateLiveData, String token) {
        DataManager.getInstance().getGovernorate(token).enqueue(new NetworkCallback<GovernorateResponse>() {
            @Override
            public void onSuccess(GovernorateResponse governorateResponse) {
                governorateLiveData.setLoadingState(false);
                governorateLiveData.setValue(governorateResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                governorateLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                governorateLiveData.setError(t);
            }
        });
    }

    void getNeighbourhood(final RichMediatorLiveData<NeighbourhoodResponse> neighbourhoodLiveData, String token, int govNo) {
        DataManager.getInstance().getNeighbourhood(getNeighbourhoodUrl(token, govNo)).enqueue(new NetworkCallback<NeighbourhoodResponse>() {
            @Override
            public void onSuccess(NeighbourhoodResponse neighbourhoodResponse) {
                neighbourhoodLiveData.setLoadingState(false);
                neighbourhoodLiveData.setValue(neighbourhoodResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                neighbourhoodLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                neighbourhoodLiveData.setError(t);
            }
        });
    }

    private String getNeighbourhoodUrl(String token, int govNo) {
        return "arcgisportal/rest/services/PACIAddressSearch/FeatureServer/2/query?where=1=1" +
                "&token=" + token + "&outFields=*&returnGeometry=false&f=pjson";
    }

    void getBlock(final RichMediatorLiveData<BlocksResponse> blocksResponseRichMediatorLiveData, String token, int nhoodNo) {
        DataManager.getInstance().getBlocks(getBlocksUrl(token, nhoodNo)).enqueue(new NetworkCallback<BlocksResponse>() {
            @Override
            public void onSuccess(BlocksResponse blocksResponse) {
                blocksResponseRichMediatorLiveData.setLoadingState(false);
                blocksResponseRichMediatorLiveData.setValue(blocksResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                blocksResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                blocksResponseRichMediatorLiveData.setError(t);
            }
        });
    }

    private String getBlocksUrl(String token, int nhoodNo) {
        return "arcgisportal/rest/services/PACIAddressSearch/FeatureServer/1/query?where=nhood_no=" + nhoodNo +
                "&token=" + token + "&outFields=*&returnGeometry=false&f=pjson";
    }

    void getStreet(final RichMediatorLiveData<StreetsResponse> streetLiveData, String token, String blockId, int nhoodNo) {
        DataManager.getInstance().getStreet(getStreetUrl(token, blockId, nhoodNo)).enqueue(new NetworkCallback<StreetsResponse>() {
            @Override
            public void onSuccess(StreetsResponse streetsResponse) {
                streetLiveData.setLoadingState(false);
                streetLiveData.setValue(streetsResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                streetLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                streetLiveData.setError(t);
            }
        });
    }

    private String getStreetUrl(String token, String blockId, int nhoodNo) {
        return "https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/PACIAddressSearch/FeatureServer/0/query?where=blockenglish='" + blockId +
                "'and nhood_no=" + nhoodNo + "&token=" + token + "&outFields=*&returnGeometry=false&f=pjson";
    }

    void getGeoCode(final RichMediatorLiveData<GeoCodeLocationResponse> geocodeLiveData, String token, String street) {
        DataManager.getInstance().getGeoCode(getGeoCodeUrl(token, street)).enqueue(new NetworkCallback<GeoCodeLocationResponse>() {
            @Override
            public void onSuccess(GeoCodeLocationResponse geoCodeLocationResponse) {
                geocodeLiveData.setLoadingState(false);
                geocodeLiveData.setValue(geoCodeLocationResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                geocodeLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                geocodeLiveData.setError(t);
            }
        });
    }

    private String getGeoCodeUrl(String token, String street) {
        return "arcgisportal/rest/services/Hosted/PACIGeocoder/FeatureServer/0/query?where=streetenglish='" + street +
                "'&returnGeometry=false&f=pjson&token=" + token + "&outFields=*";
    }

    void saveAddressInFav(final RichMediatorLiveData<SaveAddressResponse> saveAddressLiveData, SaveAddressRequest completeAddress) {
        completeAddress.setCashClientId(DataManager.getInstance().getUserId());
        completeAddress.setIsFavourite(true);
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().saveAdressInFav(new JSONObject(gson.toJson(completeAddress))).enqueue(new NetworkCallback<SaveAddressResponse>() {
                @Override
                public void onSuccess(SaveAddressResponse SaveAddressResponse) {
                    saveAddressLiveData.setLoadingState(false);
                    saveAddressLiveData.setValue(SaveAddressResponse);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    saveAddressLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    saveAddressLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    void hitEditSaveAddressApi(final RichMediatorLiveData<SaveAddressResponse> mSaveAddressLiveData, SaveAddressRequest completeAddress, String savedAddressId) {
        completeAddress.setSavedAddressId(savedAddressId);
        completeAddress.setIsFavourite(true);
        Gson gson = new GsonBuilder().create();
        try {
            DataManager.getInstance().hitEditSaveAddressApi(new JSONObject(gson.toJson(completeAddress))).enqueue(new NetworkCallback<SaveAddressResponse>() {
                @Override
                public void onSuccess(SaveAddressResponse SaveAddressResponse) {
                    mSaveAddressLiveData.setValue(SaveAddressResponse);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mSaveAddressLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mSaveAddressLiveData.setError(t);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getUserType() {
        return DataManager.getInstance().getUserType();
    }
}
