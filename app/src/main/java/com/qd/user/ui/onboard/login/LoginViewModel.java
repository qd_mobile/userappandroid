package com.qd.user.ui.onboard.login;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

public class LoginViewModel extends ViewModel {

    private RichMediatorLiveData<CreateAccountResponse> mLoginLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private LoginRepository mRepo = new LoginRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver,Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver=loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if(mLoginLiveData ==null){
            mLoginLiveData = new RichMediatorLiveData<CreateAccountResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if(mValidateLiveData == null){
                mValidateLiveData = new MutableLiveData<>();
            }
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the login live data object to {@link LoginFragment}
     *
     * @return {@link #mLoginLiveData}
     */
    RichMediatorLiveData<CreateAccountResponse> getLoginLiveData() {
        return mLoginLiveData;
    }

    void loginButtonClicked(String email, String password, String deviceId) {
        if (checkValidation(email,password)) {
            mLoginLiveData.setLoadingState(true);
            mRepo.getFireBaseToken(mLoginLiveData, email, password, deviceId);
        }
    }


    private boolean checkValidation(String email, String password) {
         if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
             mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_invalid_email));
            return false;
        }
        else if(password.length()<8 || password.length()>16){
             mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PASSWORD, R.string.s_password_should_be_between_characters));
            return false;
        }
        return true;
    }


    void setUserVerified(boolean userVerified) {
        mRepo.setUserVerified(userVerified);
    }
}
