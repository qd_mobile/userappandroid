package com.qd.user.ui.trackorder.businessorder;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.orderdetail.DropLocation;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.orderdetail.Result;
import com.qd.user.ui.trackorder.orderdetail.OrderDetailViewModel;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.DateFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BusinessOrderDetailsFragment extends BaseFragment implements DropLocationAdapter.DropLocationAdapterInterface {


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_label_order_id)
    TextView tvLabelOrderId;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.view_date_and_time)
    View viewDateAndTime;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.view_my_order)
    View viewMyOrder;
    @BindView(R.id.iv_order_type)
    ImageView ivOrderType;
    @BindView(R.id.tv_order_type)
    TextView tvOrderType;
    @BindView(R.id.tv_order_status)
    TextView tvOrderStatus;
    @BindView(R.id.iv_pickup_location)
    ImageView ivPickupLocation;
    @BindView(R.id.tv_label_pickup_location)
    TextView tvLabelPickupLocation;
    @BindView(R.id.tv_pickup_location)
    TextView tvPickupLocation;
    @BindView(R.id.view_dotted_line)
    View viewDottedLine;
    @BindView(R.id.rv_drops)
    RecyclerView rvDrops;
    /* @BindView(R.id.tv_label_product_images)
     TextView tvLabelProductImages;
     @BindView(R.id.tv_label_receipt)
     TextView tvLabelReceipt;
     @BindView(R.id.rl_product_images)
     RecyclerView rlProductImages;
     @BindView(R.id.iv_receipt)
     ImageView ivReceipt;
     @BindView(R.id.cv_receipt)
     CardView cvReceipt;*/
    @BindView(R.id.tv_label_item_price)
    TextView tvLabelItemPrice;
    @BindView(R.id.tv_item_price)
    TextView tvItemPrice;
    @BindView(R.id.tv_label_delivery_charges)
    TextView tvLabelDeliveryCharges;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.tv_label_return_charges)
    TextView tvLabelReturnCharges;
    @BindView(R.id.tv_return_charges)
    TextView tvReturnCharges;
    @BindView(R.id.tv_label_promo_code_applied)
    TextView tvLabelPromoCodeApplied;
    @BindView(R.id.tv_label_qd_promo)
    TextView tvLabelQdPromo;
    @BindView(R.id.tv_qd_promo)
    TextView tvQdPromo;
    @BindView(R.id.view_dotted_line_end)
    View viewDottedLineEnd;
    @BindView(R.id.tv_label_amount_paid)
    TextView tvLabelAmountPaid;
    @BindView(R.id.tv_amount_paid)
    TextView tvAmountPaid;
    @BindView(R.id.btn_cancel_order)
    Button btnCancelOrder;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.nsv_order_details)
    NestedScrollView nsvOrderDetails;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.cl_root)
    ConstraintLayout clRoot;
    @BindView(R.id.tv_creator_name)
    TextView tvCreatorName;
    private OrderDetailViewModel mOrderDetailViewModel;
    private Unbinder unbinder;
    private Result mResult;
    private IBusinessOrderDetailHost mBusinessOrderDetailHost;
    private ArrayList<DropLocation> mDropLocationList;
    private DropLocationAdapter mDropLocationAdapter;

    public static BusinessOrderDetailsFragment getInstance(String orderId) {
        Bundle bundle = new Bundle();
        bundle.putString("orderId", orderId);
        BusinessOrderDetailsFragment businessOrderDetailsFragment = new BusinessOrderDetailsFragment();
        businessOrderDetailsFragment.setArguments(bundle);
        return businessOrderDetailsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_details_for_vendor, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mOrderDetailViewModel = new ViewModelProvider(this).get(OrderDetailViewModel.class);
        mOrderDetailViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        getArgumentsData();

    }

    private void observeLiveData() {
        mOrderDetailViewModel.getOrderDetailsLiveData().observe(getViewLifecycleOwner(), orderDetailResponse -> {
            if (orderDetailResponse != null) {
                nsvOrderDetails.setVisibility(View.VISIBLE);
                tvNoList.setVisibility(View.GONE);

                setOrderDetails(orderDetailResponse);
            }
            hideProgressDialog();
        });

        mOrderDetailViewModel.getOrderCancellationLiveData().observe(getViewLifecycleOwner(), orderCancellationResponse -> {
            hideProgressDialog();
            if (orderCancellationResponse != null) {
                showSnackBar(getString(R.string.s_order_cancelled));
                mBusinessOrderDetailHost.refreshOrders(orderCancellationResponse.getResult().getId());
            }
        });
    }

    @Override
    protected void initViewsAndVariables() {
        mDropLocationList = new ArrayList<>();
        tvTitle.setText(getString(R.string.s_my_orders));
        /*tvLabelReceipt.setVisibility(View.GONE);
        cvReceipt.setVisibility(View.GONE);*/
        nsvOrderDetails.setVisibility(View.GONE);
    }

    private void setOrderDetails(OrderDetailResponse orderDetailResponse) {

        mResult = orderDetailResponse.getResult().get(0);

        if (isOrderPickedUp() || anyDropCancelled()) {
            btnCancelOrder.setVisibility(View.GONE);
        } else btnCancelOrder.setVisibility(View.VISIBLE);

        //Setting orderId
        tvOrderId.setText(String.format("ID - #%s", orderDetailResponse.getResult().get(0).getOrderUniqueId()));

        //Setting date and time
        if (orderDetailResponse.getResult().get(0).getScheduledStartTime() != null) {
            tvDay.setText(DateFormatter.getInstance().getFormattedDay(orderDetailResponse.getResult().get(0).getScheduledStartTime()));
            tvTime.setText(DateFormatter.getInstance().getFormattedTime(orderDetailResponse.getResult().get(0).getScheduledStartTime()));
            tvDate.setText(DateFormatter.getInstance().getFormattedDate(orderDetailResponse.getResult().get(0).getScheduledStartTime()));
        }

        //Setting order type
        tvOrderType.setText(orderDetailResponse.getResult().get(0).getOrderType());

        setCurrentCharges();

        //Setting order type image
        if (orderDetailResponse.getResult().get(0).getOrderType().equals(getString(R.string.send)) || orderDetailResponse.getResult().get(0).getOrderType().equals(getString(R.string.s_special_send)))
            ivOrderType.setImageDrawable((getResources().getDrawable(R.drawable.ic_dashboard_send)));
        else if (orderDetailResponse.getResult().get(0).getOrderType().equals(getString(R.string.get)) || orderDetailResponse.getResult().get(0).getOrderType().equals(getString(R.string.s_special_get)))
            ivOrderType.setImageDrawable(getResources().getDrawable(R.drawable.ic_dashboard_get));
        else if (orderDetailResponse.getResult().get(0).getOrderType().equals(getString(R.string.business)))
            ivOrderType.setImageDrawable((getResources().getDrawable(R.drawable.ic_dashboard_business)));

        //Setting order status
        setOrderStatus(orderDetailResponse.getResult().get(0));

        tvPickupLocation.setText(orderDetailResponse.getResult().get(0).getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (orderDetailResponse.getResult().get(0).getVendorId().get(0).getCompany() != null)
            tvCreatorName.setText(String.format("(%s)", orderDetailResponse.getResult().get(0).getVendorId().get(0).getCompany().getNameEnglish()));
        else
            tvCreatorName.setText(String.format("(%s)", orderDetailResponse.getResult().get(0).getPickupLocation().get(0).getRecipientName()));

        if (mResult.getDropLocation().size() > 0) {
            mDropLocationList.addAll(mResult.getDropLocation());
            setUpDropLocationAdapter();
        }

        //set charges
        setTotalCharges();


    }

    private boolean anyDropCancelled() {
        for (int i = 0; i < mResult.getDropLocation().size(); i++) {
            if (mResult.getDropLocation().get(i).getCancelled()) {
                return !isOrderPickedUp();
            }
        }

        return false;
    }


    private boolean isOrderPickedUp() {

        for (int i = 0; i < mResult.getOrderStatus().size(); i++) {
            if (mResult.getOrderStatus().get(i).getStatus().equals(AppConstants.OrderStatus.PICKEDUP)) {
                return true;
            }
        }

        return false;
    }


    private void setCurrentCharges() {
        BigDecimal previousBal = mResult.getPaymentDetails().getPreviousCancellationCharges().add(mResult.getPaymentDetails().getPreviousReturnCharges()
                .add(mResult.getPaymentDetails().getPreviousCancellationCharges()));

        if (mResult.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
            tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getTotalItemCostCod()));
        } else {

            if (mResult.getPaymentDetails().getTotalItemCost().compareTo(mResult.getPaymentDetails().getDeliveryCharge().add(previousBal)) > 0) {
                tvLabelAmountPaid.setText(getString(R.string.s_amount_collected));
                tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getTotalItemCost()
                        .subtract(mResult.getPaymentDetails().getDeliveryChargeAfterDiscount().add(previousBal))));
            } else {
                tvLabelAmountPaid.setText(R.string.s_amount_paid);
                tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getDeliveryCharge().add(previousBal)
                        .subtract(mResult.getPaymentDetails().getTotalItemCost())));
            }
        }
    }

    private void setTotalCharges() {
        tvItemPrice.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getTotalItemCostCod()));
        tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getDeliveryCharge()));

        if (mResult.getPaymentDetails().getReturnCharges() != null && mResult.getPaymentDetails().getReturnCharges().compareTo(BigDecimal.valueOf(0)) > 0) {
            tvReturnCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getReturnCharges()));
        } else {
            tvLabelReturnCharges.setVisibility(View.GONE);
            tvReturnCharges.setVisibility(View.GONE);
        }
    }

    private void setUpDropLocationAdapter() {
        mDropLocationAdapter = new DropLocationAdapter(mDropLocationList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvDrops.setLayoutManager(linearLayoutManager);
        rvDrops.setAdapter(mDropLocationAdapter);
        rvDrops.scheduleLayoutAnimation();
    }



    /**
     * Sets order status
     *
     * @param datum Order data
     */
    private void setOrderStatus(Result datum) {
        switch (datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus()) {
            case AppConstants.OrderStatus.ORDER_CREATED:
            case AppConstants.OrderStatus.DRIVER_ALIGNED:
            case AppConstants.OrderStatus.DELIVERED:
                if (datum.getDropLocation().size() == 1 &&
                        (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                    tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).
                            getDriverTrackingStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                } else {
                    tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorGreenishTeal));
                }
                break;
            case AppConstants.OrderStatus.RETURNED:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorBlue));
                break;
            case AppConstants.OrderStatus.CANCELLED:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_reddish));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorDarkRed));
                break;
            default:
                if (datum.getDropLocation().size() == 1 &&
                        (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                    tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().
                            get(0).getDriverTrackingStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                }else if(mResult.getOrderStatus().get(mResult.getOrderStatus().size()-1).getStatus().equals(AppConstants.OrderStatus.PICKEDUP) &&
                mResult.getServiceType().equals(AppConstants.serviceType.FLEXIBLE)){
                    tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorGreenishTeal));
                }else {
                    tvOrderStatus.setText(R.string.s_track);
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_rectangle_stroke_gun_metal));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorTextBlack));
                }
        }
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey("orderId") && getArguments().getString("orderId") != null) {
            showProgressDialog();
            mOrderDetailViewModel.getOrderDetail(getArguments().getString("orderId"));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.tv_order_status, R.id.btn_cancel_order})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mBusinessOrderDetailHost.onBackPressed();
                break;
            case R.id.tv_order_status:
                if (isToTrack())
                    mBusinessOrderDetailHost.openTrackOrderFragment(mResult.getId(), false);
                break;
            case R.id.btn_cancel_order:
                openOrderCancellationDialog(null, null);
                break;
        }
    }

    private boolean isToTrack() {
        switch (mResult.getOrderStatus().get(mResult.getOrderStatus().size() - 1).getStatus()) {
            case AppConstants.OrderStatus.ORDER_CREATED:
            case AppConstants.OrderStatus.DRIVER_ALIGNED:
            case AppConstants.OrderStatus.DELIVERED:
            case AppConstants.OrderStatus.RETURNED:
            case AppConstants.OrderStatus.CANCELLED:
            case AppConstants.OrderStatus.CASHBACK_RETURNED:
                return false;
            case AppConstants.OrderStatus.PICKEDUP:
                return !mResult.getServiceType().equals(AppConstants.serviceType.FLEXIBLE);
            default:
                return true;
        }
    }

    /**
     * Opens dialog for listing the reason to cancel the particular order.
     * Only one of the given reasons can be selected at a time and an additional edit text is provided for description.
     */
    private void openOrderCancellationDialog(String id, String driverId) {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.layout_cancel_order);
            AppUtils.getInstance().dimDialogBackground(dialog);

            final RadioGroup rgReasons = dialog.findViewById(R.id.rg_reasons);
            final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
            final Button btnSubmit = dialog.findViewById(R.id.btn_submit);
            final EditText etDescription = dialog.findViewById(R.id.et_message);
            final TextView tvError = dialog.findViewById(R.id.tv_error);

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            List<com.qd.user.model.commomresponse.Result> reasons = DataManager.getInstance().getCancelReasons();

            for (int i = 0; i < reasons.size(); i++) {
                RadioButton button = new RadioButton(getContext());
                button.setId(i);
                button.setText(reasons.get(i).getReason());
                rgReasons.addView(button);
            }

            btnSubmit.setOnClickListener(v -> {
                if (rgReasons.getCheckedRadioButtonId() != -1) {
                    if (reasons.get(rgReasons.getCheckedRadioButtonId()).getDescriptionMandatory() && etDescription.getText().toString().isEmpty()) {
                        tvError.setText(R.string.s_please_describe_reason_for_cancellation);
                        tvError.setVisibility(View.VISIBLE);
                        shakeLayout(tvError);
                    } else {
                        String reportIssue = null;
                        tvError.setVisibility(View.GONE);
                        reportIssue = reasons.get(rgReasons.getCheckedRadioButtonId()).getReason();

                        mOrderDetailViewModel.cancelOrder(requestPayloadForOrderCancellation(id, driverId, reportIssue, etDescription.getText().toString()));
                        dialog.dismiss();
                    }
                } else {
                    tvError.setText(R.string.s_please_select_a_reason_for_cancellation);
                    tvError.setVisibility(View.VISIBLE);
                    shakeLayout(tvError);
                }
            });

            dialog.show();
        }
    }


    private JSONObject requestPayloadForOrderCancellation(String id, String driverId, String reportIssue, String description) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("requestFrom", mResult.getOrderType().equals(getString(R.string.business)) ? getString(R.string.business) : getString(R.string.cash_client));
            jsonObject.put("orderId", mResult.getId());
            if (id != null) {
                jsonObject.put("elementId", id);
                jsonObject.put("isElement", true);
            } else {
                jsonObject.put("elementId", mResult.getPickupLocation().get(0).getId());
                jsonObject.put("isElement", false);
            }

            if (driverId != null) {
                jsonObject.put("driverId", driverId);
            } else if (mResult.getPickupLocation().get(0).getPickupDriverId() != null) {
                jsonObject.put("driverId", mResult.getPickupLocation().get(0).getPickupDriverId());
            }

            jsonObject.put("reason", reportIssue);
            if (description != null)
                jsonObject.put("description", description);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    /**
     * Determines the current element in progress, returns -1 if pickup is pending,
     * otherwise returns the position of current drop in progress.
     *
     * @return position of current drop or -1, if pickup is pending
     */
    private int currentElementInProgress() {
        int currentElement = -1;
        for (int i = 0; i < mResult.getOrderStatus().size(); i++) {
            if (mResult.getOrderStatus().get(i).getStatus().equals(AppConstants.OrderStatus.PICKEDUP)) {
                for (int j = 0; j < mResult.getDropLocation().size(); j++) {
                    if (mResult.getDropLocation().get(j).getIsElementInprogress()) {
                        currentElement = j;
                    }
                }
                break;
            }
        }

        return currentElement;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IBusinessOrderDetailHost) {
            mBusinessOrderDetailHost = (IBusinessOrderDetailHost) context;
        } else throw new IllegalStateException("Host must implement IOrderDetailHost");
    }

    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();

        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        nsvOrderDetails.setVisibility(View.GONE);
    }

    @Override
    public void onDropOffCancelled(String id, String driverId) {
        openOrderCancellationDialog(id, driverId);
    }

    @Override
    public String getServiceType() {
      return  mResult.getServiceType();
    }

    public interface IBusinessOrderDetailHost {
        void onBackPressed();

        void openTrackOrderFragment(String id, boolean isFromReceived);

        void refreshOrders(String id);
    }
}
