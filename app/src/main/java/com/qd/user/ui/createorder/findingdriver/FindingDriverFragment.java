package com.qd.user.ui.createorder.findingdriver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.response.PickupLocation;
import com.qd.user.model.createorder.response.Result;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.PermissionHelper;
import com.qd.user.utils.RippleAnimation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FindingDriverFragment extends BaseFragment implements OnMapReadyCallback, PermissionHelper.IGetPermissionListener {

    private static final int ANIMATE_INTERVAL = 2000;
    private static final int MINUTES = 60;
    public static final int CANCELLATION_FAILURE = 42291;
    private static final int AUTO_CANCELLED_ORDER = 42216;
    @BindView(R.id.tv_finding_driver)
    TextView tvFindingDriver;

    @BindView(R.id.ripple)
    RippleAnimation ripple;
    private LocationManager mLocationManager;
    @BindView(R.id.tv_retry)
    TextView tvRetry;
    private Unbinder unbinder;
    private IFindingDriverHost mHost;
    private FindingDriverViewModel mFindingDriverViewModel;
    private GoogleMap mGoogleMap;
    private PermissionHelper mPermissionHelper;
    private PickupLocation mSelectedLocation;
    private Result mResult;
    private CountDownTimer cTimer;

    public FindingDriverFragment() {

    }

    public static FindingDriverFragment getInstance(Parcelable result) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.ORDER_DATA, result);
        FindingDriverFragment findingDriverFragment = new FindingDriverFragment();
        findingDriverFragment.setArguments(bundle);
        return findingDriverFragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_finding_driver, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IFindingDriverHost) {
            mHost = (IFindingDriverHost) context;
        } else throw new IllegalStateException("Host must implement IFindingDriverHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFindingDriverViewModel = new ViewModelProvider(this).get(FindingDriverViewModel.class);
        mFindingDriverViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        getArgumentsData();
        setMap();

        mFindingDriverViewModel.getCancellationReasons();

    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            if (getArguments().getParcelable(AppConstants.ORDER_DATA) != null) {
                mResult = getArguments().getParcelable(AppConstants.ORDER_DATA);
                if ((mResult != null ? mResult.getPickupLocation() : null) != null)
                    mSelectedLocation = mResult != null ? mResult.getPickupLocation().get(0) : null;
                mFindingDriverViewModel.emitForDriverAvailability(mResult != null ? mResult.getId() : null);
                startTimer();
            }
        }
    }


    //start timer function
    private void startTimer() {
        cTimer = new CountDownTimer(mFindingDriverViewModel.getWaitingTimeForRetry() * MINUTES * 1000, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (getContext() != null) {
                    tvFindingDriver.setText(getString(R.string.driver_not_available));
                    ripple.stopRippleAnimation();
                    tvRetry.setVisibility(View.VISIBLE);
                }
            }
        };
        cTimer.start();
    }

    //cancel timer
    private void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tvRetry.getVisibility() == View.GONE)
            ripple.startRippleAnimation();
    }



    /**
     * method to setup Map initially
     */
    private void setMap() {
        final SupportMapFragment FindingDriverFragment = SupportMapFragment.newInstance();
        addFragmentWithoutAnimation(R.id.fl_map_container, FindingDriverFragment, null);
        FindingDriverFragment.getMapAsync(this);
    }

    private void observeLiveData() {

        mFindingDriverViewModel.getDriverAvailabilityLiveData().observe(getViewLifecycleOwner(), result -> {
            if (result != null) {
                navigateToOrderDetails();
            }
        });

        mFindingDriverViewModel.getRetryLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            if (commonResponse != null) {

                if (commonResponse.getResult().getOrderStatus() != null) {
                    if (!commonResponse.getResult().getOrderStatus().equals(AppConstants.OrderStatus.ORDER_CREATED)) {
                        navigateToOrderDetails();
                    }
                } else {
                    startTimer();
                    tvRetry.setVisibility(View.GONE);
                    tvFindingDriver.setText(R.string.s_finding_your_driver);
                    ripple.startRippleAnimation();
                }
            }
        });

        mFindingDriverViewModel.getOrderCancellationLiveData().observe(getViewLifecycleOwner(), orderCancellationResponse -> {
            if (orderCancellationResponse != null) {
                mHost.onBackPressed();
            }
        });

        mFindingDriverViewModel.getAutoCancellationLiveData().observe(getViewLifecycleOwner(), orderCancellationResponse -> {
            hideProgressDialog();
            if (orderCancellationResponse != null) {
                openCancellationDialog(getString(R.string.s_seems_our_drivers_are_busy_try_again_later));
            }
        });
    }


    @Override
    protected void initViewsAndVariables() {
        mHost.enterFullScreenMode();
        mPermissionHelper = new PermissionHelper(this);
        tvRetry.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();
        ripple.stopRippleAnimation();
        cancelTimer();
        mFindingDriverViewModel.disconnectSocketListeners();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        moveToCurrentLocation(new LatLng(AppConstants.KUWAIT_CENTER_LAT, AppConstants.KUWAIT_CENTER_LONG));

        if (getActivity() != null) {
            mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (mPermissionHelper.hasPermission(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.PermissionConstants.REQUEST_LOCATION)) {
                permissionsGiven(AppConstants.PermissionConstants.REQUEST_LOCATION);
            }
        }

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);

    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 10));
        // Zoom in, animating the camera.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 12, animating with a duration of 2 seconds.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(10), ANIMATE_INTERVAL, null);

    }

    private void openCancellationDialog(CharSequence message) {
        if (getContext() != null) {
            final Dialog successDialog = new Dialog(getContext(), R.style.customDialog);
            successDialog.setContentView(R.layout.dialog_forgot_password);
            TextView tvMessage = successDialog.findViewById(R.id.tv_message);
            tvMessage.setText(message);
            ImageView ivTick = successDialog.findViewById(R.id.iv_resent_link);
            ivTick.setImageDrawable(getContext().getResources().getDrawable(R.drawable.ic_password_changed));
            AppUtils.getInstance().dimDialogBackground(successDialog);
            Button btnOk = successDialog.findViewById(R.id.btn_ok);
            successDialog.show();

            btnOk.setOnClickListener(v -> successDialog.dismiss());

            successDialog.setOnDismissListener(dialog -> mHost.onBackPressed());
        }

    }

    @OnClick({R.id.tv_in_progress, R.id.tv_retry})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_in_progress:
                autoCancelOrder();
                break;
            case R.id.tv_retry:
                mFindingDriverViewModel.retryForFindingDriver(mResult.getId());
                break;

        }
    }

    private void autoCancelOrder() {
        showProgressDialog();
        mFindingDriverViewModel.autoCancelOrder(mResult.getId());
    }

    private void navigateToOrderDetails() {
        if (mResult != null && mResult.getIsLater()) {
            mHost.navigateToOrderDetails(mResult.getId());
        } else
            mHost.navigateToTrackOrderFragment(mResult != null ? mResult.getId() : null);

    }

    /**
     * Opens dialog for listing the reason to cancel the particular order.
     * Only one of the given reasons can be selected at a time and an additional edit text is provided for description.
     */
    private void openOrderCancellationDialog() {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.layout_cancel_order);
            AppUtils.getInstance().dimDialogBackground(dialog);

            final RadioGroup rgReasons = dialog.findViewById(R.id.rg_reasons);
            final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
            final Button btnSubmit = dialog.findViewById(R.id.btn_submit);
            final EditText etDescription = dialog.findViewById(R.id.et_message);
            final TextView tvError = dialog.findViewById(R.id.tv_error);

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            List<com.qd.user.model.commomresponse.Result> reasons = DataManager.getInstance().getCancelReasons();

            for (int i = 0; i < reasons.size(); i++) {
                RadioButton button = new RadioButton(getContext());
                button.setId(i);
                button.setText(reasons.get(i).getReason());
                rgReasons.addView(button);
            }

            btnSubmit.setOnClickListener(v -> {
                if (rgReasons.getCheckedRadioButtonId() != -1) {
                    if (reasons.get(rgReasons.getCheckedRadioButtonId()).getDescriptionMandatory() && etDescription.getText().toString().isEmpty()) {
                        tvError.setText(R.string.s_please_describe_reason_for_cancellation);
                        tvError.setVisibility(View.VISIBLE);
                        shakeLayout(tvError);
                    } else {
                        String reportIssue = null;
                        tvError.setVisibility(View.GONE);
                        reportIssue = reasons.get(rgReasons.getCheckedRadioButtonId()).getReason();

                        mFindingDriverViewModel.cancelOrder(requestPayloadForOrderCancellation(reportIssue, etDescription.getText().toString()));
                        dialog.dismiss();
                    }
                } else {
                    tvError.setText(R.string.s_please_select_a_reason_for_cancellation);
                    tvError.setVisibility(View.VISIBLE);
                    shakeLayout(tvError);
                }
            });

            dialog.show();
        }
    }

    /**
     * Create request payload for order cancellation
     *
     * @param reportIssue Reason selected for cancellation
     * @param description Additional message, if the user has entered
     * @return JSON Object for order cancellation request
     */
    private JSONObject requestPayloadForOrderCancellation(String reportIssue, String description) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("requestFrom", mResult.getOrderType().equals(getString(R.string.business)) ? getString(R.string.business) : getString(R.string.cash_client));
            jsonObject.put("orderId", mResult.getId());
            jsonObject.put("elementId", "");
            jsonObject.put("reason", reportIssue);
            if (mResult.getPickupLocation().get(0).getPickupDriverId() != null) {
                jsonObject.put("driverId", mResult.getPickupLocation().get(0).getPickupDriverId());
            }
            if (description != null)
                jsonObject.put("description", description);
            jsonObject.put("isElement", false);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionHelper.setPermissionResult(requestCode, permissions, grantResults);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void permissionsGiven(int requestCode) {

        if (mSelectedLocation != null) {
            Location location = new Location("");
            location.setLatitude(mSelectedLocation.getGeometry().getCoordinates().get(1));
            location.setLongitude(mSelectedLocation.getGeometry().getCoordinates().get(0));
            setMarkerAtGivenLocation(location);
        }
    }


    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        if (failureResponse.getErrorCode() == CANCELLATION_FAILURE) {
            AppUtils.getInstance().showAlertDialog(getContext(), getString(R.string.s_driver_already_aligned_do_you_still_want_to_cancel),
                    (dialog, which) -> openOrderCancellationDialog(), (dialog, which) -> mHost.navigateToTrackOrderFragment(mResult.getId()));
        } else if (failureResponse.getErrorCode() == AUTO_CANCELLED_ORDER) {
            AppUtils.getInstance().showAlertDialog(getContext(), getString(R.string.s_seems_our_drivers_are_busy_try_again_later), null, null);
        }
    }

    /**
     * Sets the marker at particular given location and zooms the map at the same too
     *
     * @param location Location where
     */
    private void setMarkerAtGivenLocation(Location location) {
        moveToCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()));

    }

    public void checkForOrderStatus() {
        if (mResult != null) {
            mFindingDriverViewModel.checkForOrderStatus(mResult.getId());
        }
    }



    public interface IFindingDriverHost {
        void onBackPressed();

        void enterFullScreenMode();

        void navigateToTrackOrderFragment(String id);

        void navigateToOrderDetails(String id);
    }
}
