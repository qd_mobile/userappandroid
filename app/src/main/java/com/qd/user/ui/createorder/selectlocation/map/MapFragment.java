package com.qd.user.ui.createorder.selectlocation.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.Geometry;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.model.createorder.savedaddress.SavedLocation;
import com.qd.user.model.kuwaitfinder.geocoder.Attributes;
import com.qd.user.ui.createorder.droplocation.DropLocationsListFragment;
import com.qd.user.ui.createorder.recipientdetails.RecipientDetailsFragment;
import com.qd.user.utils.PermissionHelper;
import com.qd.user.utils.maps.ReverseGeoCoderForKuwait;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MapFragment extends BaseFragment implements OnMapReadyCallback, LocationListener,
        PermissionHelper.IGetPermissionListener, PlacesAutoCompleteAdapter.PlaceAutoCompleteAdapterInterface, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {

    private static final int MY_LOCATION_BUTTON_MARGIN = 30;
    private static final String TAG = MapFragment.class.getName();

    @BindView(R.id.atv_pickup_point)
    AutoCompleteTextView etPickupPoint;
    @BindView(R.id.iv_pickup_favourite)
    ImageView ivPickupFavourite;
    @BindView(R.id.cl_container_map)
    ConstraintLayout clContainerMap;
    @BindView(R.id.iv_satellite_view)
    ImageView ivSatelliteView;
    @BindView(R.id.iv_traffic_view)
    ImageView ivTrafficView;

    private Unbinder unbinder;
    private IMapHost mHost;
    private MapViewModel mMapViewModel;
    private GoogleMap mGoogleMap;
    private PlacesAutoCompleteAdapter mAdapter;

    private ArrayList<AutocompletePrediction> mPlacesAutoCompleteList;
    private boolean mIsForPickup;
    private static final int LAT_LNG_ZOOM_LEVEL = 15;
    private static final int ANIMATE_DURATION = 2000;
    private LocationManager mLocationManager;

    private PermissionHelper mPermissionHelper;
    // private GeoDataClient mGeoDataClient;
    private DropOffArr mSelectedDropLocation;
    private ReverseGeoCoderForKuwait mReverseGeoCoderReq;
    private Attributes mAttributes;
    private LatLng midLatLng;
    private String mNavigation;
    private PlacesClient placesClient;
    private boolean isFromAutoComplete;
    private String mStreet;
    private PickupLocation mSelectedPickUpLocation;
    private String mSelectedLocation;
    private int count;

    public MapFragment() {

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static MapFragment getInstance(boolean isForPickup, Parcelable selectedPickupLocation, Parcelable selectedLocation, String navigation) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(NetworkConstants.KEY_IS_FOR_PICKUP, isForPickup);

        if (isForPickup)
            bundle.putParcelable(AppConstants.SELECTED_ADDRESS, selectedPickupLocation);
        else bundle.putParcelable(AppConstants.SELECTED_ADDRESS, selectedLocation);

        bundle.putString(AppConstants.ACTION_NAVIGATION, navigation);
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        return mapFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IMapHost) {
            mHost = (IMapHost) context;
        } else throw new IllegalStateException("Host must implement IMapHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
        mMapViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        getArgumentsData();
        setMap();

    }

    /**
     * Method to get data from arguments, checking if the arguments are null or if they contain
     * the key value needed
     */
    private void getArgumentsData() {
        if (getArguments() != null) {
            mIsForPickup = getArguments().getBoolean(NetworkConstants.KEY_IS_FOR_PICKUP);
            mNavigation = getArguments().getString(AppConstants.ACTION_NAVIGATION);

            if (getArguments().getParcelable(AppConstants.SELECTED_ADDRESS) != null) {
                isFromAutoComplete = true;
                if (mIsForPickup) {
                    mSelectedPickUpLocation = getArguments().getParcelable(AppConstants.SELECTED_ADDRESS);
                    etPickupPoint.setText(mSelectedPickUpLocation != null ? mSelectedPickUpLocation.getFullAddress() : null);
                } else {
                    mSelectedDropLocation = getArguments().getParcelable(AppConstants.SELECTED_ADDRESS);
                    etPickupPoint.setText(mSelectedDropLocation != null ? mSelectedDropLocation.getFullAddress() : null);
                }
            }

            etPickupPoint.setHint(mIsForPickup ? R.string.s_enter_pick_location : R.string.s_enter_drop_location);
        }
    }


    /**
     * method to setup map initially
     */
    private void setMap() {
        final SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        addFragmentWithoutAnimation(R.id.cl_container_map, mapFragment, null);
        mapFragment.getMapAsync(this);
    }

    private void observeLiveData() {
        mMapViewModel.getPlacesAutoCompleteLiveData().observe(getViewLifecycleOwner(), placesAutoCompleteResponse -> {
            mPlacesAutoCompleteList.clear();
            /*if (placesAutoCompleteResponse != null &&
                    placesAutoCompleteResponse.getPredictions().size() > 0 &&
                    placesAutoCompleteResponse.getPredictions().get(0).getMatchedSubstrings().get(0).getLength() == etPickupPoint.getText().length()) {
                mPlacesAutoCompleteList.addAll(placesAutoCompleteResponse.getPredictions());
                mAdapter.notifyDataSetChanged();
            }*/
        });

        mMapViewModel.getReverseGeoCodingLiveData().observe(getViewLifecycleOwner(), kuwaitReverseGeoCodingResponse -> {
            if (kuwaitReverseGeoCodingResponse != null && kuwaitReverseGeoCodingResponse.getResults() != null &&
                    kuwaitReverseGeoCodingResponse.getResults().size() > 0) {
                mAttributes = kuwaitReverseGeoCodingResponse.getResults().get(0).getAttributes();
                if (mStreet != null) {
                    mAttributes.setStreet(mStreet);
                }
                if (mSelectedLocation != null) {
                    mAttributes.setAddressName(mSelectedLocation);
                } else if (mSelectedPickUpLocation != null && count == 0) {
                    mAttributes.setAddressName(mSelectedPickUpLocation.getFullAddress());
                } else if (mSelectedDropLocation != null && count == 0) {
                    mAttributes.setAddressName(mSelectedDropLocation.getFullAddress());
                }

                hideSnackBar();
                if (!isFromAutoComplete)
                    etPickupPoint.setText(String.format("%s, %s, %s", kuwaitReverseGeoCodingResponse.getResults().get(0).getAttributes().getBlockenglish(), kuwaitReverseGeoCodingResponse.getResults().get(0).getAttributes().getNeighborhoodenglish(), kuwaitReverseGeoCodingResponse.getResults().get(0).getAttributes().getGovernorateenglish()));
                etPickupPoint.setSelection(etPickupPoint.getText().toString().length());
                isFromAutoComplete = false;
                count++;
                mSelectedLocation = null;
            }
        });

        mMapViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                showSnackBar(getString(validationErrors.getValidationMessage()));
            }
        });

    }

    /**
     * Initializes views and variables
     */
    @Override
    protected void initViewsAndVariables() {
        mPlacesAutoCompleteList = new ArrayList<>();
        mReverseGeoCoderReq = new ReverseGeoCoderForKuwait();

        ivSatelliteView.setVisibility(View.GONE);
        ivTrafficView.setVisibility(View.GONE);

        if (getActivity() != null) {
           /* // Construct a GeoDataClient.
            mGeoDataClient = Places.getGeoDataClient(getActivity());*/
            // Initialize Places.
            Places.initialize(getApplicationContext(), getString(R.string.google_key_api));

            // Create a new Places client instance.
            placesClient = Places.createClient(getActivity());

        }

        setAdapterForAutoCompletePlaces();
        setItemClickListeners();

        mPermissionHelper = new PermissionHelper(this);

    }


    /**
     * Sets item click listeners on @{@link AutoCompleteTextView} so as to specify what to be done,
     * when a particular place is selected
     * <p>
     * {@link #getLatLngFromPlaceId(String)} is used to get LatLng from place id in order to place marker on the map
     * at the location selected
     */
    @SuppressWarnings("JavadocReference")
    private void setItemClickListeners() {

        etPickupPoint.setOnItemClickListener((parent, view, position, id) -> {
            // Get data associated with the specified position
            // in the list (AdapterView)
            AutocompletePrediction selected = null;

            for (int i = 0; i < mAdapter.getList().size(); i++) {
                if (mAdapter.getList().get(i).getFullText(null).toString().equals(parent.getAdapter().getItem(position))) {
                    selected = mAdapter.getList().get(i);
                    mSelectedLocation = String.valueOf(mAdapter.getList().get(i).getFullText(null));
                    isFromAutoComplete = true;
                }
            }

            getLatLngFromPlaceId(selected != null ? selected.getPlaceId() : null);
        });


        etPickupPoint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 1) {
                    ivPickupFavourite.setVisibility(View.VISIBLE);
                    mAdapter.getFilter().filter(s.toString());
                    getAutoCompletePredictions(s.toString());
                    //mMapViewModel.getPlacesAutoCompleteResponse(mPlaces.autocomplete(s.toString()));
                } else ivPickupFavourite.setVisibility(View.GONE);
            }


            private void getAutoCompletePredictions(String query) {
                // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
                // and once again when the user makes a selection (for example when calling fetchPlace()).
                AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

                // Use the builder to create a FindAutocompletePredictionsRequest.
                FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                        .setSessionToken(token)
                        .setCountry("kw")
                        .setQuery(query)
                        .build();

                placesClient.findAutocompletePredictions(request).addOnSuccessListener((response) -> {

                    mPlacesAutoCompleteList.clear();
                    if (response.getAutocompletePredictions().size() > 0) {
                        mPlacesAutoCompleteList.addAll(response.getAutocompletePredictions());
                        mAdapter.notifyDataSetChanged();
                    }

                }).addOnFailureListener((exception) -> {
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        Log.e(TAG, "Place not found: " + apiException.getStatusCode());
                    }
                });
            }



        });
    }


    /**
     * Gets LatLng from placeId by using {@code mGeoDataClient.getPlaceById()} and
     * adds marker on Google map at location specified
     *
     * @param placeId PlaceId whose LatLng are to be found
     */
    private void getLatLngFromPlaceId(String placeId) {

        // Specify the fields to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG);

        // Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields)
                .build();

        // Add a listener to handle the response.
        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();
            Location location = new Location("");

            if (place.getLatLng() != null) {
                location.setLatitude(place.getLatLng().latitude);
                location.setLongitude(place.getLatLng().longitude);
                setMarkerAtGivenLocation(location);
            }
            Log.i(TAG, "Place found: " + place.getName() + location);
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                showSnackBar(apiException.getMessage());
                Log.e(TAG, "Place not found: " + exception.getMessage());
            }
        });
    }

    /**
     * Sets adapter for @{@link AutoCompleteTextView}
     */
    private void setAdapterForAutoCompletePlaces() {
        mAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.item_simple_text, mPlacesAutoCompleteList);
        etPickupPoint.setThreshold(3);
        etPickupPoint.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mGoogleMap = googleMap;
        moveToCurrentLocation(new LatLng(AppConstants.KUWAIT_CENTER_LAT, AppConstants.KUWAIT_CENTER_LONG));

        if (getActivity() != null) {
            mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (mPermissionHelper.hasPermission(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.PermissionConstants.REQUEST_LOCATION)) {

                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setScrollGesturesEnabled(true);

                googleMap.setOnMyLocationButtonClickListener(this);
                googleMap.setOnMyLocationClickListener(this);


                if (clContainerMap != null &&
                        clContainerMap.findViewById(Integer.parseInt("1")) != null) {
                    View locationButton = ((View) clContainerMap.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                    RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                    // position on right bottom
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    rlp.setMargins(0, 0, MY_LOCATION_BUTTON_MARGIN, MY_LOCATION_BUTTON_MARGIN);
                }

                googleMap.setOnCameraIdleListener(() -> {
                    //get lat lng at the center by calling
                    midLatLng = googleMap.getCameraPosition().target;
                    getAddressThroughLatLng(midLatLng);
                    mMapViewModel.getReverseGeoCodingUrl(mReverseGeoCoderReq.reverseGeoCodingUrl(mMapViewModel.getKuwaitFinderToken(), midLatLng));
                });

                permissionsGiven(AppConstants.PermissionConstants.REQUEST_LOCATION);
            }
        }

    }

    /**
     * Moves camera to the position specified on Google map
     *
     * @param currentLocation Location where camera is to be zoomed
     */
    private void moveToCurrentLocation(LatLng currentLocation) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, LAT_LNG_ZOOM_LEVEL));
        // Zoom in, animating the camera.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 12, animating with a duration of 2 seconds.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(LAT_LNG_ZOOM_LEVEL), ANIMATE_DURATION, null);


    }


    /**
     * Returns address through LatLng using @{@link Geocoder}
     *
     * @param latLng LatLng of the location corresponding to whom address has to be returned
     */
    private void getAddressThroughLatLng(LatLng latLng) {
        Geocoder geocoder = new Geocoder(getContext());

        try {
            List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                Log.e(TAG, addressList.get(0).toString());

                if (addressList.get(0).getThoroughfare() != null) {
                    mStreet = addressList.get(0).getThoroughfare();
                } else mStreet = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.iv_satellite_view, R.id.btn_continue, R.id.iv_traffic_view, R.id.iv_pickup_favourite})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_satellite_view:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.btn_continue:
                if (mMapViewModel.validateLocation(etPickupPoint.getText().toString(), mAttributes)) {
                    if (mNavigation.equals(RecipientDetailsFragment.class.getName())) {
                        if (mSelectedDropLocation != null || mSelectedPickUpLocation != null)
                            mHost.openAdvancedSearchFragment(mIsForPickup, getAddressFromMap(), AppConstants.Navigation.EDIT_ADDRESS_FROM_MAP);
                        else
                            mHost.openAdvancedSearchFragment(mIsForPickup, getAddressFromMap(), DropLocationsListFragment.class.getName());

                    } else
                        mHost.openAdvancedSearchFragment(mIsForPickup, getAddressFromMap(), MapFragment.class.getName());
                }
                break;
            case R.id.iv_traffic_view:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.iv_pickup_favourite:
                etPickupPoint.setText("");
                break;
        }
    }

    private Result getAddressFromMap() {
        Result result = new Result();
        List<Double> latLng = new ArrayList<>();
        SavedLocation savedLocation = new SavedLocation();


        if (mAttributes != null) {
            savedLocation.setGovernorate(mAttributes.getGovernorateenglish());
            savedLocation.setArea(mAttributes.getNeighborhoodenglish());
            savedLocation.setBlockNumber(mAttributes.getBlockenglish());
            savedLocation.setStreet(mAttributes.getStreet());
            savedLocation.setAddressName(mAttributes.getAddressName());
        }

        latLng.add(midLatLng.longitude);
        latLng.add(midLatLng.latitude);

        Geometry geometry = new Geometry();
        geometry.setCoordinates(latLng);
        savedLocation.setGeometry(geometry);
        result.setSavedLocation(savedLocation);
        return result;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionHelper.setPermissionResult(requestCode, permissions, grantResults);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void permissionsGiven(int requestCode) {
        Location location = new Location("");
        if (mSelectedPickUpLocation != null) {
            location.setLatitude(mSelectedPickUpLocation.getGeometry().getCoordinates().get(1));
            location.setLongitude(mSelectedPickUpLocation.getGeometry().getCoordinates().get(0));
            setMarkerAtGivenLocation(location);
        } else if (mSelectedDropLocation != null) {
            location.setLatitude(mSelectedDropLocation.getGeometry().getCoordinates().get(1));
            location.setLongitude(mSelectedDropLocation.getGeometry().getCoordinates().get(0));
            setMarkerAtGivenLocation(location);
        } else {
            location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                setMarkerAtGivenLocation(location);
            } else {
                getLastLocationNewMethod();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mSelectedPickUpLocation == null && mSelectedDropLocation == null && location != null) {
            setMarkerAtGivenLocation(location);
        }
    }

    /**
     * Sets marker at given location and sets the address in {@link #etPickupPoint},
     * also moves the camera to zoom on the location specified by calling {@link #moveToCurrentLocation(LatLng)}
     *
     * @param location Location where marker is to be set
     */
    private void setMarkerAtGivenLocation(Location location) {
        moveToCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()));

        mLocationManager.removeUpdates(this);
    }

    @SuppressLint("MissingPermission")
    private void getLastLocationNewMethod() {
        if (getContext() != null) {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(location -> {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            setMarkerAtGivenLocation(location);
                        }
                    })
                    .addOnFailureListener(Throwable::printStackTrace);
        }
    }


    @Override
    public boolean onMyLocationButtonClick() {
        getLastLocationNewMethod();
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    public interface IMapHost {
        void onBackPressed();

        void openAdvancedSearchFragment(boolean isForPickup, Result addressFromMap, String navigation);
    }
}
