package com.qd.user.ui.createorder.servicetype;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

class ServiceTypeViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> mFragmentsList;


    ServiceTypeViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragmentArrayList) {
        super(fm);
        mFragmentsList = fragmentArrayList;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = mFragmentsList.get(0);
        } else if (position == 1) {
            fragment = mFragmentsList.get(1);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mFragmentsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;

        if (mFragmentsList.size() == 1) {
            title = "Premium";
        } else {
            if (position == 0) {
                title = "Flexible";
            } else if (position == 1) {
                title = "Premium";
            }
        }
        return title;
    }

}



