package com.qd.user.ui.home.paymenthistory;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

class PaymentHistoryViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> mFragmentList;

    PaymentHistoryViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragmentList) {
        super(fm);
        mFragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = mFragmentList.get(0);
        } else if (position == 1) {
            fragment = mFragmentList.get(1);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Personal";
        } else if (position == 1) {
            title = "Business";
        }
        return title;
    }

}