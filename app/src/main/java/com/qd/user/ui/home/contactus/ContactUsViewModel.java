package com.qd.user.ui.home.contactus;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.contactus.ContactUsResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

import java.util.regex.Pattern;

public class ContactUsViewModel extends ViewModel {

    private RichMediatorLiveData<ContactUsResponse> mContactUsLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private  Observer<Boolean> loadingStateObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;


    private ContactUsRepository mRepo = new ContactUsRepository();


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.loadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {

        if(mContactUsLiveData ==null){
            mContactUsLiveData = new RichMediatorLiveData<ContactUsResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return loadingStateObserver;
                }
            };

        }

        if(mValidateLiveData == null){
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }


    /**
     * This method gives the contact us live data object to {@link ContactUsFragment}
     *
     * @return {@link #mContactUsLiveData}
     */
    RichMediatorLiveData<ContactUsResponse> getContactUsLiveData() {
        return mContactUsLiveData;
    }

    void hitContactUs(String name, Object selectedItem, String message, String email, String phone) {

        if (validateData(name, selectedItem, message, email, phone)) {
            mContactUsLiveData.setLoadingState(true);
            mRepo.hitContactUsApi(mContactUsLiveData, name, selectedItem, message, email, phone);
        }
    }

    private boolean validateData(String name, Object selectedItem, String message, String email, String phone) {

        if(name.isEmpty()){
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_USERNAME, R.string.s_please_enter_name));
            return false;
        } else if (!Pattern.matches("[a-zA-Z ]+", name)) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_USERNAME, R.string.s_please_enter_valid_name));
            return false;
        } else if (DataManager.getInstance().getUserId() == null && email.isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_enter_email_address));
            return false;
        } else if (DataManager.getInstance().getUserId() == null && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_please_enter_valid_email_address));
            return false;
        } else if (DataManager.getInstance().getUserId() == null && (phone.isEmpty() || !phone.matches("[0-9]{7,9}$"))) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER, R.string.s_please_enter_valid_phone_number));
            return false;
        }else if(selectedItem.equals("Other")){
            if(message.isEmpty()){
                mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_DESCRIPTION, R.string.s_please_enter_description));
                return false;
            }
        }
        return true;
    }

    String getUsername() {
        return mRepo.getUsername();
    }
}
