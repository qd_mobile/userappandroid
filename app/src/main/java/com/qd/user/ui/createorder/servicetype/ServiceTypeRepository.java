package com.qd.user.ui.createorder.servicetype;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.servicetype.FlexibleSlotsResponse;
import com.qd.user.model.failureresponse.FailureResponse;

class ServiceTypeRepository {

    void getFlexibleTimeSlots(final RichMediatorLiveData<FlexibleSlotsResponse> flexibleSlotsLiveData) {
        DataManager.getInstance().getTimeSlots().enqueue(new NetworkCallback<FlexibleSlotsResponse>() {
            @Override
            public void onSuccess(FlexibleSlotsResponse flexibleSlotsResponse) {
                flexibleSlotsLiveData.setValue(flexibleSlotsResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                flexibleSlotsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                flexibleSlotsLiveData.setError(t);
            }
        });
    }

    public String getServiceType() {
        return DataManager.getInstance().getServiceType();
    }
}
