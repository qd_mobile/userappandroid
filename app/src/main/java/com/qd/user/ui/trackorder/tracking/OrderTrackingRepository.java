package com.qd.user.ui.trackorder.tracking;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.orderstatus.CurrentLocationResponse;
import com.qd.user.socket.SocketConstants;
import com.qd.user.socket.SocketManager;

import org.json.JSONObject;


class OrderTrackingRepository {

    void getOrderDetails(String orderId, final RichMediatorLiveData<OrderDetailResponse> orderDetailsLiveData) {
        DataManager.getInstance().hitOrderDetailApi(orderId).enqueue(new NetworkCallback<OrderDetailResponse>() {
            @Override
            public void onSuccess(OrderDetailResponse orderDetailResponse) {

                orderDetailsLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderDetailsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderDetailsLiveData.setError(t);
            }
        });
    }


    public void cancelOrder(JSONObject request, final RichMediatorLiveData<OrderCancellationResponse> orderDetailsLiveData) {
        DataManager.getInstance().cancelOrder(request).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse orderDetailResponse) {
                orderDetailsLiveData.setLoadingState(false);
                orderDetailsLiveData.setValue(orderDetailResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderDetailsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderDetailsLiveData.setError(t);
            }
        });
    }

    void getDirectionsApiResponse(final RichMediatorLiveData<DirectionsApiResponse> directionsApiLiveData, String url) {
        DataManager.getInstance().getDirectionsApiResponse(url).enqueue(new NetworkCallback<DirectionsApiResponse>() {
            @Override
            public void onSuccess(DirectionsApiResponse directionsApiResponse) {
                directionsApiLiveData.setValue(directionsApiResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                directionsApiLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                directionsApiLiveData.setError(t);
            }
        });
    }

    void listenForDriverCurrentLocation(final RichMediatorLiveData<CurrentLocationResponse> currentLocationResponseLiveData) {
        SocketManager.getInstance().listenToDriverCurrentDriverLocation(args -> {
            if (args[0] != null) {
                final String s = args[0].toString();
                Log.e("Socket", s);
                CurrentLocationResponse currentLocationResponse = new GsonBuilder().create().fromJson(s, CurrentLocationResponse.class);
                currentLocationResponseLiveData.postValue(currentLocationResponse);
            }
        });
    }

    void emitToListenDriverCurrentLocation(JSONObject jsonForCurrentLocation) {
        SocketManager.getInstance().sendDataOnSocket(SocketConstants.EVENT_ORDER_STATUS, jsonForCurrentLocation);
    }

    void disconnectSocketListenerForCurrentLocation() {
        SocketManager.getInstance().offListeningEvent(SocketConstants.EVENT_DRIVER_LOCATION);
    }

    void autoCancelOrder(String id, RichMediatorLiveData<OrderCancellationResponse> autoCancellationLiveData) {
        DataManager.getInstance().autoCancelOrder(id).enqueue(new NetworkCallback<OrderCancellationResponse>() {
            @Override
            public void onSuccess(OrderCancellationResponse commonResponse) {
                autoCancellationLiveData.setValue(commonResponse);
                autoCancellationLiveData.setLoadingState(false);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                autoCancellationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                autoCancellationLiveData.setError(t);
            }
        });
    }
}
