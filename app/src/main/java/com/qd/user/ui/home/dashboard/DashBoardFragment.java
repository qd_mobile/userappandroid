package com.qd.user.ui.home.dashboard;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.utils.AppUtils;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DashBoardFragment extends BaseFragment {

    @BindView(R.id.iv_send)
    ImageView ivSend;
    @BindView(R.id.tv_send)
    TextView tvSend;
    @BindView(R.id.tv_send_message)
    TextView tvSendMessage;
    @BindView(R.id.cv_send)
    CardView cvSend;
    @BindView(R.id.iv_get)
    ImageView ivGet;
    @BindView(R.id.tv_get)
    TextView tvGet;
    @BindView(R.id.tv_get_message)
    TextView tvGetMessage;
    @BindView(R.id.cv_get)
    CardView cvGet;
    @BindView(R.id.tv_business)
    TextView tvBusiness;
    @BindView(R.id.tv_business_message)
    TextView tvBusinessMessage;
    @BindView(R.id.cl_business)
    ConstraintLayout clBusiness;

    private IDashboardHost mHost;
    private DashboardViewModel mViewModel;
    private Unbinder unbinder;
    private String mOrderType;
    private boolean isClicked;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);
        mHost.updateStatusBarColour(R.color.colorGreenishTeal);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        mViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

        if (mViewModel.getUserType() != null)
            mViewModel.getStatusOfVendor(false, getDeviceId(), AppConstants.ANDROID);
        setViews();

    }

    private void observeLiveData() {
        mViewModel.getVendorStatusLiveData().observe(getViewLifecycleOwner(), vendorStatusResponse -> {
            if (vendorStatusResponse != null && vendorStatusResponse.getResult().get(0).getCurrentStatus() != null) {

                mHost.setCounts(vendorStatusResponse.getResult().get(0).getNotificationCount(), vendorStatusResponse.getResult().get(0).getWalletAmount());

                if (isClicked)
                    if (mOrderType != null) {
                        if (vendorStatusResponse.getResult().get(0).isPendingOrders()) {
                            if (vendorStatusResponse.getResult().get(0).getOrderCurrentStatus().equals(AppConstants.OrderStatus.ORDER_CREATED)) {
                                mHost.navigateToFindingDriverFragment(vendorStatusResponse.getResult().get(0).getOrderId());
                            } else
                                mHost.navigateToOrderTrackingFragment(vendorStatusResponse.getResult().get(0).getOrderId());
                        } else {
                            mHost.openCreateOrderFragment(mOrderType);
                        }
                    } else {
                        switch (vendorStatusResponse.getResult().get(0).getCurrentStatus()) {
                            case AppConstants.INACTIVE:
                                showSnackBar(R.string.s_your_request_is_in_progress);
                                break;
                            case AppConstants.ACTIVE:
                                tvBusiness.setText(R.string.s_business);
                                mHost.openCreateOrderFragment(AppConstants.orderType.BUSINESS);
                                break;
                            default:
                                showSnackBar(R.string.s_you_are_not_currently_authorized_for_Service);

                                break;
                        }

                    }
            }
        });

    }

    private void setViews() {
        if (mViewModel.getCurrentStatus() != null && (mViewModel.getUserType().equals(AppConstants.BOTH) ||
                mViewModel.getUserType().equals(AppConstants.ONLY_VENDOR)) &&
                mViewModel.getCurrentStatus().equals(AppConstants.ACTIVE)) {
            tvBusiness.setText(R.string.s_business);
            tvBusinessMessage.setText(R.string.s_business_description);
        } else {
            tvBusiness.setText(getString(R.string.s_become_a_vendor));
        }

    }

    public static DashBoardFragment getInstance() {
        return new DashBoardFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IDashboardHost) {
            mHost = (IDashboardHost) context;
        } else
            throw new IllegalStateException("Host must implement IDashboardHost");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected void initViewsAndVariables() {

    }

    @OnClick({R.id.cv_send, R.id.cv_get, R.id.cv_business})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_send:
                isClicked = true;
                cvSend.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.scale));
                if (mViewModel.getUserType() != null) {
                    mViewModel.getStatusOfVendor(true, getDeviceId(), AppConstants.ANDROID);
                    mOrderType = AppConstants.orderType.SEND;
                } else mHost.openCreateOrderFragment(AppConstants.orderType.SEND);
                break;
            case R.id.cv_get:
                isClicked = true;
                cvGet.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.scale));
                if (mViewModel.getUserType() != null) {
                    mViewModel.getStatusOfVendor(true, getDeviceId(), AppConstants.ANDROID);
                    mOrderType = AppConstants.orderType.GET;
                } else mHost.openCreateOrderFragment(AppConstants.orderType.GET);
                break;

            case R.id.cv_business:
                isClicked = true;
                clBusiness.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.scale));
                mOrderType = null;
                if (mViewModel.getUserType() != null) {

                    switch (mViewModel.getUserType()) {
                        case AppConstants.ONLY_CASH_CLIENT:
                            if (mViewModel.getCashClientCurrentStatus().equals(AppConstants.ACTIVE)) {
                                mHost.navigateToBecomeAVendor();
                            } else {
                                showSnackBar(getString(R.string.s_you_are_currently_unauthorized_to_take_this_service));
                            }
                            break;
                        case AppConstants.BOTH:
                        case AppConstants.ONLY_VENDOR:
                            switch (mViewModel.getCurrentStatus()) {
                                case AppConstants.ACTIVE:
                                    mHost.openCreateOrderFragment(AppConstants.orderType.BUSINESS);
                                    break;
                                case AppConstants.INACTIVE:
                                    mViewModel.getStatusOfVendor(true, getDeviceId(), AppConstants.ANDROID);
                                    break;
                                default:
                                    AppUtils.getInstance().openNoServiceDialog(getActivity(), getString(R.string.s_you_are_currently_unauthorized_to_take_this_service));
                                    break;
                            }
                            break;
                    }
                } else {
                    mHost.navigateToBecomeAVendor();
                }

                break;

        }
    }


    public interface IDashboardHost {

        void openCreateOrderFragment(String orderType);

        void navigateToBecomeAVendor();

        void updateStatusBarColour(int color);

        void navigateToOrderTrackingFragment(String orderId);

        void navigateToFindingDriverFragment(String orderId);

        void setCounts(int notificationCount, BigDecimal walletAmount);
    }
}
