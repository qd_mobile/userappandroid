package com.qd.user.ui.createorder.selectlocation.map;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.maps.autocomplete.PlacesAutoCompleteResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.kuwaitfinder.geocoder.Attributes;
import com.qd.user.model.kuwaitfinder.geocoder.KuwaitReverseGeoCodingResposne;
import com.qd.user.model.validationerror.ValidationErrors;

public class MapViewModel extends ViewModel {
    private RichMediatorLiveData<PlacesAutoCompleteResponse> mPlacesAutoCompleteLiveData;
    private RichMediatorLiveData<KuwaitReverseGeoCodingResposne> mReverseGeoCodingLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private MapRepository mRepo = new MapRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mPlacesAutoCompleteLiveData == null) {
            mPlacesAutoCompleteLiveData = new RichMediatorLiveData<PlacesAutoCompleteResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mReverseGeoCodingLiveData == null) {
            mReverseGeoCodingLiveData = new RichMediatorLiveData<KuwaitReverseGeoCodingResposne>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the Map live data object to {@link MapFragment}
     *
     * @return {@link #mPlacesAutoCompleteLiveData}
     */
    RichMediatorLiveData<PlacesAutoCompleteResponse> getPlacesAutoCompleteLiveData() {
        return mPlacesAutoCompleteLiveData;
    }

    /**
     * This method gives the reverse geo coding live data object to {@link MapFragment}
     *
     * @return {@link #mReverseGeoCodingLiveData}
     */
    RichMediatorLiveData<KuwaitReverseGeoCodingResposne> getReverseGeoCodingLiveData() {
        return mReverseGeoCodingLiveData;
    }

    void getPlacesAutoCompleteResponse(String url) {
        mRepo.getPlacesAutoCompleteResponse(mPlacesAutoCompleteLiveData, url);
    }

    boolean validateLocation(String address, Attributes mAttributes) {
        if (address.isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ADDRESS, R.string.s_please_select_address));
            return false;
        } else if (mAttributes == null) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_GEOCODE, R.string.s_getting_location_please_wait));
            return false;
        }
        return true;
    }

    void getReverseGeoCodingUrl(String url) {
        mRepo.getAddressThroughReverseGeoCoding(url, mReverseGeoCodingLiveData);
    }

    String getKuwaitFinderToken() {
        return mRepo.getKuwaitFinderToken();
    }
}