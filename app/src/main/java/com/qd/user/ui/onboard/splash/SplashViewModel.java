package com.qd.user.ui.onboard.splash;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;


public class SplashViewModel extends ViewModel {

    private RichMediatorLiveData<CommonResponse> mVersionLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private SplashRepository mRepo = new SplashRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mVersionLiveData == null) {
            mVersionLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }


    /**
     * This method gives the Version live data object to {@link SplashFragment}
     *
     * @return {@link #mVersionLiveData}
     */
    RichMediatorLiveData<CommonResponse> getVersionLiveData() {
        return mVersionLiveData;
    }


    /**
     * Method used to hit Version api after checking validations
     */
    void checkForUpdates(String versionName) {

        mRepo.hitVersionApi(mVersionLiveData, versionName);

    }

    String proceedFromSplash() {
        return mRepo.navigateFromSplash();
    }

}