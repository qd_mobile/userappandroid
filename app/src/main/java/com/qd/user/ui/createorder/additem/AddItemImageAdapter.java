package com.qd.user.ui.createorder.additem;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qd.user.R;
import com.qd.user.model.createorder.additem.AddImage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class AddItemImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_ADD_ROW = 2;

    private ArrayList<Object> mImageList;
    private IAddItemImageInterface mInterface;


    AddItemImageAdapter(ArrayList<Object> images, IAddItemImageInterface iAddItemImageInterface) {
        mImageList = images;
        mInterface = iAddItemImageInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_image, parent, false);
            return new AddItemImageAdapterViewHolder(view);
        } else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_add_image, parent, false);
        return new AddImageAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (!(mImageList.get(position) instanceof AddImage)) {
            String image = (String) mImageList.get(position);
            if (image != null) {
                Glide.with(holder.itemView.getContext())
                        .load(image)
                        .apply(new RequestOptions().placeholder(R.drawable.ic_delivery_type_single_inactive))
                        .into(((AddItemImageAdapterViewHolder) holder).ivItemImage);
            }
        }
    }


    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mImageList.get(position) instanceof AddImage)
            return VIEW_TYPE_ADD_ROW;
        else return VIEW_TYPE_ROW;
    }


    public interface IAddItemImageInterface {

        void onAddImagesClicked();

        void showSnackBar();

    }

    class AddItemImageAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_item_image)
        ImageView ivItemImage;
        @BindView(R.id.iv_cancel)
        ImageView ivCancel;

        AddItemImageAdapterViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            ivCancel.setOnClickListener(view -> {
                mImageList.remove(getAdapterPosition());
                notifyItemRemoved(getAdapterPosition());

            });

        }
    }

    class AddImageAdapterViewHolder extends RecyclerView.ViewHolder {

        AddImageAdapterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(view -> {
                if (mImageList.size() > 5) {
                    mInterface.showSnackBar();
                } else {
                    mInterface.onAddImagesClicked();

                }
            });

        }
    }
}



