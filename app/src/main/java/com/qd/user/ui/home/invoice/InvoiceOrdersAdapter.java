package com.qd.user.ui.home.invoice;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.invoicedetails.Datum;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.ProgressLoader;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

class InvoiceOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;

    private ArrayList<Object> mInvoiceOrdersList;


    InvoiceOrdersAdapter(ArrayList<Object> ordersList) {
        mInvoiceOrdersList = ordersList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_invoice, viewGroup, false);
            return new InvoiceOrdersAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mInvoiceOrdersList.get(position) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mInvoiceOrdersList.get(position) instanceof Datum) {
            ((InvoiceOrdersAdapterViewHolder) holder).tvOrderId.setText(((Datum) mInvoiceOrdersList.get(position)).getOrderUniqueId());
            ((InvoiceOrdersAdapterViewHolder) holder).tvDay.setText(DateFormatter.getInstance().getFormattedDay(((Datum) mInvoiceOrdersList.get(position)).getScheduledStartTime()));
            ((InvoiceOrdersAdapterViewHolder) holder).tvDate.setText(DateFormatter.getInstance().getFormattedDate(((Datum) mInvoiceOrdersList.get(position)).getScheduledStartTime()));
            ((InvoiceOrdersAdapterViewHolder) holder).tvTime.setText(DateFormatter.getInstance().getFormattedTime(((Datum) mInvoiceOrdersList.get(position)).getScheduledStartTime()));

            ((InvoiceOrdersAdapterViewHolder) holder).tvDriverName.setText(((Datum) mInvoiceOrdersList.get(position)).getPickupLocation().get(0).getPickupDriverName());
            ((InvoiceOrdersAdapterViewHolder) holder).tvDriverId.setText(((Datum) mInvoiceOrdersList.get(position)).getPickupLocation().get(0).getPickupDriverUniqueId());
            ((InvoiceOrdersAdapterViewHolder) holder).tvTotalNoOfDrops.setText(String.valueOf(((Datum) mInvoiceOrdersList.get(position)).getTotalDrop()));
            ((InvoiceOrdersAdapterViewHolder) holder).tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", ((Datum) mInvoiceOrdersList.get(position)).getPaymentDetails().getDeliveryCharge()));
            ((InvoiceOrdersAdapterViewHolder) holder).tvItemPrice.setText(String.format(Locale.getDefault(), "%.3f KWD", ((Datum) mInvoiceOrdersList.get(position)).getPaymentDetails().getTotalItemCost()));

            switch (((Datum) mInvoiceOrdersList.get(position)).getDriverVehicle()) {
                case AppConstants.DeliveryVehicles.BIKE:
                    Glide.with(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle.getContext())
                            .load(R.drawable.ic_choose_vehicle_bike_inactive)
                            .into(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle);
                    break;
                case AppConstants.DeliveryVehicles.CAR:
                    Glide.with(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle.getContext())
                            .load(R.drawable.ic_choose_vehicle_car_inactive)
                            .into(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle);
                    break;
                case AppConstants.DeliveryVehicles.VAN:
                    Glide.with(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle.getContext())
                            .load(R.drawable.ic_choose_vehicle_van_inactive)
                            .into(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle);
                    break;
                case AppConstants.DeliveryVehicles.COOLER:
                    Glide.with(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle.getContext())
                            .load(R.drawable.ic_choose_vehicle_cooler_inactive)
                            .into(((InvoiceOrdersAdapterViewHolder) holder).ivDriverVehicle);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mInvoiceOrdersList.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (mInvoiceOrdersList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }


    //hide progress loader after getting paginated data
    void hideProgress() {
        mInvoiceOrdersList.remove(mInvoiceOrdersList.size() - 1);
        notifyDataSetChanged();
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mInvoiceOrdersList.add(new ProgressLoader());
        notifyItemInserted(mInvoiceOrdersList.size());
    }

    class InvoiceOrdersAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_label_order_id)
        TextView tvLabelOrderId;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.view_date_and_time)
        View viewDateAndTime;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.view_order_details)
        View viewOrderDetails;
        @BindView(R.id.iv_driver_vehicle)
        ImageView ivDriverVehicle;
        @BindView(R.id.tv_label_driver_name)
        TextView tvLabelDriverName;
        @BindView(R.id.tv_driver_name)
        TextView tvDriverName;
        @BindView(R.id.tv_label_driver_id)
        TextView tvLabelDriverId;
        @BindView(R.id.tv_driver_id)
        TextView tvDriverId;
        @BindView(R.id.tv_label_total_no_of_drops)
        TextView tvLabelTotalNoOfDrops;
        @BindView(R.id.tv_total_no_of_drops)
        TextView tvTotalNoOfDrops;
        @BindView(R.id.tv_label_item_price)
        TextView tvLabelItemPrice;
        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;
        @BindView(R.id.tv_label_delivery_charges)
        TextView tvLabelDeliveryCharges;
        @BindView(R.id.tv_delivery_charges)
        TextView tvDeliveryCharges;
        @BindView(R.id.cl_order_details)
        ConstraintLayout clOrderDetails;
        @BindView(R.id.guidelineStart)
        Guideline guidelineStart;
        @BindView(R.id.guidelineEnd)
        Guideline guidelineEnd;

        InvoiceOrdersAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                if (clOrderDetails.getVisibility() == View.VISIBLE) {
                    clOrderDetails.setVisibility(View.GONE);
                } else {
                    clOrderDetails.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;


        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
