package com.qd.user.ui.createorder.selectvehicle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qd.user.R;
import com.qd.user.model.createorder.distancetime.eta.VehicleCategory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectVehicleAdapter extends RecyclerView.Adapter<SelectVehicleAdapter.VehiclesListAdapterViewHolder> {

    private final ArrayList<VehicleCategory> mVehiclesList;
    private VehiclesListAdapterInterface mInterface;


    SelectVehicleAdapter(ArrayList<VehicleCategory> itemsList, VehiclesListAdapterInterface itemsListAdapterInterface) {
        mVehiclesList = itemsList;
        mInterface = itemsListAdapterInterface;
    }


    @NonNull
    @Override
    public VehiclesListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_vehicle_type, viewGroup, false);
        return new VehiclesListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehiclesListAdapterViewHolder itemsListAdapterViewHolder, int i) {
        itemsListAdapterViewHolder.tvVehicleType.setText(mVehiclesList.get(i).getKeyword());
        itemsListAdapterViewHolder.tvEta.setText(mVehiclesList.get(i).getEta());

        if (mVehiclesList.get(i).isSelected()) {
            Glide.with(itemsListAdapterViewHolder.itemView.getContext())
                    .load(mVehiclesList.get(i).getIconLink().getGreen())
                    .apply(new RequestOptions().circleCrop().placeholder(R.drawable.drawable_ci_stroke_silver))
                    .into((itemsListAdapterViewHolder).ivVehicleType);
            itemsListAdapterViewHolder.itemView.setBackgroundColor(itemsListAdapterViewHolder.itemView.getResources().getColor(R.color.colorTransparentGreenishTeal));
        } else {
            Glide.with(itemsListAdapterViewHolder.itemView.getContext())
                    .load(mVehiclesList.get(i).getIconLink().getAddItem())
                    .into((itemsListAdapterViewHolder).ivVehicleType);
            itemsListAdapterViewHolder.itemView.setBackgroundColor(itemsListAdapterViewHolder.itemView.getResources().getColor(R.color.colorWhite));

        }


    }

    @Override
    public int getItemCount() {
        return mVehiclesList.size();
    }


    public interface VehiclesListAdapterInterface {

        void onVehicleSelected(VehicleCategory vehicle);
    }

    class VehiclesListAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_eta)
        TextView tvEta;
        @BindView(R.id.iv_vehicle_type)
        ImageView ivVehicleType;
        @BindView(R.id.tv_vehicle_type)
        TextView tvVehicleType;

        VehiclesListAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                deselectAllVehicles();
                mVehiclesList.get(getAdapterPosition()).setSelected(true);
                mInterface.onVehicleSelected(mVehiclesList.get(getAdapterPosition()));
                notifyDataSetChanged();
            });
        }

        private void deselectAllVehicles() {
            for (int i = 0; i < mVehiclesList.size(); i++) {
                mVehiclesList.get(i).setSelected(false);
            }
        }
    }
}

