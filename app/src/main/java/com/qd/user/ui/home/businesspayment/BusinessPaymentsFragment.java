package com.qd.user.ui.home.businesspayment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.invoice.InvoiceListingResponse;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.PaginationListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BusinessPaymentsFragment extends BaseFragment implements BusinessPaymentsAdapter.BusinessPaymentsAdapterInterface, PaginationListener.PaginationListenerCallbacks, DatePickerDialog.OnDateSetListener {

    private static final int MINUTE = 59;
    private static final int HOUR_OF_DAY = 23;
    @BindView(R.id.tv_label_pending)
    TextView tvLabelPending;
    @BindView(R.id.tv_pending)
    TextView tvPending;
    @BindView(R.id.tv_label_payable)
    TextView tvLabelPayable;
    @BindView(R.id.tv_payable)
    TextView tvPayable;
    @BindView(R.id.rv_order_payment)
    RecyclerView rvOrderPayment;
    @BindView(R.id.view_bill_amount)
    View viewBillAmount;
    @BindView(R.id.tv_cleared)
    TextView tvCleared;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;

    private Unbinder unbinder;
    private BusinessPaymentsAdapter mAdapter;
    /**
     * A {@link IBusinessPaymentsHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IBusinessPaymentsHost mBusinessPaymentsHost;
    private BusinessPaymentsViewModel mBusinessPaymentsViewModel;
    private PaginationListener mPaginationListener;
    private int mNextPage;
    private ArrayList<Object> mInvoiceList;
    private boolean isRefreshing;
    private String mToDate, mFromDate;
    private Date mStartDate, mEndDate;
    private String selectDateFor;
    private Calendar mCalendar;
    private LinearLayoutManager layoutManager;

    public BusinessPaymentsFragment() {
        // Required empty public constructor
    }

    public static BusinessPaymentsFragment getInstance() {
        return new BusinessPaymentsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof IBusinessPaymentsHost) {
            mBusinessPaymentsHost = (IBusinessPaymentsHost) getParentFragment();
        } else
            throw new IllegalStateException("host must implement IBusinessPaymentsHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_payment_history, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBusinessPaymentsViewModel = new ViewModelProvider(this).get(BusinessPaymentsViewModel.class);
        mBusinessPaymentsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        observeLiveData();
        clearFilters();

        //get list of invoices generated
        showShimmerEffect();
        mBusinessPaymentsViewModel.getBusinessInvoiceList(mNextPage, mFromDate, mToDate);
    }

    /**
     * Observes live data as to respond in case there is any change through any network call
     */
    private void observeLiveData() {
        mBusinessPaymentsViewModel.getInvoiceListingLiveData().observe(getViewLifecycleOwner(), invoiceListingResponse -> {
            hideShimmerEffect();
            hideProgressDialog();
            if (invoiceListingResponse != null) {

                tvPayable.setText(String.format(Locale.getDefault(), "%.3f KWD", invoiceListingResponse.getResult().getPayableAmountToQd()));
                tvPending.setText(String.format(Locale.getDefault(), "%.3f KWD", invoiceListingResponse.getResult().getPendingAmountFromQd()));

                if (invoiceListingResponse.getResult().getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (invoiceListingResponse.getResult().getData() != null && invoiceListingResponse.getResult().getData().size() > 0) {
                    if (mInvoiceList != null && mInvoiceList.size() > 0) {
                        mAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mInvoiceList.clear();
                        }
                        setResponseToAdapter(invoiceListingResponse);
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mInvoiceList.clear();
                        }

                        setResponseToAdapter(invoiceListingResponse);
                        rvOrderPayment.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }

            }
        });
    }


    public boolean showOrHideFilter() {
        if (llFilter.getVisibility() == View.VISIBLE) {
            llFilter.setVisibility(View.GONE);
            TransitionManager.beginDelayedTransition(llFilter);
            clearFilters();
            mBusinessPaymentsViewModel.getBusinessInvoiceList(mNextPage, mFromDate, mToDate);
            return false;
        } else {
            llFilter.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(llFilter);
            return true;
        }
    }

    @Override
    protected void initViewsAndVariables() {
        mInvoiceList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mCalendar = Calendar.getInstance();
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);
        hideViews();

        setRefreshLayoutListener();
        setBusinessPaymentsAdapter();
    }

    private void clearFilters() {
        mNextPage = 1;
        isRefreshing = true;
        mFromDate = mBusinessPaymentsViewModel.getRegistrationDate();
        mStartDate = DateFormatter.getInstance().getParsedDate(mBusinessPaymentsViewModel.getRegistrationDate());
        mToDate = DateFormatter.getInstance().getFormattedDateInUtc(Calendar.getInstance().getTime());
        mEndDate = Calendar.getInstance().getTime();

        tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(mToDate));
    }

    /**
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        swipeRefresh.setOnRefreshListener(
                () -> {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    refreshList();
                    mPaginationListener.setFetchingStatus(false);

                }
        );
    }

    /**
     * Refreshes list by setting {@literal #mNextPage =1}
     */
    private void refreshList() {
        isRefreshing = true;
        mNextPage = 1;
        mBusinessPaymentsViewModel.getBusinessInvoiceList(mNextPage, mFromDate, mToDate);
    }

    /**
     * Hides recycler view and and show no data found  message if data is not available
     */
    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        rvOrderPayment.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        hideViews();
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    private void hideViews() {
        tvLabelPayable.setVisibility(View.GONE);
        tvLabelPending.setVisibility(View.GONE);
        tvPayable.setVisibility(View.GONE);
        tvPending.setVisibility(View.GONE);
        tvCleared.setVisibility(View.GONE);
        viewBillAmount.setVisibility(View.GONE);
    }

    @OnClick({R.id.tv_filter, R.id.tv_start_date, R.id.tv_end_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_filter:
                mNextPage = 1;
                isRefreshing = true;
                showShimmerEffect();
                mBusinessPaymentsViewModel.getBusinessInvoiceList(mNextPage, mFromDate, mToDate);
                break;
            case R.id.tv_start_date:
                selectDateFor = getString(R.string.s_from_date);
                openDatePicker();
                break;
            case R.id.tv_end_date:
                selectDateFor = getString(R.string.s_to_date);
                openDatePicker();
                break;
        }

    }

    /**
     * Opens date picker dialog and sets selected date according to previously selected ones,
     * based on {@literal selectDateFor} and initially {@literal mStartDate} contains registration date
     * of user amd {@literal mEndDate} contains present date
     */
    private void openDatePicker() {
        Date minDate;
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.setTime(mStartDate);
            minDate = DateFormatter.getInstance().getParsedDate(mBusinessPaymentsViewModel.getRegistrationDate());
        } else {
            mCalendar.setTime(mEndDate);
            minDate = mStartDate;
        }

        if (getContext() != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(minDate.getTime());
            datePickerDialog.show();
        }
    }


    /**
     * Sets invoice list from response to adapter and calls {@code mAdapter.notifyDataSetChanged()}
     * to reflect the changes
     *
     * @param invoiceListingResponse Response containing all the invoices, it can't be null and its size will
     *                               always be greater than 0, as it's always called after checking for null
     *                               on its contents.
     */
    private void setResponseToAdapter(InvoiceListingResponse invoiceListingResponse) {
        mInvoiceList.addAll(invoiceListingResponse.getResult().getData());
        showViewsForData();
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Sets adapter to show invoices list
     */
    private void setBusinessPaymentsAdapter() {
        mAdapter = new BusinessPaymentsAdapter(mInvoiceList, this);
        rvOrderPayment.setLayoutManager(layoutManager);
        rvOrderPayment.setAdapter(mAdapter);
        rvOrderPayment.scheduleLayoutAnimation();
        rvOrderPayment.addOnScrollListener(mPaginationListener);
    }

    /**
     * Shows views when data is available for showing
     */
    private void showViewsForData() {
        tvNoList.setVisibility(View.GONE);
        rvOrderPayment.setVisibility(View.VISIBLE);

        tvLabelPayable.setVisibility(View.VISIBLE);
        tvLabelPending.setVisibility(View.VISIBLE);
        tvPayable.setVisibility(View.VISIBLE);
        tvPending.setVisibility(View.VISIBLE);
        viewBillAmount.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        hideViews();
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrderPayment.setVisibility(View.GONE);

    }

    /**
     * Hides shimmer view and set views accordingly
     */
    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerViewContainer.stopShimmer();
        shimmerViewContainer.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);

    }

    /**
     * Shows shimmer view and set views accordingly
     */
    private void showShimmerEffect() {
        swipeRefresh.setVisibility(View.GONE);
        shimmerViewContainer.setVisibility(View.VISIBLE);
        shimmerViewContainer.startShimmer();
    }

    @Override
    public void onPayNowClicked(String id, BigDecimal totalAmount) {
        mBusinessPaymentsHost.openOnlinePaymentsFragment(createRequestForInitializingPayment(id, totalAmount));
    }

    private InitializePaymentRequest createRequestForInitializingPayment(String id, BigDecimal totalAmount) {
        InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
        initializePaymentRequest.setAmount(totalAmount);
        initializePaymentRequest.setTransactionFor(AppConstants.TransactionFor.INVOICE);
        initializePaymentRequest.setInvoiceId(id);

        return initializePaymentRequest;
    }
    /**
     * Opens invoice details based on {@literal id} , passed through {@link BusinessPaymentsAdapter}
     * corresponding to the particular invoice whose details user want to see
     *
     * @param id Invoice id whose details are to be shown
     */
    @Override
    public void onRowClicked(String id) {
        mBusinessPaymentsHost.openInvoiceDetails(id);
    }

    /**
     * Hits a network call for data on next page, if present
     */
    @Override
    public void loadMoreItems() {
        mAdapter.showProgress();
        mBusinessPaymentsViewModel.getBusinessInvoiceList(mNextPage, mFromDate, mToDate);
    }

    /**
     * Callback is received in this method, when a particular date is selected from {@link DatePickerDialog}
     *
     * @param view       the picker associated with the dialog
     * @param year       the selected year
     * @param month      the selected month (0-11 for compatibility with
     *                   {@link Calendar#MONTH})
     * @param dayOfMonth th selected day of the month (1-31, depending on month)
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.set(year, month, dayOfMonth, 0, 0);
            mFromDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mStartDate = DateFormatter.getInstance().getParsedDate(mFromDate);
            tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        } else {
            mCalendar.set(year, month, dayOfMonth, HOUR_OF_DAY, MINUTE);
            mToDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mEndDate = DateFormatter.getInstance().getParsedDate(mToDate);
            tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(mToDate));
        }
    }


    public boolean isFilterApplied() {
        return llFilter.getVisibility() == View.VISIBLE;
    }


    public interface IBusinessPaymentsHost {

        void openInvoiceDetails(String id);

        void openOnlinePaymentsFragment(InitializePaymentRequest requestForInitializingPayment);
    }

}
