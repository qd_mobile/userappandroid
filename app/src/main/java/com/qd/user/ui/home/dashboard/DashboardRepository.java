package com.qd.user.ui.home.dashboard;

import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;
import com.qd.user.model.vendor.vendorstatus.VendorStatusResponse;

class DashboardRepository {
    void hitVendorStatusApi(final RichMediatorLiveData<VendorStatusResponse> mVendorStatusLiveData, String deviceId, String platform) {
        DataManager.getInstance().hitVendorStatusApi(DataManager.getInstance().getUserId(), deviceId, platform).enqueue(new NetworkCallback<VendorStatusResponse>() {
            @Override
            public void onSuccess(VendorStatusResponse vendorStatusResponse) {
                mVendorStatusLiveData.setLoadingState(false);
                setDataInPreference(vendorStatusResponse);
                mVendorStatusLiveData.setValue(vendorStatusResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mVendorStatusLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mVendorStatusLiveData.setError(t);
            }

            private void setDataInPreference(VendorStatusResponse vendorStatusResponse) {
                DataManager.getInstance().setUserType(vendorStatusResponse.getResult().get(0).getCashClientStatus());
                DataManager.getInstance().setCurrentStatus(vendorStatusResponse.getResult().get(0).getCurrentStatus());
                DataManager.getInstance().setVendorStatus(vendorStatusResponse.getResult().get(0).getStatus());
                DataManager.getInstance().setCashClientCurrentStatus(vendorStatusResponse.getResult().get(0).getCashClientCurrentStatus());
                DataManager.getInstance().setServiceType(vendorStatusResponse.getResult().get(0).getServiceOffered());
                String fullBusinessAddress = new GsonBuilder().create().toJson(vendorStatusResponse.getResult().get(0).getFullBusinessAddress());
                DataManager.getInstance().setBusinessAddress(fullBusinessAddress);
                DataManager.getInstance().setNotificationStatus(vendorStatusResponse.getResult().get(0).getEnableNotification());
                DataManager.getInstance().setVendorPaymentMode(vendorStatusResponse.getResult().get(0).getPaymentModes());
            }
        });

    }



    String getVendorStatus() {
        return DataManager.getInstance().getVendorStatus();
    }

    public String getCashClientCurrentStatus() {
        return DataManager.getInstance().getCashClientCurrentStatus();
    }

    void getVendorInfo(final RichMediatorLiveData<VendorInfoResponse> vendorInfoResponseLiveData) {
        DataManager.getInstance().getVendorInfo(DataManager.getInstance().getUserId()).enqueue(new NetworkCallback<VendorInfoResponse>() {
            @Override
            public void onSuccess(VendorInfoResponse vendorInfoResponse) {
                vendorInfoResponseLiveData.setValue(vendorInfoResponse);
                vendorInfoResponseLiveData.setLoadingState(false);
                DataManager.getInstance().setVendorPaymentMode(vendorInfoResponse.getResult().getPaymentModes());
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                vendorInfoResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                vendorInfoResponseLiveData.setError(t);
            }
        });
    }
}
