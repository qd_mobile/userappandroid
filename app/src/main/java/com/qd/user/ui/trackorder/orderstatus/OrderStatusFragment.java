package com.qd.user.ui.trackorder.orderstatus;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.orderdetail.DriverTrackingstatus;
import com.qd.user.model.orderdetail.DropLocation;
import com.qd.user.model.orderdetail.Orderstatus;
import com.qd.user.ui.home.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderStatusFragment extends BaseFragment {

    private Unbinder unbinder;
    @BindView(R.id.tv_label_order_status)
    TextView tvLabelOrderStatus;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.iv_cross)
    ImageView ivCross;
    @BindView(R.id.iv_order_created)
    ImageView ivOrderCreated;
    @BindView(R.id.tv_order_created)
    TextView tvOrderCreated;
    @BindView(R.id.tv_order_created_time)
    TextView tvOrderCreatedTime;
    @BindView(R.id.view_line_order_created)
    View viewLineOrderCreated;
    @BindView(R.id.iv_driver_aligned)
    ImageView ivDriverAligned;
    @BindView(R.id.tv_driver_aligned)
    TextView tvDriverAligned;
    @BindView(R.id.tv_driver_aligned_eta)
    TextView tvDriverAlignedEta;
    @BindView(R.id.tv_driver_aligned_time)
    TextView tvDriverAlignedTime;
    @BindView(R.id.view_line_driving_to_delivery)
    View viewLineDrivingToDelivery;
    @BindView(R.id.iv_driver_reached_pickup_location)
    ImageView ivDriverReachedPickupLocation;
    @BindView(R.id.tv_driver_reached_pickup_location)
    TextView tvDriverReachedPickupLocation;
    @BindView(R.id.view_driver_reached_pickup_location)
    View viewDriverReachedPickupLocation;
    @BindView(R.id.tv_driver_reached_pickup_location_time)
    TextView tvDriverReachedPickupLocationTime;
    @BindView(R.id.iv_order_picked_up)
    ImageView ivOrderPickedUp;
    @BindView(R.id.tv_order_picked_up)
    TextView tvOrderPickedUp;
    @BindView(R.id.tv_order_picked_up_eta)
    TextView tvOrderPickedUpEta;
    @BindView(R.id.view_line_order_picked_up)
    View viewLineOrderPickedUp;
    @BindView(R.id.tv_order_picked_up_time)
    TextView tvOrderPickedUpTime;
    @BindView(R.id.iv_driver_on_the_way)
    ImageView ivDriverOnTheWay;
    @BindView(R.id.tv_driver_on_the_way)
    TextView tvDriverOnTheWay;
    @BindView(R.id.tv_driver_on_the_way_time)
    TextView tvDriverOnTheWayTime;
    @BindView(R.id.view_driver_on_the_way)
    View viewDriverOnTheWay;
    @BindView(R.id.iv_delivered)
    ImageView ivDelivered;
    @BindView(R.id.tv_delivered)
    TextView tvDelivered;
    @BindView(R.id.rv_order_status)
    RecyclerView rvOrderStatus;
    @BindView(R.id.tv_delivered_time)
    TextView tvDeliveredTime;
    /**
     * A {@link IOrderStatusHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IOrderStatusHost mOrderStatusHost;

    /**
     * A {@link OrderStatusViewModel} object to handle all the actions and business logic of orders listing.
     */
    private OrderStatusViewModel mOrderStatusViewModel;
    private String mOrderUniqueId;
    private ArrayList<DriverTrackingstatus> mStatusList;
    private ArrayList<String> mCompleteStatusList;
    private OrderStatusAdapter mAdapter;
    private String mOrderId;

    public static OrderStatusFragment getInstance(String orderUniqueId) {
        Bundle bundle = new Bundle();
        bundle.putString("OrderUniqueId", orderUniqueId);
        OrderStatusFragment orderStatusFragment = new OrderStatusFragment();
        orderStatusFragment.setArguments(bundle);
        return orderStatusFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IOrderStatusHost) {
            mOrderStatusHost = (IOrderStatusHost) context;
        } else
            throw new IllegalStateException("host must implement IOrderStatusHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_status, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mOrderStatusViewModel = new ViewModelProvider(this).get(OrderStatusViewModel.class);
        mOrderStatusViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        observeLiveData();
        initializeSocketEvents();
        showProgressDialog();
        mOrderStatusViewModel.getCurrentOrderStatus(mOrderUniqueId);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void initViewsAndVariables() {
        if (getArguments() != null && getArguments().getString("OrderUniqueId") != null) {
            mOrderUniqueId = getArguments().getString("OrderUniqueId");
        }

        tvOrderId.setText(String.format("Order Id: %s", mOrderUniqueId));
        mStatusList = new ArrayList<>();
        createCompleteStatusList();

    }

    /**
     * Create a list for complete order status of the current order
     */
    private void createCompleteStatusList() {
        mCompleteStatusList = new ArrayList<>();

        /*mCompleteStatusList.add(AppConstants.OrderStatus.ORDER_CREATED);
        mCompleteStatusList.add(AppConstants.OrderStatus.DRIVER_ALIGNED);
        mCompleteStatusList.add(AppConstants.OrderStatus.OUT_FOR_PICKUP);
        mCompleteStatusList.add(AppConstants.OrderStatus.PICKEDUP);
        mCompleteStatusList.add(AppConstants.OrderStatus.OUT_FOR_DELIVERY);
        mCompleteStatusList.add(AppConstants.OrderStatus.DELIVERED);*/


        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.ORDER_CREATED);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.DRIVER_ACEEPTED);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.DRIVING_TO_PICKUP);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.ARRIVED_AT_PICKUP);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.ORDER_PICKEDUP);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.DRIVING_TO_DROP_OFF);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.ARRIVED_AT_DROP_OFF);
        mCompleteStatusList.add(AppConstants.OrderTrackingStatus.ORDER_DELIVERED);
        setUpAdapter();
    }


    private void observeLiveData() {
        mOrderStatusViewModel.getOrderStatusLiveData().observe(getViewLifecycleOwner(), orderStatusResponse -> {
            if (orderStatusResponse != null) {
                mStatusList.clear();
                if (isOrderPickedUp(orderStatusResponse.getOrderStatus()))
                    mStatusList.addAll(orderStatusResponse.getDropLocation().get(currentDropInProgress(orderStatusResponse.getDropLocation())).getDriverTrackingStatus());
                else
                    mStatusList.addAll(orderStatusResponse.getDropLocation().get(0).getDriverTrackingStatus());

                /*if (mStatusList.size() > mCompleteStatusList.size()) {
                    mCompleteStatusList.clear();
                    for (int j = 0; j < mStatusList.size(); j++) {
                        mCompleteStatusList.add(mStatusList.get(j).getStatus());
                    }
                }*/

                mAdapter.notifyDataSetChanged();
                mOrderId = orderStatusResponse.getId();
                mOrderStatusHost.setCurrentOrderStatus(mOrderId, mStatusList.get(mStatusList.size() - 1).getStatus());

            }
        });

        mOrderStatusViewModel.getOrdersLiveData().observe(getViewLifecycleOwner(), ordersResponse -> {
            hideProgressDialog();
            if (ordersResponse != null) {
                mStatusList.clear();

                if (isOrderPickedUp(ordersResponse.getResult().get(0).getOrderStatus()))
                    mStatusList.addAll(ordersResponse.getResult().get(0).getDropLocation().get(currentDropInProgress(ordersResponse.getResult().get(0).getDropLocation())).getDriverTrackingStatus());
                else
                    mStatusList.addAll(ordersResponse.getResult().get(0).getDropLocation().get(0).getDriverTrackingStatus());

                /*if (mStatusList.size() > mCompleteStatusList.size()) {
                    mCompleteStatusList.clear();
                    for (int j = 0; j < mStatusList.size(); j++) {
                        mCompleteStatusList.add(mStatusList.get(j).getStatus());
                    }
                }*/

                mAdapter.notifyDataSetChanged();
                mOrderId = ordersResponse.getResult().get(0).getId();
                mOrderStatusHost.setCurrentOrderStatus(mOrderId, mStatusList.get(mStatusList.size() - 1).getStatus());

            }
        });
    }

    private int currentDropInProgress(List<DropLocation> dropLocation) {
        for (int i = 0; i < dropLocation.size(); i++) {
            if (dropLocation.get(i).getIsElementInprogress()) {
                return i;
            }
        }

        return 0;
    }

    private boolean isOrderPickedUp(List<Orderstatus> orderStatus) {
        for (int i = 0; i < orderStatus.size(); i++) {
            if (orderStatus.get(i).getStatus().equals(AppConstants.OrderStatus.PICKEDUP)) {
                return true;
            }
        }

        return false;
    }

    private void setUpAdapter() {
        mAdapter = new OrderStatusAdapter(mStatusList, mCompleteStatusList);
        rvOrderStatus.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvOrderStatus.setAdapter(mAdapter);
        rvOrderStatus.scheduleLayoutAnimation();
    }


    /**
     * Initialize socket events to emit for order status
     */
    private void initializeSocketEvents() {
        mOrderStatusViewModel.emitForOrderStatus(mOrderUniqueId);
        mOrderStatusViewModel.listenToOrderStatusEvent();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_cross})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.iv_cross) {
            if (mStatusList.size() > 0 && (mStatusList.get(mStatusList.size() - 1).getStatus().equals(AppConstants.OrderStatus.DELIVERED) ||
                    mStatusList.get(mStatusList.size() - 1).getStatus().equals(AppConstants.OrderStatus.CANCELLED) ||
                    mStatusList.get(mStatusList.size() - 1).getStatus().equals(AppConstants.OrderStatus.CASHBACK_RETURNED) ||
                    mStatusList.get(mStatusList.size() - 1).getStatus().equals(AppConstants.OrderStatus.RETURNED))) {
                mOrderStatusHost.backToOrderDetailsFragment(mOrderId);
            } else
                mOrderStatusHost.onBackPressed();
        }
    }

    public interface IOrderStatusHost {

        void onBackPressed();

        void backToOrderDetailsFragment(String orderId);

        void setCurrentOrderStatus(String mOrderId, String orderstatus);
    }

}
