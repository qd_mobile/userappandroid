package com.qd.user.ui.trackorder.tracking;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.orderstatus.CurrentLocationResponse;
import com.qd.user.socket.SocketConstants;
import com.qd.user.ui.createorder.confirmation.ConfirmOrderFragment;
import com.qd.user.ui.trackorder.orderdetail.OrderDetailFragment;

import org.json.JSONException;
import org.json.JSONObject;

public class OrderTrackingViewModel extends ViewModel {
    private RichMediatorLiveData<OrderDetailResponse> mOrderDetailsLiveData;
    private RichMediatorLiveData<CurrentLocationResponse> mCurrentLocationResponseLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private OrderTrackingRepository mRepo = new OrderTrackingRepository();
    private RichMediatorLiveData<DirectionsApiResponse> mDirectionsApiLiveData;
    private RichMediatorLiveData<OrderCancellationResponse> mOrderCancellationLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mOrderDetailsLiveData == null) {
            mOrderDetailsLiveData = new RichMediatorLiveData<OrderDetailResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mDirectionsApiLiveData == null) {
            mDirectionsApiLiveData = new RichMediatorLiveData<DirectionsApiResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }

        if (mOrderCancellationLiveData == null) {
            mOrderCancellationLiveData = new RichMediatorLiveData<OrderCancellationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mCurrentLocationResponseLiveData == null) {
            mCurrentLocationResponseLiveData = new RichMediatorLiveData<CurrentLocationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

    }

    /**
     * This method gives the login live data object to {@link OrderDetailFragment}
     *
     * @return {@link #mOrderDetailsLiveData}
     */
    RichMediatorLiveData<OrderDetailResponse> getOrderDetailsLiveData() {
        return mOrderDetailsLiveData;
    }

    /**
     * This method gives the login live data object to {@link OrderDetailFragment}
     *
     * @return {@link #mOrderCancellationLiveData}
     */
    RichMediatorLiveData<OrderCancellationResponse> getOrderCancellationLiveData() {
        return mOrderCancellationLiveData;
    }


    void getOrderDetail(String orderId) {
        mRepo.getOrderDetails(orderId, mOrderDetailsLiveData);
    }


    public void cancelOrder(JSONObject request) {
        mOrderCancellationLiveData.setLoadingState(true);
        mRepo.cancelOrder(request, mOrderCancellationLiveData);
    }

    /**
     * This method gives the ConfirmOrder live data object to {@link ConfirmOrderFragment}
     *
     * @return {@link #mDirectionsApiLiveData}
     */
    RichMediatorLiveData<DirectionsApiResponse> getDirectionsApiLiveData() {
        return mDirectionsApiLiveData;
    }

    /**
     * This method gives the Current order live data object to {@link OrderTrackingFragment}
     *
     * @return {@link #mCurrentLocationResponseLiveData}
     */
    RichMediatorLiveData<CurrentLocationResponse> getCurrentLocationResponseLiveData() {
        return mCurrentLocationResponseLiveData;
    }

    void getDirectionsResponse(String url) {
        mRepo.getDirectionsApiResponse(mDirectionsApiLiveData, url);
    }

    void listenForDriverCurrentLocation() {
        mRepo.listenForDriverCurrentLocation(mCurrentLocationResponseLiveData);
    }

    void emitToListenDriverCurrentLocation(String id, String driverId) {
        mRepo.emitToListenDriverCurrentLocation(createJsonForCurrentLocation(id, driverId));

    }

    private JSONObject createJsonForCurrentLocation(String id, String driverId) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(SocketConstants.KEY_ORDER_ID, id);
            jsonObject.put(SocketConstants.KEY_DRIVER_ID, driverId);
            jsonObject.put(SocketConstants.TYPE, SocketConstants.EVENT_DRIVER_LOCATION);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    void disconnectSocketListenerForCurrentLocation() {
        mRepo.disconnectSocketListenerForCurrentLocation();
    }

    void autoCancelOrder(String id) {
        mOrderCancellationLiveData.setLoadingState(true);
        mRepo.autoCancelOrder(id, mOrderCancellationLiveData);
    }
}
