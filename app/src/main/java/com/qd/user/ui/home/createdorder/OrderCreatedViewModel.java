package com.qd.user.ui.home.createdorder;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.myorders.OrdersResponse;
import com.qd.user.model.recievedorders.RecievedOrdersResponse;

public class OrderCreatedViewModel extends ViewModel {
    private RichMediatorLiveData<OrdersResponse> mOrdersLiveData;
    private RichMediatorLiveData<RecievedOrdersResponse> mOrdersReceivedLiveData;

    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private OrderCreatedRepository mRepo = new OrderCreatedRepository();
    private RichMediatorLiveData<CommonListResponse> mCancellationReasonsLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mOrdersLiveData == null) {
            mOrdersLiveData = new RichMediatorLiveData<OrdersResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
            if (mOrdersReceivedLiveData == null) {
                mOrdersReceivedLiveData = new RichMediatorLiveData<RecievedOrdersResponse>() {
                    @Override
                    protected Observer<FailureResponse> getFailureObserver() {
                        return failureResponseObserver;
                    }

                    @Override
                    protected Observer<Throwable> getErrorObserver() {
                        return errorObserver;
                    }

                    @Override
                    protected Observer<Boolean> getLoadingStateObserver() {
                        return mLoadingStateObserver;
                    }
                };

            }
            if (mCancellationReasonsLiveData == null) {
                mCancellationReasonsLiveData = new RichMediatorLiveData<CommonListResponse>() {
                    @Override
                    protected Observer<FailureResponse> getFailureObserver() {
                        return failureResponseObserver;
                    }

                    @Override
                    protected Observer<Throwable> getErrorObserver() {
                        return errorObserver;
                    }

                    @Override
                    protected Observer<Boolean> getLoadingStateObserver() {
                        return mLoadingStateObserver;
                    }
                };
            }
        }

    }

    /**
     * This method gives the login live data object to {@link OrderCreatedFragment}
     *
     * @return {@link #mOrdersLiveData}
     */
    RichMediatorLiveData<OrdersResponse> getOrdersLiveData() {
        return mOrdersLiveData;
    }

    /**
     * This method gives the login live data object to {@link com.qd.user.ui.createorder.recipientdetails.RecipientDetailsFragment}
     *
     * @return {@link #mOrdersLiveData}
     */
    public RichMediatorLiveData<RecievedOrdersResponse> getOrdersReceivedLiveData() {
        return mOrdersReceivedLiveData;
    }

    void getCreatedOrdersList(int mNexPage, String fromDate, String toDate) {
        mRepo.getOrdersList(mNexPage, mOrdersLiveData, fromDate, toDate);
    }

    public String getRegistrationDate() {
        return mRepo.getRegistrationDate();
    }

    public void getReceivedOrdersList(int mNextPage, String fromDate, String toDate) {
        mRepo.getReceivedOrdersList(mNextPage, mOrdersReceivedLiveData, fromDate, toDate);

    }


    void getCancellationReasons() {
        mRepo.getCancellationReasons(mCancellationReasonsLiveData);
    }
}
