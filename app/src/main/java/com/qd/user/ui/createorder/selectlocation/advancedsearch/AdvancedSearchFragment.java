package com.qd.user.ui.createorder.selectlocation.advancedsearch;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.request.Geometry;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.model.kuwaitfinder.geocode.Attributes;
import com.qd.user.model.kuwaitfinder.governorate.Feature;
import com.qd.user.ui.createorder.droplocation.DropLocationsListFragment;
import com.qd.user.ui.createorder.recipientdetails.RecipientDetailsFragment;
import com.qd.user.ui.createorder.selectlocation.map.MapFragment;
import com.qd.user.ui.home.profile.ProfileFragment;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.LocaleManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdvancedSearchFragment extends BaseFragment implements AdvancedSearchAdapter.AdvanceSearchAdapterInterface, View.OnClickListener {


    private static final int TEXT_SIZE = 15;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_label_governorate)
    TextView tvLabelGovernorate;
    @BindView(R.id.tv_governorate)
    EditText tvGovernorate;
    @BindView(R.id.tv_error_governorate)
    TextView tvErrorGovernorate;
    @BindView(R.id.tv_label_area)
    TextView tvLabelArea;
    @BindView(R.id.tv_area)
    EditText tvArea;
    @BindView(R.id.view_area)
    View viewArea;
    @BindView(R.id.tv_error_area)
    TextView tvErrorArea;
    @BindView(R.id.tv_label_block_number)
    TextView tvLabelBlockNumber;
    @BindView(R.id.tv_block_number)
    EditText tvBlockNumber;
    @BindView(R.id.view_block_number)
    View viewBlockNumber;
    @BindView(R.id.tv_error_block_no)
    TextView tvErrorBlockNo;
    @BindView(R.id.tv_label_street)
    TextView tvLabelStreet;
    @BindView(R.id.tv_street)
    EditText tvStreet;
    @BindView(R.id.view_street)
    View viewStreet;
    @BindView(R.id.tv_error_street)
    TextView tvErrorStreet;
    @BindView(R.id.tv_label_avenue)
    TextView tvLabelAvenue;
    @BindView(R.id.tv_avenue)
    EditText tvAvenue;
    @BindView(R.id.view_avenue)
    View viewAvenue;
    @BindView(R.id.tv_label_house_building)
    TextView tvLabelHouseBuilding;
    @BindView(R.id.tv_house_building)
    EditText tvHouseBuilding;
    @BindView(R.id.view_house_building)
    View viewHouseBuilding;
    @BindView(R.id.tv_label_floor)
    TextView tvLabelFloor;
    @BindView(R.id.tv_floor)
    EditText tvFloor;
    @BindView(R.id.view_floor)
    View viewFloor;
    @BindView(R.id.tv_label_apartment_office)
    TextView tvLabelApartmentOffice;
    @BindView(R.id.tv_apartment_office)
    EditText tvApartmentOffice;
    @BindView(R.id.view_apartment_office)
    View viewApartmentOffice;
    @BindView(R.id.sv_advanced_search)
    ScrollView svAdvancedSearch;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.fab_save_address)
    FloatingActionButton fabSaveAddress;
    @BindView(R.id.tv_save_address_label)
    TextView tvSaveAddressLabel;
    @BindView(R.id.view_governorate)
    View viewGovernorate;

    private Unbinder unbinder;
    private IAdvancedSearchHost mHost;
    private AdvancedSearchViewModel mAdvancedSearchViewModel;
    //private String TAG = AdvancedSearchFragment.class.getName();
    private String mToken;
    private TreeSet<String> mAddressList;
    private Dialog mAdvanceSearchDialog;
    private List<Feature> mGovernorateList;
    private int mGovNo;
    private List<com.qd.user.model.kuwaitfinder.neighbourhood.Feature> mNeighbourhoodList;
    private int mNhoodNo;
    private String mBlockId;
    private List<com.qd.user.model.kuwaitfinder.streets.Feature> mStreetsList;

    private List<Attributes> mGeocodeList;
    private boolean mIsForPickup;
    private List<Double> mLatLng;
    private Result mResult;
    private String mNavigation;
    private String mSavedLocationName;


    public AdvancedSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_advanced_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static AdvancedSearchFragment getInstance(boolean pickup, Parcelable result, String navigation) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(NetworkConstants.KEY_IS_FOR_PICKUP, pickup);
        bundle.putParcelable(AppConstants.KEY_SAVED_ADDRESS_RESULT, result);
        bundle.putString(AppConstants.ACTION_NAVIGATION, navigation);
        AdvancedSearchFragment advancedSearchFragment = new AdvancedSearchFragment();
        advancedSearchFragment.setArguments(bundle);
        return advancedSearchFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IAdvancedSearchHost) {
            mHost = (IAdvancedSearchHost) context;
        } else throw new IllegalStateException("Host must implement IAdvancedSearchHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mAdvancedSearchViewModel = new ViewModelProvider(this).get(AdvancedSearchViewModel.class);
        mAdvancedSearchViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        super.onViewCreated(view, savedInstanceState);
        observeLiveData();

        mAdvancedSearchViewModel.getTokenForKuwaitFinder();

    }

    private void observeLiveData() {

        mAdvancedSearchViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_GOVERNORATE:
                        showGovernorateValidationError(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_AREA:
                        showAreaValidationError(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_BLOCK:
                        showBlockValidationError(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_STREET:
                        showStreetValidationError(validationErrors.getValidationMessage());
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_LAT_LNG:
                        showSnackBar(validationErrors.getValidationMessage());
                        break;
                }
            }
        });

        mAdvancedSearchViewModel.getSaveAddressLiveData().observe(getViewLifecycleOwner(), saveAddressResponse -> {
            if (saveAddressResponse != null) {
                if (mNavigation.equals(ProfileFragment.class.getName())) {
                    mHost.backToProfileFragment(saveAddressResponse.getResult());
                } else {
                    mSavedLocationName = saveAddressResponse.getResult().getSavedLocation().getName();
                    showSnackBar(getString(R.string.s_address_saved_successfully));
                    fabSaveAddress.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_address_active));
                    mHost.updateSavedAddress(saveAddressResponse.getResult());
                }
            }
        });

        mAdvancedSearchViewModel.getTokenLiveData().observe(getViewLifecycleOwner(), tokenResponse -> {
            hideProgressDialog();
            if (tokenResponse != null) {
                mToken = tokenResponse.getToken();
            }
        });

        mAdvancedSearchViewModel.getGovernorateLiveData().observe(getViewLifecycleOwner(), governorateResponse -> {
            if (governorateResponse != null) {
                mGovernorateList.clear();
                mAddressList.clear();
                mGovernorateList.addAll(governorateResponse.getFeatures());
                for (int i = 0; i < governorateResponse.getFeatures().size(); i++) {
                    if (LocaleManager.getLanguage(getContext()).equals("en"))
                        mAddressList.add(governorateResponse.getFeatures().get(i).getAttributes().getGovernorateenglish());
                    else
                        mAddressList.add(governorateResponse.getFeatures().get(i).getAttributes().getGovernoratearabic());
                }
                openSelectionDialog(mAddressList, getString(R.string.s_governorate), AppConstants.Address.GOVERNORATE);

            }
        });

        mAdvancedSearchViewModel.getNeighbourhoodLiveData().observe(getViewLifecycleOwner(), neighbourhoodResponse -> {
            hideProgressDialog();
            if (neighbourhoodResponse != null) {
                mNeighbourhoodList.clear();
                mNeighbourhoodList.addAll(neighbourhoodResponse.getFeatures());
                mAddressList.clear();
                for (int i = 0; i < neighbourhoodResponse.getFeatures().size(); i++) {
                    if (LocaleManager.getLanguage(getContext()).equals("en"))
                        mAddressList.add(neighbourhoodResponse.getFeatures().get(i).getAttributes().getNeighborhoodenglish());
                    else
                        mAddressList.add(neighbourhoodResponse.getFeatures().get(i).getAttributes().getNeighborhoodarabic());

                }
                openSelectionDialog(mAddressList, getString(R.string.s_area), AppConstants.Address.AREA);

            }
        });

        mAdvancedSearchViewModel.getBlocksLiveData().observe(getViewLifecycleOwner(), blocksResponse -> {
            mAddressList.clear();
            if (blocksResponse != null && blocksResponse.getFeatures().size() > 0) {
                if (blocksResponse.getFeatures().size() > 0) {
                    for (int i = 0; i < blocksResponse.getFeatures().size(); i++) {
                        if (LocaleManager.getLanguage(getContext()).equals("en"))
                            mAddressList.add(getString(R.string.s_block) + " " + blocksResponse.getFeatures().get(i).getAttributes().getBlockenglish());
                        else
                            mAddressList.add(getString(R.string.s_block) + " " + blocksResponse.getFeatures().get(i).getAttributes().getBlockarabic());

                    }

                }
            } else {
                mAddressList.add(getString(R.string.s_no_data_found));
            }

            openSelectionDialog(mAddressList, getString(R.string.s_block_number), AppConstants.Address.BLOCK);
        });

        mAdvancedSearchViewModel.getStreetsLiveData().observe(getViewLifecycleOwner(), streetsResponse -> {
            mAddressList.clear();
            if (streetsResponse != null && streetsResponse.getFeatures().size() > 0) {
                mStreetsList.clear();
                mStreetsList.addAll(streetsResponse.getFeatures());
                for (int i = 0; i < streetsResponse.getFeatures().size(); i++) {
                    if (LocaleManager.getLanguage(getContext()).equals("en"))
                        mAddressList.add(streetsResponse.getFeatures().get(i).getAttributes().getStreetenglish());
                    else
                        mAddressList.add(streetsResponse.getFeatures().get(i).getAttributes().getStreetarabic());
                }
            } else mAddressList.add(getString(R.string.s_no_data_found));
            openSelectionDialog(mAddressList, getString(R.string.s_street), AppConstants.Address.STREET);
        });

        mAdvancedSearchViewModel.getGeoCodeLiveData().observe(getViewLifecycleOwner(), geoCodeLocationResponse -> {
            mAddressList.clear();
            if (geoCodeLocationResponse != null) {
                mGeocodeList.clear();
                Collection<Attributes> geoSet = new TreeSet<>();
                if (geoCodeLocationResponse.getFeatures() != null && geoCodeLocationResponse.getFeatures().size() > 0)
                    for (int i = 0; i < geoCodeLocationResponse.getFeatures().size(); i++) {
                        geoSet.add(geoCodeLocationResponse.getFeatures().get(i).getAttributes());
                    }
                mGeocodeList.addAll(geoSet);
                if (mGeocodeList.size() > 0) {
                    for (int i = 0; i < mGeocodeList.size(); i++) {
                        if (!mGeocodeList.get(i).getHouseenglish().equals("0"))
                            mAddressList.add(" House " + mGeocodeList.get(i).getHouseenglish() + " Parcel " + mGeocodeList.get(i).getParcelenglish());
                        else
                            mAddressList.add("Parcel " + mGeocodeList.get(i).getParcelenglish());

                    }
                } else mAddressList.add(getString(R.string.s_no_data_found));

                openSelectionDialog(mAddressList, getString(R.string.s_house_building), AppConstants.Address.STREET);

            }
        });

    }

    private void hideAllErrorFields() {
        tvErrorArea.setVisibility(View.GONE);
        tvErrorBlockNo.setVisibility(View.GONE);
        tvErrorGovernorate.setVisibility(View.GONE);
        tvErrorStreet.setVisibility(View.GONE);
    }

    private void showStreetValidationError(int errorMessage) {
        shakeLayout(tvErrorStreet);
        tvErrorStreet.setText(errorMessage);
        tvErrorStreet.setVisibility(View.VISIBLE);
    }

    private void showBlockValidationError(int errorMessage) {
        shakeLayout(tvErrorBlockNo);
        tvErrorBlockNo.setText(errorMessage);
        tvErrorBlockNo.setVisibility(View.VISIBLE);
    }

    private void showAreaValidationError(int errorMessage) {
        shakeLayout(tvErrorArea);
        tvErrorArea.setText(errorMessage);
        tvErrorArea.setVisibility(View.VISIBLE);
    }

    private void showGovernorateValidationError(int errorMessage) {
        shakeLayout(tvErrorGovernorate);
        tvErrorGovernorate.setText(errorMessage);
        tvErrorGovernorate.setVisibility(View.VISIBLE);

    }

    /**
     * Opens address selection dialog, where user can select a particular address from the list
     *  @param addressList Address list to be passed to adapter
     * @param title       Title for which that particular list belongs to
     * @param head        Title for matching while selecting item
     */
    private void openSelectionDialog(TreeSet<String> addressList, CharSequence title, String head) {
        if (getActivity() != null) {

            if (mAdvanceSearchDialog == null) {
                mAdvanceSearchDialog = new Dialog(getActivity(), R.style.customDialog);
                mAdvanceSearchDialog.setContentView(R.layout.dialog_address);
                mAdvanceSearchDialog.setCanceledOnTouchOutside(true);
                mAdvanceSearchDialog.setCancelable(true);

                if (mAdvanceSearchDialog.getWindow() != null)
                    mAdvanceSearchDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                AppUtils.getInstance().dimDialogBackground(mAdvanceSearchDialog);
            }

            TextView tvTitle = mAdvanceSearchDialog.findViewById(R.id.tv_address_title);
            SearchView svArea = mAdvanceSearchDialog.findViewById(R.id.sv_area);

            EditText editText = svArea.findViewById(androidx.appcompat.R.id.search_src_text);
            editText.setHintTextColor(getResources().getColor(R.color.colorTextBlack));
            editText.setTextColor(getResources().getColor(R.color.colorTextBlack));
            editText.setTextSize(TEXT_SIZE);

            tvTitle.setText(title);
            svArea.setIconified(false);
            svArea.setQuery("", true);

           /* if(title.equals(getString(R.string.s_area)))
                svArea.setVisibility(View.VISIBLE);
            else  svArea.setVisibility(View.GONE);*/

            RecyclerView rvAddress = mAdvanceSearchDialog.findViewById(R.id.rv_address);

            AdvancedSearchAdapter advanceSearchAdapter = new AdvancedSearchAdapter(addressList, head, this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvAddress.setLayoutManager(linearLayoutManager);
            rvAddress.setHasFixedSize(true);
            rvAddress.setAdapter(advanceSearchAdapter);

            svArea.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    advanceSearchAdapter.getFilter().filter(s);
                    return false;
                }
            });

            mAdvanceSearchDialog.show();
        }
    }

    @Override
    protected void initViewsAndVariables() {
        mAddressList = new TreeSet<>();
        mGovernorateList = new ArrayList<>();
        mNeighbourhoodList = new ArrayList<>();
        mStreetsList = new ArrayList<>();
        mGeocodeList = new ArrayList<>();
        mLatLng = new ArrayList<>();

        toolbar.setVisibility(View.GONE);
        getArgumentsData();
        setUpListeners();
    }

    /**
     * Set up listeners for getting callbacks on click events
     */
    private void setUpListeners() {
        if (mResult == null || mNavigation.equals(RecipientDetailsFragment.class.getName()) || mNavigation.equals(ProfileFragment.class.getName())) {
            //tvGovernorate.setOnClickListener(this);
            tvArea.setOnClickListener(this);
            tvBlockNumber.setOnClickListener(this);
            tvStreet.setOnClickListener(this);
            // tvHouseBuilding.setOnClickListener(this);
        }
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            mNavigation = getArguments().getString(AppConstants.ACTION_NAVIGATION);
            mIsForPickup = getArguments().getBoolean(NetworkConstants.KEY_IS_FOR_PICKUP);

            if (getArguments().getParcelable(AppConstants.KEY_SAVED_ADDRESS_RESULT) != null) {
                mResult = getArguments().getParcelable(AppConstants.KEY_SAVED_ADDRESS_RESULT);
                showSavedAddressView(mResult);
            } else {
                showSaveAddressView();
            }
        }
    }

    /**
     * show views of advance search when user first time save address
     */
    private void showSaveAddressView() {
        if (mAdvancedSearchViewModel.getUserType() != null)
            fabSaveAddress.show();
        else
            fabSaveAddress.hide();
        toolbar.setVisibility(View.GONE);

        tvLabelGovernorate.setVisibility(View.GONE);
        tvGovernorate.setVisibility(View.GONE);
        viewGovernorate.setVisibility(View.GONE);

        tvGovernorate.setFocusable(false);
        tvArea.setFocusable(false);
        tvBlockNumber.setFocusable(false);
        tvStreet.setFocusable(false);
    }


    /**
     * show saved address data on the fields when user wants to edit it
     *
     * @param result saved address that already add as favourite and user wants to edit that
     */
    private void showSavedAddressView(Result result) {

        if (mNavigation.equals(MapFragment.class.getName()) || mNavigation.equals(AppConstants.Navigation.EDIT_ADDRESS_FROM_MAP)
                || mNavigation.equals(DropLocationsListFragment.class.getName())) {
            enterAddressManually();
            btnSubmit.setText(R.string.s_confirm_address);
            toolbar.setVisibility(View.VISIBLE);
            if (mIsForPickup)
                tvTitle.setText(R.string.s_pick_location);
            else tvTitle.setText(R.string.s_drop_location);
        } else {
            tvGovernorate.setFocusable(false);
            tvArea.setFocusable(false);
            tvBlockNumber.setFocusable(false);
            tvStreet.setFocusable(false);
        }

        if (mNavigation.equals(ProfileFragment.class.getName())) {
            fabSaveAddress.hide();
            toolbar.setVisibility(View.VISIBLE);
            tvTitle.setText(R.string.s_saved_address);
        }

        tvGovernorate.setText(result.getSavedLocation().getGovernorate());
        tvArea.setText(result.getSavedLocation().getArea());
        tvBlockNumber.setText(result.getSavedLocation().getBlockNumber());
        tvStreet.setText(result.getSavedLocation().getStreet());
        if (result.getSavedLocation().getAvenue() != null)
            tvAvenue.setText(result.getSavedLocation().getAvenue());
        if (result.getSavedLocation().getHouseOrbuilding() != null)
            tvHouseBuilding.setText(result.getSavedLocation().getHouseOrbuilding());
        if (result.getSavedLocation().getFloor() != null)
            tvFloor.setText(result.getSavedLocation().getFloor());
        if (result.getSavedLocation().getApartmentOrOffice() != null)
            tvFloor.setText(result.getSavedLocation().getApartmentOrOffice());

    }

    private void enterAddressManually() {
        tvLabelStreet.setText(R.string.s_street_optional);

        tvGovernorate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tvArea.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tvBlockNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tvStreet.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        tvHouseBuilding.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        tvGovernorate.setFocusable(true);
        tvArea.setFocusable(true);
        tvBlockNumber.setFocusable(true);
        tvStreet.setFocusable(true);
        tvHouseBuilding.setFocusable(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_submit, R.id.fab_save_address, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab_save_address:
                hideAllErrorFields();
                fabSaveAddress.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in));
                if (mAdvancedSearchViewModel.checkValidation(getCompleteAddress(), true)) {
                    if (((BitmapDrawable) fabSaveAddress.getDrawable()).getBitmap().sameAs(((BitmapDrawable) getResources().getDrawable(R.drawable.ic_save_address_inactive)).getBitmap()))
                        openSaveAddressDialog(true, null);
                    else

                        openSaveAddressDialog(false, mSavedLocationName);
                }
                break;
            case R.id.btn_submit:
                hideAllErrorFields();
                if (mResult != null && mAdvancedSearchViewModel.checkValidation(getCompleteAddress(), true)) {
                    if (mNavigation.equals(RecipientDetailsFragment.class.getName()) || mNavigation.equals(AppConstants.Navigation.EDIT_ADDRESS_FROM_MAP)) {
                        mHost.backToRecipientDetails(getCompleteAddress());
                    } else if (mNavigation.equals(MapFragment.class.getName())) {
                        mHost.navigateToCreateOrderScreen(mIsForPickup, getCompleteAddress());
                    } else if (mNavigation.equals(ProfileFragment.class.getName())) {
                        openSaveAddressDialog(true, null);
                    } else if (mNavigation.equals(DropLocationsListFragment.class.getName())) {
                        mHost.openAddItemFragment(getCompleteAddress());
                    }
                } else if (mAdvancedSearchViewModel.checkValidation(getCompleteAddress(), false)) {
                    if (mNavigation.equals(RecipientDetailsFragment.class.getName())) {
                        //mHost.backToRecipientDetails(getCompleteAddress());
                        mHost.openAddItemFragment(getCompleteAddress());
                    } else
                        mHost.navigateToCreateOrderScreen(mIsForPickup, getCompleteAddress());
                }

                break;
            case R.id.iv_back:
                mHost.onBackPressed();
        }
    }

    private void openSaveAddressDialog(boolean isAddressEditable, CharSequence savedLocationName) {
        if (getActivity() != null) {
            final Dialog saveAddressDialog = new Dialog(getActivity(), R.style.customDialog);
            saveAddressDialog.setContentView(R.layout.dialog_name_your_address);
            saveAddressDialog.setCanceledOnTouchOutside(true);
            saveAddressDialog.setCancelable(true);
            AppUtils.getInstance().dimDialogBackground(saveAddressDialog);

            final TextView tvTitle = saveAddressDialog.findViewById(R.id.tv_title);
            final EditText etAddressName = saveAddressDialog.findViewById(R.id.et_address_title);
            final TextView tvError = saveAddressDialog.findViewById(R.id.tv_error_title);
            Button btnCancel = saveAddressDialog.findViewById(R.id.btn_cancel);
            Button btnSave = saveAddressDialog.findViewById(R.id.btn_save);
            View viewAddress = saveAddressDialog.findViewById(R.id.view_address_name);


            if (mResult != null) {
                etAddressName.setText(mResult.getSavedLocation().getName());
                etAddressName.setSelection(etAddressName.getText().length());
            }

            if (!isAddressEditable) {
                etAddressName.setFocusable(false);
                btnCancel.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
                viewAddress.setVisibility(View.GONE);
                btnSave.setText(R.string.s_ok);
                tvTitle.setText(savedLocationName);
                etAddressName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_order_summary_drop_location, 0, 0, 0);
                etAddressName.setCompoundDrawablePadding(10);
                etAddressName.setText(getCompleteAddress().getFullAddress());
            }

            btnCancel.setOnClickListener(v -> saveAddressDialog.dismiss());

            btnSave.setOnClickListener(v -> {
                hideKeyboard();
                if (!etAddressName.getText().toString().trim().isEmpty()) {
                    tvError.setVisibility(View.GONE);
                    if (mResult != null && mNavigation.equals(ProfileFragment.class.getName())) {
                        mAdvancedSearchViewModel.onSaveAddress(getCompleteAddress(), mResult.getId(), etAddressName.getText().toString());
                    } else {
                        mAdvancedSearchViewModel.markAddressAsFavourite(getCompleteAddress(), etAddressName.getText().toString().trim());
                    }
                    saveAddressDialog.dismiss();
                } else {
                    tvError.setVisibility(View.VISIBLE);
                }
            });

            saveAddressDialog.show();
        }
    }


    private SaveAddressRequest getCompleteAddress() {
        SaveAddressRequest advanceSearchRequest = new SaveAddressRequest();
        advanceSearchRequest.setGovernorate(tvGovernorate.getText().toString().trim());
        advanceSearchRequest.setArea(tvArea.getText().toString().trim());
        advanceSearchRequest.setBlockNumber(tvBlockNumber.getText().toString().trim());
        advanceSearchRequest.setStreet(tvStreet.getText().toString().trim());
        advanceSearchRequest.setAvenue(tvAvenue.getText().toString().trim());
        advanceSearchRequest.setHouseOrbuilding(tvHouseBuilding.getText().toString().trim());
        advanceSearchRequest.setFloor(tvFloor.getText().toString().trim());
        advanceSearchRequest.setApartmentOrOffice(tvApartmentOffice.getText().toString().trim());

        if (mLatLng.size() > 0) {
            Geometry geometry = new Geometry();
            geometry.setType("point");
            geometry.setCoordinates(mLatLng);
            advanceSearchRequest.setGeometry(geometry);
        } else if (mResult != null && mResult.getSavedLocation().getGeometry() != null &&
                mResult.getSavedLocation().getGeometry().getCoordinates().size() > 0) {
            advanceSearchRequest.setGeometry(mResult.getSavedLocation().getGeometry());
        }


        if (mResult != null && mResult.getSavedLocation().getAddressName() != null)
            advanceSearchRequest.setFullAddress(mResult.getSavedLocation().getAddressName());
        else
            advanceSearchRequest.setFullAddress(
                    (!advanceSearchRequest.getApartmentOrOffice().equals("") ? advanceSearchRequest.getApartmentOrOffice() : "") +
                            (!advanceSearchRequest.getFloor().equals("") ? ", " + advanceSearchRequest.getFloor() : "") +
                            (!advanceSearchRequest.getAvenue().equals("") ? ", " + advanceSearchRequest.getAvenue() : "") +
                            (!advanceSearchRequest.getHouseOrbuilding().equals("") ? ", " + advanceSearchRequest.getHouseOrbuilding() : "") +
                            (!advanceSearchRequest.getStreet().equals("") ? ", " + advanceSearchRequest.getStreet() : "") +
                            ", " + advanceSearchRequest.getBlockNumber() +
                            ", " + advanceSearchRequest.getArea() +
                            ", " + advanceSearchRequest.getGovernorate());


        if (advanceSearchRequest.getFullAddress().startsWith(","))
            advanceSearchRequest.setFullAddress(advanceSearchRequest.getFullAddress().substring(1));
        return advanceSearchRequest;
    }

    /**
     * Specifies what to be done on selecting a particular item from the list
     *
     * @param item  Item selected by the user
     * @param title For which particular case, this item is selected
     */
    @Override
    public void onItemSelected(String item, String title) {
        hideAllErrorFields();
        fabSaveAddress.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_address_inactive));
        if (item.equals(getString(R.string.s_no_data_found))) {
            if (title.equals(AppConstants.Address.STREET)) {
                tvStreet.setFocusableInTouchMode(true);
                tvStreet.setFocusable(true);
            }
            mAdvanceSearchDialog.dismiss();
        } else {
            switch (title) {
                case AppConstants.Address.GOVERNORATE:
                    tvGovernorate.setText(item);
                    mGovNo = getSelectedGovernorate(item);
                    mAdvancedSearchViewModel.getNeighbourhood(mToken, mGovNo);
                    break;
                case AppConstants.Address.AREA:
                    tvArea.setText(item);
                    getGovernorateForSelectedArea(item);
                    mNhoodNo = getSelectedNeighbourhood(item);
                    mAdvancedSearchViewModel.getBlock(mToken, mNhoodNo);
                    break;
                case AppConstants.Address.BLOCK:
                    tvBlockNumber.setText(item);
                    mBlockId = (item.split(" "))[1];
                    mAdvancedSearchViewModel.getStreet(mToken, mBlockId, mNhoodNo);
                    tvStreet.setFocusable(false);
                    break;
                case AppConstants.Address.STREET:
                    tvStreet.setText(item);
                    mLatLng = getStreetLatLng(item);
                    //  mAdvancedSearchViewModel.getGeoCodeLocation(mToken, item);
                    mAdvanceSearchDialog.dismiss();
                    break;
                case AppConstants.Address.HOUSE:
                    tvHouseBuilding.setText(item);
                    mLatLng = getLocationLatLng(item);
                    mAdvanceSearchDialog.dismiss();
            }
            resetFields(title);
        }
    }

    private void getGovernorateForSelectedArea(String item) {
        for (int i = 0; i < mNeighbourhoodList.size(); i++) {
            if (LocaleManager.getLanguage(getContext()).equals("en") &&
                    item.equals(mNeighbourhoodList.get(i).getAttributes().getNeighborhoodenglish())) {
                tvLabelGovernorate.setVisibility(View.VISIBLE);
                tvGovernorate.setVisibility(View.VISIBLE);
                viewGovernorate.setVisibility(View.VISIBLE);
                tvGovernorate.setText(mNeighbourhoodList.get(i).getAttributes().getGovernorateenglish());
                break;
            } else if (LocaleManager.getLanguage(getContext()).equals("ar") &&
                    item.equals(mNeighbourhoodList.get(i).getAttributes().getNeighborhoodarabic())) {
                tvLabelGovernorate.setVisibility(View.VISIBLE);
                tvGovernorate.setVisibility(View.VISIBLE);
                viewGovernorate.setVisibility(View.VISIBLE);
                tvGovernorate.setText(mNeighbourhoodList.get(i).getAttributes().getGovernoratearabic());
                break;
            }
        }
    }

    /**
     * Resets fields based on hierarchy, if any of the the field is changed then all the fields that are below
     * that one in hierarchy should be reset.
     *
     * @param title Field that is changed, corresponding to which other fields have to be resettled
     */
    private void resetFields(String title) {
        switch (title) {
            case AppConstants.Address.GOVERNORATE:
                tvArea.setText("");
            case AppConstants.Address.AREA:
                tvBlockNumber.setText("");
            case AppConstants.Address.BLOCK:
                tvStreet.setText("");
            case AppConstants.Address.STREET:
                tvHouseBuilding.setText("");
                tvAvenue.setText("");
                tvFloor.setText("");
                tvApartmentOffice.setText("");
        }
    }

    /**
     * Returns precise LatLng corresponding to house/building user has selected
     *
     * @param item Street selected by the user
     * @return Returns LatLng corresponding to house/building selected
     */
    private List<Double> getLocationLatLng(String item) {
        List<Double> latLng = new ArrayList<>();
        for (int i = 0; i < mGeocodeList.size(); i++) {
            if (item.equals(" House " + mGeocodeList.get(i).getHouseenglish() + " Parcel " + mGeocodeList.get(i).getParcelenglish()) ||
                    item.equals("Parcel " + mGeocodeList.get(i).getParcelenglish())) {
                latLng.add(mGeocodeList.get(i).getLon());
                latLng.add(mGeocodeList.get(i).getLat());
                return latLng;
            }
        }
        return latLng;
    }

    /**
     * Returns street LatLng to be used in case we didn't get any from {@link #getLocationLatLng(String)}
     *
     * @param item Street selected by the user
     * @return Returns LatLng corresponding to street selected
     */
    private List<Double> getStreetLatLng(String item) {
        List<Double> latLng = new ArrayList<>();
        for (int i = 0; i < mStreetsList.size(); i++) {
            if (LocaleManager.getLanguage(getContext()).equals("en") &&
                    item.equals(mStreetsList.get(i).getAttributes().getStreetenglish())) {
                latLng.add(Double.valueOf((mStreetsList.get(i).getAttributes().getLocation()).split(",")[1]));
                latLng.add(Double.valueOf((mStreetsList.get(i).getAttributes().getLocation()).split(",")[0]));
            } else if (LocaleManager.getLanguage(getContext()).equals("ar") &&
                    item.equals(mStreetsList.get(i).getAttributes().getStreetarabic())) {
                latLng.add(Double.valueOf((mStreetsList.get(i).getAttributes().getLocation()).split(",")[1]));
                latLng.add(Double.valueOf((mStreetsList.get(i).getAttributes().getLocation()).split(",")[0]));
            }
        }
        return latLng;
    }

    /**
     * Returns {@link #mGovNo}  for the selected governorate to fetch the neighbourhoods associated with that
     * particular governorate.
     *
     * @param item Governorate selected by the user
     * @return govNo of selected governorate
     */
    private int getSelectedNeighbourhood(String item) {
        for (int i = 0; i < mNeighbourhoodList.size(); i++) {
            if (LocaleManager.getLanguage(getContext()).equals("en") &&
                    mNeighbourhoodList.get(i).getAttributes().getNeighborhoodenglish().equals(item))
                return mNeighbourhoodList.get(i).getAttributes().getNhoodNo();
            else if (LocaleManager.getLanguage(getContext()).equals("ar") &&
                    mNeighbourhoodList.get(i).getAttributes().getNeighborhoodarabic().equals(item))
                return mNeighbourhoodList.get(i).getAttributes().getNhoodNo();
        }
        return -1;
    }


    /**
     * Return {@link #mNhoodNo} for the selected neighbourhood to fetch all the associated blocks
     * with that particular selected neighbourhood
     *
     * @param item Neighbourhood selected by the user
     * @return nHoodNo of selected Neighbourhood
     */
    private int getSelectedGovernorate(String item) {
        for (int i = 0; i < mGovernorateList.size(); i++) {
            if (LocaleManager.getLanguage(getContext()).equals("en") &&
                    mGovernorateList.get(i).getAttributes().getGovernorateenglish().equals(item))
                return mGovernorateList.get(i).getAttributes().getGovNo();
            else if (LocaleManager.getLanguage(getContext()).equals("ar") &&
                    mGovernorateList.get(i).getAttributes().getGovernoratearabic().equals(item))
                return mGovernorateList.get(i).getAttributes().getGovNo();
        }
        return -1;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_governorate:
                if (mToken != null)
                    mAdvancedSearchViewModel.getGovernorate(mToken);
                else
                    mAdvancedSearchViewModel.getTokenForKuwaitFinder();
                break;
            case R.id.tv_area:
                if (mToken != null) {
                    mAdvancedSearchViewModel.getNeighbourhood(mToken, mGovNo);
                } else {
                    showProgressDialog();
                    mAdvancedSearchViewModel.getTokenForKuwaitFinder();
                }
                break;
            case R.id.tv_block_number:
                if (mNhoodNo != 0) {
                    mAdvancedSearchViewModel.getBlock(mToken, mNhoodNo);
                } else showSnackBar(getString(R.string.s_select_area_first));
                break;
            case R.id.tv_street:
                if (mBlockId != null && mNhoodNo != 0) {
                    mAdvancedSearchViewModel.getStreet(mToken, mBlockId, mNhoodNo);
                } else showSnackBar(getString(R.string.s_select_block_number_first));
                break;
            case R.id.tv_house_building:
                /*if (mStreet != null) {
                    mAdvancedSearchViewModel.getGeoCodeLocation(mToken, mStreet);
                } else showSnackBar(getString(R.string.s_select_street_first));*/
                break;
        }
    }


    public interface IAdvancedSearchHost {
        void onBackPressed();

        void navigateToCreateOrderScreen(boolean isForPickup, SaveAddressRequest completeAddress);

        void updateSavedAddress(Result result);

        void backToProfileFragment(Result result);

        void backToRecipientDetails(SaveAddressRequest completeAddress);

        void openAddItemFragment(SaveAddressRequest createOrderRequest);
    }

}
