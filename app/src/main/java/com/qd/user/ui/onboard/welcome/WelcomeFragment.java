package com.qd.user.ui.onboard.welcome;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.socialresponse.FbLoginResponse;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.LocaleManager;
import com.qd.user.utils.customviews.ScrollingImageView;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WelcomeFragment extends BaseFragment {
    private static final int SUCCESS_CODE = 200;
    private static final int FB_SUCCESS_CODE = 250;
    @BindView(R.id.fl_image_container)
    ConstraintLayout flImageContainer;
    @BindView(R.id.ll_english)
    LinearLayout llEnglish;
    @BindView(R.id.tv_arabic)
    TextView tvArabic;
    @BindView(R.id.ll_Arabic)
    LinearLayout llArabic;
    @BindView(R.id.btn_create_account)
    Button btnCreateAccount;
    @BindView(R.id.btn_others)
    Button btnOthers;
    private Unbinder unbinder;
    @BindView(R.id.tv_english)
    TextView tvEnglish;
    @BindView(R.id.iv_vehicle)
    ImageView ivVehicle;
    @BindView(R.id.tv_continue_as_guest)
    TextView tvContinueAsGuest;
    /* @BindView(R.id.iv_bg)
     ImageView ivBg;*/
    @BindView(R.id.scrolling_background)
    ScrollingImageView scrollingBackground;

    private IWelcomeHost mHost;
    private WelcomeViewModel mWelcomeViewModel;
    private GoogleSignInClient mGoogleSignInClient;
    private FbLoginResponse mFbLoginResponse;
    private GoogleSignInAccount mGoogleSignInAccount;
    private CreateOrderRequest mCreateOrderDetails;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment_order_status
        mHost.exitFullScreenMode();
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWelcomeViewModel = new ViewModelProvider(this).get(WelcomeViewModel.class);
        mWelcomeViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        getDeviceId();
        mWelcomeViewModel.initializeFbLogin();
        initializeGmailLogin();
        getArgumentsData();

        getSelectedLanguage();
    }

    private void getSelectedLanguage() {
        Log.e("Language", LocaleManager.getLanguage(getContext()));
        if (LocaleManager.getLanguage(getContext()).equals("en")) {
            selectEnglish();
        } else selectArabic();
    }

    @Override
    public void onResume() {
        super.onResume();
        animateVehicle();
    }

    @Override
    public void onStop() {
        super.onStop();
        scrollingBackground.stop();
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.CREATE_ORDER_DETAILS)) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
        }
    }

    private void initializeGmailLogin() {
        if (getActivity() != null) {
           /* GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(AppConstants.SERVER_CLIENT_ID)
                    .requestEmail().build();
            mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);*/
            mGoogleSignInClient = com.qd.user.utils.GoogleSignIn.getGoogleSignInClient(getActivity());
        }
    }

    private void observeLiveData() {
        mWelcomeViewModel.getFacebookLoginLiveData().observe(getViewLifecycleOwner(), fbLoginResponse -> {
            if (fbLoginResponse != null) {
                mFbLoginResponse = fbLoginResponse;
                showProgressDialog();
                mWelcomeViewModel.signUpThroughFacebookOrGoogle(getAccountInfo());

            }

        });
        mWelcomeViewModel.getSocialLoginLiveData().observe(getViewLifecycleOwner(), socialLoginResponse -> {
            hideProgressDialog();
            if (socialLoginResponse != null) {
                switch (socialLoginResponse.getStatusCode()) {
                    case SUCCESS_CODE:
                        //if user already filled create order details
                        if (mCreateOrderDetails != null) {
                            mHost.onBackPressed();
                        } else {
                            if (socialLoginResponse.getResult().getUserDetails().isOtpVerified()) {
                                mWelcomeViewModel.setIsUserVerified();
                                mHost.openHomeScreen();
                            } else
                                openOtpSentDialog();
                        }
                        break;
                    case FB_SUCCESS_CODE:
                        if (mFbLoginResponse != null) {
                            mHost.steerToCreateAccountScreen(mFbLoginResponse, null, mCreateOrderDetails);
                        } else if (mGoogleSignInAccount != null) {
                            mHost.steerToCreateAccountScreen(null, mGoogleSignInAccount, mCreateOrderDetails);
                        }
                        break;


                }

            }

        });

    }

    private void openOtpSentDialog() {
        if (getActivity() != null) {
            final Dialog otpSentDialog = new Dialog(getActivity(), R.style.customDialog);
            otpSentDialog.setContentView(R.layout.dialog_forgot_password);
            AppUtils.getInstance().dimDialogBackground(otpSentDialog);
            Button btnOk = otpSentDialog.findViewById(R.id.btn_ok);
            ImageView ivCodeSend = otpSentDialog.findViewById(R.id.iv_resent_link);
            TextView tvMessage = otpSentDialog.findViewById(R.id.tv_message);
            tvMessage.setText(R.string.s_verification_code_send_to_your_phone_number);
            ivCodeSend.setImageDrawable(getResources().getDrawable(R.drawable.ic_login_code_sent));
            otpSentDialog.show();

            btnOk.setOnClickListener(v -> {
                otpSentDialog.dismiss();
                mHost.openVerifyOtpScreen(null);
            });
        }

    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {

    }

    private CreateAccountInfo getAccountInfo() {
        CreateAccountInfo createAccountInfo = new CreateAccountInfo();
        //if user choose facebook as sign up type
        if (mFbLoginResponse != null) {
            createAccountInfo.setFirstName(mFbLoginResponse.getFirstName());
            createAccountInfo.setLastName(mFbLoginResponse.getLastName());
            createAccountInfo.setEmail(mFbLoginResponse.getEmailId());
            createAccountInfo.setPhoneNumber(mFbLoginResponse.getPhoneNumber());
            createAccountInfo.setAppId(mFbLoginResponse.getFbId());
            createAccountInfo.setPlatform(AppConstants.FACEBOOK);
        }
        //if user choose mail as sign up type
        else if (mGoogleSignInAccount != null) {
            createAccountInfo.setFirstName(mGoogleSignInAccount.getGivenName());
            createAccountInfo.setLastName(mGoogleSignInAccount.getFamilyName());
            createAccountInfo.setEmail(mGoogleSignInAccount.getEmail());
            createAccountInfo.setAppId(mGoogleSignInAccount.getId());
            createAccountInfo.setPlatform(AppConstants.GMAIL);
        }
        createAccountInfo.setDeviceId(getDeviceId());

        return createAccountInfo;

    }

    public static WelcomeFragment getInstance(Parcelable parcelable) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, parcelable);
        WelcomeFragment welcomeFragment = new WelcomeFragment();
        welcomeFragment.setArguments(bundle);
        return welcomeFragment;
    }

    @Override
    protected void initViewsAndVariables() {
        //setSpannableStringForLogIn();
    }

    private void animateVehicle() {
        scrollingBackground.start();
    }


    /* private void setSpannableStringForLogIn() {
         Spannable spannableString = new SpannableString(getString(R.string.s_already_a_member_login));
         spannableString.setSpan(new StyleSpan(Typeface.BOLD), 18, 23, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

         ClickableSpan clickableSpan = new ClickableSpan() {
             @Override
             public void onClick(View textView) {
                 textView.setBackground(null);
                 mHost.steerToLogInFragment(mCreateOrderDetails);
             }

             @Override
             public void updateDrawState(TextPaint ds) {
                 ds.setUnderlineText(false);
             }
         };
         spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(btnLogin.getContext(), R.color.colorGreenishTeal)), 18, 23, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
         spannableString.setSpan(clickableSpan, 18, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
         btnLogin.setText(spannableString);
         btnLogin.setHighlightColor(Color.TRANSPARENT);
         btnLogin.setMovementMethod(LinkMovementMethod.getInstance());
     }
 */
    @OnClick({R.id.ll_english, R.id.ll_Arabic, R.id.btn_create_account, R.id.btn_others, R.id.tv_continue_as_guest, R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_english:
                mHost.setLocale("en");
                selectEnglish();
                break;
            case R.id.ll_Arabic:
                mHost.setLocale("ar");
                selectArabic();
                break;
            case R.id.btn_create_account:
                mHost.steerToCreateAccountScreen(null, null, mCreateOrderDetails);
                break;
            case R.id.btn_others:
                openDialogForSocialLogin();
                break;
            case R.id.tv_continue_as_guest:
                mHost.moveToHomeScreen();
                break;
            case R.id.btn_login:
                mHost.steerToLogInFragment(mCreateOrderDetails);
                break;
        }
    }

    private void selectArabic() {
        llArabic.setBackground(getResources().getDrawable(R.drawable.drawable_green_round_stroke));
        tvArabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_welcome_language_select, 0);
        llEnglish.setBackground(getResources().getDrawable(R.drawable.drawable_grey_round_cornered_stroke));
        tvEnglish.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

    }

    private void selectEnglish() {
        llEnglish.setBackground(getResources().getDrawable(R.drawable.drawable_green_round_stroke));
        tvEnglish.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_welcome_language_select, 0);
        llArabic.setBackground(getResources().getDrawable(R.drawable.drawable_grey_round_cornered_stroke));
        tvArabic.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

    }


    /**
     * open dialog for social login options
     */
    private void openDialogForSocialLogin() {
        if (getActivity() != null) {
            final Dialog socialLoginDialog = new Dialog(getActivity(), R.style.customDialog);
            socialLoginDialog.setContentView(R.layout.dialog_social_logins);
            AppUtils.getInstance().dimDialogBackground(socialLoginDialog);
            ConstraintLayout clFacebookLogin = socialLoginDialog.findViewById(R.id.cl_facebook);
            ConstraintLayout clnGoogleLogin = socialLoginDialog.findViewById(R.id.cl_google);
            socialLoginDialog.show();

            clFacebookLogin.setOnClickListener(v -> {
                socialLoginDialog.dismiss();
                doFbLogin();

            });
            clnGoogleLogin.setOnClickListener(v -> {
                socialLoginDialog.dismiss();
                googleSignIn();
            });
        }
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, AppConstants.RC_SIGN_IN);
    }

    private void doFbLogin() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.RC_SIGN_IN) {
            {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleGoogleSignInResult(task);
            }
        } else
            mWelcomeViewModel.getCallBackManager().onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IWelcomeHost) {
            mHost = (IWelcomeHost) context;

        } else
            throw new IllegalStateException("Host  must implement IWelcomeHost");
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            mGoogleSignInAccount = completedTask.getResult(ApiException.class);
            showProgressDialog();
            mWelcomeViewModel.signUpThroughFacebookOrGoogle(getAccountInfo());

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * This interface is used to interact with the host
     */
    public interface IWelcomeHost {

        void steerToLogInFragment(CreateOrderRequest mCreateOrderDetails);

        void exitFullScreenMode();

        void moveToHomeScreen();

        void steerToCreateAccountScreen(FbLoginResponse fbLoginResponse, GoogleSignInAccount googleSignInAccount, CreateOrderRequest mCreateOrderDetails);

        void openHomeScreen();

        void onBackPressed();

        void openVerifyOtpScreen(String phoneNumber);

        void setLocale(String locale);
    }


}
