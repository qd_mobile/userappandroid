package com.qd.user.ui.home.contactus;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.CustomEditText;
import com.qd.user.utils.EmptyStateTextInputLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ContactUsFragment extends BaseFragment implements AppUtils.ISuccess {

    @BindView(R.id.iv_email)
    ImageView ivEmail;
    @BindView(R.id.tv_label_email)
    TextView tvLabelEmail;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.iv_contact)
    ImageView ivContact;
    @BindView(R.id.tv_label_contact)
    TextView tvLabelContact;
    @BindView(R.id.tv_contact)
    TextView tvContact;
    @BindView(R.id.iv_website)
    ImageView ivWebsite;
    @BindView(R.id.tv_label_website)
    TextView tvLabelWebsite;
    @BindView(R.id.tv_website)
    TextView tvWebsite;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.tv_name)
    EmptyStateTextInputLayout tvName;
    @BindView(R.id.view_name)
    View viewName;
    @BindView(R.id.tv_error_name)
    TextView tvErrorName;
    @BindView(R.id.tv_label_enquiry_type)
    TextView tvLabelEnquiryType;
    @BindView(R.id.view_enquiry_type)
    View viewEnquiryType;
    @BindView(R.id.tv_label_message)
    TextView tvLabelMessage;
    @BindView(R.id.et_message)
    CustomEditText etMessage;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.spinner_enquiry_type)
    Spinner spinnerEnquiryType;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.et_country_code)
    EditText etCountryCode;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.tv_phone_number)
    TextInputLayout tvPhoneNumber;
    @BindView(R.id.view_country_code)
    View viewCountryCode;
    @BindView(R.id.view_phone_number)
    View viewPhoneNumber;
    @BindView(R.id.tv_error_phone_number)
    TextView tvErrorPhoneNumber;
    @BindView(R.id.cl_phone_email)
    ConstraintLayout clPhoneEmail;

    private Unbinder unbinder;


    /**
     * A {@link IContactUsHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IContactUsHost mContactUsHost;

    /**
     * A {@link ContactUsViewModel} object to handle all the actions and business logic of orders listing.
     */
    private ContactUsViewModel mContactUsViewModel;

    public static ContactUsFragment getInstance() {
        return new ContactUsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IContactUsHost) {
            mContactUsHost = (IContactUsHost) context;
        } else
            throw new IllegalStateException("host must implement IContactUsHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mContactUsViewModel = new ViewModelProvider(this).get(ContactUsViewModel.class);
        mContactUsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        observeLiveData();

        super.onViewCreated(view, savedInstanceState);
    }

    private void observeLiveData() {
        mContactUsViewModel.getContactUsLiveData().observe(getViewLifecycleOwner(), contactUsResponse -> {
            if (contactUsResponse != null) {
                openConfirmationDialog();
            }
        });


        mContactUsViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_EMAIL:
                        showEmailValidationError(validationErrors.getValidationMessage());
                        focusEmailInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER:
                        showPhoneNumberValidationError(validationErrors.getValidationMessage());
                        focusPhoneNumberInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_USERNAME:
                        showNameValidationError(validationErrors.getValidationMessage());
                        focusNameInputField();
                        break;
                    default:
                        showSnackBar(validationErrors.getValidationMessage());
                }
            }

        });
    }

    //focus last name field
    private void focusNameInputField() {
        etUsername.setSelection(etUsername.getText().toString().length());
        etUsername.requestFocus();
    }

    //show error message of invalid or empty last name
    private void showNameValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorName);
        tvErrorName.setText(errorMessage);
        tvErrorName.setVisibility(View.VISIBLE);
    }

    //show error message of invalid or empty email address
    private void showEmailValidationError(int message) {
        AppUtils.getInstance().shakeLayout(tvErrorEmail);
        tvErrorEmail.setText(message);
        tvErrorEmail.setVisibility(View.VISIBLE);
    }

    //focus email address field
    private void focusEmailInputField() {
        etEmail.setSelection(etEmail.getText().toString().length());
        etEmail.requestFocus();
    }

    private void focusPhoneNumberInputField() {
        etPhoneNumber.setSelection(etPhoneNumber.getText().toString().length());
        etPhoneNumber.requestFocus();
    }

    private void showPhoneNumberValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorPhoneNumber);
        tvErrorPhoneNumber.setText(errorMessage);
        tvErrorPhoneNumber.setVisibility(View.VISIBLE);
    }

    private void hideErrorFields() {
        tvErrorEmail.setVisibility(View.GONE);
        tvErrorPhoneNumber.setVisibility(View.GONE);
        tvErrorName.setVisibility(View.GONE);
    }

    private void openConfirmationDialog() {
        if (getActivity() != null) {

            AppUtils.getInstance().openSuccessDialog(getActivity(), getString(R.string.s_someone_from_qd_will_get_back_to_you_shortly), this);
        }
    }

    @Override
    protected void initViewsAndVariables() {
        //mContactUsHost.setDrawerLocked(false);
        tvTitle.setText(R.string.s_contact_us);

        tvContact.setText(AppConstants.ContactUs.CONTACT);
        tvEmail.setText(AppConstants.ContactUs.EMAIL);
        tvWebsite.setText(AppConstants.ContactUs.WEBSITE);

        if (mContactUsViewModel.getUsername() != null) {
            etUsername.setText(mContactUsViewModel.getUsername());
            etUsername.setEnabled(false);
        } else {
            clPhoneEmail.setVisibility(View.VISIBLE);
        }

        etMessage.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.et_message) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                    view.performClick();
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        });

        setSpinnerAdapter();
    }

    private void setSpinnerAdapter() {
        SpinnerAdapter adapter = new ArrayAdapter<>(spinnerEnquiryType.getContext(), R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.enquiry_types));
        spinnerEnquiryType.setAdapter(adapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_send, R.id.iv_contact, R.id.tv_contact, R.id.iv_email, R.id.tv_label_email,
            R.id.tv_email, R.id.iv_website, R.id.tv_label_website, R.id.tv_website})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mContactUsHost.onBackPressed();
                break;
            case R.id.btn_send:
                hideErrorFields();
                mContactUsViewModel.hitContactUs(etUsername.getText().toString().trim(),
                        spinnerEnquiryType.getSelectedItem(),
                        etMessage.getText() != null ? etMessage.getText().toString() : "", etEmail.getText().toString().toLowerCase().trim(),
                        etPhoneNumber.getText().toString());
                break;
            case R.id.iv_contact:
            case R.id.tv_contact:
                callToConnect();
                break;
            case R.id.iv_email:
            case R.id.tv_email:
                steerToGmail();
                break;
            case R.id.iv_website:
            case R.id.tv_website:
                navigateToBrowser();
                break;

        }
    }

    private void navigateToBrowser() {
        String urlString = "https://www.quickdeliveryco.com";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            intent.setPackage(null);
            startActivity(intent);
        }
    }

    /**
     * Opens gmail to mail query to support
     */
    private void steerToGmail() {
        if (isGooglePlayServicesAvailable()) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);

            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@quickdeliveryco.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Quick Delivery");
            emailIntent.setType("text/plain");

           /* try {
                PackageInfo pInfo;
                if (getActivity() != null && getActivity().getPackageManager() != null) {
                    pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Version no: " + pInfo.versionName + " \n"
                            + "Device info: " + Build.MODEL + "\t" + Build.DEVICE);

                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
*/

            if (getActivity() != null) {
                final List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(emailIntent, 0);
                ResolveInfo best = null;
                for (final ResolveInfo info : matches)
                    if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                        best = info;
                if (best != null)
                    emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                getActivity().startActivity(emailIntent);
            }

        }
    }

    /**
     * check for availability of Google play services
     *
     * @return true, if available
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(getContext());
        return resultCode == ConnectionResult.SUCCESS;
    }

    private void callToConnect() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+965 224 111 44", null));
        startActivity(intent);
    }

    @Override
    public void onOkViewClicked() {
        mContactUsHost.onBackPressed();
    }


    public interface IContactUsHost {
        void onBackPressed();

    }
}
