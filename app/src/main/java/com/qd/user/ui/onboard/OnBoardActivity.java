package com.qd.user.ui.onboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.qd.user.QuickDelivery;
import com.qd.user.R;
import com.qd.user.base.BaseActivity;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.response.Result;
import com.qd.user.model.socialresponse.FbLoginResponse;
import com.qd.user.ui.approveReturn.ApproveReturnFragment;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.ui.home.settings.StaticPageFragment;
import com.qd.user.ui.onboard.createaccount.CreateAccountFragment;
import com.qd.user.ui.onboard.forgotpassword.ForgotPasswordFragment;
import com.qd.user.ui.onboard.login.LoginFragment;
import com.qd.user.ui.onboard.resetpassword.ResetPasswordFragment;
import com.qd.user.ui.onboard.splash.SplashFragment;
import com.qd.user.ui.onboard.verifyotp.VerifyOtpFragment;
import com.qd.user.ui.onboard.welcome.WelcomeFragment;
import com.qd.user.ui.trackorder.TrackOrderActivity;
import com.qd.user.utils.GoogleSignIn;
import com.qd.user.utils.LocaleManager;
import com.qd.user.utils.notifications.HandlePushNotifications;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

import static com.qd.user.ui.home.HomeActivity.mNotificationCount;

public class OnBoardActivity extends BaseActivity implements SplashFragment.ISplashHost, LoginFragment.ILoginHost,
        VerifyOtpFragment.IVerifyOtpHost, WelcomeFragment.IWelcomeHost, CreateAccountFragment.ICreateAccountHost, ForgotPasswordFragment.IForgotPasswordHost,
        ResetPasswordFragment.IResetPasswordHost, StaticPageFragment.IStaticPageHost, ApproveReturnFragment.IApproveReturnHost {

    @BindView(R.id.cl_root_view)
    ConstraintLayout clOnBoard;

    @Override
    protected void initViewsAndVariables() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntentData();

    }

    //get data from intent and show screen accordingly
    private void getIntentData() {
        if (getIntent() != null && (getIntent().hasExtra("notification_data") || getIntent().hasExtra("data"))) {
            getNotificationDataFromBackground();
        } else if (getIntent() != null && getIntent().hasExtra(AppConstants.ACTION_NAVIGATION)) {
            switch (getIntent().getStringExtra(AppConstants.ACTION_NAVIGATION)) {
                case AppConstants.ACTION_LOGIN:
                    openWelcomeFragment(null);
                    steerToLogInFragment(null);
                    break;
                case AppConstants.ACTION_WELCOME:
                    if (getIntent().hasExtra(AppConstants.CREATE_ORDER_DETAILS) && getIntent().getParcelableExtra(AppConstants.CREATE_ORDER_DETAILS) != null) {
                        openWelcomeFragment(getIntent().getParcelableExtra(AppConstants.CREATE_ORDER_DETAILS));
                    } else {
                        LoginManager.getInstance().logOut();
                        GoogleSignIn.getGoogleSignInClient(this).signOut()
                                .addOnCompleteListener(this, task -> openWelcomeFragment(null));
                    }
                    break;
                case AppConstants.Navigation.APPROVE_RETURN:
                    openApproveReturnFragment(getIntent().getParcelableExtra(AppConstants.ORDER_DATA));
            }
        } else if (getSupportFragmentManager().findFragmentByTag(WelcomeFragment.class.getName()) == null)
            showSplashScreen();
    }


    @Override
    protected void onResume() {
        super.onResume();
        QuickDelivery.getInstance().setCurrentActivity(this);
    }

    private void clearReferences() {
        Activity currActivity = QuickDelivery.getInstance().getCurrentActivity();
        if (this.equals(currActivity))
            QuickDelivery.getInstance().setCurrentActivity(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearReferences();
    }

    @Override
    protected void onPause() {
        super.onPause();
        clearReferences();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("notification_data"))
            getNotificationDataFromBackground();
    }

    private void getNotificationDataFromBackground() {
        JSONObject jsonObject;
        String type;
        Intent resultantIntent;

        try {
            if (getIntent().getExtras() != null) {

                if (getIntent().getExtras().getString("notification_data") == null)
                    jsonObject = new JSONObject(getIntent().getExtras().getString("data"));
                else
                    jsonObject = new JSONObject(getIntent().getExtras().getString("notification_data"));
                type = jsonObject.getString("type");

                mNotificationCount++;
                switch (type) {
                    case AppConstants.NotificationType.ORDER_ACCEPTED_BY_DRIVER:
                        resultantIntent = new Intent(this, TrackOrderActivity.class);
                        resultantIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        resultantIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        resultantIntent.putExtra(AppConstants.ORDER_ID, ((Result) HandlePushNotifications.getInstance().parseNotificationData(jsonObject)).getId());
                        resultantIntent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.TRACK_ORDER);
                        startActivity(resultantIntent);
                        break;
                    case AppConstants.NotificationType.ORDER_STATUS_RECEIVER:
                        resultantIntent = new Intent(this, TrackOrderActivity.class);
                        resultantIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        resultantIntent.putExtra(AppConstants.ORDER_ID, ((Result) HandlePushNotifications.getInstance().parseNotificationData(jsonObject)).getId());
                        resultantIntent.putExtra(AppConstants.IS_ORDER_RECEIVED, true);
                        resultantIntent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.ORDER_DETAIL);
                        startActivity(resultantIntent);
                        break;
                    case AppConstants.NotificationType.ORDER_STATUS_SENDER:
                        resultantIntent = new Intent(this, TrackOrderActivity.class);
                        resultantIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        resultantIntent.putExtra(AppConstants.ORDER_ID, ((Result) HandlePushNotifications.getInstance().parseNotificationData(jsonObject)).getId());
                        resultantIntent.putExtra(AppConstants.ACTION_NAVIGATION,
                                ((Result) HandlePushNotifications.getInstance().parseNotificationData(jsonObject)).getOrderType().equals(AppConstants.OrderType.BUSINESS) ?
                                        AppConstants.Navigation.BUSINESS_ORDER_DETAIL : AppConstants.Navigation.ORDER_DETAIL);
                        startActivity(resultantIntent);
                        break;
                    case AppConstants.NotificationType.ISSUE_REPORTED_BY_DRIVER:
                        openApproveReturnFragment((Parcelable) HandlePushNotifications.getInstance().parseNotificationData(jsonObject));
                        break;

                    case AppConstants.NotificationType.PROMOTIONAL_NOTIFICATION:
                        resultantIntent = new Intent(this, HomeActivity.class);
                        resultantIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        resultantIntent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.NOTIFICATION);
                        startActivity(resultantIntent);

                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openApproveReturnFragment(Parcelable notificationData) {
        addFragment(R.id.cl_root_view, ApproveReturnFragment.getInstance(notificationData), ApproveReturnFragment.class.getName());
    }


    @Override
    public void openHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_DASHBOARD);
        startActivity(intent);
        finishAfterTransition();
    }

    @Override
    public void steerToForgotPasswordScreen() {
        addFragmentWithBackStack(R.id.cl_root_view, ForgotPasswordFragment.getInstance(), ForgotPasswordFragment.class.getName());
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_on_board;
    }

    @Override
    public void enterFullScreenMode() {
        if (getWindow() != null) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    public void exitFullScreenMode() {
        //Here activity will exit the full screen mode after the splash view
        if (getWindow() != null) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }

    @Override
    public void moveToHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_DASHBOARD);
        startActivity(intent);
    }


    @Override
    public void openWelcomeFragment(Parcelable parcelable) {
        replaceFragment(R.id.cl_root_view, WelcomeFragment.getInstance(parcelable), WelcomeFragment.class.getName());
    }


    private void showSplashScreen() {
        addFragment(R.id.cl_root_view, new SplashFragment(), SplashFragment.class.getName());
    }


    @Override
    public void steerToCreateAccountScreen(FbLoginResponse fbLoginResponse, GoogleSignInAccount googleSignInAccount, CreateOrderRequest mCreateOrderDetails) {
        addFragmentWithBackStack(R.id.cl_root_view, CreateAccountFragment.getInstance(fbLoginResponse, googleSignInAccount, mCreateOrderDetails), CreateAccountFragment.class.getName());
    }


    @Override
    public void steerToLogInFragment(CreateOrderRequest mCreateOrderDetails) {
        addFragmentWithBackStack(R.id.cl_root_view, LoginFragment.getInstance(mCreateOrderDetails), LoginFragment.class.getName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && data != null) {

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
            if (fragment instanceof WelcomeFragment)
                fragment.onActivityResult(requestCode, resultCode, data);

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void openVerifyOtpScreen(String phoneNumber) {
        addFragment(R.id.cl_root_view, VerifyOtpFragment.getInstance(phoneNumber), VerifyOtpFragment.class.getName());
    }

    @Override
    public void showStaticPage(String string) {
        addFragmentWithBackStack(R.id.cl_root_view, StaticPageFragment.getInstance(string), StaticPageFragment.class.getName());
    }


    @Override
    public void setLocale(String localeName) {
        LocaleManager.persist(this, localeName);
        recreate();
    }


    @Override
    public void steerToResetPasswordScreen(String resetToken) {
        addFragmentWithBackStack(R.id.cl_root_view, ResetPasswordFragment.getInstance(resetToken), ResetPasswordFragment.class.getName());

    }

    @Override
    public void
    onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof ApproveReturnFragment) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_DASHBOARD);
            startActivity(intent);
        }
    }

    @Override
    public void backToOrderSummary() {
        finishAfterTransition();
    }

}
