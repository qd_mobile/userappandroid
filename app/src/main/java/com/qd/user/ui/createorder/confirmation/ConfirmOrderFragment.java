package com.qd.user.ui.createorder.confirmation;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.location.Route;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.utils.maps.DirectionsApi;
import com.qd.user.utils.maps.IDirectionsParserInterface;
import com.qd.user.utils.maps.ParserTask;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.qd.user.ui.createorder.CreateOrderFragment.ZOOM_ANIMATE_DURATION;
import static com.qd.user.ui.createorder.CreateOrderFragment.ZOOM_LEVEL;

public class ConfirmOrderFragment extends BaseFragment implements OnMapReadyCallback, IDirectionsParserInterface {
    private static final float MARKER_POSITIONING_DIMEN = 0.5f;
    private static final int MAP_BOUND_PADDING = 250;
    private static final int MAP_BOTTOM_PADDING = 350;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.tv_label_delivery_charges)
    TextView tvLabelDeliveryCharges;
    @BindView(R.id.tv_label_eta)
    TextView tvLabelEta;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.tv_eta)
    TextView tvEta;
    @BindView(R.id.btn_proceed)
    Button btnProceed;
    @BindView(R.id.fl_map_container)
    FrameLayout flMapContainer;
    private Unbinder unbinder;
    private IConfirmOrderHost mHost;
    private ConfirmOrderViewModel mConfirmOrderViewModel;
    private GoogleMap mGoogleMap;
    private CreateOrderRequest mCreateOrderDetails;
    private DirectionsApi mDirectionsApiReq;
    private LatLng mOrigin;
    private Polyline mPolyLine;
    private ArrayList<LatLng> mDropsList;
    private boolean mIsToOptimize;
    private List<Route> mRoutes;


    public ConfirmOrderFragment() {

    }

    public static ConfirmOrderFragment getInstance(Parcelable createOrderDetails, boolean isToOptimize) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, createOrderDetails);
        bundle.putBoolean(AppConstants.IS_TO_OPTIMIZE, isToOptimize);
        ConfirmOrderFragment confirmOrderFragment = new ConfirmOrderFragment();
        confirmOrderFragment.setArguments(bundle);
        return confirmOrderFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_confirmation, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IConfirmOrderHost) {
            mHost = (IConfirmOrderHost) context;
        } else throw new IllegalStateException("Host must implement IConfirmOrderHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mConfirmOrderViewModel = new ViewModelProvider(this).get(ConfirmOrderViewModel.class);
        mConfirmOrderViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setMap();
        super.onViewCreated(view, savedInstanceState);

    }

    /**
     * method to setup map initially
     */
    private void setMap() {
        final SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        addFragmentWithoutAnimation(R.id.fl_map_container, mapFragment, null);
        mapFragment.getMapAsync(this);
    }

    private void observeLiveData() {
        mConfirmOrderViewModel.getDirectionsApiLiveData().observe(getViewLifecycleOwner(), directionsApiResponse -> {
            if (directionsApiResponse != null) {
                new ParserTask(ConfirmOrderFragment.this).execute(new Gson().toJson(directionsApiResponse));
                mRoutes = directionsApiResponse.getRoutes();
            }
        });

    }

    /**
     * Resets the drop sequence based on the response provided by directions Api
     * for optimized route
     *
     * @param routes Routes provided by google for given way points
     */
    private void setDropSequence(List<Route> routes) {
        List<Integer> seq = null;
        if (routes != null && routes.size() > 0) {
            seq = routes.get(0).getWaypointOrder();
        }

        List<DropOffArr> updatedDrops = new ArrayList<>();

        if (seq != null) {
            for (int i = 0; i < mCreateOrderDetails.getDropOffArr().size(); i++) {
                updatedDrops.add(mCreateOrderDetails.getDropOffArr().get(seq.get(i)));
            }

            mCreateOrderDetails.setDropOffArr(updatedDrops);
        }


    }

    @Override
    protected void initViewsAndVariables() {
        mDirectionsApiReq = new DirectionsApi();
        mDropsList = new ArrayList<>();
        getArgumentsData();
        tvTitle.setText(mCreateOrderDetails.getOrderType());

    }


    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS)!=null) {

            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
            mIsToOptimize = getArguments().getBoolean(AppConstants.IS_TO_OPTIMIZE);
            assert mCreateOrderDetails != null;

            setMarkersForPickupAndDrop(mCreateOrderDetails);
            setDeliveryCharges();
        }
    }

    private void setDeliveryCharges() {
        tvEta.setText(mCreateOrderDetails.getEta());

        BigDecimal amount = BigDecimal.valueOf(0);
        for (int i = 0; i < mCreateOrderDetails.getDropOffArr().size(); i++) {
            amount = amount.add(mCreateOrderDetails.getDropOffArr().get(i).getDeliveryCharge());
        }
        tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", amount));

    }


    private void setMarkersForPickupAndDrop(CreateOrderRequest createOrderDetails) {
        mOrigin = new LatLng(createOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                createOrderDetails.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));

        for (int i = 0; i < createOrderDetails.getDropOffArr().size(); i++) {
            mDropsList.add(new LatLng(createOrderDetails.getDropOffArr().get(i).getGeometry().getCoordinates().get(1),
                    createOrderDetails.getDropOffArr().get(i).getGeometry().getCoordinates().get(0)));
        }


        mConfirmOrderViewModel.getDirectionsResponse(mDirectionsApiReq.directionRequestUrl(mOrigin, mDropsList, mIsToOptimize));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        moveToCurrentLocation(new LatLng(AppConstants.KUWAIT_CENTER_LAT, AppConstants.KUWAIT_CENTER_LONG));


        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.setPadding(0, 0, 0, MAP_BOTTOM_PADDING);

    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, ZOOM_LEVEL));
        // Zoom in, animating the camera.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 15, animating with a duration of 2 seconds.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL), ZOOM_ANIMATE_DURATION, null);

    }

    @OnClick({R.id.btn_proceed, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed:
                mHost.navigateToOrderSummaryFragment(getOrderDetails());
                break;
            case R.id.iv_back:
                mHost.onBackPressed();
                break;

        }
    }

    private CreateOrderRequest getOrderDetails() {
        return mCreateOrderDetails;
    }

    @Override
    public void setPolyLineOptions(PolylineOptions polyLineOptions) {
        if (mGoogleMap != null) {

            MarkerOptions startMarkerOptions = new MarkerOptions().position(mOrigin).anchor(MARKER_POSITIONING_DIMEN, MARKER_POSITIONING_DIMEN).
                    icon(BitmapDescriptorFactory.fromBitmap(getBitmapForStartMarker()));

            ArrayList<MarkerOptions> mDropMarkerOptions = new ArrayList<>();
            for (int i = 0; i < mDropsList.size(); i++) {
                MarkerOptions dropMarkerOptions = new MarkerOptions().position(mDropsList.get(i)).anchor(MARKER_POSITIONING_DIMEN, MARKER_POSITIONING_DIMEN).
                        icon(BitmapDescriptorFactory.fromBitmap(getBitmapForDropMarker(mCreateOrderDetails.getDropOffArr().get(i).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""))));
                mDropMarkerOptions.add(dropMarkerOptions);
            }

            if (mPolyLine != null)
                mPolyLine.remove();

            mPolyLine = mGoogleMap.addPolyline(polyLineOptions);

            mGoogleMap.addMarker(startMarkerOptions);

            for (int i = 0; i < mDropsList.size(); i++) {
                mGoogleMap.addMarker(mDropMarkerOptions.get(i));
            }

            //startMarker.showInfoWindow();
            //destinationMarker.showInfoWindow();

            //fit to bound
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(mOrigin);

            for (int i = 0; i < mDropsList.size(); i++) {
                builder.include(mDropsList.get(i));
            }

            LatLngBounds bounds = builder.build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAP_BOUND_PADDING);
            mGoogleMap.animateCamera(cameraUpdate);

            setDropSequence(mRoutes);

        }
    }


    private Bitmap getBitmapForStartMarker() {
        LinearLayout llPickUpMarker = (LinearLayout) this.getLayoutInflater().inflate(R.layout.layout_start_marker, flMapContainer, false);
        TextView title = llPickUpMarker.findViewById(R.id.tv_title);
        title.setText(mCreateOrderDetails.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        llPickUpMarker.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        llPickUpMarker.layout(0, 0, llPickUpMarker.getMeasuredWidth(), llPickUpMarker.getMeasuredHeight());
        llPickUpMarker.setDrawingCacheEnabled(true);
        llPickUpMarker.buildDrawingCache();
        return llPickUpMarker.getDrawingCache();
    }

    private Bitmap getBitmapForDropMarker(CharSequence address) {
        LinearLayout llDropMarker = (LinearLayout) this.getLayoutInflater().inflate(R.layout.layout_drop_marker, flMapContainer, false);
        TextView dropAddress = llDropMarker.findViewById(R.id.tv_title);
        dropAddress.setText(address);
        llDropMarker.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        llDropMarker.layout(0, 0, llDropMarker.getMeasuredWidth(), llDropMarker.getMeasuredHeight());
        llDropMarker.setDrawingCacheEnabled(true);
        llDropMarker.buildDrawingCache();
        return llDropMarker.getDrawingCache();

    }

    @Override
    public void showNoRouteFound() {
        showSnackBar("No route found");
    }


    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();

    }

    public interface IConfirmOrderHost {
        void onBackPressed();
        void navigateToOrderSummaryFragment(CreateOrderRequest mCreateOrderDetails);
    }

}
