package com.qd.user.ui.onboard.welcome;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.socialresponse.FbLoginResponse;
import com.qd.user.model.socialresponse.socialsigninresponse.Result;
import com.qd.user.model.socialresponse.socialsigninresponse.SocialLoginResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

class WelcomeRepository {
    private CallbackManager mCallbackManager;

    void initializeFbLogin(final RichMediatorLiveData<FbLoginResponse> mFacebookLoginLiveData) {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("FacebookLogin:", "Success");
                        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), (jsonObject, graphResponse) -> {
                            FbLoginResponse fbLoginResponse = null;
                            try {
                                fbLoginResponse = new FbLoginResponse();
                                fbLoginResponse.setToken(AccessToken.getCurrentAccessToken().getToken());
                                fbLoginResponse.setFirstName(jsonObject.getString("first_name"));
                                fbLoginResponse.setLastName(jsonObject.getString("last_name"));
                                fbLoginResponse.setEmailId(jsonObject.getString("email"));
                                fbLoginResponse.setFbId(jsonObject.getString("id"));

                                JSONObject picture = new JSONObject(jsonObject.getString("picture"));
                                JSONObject data = picture.getJSONObject("data");
                                fbLoginResponse.setImgUrl(data.getString("url"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mFacebookLoginLiveData.setValue(fbLoginResponse);
                        });
                        Bundle param = new Bundle();
                        param.putString("fields", "id,email,picture,first_name,last_name");
                        graphRequest.setParameters(param);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("FacebookLogin:", "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("FacebookLogin:", exception.toString());
                    }
                });
    }


    CallbackManager getCallBackManager() {
        return mCallbackManager;
    }


    void getFireBaseToken(final RichMediatorLiveData<SocialLoginResponse> signUpLiveData, final CreateAccountInfo createAccountInfo) {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("Failed", "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    if (task.getResult() != null && !task.getResult().getToken().isEmpty()) {
                        DataManager.getInstance().setDeviceToken(task.getResult().getToken());
                        createAccountInfo.setDeviceToken(DataManager.getInstance().getDeviceToken());
                        signInThroughFbOrGoogle(signUpLiveData, createAccountInfo);
                    }
                });
    }

    private void signInThroughFbOrGoogle(final RichMediatorLiveData<SocialLoginResponse> mSocialLogInLiveData, CreateAccountInfo accountInfo) {
        DataManager.getInstance().hitSocialLoginApi(createSocialSignUnObject(accountInfo)).enqueue(new NetworkCallback<SocialLoginResponse>() {
            @Override
            public void onSuccess(SocialLoginResponse socialLoginResponse) {
                    setDataInPreference(socialLoginResponse.getResult());
                mSocialLogInLiveData.setValue(socialLoginResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mSocialLogInLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mSocialLogInLiveData.setError(t);
            }


            private void setDataInPreference(Result result) {
                if (result.getNewUser() != null) {
                    DataManager.getInstance().setUserId(result.getUserDetails().getId());
                    //DataManager.getInstance().setServiceType(result.getUserDetails().getS);
                    DataManager.getInstance().setRegistrationDateAndTime(result.getUserDetails().getCreatedAt());
                    DataManager.getInstance().setAccessToken(result.getAuthenticationToken());
                    DataManager.getInstance().setUserName(result.getUserDetails().getName());
                    DataManager.getInstance().setSignUpType(result.getUserDetails().getSignupType());
                    DataManager.getInstance().setCountryCode(result.getUserDetails().getContact().getCcPersonal());
                    DataManager.getInstance().setPhoneNumber(result.getUserDetails().getContact().getPersonal());
                    DataManager.getInstance().setEmailAddress(result.getUserDetails().getEmail());
                    DataManager.getInstance().setUserType(result.getUserDetails().getCashClientStatus());
                    DataManager.getInstance().setVendorStatus(result.getUserDetails().getStatus());
                    DataManager.getInstance().setCashClientCurrentStatus(result.getUserDetails().getCashClientCurrentStatus());
                    DataManager.getInstance().setCurrentStatus(result.getUserDetails().getCurrentStatus());
                    DataManager.getInstance().setUserLoggedIn(true);

                }


            }


        });
    }


    private HashMap<String, String> createSocialSignUnObject(CreateAccountInfo createAccountInfo) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_SOCIAL_PLATFORM, createAccountInfo.getPlatform());
        hashMap.put(NetworkConstants.KEY_APP_ID, createAccountInfo.getAppId());
        hashMap.put(NetworkConstants.KEY_CC_PERSONAL, AppConstants.COUNTRY_CODE);
        if (createAccountInfo.getPhoneNumber() != null)
            hashMap.put(NetworkConstants.KEY_PERSONAL, createAccountInfo.getPhoneNumber());
        hashMap.put(NetworkConstants.KEY_DEVICE_ID, createAccountInfo.getDeviceId());
        hashMap.put(NetworkConstants.KEY_PLATFORM, AppConstants.ANDROID);
        hashMap.put(NetworkConstants.KEY_DEVICE_TOKEN, DataManager.getInstance().getDeviceToken());
        if (createAccountInfo.getEmail() != null)
            hashMap.put(NetworkConstants.KEY_EMAIL, createAccountInfo.getEmail());
        if (createAccountInfo.getFirstName() != null)
            hashMap.put(NetworkConstants.KEY_FIRST_NAME, createAccountInfo.getFirstName());
        if (createAccountInfo.getLastName() != null)
            hashMap.put(NetworkConstants.KEY_LAST_NAME, createAccountInfo.getLastName());
        return hashMap;

    }


    void setIsUserVerified() {
        DataManager.getInstance().setUserVerified(true);
    }
}
