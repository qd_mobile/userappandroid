package com.qd.user.ui.createorder;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;
import com.qd.user.model.validationerror.ValidationErrors;

import java.util.List;

public class CreateOrderViewModel extends ViewModel {
    private RichMediatorLiveData<VehicleEtaResponse> mVehicleCategoryLiveData;
    private RichMediatorLiveData<DriverAvailabilityResponse> mDriverAvailabilityLiveData;
    private RichMediatorLiveData<OptimizedDeliveryResponse> mOptimizedDeliveryLiveData;
    private RichMediatorLiveData<VendorInfoResponse> mVendorInfoResponseLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private CreateOrderRepository mRepo = new CreateOrderRepository();


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mVehicleCategoryLiveData == null) {
            mVehicleCategoryLiveData = new RichMediatorLiveData<VehicleEtaResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mOptimizedDeliveryLiveData == null) {
            mOptimizedDeliveryLiveData = new RichMediatorLiveData<OptimizedDeliveryResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mDriverAvailabilityLiveData == null) {
            mDriverAvailabilityLiveData = new RichMediatorLiveData<DriverAvailabilityResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mVendorInfoResponseLiveData == null) {
            mVendorInfoResponseLiveData = new RichMediatorLiveData<VendorInfoResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the vehicle live data object to {@link CreateOrderFragment}
     *
     * @return {@link #mVehicleCategoryLiveData}
     */
    RichMediatorLiveData<VehicleEtaResponse> getVehicleCategoryLiveData() {
        return mVehicleCategoryLiveData;
    }

    /**
     * This method gives the CreateOrder live data object to {@link CreateOrderFragment}
     *
     * @return {@link #mDriverAvailabilityLiveData}
     */
    RichMediatorLiveData<DriverAvailabilityResponse> getDriverAvailabilityLiveData() {
        return mDriverAvailabilityLiveData;
    }

    /**
     * This method gives the delivery Charges live data object to {@link CreateOrderFragment}
     *
     * @return {@link #mOptimizedDeliveryLiveData}
     */
    RichMediatorLiveData<OptimizedDeliveryResponse> getOptimizedDeliveryLiveData() {
        return mOptimizedDeliveryLiveData;
    }

    /**
     * This method gives the vendor info live data object to {@link CreateOrderFragment} and {@link CreateOrderActivity}
     *
     * @return {@link #mVendorInfoResponseLiveData}
     */
    RichMediatorLiveData<VendorInfoResponse> getVendorInfoResponseLiveData() {
        return mVendorInfoResponseLiveData;
    }

    void getVehicleCategories(String orderType, double latitude, double longitude) {
        mRepo.getVehicleCategories(mVehicleCategoryLiveData, orderType, latitude, longitude);
    }

    boolean validateOrderLocationAndVehicle(List<PickupLocation> pickupLocation, List<DropOffArr> dropOffArr, String vehicleType) {
        if (pickupLocation == null || pickupLocation.get(0).getFullAddress() == null || pickupLocation.get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", "").isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PICKUP_LOCATION, R.string.s_please_select_pickup_location));
            return false;
        } else if (dropOffArr == null || dropOffArr.get(0).getFullAddress() == null || dropOffArr.get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", "").isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_DROP_LOCATION, R.string.s_please_select_drop_location));
            return false;
        } else if (vehicleType == null || vehicleType.isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_VEHICLE_TYPE, R.string.s_please_select_vehicle));
            return false;
        }
        return true;
    }

    public String getBusinessAddress() {
        return mRepo.getBusinessAddress();
    }


    public void getDeliveryCharges(OptimizedDeliveryRequest dataForOptimizedRoute) {
        mOptimizedDeliveryLiveData.setLoadingState(true);
        mRepo.getDeliveryCharges(mOptimizedDeliveryLiveData, dataForOptimizedRoute);
    }

    void getVendorInfo() {
//        mVendorInfoResponseLiveData.setLoadingState(true);
        mRepo.getVendorInfo(mVendorInfoResponseLiveData);
    }

    void checkAvailabilityStatus(List<Double> coordinates, String vehicleType) {
        mRepo.checkDriverAvailability(mDriverAvailabilityLiveData, coordinates, vehicleType);
    }

    String getVendorPaymentMode() {
        return mRepo.getVendorPaymentMode();
    }
}
