package com.qd.user.ui.home.businesspayment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.invoice.InvoiceListingResponse;

public class BusinessPaymentsViewModel extends ViewModel {


    private RichMediatorLiveData<InvoiceListingResponse> mInvoiceListingLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private BusinessPaymentsRepository mRepo = new BusinessPaymentsRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mInvoiceListingLiveData == null) {
            mInvoiceListingLiveData = new RichMediatorLiveData<InvoiceListingResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }


    /**
     * This method gives the Invoice listing live data object to {@link BusinessPaymentsFragment}
     *
     * @return {@link #mInvoiceListingLiveData}
     */
    RichMediatorLiveData<InvoiceListingResponse> getInvoiceListingLiveData() {
        return mInvoiceListingLiveData;
    }


    void getBusinessInvoiceList(int page, String fromDate, String toDate) {
        mRepo.getInvoiceListing(mInvoiceListingLiveData, page, fromDate, toDate);
    }

    public String getRegistrationDate() {
        return mRepo.getRegistrationDate();
    }
}
