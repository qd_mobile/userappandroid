package com.qd.user.ui.home;

interface DrawerLocked {
    void setDrawerLocked(boolean shouldLock);
}
