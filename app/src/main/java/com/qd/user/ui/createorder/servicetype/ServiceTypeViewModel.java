package com.qd.user.ui.createorder.servicetype;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.createorder.servicetype.FlexibleSlotsResponse;
import com.qd.user.model.failureresponse.FailureResponse;

public class ServiceTypeViewModel extends ViewModel {

    private RichMediatorLiveData<FlexibleSlotsResponse> mFlexibleSlotsLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;


    private ServiceTypeRepository mRepo = new ServiceTypeRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mFlexibleSlotsLiveData == null) {
            mFlexibleSlotsLiveData = new RichMediatorLiveData<FlexibleSlotsResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }
    }

    /**
     * This method gives the ServiceType live data object to {@link ServiceTypeDialogFragment}
     *
     * @return {@link #mFlexibleSlotsLiveData}
     */
    RichMediatorLiveData<FlexibleSlotsResponse> getFlexibleSlotsLiveData() {
        return mFlexibleSlotsLiveData;
    }

    void getFlexibleTimeSlots() {
        mRepo.getFlexibleTimeSlots(mFlexibleSlotsLiveData);
    }


    public String getServiceType() {
        return mRepo.getServiceType();
    }
}
