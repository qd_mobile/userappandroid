package com.qd.user.ui.trackorder.orderstatus;

import com.google.gson.GsonBuilder;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.orderstatus.OrderStatusResponse;
import com.qd.user.socket.SocketConstants;
import com.qd.user.socket.SocketManager;

import org.json.JSONException;
import org.json.JSONObject;

class OrderStatusRepository {

    void emitForOrderStatus(String orderUniqueId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(NetworkConstants.ORDER_ID, orderUniqueId);
            SocketManager.getInstance().sendDataOnSocket(SocketConstants.EVENT_ORDER_STATUS, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void listenToOrderStatus(final RichMediatorLiveData<OrderStatusResponse> orderStatusLiveData) {
        SocketManager.getInstance().listenToOrderStatus(args -> {
            final String s = args[0].toString();
            OrderStatusResponse orderStatusResponse = new GsonBuilder().create().fromJson(s, OrderStatusResponse.class);
            orderStatusLiveData.postValue(orderStatusResponse);
        });
    }

    void getCurrentOrderStatus(final RichMediatorLiveData<OrderDetailResponse> orderStatusLiveData, String orderUniqueId) {
        DataManager.getInstance().getCurrentOrderStatus(orderUniqueId).enqueue(new NetworkCallback<OrderDetailResponse>() {
            @Override
            public void onSuccess(OrderDetailResponse ordersResponse) {
                orderStatusLiveData.setValue(ordersResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                orderStatusLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                orderStatusLiveData.setError(t);
            }
        });
    }
}
