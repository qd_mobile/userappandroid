package com.qd.user.ui.home.myorders;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

class MyOrdersViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    //Constructor
    MyOrdersViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    // Fragment for returning.

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    //Fragment loading count.
    @Override
    public int getCount() {
        return mFragmentList.size();
    }


    // Fragment & Title Insertion.
    void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    //Tab text
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

}
