package com.qd.user.ui.createorder.selectlocation.advancedsearch;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.advancedsearch.SaveAddressResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.kuwaitfinder.blocks.BlocksResponse;
import com.qd.user.model.kuwaitfinder.geocode.GeoCodeLocationResponse;
import com.qd.user.model.kuwaitfinder.governorate.GovernorateResponse;
import com.qd.user.model.kuwaitfinder.neighbourhood.NeighbourhoodResponse;
import com.qd.user.model.kuwaitfinder.streets.StreetsResponse;
import com.qd.user.model.kuwaitfinder.token.TokenResponse;
import com.qd.user.model.validationerror.ValidationErrors;


public class AdvancedSearchViewModel extends ViewModel {

    private RichMediatorLiveData<TokenResponse> mTokenLiveData;
    private RichMediatorLiveData<GovernorateResponse> mGovernorateLiveData;
    private RichMediatorLiveData<BlocksResponse> mBlocksLiveData;
    private RichMediatorLiveData<StreetsResponse> mStreetLiveData;
    private RichMediatorLiveData<NeighbourhoodResponse> mNeighbourhoodLiveData;
    private RichMediatorLiveData<GeoCodeLocationResponse> mGeocodeLiveData;
    private RichMediatorLiveData<SaveAddressResponse> mSaveAddressLiveData;


    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private AdvancedSearchRepository mRepo = new AdvancedSearchRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mTokenLiveData == null) {
            mTokenLiveData = new RichMediatorLiveData<TokenResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if (mValidateLiveData == null) {
                mValidateLiveData = new MutableLiveData<>();
            }
        }

        if (mGovernorateLiveData == null) {
            mGovernorateLiveData = new RichMediatorLiveData<GovernorateResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mNeighbourhoodLiveData == null) {
            mNeighbourhoodLiveData = new RichMediatorLiveData<NeighbourhoodResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mBlocksLiveData == null) {
            mBlocksLiveData = new RichMediatorLiveData<BlocksResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mStreetLiveData == null) {
            mStreetLiveData = new RichMediatorLiveData<StreetsResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mGeocodeLiveData == null) {
            mGeocodeLiveData = new RichMediatorLiveData<GeoCodeLocationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mSaveAddressLiveData == null) {
            mSaveAddressLiveData = new RichMediatorLiveData<SaveAddressResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateLiveData == null) {
            mValidateLiveData = new RichMediatorLiveData<ValidationErrors>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the token live data object to {@link AdvancedSearchFragment}
     *
     * @return {@link #mTokenLiveData}
     */
    RichMediatorLiveData<TokenResponse> getTokenLiveData() {
        return mTokenLiveData;
    }

    /**
     * This method gives the governorate live data object to {@link AdvancedSearchFragment}
     *
     * @return {@link #mGovernorateLiveData}
     */
    RichMediatorLiveData<GovernorateResponse> getGovernorateLiveData() {
        return mGovernorateLiveData;
    }

    RichMediatorLiveData<BlocksResponse> getBlocksLiveData() {
        return mBlocksLiveData;
    }

    RichMediatorLiveData<StreetsResponse> getStreetsLiveData() {
        return mStreetLiveData;
    }

    RichMediatorLiveData<NeighbourhoodResponse> getNeighbourhoodLiveData() {
        return mNeighbourhoodLiveData;
    }

    void getTokenForKuwaitFinder() {
        mRepo.getTokenForKuwaitFinder(mTokenLiveData);
    }

    void getGovernorate(String token) {
        mGovernorateLiveData.setLoadingState(true);
        mRepo.getGovernorate(mGovernorateLiveData, token);
    }

    void getNeighbourhood(String token, int govNo) {
        mNeighbourhoodLiveData.setLoadingState(true);
        mRepo.getNeighbourhood(mNeighbourhoodLiveData, token, govNo);
    }

    void getBlock(String token, int nhoodNo) {
        mBlocksLiveData.setLoadingState(true);
        mRepo.getBlock(mBlocksLiveData, token, nhoodNo);
    }

    void getStreet(String token, String blockId, int nhoodNo) {
        mStreetLiveData.setLoadingState(true);
        mRepo.getStreet(mStreetLiveData, token, blockId, nhoodNo);
    }

    /*void getGeoCodeLocation(String token, String street) {
        mGeocodeLiveData.setLoadingState(true);
        mRepo.getGeoCode(mGeocodeLiveData, token, street);
    }*/

    RichMediatorLiveData<GeoCodeLocationResponse> getGeoCodeLiveData() {
        return mGeocodeLiveData;
    }

    RichMediatorLiveData<SaveAddressResponse> getSaveAddressLiveData() {
        return mSaveAddressLiveData;
    }

    boolean checkValidation(SaveAddressRequest completeAddress, boolean isStreetOptional) {
        if (TextUtils.isEmpty(completeAddress.getGovernorate())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_GOVERNORATE, R.string.s_please_select_governorate));
            return false;
        } else if (TextUtils.isEmpty(completeAddress.getArea())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_AREA, R.string.s_please_Select_area));
            return false;
        } else if (TextUtils.isEmpty(completeAddress.getBlockNumber())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_BLOCK, R.string.s_please_select_block_number));
            return false;
        } else if (!isStreetOptional && TextUtils.isEmpty(completeAddress.getStreet())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_STREET, R.string.s_please_select_street));
            return false;
        }else if(completeAddress.getGeometry()==null || completeAddress.getGeometry().getCoordinates()==null){
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_LAT_LNG, R.string.s_unable_to_get_location_coordinates));
            return false;
        }
        return true;
    }

    void markAddressAsFavourite(SaveAddressRequest completeAddress, String title) {
        mSaveAddressLiveData.setLoadingState(true);
        completeAddress.setName(title);
        mRepo.saveAddressInFav(mSaveAddressLiveData, completeAddress);
    }

    void onSaveAddress(SaveAddressRequest completeAddress, String id, String name) {
        completeAddress.setName(name);
        mRepo.hitEditSaveAddressApi(mSaveAddressLiveData, completeAddress, id);
    }

    public String getUserType() {
        return mRepo.getUserType();
    }

}
