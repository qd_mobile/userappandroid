package com.qd.user.ui.createorder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.eyalbira.loadingdots.LoadingDots;
import com.qd.user.R;
import com.qd.user.model.createorder.distancetime.eta.VehicleCategory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleTypeAdapter extends RecyclerView.Adapter<VehicleTypeAdapter.VehicleTypeAdapterViewHolder> {


    private ArrayList<VehicleCategory> mVehiclesList;
    private VehicleTypeAdapterInterface mInterface;

    VehicleTypeAdapter(ArrayList<VehicleCategory> vehiclesList, VehicleTypeAdapterInterface vehicleTypeAdapterInterface) {
        mVehiclesList = vehiclesList;
        mInterface = vehicleTypeAdapterInterface;

    }

    @NonNull
    @Override
    public VehicleTypeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_select_vehicle, viewGroup, false);
        return new VehicleTypeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleTypeAdapterViewHolder vehicleTypeAdapterViewHolder, int i) {
        vehicleTypeAdapterViewHolder.tvVehicleType.setText(mVehiclesList.get(i).getKeyword());
        if (mVehiclesList.get(i).getEta() != null) {
            vehicleTypeAdapterViewHolder.ldProgress.stopAnimation();
            vehicleTypeAdapterViewHolder.ldProgress.setVisibility(View.GONE);
            vehicleTypeAdapterViewHolder.tvEta.setVisibility(View.VISIBLE);
            vehicleTypeAdapterViewHolder.tvEta.setText(mVehiclesList.get(i).getEta());
        } else {
            vehicleTypeAdapterViewHolder.ldProgress.setVisibility(View.VISIBLE);
            vehicleTypeAdapterViewHolder.ldProgress.startAnimation();
            vehicleTypeAdapterViewHolder.tvEta.setVisibility(View.GONE);
        }


        if (mVehiclesList.get(i).isSelected()) {
            Glide.with(vehicleTypeAdapterViewHolder.ivVehicleType.getContext())
                    .load(mVehiclesList.get(i).getIconLink().getBlack())
                    .apply(RequestOptions.circleCropTransform())
                    .into(vehicleTypeAdapterViewHolder.ivVehicleType);
        } else {
            Glide.with(vehicleTypeAdapterViewHolder.ivVehicleType.getContext())
                    .load(mVehiclesList.get(i).getIconLink().getGrey())
                    .apply(RequestOptions.circleCropTransform())
                    .into(vehicleTypeAdapterViewHolder.ivVehicleType);
        }

    }

    @Override
    public int getItemCount() {
        return mVehiclesList.size();
    }

    public interface VehicleTypeAdapterInterface {

        void onVehicleTypeSelected(VehicleCategory result);
    }

    class VehicleTypeAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ld_progress)
        LoadingDots ldProgress;
        @BindView(R.id.tv_eta)
        TextView tvEta;
        @BindView(R.id.iv_vehicle_type)
        ImageView ivVehicleType;
        @BindView(R.id.tv_vehicle_type)
        TextView tvVehicleType;

        VehicleTypeAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                deselectAllVehicles();
                mVehiclesList.get(getAdapterPosition()).setSelected(true);
                mInterface.onVehicleTypeSelected(mVehiclesList.get(getAdapterPosition()));
                notifyDataSetChanged();
            });
        }

        private void deselectAllVehicles() {
            for (int i = 0; i < mVehiclesList.size(); i++) {
                mVehiclesList.get(i).setSelected(false);
            }
        }
    }
}
