package com.qd.user.ui.createorder.selectlocation.savedaddress;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.failureresponse.FailureResponse;

public class SavedAddressViewModel extends ViewModel {
    private RichMediatorLiveData<SavedAddressResponse> mSavedAddressLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;


    private SavedAddressRepository mRepo = new SavedAddressRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mSavedAddressLiveData == null) {
            mSavedAddressLiveData = new RichMediatorLiveData<SavedAddressResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }
    }


    /**
     * This method gives the SavedAddress live data object to {@link SavedAddressFragment}
     *
     * @return {@link #mSavedAddressLiveData}
     */
    RichMediatorLiveData<SavedAddressResponse> getSavedAddressLiveData() {
        return mSavedAddressLiveData;
    }


    void getSavedAddresses(String searchString) {
        mRepo.getSavedAddresses(mSavedAddressLiveData, searchString);
    }
}
