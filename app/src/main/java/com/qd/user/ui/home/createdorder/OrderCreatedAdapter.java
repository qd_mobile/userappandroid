package com.qd.user.ui.home.createdorder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.myorders.Datum;
import com.qd.user.model.myorders.DropLocation;
import com.qd.user.model.myorders.PickupLocation;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.ProgressLoader;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

class OrderCreatedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Object> mOrdersList;
    private IMyOrders mIMyOrders;
    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;


    OrderCreatedAdapter(ArrayList<Object> ordersList, IMyOrders iMyOrders) {
        mIMyOrders = iMyOrders;
        mOrdersList = ordersList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_my_orders, viewGroup, false);
            return new OrdersAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mOrdersList.get(position) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mOrdersList.get(position) instanceof Datum) {
            Datum datum = (Datum) mOrdersList.get(position);
            ((OrdersAdapterViewHolder) holder).tvOrderId.setText(String.format("ID - #%s", datum.getOrderUniqueId()));

            if (datum.getVendorId().get(0).getCompany() != null)
                ((OrdersAdapterViewHolder) holder).tvCreatorName.setText(String.format("(%s)", datum.getVendorId().get(0).getCompany().getNameEnglish()));
            else
                ((OrdersAdapterViewHolder) holder).tvCreatorName.setText(String.format("(%s)", datum.getPickupLocation().get(0).getRecipientName()));
            ((OrdersAdapterViewHolder) holder).tvReceiverName.setText(String.format("(%s)", datum.getDropLocation().get(0).getRecipientName()));
            ((OrdersAdapterViewHolder) holder).tvPickupLocation.setText(datum.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            ((OrdersAdapterViewHolder) holder).tvDropOffLocation.setText(datum.getDropLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));


            if (datum.getDropLocation().size() > 1) {
                ((OrdersAdapterViewHolder) holder).tvMoreDrops.setText(String.format(Locale.getDefault(), "(+%d more)", datum.getDropLocation().size() - 1));
            } else
                ((OrdersAdapterViewHolder) holder).tvMoreDrops.setVisibility(View.GONE);

            if (datum.getScheduledStartTime() != null) {
                ((OrdersAdapterViewHolder) holder).tvDate.setText(DateFormatter.getInstance().getFormattedDate(datum.getScheduledStartTime()));
                ((OrdersAdapterViewHolder) holder).tvTime.setText(DateFormatter.getInstance().getFormattedTime(datum.getScheduledStartTime()));
                ((OrdersAdapterViewHolder) holder).tvDay.setText(DateFormatter.getInstance().getFormattedDay(datum.getScheduledStartTime()));
            }

            ((OrdersAdapterViewHolder) holder).tvOrderType.setText(datum.getOrderType());
            switch (datum.getOrderType()) {
                case AppConstants.OrderType.SEND:
                case AppConstants.OrderType.SPECIAL_SEND:
                    ((OrdersAdapterViewHolder) holder).ivOrderType.setImageDrawable(((OrdersAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_send));
                    break;
                case AppConstants.OrderType.GET:
                case AppConstants.OrderType.SPECIAL_GET:
                    ((OrdersAdapterViewHolder) holder).ivOrderType.setImageDrawable(((OrdersAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_get));
                    break;
                case AppConstants.OrderType.BUSINESS:
                    ((OrdersAdapterViewHolder) holder).ivOrderType.setImageDrawable(((OrdersAdapterViewHolder) holder).ivOrderType.getResources()
                            .getDrawable(R.drawable.ic_dashboard_business));
                    break;
            }

            switch (datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus()) {
                case AppConstants.OrderStatus.ORDER_CREATED:
                case AppConstants.OrderStatus.DRIVER_ALIGNED:
                case AppConstants.OrderStatus.DELIVERED:
                    if (datum.getDropLocation().size() == 1 &&
                            (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                    .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).
                                getDriverTrackingStatus().size() - 1).getStatus());
                        ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.GONE);
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorBlue));
                    } else {
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                        ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.GONE);
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorGreenishTeal));
                    }
                    break;
                case AppConstants.OrderStatus.RETURNED:
                case AppConstants.OrderStatus.CASHBACK_RETURNED:
                    ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.GONE);
                    ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                    break;
                case AppConstants.OrderStatus.AUTO_CANCELLED:
                case AppConstants.OrderStatus.CANCELLED:
                    ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.GONE);
                    ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_reddish));
                    ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                            getColor(R.color.colorDarkRed));
                    break;

                case AppConstants.OrderStatus.PICKEDUP:
                    if (((Datum) mOrdersList.get(position)).getServiceType().equals(AppConstants.serviceType.FLEXIBLE)) {
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                        ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.GONE);
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorGreenishTeal));
                        break;
                    }
                default:
                    if (datum.getDropLocation().size() == 1 &&
                            (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                    .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().
                                get(0).getDriverTrackingStatus().size() - 1).getStatus());
                        ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.GONE);
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorBlue));
                    } else {
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setText(R.string.s_track);
                        ((OrdersAdapterViewHolder) holder).tvInProgress.setVisibility(View.VISIBLE);
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setBackground(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getDrawable(R.drawable.drawable_rectangle_stroke_gun_metal));
                        ((OrdersAdapterViewHolder) holder).tvOrderStatus.setTextColor(((OrdersAdapterViewHolder) holder).tvOrderStatus.getResources().
                                getColor(R.color.colorTextBlack));
                    }
            }

        }

    }

    @Override
    public int getItemCount() {
        return mOrdersList.size();
    }

    //hide progress loader after getting paginated data
    void hideProgress() {
        mOrdersList.remove(mOrdersList.size() - 1);
        notifyDataSetChanged();
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mOrdersList.add(new ProgressLoader());
        notifyItemInserted(mOrdersList.size());
    }

    public interface IMyOrders {
        void onRowOrderViewClick(String id, String orderType);

        void onTrackOrderClicked(String id);

        void showPickupAddress(PickupLocation pickupLocation);

        void showDropAddress(DropLocation dropLocation);
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrdersList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }

    class OrdersAdapterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_label_order_id)
        TextView tvLabelOrderId;
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_order_id)
        TextView tvOrderId;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.view_date_and_time)
        View viewDateAndTime;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.view_my_order)
        View viewMyOrder;
        @BindView(R.id.iv_order_type)
        ImageView ivOrderType;
        @BindView(R.id.tv_order_type)
        TextView tvOrderType;
        @BindView(R.id.tv_order_status)
        TextView tvOrderStatus;
        @BindView(R.id.tv_in_progress)
        TextView tvInProgress;
        @BindView(R.id.iv_pickup_location)
        ImageView ivPickupLocation;
        @BindView(R.id.tv_label_pickup_location)
        TextView tvLabelPickupLocation;
        @BindView(R.id.tv_creator_name)
        TextView tvCreatorName;
        @BindView(R.id.tv_pickup_location)
        TextView tvPickupLocation;
        @BindView(R.id.view_dotted_line)
        View viewDottedLine;
        @BindView(R.id.iv_drop_off_location)
        ImageView ivDropOffLocation;
        @BindView(R.id.tv_label_drop_off_location)
        TextView tvLabelDropOffLocation;
        @BindView(R.id.tv_receiver_name)
        TextView tvReceiverName;
        @BindView(R.id.tv_drop_off_location)
        TextView tvDropOffLocation;
        @BindView(R.id.tv_more_drops)
        TextView tvMoreDrops;
        @BindView(R.id.rl_location_info)
        RelativeLayout rlLocationInfo;
        @BindView(R.id.guidelineStart)
        Guideline guidelineStart;
        @BindView(R.id.guidelineEnd)
        Guideline guidelineEnd;

        OrdersAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> mIMyOrders.onRowOrderViewClick(((Datum) mOrdersList.get(getAdapterPosition())).getId(), ((Datum) mOrdersList.get(getAdapterPosition())).getOrderType()));


            tvOrderStatus.setOnClickListener(v -> {
                switch (((Datum) mOrdersList.get(getAdapterPosition())).getOrderStatus().get(((Datum) mOrdersList.get(getAdapterPosition())).getOrderStatus().size() - 1).getStatus()) {
                    case AppConstants.OrderStatus.ORDER_CREATED:
                    case AppConstants.OrderStatus.DRIVER_ALIGNED:
                    case AppConstants.OrderStatus.DELIVERED:
                    case AppConstants.OrderStatus.RETURNED:
                    case AppConstants.OrderStatus.CANCELLED:
                    case AppConstants.OrderStatus.AUTO_CANCELLED:
                    case AppConstants.OrderStatus.CASHBACK_RETURNED:
                        break;
                    case AppConstants.OrderStatus.PICKEDUP:
                        if(((Datum) mOrdersList.get(getAdapterPosition())).getServiceType().equals(AppConstants.serviceType.FLEXIBLE))
                        break;
                        else {
                            mIMyOrders.onTrackOrderClicked(((Datum) mOrdersList.get(getAdapterPosition())).getId());
                            break;
                        }
                    default:
                        mIMyOrders.onTrackOrderClicked(((Datum) mOrdersList.get(getAdapterPosition())).getId());
                }
            });

            tvPickupLocation.setOnClickListener(v -> {
                mIMyOrders.showPickupAddress(((Datum) mOrdersList.get(getAdapterPosition())).getPickupLocation().get(0));
            });

            tvDropOffLocation.setOnClickListener(v -> {
                mIMyOrders.showDropAddress(((Datum) mOrdersList.get(getAdapterPosition())).getDropLocation().get(0));
            });


        }
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;

        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
