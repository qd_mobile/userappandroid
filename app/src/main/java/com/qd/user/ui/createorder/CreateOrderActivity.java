package com.qd.user.ui.createorder;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.GsonBuilder;
import com.qd.user.QuickDelivery;
import com.qd.user.R;
import com.qd.user.base.BaseActivity;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.additem.ItemDetails;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.distancetime.eta.VehicleCategory;
import com.qd.user.model.createorder.optimizedroute.DropLocation;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.Origin;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.ui.createorder.additem.AddItemFragment;
import com.qd.user.ui.createorder.confirmation.ConfirmOrderFragment;
import com.qd.user.ui.createorder.droplocation.DropLocationsListFragment;
import com.qd.user.ui.createorder.findingdriver.FindingDriverFragment;
import com.qd.user.ui.createorder.itemlist.ItemsListFragment;
import com.qd.user.ui.createorder.ordersummary.OrderSummaryFragment;
import com.qd.user.ui.createorder.recipientdetails.RecipientDetailsFragment;
import com.qd.user.ui.createorder.selectlocation.SelectLocationFragment;
import com.qd.user.ui.createorder.selectlocation.advancedsearch.AdvancedSearchFragment;
import com.qd.user.ui.createorder.selectlocation.map.MapFragment;
import com.qd.user.ui.createorder.selectlocation.savedaddress.SavedAddressFragment;
import com.qd.user.ui.createorder.selectvehicle.SelectVehicleFragment;
import com.qd.user.ui.createorder.servicetype.ServiceTypeDialogFragment;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.ui.home.notifications.NotificationsFragment;
import com.qd.user.ui.onboard.OnBoardActivity;
import com.qd.user.ui.trackorder.TrackOrderActivity;
import com.qd.user.utils.PermissionHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

public class CreateOrderActivity extends BaseActivity implements CreateOrderFragment.ICreateOrderHost,
        SelectLocationFragment.ISelectLocationHost, MapFragment.IMapHost, AdvancedSearchFragment.IAdvancedSearchHost,
        SavedAddressFragment.ISavedAddressHost, AddItemFragment.IAddItemHost, RecipientDetailsFragment.IRecipientDetailsHost,
        ItemsListFragment.IItemsListHost, ConfirmOrderFragment.IConfirmOrderHost, OrderSummaryFragment.IOrderSummaryHost,
        ServiceTypeDialogFragment.IServiceTypeHost, SelectVehicleFragment.ISelectVehicleHost, DropLocationsListFragment.IDropLocationsListHost,
        FindingDriverFragment.IFindingDriverHost, NotificationsFragment.INotificationsHost, PermissionHelper.IGetPermissionListener {


    @BindView(R.id.cl_root_view)
    ConstraintLayout clRootView;
    private CreateOrderViewModel mCreateOrderViewModel;
    private CreateOrderRequest mCreateOrderDetails;
    private int count;
    private DropOffArr mDrop;


    @Override
    protected void initViewsAndVariables() {
        mCreateOrderViewModel = new ViewModelProvider(this).get(CreateOrderViewModel.class);
        mCreateOrderViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
    }

    @Override
    protected int getResourceId() {
        return R.layout.activity_on_board;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null && getIntent().getStringExtra(AppConstants.ORDER_TYPE) != null) {
            openCreateOrderFragment(getIntent().getStringExtra(AppConstants.ORDER_TYPE));
        }

        observeLiveData();
        getIntentData(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getIntentData(intent);

    }


    private void getIntentData(Intent intent) {
        if (intent != null && intent.getStringExtra(AppConstants.ACTION_NAVIGATION) != null) {
            switch (Objects.requireNonNull(intent.getStringExtra(AppConstants.ACTION_NAVIGATION))) {
                case AppConstants.Navigation.TRACK_ORDER:
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
                    if (fragment instanceof FindingDriverFragment) {
                        String orderId = intent.getStringExtra(AppConstants.ORDER_ID);
                        navigateToTrackOrderFragment(orderId);
                    }
                    break;
                case AppConstants.Navigation.FINDING_DRIVER:
                    com.qd.user.model.createorder.response.Result result = new com.qd.user.model.createorder.response.Result();
                    result.setId(intent.getStringExtra(AppConstants.ORDER_ID));
                    result.setOrderType(getString(R.string.cash_client));
                    addFragment(R.id.cl_root_view, FindingDriverFragment.getInstance(result), FindingDriverFragment.class.getName());
            }
        }
    }

    private void observeLiveData() {
        mCreateOrderViewModel.getOptimizedDeliveryLiveData().observe(this, optimizedDeliveryResponse -> {
            if (optimizedDeliveryResponse != null && optimizedDeliveryResponse.getResult().get(0).getDeliveryCharge() != null) {
                if (mDrop != null) {
                    mDrop.setDeliveryCharge(optimizedDeliveryResponse.getResult().get(0).getDeliveryCharge());
                    mCreateOrderDetails.getDropOffArr().add(mDrop);
                    mDrop = null;
                    addFragmentWithBackStack(R.id.cl_root_view, AddItemFragment.getInstance(null, mCreateOrderDetails.getDropOffArr().size() - 1, mCreateOrderDetails), AddItemFragment.class.getName());
                }
            }
        });
    }

    /*@Override
    public void navigateToItemsListFragment(ItemDetails item, CreateOrderRequest createOrderDetails) {
        onBackPressed();
        if (getSupportFragmentManager().findFragmentByTag(ItemsListFragment.class.getName()) == null) {
            addFragmentWithBackStack(R.id.cl_root_view, ItemsListFragment.getInstance(item, createOrderDetails), ItemsListFragment.class.getName());
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
            if (fragment instanceof ItemsListFragment) {
                ((ItemsListFragment) fragment).updateItemsList(item);
            }
        }
    }*/

    /**
     * Updates saved addresses as soon as a new address has been added to the list
     *
     * @param result Object containing the new saved address
     */
    @Override
    public void updateSavedAddress(Result result) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof SelectLocationFragment) {
            ((SelectLocationFragment) fragment).updateSavedAddress(result);
        }
    }

    @Override
    public void backToProfileFragment(Result result) {

    }

    private void clearReferences() {
        Activity currActivity = QuickDelivery.getInstance().getCurrentActivity();
        if (this.equals(currActivity))
            QuickDelivery.getInstance().setCurrentActivity(null);
    }


    /**
     * Opens drop location list fragment, while removing all the fragments that currently exist in the stack
     * using {@code getSupportFragmentManager().popBackStackImmediate()} as because once the user navigates to
     * {@link DropLocationsListFragment} he should not be able to go back and edit the details, for that edit
     * option has been given itself on {@link DropLocationsListFragment}
     *
     * @param createOrderRequest CreateOrderRequest object containing all the updated data
     */
    @Override
    public void openDropLocationListFragment(CreateOrderRequest createOrderRequest) {
        if (getSupportFragmentManager().findFragmentByTag(DropLocationsListFragment.class.getName()) == null) {
            while (getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof CreateOrderFragment) {
                getSupportFragmentManager().popBackStackImmediate();
            }
            addFragmentWithBackStack(R.id.cl_root_view, DropLocationsListFragment.getInstance(createOrderRequest, RecipientDetailsFragment.class.getName()), DropLocationsListFragment.class.getName());
        } else {
            while (!(getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof DropLocationsListFragment)) {
                getSupportFragmentManager().popBackStackImmediate();
            }
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
            if (fragment instanceof DropLocationsListFragment) {
                ((DropLocationsListFragment) fragment).updateDropLocationsList((ArrayList<DropOffArr>) createOrderRequest.getDropOffArr());
            }
        }
    }

    @Override
    public void getDeliveryCharges(CreateOrderRequest createOrderRequest) {
        mCreateOrderDetails = createOrderRequest;
        mCreateOrderViewModel.getDeliveryCharges(getDataForOptimizedRoute(createOrderRequest));
    }

    /**
     * Opens add Item fragment by mapping the {@link SaveAddressRequest} object with the {@link DropOffArr}
     * and if it is second attempt i.e., in case of multiple drops, check for instance of {@link DropLocationsListFragment}
     * and add dropOff Array to existing {@code CreateOrderRequest}
     *
     * @param saveAddressRequest Object containing Address selected
     */
    @Override
    public void openAddItemFragment(SaveAddressRequest saveAddressRequest) {
        DropOffArr dropOffArr = new GsonBuilder().create().fromJson(new GsonBuilder().create().toJson(saveAddressRequest), DropOffArr.class);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(DropLocationsListFragment.class.getName());
        if (fragment instanceof DropLocationsListFragment) {
            CreateOrderRequest createOrderRequest = ((DropLocationsListFragment) fragment).getOrderRequest();
            createOrderRequest.getDropOffArr().add(dropOffArr);
            mCreateOrderViewModel.getDeliveryCharges(getDataForOptimizedRoute(createOrderRequest));
            mCreateOrderDetails = createOrderRequest;
        }
    }

    /**
     * Creates a request object to find out delivery charges for selected pickup and drop location
     *
     * @param createOrderRequest Create Order Request object that contains all the data filled by the user so far
     * @return Request object to make network call to get required delivery charges
     */
    private OptimizedDeliveryRequest getDataForOptimizedRoute(CreateOrderRequest createOrderRequest) {
        OptimizedDeliveryRequest optimizedDeliveryRequest = new OptimizedDeliveryRequest();
        Origin origin = new Origin();
        origin.setLat(createOrderRequest.getPickupLocation().get(0).getGeometry().getCoordinates().get(1));
        origin.setLng(createOrderRequest.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
        optimizedDeliveryRequest.setOrigin(origin);

        DropLocation dropLocation = new DropLocation();
        dropLocation.setLat(createOrderRequest.getDropOffArr().get(createOrderRequest.getDropOffArr().size() - 1).getGeometry().getCoordinates().get(1));
        dropLocation.setLng(createOrderRequest.getDropOffArr().get(createOrderRequest.getDropOffArr().size() - 1).getGeometry().getCoordinates().get(0));

        List<DropLocation> dropLocationList = new ArrayList<>();
        dropLocationList.add(dropLocation);
        optimizedDeliveryRequest.setDropLocation(dropLocationList);

        optimizedDeliveryRequest.setServiceType(createOrderRequest.getServiceType());

        if (createOrderRequest.getOrderType().equals(AppConstants.OrderType.SEND) || createOrderRequest.getOrderType().equals(AppConstants.OrderType.SPECIAL_SEND))
            optimizedDeliveryRequest.setOrderType(AppConstants.OrderType.SPECIAL_SEND);
        else
            optimizedDeliveryRequest.setOrderType(createOrderRequest.getOrderType());


        optimizedDeliveryRequest.setVehicleType(createOrderRequest.getVehicleType());
        optimizedDeliveryRequest.setUserType(createOrderRequest.getRequestFrom());
        optimizedDeliveryRequest.setOptimize(true);

        mDrop = createOrderRequest.getDropOffArr().remove(createOrderRequest.getDropOffArr().size() - 1);

        return optimizedDeliveryRequest;
    }

    /**
     * Opens add item fragment in general case for no drop position to be updated, hence itemDetails are passed as {@null} and
     * drop position as -1
     */
    @Override
    public void openAddItemFragment(CreateOrderRequest createOrderData) {
        addFragmentWithBackStack(R.id.cl_root_view, AddItemFragment.getInstance(null, -1, createOrderData), AddItemFragment.class.getName());
    }

    /**
     * Navigates to {@link ConfirmOrderFragment} where user can see the preferred route for the drops he selected
     * and total delivery charges that will be charged for that order
     *
     * @param createOrderDetails Updated {@link CreateOrderRequest} object containing all the updated data
     * @param isToOptimize       Whether user wants the route to be optimized or in the same sequence he has re-arranged
     *                           the drops in case of multiple deliveries
     */
    @Override
    public void navigateToConfirmOrderFragment(CreateOrderRequest createOrderDetails, boolean isToOptimize) {
        addFragmentWithBackStack(R.id.cl_root_view, ConfirmOrderFragment.getInstance(createOrderDetails, isToOptimize), ConfirmOrderFragment.class.getName());
    }


    /**
     * Back to {@link AddItemFragment} for editing the item details corresponding to the item details and drop position given
     *
     * @param itemDetails        {@link ItemDetails} object containing item details that are to be shown pre-filled for editing
     * @param position           Drop position to be edited
     * @param createOrderDetails Updated {@link CreateOrderRequest} object containing all the updated data till now
     */
    @Override
    public void backToAddItemFragment(ItemDetails itemDetails, int position, CreateOrderRequest createOrderDetails) {
        addFragmentWithBackStack(R.id.cl_root_view, AddItemFragment.getInstance(itemDetails, position, createOrderDetails), AddItemFragment.class.getName());
    }

    /**
     * Navigates to {@link TrackOrderActivity} for tracking the current order created,an {@linkplain AppConstants.Navigation}
     * and {@code result.getId()} has been passed for identifying the order and action to be performed
     *
     * @param id     Order id for the order to be tracked
     */
    @Override
    public void navigateToTrackOrderFragment(String id) {

        finishAfterTransition();

        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.TRACK_ORDER);
        intent.putExtra(AppConstants.ORDER_ID, id);
        startActivity(intent);
    }


    /**
     * Callback for the result from requesting permissions.
     *
     * @param requestCode  The request code passed in
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof RecipientDetailsFragment) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (fragment instanceof AddItemFragment) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (fragment instanceof MapFragment) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (fragment instanceof CreateOrderFragment) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        QuickDelivery.getInstance().setCurrentActivity(this);
        checkForIsDriverAligned();
    }

    private void checkForIsDriverAligned() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof FindingDriverFragment) {
            ((FindingDriverFragment) fragment).checkForOrderStatus();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * Perform any final cleanup before an activity is destroyed.  This can
     * happen either because the activity is finishing (someone called
     * {@link #finish} on it, or because the system is temporarily destroying
     * this instance of the activity to save space.  You can distinguish
     * between these two scenarios with the {@link #isFinishing} method.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearReferences();
    }

    @Override
    protected void onPause() {
        super.onPause();
        clearReferences();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        PermissionHelper mPermissionHelper = new PermissionHelper(this);
        if (mPermissionHelper.hasPermission(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.PermissionConstants.REQUEST_LOCATION)) {
            permissionsGiven(AppConstants.PermissionConstants.REQUEST_LOCATION);
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof OrderSummaryFragment) {
            ((OrderSummaryFragment) fragment).checkForPaymentMode();
        }
    }

    /**
     * Opens {@link SelectVehicleFragment} to change the vehicle if the user has chosen an item that doesn't
     * suit the already selected vehicle or if he wants to change for any other reason
     *  @param vehicleTypeList       List of all available vehicles with selected vehicle
     * @param createOrderDetails   Create order request object with current order data
     */
    @Override
    public void openSelectVehicleType(ArrayList<VehicleCategory> vehicleTypeList, CreateOrderRequest createOrderDetails) {
        addFragmentWithBackStack(R.id.cl_root_view, SelectVehicleFragment.getInstance(vehicleTypeList, createOrderDetails), AddItemFragment.class.getName());
    }

    /**
     * Opens {@link SelectLocationFragment} to select location with any of types specified, i,e.,
     * {@link MapFragment}, {@link AdvancedSearchFragment} or {@link SavedAddressFragment}
     *
     * @param isForPickup       Specifies if the location is to selected for pickup or drop
     * @param pickupLocation    Pickup location selected
     * @param dropOffArr        Drop off location selected
     */
    @Override
    public void openSelectLocationFragment(boolean isForPickup, List<PickupLocation> pickupLocation, List<DropOffArr> dropOffArr) {
        addFragmentWithBackStack(R.id.cl_root_view, SelectLocationFragment.getInstance(isForPickup, pickupLocation != null ? pickupLocation.get(0) : null, dropOffArr != null ? dropOffArr.get(0) : null, false), SelectLocationFragment.class.getName());
    }


    /**
     * Opens {@link CreateOrderFragment} to select pick up and drop location initially, also the vehicle type
     * for the order
     *
     * @param orderType Type of the order, the user has selected to create
     */
    private void openCreateOrderFragment(String orderType) {
        addFragment(R.id.cl_root_view, CreateOrderFragment.getInstance(orderType), CreateOrderFragment.class.getName());
    }

    /**
     * Navigates to {@link RecipientDetailsFragment} for taking the recipient details corresponding to a particular delivery location
     *
     * @param orderDetails Updated {@link CreateOrderRequest} object containing all the updated data
     * @param pos          Position of the drop location whose recipient details is to be added
     */
    @Override
    public void navigateToRecipientDetailsFragment(CreateOrderRequest orderDetails, int pos) {
        addFragmentWithBackStack(R.id.cl_root_view, RecipientDetailsFragment.getInstance(orderDetails, pos), RecipientDetailsFragment.class.getName());
    }

    /**
     * Updates order summary fragment if the user has edited schedule timing from order summary page, in order to
     * reflect the updated timing on the summary
     *
     * @param createOrderDetails Updated {@link CreateOrderRequest} object containing all the updated data
     */
    @Override
    public void updateOrderSummaryFragment(CreateOrderRequest createOrderDetails) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof OrderSummaryFragment) {
            ((OrderSummaryFragment) fragment).updateScheduleTiming(createOrderDetails);
        }
    }

    /**
     * Opens {@link ServiceTypeDialogFragment} to specify the order schedule timings in case of vendor,
     * based on if the vendor has opted for flexible service or the premium one
     *
     * @param createOrderData Updated {@link CreateOrderRequest} object containing all the updated data
     */
    @Override
    public void openTimeSelectionFragment(CreateOrderRequest createOrderData, String navigation) {
        ServiceTypeDialogFragment serviceType = ServiceTypeDialogFragment.getInstance(createOrderData, navigation);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(serviceType, ServiceTypeDialogFragment.class.getName());
        ft.addToBackStack(ServiceTypeDialogFragment.class.getName());
        ft.commit();
    }

    @Override
    public void openCreatedOrders() {
        finishAfterTransition();

        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.ORDERS_LIST);
        startActivity(intent);
    }


    @Override
    public void openAdvancedSearchFragment(boolean isForPickup, Result addressFromMap, String navigation) {
        addFragmentWithBackStack(R.id.cl_root_view, AdvancedSearchFragment.getInstance(isForPickup, addressFromMap, navigation), AdvancedSearchFragment.class.getName());
    }

    /**
     * Navigates to {@link OrderSummaryFragment} for showing complete order details before creating an order
     *
     * @param createOrderDetails Complete {@link CreateOrderRequest} object for the order to be created
     */
    @Override
    public void navigateToOrderSummaryFragment(CreateOrderRequest createOrderDetails) {
        addFragmentWithBackStack(R.id.cl_root_view, OrderSummaryFragment.getInstance(createOrderDetails), OrderSummaryFragment.class.getName());
    }

    /**
     * Navigates to {@link DropLocationsListFragment} for adding more drops in case of multiple drops, and for showing
     * the same from {@link OrderSummaryFragment}
     *
     * @param createOrderDetails Updated {@link CreateOrderRequest} object containing all the updated data
     */
    @Override
    public void navigateToDropLocationList(CreateOrderRequest createOrderDetails) {
        addFragmentWithBackStack(R.id.cl_root_view, DropLocationsListFragment.getInstance(createOrderDetails, OrderSummaryFragment.class.getName()), DropLocationsListFragment.class.getName());
    }

    /**
     * Navigates to {@link TrackOrderActivity} for showing Order details of the order created, in case of flexible vendors
     * as drivers are not immediately assigned to them, an {@linkplain AppConstants.Navigation}
     * and {@code result.getId()} has been passed for identifying the order and action to be performed
     *
     * @param id Order id whose details are to be shown
     */
    @Override
    public void navigateToOrderDetails(String id) {
        finishAfterTransition();

        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.BUSINESS_ORDER_DETAIL);
        intent.putExtra(AppConstants.ORDER_ID, id);
        startActivity(intent);
    }

    /**
     * Navigates to {@link com.qd.user.ui.home.onlinepayment.OnlinePaymentFragment} if the user has selected to
     * pay online for the order that is going to create
     * @param totalDeliveryCharges total delivery charges of order being created
     * @param walletAmount          Existing wallet Amount
     * @param id                    orderId for online payment
     * @param paymentUrl            Url to initialize payment
     */
    @Override
    public void navigateForOnlinePayment(BigDecimal totalDeliveryCharges, BigDecimal walletAmount, String id, String paymentUrl) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(AppConstants.PAYMENT_REQUEST, createRequestToInitializePayment(totalDeliveryCharges, walletAmount, id));
        intent.putExtra(AppConstants.PAYMENT_URL, paymentUrl);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.ONLINE_PAYMENT);
        startActivityForResult(intent, AppConstants.PermissionConstants.REEQUEST_PAYMENT_STATUS);
    }

    /**
     * Creates an object for wallet recharge in case {@code totalDeliveryCharges} are greater than existing wallet amount
     * So, for adding required amount to wallet, online payment option is given to recharge wallet and hence create order
     *
     * @param totalDeliveryCharges Total delivery charges of order
     * @param walletAmount         Current wallet amount
     * @param id                   Order Id for Online payment
     * @return {@link InitializePaymentRequest} object to initialize wallet recharge
     */
    private InitializePaymentRequest createRequestToInitializePayment(BigDecimal totalDeliveryCharges, BigDecimal walletAmount, String id) {
        InitializePaymentRequest initializePaymentRequest = new InitializePaymentRequest();
        if (walletAmount != null) {
            initializePaymentRequest.setTransactionFor(AppConstants.TransactionFor.WALLET_RECHARGE);
            initializePaymentRequest.setPrice(totalDeliveryCharges.subtract(walletAmount).doubleValue() + AppConstants.MIN_WALLET_AMOUNT);
        } else {
            initializePaymentRequest.setTransactionFor(AppConstants.TransactionFor.ORDER);
            initializePaymentRequest.setPrice(totalDeliveryCharges.doubleValue());
            initializePaymentRequest.setOrderId(id);
        }

        return initializePaymentRequest;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == AppConstants.PermissionConstants.REEQUEST_PAYMENT_STATUS && data != null) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
            if (fragment instanceof OrderSummaryFragment) {
                ((OrderSummaryFragment) fragment).updatePaymentStatus(data.getBooleanExtra(AppConstants.PAYMENT_STATUS, false), data.getStringExtra(AppConstants.ORDER_ID));
            }
        }
    }

    /**
     * Navigates to {@link FindingDriverFragment} for finding the Driver in case of cash client and premium
     * type vendors
     *
     * @param result {@link Result} object containing data of order recently created
     */
    @Override
    public void navigateToFindingDriverScreen(com.qd.user.model.createorder.response.Result result) {
        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onPostResume();
            getSupportFragmentManager().popBackStackImmediate();
        }
        addFragment(R.id.cl_root_view, FindingDriverFragment.getInstance(result), FindingDriverFragment.class.getName());
    }

    /**
     * Navigates back to recipient details fragment, in case if user has edited drop location through
     * any of map, advanced search or from saved addresses
     *
     * @param completeAddress Updated address user has selected
     */
    @Override
    public void backToRecipientDetails(SaveAddressRequest completeAddress) {
        while (!(getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof RecipientDetailsFragment)) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof RecipientDetailsFragment) {
            ((RecipientDetailsFragment) fragment).updateLocation(completeAddress);
        }

    }

    @Override
    public void decreaseNotificationCount() {

    }

    /**
     * Opens {@link SelectLocationFragment} to edit recipient address from {@link RecipientDetailsFragment}
     *
     * @param pickupLocation    Pickup location whose details are filled
     * @param dropOffArr        Drop off location whose recipient address is to updated
     */
    @Override
    public void editRecipientAddress(PickupLocation pickupLocation, DropOffArr dropOffArr) {
        addFragmentWithBackStack(R.id.cl_root_view, SelectLocationFragment.getInstance(pickupLocation != null, pickupLocation, dropOffArr, true), SelectLocationFragment.class.getName());
    }

    /**
     * Navigates to {@link CreateOrderFragment} to fill the location user has selected, based on if
     * the user has selected that particular location for pickup or drop {@literal isForPickup}
     *
     * @param isForPickup     Specifies if the location is corresponding to pickup or drop
     * @param completeAddress Complete address that has been selected by the user
     */
    @Override
    public void navigateToCreateOrderScreen(boolean isForPickup, SaveAddressRequest completeAddress) {
        while (!(getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof CreateOrderFragment)) {
            onBackPressed();
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof CreateOrderFragment) {
            ((CreateOrderFragment) fragment).setLocation(isForPickup, completeAddress);
        }
    }

    /**
     * Updates add item fragment after the user has selected appropriate vehicle
     * according to the items in the order
     *  @param vehicle           Details of the vehicle user has selected
     * @param deliveryCharges   Delivery charges of updated vehicle
     */
    @Override
    public void backToAddItemFragment(VehicleCategory vehicle, BigDecimal deliveryCharges) {
        onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof AddItemFragment) {
            ((AddItemFragment) fragment).updateVehicleType(vehicle, deliveryCharges);
        }
    }

    /**
     * Moves to {@link OnBoardActivity} in case if user has not yet been registered with Quick Delivery platform,
     * and wants to create an order so he should first register himself hence navigating him to {@link com.qd.user.ui.onboard.welcome.WelcomeFragment}
     * through specifying {@literal AppConstants.ACTION_NAVIGATION}
     *
     * @param createOrderDetails Order details that should persists after registering to create the order
     */
    @Override
    public void moveToOnBoard(CreateOrderRequest createOrderDetails) {
        Intent intent = new Intent(this, OnBoardActivity.class);
        intent.putExtra(AppConstants.CREATE_ORDER_DETAILS, createOrderDetails);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_WELCOME);
        startActivity(intent);
    }

    /**
     * Opens Notification fragment when bell icon is pressed from top right corner on {@link CreateOrderFragment}
     */
    @Override
    public void openNotificationsFragment() {
        addFragmentWithBackStack(R.id.cl_root_view, NotificationsFragment.getInstance(), NotificationsFragment.class.getName());
    }

    @Override
    public void onBackPressedOnDataEntered() {
        super.onBackPressed();
    }

    /**
     * Called when back press is detected
     * Specifies the action to be performed on back press either from app or the device
     */
    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
        if (fragment instanceof DropLocationsListFragment) {
            if (((DropLocationsListFragment) Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.cl_root_view))).
                    getActionNavigation().equals(OrderSummaryFragment.class.getName())) {
                super.onBackPressed();
            } else {
                while (!(getSupportFragmentManager().findFragmentById(R.id.cl_root_view) instanceof CreateOrderFragment)) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.cl_root_view);
                if (fragment1 instanceof CreateOrderFragment) {
                    ((CreateOrderFragment) fragment1).resetDrop();
                }
            }
        } else if (fragment instanceof AddItemFragment &&
                getSupportFragmentManager().findFragmentByTag(DropLocationsListFragment.class.getName()) instanceof DropLocationsListFragment && count == 0) {
            count++;
            mCreateOrderDetails.getDropOffArr().remove(mCreateOrderDetails.getDropOffArr().size() - 1);
            super.onBackPressed();
            count = 0;
        } else if (fragment instanceof CreateOrderFragment) {
            ((CreateOrderFragment) fragment).onBackPressed();
        } else
            super.onBackPressed();
    }

    @Override
    public void navigateToBusinessOrderDetail(String orderId) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.BUSINESS_ORDER_DETAIL);
        startActivity(intent);
    }

    /**
     * Navigates to Order details fragment by passing an intent to
     * {@link TrackOrderActivity}
     *
     * @param orderId         Order Id corresponding to which order details has to be shown
     * @param isOrderReceived true, if to open the details in context to receiver, false otherwise
     */
    @Override
    public void navigateToOrderDetail(String orderId, boolean isOrderReceived) {
        Intent intent = new Intent(this, TrackOrderActivity.class);
        intent.putExtra(AppConstants.ORDER_ID, orderId);
        intent.putExtra(AppConstants.IS_ORDER_RECEIVED, isOrderReceived);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.ORDER_DETAIL);
        startActivity(intent);
    }

    @Override
    public void permissionsGiven(int requestCode) {

    }
}
