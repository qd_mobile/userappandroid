package com.qd.user.ui.createorder.selectlocation.savedaddress;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.savedaddress.Result;
import com.qd.user.model.failureresponse.FailureResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SavedAddressFragment extends BaseFragment implements SavedAddressAdapter.SavedAddressAdapterInterface, RecentAddressAdapter.RecentAddressAdapterInterface {
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.cl_search)
    ConstraintLayout clSearch;
    @BindView(R.id.rv_fav_address)
    RecyclerView rvFavAddress;
    @BindView(R.id.tv_label_recent)
    TextView tvLabelRecent;
    @BindView(R.id.rv_recent_address)
    RecyclerView rvRecentAddress;
    @BindView(R.id.nsv_address)
    NestedScrollView nsvAddress;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    private Unbinder unbinder;
    private ISavedAddressHost mHost;
    private SavedAddressViewModel mSavedAddressViewModel;
    private ArrayList<Result> mFavAddressList;
    private SavedAddressAdapter mSavedAddressAdapter;

    private String TAG = SavedAddressFragment.class.getName();
    private ArrayList<Result> mRecentAddressList;
    private RecentAddressAdapter mRecentAddressAdapter;
    private boolean mIsForPickup;
    private String mSearchString;
    private boolean isRefreshing;
    private boolean mIsFromRecipientDetails;
    private Result mSelectedAddress;

    public SavedAddressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saved_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static SavedAddressFragment getInstance(boolean isForPickup, Parcelable selectedAddress, boolean isFromRecipientDetails) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(NetworkConstants.KEY_IS_FOR_PICKUP, isForPickup);
        bundle.putBoolean(AppConstants.IS_FROM_RECIPIENT_DETAILS, isFromRecipientDetails);
        bundle.putParcelable(AppConstants.SELECTED_ADDRESS, selectedAddress);
        SavedAddressFragment savedAddressFragment = new SavedAddressFragment();
        savedAddressFragment.setArguments(bundle);
        return savedAddressFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ISavedAddressHost) {
            mHost = (ISavedAddressHost) context;
        } else throw new IllegalStateException("Host must implement ISavedAddressHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSavedAddressViewModel = new ViewModelProvider(this).get(SavedAddressViewModel.class);
        mSavedAddressViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();

        mSavedAddressViewModel.getSavedAddresses(mSearchString);

    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            mIsFromRecipientDetails = getArguments().getBoolean(AppConstants.IS_FROM_RECIPIENT_DETAILS);
            mIsForPickup = getArguments().getBoolean(NetworkConstants.KEY_IS_FOR_PICKUP);
            mSelectedAddress = getArguments().getParcelable(AppConstants.SELECTED_ADDRESS);
        }
    }

    private void observeLiveData() {
        mSavedAddressViewModel.getSavedAddressLiveData().observe(getViewLifecycleOwner(), savedAddressResponse -> {

            if (savedAddressResponse != null) {
                if (isRefreshing) {
                    isRefreshing = false;
                    swipeRefresh.setRefreshing(false);
                }
                if (savedAddressResponse.getResult().size() > 0) {

                    tvNoList.setVisibility(View.GONE);
                    clSearch.setVisibility(View.VISIBLE);
                    nsvAddress.setVisibility(View.VISIBLE);

                    mFavAddressList.clear();
                    mRecentAddressList.clear();

                    for (int i = 0; i < savedAddressResponse.getResult().size(); i++) {
                        if (savedAddressResponse.getResult().get(i).getIsFavourite() != null && savedAddressResponse.getResult().get(i).getIsFavourite()) {
                            mFavAddressList.add(savedAddressResponse.getResult().get(i));
                        } else {
                            mRecentAddressList.add(savedAddressResponse.getResult().get(i));
                        }
                    }

                    if (mFavAddressList.size() > 0) {
                        mSavedAddressAdapter.notifyDataSetChanged();
                    }

                    if (mRecentAddressList.size() > 0) {
                        tvLabelRecent.setVisibility(View.VISIBLE);
                        mRecentAddressAdapter.notifyDataSetChanged();
                    } else {
                        tvLabelRecent.setVisibility(View.GONE);
                    }

                } else showViewsForDataUnavailable();
            }
        });

    }

    private void showViewsForDataUnavailable() {
        nsvAddress.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
        tvNoList.setText(R.string.s_no_data_found);
    }

    @Override
    protected void initViewsAndVariables() {
        mSearchString = "";
        mFavAddressList = new ArrayList<>();
        mRecentAddressList = new ArrayList<>();

        tvLabelRecent.setVisibility(View.GONE);
        clSearch.setVisibility(View.GONE);

        getArgumentsData();
        setUpAdapter();
        setUpListeners();
    }

    private void setUpListeners() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mSearchString = s.toString();
                    mSavedAddressViewModel.getSavedAddresses(mSearchString);
                } else {
                    mSavedAddressViewModel.getSavedAddresses("");
                }

            }
        });

        setRefreshLayoutListener();

    }

    /*
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        // This method performs the actual data-refresh operation.
// The method calls setRefreshing(false) when it's finished.
        swipeRefresh.setOnRefreshListener(
                this::refreshList
        );
    }

    /**
     * Refreshes list
     */
    private void refreshList() {
        isRefreshing = true;
        mSearchString = "";
        etSearch.setText("");
        mSavedAddressViewModel.getSavedAddresses(mSearchString);
    }

    private void setUpAdapter() {
        mSavedAddressAdapter = new SavedAddressAdapter(mFavAddressList, this);
        rvFavAddress.setLayoutManager(new LinearLayoutManager(rvFavAddress.getContext(), LinearLayoutManager.VERTICAL, false));
        rvFavAddress.setAdapter(mSavedAddressAdapter);
        rvRecentAddress.setAdapter(mSavedAddressAdapter);

        mRecentAddressAdapter = new RecentAddressAdapter(mRecentAddressList, this);
        rvRecentAddress.setLayoutManager(new LinearLayoutManager(rvFavAddress.getContext(), LinearLayoutManager.VERTICAL, false));
        rvRecentAddress.setAdapter(mRecentAddressAdapter);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void updateSavedAddress(Result savedAddress) {
        if (mFavAddressList != null && savedAddress != null) {
            mFavAddressList.add(0, savedAddress);
            mSavedAddressAdapter.notifyItemInserted(0);
        }

    }

    @Override
    public void onSavedAddressClicked(Result result) {
        if (mIsFromRecipientDetails) {
            if (mSelectedAddress != null) {
                mHost.backToRecipientDetails(getCompleteAddress(result, true));
            } else
                mHost.openAddItemFragment(getCompleteAddress(result, true));
        } else {
            mHost.navigateToCreateOrderScreen(mIsForPickup, getCompleteAddress(result, true));
        }
    }

    private SaveAddressRequest getCompleteAddress(Result result, boolean isFav) {
        SaveAddressRequest savedAddress = new SaveAddressRequest();
        savedAddress.setFullAddress(result.getSavedLocation().getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        savedAddress.setGovernorate(result.getSavedLocation().getGovernorate());
        savedAddress.setArea(result.getSavedLocation().getArea());
        savedAddress.setBlockNumber(result.getSavedLocation().getBlockNumber());
        savedAddress.setStreet(result.getSavedLocation().getStreet());
        savedAddress.setAvenue(result.getSavedLocation().getAvenue());
        savedAddress.setHouseOrbuilding(result.getSavedLocation().getHouseOrbuilding());
        savedAddress.setFloor(result.getSavedLocation().getFloor());
        savedAddress.setApartmentOrOffice(result.getSavedLocation().getApartmentOrOffice());
        savedAddress.setGeometry(result.getSavedLocation().getGeometry());
        savedAddress.setIsFavourite(isFav);
        savedAddress.setName(result.getSavedLocation().getName());

        return savedAddress;

    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);

        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();

        isRefreshing = false;
        swipeRefresh.setRefreshing(false);

        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        nsvAddress.setVisibility(View.GONE);
        clSearch.setVisibility(View.GONE);
    }

    @Override
    protected void onErrorOccurred(Throwable throwable) {
        super.onErrorOccurred(throwable);
    }

    @Override
    public void onRecentAddressClicked(Result result) {
        if (mIsFromRecipientDetails) {
            if (mSelectedAddress != null) {
                mHost.backToRecipientDetails(getCompleteAddress(result, false));
            } else
                mHost.openAddItemFragment(getCompleteAddress(result, false));
        } else {
            mHost.navigateToCreateOrderScreen(mIsForPickup, getCompleteAddress(result, false));
        }
    }

    @OnClick(R.id.iv_search)
    public void onViewClicked() {
        mSavedAddressViewModel.getSavedAddresses(etSearch.getText().toString());
    }


    public interface ISavedAddressHost {
        void onBackPressed();

        void navigateToCreateOrderScreen(boolean mIsForPickup, SaveAddressRequest completeAddress);

        void backToRecipientDetails(SaveAddressRequest completeAddress);

        void openAddItemFragment(SaveAddressRequest completeAddress);
    }
}
