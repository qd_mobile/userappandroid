package com.qd.user.ui.onboard.createaccount;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.failureresponse.FailureResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

class CreateAccountRepository {
    private void hitSignUpApi(final RichMediatorLiveData<CreateAccountResponse> mSignUpLiveData, final CreateAccountInfo createAccountInfo) {
        DataManager.getInstance().hitSignUpApi(createRequestPayloadForSignUp(createAccountInfo)).enqueue(new NetworkCallback<CreateAccountResponse>() {
            @Override
            public void onSuccess(CreateAccountResponse createAccountResponse) {
                setUserDataInPreferences(createAccountResponse);
                mSignUpLiveData.setValue(createAccountResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mSignUpLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mSignUpLiveData.setError(t);
            }
        });

    }

    //set user info in preference to show it on profile of that user
    private void setUserDataInPreferences(CreateAccountResponse createAccountResponse) {
        DataManager.getInstance().setUserLoggedIn(true);
        DataManager.getInstance().setServiceType(createAccountResponse.getResult().getUserDetails().getServicesOffered());
        DataManager.getInstance().setSignUpType(createAccountResponse.getResult().getUserDetails().getSignupType());
        DataManager.getInstance().setUserId(createAccountResponse.getResult().getUserDetails().getId());
        DataManager.getInstance().setRegistrationDateAndTime(createAccountResponse.getResult().getUserDetails().getCreatedAt());
        DataManager.getInstance().setAccessToken(createAccountResponse.getResult().getAuthenticationToken());
        DataManager.getInstance().setUserName(createAccountResponse.getResult().getUserDetails().getName());
        DataManager.getInstance().setCountryCode(createAccountResponse.getResult().getUserDetails().getContact().getCcPersonal());
        DataManager.getInstance().setPhoneNumber(createAccountResponse.getResult().getUserDetails().getContact().getPersonal());
        DataManager.getInstance().setEmailAddress(createAccountResponse.getResult().getUserDetails().getEmail());
        DataManager.getInstance().setUserType(createAccountResponse.getResult().getUserDetails().getCashClientStatus());
        DataManager.getInstance().setVendorStatus(createAccountResponse.getResult().getUserDetails().getStatus());
        DataManager.getInstance().setCashClientCurrentStatus(createAccountResponse.getResult().getUserDetails().getCashClientCurrentStatus());
        DataManager.getInstance().setCurrentStatus(createAccountResponse.getResult().getUserDetails().getCurrentStatus());

    }


    private JSONObject createRequestPayloadForSignUp(CreateAccountInfo createAccountInfo) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(NetworkConstants.KEY_FIRST_NAME, createAccountInfo.getFirstName());
            jsonObject.put(NetworkConstants.KEY_LAST_NAME, createAccountInfo.getLastName());
            jsonObject.put(NetworkConstants.KEY_EMAIL, createAccountInfo.getEmail());
            jsonObject.put(NetworkConstants.KEY_PASSWORD, createAccountInfo.getPassword());
            jsonObject.put(NetworkConstants.KEY_RE_PASSWORD, createAccountInfo.getConfirmPassword());
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(NetworkConstants.KEY_CC_PERSONAL, AppConstants.COUNTRY_CODE);
            jsonObject1.put(NetworkConstants.KEY_PERSONAL, createAccountInfo.getPhoneNumber());
            jsonObject.put(NetworkConstants.KEY_DEVICE_TOKEN, DataManager.getInstance().getDeviceToken());
            jsonObject.put(NetworkConstants.KEY_CONTACT, jsonObject1);
            jsonObject.put(NetworkConstants.KEY_DEVICE_ID, createAccountInfo.getDeviceId());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    void getFireBaseToken(final RichMediatorLiveData<CreateAccountResponse> signUpLiveData, final CreateAccountInfo createAccountInfo, final boolean isForSocialLogin) {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("Failed", "getInstanceId failed", task.getException());
                        signUpLiveData.setFailure(new FailureResponse(9, "Please try again later"));
                        return;
                    }

                    // Get new Instance ID token
                    if (task.getResult() != null && !task.getResult().getToken().isEmpty()) {
                        DataManager.getInstance().setDeviceToken(task.getResult().getToken());
                        if (isForSocialLogin)
                            onUpdateSocialSignUp(signUpLiveData, createAccountInfo);
                        else
                            hitSignUpApi(signUpLiveData, createAccountInfo);
                    }
                });
    }



    private HashMap<String, String> createSocialSignUpObject(CreateAccountInfo createAccountInfo) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NetworkConstants.KEY_SOCIAL_PLATFORM, createAccountInfo.getPlatform());
        hashMap.put(NetworkConstants.KEY_APP_ID, createAccountInfo.getAppId());
        hashMap.put(NetworkConstants.KEY_CC_PERSONAL, createAccountInfo.getCountryCode());
        hashMap.put(NetworkConstants.KEY_PERSONAL, createAccountInfo.getPhoneNumber());
        hashMap.put(NetworkConstants.KEY_DEVICE_ID, createAccountInfo.getDeviceId());
        hashMap.put(NetworkConstants.KEY_PLATFORM, AppConstants.ANDROID);
        hashMap.put(NetworkConstants.KEY_DEVICE_TOKEN, DataManager.getInstance().getDeviceToken());
        hashMap.put(NetworkConstants.KEY_EMAIL, createAccountInfo.getEmail());
        hashMap.put(NetworkConstants.KEY_FIRST_NAME, createAccountInfo.getFirstName());
        if (createAccountInfo.getLastName() != null)
            hashMap.put(NetworkConstants.KEY_LAST_NAME, createAccountInfo.getLastName());
        return hashMap;

    }


    private void onUpdateSocialSignUp(final RichMediatorLiveData<CreateAccountResponse> mSignUpLiveData, CreateAccountInfo createAccountInfo) {
        DataManager.getInstance().hitSocialSignUpApi(createSocialSignUpObject(createAccountInfo)).enqueue(new NetworkCallback<CreateAccountResponse>() {
            @Override
            public void onSuccess(CreateAccountResponse createAccountResponse) {
                setUserDataInPreferences(createAccountResponse);
                mSignUpLiveData.setValue(createAccountResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mSignUpLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mSignUpLiveData.setError(t);
            }
        });

    }

    void setIsUserVerified(boolean isUserVerified) {
        DataManager.getInstance().setUserVerified(isUserVerified);
    }
}
