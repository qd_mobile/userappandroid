package com.qd.user.ui.createorder.recipientdetails;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.maps.autocomplete.PlacesAutoCompleteResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

import java.util.regex.Pattern;

public class RecipientDetailsViewModel extends ViewModel {
    private RichMediatorLiveData<PlacesAutoCompleteResponse> mPlacesAutoCompleteLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private RecipientRepository mRepo = new RecipientRepository();
    private RichMediatorLiveData<OptimizedDeliveryResponse> mOptimizedDeliveryLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    String getUserName() {
        return mRepo.getUserName();
    }

    public String getEmail() {
        return mRepo.getEmail();
    }

    String getMobileNumber() {
        return mRepo.getMobileNumber();
    }

    boolean checkValidation(String name, String contact, String email) {
        if (name.isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_NAME, R.string.s_please_enter_name));
            return false;
        } /*else if (!Pattern.matches("[a-zA-Z ]+", name)) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_FIRST_NAME, R.string.s_please_enter_a_valid_name));
            return false;
        }*/ else if (!contact.matches("[0-9]{7,9}$")) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER, R.string.s_please_enter_valid_phone_number));
            return false;
        } else if (!email.isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_please_enter_valid_email_address));
            return false;
        }

        return true;
    }

    private void initLiveData() {
        if (mPlacesAutoCompleteLiveData == null) {
            mPlacesAutoCompleteLiveData = new RichMediatorLiveData<PlacesAutoCompleteResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }

        if (mOptimizedDeliveryLiveData == null) {
            mOptimizedDeliveryLiveData = new RichMediatorLiveData<OptimizedDeliveryResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }


    public RichMediatorLiveData<OptimizedDeliveryResponse> getOptimizedDeliveryLiveData() {
        return mOptimizedDeliveryLiveData;
    }

    public String getContact() {
        return mRepo.getContact();
    }

    boolean checkSenderDetailsValidation(String name, String number) {
        if (name.isEmpty()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_SENDER_NAME, R.string.s_please_enter_name));
            return false;
        } else if (!Pattern.matches("[a-zA-Z ]+", name)) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_SENDER_NAME, R.string.s_please_enter_a_valid_name));
            return false;
        } else if (!number.matches("[0-9]{7,9}$")) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_SENDER_CONTACT, R.string.s_please_enter_valid_phone_number));
            return false;
        }

        return true;
    }

    public void getDeliveryCharges(OptimizedDeliveryRequest dataForOptimizedRoute) {
        mOptimizedDeliveryLiveData.setLoadingState(true);
        mRepo.getDeliveryCharges(mOptimizedDeliveryLiveData, dataForOptimizedRoute);
    }
}