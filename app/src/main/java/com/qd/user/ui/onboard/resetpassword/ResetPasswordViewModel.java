package com.qd.user.ui.onboard.resetpassword;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;

public class ResetPasswordViewModel extends ViewModel {
    private RichMediatorLiveData<CommonResponse> mResetPasswordLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private ResetPasswordRepository mRepo = new ResetPasswordRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mResetPasswordLiveData == null) {
            mResetPasswordLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if (mValidateLiveData == null) {
                mValidateLiveData = new MutableLiveData<>();
            }
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the login live data object to {@link ResetPasswordFragment}
     *
     * @return {@link #mResetPasswordLiveData}
     */
    public RichMediatorLiveData<CommonResponse> getResetPasswordLiveData() {
        return mResetPasswordLiveData;
    }

    public void onSubmitViewClick(String newPassword, String confirmPassword, String token) {
        if (validateResetPasswordFields(newPassword, confirmPassword)) {
            mRepo.hitResetPasswordApi(newPassword, mResetPasswordLiveData, token);
        }
    }

    private boolean validateResetPasswordFields(String newPassword, String confirmPassword) {
        if (newPassword.length() < 8 || newPassword.length() > 16) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_NEW_PASSWORD, R.string.s_password_should_be_between_characters));
            return false;
        } else if (!confirmPassword.equals(newPassword)) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_CONFIRM_NEW_PASSWORD, R.string.s_password_does_not_match));
            return false;
        }
        return true;
    }
}
