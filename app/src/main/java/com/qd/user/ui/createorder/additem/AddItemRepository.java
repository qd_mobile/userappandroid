package com.qd.user.ui.createorder.additem;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.additem.itemstype.ItemsTypeListResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.failureresponse.FailureResponse;

class AddItemRepository {

    void getItemTypes(final RichMediatorLiveData<ItemsTypeListResponse> itemsListLiveData) {
        DataManager.getInstance().getItemTypes().enqueue(new NetworkCallback<ItemsTypeListResponse>() {
            @Override
            public void onSuccess(ItemsTypeListResponse itemsTypeListResponse) {
                itemsListLiveData.setLoadingState(false);
                itemsListLiveData.setValue(itemsTypeListResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                itemsListLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                itemsListLiveData.setError(t);
            }
        });

    }

    public void getVehicleCategories(final RichMediatorLiveData<VehicleEtaResponse> vehicleCategoryLiveData, String orderType, Double lat, Double lng) {
        DataManager.getInstance().getAvailableVehicles(orderType, lat, lng, DataManager.getInstance().getUserId()).enqueue(new NetworkCallback<VehicleEtaResponse>() {
            @Override
            public void onSuccess(VehicleEtaResponse vehicleCategoryResponse) {
                vehicleCategoryLiveData.setValue(vehicleCategoryResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                vehicleCategoryLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                vehicleCategoryLiveData.setError(t);
            }
        });
    }

    String getVendorPaymentMode() {
        return DataManager.getInstance().getVendorPaymentMode();
    }
}
