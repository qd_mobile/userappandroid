package com.qd.user.ui.createorder.additem;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.additem.ItemDetails;
import com.qd.user.model.createorder.additem.itemstype.ItemsTypeListResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;
import com.qd.user.ui.createorder.CreateOrderFragment;


public class AddItemViewModel extends ViewModel {

    private RichMediatorLiveData<ItemsTypeListResponse> mItemsListLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;
    private RichMediatorLiveData<VehicleEtaResponse> mVehicleCategoryLiveData;



    private AddItemRepository mRepo = new AddItemRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mItemsListLiveData == null) {
            mItemsListLiveData = new RichMediatorLiveData<ItemsTypeListResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mVehicleCategoryLiveData == null) {
            mVehicleCategoryLiveData = new RichMediatorLiveData<VehicleEtaResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the ItemsList live data object to {@link AddItemFragment}
     *
     * @return {@link #mItemsListLiveData}
     */
    RichMediatorLiveData<ItemsTypeListResponse> getItemsListLiveData() {
        return mItemsListLiveData;
    }

    /**
     * This method gives the CreateOrder live data object to {@link CreateOrderFragment}
     *
     * @return {@link #mVehicleCategoryLiveData}
     */
    RichMediatorLiveData<VehicleEtaResponse> getVehicleCategoryLiveData() {
        return mVehicleCategoryLiveData;
    }


    void getItemTypes() {
        // mItemsListLiveData.setLoadingState(true);
        mRepo.getItemTypes(mItemsListLiveData);
    }

    boolean checkValidations(ItemDetails item, String paymentMode, int i, String orderType) {
        if (TextUtils.isEmpty(item.getItemDescription()) && !orderType.equals(AppConstants.OrderType.BUSINESS)) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ITEM_DESCRIPTION, R.string.s_please_enter_item_description));
            return false;
        } else if (TextUtils.isEmpty(item.getItemType())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ITEM_TYPE, R.string.s_please_enter_item_type));
            return false;
        }/* else if (i == 0) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ITEM_IMAGES, R.string.s_please_select_image));
            return false;
        } else if (orderType.equals(AppConstants.OrderType.SPECIAL_GET) && item.getItemPrice().compareTo(BigDecimal.valueOf(AppConstants.MIN_ITEM_PRICE_FOR_SPECIAL_ORDERS)) > 0) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ITEM_PRICE, R.string.s_service_not_available));
            return false;
        }*/ else if (item.getItemPrice() == null) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_ITEM_PRICE, R.string.s_please_enter_item_price));
            return false;
        } else if (orderType.equals(AppConstants.OrderType.BUSINESS) && paymentMode == null) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PAYMENT_MODE, R.string.s_please_select_payment_mode));
            return false;
        }
         /*else if (item.getItemPrice() != null && Integer.valueOf(item.getItemPrice()) > 60 && !isTimeForPaymentSelected) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_TIME_OF_PAYMENT, "Please select time for payment"));
            return false;
        }*/
        return true;
    }

    void getVehicleTypes(String orderType, Double lat, Double lng) {
        mRepo.getVehicleCategories(mVehicleCategoryLiveData, orderType, lat, lng);
    }

   /* public String getVendorPaymentMode() {
        return mRepo.getVendorPaymentMode();
    }*/
}
