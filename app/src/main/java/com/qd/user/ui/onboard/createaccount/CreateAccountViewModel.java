package com.qd.user.ui.onboard.createaccount;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.validationerror.ValidationErrors;
import com.qd.user.ui.onboard.login.LoginFragment;

public class CreateAccountViewModel extends ViewModel {
    private RichMediatorLiveData<CreateAccountResponse> mSignUpLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;


    private CreateAccountRepository mRepo = new CreateAccountRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mSignUpLiveData == null) {
            mSignUpLiveData = new RichMediatorLiveData<CreateAccountResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if (mValidateLiveData == null) {
                mValidateLiveData = new MutableLiveData<>();
            }
        }
        /*if (mSocialLogInLiveData == null) {
            mSocialLogInLiveData = new RichMediatorLiveData<SocialLoginResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

            if (mValidateLiveData == null) {
                mValidateLiveData = new MutableLiveData<>();
            }
        }*/
    }

    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }

    /**
     * This method gives the login live data object to {@link LoginFragment}
     *
     * @return {@link #mSignUpLiveData}
     */
    public RichMediatorLiveData<CreateAccountResponse> getSignUpLiveData() {
        return mSignUpLiveData;
    }

    /**
     * This method gives the login live data object to {@link CreateAccountFragment}
     *
     * @return {@link #mSignUpLiveData}
     */
    public RichMediatorLiveData<CreateAccountResponse> getSocialSignUpLiveData() {
        return mSignUpLiveData;
    }




    private boolean validateInputFieldsForSignUp(CreateAccountInfo createAccountInfo, boolean isForSocialLogin) {
        /*if (!Pattern.matches("[a-zA-Z ]+", createAccountInfo.getFirstName())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_FIRST_NAME, R.string.s_please_enter_valid_first_name));
            return false;
        } else if (!Pattern.matches("[a-zA-Z ]+", createAccountInfo.getLastName())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_LAST_NAME, R.string.s_please_enter_valid_last_name));
            return false;
        } else*/
        if (!Patterns.EMAIL_ADDRESS.matcher(createAccountInfo.getEmail()).matches()) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_EMAIL, R.string.s_invalid_email));
            return false;
        } else if (!isForSocialLogin) {
            if (createAccountInfo.getPassword().length() < 8 || createAccountInfo.getPassword().length() > 16) {
                mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PASSWORD, R.string.s_password_should_be_between_characters));
                return false;
            }
            if (!createAccountInfo.getConfirmPassword().equals(createAccountInfo.getPassword())) {
                mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_CONFIRM_PASSWORD, R.string.s_password_does_not_match));
                return false;
            }
        }
        if (!createAccountInfo.getPhoneNumber().matches("[0-9]{7,9}$")) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER, R.string.s_please_enter_valid_phone_number));
            return false;
        }

        return true;
    }


    void onCreateAccountViewClicked(CreateAccountInfo createAccountInfo, boolean isForSocialLogin) {
        if (validateInputFieldsForSignUp(createAccountInfo, isForSocialLogin)) {
            mRepo.getFireBaseToken(mSignUpLiveData, createAccountInfo, isForSocialLogin);
        }
    }

    void setIsUserVerified(boolean isUserVerified) {
        mRepo.setIsUserVerified(isUserVerified);
    }
}
