package com.qd.user.ui.home.applyvendor;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.vendor.becomeavendor.BecomeAVendorInfo;
import com.qd.user.ui.home.changepassword.ChangePasswordFragment;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.CustomEditText;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class BecomeAVendorFragment extends BaseFragment implements AppUtils.ISuccess, TextWatcher, AdapterView.OnItemSelectedListener {


    private static final int EXISTING_EMAIL_FAILURE = 42215;
    private Unbinder unbinder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_toolbar_tittle)
    TextView tvToolbarTittle;
    @BindView(R.id.iv_report_issue)
    ImageView ivReportIssue;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.view_toolbar)
    View viewToolbar;
    @BindView(R.id.tv_label_apply_to_be_a_vendor)
    TextView tvLabelApplyToBeAVendor;
    @BindView(R.id.et_company_name)
    EditText etCompanyName;
    @BindView(R.id.til_company_name)
    TextInputLayout tilCompanyName;
    @BindView(R.id.view_company_name)
    View viewCompanyName;
    @BindView(R.id.tv_error_company_name)
    TextView tvErrorCompanyName;
    @BindView(R.id.tv_label_bussiness_type)
    TextView tvLabelBusinessType;
    @BindView(R.id.sp_business_type)
    Spinner spBusinessType;
    @BindView(R.id.view_business_type)
    View viewBusinessType;
    @BindView(R.id.tv_error_business_type)
    TextView tvErrorBusinessType;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.view_address)
    View viewAddress;
    @BindView(R.id.tv_error_address)
    TextView tvErrorAddress;
    @BindView(R.id.et_contact_person)
    EditText etContactPerson;
    @BindView(R.id.til_contact_person)
    TextInputLayout tilContactPerson;
    @BindView(R.id.view_contact_person)
    View viewContactPerson;
    @BindView(R.id.tv_error_contact_person)
    TextView tvErrorContactPerson;
    @BindView(R.id.tv_country_code)
    TextView tvCountryCode;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.til_phone_number)
    TextInputLayout tilPhoneNumber;
    @BindView(R.id.view_country_code)
    View viewCountryCode;
    @BindView(R.id.view_phone_number)
    View viewPhoneNumber;
    @BindView(R.id.tv_error_number)
    TextView tvErrorNumber;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.cl_recipient_details)
    ConstraintLayout clRecipientDetails;
    @BindView(R.id.sv_recipient_details)
    ScrollView svRecipientDetails;
    @BindView(R.id.et_company_profile)
    CustomEditText etCompanyProfile;
    @BindView(R.id.tv_label_contact_details)
    TextView tvLabelContactDetails;
    private BecomeAVendorViewModel mBecomeAVendorViewModel;
    private IBecomeAVendorHost mHost;
    private String mBusinessType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_become_a_vendor, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBecomeAVendorViewModel = new ViewModelProvider(this).get(BecomeAVendorViewModel.class);
        mBecomeAVendorViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setListeners();
        setViews();

    }

    /**
     * show pre filled email id in case user is cash client and make it non editable else if user is guest make it editable
     */
    private void setViews() {
        if (mBecomeAVendorViewModel.getUserType() != null) {
            etEmail.setText(mBecomeAVendorViewModel.getEmailId());
            etPhoneNumber.setText(mBecomeAVendorViewModel.getPhoneNumber());
            etContactPerson.setText(mBecomeAVendorViewModel.getName());
            etEmail.setEnabled(false);
            etPhoneNumber.setEnabled(false);
            etContactPerson.setEnabled(false);
        } else {
            etEmail.setEnabled(true);
            etPhoneNumber.setEnabled(true);
            etContactPerson.setEnabled(true);
        }
    }

    private void setListeners() {
        etEmail.addTextChangedListener(this);
        etAddress.addTextChangedListener(this);
        etCompanyName.addTextChangedListener(this);
        etPhoneNumber.addTextChangedListener(this);
        etContactPerson.addTextChangedListener(this);
        etCompanyProfile.addTextChangedListener(this);
        spBusinessType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    mBusinessType = spBusinessType.getItemAtPosition(i).toString();
                } else {
                    mBusinessType = null;
                }
                checkForEnablingButton();
            }

            public void onNothingSelected(
                    AdapterView<?> adapterView) {

            }
        });
    }


    public static BecomeAVendorFragment getInstance() {
        return new BecomeAVendorFragment();
    }

    private void observeLiveData() {
        mBecomeAVendorViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_COMPANY_NAME:
                        showCompanyNameValidationError(validationErrors.getValidationMessage());
                        focusCompanyNameInputField();
                    case AppConstants.UI_VALIDATIONS.INVALID_EMAIL:
                        showEmailValidationError(validationErrors.getValidationMessage());
                        focusEmailInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER:
                        showPhoneNumberValidationError(validationErrors.getValidationMessage());
                        focusPhoneNumberInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_NAME:
                        showContactPersonValidationError(validationErrors.getValidationMessage());
                        focusContactPersonInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_ADDRESS:
                        showAddressValidationError(validationErrors.getValidationMessage());
                        focusAddressValidationInputField();
                        showSnackBar(validationErrors.getErrorMessage());
                }
            }
        });
        mBecomeAVendorViewModel.getBecomeAVendorLiveData().observe(getViewLifecycleOwner(), vendorSignUpResponse -> {
            if (vendorSignUpResponse != null) {
                hideProgressDialog();
                AppUtils.getInstance().openSuccessDialog(getActivity(), "Your request has been submitted successfully!", BecomeAVendorFragment.this);
            }
        });
        mBecomeAVendorViewModel.getUniqueVendorLiveData().observe(getViewLifecycleOwner(), uniqueEmailResponse -> {
            if (uniqueEmailResponse != null) {
                {
                    hideProgressDialog();
                    tvErrorEmail.setVisibility(View.GONE);
                    mBecomeAVendorViewModel.onSubmitViewClicked(getVendorInfo(), getDeviceId());

                }

            }
        });
    }

    private void focusCompanyNameInputField() {
        etCompanyName.setSelection(etCompanyName.getText().toString().length());
        etCompanyName.requestFocus();
    }

    private void showCompanyNameValidationError(int validationMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorCompanyName);
        tvErrorCompanyName.setText(validationMessage);
        tvErrorCompanyName.setVisibility(View.VISIBLE);
    }

    private void showAddressValidationError(int validationMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorAddress);
        tvErrorAddress.setText(validationMessage);
        tvErrorAddress.setVisibility(View.VISIBLE);
    }

    private void focusAddressValidationInputField() {
        etAddress.setSelection(etAddress.getText().toString().length());
        etAddress.requestFocus();
    }


    @Override
    protected void onFailure(FailureResponse failureResponse) {
        hideErrorFields();
        if (failureResponse.getErrorCode() == EXISTING_EMAIL_FAILURE) {
            showEmailValidationError(R.string.s_email_already_registered);
            focusEmailInputField();
        }
    }

    @Override
    protected void initViewsAndVariables() {
        btnSubmit.setEnabled(false);
        tvToolbarTittle.setText(getString(R.string.s_become_a_vendor));

        if (getContext() != null) {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.business_type));
            dataAdapter.setDropDownViewResource(R.layout.layout_custom_spinner_item);
            spBusinessType.setAdapter(dataAdapter);
        }

        spBusinessType.setOnItemSelectedListener(this);

    }

    private void focusPhoneNumberInputField() {
        etPhoneNumber.setSelection(etPhoneNumber.getText().toString().length());
        etPhoneNumber.requestFocus();
    }

    private void showPhoneNumberValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorNumber);
        tvErrorNumber.setText(errorMessage);
        tvErrorNumber.setVisibility(View.VISIBLE);
    }

    //show error message of invalid Username
    private void showEmailValidationError(int message) {
        AppUtils.getInstance().shakeLayout(tvErrorEmail);
        tvErrorEmail.setText(message);
        tvErrorEmail.setVisibility(View.VISIBLE);
    }

    //focus email address field
    private void focusEmailInputField() {
        etEmail.setSelection(etEmail.getText().toString().length());
        etEmail.requestFocus();
    }

    private void focusContactPersonInputField() {
        etContactPerson.setSelection(etContactPerson.getText().toString().length());
        etContactPerson.requestFocus();
    }

    private void showContactPersonValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorContactPerson);
        tvErrorContactPerson.setText(errorMessage);
        tvErrorContactPerson.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_submit, R.id.iv_report_issue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_submit:
                hideErrorFields();
                showProgressDialog();
                if (mBecomeAVendorViewModel.getUserType() == null) {
                    mBecomeAVendorViewModel.checkForUniqueEmail(etEmail.getText().toString().trim().toLowerCase());
                } else
                    mBecomeAVendorViewModel.onSubmitViewClicked(getVendorInfo(), getDeviceId());
                break;
            case R.id.iv_report_issue:
                mHost.openStaticPage(AppConstants.StaticPages.ABOUT_US);

        }
    }

    private void hideErrorFields() {
        tvErrorAddress.setVisibility(View.GONE);
        tvErrorBusinessType.setVisibility(View.GONE);
        tvErrorCompanyName.setVisibility(View.GONE);
        tvErrorEmail.setVisibility(View.GONE);
        tvErrorNumber.setVisibility(View.GONE);
        tvErrorContactPerson.setVisibility(View.GONE);

    }

    private BecomeAVendorInfo getVendorInfo() {
        BecomeAVendorInfo becomeAVendorInfo = new BecomeAVendorInfo();
        becomeAVendorInfo.setCompanyName(etCompanyName.getText().toString().trim());
        becomeAVendorInfo.setBusinessType(mBusinessType);
        becomeAVendorInfo.setName(etContactPerson.getText().toString().trim());
        becomeAVendorInfo.setAddress(etAddress.getText().toString().trim());
        becomeAVendorInfo.setPhoneNumber(etPhoneNumber.getText().toString().trim());
        becomeAVendorInfo.setEmailAddress(etEmail.getText().toString().trim());
        becomeAVendorInfo.setCountryCode(tvCountryCode.getText().toString());
        becomeAVendorInfo.setCompanyProfile(Objects.requireNonNull(etCompanyProfile.getText()).toString().trim());
        if (mBecomeAVendorViewModel.getUserType() == null)
            becomeAVendorInfo.setName(etContactPerson.getText().toString().trim());
        return becomeAVendorInfo;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        if (context instanceof ChangePasswordFragment.IChangePasswordHost) {
            mHost = (IBecomeAVendorHost) context;
        } else throw new IllegalStateException("Host must implement IBecomeAVendorHost");
        super.onAttach(context);
    }

    @Override
    public void onOkViewClicked() {
        mHost.onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        checkForEnablingButton();
    }


    private void checkForEnablingButton() {
        if (!etContactPerson.getText().toString().trim().isEmpty() && !etCompanyName.getText().toString().trim().isEmpty() && mBusinessType != null && !etAddress.getText().toString().trim().isEmpty()
                && !Objects.requireNonNull(etCompanyProfile.getText()).toString().trim().isEmpty() && !etPhoneNumber.getText().toString().trim().isEmpty() && !etEmail.getText().toString().trim().isEmpty()
                && tvErrorEmail.getVisibility() == View.GONE) {

            btnSubmit.setEnabled(true);
            btnSubmit.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public interface IBecomeAVendorHost {

        void onBackPressed();

        void openStaticPage(String title);
    }
}
