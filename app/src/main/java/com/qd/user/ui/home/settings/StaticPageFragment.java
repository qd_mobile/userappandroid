package com.qd.user.ui.home.settings;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.ui.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class StaticPageFragment extends BaseFragment {

    private Unbinder unbinder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.wv_content)
    WebView wvContent;

    /**
     * A {@link IStaticPageHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IStaticPageHost mStaticPageHost;
    private SettingsViewModel mSettingsViewModel;
    private String title;


    public static StaticPageFragment getInstance(String title) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.PAGE_TITLE, title);
        StaticPageFragment staticPageFragment = new StaticPageFragment();
        staticPageFragment.setArguments(bundle);
        return staticPageFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IStaticPageHost) {
            mStaticPageHost = (IStaticPageHost) context;
        } else
            throw new IllegalStateException("host must implement IStaticPageHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_static_pages, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSettingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        mSettingsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        getArgumentsData();
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getString(AppConstants.PAGE_TITLE) != null) {
            title = getArguments().getString(AppConstants.PAGE_TITLE);
            loadWebUrl(title);
            switch (title) {
                case AppConstants.StaticPages.ABOUT_US:
                    tvTitle.setText(R.string.s_about_us);
                    break;
                case AppConstants.StaticPages.PRIVACY_POLICY:
                    tvTitle.setText(R.string.s_privacy_policy);
                    break;
                case AppConstants.StaticPages.TERMS_AND_CONDITIONS:
                    tvTitle.setText(R.string.s_terms_conditions);
                    break;
            }
        }
    }

    @Override
    protected void initViewsAndVariables() {
        ivNotification.setVisibility(View.GONE);
    }

    private void loadWebUrl(String title) {

        wvContent.setWebViewClient(new WebViewClient() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError err) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, err.getErrorCode(), err.getDescription().toString(), req.getUrl().toString());
            }
        });

        switch (title) {
            case AppConstants.StaticPages.TERMS_AND_CONDITIONS:
                loadPage("getTermsConditionsForUser");
                break;
            case AppConstants.StaticPages.PRIVACY_POLICY:
                loadPage("getPrivacyPolicyForUser");
                break;
            case AppConstants.StaticPages.ABOUT_US:
                loadPage("getaboutUsForUser");
                break;
        }

    }

    private void loadPage(String endpoint) {
        wvContent.loadUrl(mSettingsViewModel.getBaseUrl() + "pages/" + endpoint);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mStaticPageHost.onBackPressed();
                break;
        }
    }


    public interface IStaticPageHost {

        void onBackPressed();
    }
}
