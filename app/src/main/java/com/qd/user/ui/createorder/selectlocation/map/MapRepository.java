package com.qd.user.ui.createorder.selectlocation.map;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.maps.autocomplete.PlacesAutoCompleteResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.kuwaitfinder.geocoder.KuwaitReverseGeoCodingResposne;

class MapRepository {

    void getPlacesAutoCompleteResponse(final RichMediatorLiveData<PlacesAutoCompleteResponse> placesAutoCompleteLiveData, String url) {
        DataManager.getInstance().getPlacesAutoCompleteResponse(url).enqueue(new NetworkCallback<PlacesAutoCompleteResponse>() {
            @Override
            public void onSuccess(PlacesAutoCompleteResponse placesAutoCompleteResponse) {
                placesAutoCompleteLiveData.setLoadingState(false);
                placesAutoCompleteLiveData.setValue(placesAutoCompleteResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                placesAutoCompleteLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                placesAutoCompleteLiveData.setError(t);
            }
        });
    }

    String getKuwaitFinderToken() {
        return DataManager.getInstance().getKuwaitFinderToken();
    }

    void getAddressThroughReverseGeoCoding(String url, final RichMediatorLiveData<KuwaitReverseGeoCodingResposne> reverseGeoCodingLiveData) {
        DataManager.getInstance().getAddressThroughReverseGeoCoding(url).enqueue(new NetworkCallback<KuwaitReverseGeoCodingResposne>() {
            @Override
            public void onSuccess(KuwaitReverseGeoCodingResposne kuwaitReverseGeoCodingResposne) {
                reverseGeoCodingLiveData.setValue(kuwaitReverseGeoCodingResposne);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                reverseGeoCodingLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                reverseGeoCodingLiveData.setError(t);
            }
        });
    }
}
