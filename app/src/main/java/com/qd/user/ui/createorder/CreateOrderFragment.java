package com.qd.user.ui.createorder;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.distancetime.eta.ETA;
import com.qd.user.model.createorder.distancetime.eta.VehicleCategory;
import com.qd.user.model.createorder.optimizedroute.DropLocation;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.Origin;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.PermissionHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CreateOrderFragment extends BaseFragment implements OnMapReadyCallback, VehicleTypeAdapter.VehicleTypeAdapterInterface,
        PermissionHelper.IGetPermissionListener, LocationListener {


    public static final int ZOOM_LEVEL = 15;
    public static final int ZOOM_ANIMATE_DURATION = 2000;
    private static final int MAP_BOTTOM_PADDING = 50;
    private static final int MAP_BOUND_PADDING = 250;
    private static final int MAP_TOP_PADDING = 350;

    @BindView(R.id.iv_pickup_point)
    ImageView ivPickupPoint;
    @BindView(R.id.et_pickup_point)
    TextView etPickupPoint;
    @BindView(R.id.iv_pickup_favourite)
    ImageView ivPickupFavourite;
    @BindView(R.id.cl_pickup_location)
    ConstraintLayout clPickupLocation;
    @BindView(R.id.iv_drop_point)
    ImageView ivDropPoint;
    @BindView(R.id.et_drop_point)
    TextView etDropPoint;
    @BindView(R.id.iv_drop_favourite)
    ImageView ivDropFavourite;
    @BindView(R.id.cl_drop_location)
    ConstraintLayout clDropLocation;
    @BindView(R.id.rl_container_map)
    RelativeLayout rlContainerMap;
    @BindView(R.id.rv_vehicle_type)
    RecyclerView rvVehicleType;
    @BindView(R.id.btn_proceed)
    Button btnProceed;
    private Unbinder unbinder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.iv_retry)
    ImageView ivRetry;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;

    private CreateOrderViewModel mCreateOrderViewModel;
    private ICreateOrderHost mHost;
    private VehicleTypeAdapter mVehicleTypesAdapter;
    private ArrayList<VehicleCategory> mVehiclesList;

    private CreateOrderRequest mCreateOrderData;
    private GoogleMap mGoogleMap;
    private LocationManager mLocationManager;
    private PermissionHelper mPermissionHelper;
    private boolean isPickupSaved;
    private boolean isDropSaved;
    private boolean isDriverAvailable;
    private boolean isSelected;


    public CreateOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_order, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static CreateOrderFragment getInstance(String orderType) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.ORDER_TYPE, orderType);
        CreateOrderFragment createOrderFragment = new CreateOrderFragment();
        createOrderFragment.setArguments(bundle);
        return createOrderFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ICreateOrderHost) {
            mHost = (ICreateOrderHost) context;
        } else throw new IllegalStateException("Host must implement ICreateOrderHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mCreateOrderViewModel = new ViewModelProvider(this).get(CreateOrderViewModel.class);
        mCreateOrderViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        super.onViewCreated(view, savedInstanceState);
        observeLiveData();
        setMap();

    }

    private void getVehicleCategories(double latitude, double longitude) {
        if (mCreateOrderData.getOrderType() != null)
            mCreateOrderViewModel.getVehicleCategories(mCreateOrderData.getOrderType(), latitude, longitude);
    }

    private void observeLiveData() {
        mCreateOrderViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                showSnackBar(validationErrors.getValidationMessage());
            }
        });

        mCreateOrderViewModel.getVehicleCategoryLiveData().observe(getViewLifecycleOwner(), vehicleCategoryResponse -> {
            ivRetry.setVisibility(View.GONE);
            showVehicleTypes();
            hideProgressDialog();
            if (vehicleCategoryResponse != null && vehicleCategoryResponse.getResult().getVehicleCategory().size() > 0) {
                mVehiclesList.clear();
                mVehiclesList.addAll(vehicleCategoryResponse.getResult().getVehicleCategory());
                if (vehicleCategoryResponse.getResult().getETA() != null)
                    setEtaForVehicles(vehicleCategoryResponse.getResult().getETA());
                mVehicleTypesAdapter.notifyDataSetChanged();
            }
        });

        mCreateOrderViewModel.getOptimizedDeliveryLiveData().observe(getViewLifecycleOwner(), optimizedDeliveryResponse -> {
            if (optimizedDeliveryResponse != null) {
                mCreateOrderData.getDropOffArr().get(0).setDeliveryCharge((optimizedDeliveryResponse.getResult().get(0).getDeliveryCharge()));
                mCreateOrderData.getDropOffArr().get(0).setCashBackAmount(optimizedDeliveryResponse.getResult().get(0).getReturnCharges());

                if (mCreateOrderData.getOrderType().equals(getString(R.string.business))) {
                    openTimeSelectionDialog();
                } else {
                    mHost.openAddItemFragment(mCreateOrderData);
                }

                /*if (mCreateOrderData.getOrderType().equals(AppConstants.OrderType.BUSINESS) && mCreateOrderData.getPaymentType() == null) {
                    mCreateOrderViewModel.getVendorInfo();
                } else {
                    hideProgressDialog();
                    if (shouldProceed) {
                        if (mCreateOrderData.getOrderType().equals(getString(R.string.business))) {
                            openTimeSelectionDialog();
                        } else {
                            mHost.openAddItemFragment(mCreateOrderData);
                        }
                    }
                }*/
            }
        });

        mCreateOrderViewModel.getVendorInfoResponseLiveData().observe(getViewLifecycleOwner(), vendorInfoResponse -> {
           /* if (vendorInfoResponse != null) {
                hideProgressDialog();
                mCreateOrderData.setPaymentType(vendorInfoResponse.getResult().getPaymentModes());
                if (mCreateOrderData.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
                    setPickupLocationForVendor();
                }
                if (mCreateOrderData.getDropOffArr() != null && mCreateOrderData.getDropOffArr().get(0).getDeliveryCharge() == null) {
                    if (shouldProceed) {
                        if (mCreateOrderData.getOrderType().equals(getString(R.string.business))) {
                            openTimeSelectionDialog();
                        } else {
                            mHost.openAddItemFragment(mCreateOrderData);
                        }
                    }
                }
            }*/
        });

        mCreateOrderViewModel.getDriverAvailabilityLiveData().observe(getViewLifecycleOwner(), driverAvailabilityResponse -> {
            if (driverAvailabilityResponse != null) {
                isDriverAvailable = true;

                if (mCreateOrderData.getDropOffArr() != null && mCreateOrderData.getDropOffArr().get(0) != null) {
                    mCreateOrderViewModel.getDeliveryCharges(getDataForOptimizedRoute());
                }
            }
        });

    }

    private void setEtaForVehicles(ETA eta) {
        for (int i = 0; i < mVehiclesList.size(); i++) {
            switch (mVehiclesList.get(i).getCategoryName()) {
                case AppConstants.DeliveryVehicles.BIKE:
                    if (eta.getBike().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getBike().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;

                case AppConstants.DeliveryVehicles.CAR:
                    mVehiclesList.get(i).setSelected(true);
                    mCreateOrderData.setVehicleType(mVehiclesList.get(i).getCategoryName());
                    if (eta.getCar().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getCar().get(0).getETA());
                        mCreateOrderData.setEta(eta.getCar().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;

                case AppConstants.DeliveryVehicles.VAN:
                    if (eta.getVan().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getVan().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;
                case AppConstants.DeliveryVehicles.COOLER:
                    if (eta.getCooler().size() > 0) {
                        mVehiclesList.get(i).setEta(eta.getCooler().get(0).getETA());
                    } else mVehiclesList.get(i).setEta(getString(R.string.not_available));
                    break;
            }
        }
    }

    @Override
    protected void initViewsAndVariables() {
        setInitialViews();
        getArgumentsData();
        setUpAdapterForVehicleTypes();
        hideVehicleTypes();
        etPickupPoint.setSelected(true);
        etDropPoint.setSelected(true);
        mPermissionHelper = new PermissionHelper(this);
    }

    private void setInitialViews() {
        ivNotification.setVisibility(View.VISIBLE);
        ivPickupFavourite.setVisibility(View.GONE);
        ivDropFavourite.setVisibility(View.GONE);
        mVehiclesList = new ArrayList<>();
        mCreateOrderData = new CreateOrderRequest();
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().getString(AppConstants.ORDER_TYPE) != null) {
            String mOrderType = getArguments().getString(AppConstants.ORDER_TYPE);
            assert mOrderType != null;
            switch (mOrderType) {
                case AppConstants.orderType.SEND:
                    mCreateOrderData.setOrderType(AppConstants.OrderType.SEND);
                    mCreateOrderData.setDeliveryBySender(true);
                    tvTitle.setText(R.string.s_send_item);
                    setOrderDetails();
                    break;
                case AppConstants.orderType.GET:
                    mCreateOrderData.setOrderType(AppConstants.OrderType.GET);
                    tvTitle.setText(R.string.s_get_item);
                    mCreateOrderData.setDeliveryBySender(false);
                    setOrderDetails();
                    break;
                case AppConstants.orderType.BUSINESS:
                    mCreateOrderData.setOrderType(AppConstants.OrderType.BUSINESS);
                    setPickupLocationForVendor();
                    mCreateOrderData.setPaymentType(mCreateOrderViewModel.getVendorPaymentMode());
                    isDriverAvailable = true;
                    mCreateOrderData.setDeliveryBySender(true);
                    openDeliveryTypeDialog();
                    tvTitle.setText(R.string.s_business);
                    //mCreateOrderViewModel.getVendorInfo();
                    break;
            }
        }
    }

    private void setPickupLocationForVendor() {
        showVehicleTypes();
        List<PickupLocation> pickupLocationList = new ArrayList<>();
        pickupLocationList.add(new GsonBuilder().create().fromJson(mCreateOrderViewModel.getBusinessAddress(), PickupLocation.class));
        mCreateOrderData.setPickupLocation(pickupLocationList);
        etPickupPoint.setText(mCreateOrderData.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        etPickupPoint.setEnabled(false);
    }

    private void openDeliveryTypeDialog() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.layout_delivery_type);
            AppUtils.getInstance().dimDialogBackground(dialog);
            dialog.setCanceledOnTouchOutside(false);

            final TextView tvSingleDelivery = dialog.findViewById(R.id.tv_single_delivery);
            final TextView tvMultipleDelivery = dialog.findViewById(R.id.tv_multiple_delivery);
            final Button btnProceed = dialog.findViewById(R.id.btn_proceed);

            tvSingleDelivery.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.enter_anim));
            tvMultipleDelivery.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.exit_anim));

            dialog.setOnDismissListener(dialog1 -> {
                if (!isSelected || mCreateOrderData.getDeliveryType() == null || mCreateOrderData.getDeliveryType().equals("")) {
                    mHost.onBackPressedOnDataEntered();
                }
            });

            btnProceed.setOnClickListener(v -> {
                isSelected = true;
                if (mCreateOrderData.getDeliveryType() != null && !mCreateOrderData.getDeliveryType().equals("")) {
                    setOrderDetails();
                    getVehicleCategories(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                            mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
                    dialog.dismiss();
                }
            });

            tvSingleDelivery.setOnClickListener(v -> {
                btnProceed.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
                btnProceed.setEnabled(true);
                tvSingleDelivery.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_delivery_type_single_active, 0, 0);
                tvSingleDelivery.setBackgroundResource(R.drawable.drawable_circular_corner_pale_grey);
                tvMultipleDelivery.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_delivery_type_multiple_inactive, 0, 0);
                tvMultipleDelivery.setBackgroundResource(R.color.colorWhite);
                tvSingleDelivery.setTextColor(getResources().getColor(R.color.colorTextBlack));
                mCreateOrderData.setDeliveryType(getString(R.string.single_delivery));

            });

            tvMultipleDelivery.setOnClickListener(v -> {
                btnProceed.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
                btnProceed.setEnabled(true);
                tvMultipleDelivery.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_delivery_type_multiple_active, 0, 0);
                tvMultipleDelivery.setBackgroundResource(R.drawable.drawable_circular_corner_pale_grey);
                tvSingleDelivery.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_delivery_type_single_inactive, 0, 0);
                tvSingleDelivery.setBackgroundResource(R.color.colorWhite);
                tvMultipleDelivery.setTextColor(getResources().getColor(R.color.colorTextBlack));
                mCreateOrderData.setDeliveryType(getString(R.string.multiple_delivery));
            });

            dialog.show();
        }
    }

    private void setUpAdapterForVehicleTypes() {
        mVehicleTypesAdapter = new VehicleTypeAdapter(mVehiclesList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvVehicleType.setLayoutManager(linearLayoutManager);
        rvVehicleType.setAdapter(mVehicleTypesAdapter);

    }

    /**
     * method to setup map initially
     */
    private void setMap() {
        final SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        addFragmentWithoutAnimation(R.id.rl_container_map, mapFragment, null);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        moveToCurrentLocation(new LatLng(AppConstants.KUWAIT_CENTER_LAT, AppConstants.KUWAIT_CENTER_LONG));


        if (getActivity() != null) {
            mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (mPermissionHelper.hasPermission(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.PermissionConstants.REQUEST_LOCATION)) {

                permissionsGiven(AppConstants.PermissionConstants.REQUEST_LOCATION);
            }

            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setScrollGesturesEnabled(true);

            googleMap.setPadding(0, MAP_TOP_PADDING, 0, MAP_BOTTOM_PADDING);
        }

    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, ZOOM_LEVEL));
        // Zoom in, animating the camera.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL), ZOOM_ANIMATE_DURATION, null);

    }

    @OnClick({R.id.et_pickup_point, R.id.iv_pickup_favourite, R.id.et_drop_point, R.id.iv_drop_favourite, R.id.btn_proceed, R.id.iv_back,
            R.id.iv_retry, R.id.iv_notification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_pickup_point:
                mHost.openSelectLocationFragment(true, mCreateOrderData.getPickupLocation(), null);
                break;
            case R.id.iv_pickup_favourite:
                if (!isPickupSaved) {
                    ivPickupFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_address_active));
                    //saveAddress(getAddressToSave(mCreateOrderData.getPickupLocation().get(0)));
                } else {
                    ivPickupFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_star));
                }
                break;
            case R.id.et_drop_point:
                mHost.openSelectLocationFragment(false, null, mCreateOrderData.getDropOffArr());
                break;
            case R.id.iv_drop_favourite:
                if (!isDropSaved) {
                    ivDropFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_address_active));
                } else {
                    ivDropFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_star));
                }
                break;
            case R.id.btn_proceed:
                if (mCreateOrderViewModel.validateOrderLocationAndVehicle(mCreateOrderData.getPickupLocation(),
                        mCreateOrderData.getDropOffArr(),
                        mCreateOrderData.getVehicleType())) {
                    showProgressDialog();


                    if (mCreateOrderData.getOrderType().equals(getString(R.string.business))) {
                        if (mCreateOrderData.getDropOffArr().get(0).getDeliveryCharge() == null)
                            mCreateOrderViewModel.getDeliveryCharges(getDataForOptimizedRoute());
                        else
                            openTimeSelectionDialog();
                    } else {
                        if (isDriverAvailable) {
                            mCreateOrderViewModel.getDeliveryCharges(getDataForOptimizedRoute());
                        } else {
                            mCreateOrderViewModel.checkAvailabilityStatus(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates(),
                                    mCreateOrderData.getVehicleType());
                        }

                    }

                }
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_retry:
                getEtaAgain();
                break;
            case R.id.iv_notification:
                mHost.openNotificationsFragment();
                break;
        }
    }

    public void onBackPressed() {
        if (!etPickupPoint.getText().toString().isEmpty() || !etDropPoint.getText().toString().isEmpty() || mCreateOrderData.getVehicleType() != null)
            AppUtils.getInstance().showAlertDialog(getContext(), getString(R.string.s_are_you_sure_you_want_to_go_back_all_data_will_be_cleared), (dialog, which) -> mHost.onBackPressedOnDataEntered(), (dialog, which) -> {

            });
        else {
            mHost.onBackPressedOnDataEntered();
        }
    }

    private void getEtaAgain() {
        if (mVehiclesList.size() == 0 || mVehiclesList.get(0).getEta() == null) {
            showProgressDialog();
            if (mCreateOrderData.getPickupLocation() != null && mCreateOrderData.getPickupLocation().get(0).getGeometry() != null) {
                getVehicleCategories(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                        mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
            }/* else if (mCurrentLocation != null)
                getVehicleCategories(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());*/
        }
    }

    private OptimizedDeliveryRequest getDataForOptimizedRoute() {
        OptimizedDeliveryRequest optimizedDeliveryRequest = new OptimizedDeliveryRequest();
        Origin origin = new Origin();
        origin.setLat(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(1));
        origin.setLng(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
        optimizedDeliveryRequest.setOrigin(origin);

        DropLocation dropLocation = new DropLocation();
        dropLocation.setLat(mCreateOrderData.getDropOffArr().get(0).getGeometry().getCoordinates().get(1));
        dropLocation.setLng(mCreateOrderData.getDropOffArr().get(0).getGeometry().getCoordinates().get(0));
        List<DropLocation> dropLocationList = new ArrayList<>();
        dropLocationList.add(dropLocation);
        optimizedDeliveryRequest.setDropLocation(dropLocationList);

        optimizedDeliveryRequest.setVehicleType(mCreateOrderData.getVehicleType());
        optimizedDeliveryRequest.setUserType(mCreateOrderData.getRequestFrom());

        optimizedDeliveryRequest.setServiceType(AppConstants.serviceType.PREMIUM);

        if (mCreateOrderData.getOrderType().equals(AppConstants.OrderType.SEND) ||
                mCreateOrderData.getOrderType().equals(AppConstants.OrderType.SPECIAL_SEND)) {
            optimizedDeliveryRequest.setOrderType(AppConstants.OrderType.SPECIAL_SEND);
        } else optimizedDeliveryRequest.setOrderType(mCreateOrderData.getOrderType());

        optimizedDeliveryRequest.setOptimize(true);

        return optimizedDeliveryRequest;
    }

    /*private SaveAddressRequest getAddressToSave(PickupLocation pickupLocation) {
        SaveAddressRequest saveAddressRequest = new SaveAddressRequest();
        saveAddressRequest.setGovernorate(pickupLocation.getGovernorate());
        saveAddressRequest.setArea(pickupLocation.getArea());
        saveAddressRequest.setBlockNumber(pickupLocation.getBlockNumber());
        saveAddressRequest.setStreet(pickupLocation.getStreet());
        saveAddressRequest.setAvenue(pickupLocation.getAvenue());
        saveAddressRequest.setHouseOrBuilding(pickupLocation.getAvenue());
        saveAddressRequest.setFloor(pickupLocation.getFloor());
        saveAddressRequest.setApartmentOrOffice(pickupLocation.getApartmentOrOffice());
        saveAddressRequest.setGeometry(pickupLocation.getGeometry());
        saveAddressRequest.setFullAddress(pickupLocation.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        return saveAddressRequest;
    }*/

    private void openTimeSelectionDialog() {
        mHost.openTimeSelectionFragment(mCreateOrderData, CreateOrderFragment.class.getName());
    }

    private void setOrderDetails() {
        if (mCreateOrderData.getOrderType().equals(getString(R.string.send)) ||
                mCreateOrderData.getOrderType().equals(getString(R.string.get))) {

            mCreateOrderData.setRequestFrom(getString(R.string.cash_client));
            mCreateOrderData.setRequestFor(getString(R.string.cash_client));
            mCreateOrderData.setIsScheduled(true);
            mCreateOrderData.setIslater(false);
            mCreateOrderData.setDeliveryType(getString(R.string.single_delivery));
            mCreateOrderData.setServiceType(getString(R.string.s_premium));

        } else if (mCreateOrderData.getOrderType().equals(getString(R.string.business))) {
            mCreateOrderData.setRequestFrom(getString(R.string.vendor));
            mCreateOrderData.setRequestFor(getString(R.string.vendor));
        }
    }

    public void setLocation(boolean isForPickup, SaveAddressRequest completeAddress) {
        if (isForPickup) {
            //ivPickupFavourite.setVisibility(View.VISIBLE);
            etPickupPoint.setText(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            setPickupLocation(completeAddress);

            if (completeAddress.getIsFavourite()) {
                isPickupSaved = true;
                ivPickupFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_address_active));
            } else {
                isPickupSaved = false;
                ivDropFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_star));
            }
        } else {
            //ivDropFavourite.setVisibility(View.VISIBLE);
            etDropPoint.setText(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            setDropLocation(completeAddress);
            if (completeAddress.getIsFavourite()) {
                isDropSaved = true;
                ivDropFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_save_address_active));
            } else {
                isDropSaved = false;
                ivDropFavourite.setImageDrawable(getResources().getDrawable(R.drawable.ic_send_star));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionHelper.setPermissionResult(requestCode, permissions, grantResults);
    }

    private void setDropLocation(SaveAddressRequest completeAddress) {
        DropOffArr dropOffArr = new DropOffArr();
        dropOffArr.setGovernorate(completeAddress.getGovernorate());
        dropOffArr.setArea(completeAddress.getArea());
        dropOffArr.setBlockNumber(completeAddress.getBlockNumber());
        dropOffArr.setStreet(completeAddress.getStreet());
        dropOffArr.setAvenue(completeAddress.getAvenue());
        dropOffArr.setHouseOrbuilding(completeAddress.getHouseOrbuilding());
        dropOffArr.setApartmentOrOffice(completeAddress.getApartmentOrOffice());
        dropOffArr.setFloor(completeAddress.getFloor());
        dropOffArr.setFullAddress(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        dropOffArr.setGeometry(completeAddress.getGeometry());
        dropOffArr.setName(completeAddress.getName());
        dropOffArr.setFavourite(completeAddress.getIsFavourite());

        List<DropOffArr> dropOffArrList = new ArrayList<>();
        dropOffArrList.add(dropOffArr);

        if (mGoogleMap != null) {
            mGoogleMap.clear();

            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(dropOffArr.getGeometry().getCoordinates().get(1),
                    dropOffArr.getGeometry().getCoordinates().get(0)))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_drop))
                    .title(dropOffArr.getFullAddress()));

            moveToCurrentLocation(new LatLng(dropOffArr.getGeometry().getCoordinates().get(1),
                    dropOffArr.getGeometry().getCoordinates().get(0)));

            if (mCreateOrderData.getPickupLocation() != null && mCreateOrderData.getPickupLocation().size() > 0) {
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                        mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(0)))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pickup))
                        .title(mCreateOrderData.getPickupLocation().get(0).getFullAddress()));

                moveToCurrentLocation(new LatLng(mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(1),
                        mCreateOrderData.getPickupLocation().get(0).getGeometry().getCoordinates().get(0)), new LatLng(dropOffArr.getGeometry().getCoordinates().get(1),
                        dropOffArr.getGeometry().getCoordinates().get(0)));
            }
        }

        mCreateOrderData.setDropOffArr(dropOffArrList);
    }

    private void moveToCurrentLocation(LatLng origin, LatLng destination) {
        //fit to bound
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(origin);
        builder.include(destination);

        LatLngBounds bounds = builder.build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAP_BOUND_PADDING);
        mGoogleMap.animateCamera(cameraUpdate);
    }

    private void setPickupLocation(SaveAddressRequest completeAddress) {
        showVehicleTypes();

        PickupLocation pickupLocation = new PickupLocation();
        pickupLocation.setGovernorate(completeAddress.getGovernorate());
        pickupLocation.setArea(completeAddress.getArea());
        pickupLocation.setBlockNumber(completeAddress.getBlockNumber());
        pickupLocation.setStreet(completeAddress.getStreet());
        pickupLocation.setAvenue(completeAddress.getAvenue());
        pickupLocation.setHouseOrbuilding(completeAddress.getHouseOrbuilding());
        pickupLocation.setApartmentOrOffice(completeAddress.getApartmentOrOffice());
        pickupLocation.setFloor(completeAddress.getFloor());
        pickupLocation.setFullAddress(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        pickupLocation.setGeometry(completeAddress.getGeometry());
        pickupLocation.setFavourite(completeAddress.getIsFavourite());
        pickupLocation.setName(completeAddress.getName());
        List<PickupLocation> pickupLocationList = new ArrayList<>();
        pickupLocationList.add(pickupLocation);
        mCreateOrderData.setPickupLocation(pickupLocationList);

        if (mGoogleMap != null) {
            mGoogleMap.clear();

            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(pickupLocation.getGeometry().getCoordinates().get(1),
                    pickupLocation.getGeometry().getCoordinates().get(0)))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pickup))
                    .title(pickupLocation.getFullAddress()));
            moveToCurrentLocation(new LatLng(pickupLocation.getGeometry().getCoordinates().get(1),
                    pickupLocation.getGeometry().getCoordinates().get(0)));

            if (mCreateOrderData.getDropOffArr() != null && mCreateOrderData.getDropOffArr().size() > 0) {
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(mCreateOrderData.getDropOffArr().get(0).getGeometry().getCoordinates().get(1),
                        mCreateOrderData.getDropOffArr().get(0).getGeometry().getCoordinates().get(0)))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_drop))
                        .title(mCreateOrderData.getDropOffArr().get(0).getFullAddress()));

                moveToCurrentLocation(new LatLng(pickupLocation.getGeometry().getCoordinates().get(1),
                        pickupLocation.getGeometry().getCoordinates().get(0)), new LatLng(mCreateOrderData.getDropOffArr().get(0).getGeometry().getCoordinates().get(1),
                        mCreateOrderData.getDropOffArr().get(0).getGeometry().getCoordinates().get(0)));

            }
        }

        getVehicleCategories(completeAddress.getGeometry().getCoordinates().get(1), completeAddress.getGeometry().getCoordinates().get(0));

    }

    private void hideVehicleTypes() {
        rvVehicleType.animate()
                .translationY(rvVehicleType.getHeight())
                .alpha(0.0f)
                .setDuration(AppConstants.SHAKE_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (isAdded())
                            rvVehicleType.setVisibility(View.GONE);
                    }
                });
    }

    private void showVehicleTypes() {
        rvVehicleType.animate()
                .translationY(0)
                .alpha(1.0f)
                .setDuration(AppConstants.SHAKE_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (isVisible())
                            rvVehicleType.setVisibility(View.VISIBLE);
                    }
                });
    }

    @Override
    public void onVehicleTypeSelected(VehicleCategory result) {
        if (!mCreateOrderData.getVehicleType().equals(result.getCategoryName())) {
            mCreateOrderData.setVehicleType(result.getCategoryName());
            mCreateOrderData.setEta(result.getEta());
            mCreateOrderData.getDropOffArr().get(0).setDeliveryCharge(null);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void permissionsGiven(int requestCode) {
        Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            setMarkerAtCurrentLocation(location);
        } else {
            getLastLocationNewMethod();
        }
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            setMarkerAtCurrentLocation(location);
        }
    }

    @SuppressLint("MissingPermission")
    private void getLastLocationNewMethod() {
        if (getContext() != null) {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(location -> {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            setMarkerAtCurrentLocation(location);
                        }
                    })
                    .addOnFailureListener(Throwable::printStackTrace);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        if (mVehiclesList.size() == 0)
            ivRetry.setVisibility(View.VISIBLE);
        else if (mVehiclesList.get(0).getEta() == null) {
            getEtaAgain();
        }
    }

    private void setMarkerAtCurrentLocation(Location location) {
        /*mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).
                icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_send_current_location)));*/
        moveToCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()));
        mLocationManager.removeUpdates(this);
    }

    void resetDrop() {
        etDropPoint.setText("");
        mCreateOrderData.setDropOffArr(null);
        mCreateOrderData.setVehicleType(null);
        for (int i = 0; i < mVehiclesList.size(); i++) {
            mVehiclesList.get(i).setSelected(false);
        }
        mVehicleTypesAdapter.notifyDataSetChanged();

    }


    public interface ICreateOrderHost {

        void openSelectLocationFragment(boolean isForPickup, List<PickupLocation> pickupLocation, List<DropOffArr> drop);

        void openAddItemFragment(CreateOrderRequest mCreateOrderData);

        void openTimeSelectionFragment(CreateOrderRequest createOrderData, String navigation);

        void onBackPressedOnDataEntered();

        void openNotificationsFragment();
    }
}