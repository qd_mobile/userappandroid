package com.qd.user.ui.home.editprofile;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EditProfileFragment extends BaseFragment implements TextWatcher ,AppUtils.ISuccess{

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_toolbar_divider)
    View viewToolbarDivider;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.til_name)
    TextInputLayout tilName;
    @BindView(R.id.view_name)
    View viewName;
    @BindView(R.id.tv_error_name)
    TextView tvErrorName;
    @BindView(R.id.tv_country_code)
    TextView tvCountryCode;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.til_phone_number)
    TextInputLayout tilPhoneNumber;
    @BindView(R.id.view_country_code)
    View viewCountryCode;
    @BindView(R.id.view_phone_number)
    View viewPhoneNumber;
    @BindView(R.id.tv_error_phone_number)
    TextView tvErrorPhoneNumber;
    @BindView(R.id.tv_label_contact_info)
    TextView tvLabelContactInfo;
    @BindView(R.id.et_email_address)
    EditText etEmailAddress;
    @BindView(R.id.til_email_address)
    TextInputLayout tilEmailAddress;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.tv_change)
    TextView tvChange;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    private Unbinder unbinder;
    @BindView(R.id.tv_label_password)
    TextView tvLabelPassword;
    private EditProfileViewModel mEditProfileViewModel;
    private IEditProfileHost mHost;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditProfileViewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);
        mEditProfileViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setUserView();
        setListeners();

    }


    private void observeLiveData() {
        mEditProfileViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_NAME:
                        showNameValidationError(validationErrors.getValidationMessage());
                        focusNameInputField();

                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER:
                        showPhoneNumberValidationError(validationErrors.getValidationMessage());
                        focusPhoneNumberInputField();
                        break;
                }
            }
        });
        mEditProfileViewModel.getEditProfileLiveData().observe(getViewLifecycleOwner(), editProfileResponse -> {
            if (editProfileResponse != null) {
                hideProgressDialog();
                AppUtils.getInstance().openSuccessDialog(getActivity(), "Profile updated successfully!", EditProfileFragment.this);

            }
        });

    }
   //focus name field
    private void focusNameInputField() {
        etName.setSelection(etName.getText().toString().length());
        etName.requestFocus();
    }

    //show error message of invalid username
    private void showNameValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(etName);
        tvErrorName.setText(getString(errorMessage));
        tvErrorName.setVisibility(View.VISIBLE);
    }

    //show error message of invalid phone number
    private void showPhoneNumberValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorPhoneNumber);
        tvErrorPhoneNumber.setText(getString(errorMessage));
        tvErrorPhoneNumber.setVisibility(View.VISIBLE);
    }

    //focus phone number field
    private void focusPhoneNumberInputField() {
        etPhoneNumber.setSelection(etPhoneNumber.getText().toString().length());
        etPhoneNumber.requestFocus();

    }


    private void setListeners() {
        etName.addTextChangedListener(this);
        etPhoneNumber.addTextChangedListener(this);
    }

    //set user info to the fields of edit profile
    private void setUserView() {
        etName.setText(mEditProfileViewModel.getUserName());
        etEmailAddress.setText(mEditProfileViewModel.getEmail());
        etPhoneNumber.setText(mEditProfileViewModel.getPhoneNumber());
        tvCountryCode.setText(mEditProfileViewModel.getCountryCode());
        if (mEditProfileViewModel.getUserSignUpType().equals(AppConstants.GMAIL) || mEditProfileViewModel.getUserSignUpType().equals(AppConstants.FACEBOOK)) {
            tvLabelPassword.setVisibility(View.GONE);
            tvChange.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_edit_profile);
        super.initViewsAndVariables();
    }

    public static EditProfileFragment getInstance() {
        return new EditProfileFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        enableOrDisableButton();
    }

    private void enableOrDisableButton() {
        if (!etName.getText().toString().trim().equals("") && !etPhoneNumber.getText().toString().trim().equals("")) {
            btnSave.setEnabled(true);
            btnSave.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_greenishteal));

        } else {
            btnSave.setEnabled(false);
            btnSave.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));

        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IEditProfileHost){
            mHost=(IEditProfileHost)context;
        }
        else throw new IllegalStateException("Host must implement IEditProfileHost");
    }

    @OnClick({R.id.btn_save, R.id.tv_change, R.id.iv_back})
    public void onViewClicked(View view) {
        switch(view.getId()){
            case R.id.btn_save:
                hideErrorFields();
                showProgressDialog();
                mEditProfileViewModel.onSaveViewClicked(getEditUserInfo());
                break;
            case R.id.tv_change:
                mHost.openChangePasswordFragment();
                break;
            case R.id.iv_back:
                mHost.onBackPressed();
                break;

        }
    }

    private void hideErrorFields() {
        tvErrorPhoneNumber.setVisibility(View.GONE);
        tvErrorName.setVisibility(View.GONE);
    }

    //get updated data which user want to update on profile
    private CreateAccountInfo getEditUserInfo() {
        CreateAccountInfo createAccountInfo=new CreateAccountInfo();
        createAccountInfo.setPhoneNumber(etPhoneNumber.getText().toString().trim());
        createAccountInfo.setName(etName.getText().toString().trim());
        createAccountInfo.setCountryCode(tvCountryCode.getText().toString().trim());
        return createAccountInfo;
    }

    @Override
    public void onOkViewClicked() {
        mHost.backToProfileFragment(null);
    }

    public interface IEditProfileHost{

        void backToProfileFragment(com.qd.user.model.createorder.savedaddress.Result result);

        void openChangePasswordFragment();

        void onBackPressed();
    }
}
