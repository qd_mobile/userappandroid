package com.qd.user.ui.home.paymenthistory;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.model.failureresponse.FailureResponse;

public class PaymentHistoryViewModel extends ViewModel {

    private PaymentHistoryRepo mRepo = new PaymentHistoryRepo();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {

    }

    public String getUserType() {
        return mRepo.getUserType();
    }
}
