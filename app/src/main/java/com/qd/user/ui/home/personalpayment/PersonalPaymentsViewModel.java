package com.qd.user.ui.home.personalpayment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.payments.personal.PaymentHistoryResponse;

public class PersonalPaymentsViewModel extends ViewModel {

    private RichMediatorLiveData<PaymentHistoryResponse> mPaymentHistoryLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private PersonalPaymentsRepository mRepo = new PersonalPaymentsRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mPaymentHistoryLiveData == null) {
            mPaymentHistoryLiveData = new RichMediatorLiveData<PaymentHistoryResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
    }


    /**
     * This method gives the Invoice listing live data object to {@link PersonalPaymentsFragment}
     *
     * @return {@link #mPaymentHistoryLiveData}
     */
    RichMediatorLiveData<PaymentHistoryResponse> getPaymentHistoryLiveData() {
        return mPaymentHistoryLiveData;
    }


    void getPersonalPaymentsHistory(int page, String fromDate, String toDate) {
        mRepo.getPaymentHistory(mPaymentHistoryLiveData, page, fromDate, toDate);
    }

    public String getRegistrationDate() {
        return mRepo.getRegistrationDate();
    }
}
