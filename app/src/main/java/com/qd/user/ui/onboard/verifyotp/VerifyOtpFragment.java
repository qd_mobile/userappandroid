package com.qd.user.ui.onboard.verifyotp;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.NetworkConstants;
import com.qd.user.ui.onboard.OnBoardActivity;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class VerifyOtpFragment extends BaseFragment implements View.OnKeyListener, AppUtils.ISuccess {
    private Unbinder unbinder;
    @BindView(R.id.et_otp_one)
    EditText etOtpOne;
    @BindView(R.id.view_round_one)
    View viewRoundOne;
    @BindView(R.id.et_otp_two)
    EditText etOtpTwo;
    @BindView(R.id.view_round_two)
    View viewRoundTwo;
    @BindView(R.id.et_otp_three)
    EditText etOtpThree;
    @BindView(R.id.view_round_three)
    View viewRoundThree;
    @BindView(R.id.et_otp_four)
    EditText etOtpFour;
    @BindView(R.id.view_round_four)
    View viewRoundFour;
    @BindView(R.id.et_otp_five)
    EditText etOtpFive;
    @BindView(R.id.view_round_five)
    View viewRoundFive;
    @BindView(R.id.et_otp_six)
    EditText etOtpSix;
    @BindView(R.id.view_round_six)
    View viewRoundSix;
    @BindView(R.id.tv_resend_code)
    TextView tvResendCode;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_subtittle)
    TextView tvSubtittle;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;


    /**
     * An {@link IVerifyOtpHost} object to interact with the host{@link OnBoardActivity}
     * if any action has to be performed from the host.
     */
    private IVerifyOtpHost mHost;

    /**
     * A {@link VerifyOtpViewModel} object to handle all the actions and business logic of verification
     */
    private VerifyOtpViewModel mVerifyOtpViewModel;
    private GenericTextWatcher et1Watcher;
    private GenericTextWatcher et2Watcher;
    private GenericTextWatcher et3Watcher;
    private GenericTextWatcher et4Watcher;
    private GenericTextWatcher et5Watcher;
    private GenericTextWatcher et6Watcher;
    private String mPhoneNumber;

    public static VerifyOtpFragment getInstance(String phoneNumber) {
        Bundle bundle = new Bundle();
        bundle.putString(NetworkConstants.KEY_PHONE_NUMBER, phoneNumber);
        VerifyOtpFragment verifyOtpFragment = new VerifyOtpFragment();
        verifyOtpFragment.setArguments(bundle);
        return verifyOtpFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IVerifyOtpHost) {
            mHost = (IVerifyOtpHost) context;
        } else
            throw new IllegalStateException("host must implement IVerifyOtpHost");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment_order_status
        View view = inflater.inflate(R.layout.fragment_verify_otp, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mVerifyOtpViewModel = new ViewModelProvider(this).get(VerifyOtpViewModel.class);
        mVerifyOtpViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        setSpannableStringToResendOtp();
        observeLiveData();
        getArgumentsData();
        super.onViewCreated(view, savedInstanceState);
        setListeners();

    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(NetworkConstants.KEY_PHONE_NUMBER) && getArguments().getString(NetworkConstants.KEY_PHONE_NUMBER) != null) {
            mPhoneNumber = getArguments().getString(NetworkConstants.KEY_PHONE_NUMBER);

        } else {
            mPhoneNumber = mVerifyOtpViewModel.getPhoneNumber();
        }
    }

    private void setListeners() {
        etOtpOne.setOnKeyListener(this);
        etOtpTwo.setOnKeyListener(this);
        etOtpThree.setOnKeyListener(this);
        etOtpFour.setOnKeyListener(this);
        etOtpFive.setOnKeyListener(this);
        etOtpSix.setOnKeyListener(this);

        et1Watcher = new GenericTextWatcher(etOtpOne);
        et2Watcher = new GenericTextWatcher(etOtpTwo);
        et3Watcher = new GenericTextWatcher(etOtpThree);
        et4Watcher = new GenericTextWatcher(etOtpFour);
        et5Watcher = new GenericTextWatcher(etOtpFive);
        et6Watcher = new GenericTextWatcher(etOtpSix);

        setTextWatcherOnEditText();
    }

    private void setTextWatcherOnEditText() {
        etOtpOne.addTextChangedListener(et1Watcher);
        etOtpTwo.addTextChangedListener(et2Watcher);
        etOtpThree.addTextChangedListener(et3Watcher);
        etOtpFour.addTextChangedListener(et4Watcher);
        etOtpFive.addTextChangedListener(et5Watcher);
        etOtpSix.addTextChangedListener(et6Watcher);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        //This is the filter
        if (event.getAction() != KeyEvent.ACTION_DOWN)
            return false;
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            switch (v.getId()) {
                case R.id.et_otp_one:
                    break;
                case R.id.et_otp_two:
                    setFocusOnEditText(etOtpOne);
                    break;
                case R.id.et_otp_three:
                    setFocusOnEditText(etOtpTwo);
                    break;
                case R.id.et_otp_four:
                    setFocusOnEditText(etOtpThree);
                    break;
                case R.id.et_otp_five:
                    setFocusOnEditText(etOtpFour);
                    break;
                case R.id.et_otp_six:
                    setFocusOnEditText(etOtpFive);
                    break;
            }
        }
        return false;
    }

    /**
     * set focus to previous edit text on delete
     *
     * @param etPreviousEditText on which focus has to be shift
     */
    private void setFocusOnEditText(EditText etPreviousEditText) {
        etPreviousEditText.requestFocus();
        etPreviousEditText.setSelection(etPreviousEditText.getText().length());
    }

    @Override
    public void onOkViewClicked() {
    }

    class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            String text = arg0.toString();
            switch (view.getId()) {
                case R.id.et_otp_one:
                    manageInputFieldState(text, etOtpOne, etOtpTwo, viewRoundOne);
                    break;
                case R.id.et_otp_two:
                    manageInputFieldState(text, etOtpTwo, etOtpThree, viewRoundTwo);

                    break;
                case R.id.et_otp_three:
                    manageInputFieldState(text, etOtpThree, etOtpFour, viewRoundThree);
                    break;
                case R.id.et_otp_four:
                    manageInputFieldState(text, etOtpFour, etOtpFive, viewRoundFour);
                    break;
                case R.id.et_otp_five:
                    manageInputFieldState(text, etOtpFive, etOtpSix, viewRoundFive);

                    break;
                case R.id.et_otp_six:
                    manageInputFieldState(text, etOtpFive, etOtpSix, viewRoundSix);
                    break;
            }

            enableOrDisableButton();

        }
    }


    private void manageInputFieldState(String text, EditText previousEditText, EditText nextEditText, View viewRound) {
        if (text.length() == 1) {
            nextEditText.requestFocus();
            viewRound.setVisibility(View.GONE);
        } else if (text.length() > 1) {
            removeCallback(nextEditText);
            removeCallback(previousEditText);
            nextEditText.requestFocus();
            previousEditText.setText(text.substring(0, 1));
            nextEditText.setText(text.substring(1));
            nextEditText.setSelection(nextEditText.getText().length());
            setTextWatcherOnEditText();
            viewRound.setVisibility(View.GONE);
        } else viewRound.setVisibility(View.VISIBLE);
    }

    /**
     * @param editText for which listener has to be remove
     */
    private void removeCallback(EditText editText) {
        switch (editText.getId()) {
            case R.id.et_otp_one:
                editText.removeTextChangedListener(et1Watcher);
                break;
            case R.id.et_otp_two:
                editText.removeTextChangedListener(et2Watcher);

                break;
            case R.id.et_otp_three:
                editText.removeTextChangedListener(et3Watcher);
                break;
            case R.id.et_otp_four:
                editText.removeTextChangedListener(et4Watcher);
                break;
            case R.id.et_otp_five:
                editText.removeTextChangedListener(et5Watcher);

                break;
            case R.id.et_otp_six:
                editText.removeTextChangedListener(et6Watcher);
                break;
        }
    }

    /**
     * Enables or disables {@link #btnVerify} based on if complete OTP is entered by user or not
     */
    private void enableOrDisableButton() {
        if (etOtpOne.getText().toString().trim().equals("") ||
                etOtpTwo.getText().toString().trim().equals("") ||
                etOtpThree.getText().toString().trim().equals("") ||
                etOtpFour.getText().toString().trim().equals("") ||
                etOtpFive.getText().toString().trim().equals("") ||
                etOtpSix.getText().toString().trim().equals("")) {

            btnVerify.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
            btnVerify.setClickable(false);
        } else {
            btnVerify.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
            btnVerify.setClickable(true);
        }
    }

    private void setSpannableStringToResendOtp() {
        Spannable spannableString = new SpannableString(getString(R.string.s_resend));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.setBackground(null);

                //resend code to registered number
                mVerifyOtpViewModel.resendOtp();

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
                ds.linkColor = R.color.colorPrimary;
            }
        };
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(tvResendCode.getContext(), R.color.colorGreenishTeal)), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvResendCode.setText(TextUtils.concat(getString(R.string.s_did_n_t_received_otp_resend_otp), spannableString));
        tvResendCode.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private void observeLiveData() {

        mVerifyOtpViewModel.getVerificationLiveData().observe(getViewLifecycleOwner(), otpVerificationResponse -> {
            hideProgressDialog();
            if (otpVerificationResponse != null) {
                if (mVerifyOtpViewModel.getPhoneNumber() != null)
                    mHost.openHomeScreen();
                else
                    mHost.steerToResetPasswordScreen(otpVerificationResponse.getResult().getResetToken());
            }

        });

        mVerifyOtpViewModel.getResendLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            showSnackBar(R.string.s_otp_sent_successfully);
        });
    }


    @Override
    protected void initViewsAndVariables() {
        btnVerify.setClickable(false);
        ivBack.setVisibility(View.GONE);
        tvTittle.setText(R.string.s_enter_otp);
        tvSubtittle.setVisibility(View.VISIBLE);
        if (mPhoneNumber != null)
            tvSubtittle.setText(String.format("%s %s", getString(R.string.s_a_verification_code_has_been_send_to_you), mPhoneNumber));

    }


    @OnClick(R.id.btn_verify)
    public void onViewClicked() {
        showProgressDialog();
        mVerifyOtpViewModel.onVerifyViewClicked(getCompleteOtp(), mPhoneNumber);
    }

    /**
     * Get complete OTP the user has entered
     *
     * @return String that contains Otp entered by user
     */
    private String getCompleteOtp() {
        return etOtpOne.getText().toString() +
                etOtpTwo.getText().toString() +
                etOtpThree.getText().toString() +
                etOtpFour.getText().toString() +
                etOtpFive.getText().toString() +
                etOtpSix.getText().toString();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public interface IVerifyOtpHost {


        void steerToResetPasswordScreen(String resetToken);

        void openHomeScreen();
    }

}
