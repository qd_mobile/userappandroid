package com.qd.user.ui.onboard.login;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.ui.onboard.OnBoardActivity;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends BaseFragment implements TextWatcher {


    private Unbinder unbinder;
    @BindView(R.id.tv_password_visibility)
    TextView tvPasswordVisibility;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.tv_subtittle)
    TextView tvSubtitle;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.tv_email)
    TextInputLayout tvEmail;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tv_password)
    TextInputLayout tvPassword;
    @BindView(R.id.view_password)
    View viewPassword;
    @BindView(R.id.tv_error_password)
    TextView tvErrorPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;
    @BindView(R.id.login_bg)
    ImageView loginBg;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    /**
     * A {@link ILoginHost} object to interact with the host{@link OnBoardActivity}
     * if any action has to be performed from the host.
     */
    private ILoginHost mLoginHost;

    /**
     * A {@link LoginViewModel} object to handle all the actions and business logic of login
     */
    private LoginViewModel mLoginViewModel;
    private boolean isPasswordVisible;
    private CreateOrderRequest mCreateOrderDetails;

    public static LoginFragment getInstance(Parcelable createOrderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, createOrderDetails);
        LoginFragment loginFragment = new LoginFragment();
        loginFragment.setArguments(bundle);
        return loginFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ILoginHost) {
            mLoginHost = (ILoginHost) context;
        } else
            throw new IllegalStateException("host must implement ILoginHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLoginHost.exitFullScreenMode();
        // Inflate the layout for this fragment_order_status
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        mLoginViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        if (getActivity() != null && !getActivity().isFinishing())
            FirebaseApp.initializeApp(getActivity());

        observeLiveData();
        setListeners();
        getArgumentsData();

    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.CREATE_ORDER_DETAILS)) {
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
        }
    }

    @Override
    protected void initViewsAndVariables() {
        tvTittle.setText(R.string.login);
        btnLogin.setEnabled(false);
        btnLogin.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
    }

    private void setListeners() {
        etEmail.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
    }

    /**
     * Method to observe all live data objects set on the view
     */
    private void observeLiveData() {

        //observing validation live data
        mLoginViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_EMAIL:
                        showEmailValidationError(validationErrors.getValidationMessage());
                        focusEmailInputField();

                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PASSWORD:
                        showPasswordValidationError(validationErrors.getValidationMessage());
                        focusPasswordInputField();
                        break;
                }
            }
        });

        mLoginViewModel.getLoginLiveData().observe(getViewLifecycleOwner(), loginCommonResponse -> {
            if (loginCommonResponse != null) {
                hideProgressDialog();
                if (loginCommonResponse.getResult().getUserDetails().isOtpVerified()) {
                    mLoginViewModel.setUserVerified(true);
                    if (mCreateOrderDetails != null) {
                        mLoginHost.backToOrderSummary();
                    } else
                        mLoginHost.openHomeScreen();
                } else openOtpSentDialog();
            }

        });
    }

    private void openOtpSentDialog() {
        if (getActivity() != null) {
            final Dialog otpSentDialog = new Dialog(getActivity(), R.style.customDialog);
            otpSentDialog.setContentView(R.layout.dialog_forgot_password);
            AppUtils.getInstance().dimDialogBackground(otpSentDialog);
            Button btnOk = otpSentDialog.findViewById(R.id.btn_ok);
            ImageView ivCodeSend = otpSentDialog.findViewById(R.id.iv_resent_link);
            TextView tvMessage = otpSentDialog.findViewById(R.id.tv_message);
            tvMessage.setText(R.string.s_verification_code_send_to_your_phone_number);
            ivCodeSend.setImageDrawable(getResources().getDrawable(R.drawable.ic_login_code_sent));
            otpSentDialog.show();

            btnOk.setOnClickListener(v -> {
                otpSentDialog.dismiss();
                mLoginHost.openVerifyOtpScreen(null);
            });

        }
    }

    private void hideErrorFields() {
        tvErrorEmail.setVisibility(View.GONE);
        tvErrorPassword.setVisibility(View.GONE);
    }

    //show error message of invalid Username
    private void showEmailValidationError(int message) {
        AppUtils.getInstance().shakeLayout(tvErrorEmail);
        tvErrorEmail.setText(message);
        tvErrorEmail.setVisibility(View.VISIBLE);
    }

    //focus email address field
    private void focusEmailInputField() {
        etEmail.setSelection(etEmail.getText().toString().length());
        etEmail.requestFocus();
    }

    //show error message of invalid password
    private void showPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(tvErrorPassword);
        tvErrorPassword.setText(errorMessage);
        tvErrorPassword.setVisibility(View.VISIBLE);
    }

    //focus password field
    private void focusPasswordInputField() {
        etPassword.setSelection(etPassword.getText().toString().length());
        etPassword.requestFocus();
    }


    @Override
    protected void onErrorOccurred(Throwable throwable) {
        super.onErrorOccurred(throwable);
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (etEmail.getText().toString().trim().length() > 2 && etPassword.getText().toString().trim().length() >= 6) {
            btnLogin.setEnabled(true);
            btnLogin.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        } else {
            btnLogin.setEnabled(false);
            btnLogin.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
        }


        if (etPassword.getText().toString().trim().length() > 0) {
            tvPasswordVisibility.setVisibility(View.VISIBLE);
        } else tvPasswordVisibility.setVisibility(View.GONE);
    }

    @OnClick({R.id.iv_back, R.id.btn_login, R.id.tv_password_visibility,R.id.tv_forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mLoginHost.onBackPressed();
                break;
            case R.id.btn_login:
                hideErrorFields();
                mLoginViewModel.loginButtonClicked(etEmail.getText().toString().toLowerCase(), etPassword.getText().toString(), getDeviceId());
                break;
            case R.id.tv_password_visibility:
                if (isPasswordVisible) {
                    tvPasswordVisibility.setText(R.string.s_hide);
                    etPassword.setTransformationMethod(null);
                    etPassword.setSelection(etPassword.getText().length());
                    isPasswordVisible = false;
                } else {
                    tvPasswordVisibility.setText(R.string.s_show);
                    etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etPassword.setSelection(etPassword.getText().length());
                    isPasswordVisible = true;
                }
                break;
            case R.id.tv_forgot_password:
                mLoginHost.steerToForgotPasswordScreen();
        }
    }

    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        showSnackBar(getString(R.string.s_no_internet_connection));
    }

    /**
     * This interface is used to interact with the host {@link OnBoardActivity}
     */
    public interface ILoginHost {
        void exitFullScreenMode();

        void openHomeScreen();

        void steerToForgotPasswordScreen();

        void onBackPressed();

        void backToOrderSummary();

        void openVerifyOtpScreen(String phoneNumber);
    }
}
