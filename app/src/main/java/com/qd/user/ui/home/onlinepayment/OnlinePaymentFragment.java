package com.qd.user.ui.home.onlinepayment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.payments.wallet.InitializePaymentRequest;
import com.qd.user.ui.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OnlinePaymentFragment extends BaseFragment {


    private static final int DELAY_MILLIS = 3000;
    private static final int START_OFFSET = 20;
    private static final int DURATION_MILLIS = 1500;
    private Unbinder unbinder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.wv_online_payment)
    WebView wvOnlinePayment;
    @BindView(R.id.tv_processing)
    TextView tvProcessing;


    /**
     * A {@link IOnlinePaymentHost} object to interact with the host{@link HomeActivity}
     * if any action has to be performed from the host.
     */
    private IOnlinePaymentHost mOnlinePaymentHost;

    /**
     * A {@link OnlinePaymentViewModel} object to handle all the actions and business logic of orders listing.
     */
    private OnlinePaymentViewModel mOnlinePaymentViewModel;
    private InitializePaymentRequest mPaymentRequest;
    private boolean isResultFetched;

    public static OnlinePaymentFragment getInstance(Parcelable request, String url) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.PAYMENT_REQUEST, request);
        bundle.putString(AppConstants.PAYMENT_URL, url);
        OnlinePaymentFragment onlinePaymentFragment = new OnlinePaymentFragment();
        onlinePaymentFragment.setArguments(bundle);
        return onlinePaymentFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IOnlinePaymentHost) {
            mOnlinePaymentHost = (IOnlinePaymentHost) context;
        } else
            throw new IllegalStateException("host must implement IOnlinePaymentHost");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online_payment, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mOnlinePaymentViewModel = new ViewModelProvider(this).get(OnlinePaymentViewModel.class);
        mOnlinePaymentViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        initializePayment();
        observeLiveData();


    }

    private void initializePayment() {
        if (mPaymentRequest != null && (getArguments() != null ? getArguments().getString(AppConstants.PAYMENT_URL) : null) == null)
            mOnlinePaymentViewModel.initializePayment(mPaymentRequest);
    }

    private void observeLiveData() {
        mOnlinePaymentViewModel.getInitializePaymentLiveData().observe(getViewLifecycleOwner(), walletRechargeResponse -> {
            if (walletRechargeResponse != null)
                loadWebUrl(walletRechargeResponse.getResult());
        });
    }


    @Override
    protected void initViewsAndVariables() {
        showProgressDialog();
        getArgumentsData();
        tvTitle.setText(getString(R.string.payment));
        ivNotification.setVisibility(View.GONE);
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            mPaymentRequest = getArguments().getParcelable(AppConstants.PAYMENT_REQUEST);
            if (getArguments().getString(AppConstants.PAYMENT_URL) != null)
                loadWebUrl(getArguments().getString(AppConstants.PAYMENT_URL));
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadWebUrl(String url) {

        wvOnlinePayment.setWebViewClient(new WebViewClient() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError err) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, err.getErrorCode(), err.getDescription().toString(), req.getUrl().toString());
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                hideProgressDialog();
                blinkAnimation();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                tvProcessing.clearAnimation();
                tvProcessing.setVisibility(View.GONE);
                Log.e("url", "onPageFinished: " + url);

                if (url.contains("status")) {
                    if (!isResultFetched) {
                        switch (mPaymentRequest.getTransactionFor()) {
                            case AppConstants.TransactionFor.ORDER:
                                if (url.contains("failed")) {
                                    mOnlinePaymentHost.onOrderPayment(mPaymentRequest.getOrderId(), false);
                                } else if (url.contains("transactionId")) {
                                    mOnlinePaymentHost.onOrderPayment(mPaymentRequest.getOrderId(), true);
                                }
                                break;
                            default:
                                navigateBack();
                        }
                    }

                }

            }

            private void blinkAnimation() {
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(DURATION_MILLIS); //You can manage the blinking time with this parameter
                anim.setStartOffset(START_OFFSET);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                tvProcessing.setVisibility(View.VISIBLE);
                tvProcessing.startAnimation(anim);
            }


            private void navigateBack() {
                isResultFetched = true;
                Handler handler = new Handler();
                handler.postDelayed(() -> mOnlinePaymentHost.onBackPressed(), DELAY_MILLIS);

            }


        });

        wvOnlinePayment.getSettings().setJavaScriptEnabled(true);
        wvOnlinePayment.getSettings().setDomStorageEnabled(true);

        wvOnlinePayment.loadUrl(url);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.iv_back)
    public void onViewClicked() {
        mOnlinePaymentHost.onBackPressed();
    }


    public interface IOnlinePaymentHost {
        void onBackPressed();

        void onOrderPayment(String orderId, boolean paymentStatus);
    }
}

