package com.qd.user.ui.onboard.resetpassword;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ResetPasswordFragment extends BaseFragment implements TextWatcher ,AppUtils.ISuccess{
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_old_password)
    EditText etOldPassword;
    @BindView(R.id.til_old_password)
    TextInputLayout tilOldPassword;
    @BindView(R.id.view_old_password)
    View viewOldPassword;
    @BindView(R.id.tv_error_old_password)
    TextView tvErrorOldPassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.til_new_password)
    TextInputLayout tilNewPassword;
    @BindView(R.id.tv_new_password_visibiity)
    TextView tvNewPasswordVisibiity;
    @BindView(R.id.view_new_password)
    View viewNewPassword;
    @BindView(R.id.tv_error_new_password)
    TextView tvErrorNewPassword;
    @BindView(R.id.et_confirm_new_password)
    EditText etConfirmNewPassword;
    @BindView(R.id.til_confirm_new_password)
    TextInputLayout tilConfirmNewPassword;
    @BindView(R.id.tv_confirm_password_visibility)
    TextView tvConfirmPasswordVisibility;
    @BindView(R.id.view_confirm_password)
    View viewConfirmPassword;
    @BindView(R.id.tv_error_confirm_password)
    TextView tvErrorConfirmPassword;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    private Unbinder unbinder;
    private IResetPasswordHost mHost;
    private ResetPasswordViewModel mResetPasswordViewModel;
    private String resetToken;
    private boolean isConfirmNewPasswordvisible;
    private boolean isNewPasswordvisible;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mResetPasswordViewModel = new ViewModelProvider(this).get(ResetPasswordViewModel.class);
        mResetPasswordViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setListeners();
        getArgumentsData();

    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.RESET_TOKEN) && getArguments().getString(AppConstants.RESET_TOKEN) != null) {
            resetToken = getArguments().getString(AppConstants.RESET_TOKEN);
        }

    }

    @Override
    protected void initViewsAndVariables() {
     btnSave.setText(R.string.s_submit);
        btnSave.setClickable(false);
        tvTitle.setText(R.string.s_reset_password);
     tilOldPassword.setVisibility(View.GONE);
     tvErrorOldPassword.setVisibility(View.GONE);
     viewOldPassword.setVisibility(View.GONE);

    }

    private void setListeners() {
        etNewPassword.addTextChangedListener(this);
        etConfirmNewPassword.addTextChangedListener(this);

    }

    private void observeLiveData() {
        mResetPasswordViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_NEW_PASSWORD:
                        showNewPasswordValidationError(validationErrors.getValidationMessage());
                        focusNewPasswordInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_CONFIRM_NEW_PASSWORD:
                        showConfirmPasswordValidationError(validationErrors.getValidationMessage());
                        focusConfirmPasswordInputField();
                        break;
                }
            }
        });
        mResetPasswordViewModel.getResetPasswordLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            if (commonResponse != null) {
                hideProgressDialog();
                AppUtils.getInstance().openSuccessDialog(getActivity(), getString(R.string.s_password_has_been_reset_successfully), ResetPasswordFragment.this);

            }
        });
    }

    private void focusConfirmPasswordInputField() {
        etConfirmNewPassword.setSelection(etConfirmNewPassword.getText().length());
        etConfirmNewPassword.requestFocus();
    }

    private void showConfirmPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(etConfirmNewPassword);
        tvErrorConfirmPassword.setText(errorMessage);
        tvErrorConfirmPassword.setVisibility(View.VISIBLE); }

    private void focusNewPasswordInputField() {
        etNewPassword.setSelection(etNewPassword.getText().length());
        etNewPassword.requestFocus();
    }

    private void showNewPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(etNewPassword);
        tvErrorNewPassword.setText(errorMessage);
        tvErrorNewPassword.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.iv_back, R.id.btn_save, R.id.tv_confirm_password_visibility, R.id.tv_new_password_visibiity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_save:
                hideErrorViews();
                mResetPasswordViewModel.onSubmitViewClick(etNewPassword.getText().toString().trim(), etConfirmNewPassword.getText().toString(), resetToken);
                break;
            case R.id.tv_confirm_password_visibility:
                if (isConfirmNewPasswordvisible) {
                    tvConfirmPasswordVisibility.setText(R.string.s_hide);
                    etConfirmNewPassword.setTransformationMethod(null);
                    etConfirmNewPassword.setSelection(etConfirmNewPassword.getText().length());
                    isConfirmNewPasswordvisible = false;
                } else {
                    tvConfirmPasswordVisibility.setText(R.string.s_show);
                    etConfirmNewPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etConfirmNewPassword.setSelection(etConfirmNewPassword.getText().length());
                    isConfirmNewPasswordvisible = true;
                }
                break;
            case R.id.tv_new_password_visibiity:
                if (isNewPasswordvisible) {
                    tvNewPasswordVisibiity.setText(R.string.s_hide);
                    etNewPassword.setTransformationMethod(null);
                    etNewPassword.setSelection(etNewPassword.getText().length());
                    isNewPasswordvisible = false;
                } else {
                    tvNewPasswordVisibiity.setText(R.string.s_show);
                    etNewPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etNewPassword.setSelection(etNewPassword.getText().length());
                    isNewPasswordvisible = true;
                }
                break;
        }
    }

    private void hideErrorViews() {
        tvErrorNewPassword.setVisibility(View.GONE);
        tvErrorConfirmPassword.setVisibility(View.GONE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!etNewPassword.getText().toString().isEmpty() && !etConfirmNewPassword.getText().toString().isEmpty()) {
            btnSave.setClickable(true);
            btnSave.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        } else {
            btnSave.setClickable(false);
            btnSave.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
        }
        if (etNewPassword.getText().toString().trim().length() > 0) {
            tvNewPasswordVisibiity.setVisibility(View.VISIBLE);
        } else tvNewPasswordVisibiity.setVisibility(View.GONE);

        if (etConfirmNewPassword.getText().toString().trim().length() > 0) {
            tvConfirmPasswordVisibility.setVisibility(View.VISIBLE);
        } else tvConfirmPasswordVisibility.setVisibility(View.GONE);
    }

    @Override
    public void onOkViewClicked() {
        mHost.steerToLogInFragment(null);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        if (context instanceof IResetPasswordHost){
            mHost=(IResetPasswordHost)context;
        }
        else throw new IllegalStateException("Host must implement IResetPasswordHost");
        super.onAttach(context);
    }

    public static ResetPasswordFragment getInstance(String resetToken) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.RESET_TOKEN, resetToken);
        ResetPasswordFragment resetPasswordFragment = new ResetPasswordFragment();
        resetPasswordFragment.setArguments(bundle);
        return resetPasswordFragment;
    }

    public interface IResetPasswordHost{

        void onBackPressed();

        void steerToLogInFragment(CreateOrderRequest createOrderRequest);
    }
}
