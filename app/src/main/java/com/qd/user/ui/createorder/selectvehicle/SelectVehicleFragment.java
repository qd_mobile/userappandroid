package com.qd.user.ui.createorder.selectvehicle;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.distancetime.eta.VehicleCategory;
import com.qd.user.model.createorder.optimizedroute.DropLocation;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.Origin;
import com.qd.user.model.createorder.request.CreateOrderRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SelectVehicleFragment extends BaseFragment implements SelectVehicleAdapter.VehiclesListAdapterInterface {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.view_toolbar)
    View viewToolbar;
    @BindView(R.id.tv_label_choose_vehicle_type)
    TextView tvLabelChooseVehicleType;
    @BindView(R.id.rv_vehicle_types)
    RecyclerView rvVehicleTypes;
    @BindView(R.id.btn_select)
    Button btnSelect;
    private Unbinder unbinder;
    private ISelectVehicleHost mHost;

    private ArrayList<VehicleCategory> mSelectVehicleList;
    private VehicleCategory mVehicle;
    private SelectVehicleViewModel mSelectVehicleViewModel;
    private CreateOrderRequest mCreateOrderData;
    private BigDecimal mDeliveryCharges;
    private VehicleCategory mSelectedVehicle;
    private SelectVehicleAdapter mAdapter;

    public SelectVehicleFragment() {
        // Required empty public constructor
    }

    public static SelectVehicleFragment getInstance(ArrayList<VehicleCategory> vehicleType, Parcelable createOrderDetails) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(AppConstants.VEHICLE_TYPE, vehicleType);
        bundle.putParcelable(AppConstants.ORDER_DATA, createOrderDetails);
        SelectVehicleFragment itemsListFragment = new SelectVehicleFragment();
        itemsListFragment.setArguments(bundle);
        return itemsListFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_choose_vehicle_type, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ISelectVehicleHost) {
            mHost = (ISelectVehicleHost) context;
        } else throw new IllegalStateException("Host must implement ISelectVehicleHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSelectVehicleViewModel = new ViewModelProvider(this).get(SelectVehicleViewModel.class);
        mSelectVehicleViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        //mSelectVehicleViewModel.getVehicleCategories();
        observeLiveData();

    }

    private void observeLiveData() {
        mSelectVehicleViewModel.getOptimizedDeliveryLiveData().observe(getViewLifecycleOwner(), optimizedDeliveryResponse -> {
            hideProgressDialog();
            if (optimizedDeliveryResponse != null) {
                mSelectedVehicle = mVehicle;
                mDeliveryCharges = optimizedDeliveryResponse.getResult().get(0).getDeliveryCharge();
            }
        });
    }
/*

    private void observeLiveData() {

        mSelectVehicleViewModel.getVehicleCategoryLiveData().observe(getViewLifecycleOwner(), new Observer<VehicleCategoryResponse>() {
            @Override
            public void onChanged(@Nullable VehicleCategoryResponse vehicleCategoryResponse) {
                if (vehicleCategoryResponse != null) {
                    mSelectVehicleList.addAll(vehicleCategoryResponse.getResult());
                    for(int i=0; i<vehicleCategoryResponse.getResult().size();i++){
                        if(vehicleCategoryResponse.getResult().get(i).getKeyword().equals(mVehicleType)){
                            mSelectVehicleList.get(i).setSelected(true);
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            }
        });

    }
*/

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(getString(R.string.s_add_item));
        mSelectVehicleList = new ArrayList<>();
        getArgumentsData();
        mSelectedVehicle = mVehicle;
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.VEHICLE_TYPE)
                && getArguments().getParcelable(AppConstants.ORDER_DATA) != null) {
            mSelectVehicleList = getArguments().getParcelableArrayList(AppConstants.VEHICLE_TYPE);
            mCreateOrderData = getArguments().getParcelable(AppConstants.ORDER_DATA);

            if (mSelectVehicleList != null && mSelectVehicleList.size() > 0)
                mVehicle = getSelectedVehicle(mSelectVehicleList);
            setUpAdapter();
        }
    }

    private void setUpAdapter() {
        mAdapter = new SelectVehicleAdapter(mSelectVehicleList, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rvVehicleTypes.getContext(), LinearLayoutManager.VERTICAL, false);
        rvVehicleTypes.setLayoutManager(linearLayoutManager);
        rvVehicleTypes.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_select})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_select:
                if (mSelectedVehicle != null)
                    mHost.backToAddItemFragment(mVehicle, mDeliveryCharges);
                else {
                    showProgressDialog();
                    mSelectVehicleViewModel.getChangedDeliveryVehicle(getDataForOptimizedRoute(mCreateOrderData, mVehicle.getCategoryName()));
                }
                break;
        }

    }


    private VehicleCategory getSelectedVehicle(ArrayList<VehicleCategory> selectVehicleList) {
        for (int i = 0; i < selectVehicleList.size(); i++) {
            if (selectVehicleList.get(i).isSelected()) {
                return selectVehicleList.get(i);
            }
        }

        return null;
    }


    @Override
    public void onVehicleSelected(VehicleCategory vehicle) {
        if (!vehicle.getEta().equals("N/A") || mCreateOrderData.getOrderType().equals(AppConstants.OrderType.BUSINESS)) {
            if (vehicle != mVehicle) {
                showProgressDialog();
                mSelectedVehicle = null;
                mSelectVehicleViewModel.getChangedDeliveryVehicle(getDataForOptimizedRoute(mCreateOrderData, vehicle.getCategoryName()));
            }
            mVehicle = vehicle;
        } else {
            for (int i = 0; i < mSelectVehicleList.size(); i++) {
                if (mSelectVehicleList.get(i).getId().equals(mVehicle.getId()))
                    mSelectVehicleList.get(i).setSelected(true);
                else mSelectVehicleList.get(i).setSelected(false);
            }
            mAdapter.notifyDataSetChanged();
            showSnackBar(getString(R.string.driver_not_available));
        }
    }

    /**
     * Creates a request object to find out delivery charges for selected pickup and drop location
     *
     * @param createOrderRequest Create Order Request object that contains all the data filled by the user so far
     * @param vehicle            Vehicle selected for which delivery fare has to be calculated
     * @return Request object to make network call to get required delivery charges
     */
    private OptimizedDeliveryRequest getDataForOptimizedRoute(CreateOrderRequest createOrderRequest, String vehicle) {
        OptimizedDeliveryRequest optimizedDeliveryRequest = new OptimizedDeliveryRequest();
        Origin origin = new Origin();
        origin.setLat(createOrderRequest.getPickupLocation().get(0).getGeometry().getCoordinates().get(1));
        origin.setLng(createOrderRequest.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
        optimizedDeliveryRequest.setOrigin(origin);

        DropLocation dropLocation = new DropLocation();
        dropLocation.setLat(createOrderRequest.getDropOffArr().get(createOrderRequest.getDropOffArr().size() - 1).getGeometry().getCoordinates().get(1));
        dropLocation.setLng(createOrderRequest.getDropOffArr().get(createOrderRequest.getDropOffArr().size() - 1).getGeometry().getCoordinates().get(0));

        List<DropLocation> dropLocationList = new ArrayList<>();
        dropLocationList.add(dropLocation);
        optimizedDeliveryRequest.setDropLocation(dropLocationList);

        optimizedDeliveryRequest.setServiceType(createOrderRequest.getServiceType());

        if (createOrderRequest.getOrderType().equals(AppConstants.OrderType.SEND) || createOrderRequest.getOrderType().equals(AppConstants.OrderType.SPECIAL_SEND))
            optimizedDeliveryRequest.setOrderType(AppConstants.OrderType.SPECIAL_SEND);
        else
            optimizedDeliveryRequest.setOrderType(createOrderRequest.getOrderType());

        optimizedDeliveryRequest.setVehicleType(vehicle);
        optimizedDeliveryRequest.setUserType(createOrderRequest.getRequestFrom());
        optimizedDeliveryRequest.setOptimize(true);

        return optimizedDeliveryRequest;
    }


    public interface ISelectVehicleHost {
        void onBackPressed();

        void backToAddItemFragment(VehicleCategory vehicle, BigDecimal deliveryCharges);
    }
}
