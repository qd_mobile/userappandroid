package com.qd.user.ui.home.notifications;

import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.qd.user.R;
import com.qd.user.model.notifications.Datum;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.ProgressLoader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int VIEW_TYPE_ROW = 1;
    private static final int VIEW_TYPE_LOADER = 2;

    private ArrayList<Object> mNotificationsList;
    private INotificationInterface mInterface;


    NotificationsAdapter(ArrayList<Object> walletTransactionsList, INotificationInterface iNotificationInterface) {
        mNotificationsList = walletTransactionsList;
        mInterface = iNotificationInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == VIEW_TYPE_ROW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notifications, viewGroup, false);
            return new NotificationsAdapterViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progress_loader, viewGroup, false);
            return new LoaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (mNotificationsList.get(i) instanceof ProgressLoader) {
            ((LoaderViewHolder) holder).progress.setVisibility(View.VISIBLE);

        } else if (mNotificationsList.get(i) instanceof Datum) {
            ((NotificationsAdapterViewHolder) holder).tvNotificationTittle.setText(((Datum) mNotificationsList.get(i)).getNotificaitonTitle());
            ((NotificationsAdapterViewHolder) holder).tvNotificationSubTitle.setText(((Datum) mNotificationsList.get(i)).getNotificationMessage());

            if (((Datum) mNotificationsList.get(i)).isIsRead())
                ((NotificationsAdapterViewHolder) holder).clNotification.setBackgroundColor(((NotificationsAdapterViewHolder) holder).clNotification
                        .getResources().getColor(R.color.colorWhite));
            else
                ((NotificationsAdapterViewHolder) holder).clNotification.setBackgroundColor(((NotificationsAdapterViewHolder) holder).clNotification
                        .getResources().getColor(R.color.colorTransparentGreenishTeal));

            // ((NotificationsAdapterViewHolder) holder).tvNotificationTime.setText(getNotificationTime(((Datum) mNotificationsList.get(i)).getCreatedAt()));

            ((NotificationsAdapterViewHolder) holder).tvNotificationTime.setText(DateUtils.getRelativeTimeSpanString
                    (DateFormatter.getInstance().getParsedDate(((Datum) mNotificationsList.get(i)).getCreatedAt()).getTime(),
                            System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));

        }
    }

    private String getNotificationTime(String createdAt) {
        Date date = DateFormatter.getInstance().getParsedDate(createdAt);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(new Date());

        if (currentCalendar.get(Calendar.YEAR) - calendar.get(Calendar.YEAR) == 0) {
            if (currentCalendar.get(Calendar.MONTH) - calendar.get(Calendar.MONTH) == 0) {
                switch (currentCalendar.get(Calendar.DAY_OF_MONTH) - calendar.get(Calendar.DAY_OF_MONTH)) {
                    case 0:
                        if (currentCalendar.get(Calendar.HOUR_OF_DAY) - calendar.get(Calendar.HOUR_OF_DAY) == 0) {
                            if (currentCalendar.get(Calendar.MINUTE) - calendar.get(Calendar.MINUTE) == 0) {
                                return currentCalendar.get(Calendar.SECOND) - calendar.get(Calendar.SECOND) + " seconds ago";
                            }
                            return currentCalendar.get(Calendar.MINUTE) - calendar.get(Calendar.MINUTE) + " minutes ago";
                        }
                        return DateFormatter.getInstance().getFormattedTime(createdAt);
                    case 1:
                        return "Yesterday";
                    default:
                        return currentCalendar.get(Calendar.DAY_OF_MONTH) - calendar.get(Calendar.DAY_OF_MONTH) + " days ago";
                }
            }
            return currentCalendar.get(Calendar.MONTH) - calendar.get(Calendar.MONTH) + " months ago";
        }
        return DateFormatter.getInstance().getFormattedDate(createdAt);
    }

    @Override
    public int getItemCount() {
        return mNotificationsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mNotificationsList.get(position) instanceof ProgressLoader) {
            return VIEW_TYPE_LOADER;
        }
        return VIEW_TYPE_ROW;
    }

    //hide progress loader after getting paginated data
    void hideProgress() {
        mNotificationsList.remove(mNotificationsList.size() - 1);
        notifyItemRemoved(mNotificationsList.size());
    }

    //show progress loader for getting paginated data
    void showProgress() {
        mNotificationsList.add(new ProgressLoader());
        notifyItemInserted(mNotificationsList.size());
    }

    interface INotificationInterface {

        void onNotificationClick(Datum datum);
    }

    class LoaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress)
        ProgressBar progress;


        LoaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    class NotificationsAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_notification)
        ImageView ivNotification;
        @BindView(R.id.cl_notification)
        ConstraintLayout clNotification;
        @BindView(R.id.tv_notification_tittle)
        TextView tvNotificationTittle;
        @BindView(R.id.tv_notification_sub_title)
        TextView tvNotificationSubTitle;
        @BindView(R.id.tv_notification_time)
        TextView tvNotificationTime;

        NotificationsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v ->
                    mInterface.onNotificationClick(((Datum) mNotificationsList.get(getAdapterPosition()))));
        }
    }

}
