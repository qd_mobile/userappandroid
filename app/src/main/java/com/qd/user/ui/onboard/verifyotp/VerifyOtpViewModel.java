package com.qd.user.ui.onboard.verifyotp;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.otpverification.OtpVerificationResponse;

public class VerifyOtpViewModel extends ViewModel {
    private RichMediatorLiveData<OtpVerificationResponse> mVerificationLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> loadingStateObserver;
    private RichMediatorLiveData<CommonResponse> mResendOtpLiveData;

    private VerifyOtpRepository mRepo = new VerifyOtpRepository();


    /**
     * This method gives the verification live data object to {@link VerifyOtpFragment}
     *
     * @return {@link #mVerificationLiveData}
     */
    public LiveData<OtpVerificationResponse> getVerificationLiveData() {
        return mVerificationLiveData;
    }

    /**
     * This method gives the resend live data object to {@link VerifyOtpFragment}
     *
     * @return {@link #mResendOtpLiveData}
     */
    public LiveData<CommonResponse> getResendLiveData() {
        return mResendOtpLiveData;
    }


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.loadingStateObserver = loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mVerificationLiveData == null) {
            mVerificationLiveData = new RichMediatorLiveData<OtpVerificationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return loadingStateObserver;
                }
            };
        }

        if (mResendOtpLiveData == null) {
            mResendOtpLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return loadingStateObserver;
                }
            };
        }

    }


    void onVerifyViewClicked(String completeOtp, String phoneNumber) {
        mRepo.hitValidateOtpApi(completeOtp, phoneNumber, mVerificationLiveData);
    }

    public String getPhoneNumber() {
        return mRepo.getPhoneNumber();
    }

    public void resendOtp() {
        mRepo.resendOtp(mResendOtpLiveData);
    }
}
