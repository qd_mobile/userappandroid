package com.qd.user.ui.approveReturn;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;

import org.json.JSONObject;

public class ApproveReturnViewModel extends ViewModel {
    private RichMediatorLiveData<OrderCancellationResponse> mApproveReturnLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;
    private ApproveReturnRepository mRepo = new ApproveReturnRepository();
    private RichMediatorLiveData<OrderCancellationResponse> mOrderCancellationLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mApproveReturnLiveData == null) {
            mApproveReturnLiveData = new RichMediatorLiveData<OrderCancellationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mOrderCancellationLiveData == null) {
            mOrderCancellationLiveData = new RichMediatorLiveData<OrderCancellationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }


    }

    /**
     * This method gives the approval live data object to {@link ApproveReturnFragment}
     *
     * @return {@link #mApproveReturnLiveData}
     */
    RichMediatorLiveData<OrderCancellationResponse> getApproveReturnLiveData() {
        return mApproveReturnLiveData;
    }

    /**
     * This method gives the login live data object to {@link ApproveReturnFragment}
     *
     * @return {@link #mOrderCancellationLiveData}
     */
    RichMediatorLiveData<OrderCancellationResponse> getOrderCancellationLiveData() {
        return mOrderCancellationLiveData;
    }

    void approveOrderReturn(String elementId) {
        mApproveReturnLiveData.setLoadingState(true);
        mRepo.approveOrderReturn(elementId, mApproveReturnLiveData);
    }

    public void cancelOrder(JSONObject request) {
        mOrderCancellationLiveData.setLoadingState(true);
        mRepo.cancelOrder(request, mOrderCancellationLiveData);
    }
}
