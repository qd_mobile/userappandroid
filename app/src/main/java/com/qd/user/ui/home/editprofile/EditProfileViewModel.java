package com.qd.user.ui.home.editprofile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.R;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.createaccount.CreateAccountInfo;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.delete.DeleteSavedPlaceResponse;
import com.qd.user.model.editprofile.EditProfileResponse;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.profile.ProfileResponse;
import com.qd.user.model.validationerror.ValidationErrors;

import java.util.regex.Pattern;

public class EditProfileViewModel  extends ViewModel{
    private RichMediatorLiveData<EditProfileResponse> mEditProfileLiveData;
    private RichMediatorLiveData<SavedAddressResponse> mSavedPlacesLiveData;
    private RichMediatorLiveData<DeleteSavedPlaceResponse> mDeleteSavedLiveData;
    private RichMediatorLiveData<CommonResponse> mLogOutLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<ValidationErrors> mValidateLiveData;
    private Observer<Boolean> mLoadingStateObserver;
     private EditProfileRepository mRepo=new EditProfileRepository();
    private RichMediatorLiveData<ProfileResponse> mProfileLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver,Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver=loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if(mEditProfileLiveData ==null){
            mEditProfileLiveData = new RichMediatorLiveData<EditProfileResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if(mSavedPlacesLiveData ==null){
            mSavedPlacesLiveData = new RichMediatorLiveData<SavedAddressResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if(mDeleteSavedLiveData ==null){
            mDeleteSavedLiveData = new RichMediatorLiveData<DeleteSavedPlaceResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }
        if (mLogOutLiveData == null) {
            mLogOutLiveData = new RichMediatorLiveData<CommonResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

        if (mProfileLiveData == null) {
            mProfileLiveData = new RichMediatorLiveData<ProfileResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }


        if (mValidateLiveData == null) {
            mValidateLiveData = new MutableLiveData<>();
        }
    }
    /**
     * This method gives the login live data object to {@link EditProfileFragment}
     *
     * @return {@link #mEditProfileLiveData}
     */
    RichMediatorLiveData<EditProfileResponse> getEditProfileLiveData() {
        return mEditProfileLiveData;
    }

    /**
     * This method gives the log out live data object to {@link com.qd.user.ui.home.HomeActivity}
     *
     * @return {@link #mLogOutLiveData}
     */
    public RichMediatorLiveData<CommonResponse> getLogOutLiveData() {
        return mLogOutLiveData;
    }

    /**
     * This method gives the profile live data object to {@link com.qd.user.ui.home.profile.ProfileFragment}
     *
     * @return {@link #mProfileLiveData}
     */
    public RichMediatorLiveData<ProfileResponse> getProfileLiveData() {
        return mProfileLiveData;
    }


    public LiveData<ValidationErrors> getValidationLiveData() {
        return mValidateLiveData;
    }


    void onSaveViewClicked(CreateAccountInfo editUserInfo) {
        if (validateInputField(editUserInfo)){
            mRepo.hitEditProfileApi(editUserInfo,mEditProfileLiveData);
        }
    }
    //validate input fields
    private boolean validateInputField(CreateAccountInfo editUserInfo) {
        if (!Pattern.matches("[a-zA-Z ]+", editUserInfo.getName())) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_NAME, R.string.s_please_enter_a_valid_name));
            return false;
        } else if (!editUserInfo.getPhoneNumber().matches("[0-9]{7,9}$")) {
            mValidateLiveData.setValue(new ValidationErrors(AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER, R.string.s_please_enter_valid_phone_number));
            return false;
        }
        return true;
    }
    /**
     * This method gives the login live data object to {@link com.qd.user.ui.home.profile.ProfileFragment}
     *
     * @return {@link #mEditProfileLiveData}
     */
    public RichMediatorLiveData<SavedAddressResponse> getSavedPlacesLiveData() {
        return mSavedPlacesLiveData;
    }

    /**
     * This method gives the login live data object to {@link com.qd.user.ui.home.profile.ProfileFragment}
     *
     * @return {@link #mDeleteSavedLiveData}
     */
    public RichMediatorLiveData<DeleteSavedPlaceResponse> getDeleteSavedPlaceLiveData() {
        return mDeleteSavedLiveData;
    }


    public String getUserName() {
        return mRepo.getUserName();
    }

    public String getPhoneNumber() {
        return mRepo.getPhoneNumber();
    }

    public String getEmail() {
        return mRepo.getEmail();
    }

    public String getCountryCode() {
        return mRepo.getCountryCode();
    }

    public void getSavedPlaces() {
        mRepo.hitSavedPlacesApi(mSavedPlacesLiveData);
    }

    public void onDeleteSavedAddress(String id) {
        mRepo.deleteSavedAddress(id,mDeleteSavedLiveData);
    }

    public void clearPreference() {
        mRepo.clearPreference();
    }

    String getUserSignUpType() {
        return mRepo.getSignUpType();
    }

    public void connectSocket() {
        mRepo.connectSocket();
    }

    public void initializeSocket() {
        mRepo.initializeSocket();
    }

    public String getUserType() {
        return mRepo.getUserType();
    }

    public String getBusinessName() {
        return mRepo.getBusinessName();
    }

    public String getCompanyProfile() {
        return mRepo.getCompanyProfile();
    }

    public String getBusinessAddress() {
        return mRepo.getBusinessAddress();
    }

    public void disconnectSocket() {
        mRepo.disconnectSocket();
    }

    public void logOutUser(String deviceId) {
        mLogOutLiveData.setLoadingState(true);
        mRepo.logOutUser(mLogOutLiveData, deviceId);
    }

    public void getProfileInfo() {
        mRepo.getProfileInfo(mProfileLiveData);
    }
}
