package com.qd.user.ui.trackorder.orderdetail;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.orderdetail.Result;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.DateFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.qd.user.ui.createorder.findingdriver.FindingDriverFragment.CANCELLATION_FAILURE;

public class OrderDetailFragment extends BaseFragment implements DialogInterface.OnClickListener {

    private Unbinder unbinder;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    /*@BindView(R.id.view_toolbar_divider)
    View viewToolbarDivider;*/
    @BindView(R.id.tv_label_order_id)
    TextView tvLabelOrderId;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.view_date_and_time)
    View viewDateAndTime;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.view_my_order)
    View viewMyOrder;
    @BindView(R.id.iv_order_type)
    ImageView ivOrderType;
    @BindView(R.id.tv_order_type)
    TextView tvOrderType;
    @BindView(R.id.tv_order_status)
    TextView tvOrderStatus;
    @BindView(R.id.iv_pickup_location)
    ImageView ivPickupLocation;
    @BindView(R.id.tv_label_pickup_location)
    TextView tvLabelPickupLocation;
    @BindView(R.id.tv_pickup_location)
    TextView tvPickupLocation;
    @BindView(R.id.view_dotted_line)
    View viewDottedLine;
    @BindView(R.id.iv_drop_off_location)
    ImageView ivDropOffLocation;
    @BindView(R.id.tv_label_drop_off_location)
    TextView tvLabelDropOffLocation;
    @BindView(R.id.tv_drop_off_location)
    TextView tvDropOffLocation;
    @BindView(R.id.rl_location_info)
    RelativeLayout rlLocationInfo;
    @BindView(R.id.tv_label_product_images)
    TextView tvLabelProductImages;
    @BindView(R.id.tv_label_receipt)
    TextView tvLabelReceipt;
    @BindView(R.id.rl_product_images)
    RecyclerView rvProductImages;
    @BindView(R.id.iv_receipt)
    ImageView ivReceipt;
    @BindView(R.id.tv_label_note_for_driver)
    TextView tvLabelNoteForDriver;
    @BindView(R.id.tv_note_for_driver)
    TextView tvNoteForDriver;
    @BindView(R.id.tv_label_recipient_details)
    TextView tvLabelRecipientDetails;
    @BindView(R.id.tv_label_driver_details)
    TextView tvLabelDriverDetails;
    @BindView(R.id.tv_label_payment_mode)
    TextView tvLabelPaymentMode;
    @BindView(R.id.iv_payment_mode)
    ImageView ivPaymentMode;
    @BindView(R.id.tv_payment_mode)
    TextView tvPaymentMode;
    @BindView(R.id.tv_label_item_price)
    TextView tvLabelItemPrice;
    @BindView(R.id.tv_item_price)
    TextView tvItemPrice;
    @BindView(R.id.tv_label_delivery_charges)
    TextView tvLabelDeliveryCharges;
    @BindView(R.id.tv_delivery_charges)
    TextView tvDeliveryCharges;
    @BindView(R.id.tv_label_return_charges)
    TextView tvLabelReturnCharges;
    @BindView(R.id.tv_return_charges)
    TextView tvReturnCharges;
    @BindView(R.id.tv_label_promo_code_applied)
    TextView tvLabelPromoCodeApplied;
    @BindView(R.id.tv_promo_code_applied)
    TextView tvPromoCodeApplied;
    @BindView(R.id.tv_label_qd_promo)
    TextView tvLabelQdPromo;
    @BindView(R.id.tv_qd_promo)
    TextView tvQdPromo;
    @BindView(R.id.view_dotted_line_end)
    View viewDottedLineEnd;
    @BindView(R.id.tv_label_amount_paid)
    TextView tvLabelAmountPaid;
    @BindView(R.id.tv_amount_paid)
    TextView tvAmountPaid;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.cv_receipt)
    CardView cvReceipt;
    @BindView(R.id.tv_label_name)
    TextView tvLabelName;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_label_phone_number)
    TextView tvLabelPhoneNumber;
    @BindView(R.id.tv_phone_number)
    TextView tvPhoneNumber;
    @BindView(R.id.tv_label_email)
    TextView tvLabelEmail;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.cl_recipient_details)
    ConstraintLayout clRecipientDetails;
    @BindView(R.id.iv_driver)
    ImageView ivDriver;
    @BindView(R.id.tv_driver_name)
    TextView tvDriverName;
    @BindView(R.id.tv_driver_phone_number)
    TextView tvDriverPhoneNumber;
    @BindView(R.id.iv_vehicle_type)
    ImageView ivVehicleType;
    @BindView(R.id.cl_driver_details)
    ConstraintLayout clDriverDetails;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.sv_order_details)
    ScrollView svOrderDetails;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;
    @BindView(R.id.cl_root)
    ConstraintLayout clRoot;
    @BindView(R.id.btn_cancel_order)
    Button btnCancelOrder;
    @BindView(R.id.tv_more_drops)
    TextView tvMoreDrops;
    @BindView(R.id.tv_creator_name)
    TextView tvCreatorName;
    @BindView(R.id.tv_receiver_name)
    TextView tvReceiverName;
    @BindView(R.id.cl_order_detail)
    ConstraintLayout clOrderDetail;
    @BindView(R.id.tv_label_pending_charges)
    TextView tvLabelPendingCharges;
    @BindView(R.id.tv_pending_charges)
    TextView tvPendingCharges;
    @BindView(R.id.tv_delivery_text)
    TextView tvDeliveryText;

    private OrderDetailViewModel mOrderDetailViewModel;
    private IOrderDetailHost mOrderDetailHost;
    private ItemImageAdapter mAdapter;
    private ArrayList<String> mItemsList;
    private Result mResult;
    private boolean mIsFromReceived;

    public static OrderDetailFragment getInstance(String orderId, boolean isOrderReceived) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.ORDER_ID, orderId);
        bundle.putBoolean(AppConstants.IS_ORDER_RECEIVED, isOrderReceived);
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        orderDetailFragment.setArguments(bundle);
        return orderDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mOrderDetailViewModel = new ViewModelProvider(this).get(OrderDetailViewModel.class);
        mOrderDetailViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setUpAdapter();
        getArgumentsData();

    }

    private void setUpAdapter() {
        mAdapter = new ItemImageAdapter(mItemsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rvProductImages.getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvProductImages.setLayoutManager(linearLayoutManager);
        rvProductImages.setAdapter(mAdapter);
    }

    private void observeLiveData() {
        mOrderDetailViewModel.getOrderDetailsLiveData().observe(getViewLifecycleOwner(), orderDetailResponse -> {
            if (orderDetailResponse != null) {
                svOrderDetails.setVisibility(View.VISIBLE);
                tvNoList.setVisibility(View.GONE);

                setOrderDetails(orderDetailResponse.getResult().get(0));
            }
        });

        mOrderDetailViewModel.getOrderCancellationLiveData().observe(getViewLifecycleOwner(), orderCancellationResponse -> {
            if (orderCancellationResponse != null) {
                showSnackBar(getString(R.string.s_order_cancelled));
                mOrderDetailHost.refreshOrders(orderCancellationResponse.getResult().getId());
            }
        });
    }

    private void setOrderDetails(Result orderDetailResponse) {

        mResult = orderDetailResponse;

        if (isOrderPickedUp() || anyDropCancelled() || isOrderAutoCancelled() || mIsFromReceived) {
            btnCancelOrder.setVisibility(View.GONE);
        } else {
            btnCancelOrder.setVisibility(View.VISIBLE);
        }

        if (mResult.getDropLocation().size() > 1) {
            tvMoreDrops.setText(String.format(Locale.getDefault(), "(+%d more)", mResult.getDropLocation().size() - 1));
        }

        Log.e("Order", "OrderDetails" + 1);

        BigDecimal itemPrice = BigDecimal.valueOf(0);
        tvOrderId.setText(String.format("ID - #%s", orderDetailResponse.getOrderUniqueId()));

        //Setting date and time
        if (orderDetailResponse.getScheduledStartTime() != null) {
            tvDay.setText(DateFormatter.getInstance().getFormattedDay(orderDetailResponse.getScheduledStartTime()));
            tvTime.setText(DateFormatter.getInstance().getFormattedTime(orderDetailResponse.getScheduledStartTime()));
            tvDate.setText(DateFormatter.getInstance().getFormattedDate(orderDetailResponse.getScheduledStartTime()));
        }

        Log.e("Order", "OrderDetails" + 2);

        //Setting promo code details
        if (orderDetailResponse.getPaymentDetails().isPromoCode()) {
            tvLabelPromoCodeApplied.setVisibility(View.VISIBLE);
            tvPromoCodeApplied.setVisibility(View.VISIBLE);
            tvLabelQdPromo.setVisibility(View.VISIBLE);
            tvQdPromo.setVisibility(View.VISIBLE);

            tvPromoCodeApplied.setText(String.format(Locale.getDefault(), "- %.3f KWD", orderDetailResponse.getPaymentDetails().getPromoAppliedAmount()));
            tvLabelQdPromo.setText(String.format("(%s)", orderDetailResponse.getPaymentDetails().getPromoCode()));
            tvQdPromo.setText(orderDetailResponse.getPaymentDetails().getIsPromoDiscoutPercentage() ?
                    orderDetailResponse.getPaymentDetails().getPromoPercentage() + "% discount" :
                    "Flat " + orderDetailResponse.getPaymentDetails().getPromoAppliedAmount() + " KWD");
        }

        Log.e("Order", "OrderDetails" + 3);

        //Setting order type
        tvOrderType.setText(orderDetailResponse.getOrderType());

        //Setting order type image
        if (orderDetailResponse.getOrderType().equals(getString(R.string.send)) || orderDetailResponse.getOrderType().equals("SPECIAL SEND"))
            ivOrderType.setImageDrawable((getResources().getDrawable(R.drawable.ic_dashboard_send)));
        else if (orderDetailResponse.getOrderType().equals(getString(R.string.get)) || orderDetailResponse.getOrderType().equals("SPECIAL GET"))
            ivOrderType.setImageDrawable(getResources().getDrawable(R.drawable.ic_dashboard_get));
        else if (orderDetailResponse.getOrderType().equals(getString(R.string.business)))
            ivOrderType.setImageDrawable((getResources().getDrawable(R.drawable.ic_dashboard_business)));

        //Setting order status
        if (mIsFromReceived) {
            setReceivedOrderStatus(orderDetailResponse);
        } else {
            setOrderStatus(orderDetailResponse);
        }

        Log.e("Order", "OrderDetails" + 4);

        if (!mIsFromReceived || mResult.getOrderType().equals(AppConstants.OrderType.GET) || mResult.getOrderType().equals(AppConstants.OrderType.SPECIAL_GET))
            tvPickupLocation.setText(orderDetailResponse.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (orderDetailResponse.getVendorId().get(0).getCompany() != null)
            tvCreatorName.setText(String.format("(%s)", orderDetailResponse.getVendorId().get(0).getCompany().getNameEnglish()));
        else
            tvCreatorName.setText(String.format("(%s)", orderDetailResponse.getPickupLocation().get(0).getRecipientName()));

        tvDropOffLocation.setText(orderDetailResponse.getDropLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        tvReceiverName.setText(String.format("(%s)", orderDetailResponse.getDropLocation().get(0).getRecipientName()));

        if (mIsFromReceived) {
            if (!orderDetailResponse.getDeliveryBySender()) {
                tvPaymentMode.setText(AppConstants.PaymentMode.CASH);
            } else {
                tvPaymentMode.setVisibility(View.GONE);
                ivPaymentMode.setVisibility(View.GONE);
            }
        } else if (orderDetailResponse.getPaymentDetails().getPaymentMode() != null) {
            tvPaymentMode.setText(orderDetailResponse.getPaymentDetails().getPaymentMode());
        }


        if (orderDetailResponse.getPaymentDetails().getPaymentMode().equals("COD")) {
            ivPaymentMode.setImageDrawable(getResources().getDrawable(R.drawable.ic_order_summary_cash_active));
        } else {
            ivPaymentMode.setImageDrawable(getResources().getDrawable(R.drawable.ic_order_summary_qd_wallet_active));
        }

        if (orderDetailResponse.getPickupInstruction() != null && !orderDetailResponse.getPickupInstruction().isEmpty()) {
            tvNoteForDriver.setText(orderDetailResponse.getPickupInstruction());
        } else {
            tvLabelNoteForDriver.setVisibility(View.GONE);
            tvNoteForDriver.setVisibility(View.GONE);
        }

        if (orderDetailResponse.getPaymentDetails().getReturnCharges() != null && orderDetailResponse.getPaymentDetails().getReturnCharges().compareTo(BigDecimal.valueOf(0)) > 0) {
            tvReturnCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", orderDetailResponse.getPaymentDetails().getReturnCharges()));
        } else {
            tvLabelReturnCharges.setVisibility(View.GONE);
            tvReturnCharges.setVisibility(View.GONE);
        }

        setCurrentCharges();

        if (orderDetailResponse.getPaymentDetails().getDeliveryCharge() != null) {
            tvDeliveryCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", orderDetailResponse.getPaymentDetails().getDeliveryChargeAfterDiscount()));
            tvDeliveryText.setText(orderDetailResponse.getDeliveryBySender() ? getString(R.string.s_to_be_paid_by_sender) : getString(R.string.s_to_be_paid_by_receiver));
        }

        tvName.setText(orderDetailResponse.getDropLocation().get(0).getRecipientName());
        tvPhoneNumber.setText(orderDetailResponse.getDropLocation().get(0).getRecipientMobileNo());

        if (orderDetailResponse.getDropLocation().get(0).getRecipientEmail() != null)
            tvEmail.setText(orderDetailResponse.getDropLocation().get(0).getRecipientEmail());
        else {
            tvLabelEmail.setVisibility(View.GONE);
            tvEmail.setVisibility(View.GONE);
        }


        mItemsList.clear();
        if (orderDetailResponse.getDropLocation().get(0).getItem().size() > 0)
            for (int i = 0; i < orderDetailResponse.getDropLocation().size(); i++) {
                for (int j = 0; j < orderDetailResponse.getDropLocation().get(i).getItem().size(); j++) {
                    if (orderDetailResponse.getDropLocation().get(i).getItem().get(j).getItemImage().size() > 0) {
                        mItemsList.add(orderDetailResponse.getDropLocation().get(i).getItem().get(j).getItemImage().get(0));
                    } else {
                        tvLabelProductImages.setVisibility(View.GONE);
                        rvProductImages.setVisibility(View.GONE);
                    }
                    if (orderDetailResponse.getDropLocation().get(i).getItem().get(j).getItemPrice() != null)
                        itemPrice = itemPrice.add(orderDetailResponse.getDropLocation().get(i).getItem().get(j).getItemPrice());
                }
            }
        mAdapter.notifyDataSetChanged();

        if (orderDetailResponse.getOrderStatus().size() > 1
                && orderDetailResponse.getDropLocation().get(0).getDeliveryDriverName() != null) {

            if (orderDetailResponse.getDropLocation().get(0).getDeliveryDriverName() != null) {
                tvDriverName.setText(orderDetailResponse.getDropLocation().get(0).getDeliveryDriverName());
            }
            if (orderDetailResponse.getDropLocation().get(0).getDeliveryDriverMobileNo() != null) {
                tvDriverPhoneNumber.setText(orderDetailResponse.getDropLocation().get(0).getDeliveryDriverMobileNo());
            }
            if (orderDetailResponse.getDropLocation().get(0).getDeliveryDriverImage() != null) {
                Glide.with(ivDriver.getContext())
                        .load(orderDetailResponse.getDropLocation().get(0).getDeliveryDriverImage())
                        .apply(RequestOptions.centerCropTransform().placeholder(R.drawable.ic_profile_placeholder))
                        .into(ivDriver);
            }

            if (orderDetailResponse.getVehicleType() != null) {
                switch (orderDetailResponse.getVehicleType()) {
                    case "BIKE":
                        Glide.with(ivVehicleType.getContext()).load(R.drawable.ic_choose_vehicle_bike_inactive).into(ivVehicleType);
                        break;
                    case "CAR":
                        Glide.with(ivVehicleType.getContext()).load(R.drawable.ic_choose_vehicle_car_inactive).into(ivVehicleType);
                        break;
                    case "VAN":
                        Glide.with(ivVehicleType.getContext()).load(R.drawable.ic_choose_vehicle_van_inactive).into(ivVehicleType);
                        break;
                    case "COOLER":
                        Glide.with(ivVehicleType.getContext()).load(R.drawable.ic_choose_vehicle_cooler_inactive).into(ivVehicleType);
                        break;


                }
            }
        } else {
            tvDriverName.setText(R.string.s_driver_not_aligned);
            tvDriverName.setGravity(Gravity.CENTER);
        }

        if (orderDetailResponse.getOrderType().equals(getString(R.string.s_special_get)) || orderDetailResponse.getOrderType().equals(getString(R.string.s_special_send)) ||
                orderDetailResponse.getOrderType().equals(getString(R.string.business))) {
            tvItemPrice.setText(String.format(Locale.getDefault(), "%.3f KWD", itemPrice));
            tvItemPrice.setVisibility(View.VISIBLE);
            tvLabelItemPrice.setVisibility(View.VISIBLE);
        } else {
            tvItemPrice.setVisibility(View.GONE);
            tvLabelItemPrice.setVisibility(View.GONE);
        }
        mAdapter.notifyDataSetChanged();
        hideProgressDialog();
    }

    private boolean isDriverAlignedForNow() {
        for (int i = 0; i < mResult.getOrderStatus().size(); i++) {
            if (mResult.getOrderStatus().get(i).getStatus().equals(AppConstants.OrderStatus.DRIVER_ALIGNED)) {
                return true;
            }
        }

        return false;
    }

    private void setReceivedOrderStatus(Result datum) {
        switch (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()) {
            case AppConstants.DriverTrackingStatus.UNALIGNED:
            case AppConstants.DriverTrackingStatus.ACCEPT_ORDER:
            case AppConstants.DriverTrackingStatus.DRIVE_TO_PICKEDUP:
            case AppConstants.DriverTrackingStatus.ORDER_PICKEDUP:
            case AppConstants.DriverTrackingStatus.DROP_COMPLETE:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorGreenishTeal));
                break;
            case AppConstants.OrderStatus.CASHBACK_RETURNED:
            case AppConstants.OrderStatus.RETURNED:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorBlue));
                break;
            case AppConstants.OrderStatus.CANCELLED:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_reddish));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorDarkRed));
                break;
            default:
                tvOrderStatus.setText(R.string.s_track);
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_rectangle_stroke_gun_metal));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorTextBlack));
        }
    }


    /**
     * set charges based on current status of the order specifying, how much amount is collected
     * or paid
     */
    private void setCurrentCharges() {
        BigDecimal previousBal = mResult.getPaymentDetails().getPreviousCancellationCharges().add(mResult.getPaymentDetails().getPreviousReturnCharges());

        if (previousBal.compareTo(BigDecimal.valueOf(0)) > 0) {
            tvLabelPendingCharges.setVisibility(View.VISIBLE);
            tvPendingCharges.setVisibility(View.VISIBLE);
            tvPendingCharges.setText(String.format(Locale.getDefault(), "%.3f KWD", previousBal));
        }
        switch (mResult.getOrderType()) {
            case AppConstants.OrderType.SEND:
            case AppConstants.OrderType.GET:
                tvLabelAmountPaid.setText(R.string.s_amount_paid);
                tvAmountPaid.setText(mResult.getPaymentDetails().isPromoCode() ?
                        String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getDeliveryChargeAfterDiscount().add(previousBal)) :
                        String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getDeliveryCharge().add(previousBal)));

                break;
            case AppConstants.OrderType.SPECIAL_GET:
                getChargesForSpecialOrders();
                break;

            case AppConstants.OrderType.SPECIAL_SEND:
                if (mResult.getDeliveryBySender()) {
                    getChargesForSpecialOrders();
                } else {
                    if (mResult.getPaymentDetails().getTotalItemCost().compareTo(previousBal) > 0) {
                        tvLabelAmountPaid.setText(R.string.s_amount_collected);
                        tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getTotalItemCost().subtract(previousBal)));
                    } else {
                        tvLabelAmountPaid.setText(R.string.s_amount_paid);
                        tvLabelAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", previousBal));
                    }
                }
                break;

        }
    }

    private void getChargesForSpecialOrders() {

        BigDecimal previousBal = mResult.getPaymentDetails().getPreviousCancellationCharges().add(mResult.getPaymentDetails().getPreviousReturnCharges());

        if (mResult.getPaymentDetails().isPromoCode()) {
            if (mResult.getPaymentDetails().getTotalItemCost().compareTo(mResult.getPaymentDetails().getDeliveryChargeAfterDiscount().add(previousBal)) > 0) {
                tvLabelAmountPaid.setText(R.string.s_amount_collected);
                tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getTotalItemCost()
                        .subtract(mResult.getPaymentDetails().getDeliveryChargeAfterDiscount().add(previousBal))));
            } else {
                tvLabelAmountPaid.setText(R.string.s_amount_paid);
                tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getDeliveryChargeAfterDiscount().add(previousBal)
                        .subtract(mResult.getPaymentDetails().getTotalItemCost())));
            }
        } else {
            if (mResult.getPaymentDetails().getTotalItemCost().compareTo(mResult.getPaymentDetails().getDeliveryCharge().add(previousBal)) > 0) {
                tvLabelAmountPaid.setText(R.string.s_amount_collected);
                tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getTotalItemCost()
                        .subtract(mResult.getPaymentDetails().getDeliveryCharge().add(previousBal))));
            } else {
                tvLabelAmountPaid.setText(R.string.s_amount_paid);
                tvAmountPaid.setText(String.format(Locale.getDefault(), "%.3f KWD", mResult.getPaymentDetails().getDeliveryCharge()
                        .add(mResult.getPaymentDetails().getReturnCharges().subtract(mResult.getPaymentDetails().getDeliveryCharge()))
                        .subtract(mResult.getPaymentDetails().getCancellationCharges())
                        .add(previousBal)
                        .subtract(mResult.getPaymentDetails().getTotalItemCost()).abs()));
            }
        }
    }

    private boolean isOrderAutoCancelled() {
        return mResult.getOrderStatus().get(mResult.getOrderStatus().size() - 1).getStatus().equals(AppConstants.OrderStatus.AUTO_CANCELLED);
    }

    private boolean anyDropCancelled() {
        for (int i = 0; i < mResult.getDropLocation().size(); i++) {
            if (mResult.getDropLocation().get(i).getCancelled()) {
                return !isOrderPickedUp();
            }
        }

        return false;
    }


    private boolean isOrderPickedUp() {

        for (int i = 0; i < mResult.getOrderStatus().size(); i++) {
            if (mResult.getOrderStatus().get(i).getStatus().equals(AppConstants.OrderStatus.PICKEDUP)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sets order status
     *
     * @param datum Order data
     */
    private void setOrderStatus(Result datum) {
        switch (datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus()) {
            case AppConstants.OrderStatus.ORDER_CREATED:
            case AppConstants.OrderStatus.DRIVER_ALIGNED:
            case AppConstants.OrderStatus.DELIVERED:
                if (datum.getDropLocation().size() == 1 &&
                        (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                    tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).
                            getDriverTrackingStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                } else {
                    tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_greenish_teal));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorGreenishTeal));
                }
                break;
            case AppConstants.OrderStatus.RETURNED:
            case AppConstants.OrderStatus.CASHBACK_RETURNED:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorBlue));
                break;
            case AppConstants.OrderStatus.AUTO_CANCELLED:
            case AppConstants.OrderStatus.CANCELLED:
                tvOrderStatus.setText(datum.getOrderStatus().get(datum.getOrderStatus().size() - 1).getStatus());
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_circular_corner_transparent_reddish));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorDarkRed));
                break;
            default:
                if (datum.getDropLocation().size() == 1 &&
                        (datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()
                                .equals(AppConstants.DriverTrackingStatus.RETURNED))) {
                    tvOrderStatus.setText(datum.getDropLocation().get(0).getDriverTrackingStatus().get(datum.getDropLocation().
                            get(0).getDriverTrackingStatus().size() - 1).getStatus());
                    tvOrderStatus.setBackground(tvOrderStatus.getResources().
                            getDrawable(R.drawable.drawable_circular_corner_transparent_bluish));
                    tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                            getColor(R.color.colorBlue));
                }
                tvOrderStatus.setText(R.string.s_track);
                tvOrderStatus.setBackground(tvOrderStatus.getResources().
                        getDrawable(R.drawable.drawable_rectangle_stroke_gun_metal));
                tvOrderStatus.setTextColor(tvOrderStatus.getResources().
                        getColor(R.color.colorTextBlack));
        }
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ORDER_ID) && getArguments().getString(AppConstants.ORDER_ID) != null) {
            mIsFromReceived = getArguments().getBoolean(AppConstants.IS_ORDER_RECEIVED);
            showProgressDialog();
            if (mIsFromReceived) {
                mOrderDetailViewModel.getReceivedOrderDetail(getArguments().getString(AppConstants.ORDER_ID));
            } else {
                mOrderDetailViewModel.getOrderDetail(getArguments().getString(AppConstants.ORDER_ID));
            }
        }
    }

    @Override
    protected void initViewsAndVariables() {
        mItemsList = new ArrayList<>();
        tvTitle.setText(getString(R.string.s_my_orders));
        tvLabelReceipt.setVisibility(View.GONE);
        cvReceipt.setVisibility(View.GONE);
        svOrderDetails.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.tv_label_recipient_details, R.id.tv_label_driver_details, R.id.tv_order_status, R.id.btn_cancel_order, R.id.tv_pickup_location,
            R.id.tv_drop_off_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mOrderDetailHost.onBackPressed();
                break;
            case R.id.tv_label_recipient_details:
                if (clRecipientDetails.getVisibility() == View.VISIBLE) {
                    clRecipientDetails.setVisibility(View.GONE);
                    tvLabelRecipientDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_my_orders_arrow_down, 0);
                    TransitionManager.beginDelayedTransition(clRecipientDetails);
                } else {
                    clRecipientDetails.setVisibility(View.VISIBLE);
                    tvLabelRecipientDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_my_oreders_arrow_up, 0);
                    TransitionManager.beginDelayedTransition(clRecipientDetails);
                }
                break;
            case R.id.tv_label_driver_details:
                if (clDriverDetails.getVisibility() == View.VISIBLE) {
                    clDriverDetails.setVisibility(View.GONE);
                    tvLabelDriverDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_my_orders_arrow_down, 0);
                    TransitionManager.beginDelayedTransition(clDriverDetails);
                } else {
                    clDriverDetails.setVisibility(View.VISIBLE);
                    tvLabelDriverDetails.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_my_oreders_arrow_up, 0);
                    TransitionManager.beginDelayedTransition(clDriverDetails);
                }
                break;
            case R.id.tv_order_status:
                if (isToTrack())
                    mOrderDetailHost.openTrackOrderFragment(mResult.getId(), mIsFromReceived);
                break;
            case R.id.btn_cancel_order:
                if (!isDriverAlignedForNow()) {
                    mOrderDetailViewModel.autoCancel(mResult.getId());
                } else
                    openOrderCancellationDialog();
                break;
            case R.id.tv_pickup_location:
                showFullAddressAlertDialog(mResult.getPickupLocation().get(0).getArea(), mResult.getPickupLocation().get(0).getBlockNumber(), mResult.getPickupLocation().get(0).getStreet(), mResult.getPickupLocation().get(0).getHouseOrbuilding(),
                        mResult.getPickupLocation().get(0).getHouseOrbuilding(), mResult.getPickupLocation().get(0).getFloor(), mResult.getPickupLocation().get(0).getPickupInstructions());
                break;
            case R.id.tv_drop_off_location:
                showFullAddressAlertDialog(mResult.getDropLocation().get(0).getArea(), mResult.getDropLocation().get(0).getBlockNumber(), mResult.getDropLocation().get(0).getStreet(), mResult.getDropLocation().get(0).getHouseOrbuilding(),
                        mResult.getDropLocation().get(0).getHouseOrbuilding(), mResult.getDropLocation().get(0).getFloor(), mResult.getDropLocation().get(0).getDriverNote());

        }
    }

    private void showFullAddressAlertDialog(CharSequence area, CharSequence blockNumber, String street, String houseOrBuilding, String apartmentOrOffice, String floor, String driverNote) {
        if (getContext() != null) {
            final Dialog addressDialog = new Dialog(getContext(), R.style.customDialog);
            addressDialog.setContentView(R.layout.dialog_view_address);
            addressDialog.setCancelable(true);

            ((TextView) addressDialog.findViewById(R.id.tv_area)).setText(area);
            ((TextView) addressDialog.findViewById(R.id.tv_block)).setText(blockNumber);

            if (street != null && !street.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_street)).setText(street);
            else {
                addressDialog.findViewById(R.id.tv_street).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_street).setVisibility(View.GONE);
            }

            if (houseOrBuilding != null && !houseOrBuilding.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_house_building)).setText(houseOrBuilding);
            else {
                addressDialog.findViewById(R.id.tv_house_building).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_house_building).setVisibility(View.GONE);
            }

            if (floor != null && !floor.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_floor)).setText(floor);
            else {
                addressDialog.findViewById(R.id.tv_floor).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_floor).setVisibility(View.GONE);
            }

            if (apartmentOrOffice != null && !apartmentOrOffice.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_apartment)).setText(apartmentOrOffice);
            else {
                addressDialog.findViewById(R.id.tv_apartment).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_label_apartment).setVisibility(View.GONE);
            }

            if (driverNote != null && !driverNote.isEmpty())
                ((TextView) addressDialog.findViewById(R.id.tv_note)).setText(driverNote);
            else {
                addressDialog.findViewById(R.id.tv_label_note).setVisibility(View.GONE);
                addressDialog.findViewById(R.id.tv_note).setVisibility(View.GONE);
            }

            AppUtils.getInstance().dimDialogBackground(addressDialog);
            addressDialog.show();
        }
    }


    private boolean isToTrack() {
        if (mIsFromReceived) {
            switch (mResult.getDropLocation().get(0).getDriverTrackingStatus().get(mResult.getDropLocation().get(0).getDriverTrackingStatus().size() - 1).getStatus()) {
                case AppConstants.DriverTrackingStatus.DRIVE_TO_DROP_LOCATION:
                case AppConstants.DriverTrackingStatus.REACHED_TO_DROP_LOCATION:
                    return true;
                default:
                    return false;
            }
        } else
            switch (mResult.getOrderStatus().get(mResult.getOrderStatus().size() - 1).getStatus()) {
                case AppConstants.OrderStatus.ORDER_CREATED:
                case AppConstants.OrderStatus.DRIVER_ALIGNED:
                case AppConstants.OrderStatus.DELIVERED:
                case AppConstants.OrderStatus.RETURNED:
                case AppConstants.OrderStatus.CANCELLED:
                case AppConstants.OrderStatus.AUTO_CANCELLED:
                case AppConstants.OrderStatus.CASHBACK_RETURNED:
                    return false;
                default:
                    return true;
            }
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        if (failureResponse.getErrorCode() == CANCELLATION_FAILURE) {
            AppUtils.getInstance().showAlertDialog(getContext(), "Driver is aligned for this order so charges may apply, do you still want to cancel?", this, this);
        }
    }

    /**
     * Opens dialog for listing the reason to cancel the particular order.
     * Only one of the given reasons can be selected at a time and an additional edit text is provided for description.
     */
    private void openOrderCancellationDialog() {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity(), R.style.customDialog);
            dialog.setContentView(R.layout.layout_cancel_order);
            AppUtils.getInstance().dimDialogBackground(dialog);

            final RadioGroup rgReasons = dialog.findViewById(R.id.rg_reasons);
            final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
            final Button btnSubmit = dialog.findViewById(R.id.btn_submit);
            final EditText etDescription = dialog.findViewById(R.id.et_message);
            final TextView tvError = dialog.findViewById(R.id.tv_error);

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            List<com.qd.user.model.commomresponse.Result> reasons = DataManager.getInstance().getCancelReasons();

            for (int i = 0; i < reasons.size(); i++) {
                RadioButton button = new RadioButton(getContext());
                button.setId(i);
                button.setText(reasons.get(i).getReason());
                rgReasons.addView(button);
            }

            btnSubmit.setOnClickListener(v -> {
                if (rgReasons.getCheckedRadioButtonId() != -1) {
                    if (reasons.get(rgReasons.getCheckedRadioButtonId()).getDescriptionMandatory() && etDescription.getText().toString().isEmpty()) {
                        tvError.setText(R.string.s_please_describe_reason_for_cancellation);
                        tvError.setVisibility(View.VISIBLE);
                        shakeLayout(tvError);
                    } else {
                        String reportIssue;
                        tvError.setVisibility(View.GONE);
                        reportIssue = reasons.get(rgReasons.getCheckedRadioButtonId()).getReason();

                        mOrderDetailViewModel.cancelOrder(requestPayloadForOrderCancellation(reportIssue, etDescription.getText().toString()));
                        dialog.dismiss();
                    }
                } else {
                    tvError.setText(R.string.s_please_select_a_reason_for_cancellation);
                    tvError.setVisibility(View.VISIBLE);
                    shakeLayout(tvError);
                }
            });

            dialog.show();
        }
    }

    /**
     * Create request payload for order cancellation
     *
     * @param reportIssue Reason selected for cancellation
     * @param description Additional message, if the user has entered
     * @return JSON Object for order cancellation request
     */
    private JSONObject requestPayloadForOrderCancellation(String reportIssue, String description) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("requestFrom", mResult.getOrderType().equals(getString(R.string.business)) ? getString(R.string.business) : getString(R.string.cash_client));
            jsonObject.put(AppConstants.ORDER_ID, mResult.getId());
            jsonObject.put("elementId", mResult.getPickupLocation().get(0).getId());
            jsonObject.put("reason", reportIssue);

            if (mResult.getPickupLocation().get(0).getPickupDriverId() != null) {
                jsonObject.put("driverId", mResult.getPickupLocation().get(0).getPickupDriverId());
            }

            if (description != null)
                jsonObject.put("description", description);
            jsonObject.put("isElement", false);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IOrderDetailHost) {
            mOrderDetailHost = (IOrderDetailHost) context;
        } else throw new IllegalStateException("Host must implement IOrderDetailHost");
    }

    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();

        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        svOrderDetails.setVisibility(View.GONE);
    }

    public void updateOrder(String orderId) {
        svOrderDetails.setVisibility(View.GONE);
        mOrderDetailViewModel.getOrderDetail(orderId);
    }

    @Override
    public void onClick(DialogInterface dialog, int id) {
        switch (id) {
            case DialogInterface.BUTTON_POSITIVE:
                openOrderCancellationDialog();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                mOrderDetailViewModel.getOrderDetail(mResult.getId());
                break;
        }
    }

    public interface IOrderDetailHost {
        void onBackPressed();

        void openTrackOrderFragment(String id, boolean isFromReceived);

        void refreshOrders(String id);
    }
}
