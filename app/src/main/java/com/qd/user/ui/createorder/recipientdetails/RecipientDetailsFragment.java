package com.qd.user.ui.createorder.recipientdetails;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.advancedsearch.SaveAddressRequest;
import com.qd.user.model.createorder.optimizedroute.DropLocation;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryRequest;
import com.qd.user.model.createorder.optimizedroute.Origin;
import com.qd.user.model.createorder.request.CreateOrderRequest;
import com.qd.user.model.createorder.request.DropOffArr;
import com.qd.user.model.createorder.request.PickupLocation;
import com.qd.user.utils.CustomEditText;
import com.qd.user.utils.PermissionHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;
import static com.qd.user.constants.AppConstants.PermissionConstants.REQUEST_CODE_PICK_CONTACTS;

public class RecipientDetailsFragment extends BaseFragment implements PermissionHelper.IGetPermissionListener {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.view_toolbar)
    View viewToolbar;
    @BindView(R.id.tv_label_almost_there)
    TextView tvLabelAlmostThere;
    @BindView(R.id.tv_label_recipient_details)
    TextView tvLabelRecipientDetails;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.til_name)
    TextInputLayout tilName;
    @BindView(R.id.view_name)
    View viewName;
    @BindView(R.id.tv_error_name)
    TextView tvErrorName;
    @BindView(R.id.tv_country_code)
    TextView tvCountryCode;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.til_phone_number)
    TextInputLayout tilPhoneNumber;
    @BindView(R.id.view_country_code)
    View viewCountryCode;
    @BindView(R.id.view_phone_number)
    View viewPhoneNumber;
    @BindView(R.id.tv_error_number)
    TextView tvErrorNumber;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.view_email)
    View viewEmail;
    @BindView(R.id.tv_error_email)
    TextView tvErrorEmail;
    @BindView(R.id.tv_recipient_address)
    TextView tvRecipientAddress;
    @BindView(R.id.et_note)
    CustomEditText etNote;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.cl_recipient_details)
    ConstraintLayout clRecipientDetails;
    @BindView(R.id.sv_recipient_details)
    ScrollView svRecipientDetails;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.iv_contacts)
    ImageView ivContacts;
    @BindView(R.id.iv_notification)
    ImageView ivNotification;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbar;
    @BindView(R.id.cb_address_confirmation)
    CheckBox cbAddressConfirmation;
    @BindView(R.id.rg_delivery_option)
    RadioGroup rgDeliveryOption;
    @BindView(R.id.rb_deliver_to_me)
    RadioButton rbDeliverToMe;
    @BindView(R.id.rb_someone_else)
    RadioButton rbSomeoneElse;
    @BindView(R.id.tv_label_sender_details)
    TextView tvLabelSenderDetails;
    @BindView(R.id.et_sender_name)
    EditText etSenderName;
    @BindView(R.id.til_sender_name)
    TextInputLayout tilSenderName;
    @BindView(R.id.iv_sender_contacts)
    ImageView ivSenderContacts;
    @BindView(R.id.view_sender_name)
    View viewSenderName;
    @BindView(R.id.tv_sender_error_name)
    TextView tvSenderErrorName;
    @BindView(R.id.tv_sender_country_code)
    TextView tvSenderCountryCode;
    @BindView(R.id.et_sender_phone_number)
    EditText etSenderPhoneNumber;
    @BindView(R.id.til_sender_phone_number)
    TextInputLayout tilSenderPhoneNumber;
    @BindView(R.id.view_sender_country_code)
    View viewSenderCountryCode;
    @BindView(R.id.view_sender_phone_number)
    View viewSenderPhoneNumber;
    @BindView(R.id.tv_sender_error_number)
    TextView tvSenderErrorNumber;
    @BindView(R.id.cl_pickup_contact_details)
    ConstraintLayout clPickupContactDetails;
    private Unbinder unbinder;
    private IRecipientDetailsHost mHost;
    private RecipientDetailsViewModel mRecipientDetailsViewModel;

    private Uri uriContact;
    private String contactID;     // contacts unique ID
    private PermissionHelper mPermissionHelper;
    private CreateOrderRequest mCreateOrderDetails;
    private int mDropPosition;
    private boolean isForSender;

    public RecipientDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipient_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public static RecipientDetailsFragment getInstance(Parcelable orderDetails, int pos) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.CREATE_ORDER_DETAILS, orderDetails);
        if (pos != -1) {
            bundle.putInt(AppConstants.EDIT_INDEX, pos);
        }
        RecipientDetailsFragment recipientDetailsFragment = new RecipientDetailsFragment();
        recipientDetailsFragment.setArguments(bundle);
        return recipientDetailsFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IRecipientDetailsHost) {
            mHost = (IRecipientDetailsHost) context;
        } else throw new IllegalStateException("Host must implement IRecipientDetailsHost");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mRecipientDetailsViewModel = new ViewModelProvider(this).get(RecipientDetailsViewModel.class);
        mRecipientDetailsViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        super.onViewCreated(view, savedInstanceState);
        observeLiveData();

    }


    private void observeLiveData() {
        //observe validations error
        mRecipientDetailsViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_NAME:
                    case AppConstants.UI_VALIDATIONS.INVALID_FIRST_NAME:
                        showNameValidationError(validationErrors.getValidationMessage());
                        focusNameInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_PHONE_NUMBER:
                        showContactValidationError(validationErrors.getValidationMessage());
                        focusContactInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_EMAIL:
                        showEmailValidationError(validationErrors.getValidationMessage());
                        focusEmailInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_SENDER_NAME:
                        showSenderNameValidationError(validationErrors.getValidationMessage());
                        focusSenderNameInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_SENDER_CONTACT:
                        showSenderContactValidationError(validationErrors.getValidationMessage());
                        focusSenderContactInputField();
                        break;
                }
            }
        });

        mRecipientDetailsViewModel.getOptimizedDeliveryLiveData().observe(getViewLifecycleOwner(), optimizedDeliveryResponse -> mCreateOrderDetails.getDropOffArr().get(mDropPosition != -1 ? mDropPosition : mCreateOrderDetails.getDropOffArr().size() - 1).setDeliveryCharge(optimizedDeliveryResponse.getResult().get(0).getDeliveryCharge()));

    }

    private void focusSenderContactInputField() {
        etSenderPhoneNumber.setSelection(etSenderPhoneNumber.getText().toString().length());
        etSenderPhoneNumber.requestFocus();
    }

    private void showSenderContactValidationError(int validationMessage) {
        shakeLayout(tvSenderErrorNumber);
        tvSenderErrorNumber.setText(validationMessage);
        tvSenderErrorNumber.setVisibility(View.VISIBLE);
    }

    private void focusSenderNameInputField() {
        etSenderName.setSelection(etSenderName.getText().toString().length());
        etSenderName.requestFocus();
    }

    private void showSenderNameValidationError(int validationMessage) {
        shakeLayout(tvSenderErrorName);
        tvSenderErrorName.setText(validationMessage);
        tvSenderErrorName.setVisibility(View.VISIBLE);
    }

    private void focusContactInputField() {
        etPhoneNumber.setSelection(etPhoneNumber.getText().toString().length());
        etPhoneNumber.requestFocus();
    }

    private void showContactValidationError(int errorMessage) {
        shakeLayout(tvErrorNumber);
        tvErrorNumber.setText(errorMessage);
        tvErrorNumber.setVisibility(View.VISIBLE);
    }


    /**
     * focus first name field
     */
    private void focusNameInputField() {
        etName.setSelection(etName.getText().toString().length());
        etName.requestFocus();
    }

    /**
     * show error message of invalid or empty first name
     * @param errorMessage  Error message to be shown
     */
    private void showNameValidationError(int errorMessage) {
        shakeLayout(tvErrorName);
        tvErrorName.setText(errorMessage);
        tvErrorName.setVisibility(View.VISIBLE);
    }

    /**
     * show error message of invalid or empty email address
     *
     * @param message message to be shown
     */
    private void showEmailValidationError(int message) {
        shakeLayout(tvErrorEmail);
        tvErrorEmail.setText(message);
        tvErrorEmail.setVisibility(View.VISIBLE);
    }

    /**
     * focus email address field
     */
    private void focusEmailInputField() {
        etEmail.setSelection(etEmail.getText().toString().length());
        etEmail.requestFocus();
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(getString(R.string.s_recipient_details));
        mDropPosition = -1;
        getArgumentsData();
        setViews();

    }

    private void setViews() {

        if (mDropPosition != -1)
            tvRecipientAddress.setText(mCreateOrderDetails.getDropOffArr().get(mDropPosition).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.business))) {
            cbAddressConfirmation.setVisibility(View.VISIBLE);
            cbAddressConfirmation.setOnCheckedChangeListener((buttonView, isChecked) -> {

            });

        } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get))) {
            rgDeliveryOption.setVisibility(View.VISIBLE);
            clPickupContactDetails.setVisibility(View.VISIBLE);

            tvRecipientAddress.setText(mCreateOrderDetails.getPickupLocation().get(0).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));

            rgDeliveryOption.setOnCheckedChangeListener((group, checkedId) -> {
                if (checkedId == R.id.rb_deliver_to_me) {
                    etName.setText(mRecipientDetailsViewModel.getUserName());
                    etEmail.setText(mRecipientDetailsViewModel.getEmail());
                    etPhoneNumber.setText(mRecipientDetailsViewModel.getContact());

                    etName.setEnabled(false);
                    etEmail.setEnabled(false);
                    etPhoneNumber.setEnabled(false);
                } else {
                    etName.setText("");
                    etPhoneNumber.setText("");
                    etEmail.setText("");
                    etName.setEnabled(true);
                    etEmail.setEnabled(true);
                    etPhoneNumber.setEnabled(true);
                }
            });

            rbDeliverToMe.setChecked(true);
        }
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            if (getArguments().containsKey(AppConstants.EDIT_INDEX)) {
                mDropPosition = getArguments().getInt(AppConstants.EDIT_INDEX);
            }
            mCreateOrderDetails = getArguments().getParcelable(AppConstants.CREATE_ORDER_DETAILS);
            if (mDropPosition != -1) {
                setViewsForEditingRecipientDetails();
            }

        }
    }

    private void setViewsForEditingRecipientDetails() {
        etName.setText(mCreateOrderDetails.getDropOffArr().get(mDropPosition).getRecipientName());
        etPhoneNumber.setText(mCreateOrderDetails.getDropOffArr().get(mDropPosition).getRecipientMobileNo());
        etEmail.setText(mCreateOrderDetails.getDropOffArr().get(mDropPosition).getRecipientEmail());

        etSenderName.setText(mCreateOrderDetails.getPickupLocation().get(0).getRecipientName());
        etSenderPhoneNumber.setText(mCreateOrderDetails.getPickupLocation().get(0).getRecipientMobileNo());

        tvRecipientAddress.setText(mCreateOrderDetails.getDropOffArr().get(mDropPosition).getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.iv_back, R.id.btn_submit, R.id.iv_contacts, R.id.tv_recipient_address, R.id.iv_sender_contacts})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_submit:
                hideErrorFields();
                if (mRecipientDetailsViewModel.checkValidation(etName.getText().toString().trim(),
                        etPhoneNumber.getText().toString().trim(),
                        etEmail.getText().toString().trim())) {

                    if ((mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.GET) ||
                            mCreateOrderDetails.getOrderType().equals(AppConstants.OrderType.SPECIAL_GET))) {

                        if (mRecipientDetailsViewModel.checkSenderDetailsValidation(etSenderName.getText().toString().trim(),
                                etSenderPhoneNumber.getText().toString().trim())) {
                            mHost.navigateToConfirmOrderFragment(setOrderData(), true);

                        }
                    } else if (mCreateOrderDetails.getDeliveryType().equals(getString(R.string.multiple_delivery))) {
                        if (tvRecipientAddress.getText().toString().isEmpty()) {
                            showSnackBar(getString(R.string.s_enter_drop_location));
                        } else
                            mHost.openDropLocationListFragment(setOrderData());
                    } else {
                        mHost.navigateToConfirmOrderFragment(setOrderData(), true);
                    }
                }
                break;
            case R.id.iv_sender_contacts:
                isForSender = true;
            case R.id.iv_contacts:
                mPermissionHelper = new PermissionHelper(this);
                if (mPermissionHelper.hasPermission(getActivity(),
                        new String[]{Manifest.permission.READ_CONTACTS},
                        AppConstants.PermissionConstants.REQUEST_READ_CONTACTS)) {
                    permissionsGiven(AppConstants.PermissionConstants.REQUEST_READ_CONTACTS);
                }

                break;

            case R.id.tv_recipient_address:
                if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get)) ||
                        mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get))) {
                    mHost.editRecipientAddress(mCreateOrderDetails.getPickupLocation().get(0), null);
                } else {

                    if (mDropPosition != -1) {
                        mHost.editRecipientAddress(null, mCreateOrderDetails.getDropOffArr().get(mDropPosition));
                    } else if (mCreateOrderDetails.getDropOffArr().size() > 1) {
                        mHost.editRecipientAddress(null, null);
                    } else {
                        mDropPosition = 0;
                        mHost.editRecipientAddress(null, mCreateOrderDetails.getDropOffArr().get(mDropPosition));
                    }
                }
        }
    }

    private void hideErrorFields() {
        tvErrorName.setVisibility(View.GONE);
        tvErrorNumber.setVisibility(View.GONE);
        tvErrorEmail.setVisibility(View.GONE);
    }

    private CreateOrderRequest setOrderData() {
        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.send)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_send)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.business))) {

            mCreateOrderDetails.getPickupLocation().get(0).setDriverNote(Objects.requireNonNull(etNote.getText()).toString());

            mCreateOrderDetails.getPickupLocation().get(0).setRecipientName(mRecipientDetailsViewModel.getUserName());
            mCreateOrderDetails.getPickupLocation().get(0).setRecipientEmail(mRecipientDetailsViewModel.getEmail());
            mCreateOrderDetails.getPickupLocation().get(0).setRecipientMobileNo(mRecipientDetailsViewModel.getMobileNumber());

            if (mDropPosition != -1) {
                mCreateOrderDetails.getDropOffArr().get(mDropPosition).setRecipientName(etName.getText().toString());
                mCreateOrderDetails.getDropOffArr().get(mDropPosition).setRecipientEmail(etEmail.getText().toString());
                mCreateOrderDetails.getDropOffArr().get(mDropPosition).setRecipientMobileNo(etPhoneNumber.getText().toString());
            } else if (mCreateOrderDetails.getDropOffArr().size() > 0) {
                mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setRecipientName(etName.getText().toString());
                mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setRecipientEmail(etEmail.getText().toString());
                mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setRecipientMobileNo(etPhoneNumber.getText().toString());

            } else {
                mCreateOrderDetails.getDropOffArr().get(0).setRecipientName(etName.getText().toString());
                mCreateOrderDetails.getDropOffArr().get(0).setRecipientEmail(etEmail.getText().toString());
                mCreateOrderDetails.getDropOffArr().get(0).setRecipientMobileNo(etPhoneNumber.getText().toString());
            }

        } else if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get))) {
            mCreateOrderDetails.getPickupLocation().get(0).setRecipientName(etSenderName.getText().toString());
            mCreateOrderDetails.getPickupLocation().get(0).setRecipientMobileNo(etSenderPhoneNumber.getText().toString());

            mCreateOrderDetails.getDropOffArr().get(0).setDriverNote(String.valueOf(etNote.getText()));

            mCreateOrderDetails.getDropOffArr().get(0).setRecipientName(etName.getText().toString());
            mCreateOrderDetails.getDropOffArr().get(0).setRecipientEmail(etEmail.getText().toString());
            mCreateOrderDetails.getDropOffArr().get(0).setRecipientMobileNo(etPhoneNumber.getText().toString());
        }
        return mCreateOrderDetails;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICK_CONTACTS && resultCode == RESULT_OK) {
            Log.d("Contacts", "Response: " + data.toString());
            uriContact = data.getData();

            if (isForSender) {
                isForSender = false;
                etSenderName.setText(retrieveContactName());
                etSenderPhoneNumber.setText(retrieveContactNumber());
            } else {
                etPhoneNumber.setText(retrieveContactNumber());
                etName.setText(retrieveContactName());
            }

        }
    }

    /**
     * Retrieving contact number from phone's contacts using @code{#Cusror}
     *
     * @return Contact number, if found
     */
    private String retrieveContactNumber() {
        String contactNumber = null;
        if (getActivity() != null) {
            // getting contacts ID
            Cursor cursorID = getActivity().getContentResolver().query(uriContact,
                    new String[]{ContactsContract.Contacts._ID},
                    null, null, null);

            if (cursorID != null && cursorID.moveToFirst()) {
                contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
            }
            if (cursorID != null) {
                cursorID.close();
            }


            // Using the contact ID now we will get contact phone number
            Cursor cursorPhone = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER,
                            ContactsContract.CommonDataKinds.Phone.NUMBER,
                            ContactsContract.CommonDataKinds.Email.ADDRESS},
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                            ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                    new String[]{contactID},
                    null);
            // HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.
            if ((cursorPhone != null && cursorPhone.moveToNext()) &&
                    cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)).equalsIgnoreCase("1")) {
                contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            }
            if (cursorPhone != null) {
                cursorPhone.close();
            }
        }

        return contactNumber != null ? getFormattedContactNumber(contactNumber) : null;
    }

    private String retrieveContactName() {
        String contactName = null;

        if (getActivity() != null) {
            // querying contact data store
            Cursor cursor = getActivity().getContentResolver().query(uriContact, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                // DISPLAY_NAME = The display name for the contact.
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
            if (cursor != null) {
                cursor.close();
            }
        }

        return contactName;
    }

    private String getFormattedContactNumber(String contactNumber) {
        contactNumber = contactNumber.replace(" ", "");
        if (contactNumber.contains("+965") || (contactNumber.startsWith("965") && contactNumber.length() > 9)) {
            contactNumber = contactNumber.replaceFirst("[+]*965", "");
        }

        return contactNumber;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionHelper.setPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void permissionsGiven(int requestCode) {
        if (requestCode == AppConstants.PermissionConstants.REQUEST_READ_CONTACTS) {
            // using native contacts selection
            // Intent.ACTION_PICK = Pick an item from the data, returning what was selected.
            startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), REQUEST_CODE_PICK_CONTACTS);
        }
    }

    public void updateLocation(SaveAddressRequest completeAddress) {
        setDropLocation(completeAddress);
        tvRecipientAddress.setText(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
        mRecipientDetailsViewModel.getDeliveryCharges(getDataForOptimizedRoute(mCreateOrderDetails));
    }

    /**
     * Creates a request object to find out delivery charges for selected pickup and drop location
     *
     * @param createOrderRequest Create Order Request object that contains all the data filled by the user so far
     * @return Request object to make network call to get required delivery charges
     */
    private OptimizedDeliveryRequest getDataForOptimizedRoute(CreateOrderRequest createOrderRequest) {
        OptimizedDeliveryRequest optimizedDeliveryRequest = new OptimizedDeliveryRequest();
        Origin origin = new Origin();
        origin.setLat(createOrderRequest.getPickupLocation().get(0).getGeometry().getCoordinates().get(1));
        origin.setLng(createOrderRequest.getPickupLocation().get(0).getGeometry().getCoordinates().get(0));
        optimizedDeliveryRequest.setOrigin(origin);

        DropLocation dropLocation = new DropLocation();
        dropLocation.setLat(createOrderRequest.getDropOffArr().get(mDropPosition != -1 ? mDropPosition : createOrderRequest.getDropOffArr().size() - 1).getGeometry().getCoordinates().get(1));
        dropLocation.setLng(createOrderRequest.getDropOffArr().get(mDropPosition != -1 ? mDropPosition : createOrderRequest.getDropOffArr().size() - 1).getGeometry().getCoordinates().get(0));

        List<DropLocation> dropLocationList = new ArrayList<>();
        dropLocationList.add(dropLocation);
        optimizedDeliveryRequest.setDropLocation(dropLocationList);

        optimizedDeliveryRequest.setServiceType(createOrderRequest.getServiceType());

        if (createOrderRequest.getOrderType().equals(AppConstants.OrderType.SEND) || createOrderRequest.getOrderType().equals(AppConstants.OrderType.SPECIAL_SEND))
            optimizedDeliveryRequest.setOrderType(AppConstants.OrderType.SPECIAL_SEND);
        else
            optimizedDeliveryRequest.setOrderType(createOrderRequest.getOrderType());


        optimizedDeliveryRequest.setVehicleType(createOrderRequest.getVehicleType());
        optimizedDeliveryRequest.setUserType(createOrderRequest.getRequestFrom());
        optimizedDeliveryRequest.setOptimize(true);

        return optimizedDeliveryRequest;
    }


    private void setDropLocation(SaveAddressRequest completeAddress) {
        if (mCreateOrderDetails.getOrderType().equals(getString(R.string.get)) ||
                mCreateOrderDetails.getOrderType().equals(getString(R.string.s_special_get))) {
            mCreateOrderDetails.getPickupLocation().get(0).setGovernorate(completeAddress.getGovernorate());
            mCreateOrderDetails.getPickupLocation().get(0).setArea(completeAddress.getArea());
            mCreateOrderDetails.getPickupLocation().get(0).setBlockNumber(completeAddress.getBlockNumber());
            mCreateOrderDetails.getPickupLocation().get(0).setStreet(completeAddress.getStreet());
            mCreateOrderDetails.getPickupLocation().get(0).setAvenue(completeAddress.getAvenue());
            mCreateOrderDetails.getPickupLocation().get(0).setHouseOrbuilding(completeAddress.getHouseOrbuilding());
            mCreateOrderDetails.getPickupLocation().get(0).setApartmentOrOffice(completeAddress.getApartmentOrOffice());
            mCreateOrderDetails.getPickupLocation().get(0).setFloor(completeAddress.getFloor());
            mCreateOrderDetails.getPickupLocation().get(0).setFullAddress(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            mCreateOrderDetails.getPickupLocation().get(0).setGeometry(completeAddress.getGeometry());
        } else if (mDropPosition == -1 && mCreateOrderDetails.getDropOffArr().size() > 1) {
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setGovernorate(completeAddress.getGovernorate());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setArea(completeAddress.getArea());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setBlockNumber(completeAddress.getBlockNumber());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setStreet(completeAddress.getStreet());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setAvenue(completeAddress.getAvenue());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setHouseOrbuilding(completeAddress.getHouseOrbuilding());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setApartmentOrOffice(completeAddress.getApartmentOrOffice());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setFloor(completeAddress.getFloor());
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setFullAddress(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            mCreateOrderDetails.getDropOffArr().get(mCreateOrderDetails.getDropOffArr().size() - 1).setGeometry(completeAddress.getGeometry());
        } else {
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setGovernorate(completeAddress.getGovernorate());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setArea(completeAddress.getArea());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setBlockNumber(completeAddress.getBlockNumber());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setStreet(completeAddress.getStreet());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setAvenue(completeAddress.getAvenue());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setHouseOrbuilding(completeAddress.getHouseOrbuilding());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setApartmentOrOffice(completeAddress.getApartmentOrOffice());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setFloor(completeAddress.getFloor());
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setFullAddress(completeAddress.getFullAddress().replaceAll("[,]*[a-zA-Z]+\\s+Governorate+[,]*", ""));
            mCreateOrderDetails.getDropOffArr().get(mDropPosition).setGeometry(completeAddress.getGeometry());
        }
    }

    /*public CreateOrderRequest getCreateOrderRequest() {
        return mCreateOrderDetails;
    }*/

    public interface IRecipientDetailsHost {
        void onBackPressed();

        void navigateToConfirmOrderFragment(CreateOrderRequest createOrderDetails, boolean b);

        void openDropLocationListFragment(CreateOrderRequest createOrderRequest);

        void editRecipientAddress(PickupLocation pickupLocation, DropOffArr dropOffArr);
    }
}
