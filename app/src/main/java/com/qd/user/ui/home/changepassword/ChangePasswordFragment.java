package com.qd.user.ui.home.changepassword;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.constants.AppConstants;
import com.qd.user.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChangePasswordFragment extends BaseFragment implements TextWatcher,AppUtils.ISuccess {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_old_password)
    EditText etOldPassword;
    @BindView(R.id.til_old_password)
    TextInputLayout tilOldPassword;
    @BindView(R.id.view_old_password)
    View viewOldPassword;
    @BindView(R.id.tv_error_old_password)
    TextView tvErrorOldPassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.til_new_password)
    TextInputLayout tilNewPassword;
    @BindView(R.id.tv_new_password_visibiity)
    TextView tvNewPasswordVisibility;
    @BindView(R.id.view_new_password)
    View viewNewPassword;
    @BindView(R.id.tv_error_new_password)
    TextView tvErrorNewPassword;
    @BindView(R.id.et_confirm_new_password)
    EditText etConfirmNewPassword;
    @BindView(R.id.til_confirm_new_password)
    TextInputLayout tilConfirmNewPassword;
    @BindView(R.id.tv_confirm_password_visibility)
    TextView tvConfirmPasswordVisibility;
    @BindView(R.id.view_confirm_password)
    View viewConfirmPassword;
    @BindView(R.id.tv_error_confirm_password)
    TextView tvErrorConfirmPassword;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    private Unbinder unbinder;
    @BindView(R.id.btn_save)
    Button btnSave;
    private boolean isNewPasswordVisible;
    private boolean isConfirmNewPasswordVisible;
    private IChangePasswordHost mHost;
    private ChangePasswordViewModel mChangePasswordViewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mChangePasswordViewModel = new ViewModelProvider(this).get(ChangePasswordViewModel.class);
        mChangePasswordViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());
        observeLiveData();
        setListeners();

    }

    private void observeLiveData() {
        mChangePasswordViewModel.getValidationLiveData().observe(getViewLifecycleOwner(), validationErrors -> {
            if (validationErrors != null) {
                //Handle validations on the basis of the codes here
                hideProgressDialog();
                switch (validationErrors.getErrorCode()) {
                    case AppConstants.UI_VALIDATIONS.INVALID_OLD_PASSWORD:
                        showOldPasswordValidationError(validationErrors.getValidationMessage());
                        focusOldPasswordInputField();

                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_NEW_PASSWORD:
                        showNewPasswordValidationError(validationErrors.getValidationMessage());
                        focusNewPasswordInputField();
                        break;
                    case AppConstants.UI_VALIDATIONS.INVALID_CONFIRM_NEW_PASSWORD:
                        showConfirmPasswordValidationError(validationErrors.getValidationMessage());
                        focusConfirmPasswordInputField();
                        break;
                }
            }
        });
        mChangePasswordViewModel.getChangePasswordLiveData().observe(getViewLifecycleOwner(), commonResponse -> {
            if (commonResponse != null) {
                hideProgressDialog();
                AppUtils.getInstance().openSuccessDialog(getActivity(), getString(R.string.s_password_has_been_changed_successfully), ChangePasswordFragment.this);
            }
        });
    }


    private void focusConfirmPasswordInputField() {
        etConfirmNewPassword.setSelection(etConfirmNewPassword.getText().length());
        etConfirmNewPassword.requestFocus();
    }

    private void showConfirmPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(etConfirmNewPassword);
        tvErrorConfirmPassword.setText(errorMessage);
        tvErrorConfirmPassword.setVisibility(View.VISIBLE); }

    private void focusNewPasswordInputField() {
        etNewPassword.setSelection(etNewPassword.getText().length());
        etNewPassword.requestFocus();
    }

    private void showNewPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(etNewPassword);
        tvErrorNewPassword.setText(errorMessage);
        tvErrorNewPassword.setVisibility(View.VISIBLE);
    }

    private void focusOldPasswordInputField() {
        etOldPassword.setSelection(etOldPassword.getText().length());
        etOldPassword.requestFocus();
    }

    private void showOldPasswordValidationError(int errorMessage) {
        AppUtils.getInstance().shakeLayout(etOldPassword);
        tvErrorOldPassword.setText(errorMessage);
        tvErrorOldPassword.setVisibility(View.VISIBLE);
    }

    private void setListeners() {
        etOldPassword.addTextChangedListener(this);
        etNewPassword.addTextChangedListener(this);
        etConfirmNewPassword.addTextChangedListener(this);
    }

    @Override
    protected void initViewsAndVariables() {
        tvTitle.setText(R.string.s_change_password);
        super.initViewsAndVariables();
    }

    public static ChangePasswordFragment getInstance() {
        return new ChangePasswordFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_back, R.id.btn_save,R.id.tv_new_password_visibiity,R.id.tv_confirm_password_visibility})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_confirm_password_visibility:
                if (isConfirmNewPasswordVisible) {
                    tvConfirmPasswordVisibility.setText(R.string.s_hide);
                    etConfirmNewPassword.setTransformationMethod(null);
                    etConfirmNewPassword.setSelection(etConfirmNewPassword.getText().length());
                    isConfirmNewPasswordVisible = false;
                } else {
                    tvConfirmPasswordVisibility.setText(R.string.s_show);
                    etConfirmNewPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etConfirmNewPassword.setSelection(etConfirmNewPassword.getText().length());
                    isConfirmNewPasswordVisible = true;
                }
                break;
            case R.id.tv_new_password_visibiity:
                if (isNewPasswordVisible) {
                    tvNewPasswordVisibility.setText(R.string.s_hide);
                    etNewPassword.setTransformationMethod(null);
                    etNewPassword.setSelection(etNewPassword.getText().length());
                    isNewPasswordVisible = false;
                } else {
                    tvNewPasswordVisibility.setText(R.string.s_show);
                    etNewPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etNewPassword.setSelection(etNewPassword.getText().length());
                    isNewPasswordVisible = true;
                }
                break;
            case R.id.iv_back:
                mHost.onBackPressed();
                break;
            case R.id.btn_save:
                hideErrorFields();
                showProgressDialog();
                mChangePasswordViewModel.onSaveViewClicked(etOldPassword.getText().toString().trim(),etNewPassword.getText().toString().trim(),etConfirmNewPassword.getText().toString().trim());

        }

    }

    private void hideErrorFields() {
        tvErrorOldPassword.setVisibility(View.GONE);
        tvErrorNewPassword.setVisibility(View.GONE);
        tvErrorConfirmPassword.setVisibility(View.GONE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!etOldPassword.getText().toString().isEmpty() && !etNewPassword.getText().toString().isEmpty() && !etConfirmNewPassword.getText().toString().isEmpty()) {
            btnSave.setEnabled(true);
            btnSave.setBackground(getResources().getDrawable(R.drawable.drawable_rectangle_circular_corner_solid_green));
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackground(getResources().getDrawable(R.drawable.drawable_circular_corner_white_three));
        }
        if (etNewPassword.getText().toString().trim().length() > 0) {
            tvNewPasswordVisibility.setVisibility(View.VISIBLE);
        } else tvNewPasswordVisibility.setVisibility(View.GONE);

        if (etConfirmNewPassword.getText().toString().trim().length() > 0) {
            tvConfirmPasswordVisibility.setVisibility(View.VISIBLE);
        } else tvConfirmPasswordVisibility.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        if (context instanceof IChangePasswordHost) {
            mHost = (IChangePasswordHost) context;
        } else throw new IllegalStateException("Host must implement IChangePasswordHost");
        super.onAttach(context);
    }

    @Override
    public void onOkViewClicked() {
        mHost.onBackPressed();
    }

    public interface IChangePasswordHost {

        void onBackPressed();
    }
}
