package com.qd.user.ui.home.recievedorder;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.qd.user.R;
import com.qd.user.base.BaseFragment;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.recievedorders.Datum;
import com.qd.user.ui.home.createdorder.OrderCreatedViewModel;
import com.qd.user.utils.DateFormatter;
import com.qd.user.utils.PaginationListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OrderReceivedFragment extends BaseFragment implements PaginationListener.PaginationListenerCallbacks, OrderReceivedAdapter.IReceivedOrders, DatePickerDialog.OnDateSetListener {

    private static final int MINUTE = 59;
    private static final int HOUR_OF_DAY = 23;
    @BindView(R.id.tv_start_date)
    TextView tvStartDate;
    @BindView(R.id.tv_end_date)
    TextView tvEndDate;
    @BindView(R.id.tv_filter)
    TextView tvFilter;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    @BindView(R.id.tv_label_order_id)
    View tvLabelOrderId;
    @BindView(R.id.tv_day)
    View tvDay;
    @BindView(R.id.tv_order_id)
    View tvOrderId;
    @BindView(R.id.tv_date)
    View tvDate;
    @BindView(R.id.view_my_order)
    View viewMyOrder;
    @BindView(R.id.iv_order_type)
    View ivOrderType;
    @BindView(R.id.tv_order_type)
    View tvOrderType;
    @BindView(R.id.tv_order_status)
    View tvOrderStatus;
    @BindView(R.id.iv_pickup_location)
    View ivPickupLocation;
    @BindView(R.id.tv_label_pickup_location)
    View tvLabelPickupLocation;
    @BindView(R.id.tv_pickup_location)
    View tvPickupLocation;
    @BindView(R.id.view_dotted_line)
    View viewDottedLine;
    @BindView(R.id.iv_drop_off_location)
    View ivDropOffLocation;
    @BindView(R.id.tv_label_drop_off_location)
    View tvLabelDropOffLocation;
    @BindView(R.id.tv_drop_off_location)
    View tvDropOffLocation;
    @BindView(R.id.rl_location_info)
    RelativeLayout rlLocationInfo;
    @BindView(R.id.guidelineStart)
    Guideline guidelineStart;
    @BindView(R.id.guidelineEnd)
    Guideline guidelineEnd;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewContainer;
    @BindView(R.id.rv_orders_listing)
    RecyclerView rvOrdersListing;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tv_no_list)
    TextView tvNoList;

    private OrderCreatedViewModel mReceivedOrderViewModel;
    private int mNextPage;
    private String mFromDate;
    private String mToDate;

    private ArrayList<Object> mReceivedOrdersList;
    private PaginationListener mPaginationListener;
    private boolean isRefreshing;
    private OrderReceivedAdapter mReceivedOrdersAdapter;

    private IReceivedOrderHost mHost;

    private Unbinder unbinder;
    private Date mStartDate;
    private Calendar mCalendar;
    private Date mEndDate;
    private String selectDateFor;
    private LinearLayoutManager layoutManager;

    public static OrderReceivedFragment getInstance() {
        return new OrderReceivedFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mReceivedOrderViewModel = new ViewModelProvider(this).get(OrderCreatedViewModel.class);
        mReceivedOrderViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver(), getLoadingStateObserver());

        observeLiveData();
        clearFilters();

        showShimmerEffect();
        mReceivedOrderViewModel.getReceivedOrdersList(mNextPage, mFromDate, mToDate);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    protected void initViewsAndVariables() {
        mReceivedOrdersList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mCalendar = Calendar.getInstance();
        mPaginationListener = new PaginationListener(layoutManager, this);
        mPaginationListener.setFetchingStatus(false);
        mPaginationListener.setLastPageStatus(false);

        setRefreshLayoutListener();
        setUpAdapter();
    }

    private void observeLiveData() {
        mReceivedOrderViewModel.getOrdersReceivedLiveData().observe(getViewLifecycleOwner(), ordersResponse -> {
            hideShimmerEffect();
            hideProgressDialog();
            if (ordersResponse != null) {

                if (ordersResponse.getResult().getPage() == -1) {
                    mPaginationListener.setLastPageStatus(true);
                    mPaginationListener.setFetchingStatus(true);
                } else {
                    mPaginationListener.setFetchingStatus(false);
                    mPaginationListener.setLastPageStatus(false);
                    mNextPage += 1;
                }
                if (ordersResponse.getResult().getData() != null && ordersResponse.getResult().getData().size() > 0) {
                    if (mReceivedOrdersList != null && mReceivedOrdersList.size() > 0) {
                        mReceivedOrdersAdapter.hideProgress();
                        mPaginationListener.setFetchingStatus(false);
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mReceivedOrdersList.clear();
                        }
                        setResponseToAdapter(ordersResponse.getResult().getData());
                    } else {
                        if (isRefreshing) {
                            isRefreshing = false;
                            swipeRefresh.setRefreshing(false);
                            mReceivedOrdersList.clear();
                        }

                        setResponseToAdapter(ordersResponse.getResult().getData());
                        rvOrdersListing.scheduleLayoutAnimation();
                    }

                } else {
                    hideViewsForDataUnavailable();
                }

            }
        });
    }

    /**
     * hide recycler view and and show no data found  message if data is not available
     */
    private void hideViewsForDataUnavailable() {
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        rvOrdersListing.setVisibility(View.GONE);
        tvNoList.setVisibility(View.VISIBLE);
        tvNoList.setText(R.string.s_no_data_found);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_data, 0, 0);
    }

    /**
     * Sets orders from response to adapter and calls {@code mReceivedOrdersAdapter.notifyDataSetChanged()}
     * to reflect the changes
     *
     * @param ordersList Response containing all the orders, it can't be null and its size will
     *                       always be greater than 0, as it's always called after checking for null
     *                       on its contents.
     */
    private void setResponseToAdapter(Collection<Datum> ordersList) {
        tvNoList.setVisibility(View.GONE);
        rvOrdersListing.setVisibility(View.VISIBLE);
        mReceivedOrdersList.addAll(ordersList);
        mReceivedOrdersAdapter.notifyDataSetChanged();

    }


    /**
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void setRefreshLayoutListener() {
        swipeRefresh.setOnRefreshListener(
                () -> {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    refreshList();
                    mPaginationListener.setFetchingStatus(false);

                }
        );
    }


    public boolean showOrHideFilter() {
        if (llFilter.getVisibility() == View.VISIBLE) {
            llFilter.setVisibility(View.GONE);
            TransitionManager.beginDelayedTransition(llFilter);
            clearFilters();
            mReceivedOrderViewModel.getReceivedOrdersList(mNextPage, mFromDate, mToDate);
            return false;
        } else {
            llFilter.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(llFilter);
            return true;
        }
    }


    private void openDatePicker() {
        Date minDate;
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.setTime(mStartDate);
            minDate = DateFormatter.getInstance().getParsedDate(mReceivedOrderViewModel.getRegistrationDate());
        } else {
            mCalendar.setTime(mEndDate);
            minDate = mStartDate;
        }

        if (getContext() != null) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), this, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(minDate.getTime());
            datePickerDialog.show();
        }
    }


    /**
     * hide recycler view and and show no network  message if networks are not available
     */
    @Override
    public void showNoNetworkError() {
        super.showNoNetworkError();
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrdersListing.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);
    }

    @Override
    protected void onFailure(FailureResponse failureResponse) {
        super.onFailure(failureResponse);
        isRefreshing = false;
        swipeRefresh.setRefreshing(false);
        tvNoList.setText(R.string.s_no_internet_connection);
        tvNoList.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_no_internet, 0, 0);
        tvNoList.setVisibility(View.VISIBLE);
        rvOrdersListing.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);

    }

    @Override
    protected void hideShimmerEffect() {
        super.hideShimmerEffect();
        shimmerViewContainer.stopShimmer();
        shimmerViewContainer.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);

    }

    private void showShimmerEffect() {
        swipeRefresh.setVisibility(View.GONE);
        shimmerViewContainer.setVisibility(View.VISIBLE);
        shimmerViewContainer.startShimmer();
    }


    @Override
    public void loadMoreItems() {
        mReceivedOrdersAdapter.showProgress();
        mReceivedOrderViewModel.getReceivedOrdersList(mNextPage, mFromDate, mToDate);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (selectDateFor.equals(getString(R.string.s_from_date))) {
            mCalendar.set(year, month, dayOfMonth, 0, 0);
            mFromDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mStartDate = DateFormatter.getInstance().getParsedDate(mFromDate);
            tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        } else {
            mCalendar.set(year, month, dayOfMonth, HOUR_OF_DAY, MINUTE);
            mToDate = DateFormatter.getInstance().getFormattedDateInUtc(mCalendar.getTime());
            mEndDate = DateFormatter.getInstance().getParsedDate(mToDate);
            tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(mToDate));
        }
    }


    public boolean isFilterApplied() {
        return llFilter.getVisibility() == View.VISIBLE;
    }



    private void setUpAdapter() {
        mReceivedOrdersAdapter = new OrderReceivedAdapter(mReceivedOrdersList, this);
        rvOrdersListing.setLayoutManager(layoutManager);
        rvOrdersListing.setAdapter(mReceivedOrdersAdapter);
        rvOrdersListing.scheduleLayoutAnimation();
        rvOrdersListing.addOnScrollListener(mPaginationListener);

    }


    @Override
    public void onRowOrderViewClick(String orderId) {
        mHost.openOrderDetailFragment(orderId, true);

    }

    @OnClick({R.id.tv_filter, R.id.tv_start_date, R.id.tv_end_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_filter:
                mNextPage = 1;
                isRefreshing = true;
                showShimmerEffect();
                mReceivedOrderViewModel.getReceivedOrdersList(mNextPage, mFromDate, mToDate);
                break;
            case R.id.tv_start_date:
                selectDateFor = getString(R.string.s_from_date);
                openDatePicker();
                break;
            case R.id.tv_end_date:
                selectDateFor = getString(R.string.s_to_date);
                openDatePicker();
                break;
        }

    }


    @Override
    public void onTrackOrderClicked(String id, boolean isFromReceived) {
        mHost.trackCurrentOrder(id, true);
    }

    private void refreshList() {
        isRefreshing = true;
        mNextPage = 1;
        mReceivedOrderViewModel.getReceivedOrdersList(mNextPage, mFromDate, mToDate);
    }


    private void clearFilters() {
        mNextPage = 1;
        isRefreshing = true;
        mFromDate = mReceivedOrderViewModel.getRegistrationDate();
        mStartDate = DateFormatter.getInstance().getParsedDate(mReceivedOrderViewModel.getRegistrationDate());
        //mToDate = DateFormatter.getInstance().getFormattedDateInUtc(Calendar.getInstance().getTime());
        mEndDate = Calendar.getInstance().getTime();

        tvStartDate.setText(DateFormatter.getInstance().getFormattedDate(mFromDate));
        tvEndDate.setText(DateFormatter.getInstance().getFormattedDate(DateFormatter.getInstance().getFormattedDateInUtc(Calendar.getInstance().getTime())));
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof IReceivedOrderHost) {
            mHost = (IReceivedOrderHost) getParentFragment();
        } else throw new IllegalStateException("Host must implement ICreatedOrderHost");
    }

    public interface IReceivedOrderHost {

        void openOrderDetailFragment(String orderId, boolean isOrderReceived);

        void trackCurrentOrder(String id, boolean isFromReceived);
    }
}
