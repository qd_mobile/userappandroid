package com.qd.user.ui.createorder.selectlocation;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

class SelectLocationViewPagerAdapter extends FragmentPagerAdapter {

    private final String[] mTitleList;
    private ArrayList<Fragment> mFragmentList;
    private String mUserType;

    SelectLocationViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragmentList, String[] titleList, String userType) {
        super(fm);
        mFragmentList = fragmentList;
        mUserType = userType;
        mTitleList = titleList;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = mFragmentList.get(0);
        } else if (position == 1) {
            fragment = mFragmentList.get(1);
        } else if (mUserType != null)
            if (position == 2) {
                fragment = mFragmentList.get(2);
            }

        return fragment;
    }

    @Override
    public int getCount() {
        if (mUserType != null)
            return 3;
        else
            return 2;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList[position];
    }


}
