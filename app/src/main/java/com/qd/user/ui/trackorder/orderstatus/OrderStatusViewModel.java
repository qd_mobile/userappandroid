package com.qd.user.ui.trackorder.orderstatus;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.orderstatus.OrderStatusResponse;

public class OrderStatusViewModel extends ViewModel {

    private RichMediatorLiveData<OrderStatusResponse> mOrderStatusLiveData;
    private RichMediatorLiveData<OrderDetailResponse> mOrdersLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private Observer<Boolean> mLoadingStateObserver;

    private OrderStatusRepository mRepo = new OrderStatusRepository();

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver, Observer<Boolean> loadingStateObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        this.mLoadingStateObserver = loadingStateObserver;
        initLiveData();

    }

    private void initLiveData() {
        if (mOrderStatusLiveData == null) {
            mOrderStatusLiveData = new RichMediatorLiveData<OrderStatusResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };

        }
        if (mOrdersLiveData == null) {
            mOrdersLiveData = new RichMediatorLiveData<OrderDetailResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }

                @Override
                protected Observer<Boolean> getLoadingStateObserver() {
                    return mLoadingStateObserver;
                }
            };
        }

    }

    /**
     * This method gives the login live data object to {@link OrderStatusFragment}
     *
     * @return {@link #mOrdersLiveData}
     */
    RichMediatorLiveData<OrderDetailResponse> getOrdersLiveData() {
        return mOrdersLiveData;
    }

    /**
     * This method gives the login live data object to {@link OrderStatusFragment}
     *
     * @return {@link #mOrderStatusLiveData}
     */
    RichMediatorLiveData<OrderStatusResponse> getOrderStatusLiveData() {
        return mOrderStatusLiveData;
    }


    void emitForOrderStatus(String orderUniqueId) {
        mRepo.emitForOrderStatus(orderUniqueId);
    }

    void listenToOrderStatusEvent() {
        mRepo.listenToOrderStatus(mOrderStatusLiveData);
    }

    public void getCurrentOrderStatus(String orderUniqueId) {
        mRepo.getCurrentOrderStatus(mOrdersLiveData, orderUniqueId);
    }
}
