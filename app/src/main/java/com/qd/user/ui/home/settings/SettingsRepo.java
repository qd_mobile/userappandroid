package com.qd.user.ui.home.settings;

import com.qd.user.base.NetworkCallback;
import com.qd.user.base.RichMediatorLiveData;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.failureresponse.FailureResponse;

class SettingsRepo {

    String getBaseUrl() {
        return DataManager.getInstance().getBaseUrl();
    }

    boolean getNotificationStatus() {
        return DataManager.getInstance().getNotificationStatus();
    }

    void enableOrDisableNotifications(RichMediatorLiveData<CommonResponse> notificationStatusLiveData, boolean isChecked) {
        DataManager.getInstance().enableOrDisableNotifications(DataManager.getInstance().getUserId(), isChecked).enqueue(new NetworkCallback<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                notificationStatusLiveData.setLoadingState(false);
                DataManager.getInstance().setNotificationStatus(commonResponse.getResult().isEnableNotification());
                notificationStatusLiveData.setValue(commonResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                notificationStatusLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                notificationStatusLiveData.setError(t);
            }
        });
    }
}
