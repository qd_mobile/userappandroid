package com.qd.user.constants;

public class AppConstants {

    public static final String KEY_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String APP_ID = "295133241344285";
    public static final int RC_SIGN_IN = 222;
    public static final String COUNTRY_CODE = "+965";
    public static final String USER_DETAIL = "userdetail";
    public static final String CASH_CLIENT = "cashClient";
    public static final String FB_LOGIN_RESPONSE = "fb_login_response";
    public static final String GOOGLE_RESPONSE = "google_response";

    public static final String ANDROID = "ANDROID";
    public static final String NAVIGATE_TO_HOME = "home";
    public static final String NAVIGATE_TO_WELCOME = "welcome";
    public static final String ITEM_DETAILS = "itemDetails";


    public static final String ORDER_TYPE = "orderType";
    public static final String ONLY_CASH_CLIENT = "only cash client";
    public static final String BOTH = "Both";
    public static final String ACTIVE = "active";
    public static final String INACTIVE = "inactive";
    public static final String ONLY_VENDOR = "only vendor";
    public static final String CREATE_ORDER_DETAILS = "createOrderDetails";
    public static final String FACEBOOK = "facebook";
    public static final String GMAIL = "gmail";
    public static final String BUSINESS = "business";

    public static final String VEHICLE_TYPE = "vehicleType";
    public static final String SERVICE_TYPE = "serviceType";
    public static final String IS_FROM_MAP = "isFromMap";
    public static final String SELECTED_ADDRESS = "selectedAddress";
    public static final String IS_FROM_RECIPIENT_DETAILS = "isFromRecipientDetails";
    public static final String EDIT_INDEX = "editIndex";
    public static final String VENDOR_SIGNUP = "VendorSignUp";
    public static final String PAGE_TITLE = "pageTitle";

    public static final String SERVER_CLIENT_ID = "514262648445-642t014ft2el4rmqi4f4b37c330ul2nd.apps.googleusercontent.com";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_DATA = "Order Data";


    public static final int SHAKE_DURATION = 500;
    public static final String ACTION_NAVIGATION = "nav";
    public static final String ACTION_DASHBOARD = "dashboard";
    public static final String ACTION_LOGIN = "login";
    public static final String ACTION_WELCOME = "welcome";
    public static final String ACTION_ORDER_SUMMARY = "order summary";
    public static final String IS_TO_OPTIMIZE = "isToOptimize";
    public static final String INVOICE_ID = "invoiceId";
    public static final String KEY_IS_FOR_EDIT = "edit";
    public static final String KEY_SAVED_ADDRESS_ID = "saved_address_id";
    public static final String KEY_SAVED_ADDRESS_RESULT = "saved_address_result";
    public static final String RESET_TOKEN = "reset_token";
    public static final String KEY_USER_TYPE = "user_type";
    public static final String PAYMENT_URL = "paymentUrl";
    public static final String PAYMENT_REQUEST = "paymentRequest";
    public static final String NOTIFICATION_DATA = "notification_data";
    public static final String IS_ORDER_RECEIVED = "isOrderReceived";
    public static final long DIALOG_DISMISS_TIME_IN_MILLIS = 4000;

    public static final int MIN_ITEM_PRICE_FOR_SPECIAL_ORDERS = 30;
    public static final Double MIN_WALLET_AMOUNT = 30.0;
    public static final String PAYMENT_STATUS = "paymentStatus";
    public static final double KUWAIT_CENTER_LONG = 47.99977619200944;
    public static final double KUWAIT_CENTER_LAT = 29.34102671819135;
    public static final String FLAT = "FLAT";

    public interface orderType {
        String SEND = "Send Item";
        String GET = "Get Item";
        String BUSINESS = "Business";
    }

    public interface userType {
        String CASH_CLIENT = "cashClient";
        String ONLY_CASH_CLIENT = "only cash client";
        String VENDOR = "vendor";
        String ONLY_VENDOR = "only vendor";
        String BOTH = "Both";
    }

    public static class Timings {
        public static final int MAX_HOUR_LIMIT = 14;//22
        public static final int MAX_HOUR_LIMIT_LATER = 13;//21
        public static final int MIN_HOUR_LIMIT = 5;//7
        public static final int MAX_MIN_LIMIT = 30;
        public static final int MIN_MIN_LIMIT = 59;
    }


    public interface Language {
        String ENGLISH = "English";
        String ARABIC = "Arabic";
    }

    public interface ContactUs {
        String WEBSITE = "www.quickdeliveryco.com";
        String EMAIL = "info@quickdeliveryco.com";
        String CONTACT = "+965 224 111 44";
    }

    public interface Navigation {
        String TRACK_ORDER = "Track Order";
        String ORDER_DETAIL = "Order Detail";
        String BUSINESS_ORDER_DETAIL = "Business Order Detail";
        String ONLINE_PAYMENT = "onlinePayment";
        String EDIT_ADDRESS_FROM_MAP = "Edit Address From Map";
        String ORDERS_LIST = "Orders Listing";
        String APPROVE_RETURN = "Approve Return";
        String FINDING_DRIVER = "Finding Driver";
        String NOTIFICATION = "Notification";
    }

    public interface Address {
        String GOVERNORATE = "Governorate";
        String AREA = "Area";
        String BLOCK = "Block Number";
        String STREET = "Street";
        String HOUSE = "House/Building";
    }

    public interface PermissionConstants {

        int REQUEST_CODE_PICK_CONTACTS = 720;
        int REQUEST_IMAGE_PERMISSIONS = 721;
        int REQUEST_STORAGE = 722;
        int REQUEST_LOCATION = 723;
        int CAMERA_PIC_REQUEST = 724;
        int GALLERY_PICK_IMAGE = 725;
        int REQUEST_READ_CONTACTS = 726;
        int REEQUEST_PAYMENT_STATUS = 727;
    }

    public interface DriverTrackingStatus {
        String UNALIGNED = "UNALIGNED";
        String ACCEPT_ORDER = "ACCEPT ORDER";
        String DRIVE_TO_PICKEDUP = "DRIVE TO PICKEDUP";
        String REACHED_AT_PICKUP = "REACHED AT PICKUP";
        String ORDER_PICKEDUP = "ORDER PICKEDUP";
        String DRIVE_TO_DROP_LOCATION = "DRIVE TO DROP LOCATION";
        String REACHED_TO_DROP_LOCATION = "REACHED TO DROP LOCATION";
        String DROP_COMPLETE = "DROP COMPLETE";
        String DELIVERY_ATTEMPT_FAILED = "DELIVERY ATTEMPT FAILED";
        String RETURN_INITIATED = "RETURN INITIATED";
        String RETURNED = "RETURNED";
        String CANCELLED = "CANCELLED";
        String CASHBACK_INITIATED = "CASHBACK INITIATED";
        String CASHBACK_RETURNED = "CASHBACK RETURNED";
    }

    public interface StaticPages {
        String TERMS_AND_CONDITIONS = "Terms & Conditions";
        String ABOUT_US = "About Us";
        String PRIVACY_POLICY = "Privacy Policy";

    }

    public interface OrderTrackingStatus {
        String ORDER_CREATED = "ORDER CREATED";
        String DRIVER_ACEEPTED = "DRIVER ACCEPTED";
        String DRIVING_TO_PICKUP = "DRIVING TO PICKUP";
        String ARRIVED_AT_PICKUP = "ARRIVED AT PICKUP";
        String ORDER_PICKEDUP = "ORDER PICKED";
        String DRIVING_TO_DROP_OFF = "DRIVING TO DROP OFF";
        String ARRIVED_AT_DROP_OFF = "ARRIVED AT DROP OFF";
        String ORDER_DELIVERED = "ORDER DELIVERED";
    }

    public interface OrderStatus {
        String ORDER_CREATED = "ORDER CREATED";
        String DRIVER_ALIGNED = "DRIVER ALIGNED";
        String OUT_FOR_PICKUP = "OUT FOR PICKUP";
        String PICKEDUP = "PICKEDUP";
        String OUT_FOR_DELIVERY = "OUT FOR DELIVERY";
        String DELIVERED = "DELIVERED";
        String RETURNED = "RETURNED";
        String CANCELLED = "CANCELLED";
        String RETURN_INITIATED = "RETURN INITIATED";
        String CASHBACK_INITIATED = "CASHBACK INITIATED";
        String CASHBACK_RETURNED = "CASHBACK RETURNED";
        String AUTO_CANCELLED = "AUTO CANCELLED";
    }

    public interface PaymentMode {
        String WALLET = "WALLET";
        String COD = "COD";
        String INVOICE = "INVOICE";
        String PREPAID = "PREPAID";
        String CASH = "CASH";
        String ONLINE = "ONLINE";
        String KNET = "KNET";
    }

    public interface TransactionFor {
        String ORDER = "ORDER";
        String INVOICE = "INVOICE";
        String WALLET_RECHARGE = "WALLET RECHARGE";
    }

    public interface OrderType {
        String SEND = "SEND";
        String GET = "GET";
        String SPECIAL_SEND = "SPECIAL SEND";
        String SPECIAL_GET = "SPECIAL GET";
        String BUSINESS = "BUSINESS";
    }

    public interface TransactionType {
        String CREDIT = "CREDIT";
    }

    public interface TransactionStatus {
        String SUCCESS = "SUCCESS";
        String FAILURE = "FAILURE";
    }

    public interface DeliveryVehicles {
        String BIKE = "BIKE";
        String COOLER = "COOLER";
        String CAR = "CAR";
        String VAN = "VAN";
    }

    public interface serviceType {
        String PREMIUM = "PREMIUM";
        String FLEXIBLE = "FLEXIBLE";
    }

    public interface NotificationType {
        String ORDER_ACCEPTED_BY_DRIVER = "ORDER_ACCEPTED_BY_DRIVER";
        String ORDER_STATUS_SENDER = "ORDER_STATUS_SENDER";
        String PROMOTIONAL_NOTIFICATION = "PROMOTIONAL_NOTIFICATION";
        String ORDER_STATUS_RECEIVER = "ORDER_STATUS_RECEIVER";
        String ISSUE_REPORTED_BY_DRIVER = "ISSUE_REPORTED_BY_DRIVER";
    }

    public static class UI_VALIDATIONS {
        public static final int INVALID_FIRST_NAME = 101;
        public static final int INVALID_LAST_NAME = 102;
        public static final int INVALID_EMAIL = 103;
        public static final int INVALID_PASSWORD = 104;
        public static final int INVALID_CONFIRM_PASSWORD = 105;
        public static final int INVALID_PHONE_NUMBER = 106;
        public static final int INVALID_NAME = 107;
        public static final int INVALID_OLD_PASSWORD = 108;
        public static final int INVALID_NEW_PASSWORD = 109;
        public static final int INVALID_CONFIRM_NEW_PASSWORD = 110;

        public static final int INVALID_GOVERNORATE = 111;
        public static final int INVALID_AREA = 112;
        public static final int INVALID_BLOCK = 113;
        public static final int INVALID_STREET = 114;
        public static final int INVALID_ITEM_DESCRIPTION = 115;
        public static final int INVALID_ITEM_TYPE = 116;
        public static final int INVALID_ITEM_IMAGES = 117;
        public static final int INVALID_PICKUP_LOCATION = 119;
        public static final int INVALID_DROP_LOCATION = 120;
        public static final int INVALID_VEHICLE_TYPE = 121;
        public static final int INVALID_COMPANY_NAME = 118;
        public static final int INVALID_TIME_OF_PAYMENT = 125;
        public static final int INVALID_ITEM_PRICE = 124;
        public static final int INVALID_LAT_LNG = 128;
        public static final int INVALID_USERNAME = 129;
        public static final int INVALID_DESCRIPTION = 130;
        public static final int INVALID_PAYMENT_MODE = 131;
        public static final int INVALID_SENDER_NAME = 132;
        public static final int INVALID_SENDER_CONTACT = 133;
        public static final int INVALID_ADDRESS = 135;
        public static final int INVALID_PROMO_CODE = 136;
        public static final int INVALID_GEOCODE = 137;
    }

}
