package com.qd.user.constants;

public class NetworkConstants {

    public static final String KEY_EMAIL ="email" ;
    public static final String KEY_USERNAME ="username" ;
    public static final String KEY_PASSWORD="password";
    public static final String KEY_DEVICE_ID ="deviceId" ;
    public static final String KEY_TYPE ="type" ;
    public static final String KEY_PLATFORM = "platform";
    public static final String KEY_FIRST_NAME ="firstName" ;
    public static final String KEY_LAST_NAME ="lastName" ;
    public static final String KEY_RE_PASSWORD = "repassword";
    public static final String KEY_CC_PERSONAL = "ccPersonal";
    public static final String KEY_PERSONAL = "personal";
    public static final String KEY_CONTACT = "contact";
    public static final String KEY_SOCIAL_PLATFORM ="socialPlatform" ;
    public static final String KEY_APP_ID ="appId" ;
    public static final String KEY_DEVICE_TOKEN ="deviceToken" ;
    public static final String KEY_NAME ="name" ;
    public static final String KEY_CASH_CLIENT_ID ="cashClientId" ;
    public static final String KEY_OLD_PASSWORD ="oldpassword" ;
    public static final String KEY_NEW_PASSWORD ="newpassword" ;
    public static final String KEY_CONFIRM_NEW_PASSWORD ="re-password" ;

    public static final String KEY_IS_FOR_PICKUP = "isForPickup";
    public static final String KEY_TOKEN ="token" ;
    public static final String KEY_OTP = "otp";
    public static final String KEY_PHONE_NUMBER = "phoneNumber";
    public static final String KEY_COUNTRY_CODE = "countryCode";
    public static final String KEY_IS_CASH_CLIENT = "isCashClient";
    public static final String KEY_NAME_ENGLISH = "nameEnglish";
    public static final String KEY_BUSINESS_ADDRESS = "businessAddress";

    public static final String KEY_BUSINESS_TYPE = "businessType";
    public static final String KEY_COMPANY = "company";
    public static final String KEY_VENDOR_ID = "vendorId";
    public static final String KEY_PARAM_TYPE = "paramType";

    public static final String KEY_PARAM_VALUE = "paramVal";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_PAGE = "page";
    public static final String KEY_LIMIT = "limit";
    public static final String KEY_REGISTER_FROM = "regFrom";
    public static final String KEY_REGISTER_TO = "regTo";

    public static final String KEY_COMPANY_PROFILE = "companyProfile";
    public static final String VENDOR_ID = "vendorId";
    public static final String TRANSACTION_FOR = "transactionFor";
    public static final String AMOUNT = "amount";
    public static final String ORDER_ID = "orderId";
    public static final String ANDROID = "Android";


    public static final String USER = "User";
    public static final String APP_TYPE = "appType";
    public static final String VERSION_NUMBER = "versionNumber";
}
