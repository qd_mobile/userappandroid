package com.qd.user.base;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.utils.LogUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Response;

public abstract class NetworkCallback<T> implements retrofit2.Callback<T> {

    private String className = NetworkCallback.class.getName();
    public static final int NO_INTERNET = 9;
    private static final int FAILURE_CODE = 500;

    protected abstract void onSuccess(T t);

    protected abstract void onFailure(FailureResponse failureResponse);

    protected abstract void onError(Throwable t);

    @Override
    public void onResponse(@NonNull retrofit2.Call<T> call, @NonNull Response<T> response) {
        if (response.isSuccessful()) {
            printResponse(call, response, null);
            onSuccess(response.body());
        } else {
            FailureResponse failureErrorBody = getFailureErrorBody(call.request().url().url().getFile(), response);
            printResponse(call, response, failureErrorBody);
            onFailure(failureErrorBody);
        }
    }

    @Override
    public void onFailure(@NonNull retrofit2.Call<T> call, @NonNull Throwable t) {
        if (t instanceof SocketTimeoutException || t instanceof UnknownHostException) {
            FailureResponse failureResponseForNoNetwork = getFailureResponseForNoNetwork(t.toString());
            printFailure(failureResponseForNoNetwork);
            onFailure(failureResponseForNoNetwork);
        } else {
            onError(t);
        }
    }

    private FailureResponse getFailureResponseForNoNetwork(String s) {
        FailureResponse failureResponse = new FailureResponse();
        failureResponse.setErrorMessage("No network");
        failureResponse.setErrorCode(NO_INTERNET);
        return failureResponse;
    }

    /**
     * Create your custom failure response out of server response
     * Also save Url for any further use
     */
    private FailureResponse getFailureErrorBody(String url, Response<T> errorBody) {
//        Log.e("On failure", url);
        FailureResponse failureResponse = new FailureResponse();
        if (errorBody.code() == FAILURE_CODE) {
            failureResponse.setErrorCode(errorBody.code());
            failureResponse.setErrorMessage(errorBody.message());
            return failureResponse;
        } else {

            try {
                JSONObject jsonObject = new JSONObject(errorBody.errorBody() != null ? errorBody.errorBody().string() : null);
                failureResponse.setErrorCode(jsonObject.getInt("statusCode"));
                failureResponse.setErrorMessage(jsonObject.getString("message"));
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            return failureResponse;
        }
    }

    private void printResponse(retrofit2.Call<T> call, Response<T> response, FailureResponse failureResponse) {
        Gson gson = new Gson();
        try {
            if (failureResponse == null) {
                LogUtil.debug(className, "--------------------------------");
                LogUtil.debug(className, "method====> " + call.request().method());
                LogUtil.debug(className, "url====> " + call.request().url().toString());

                LogUtil.debug(className, gson.toJson(response.body()));

                LogUtil.debug(className, "--------------------------------");
            } else {
                LogUtil.error(className, "--------------------------------");
                if (call != null ) {
                    LogUtil.error(className, "method====> " + call.request().method());
                    LogUtil.error(className, "url====> " + call.request().url().toString());
                }

//                if (response.body() instanceof FailureResponse)
                    LogUtil.error(className, gson.toJson(failureResponse));

                LogUtil.error(className, "--------------------------------");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printFailure(@NonNull FailureResponse err) {
        Gson gson = new Gson();

        LogUtil.error(className, "--------------------------------");
        try {
            LogUtil.error(className, "printFailure()", gson.toJson(err));

        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.error(className, "--------------------------------");
    }

}
