package com.qd.user.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;
import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.ui.onboard.OnBoardActivity;
import com.qd.user.utils.AppUtils;
import com.qd.user.utils.LocaleManager;
import com.qd.user.utils.ShakeAnimation.Animation;
import com.qd.user.utils.ShakeAnimation.Techniques;

import java.util.Objects;

import butterknife.ButterKnife;

import static com.qd.user.ui.createorder.findingdriver.FindingDriverFragment.CANCELLATION_FAILURE;

public abstract class BaseActivity extends AppCompatActivity {

    public static final int UNAUTHORIZED = 401;
    private static final int SESSION_EXPIRED_CODE = 499;
    private ConstraintLayout baseContainer;
    private Observer<Throwable> errorObserver;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Boolean> loadingStateObserver;
    private Snackbar snackbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        baseContainer = findViewById(R.id.cl_base_container);
        setLayout();
        ButterKnife.bind(this);
        initObservers();
        initViewsAndVariables();


    }

    protected abstract void initViewsAndVariables();

    private void initObservers() {
        errorObserver = this::onErrorOccurred;
        failureResponseObserver = this::onFailure;
        loadingStateObserver = aBoolean -> onLoadingStateChanged(aBoolean != null ? aBoolean : false);


    }
    private void onLoadingStateChanged(Boolean aBoolean) {
        if(aBoolean){
            AppUtils.getInstance().showProgressDialog(this);
        }else AppUtils.getInstance().hideProgressDialog();
    }

    protected Observer<Boolean> getLoadingStateObserver() {
        return loadingStateObserver;
    }

    protected Observer<Throwable> getErrorObserver() {
        return errorObserver;
    }

    protected Observer<FailureResponse> getFailureResponseObserver() {
        return failureResponseObserver;
    }


    private void onFailure(FailureResponse failureResponse) {
        hideProgressDialog();
        hideShimmerEffect();
        if (failureResponse.getErrorCode() == 9) {
            showNoNetworkError();
        } else if (failureResponse.getErrorCode() == UNAUTHORIZED || failureResponse.getErrorCode() == SESSION_EXPIRED_CODE)
            autoLogout();
        else if (failureResponse.getErrorCode() != CANCELLATION_FAILURE)
            showSnackBar(failureResponse.getErrorMessage());

    }

    public void autoLogout() {
        DataManager.getInstance().clearPreference();
        Intent intent = new Intent(this, OnBoardActivity.class);
        intent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.ACTION_LOGIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAfterTransition();
    }

    private void onErrorOccurred(Throwable throwable) {
        showSnackBar(throwable.getMessage());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.onAttach(base));
    }


    /**
     * Method is used to set the layout in the Base Activity.
     * Layout params for the inserted child is match parent
     */
    private void setLayout() {
        if (getResourceId() != -1) {
            removeLayout();
            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT
                    , ConstraintLayout.LayoutParams.MATCH_PARENT);

            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                View view = layoutInflater.inflate(getResourceId(), null);
                baseContainer.addView(view, layoutParams);
            }
        }
    }

    public void enterFullScreenMode() {
        if (getWindow() != null) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    protected void exitFullScreenMode() {
        //Here activity will exit the full screen mode
        if (getWindow() != null) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }

    /**
     * Method is used by the sub class for passing the id of the layout to be inflated in the relative layout
     *
     * @return id of the resource to be inflated
     */
    protected abstract int getResourceId();


    /**
     * This method is used to remove the view already present as a child in relative layout.
     */
    private void removeLayout() {
        if (baseContainer.getChildCount() >= 1)
            baseContainer.removeAllViews();
    }

    public void hideKeyboard() {
        try {
            // use application level context to avoid unnecessary leaks.
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(Objects.requireNonNull(((Activity) getApplicationContext()).getCurrentFocus()).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* *//**
     * To shake layout on error
     *
     */
    public void shakeLayout(TextView textView) {
        Animation.with(Techniques.Shake).duration(AppConstants.SHAKE_DURATION).repeat(1)
                .playOn(textView);
    }

    public String getDeviceId(){
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * hides keyboard onClick anywhere besides edit text
     *
     * @param event Event that user has clicked somewhere besides edit text
     * @return Boolean
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] coordinates = new int[2];
            view.getLocationOnScreen(coordinates);
            float x = event.getRawX() + view.getLeft() - coordinates[0];
            float y = event.getRawY() + view.getTop() - coordinates[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) Objects.requireNonNull(this.getSystemService(Context.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(event);
    }

    protected void addFragment(int layoutResId, BaseFragment fragment, String tag) {
        if (getSupportFragmentManager().findFragmentByTag(tag) == null)
            getSupportFragmentManager().beginTransaction()
                    /* .setCustomAnimations(R.anim.frag_pop_enter, R.anim.frag_pop_exit,
                             R.anim.frag_pop_enter, R.anim.frag_pop_exit)*/
                    .add(layoutResId, fragment, tag)
                    .commitAllowingStateLoss();
    }

    protected void addFragmentWithBackStack(int layoutResId, BaseFragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                /* .setCustomAnimations(R.anim.frag_pop_enter, R.anim.frag_pop_exit, R.anim.frag_pop_enter, R.anim.frag_pop_exit)*/
                .add(layoutResId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }


    protected void replaceFragment(int layoutResId, BaseFragment fragment, String tag) {
        if (getSupportFragmentManager().findFragmentByTag(tag) == null)
            getSupportFragmentManager().beginTransaction()
                    /* .setCustomAnimations(R.anim.frag_pop_enter, R.anim.frag_pop_exit, R.anim.frag_pop_enter, R.anim.frag_pop_exit)*/
                    .replace(layoutResId, fragment, tag)
                    .commitAllowingStateLoss();
    }

    public void replaceFragmentWithBackstack(int layoutResId, BaseFragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(layoutResId, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    public void replaceFragmentWithBackstackWithStateLoss(int layoutResId, BaseFragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(layoutResId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }

    public void popFragment() {
        if (getSupportFragmentManager() != null) {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }


    public void showToastLong(CharSequence message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showToastShort(CharSequence message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * check for availability of Google play services
     *
     * @return true, if available
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);
        return resultCode == ConnectionResult.SUCCESS;
    }


    public void showSnackBar(CharSequence message) {
        //final Snackbar snackbar;
        snackbar = Snackbar.make(baseContainer, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.s_ok), v -> snackbar.dismiss());
        snackbar.setActionTextColor(
                ContextCompat.getColor(this, R.color.colorNewWhite)
        );
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorTransparentGrey));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.colorNewWhite));
        snackbar.show();
    }

    public void showSnackBar(int message) {
        //final Snackbar snackbar;
        snackbar = Snackbar.make(baseContainer, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.s_ok), v -> snackbar.dismiss());
        snackbar.setActionTextColor(
                ContextCompat.getColor(this, R.color.colorNewWhite)
        );
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorTransparentGrey));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.colorNewWhite));
        snackbar.show();
    }

    public void hideSnackBar() {
        if (snackbar != null)
            snackbar.dismiss();
    }

    public void showNoNetworkError() {
        hideProgressDialog();
        hideShimmerEffect();
    }

    private void hideShimmerEffect() {
    }

    public void showProgressDialog() {
        AppUtils.getInstance().showProgressDialog(this);
    }

    public void hideProgressDialog() {
        AppUtils.getInstance().hideProgressDialog();
    }


}
