package com.qd.user.base;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.qd.user.model.failureresponse.FailureResponse;
import com.qd.user.utils.AppUtils;

import static com.qd.user.base.BaseActivity.UNAUTHORIZED;
import static com.qd.user.base.NetworkCallback.NO_INTERNET;
import static com.qd.user.ui.createorder.findingdriver.FindingDriverFragment.CANCELLATION_FAILURE;


public class BaseFragment extends Fragment {

    public static final int SESSION_FAILURE = 401;
    private static final int SESSION_EXPIRED_CODE = 499;
    private Observer<Throwable> errorObserver;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Boolean> loadingStateObserver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initObservers();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewsAndVariables();
    }

    protected void initViewsAndVariables() {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).initViewsAndVariables();

    }

    private void initObservers() {
        errorObserver = this::onErrorOccurred;

        failureResponseObserver = this::onFailure;

        loadingStateObserver = this::onLoadingStateChanged;

    }

    private void onLoadingStateChanged(Boolean aBoolean) {
        if(aBoolean){
            AppUtils.getInstance().showProgressDialog(getActivity());
        }else AppUtils.getInstance().hideProgressDialog();
    }

    protected Observer<Throwable> getErrorObserver() {
        return errorObserver;
    }

    protected Observer<FailureResponse> getFailureResponseObserver() {
        return failureResponseObserver;
    }

    protected Observer<Boolean> getLoadingStateObserver() {
        return loadingStateObserver;
    }

    protected void onFailure(FailureResponse failureResponse) {
        hideProgressDialog();
        hideShimmerEffect();
        if (failureResponse.getErrorCode() == NO_INTERNET) {
            showNoNetworkError();
        } else if (failureResponse.getErrorCode() == UNAUTHORIZED || failureResponse.getErrorCode() == SESSION_EXPIRED_CODE)
            autoLogout();
        else if (failureResponse.getErrorCode() != CANCELLATION_FAILURE)
            showSnackBar(failureResponse.getErrorMessage());

    }

    private void autoLogout() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).autoLogout();
        }
    }

    protected void hideShimmerEffect() {

    }

    protected void onErrorOccurred(Throwable throwable) {
        hideProgressDialog();
        hideShimmerEffect();
        showSnackBar(throwable.getMessage());

    }

    protected String getDeviceId(){
        if(getActivity()!=null)
            return ((BaseActivity) getActivity()).getDeviceId();

        return null;
    }

    public void showToastLong(CharSequence message) {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).showToastLong(message);
    }

    public void showToastShort(CharSequence message) {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).showToastShort(message);
    }

    protected void hideKeyboard() {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).hideKeyboard();
    }

    public void showNoNetworkError() {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).showNoNetworkError();
    }

    protected void addFragmentWithoutAnimation(int layoutResId, Fragment fragment, String tag) {
        if (getChildFragmentManager().findFragmentByTag(tag) == null)
            getChildFragmentManager().beginTransaction()
                    .add(layoutResId, fragment, tag)
                    .commitAllowingStateLoss();
    }

    /**
     * To shake layout on error
     *
     * @param textView for shaking.
     */
    protected void shakeLayout(TextView textView) {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).shakeLayout(textView);
    }


    protected void showProgressDialog() {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showProgressDialog();
    }

    protected void hideProgressDialog() {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).hideProgressDialog();
    }

    protected void showSnackBar(CharSequence message) {
        if (getActivity() != null && !getActivity().isFinishing())
            ((BaseActivity) getActivity()).showSnackBar(message);
    }

    protected void hideSnackBar() {
        if(getActivity()!=null)
            ((BaseActivity) getActivity()).hideSnackBar();
    }

    protected void showSnackBar(int message) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showSnackBar(message);
    }

    public void popFragment() {
        if (getFragmentManager() != null) {
            getFragmentManager().popBackStackImmediate();
        }
    }


}
