package com.qd.user.base;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.qd.user.model.failureresponse.FailureResponse;


public abstract class RichMediatorLiveData<T> extends MediatorLiveData<T> {

    private MutableLiveData<Throwable> errorLiveData;
    private MutableLiveData<FailureResponse> failureResponseLiveData;
    private MutableLiveData<Boolean> loadingStateLiveData;


    private void initLiveData() {
        if(errorLiveData==null)
        errorLiveData = new MutableLiveData<>();
        if(failureResponseLiveData==null)
        failureResponseLiveData = new MutableLiveData<>();
        if(loadingStateLiveData ==null)
            loadingStateLiveData = new MutableLiveData<>();
    }

    protected abstract Observer<FailureResponse> getFailureObserver();
    protected abstract Observer<Throwable> getErrorObserver();
    protected abstract Observer<Boolean> getLoadingStateObserver();


    @Override
    protected void onInactive() {
        super.onInactive();
        removeSource(failureResponseLiveData);
        removeSource(errorLiveData);
        removeSource(loadingStateLiveData);

    }

    @Override
    protected void onActive() {
        super.onActive();
        initLiveData();
        addSource(failureResponseLiveData, getFailureObserver());
        addSource(errorLiveData, getErrorObserver());
        addSource(loadingStateLiveData,getLoadingStateObserver());

    }


    public void setFailure(FailureResponse failureResponse) {
        loadingStateLiveData.postValue(false);
        failureResponseLiveData.setValue(failureResponse);
    }

    public void setError(Throwable t) {
        loadingStateLiveData.postValue(false);
        errorLiveData.setValue(t);
    }
    public void setLoadingState(Boolean b){
        loadingStateLiveData.setValue(b);
    }
}
