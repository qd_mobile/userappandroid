package com.qd.user.utils;

import android.app.Activity;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.qd.user.constants.AppConstants;

public class GoogleSignIn {

    private static GoogleSignIn mGoogleSignIn;

    public static GoogleSignIn getInstance() {
        if (mGoogleSignIn == null) {
            mGoogleSignIn = new GoogleSignIn();
        }
        return mGoogleSignIn;
    }

    public static GoogleSignInClient getGoogleSignInClient(Activity activity) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(AppConstants.SERVER_CLIENT_ID)
                .requestEmail().build();
        return com.google.android.gms.auth.api.signin.GoogleSignIn.getClient(activity, gso);
    }
}
