package com.qd.user.utils.maps;

import com.google.android.gms.maps.model.PolylineOptions;

public interface IDirectionsParserInterface {

    void setPolyLineOptions(PolylineOptions polyLineOptions);

    void showNoRouteFound();
}
