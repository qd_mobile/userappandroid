package com.qd.user.utils.glideforsvg;

import java.io.InputStream;

/**
 * Decodes an SVG internal representation from an {@link InputStream}.
 */
/*class SvgDecoder implements ResourceDecoder<InputStream, SVG> {

    @Override
    public boolean handles(@NonNull InputStream source, @NonNull Options options) {
        return true;
    }

    public Resource<SVG> decode(@NonNull InputStream source, int width, int height,
                                @NonNull Options options)
            throws IOException {
        try {
            SVG svg = SVG.getFromInputStream(source);
            return new SimpleResource<>(svg);
        } catch (SVGParseException ex) {
            throw new IOException("Cannot load SVG from stream", ex);
        }
    }
}*/
