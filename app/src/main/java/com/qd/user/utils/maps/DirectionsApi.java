package com.qd.user.utils.maps;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class DirectionsApi {
    private static final String DIRECTIONS_API_BASE = "https://maps.googleapis.com/maps/api/directions/";
    private static final String MODE = "driving";
    private static final String OUTPUT_FORMAT = "json";
    private static final String API_KEY = "AIzaSyBr7jzQzAqTsnxjctV3_Mxe5yXiCl2jqq4";


    /*public String directionRequestUrl(LatLng origin, LatLng destination) {

        String request;
        request = DIRECTIONS_API_BASE +
                OUTPUT_FORMAT +
                "?origin=" + origin.latitude + "," + origin.longitude +
                "&destination=" + destination.latitude + "," + destination.longitude +
                "&mode=" + MODE +
                "&key=" + API_KEY;

        return request;

    }*/

    public String directionRequestUrl(LatLng origin, List<LatLng> dropsList, boolean isToOptimize) {

        StringBuilder request;
        request = new StringBuilder(DIRECTIONS_API_BASE +
                OUTPUT_FORMAT +
                "?origin=" + origin.latitude + "," + origin.longitude +
                "&destination=" + origin.latitude + "," + origin.longitude +
                "&waypoints=optimize:" + isToOptimize + "|");


        for (int i = 0; i < dropsList.size(); i++) {
            request.append(dropsList.get(i).latitude).append(",").append(dropsList.get(i).longitude);
            request.append("|");
        }

        if (request.charAt(request.length() - 1) == '|')
            request.deleteCharAt(request.length() - 1);
        request.append("&mode=" + MODE + "&key=" + API_KEY);

        return request.toString();

    }
}
