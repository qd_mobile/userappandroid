package com.qd.user.utils.notifications;

import com.google.gson.Gson;
import com.qd.user.constants.AppConstants;
import com.qd.user.model.createorder.response.Result;
import com.qd.user.model.myorders.report.ReportNotificationResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class HandlePushNotifications {

    private static HandlePushNotifications handlePushNotifications;
    private Object obj;


    /**
     * Returns instance of {@link HandlePushNotifications} to communicate and access required methods
     *
     * @return HandlePushNotifications Instance
     */
    public static HandlePushNotifications getInstance() {
        if (handlePushNotifications == null) {
            handlePushNotifications = new HandlePushNotifications();
        }
        return handlePushNotifications;
    }

    public Object parseNotificationData(JSONObject jsonObject) {

        String type = "";
        try {
            type = jsonObject.getString("type");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        switch (type) {
            case AppConstants.NotificationType.ORDER_ACCEPTED_BY_DRIVER:
            case AppConstants.NotificationType.ORDER_STATUS_SENDER:
            case AppConstants.NotificationType.ORDER_STATUS_RECEIVER:
                try {
                    obj = new Gson().fromJson(jsonObject.getString("notificationData"), Result.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case AppConstants.NotificationType.ISSUE_REPORTED_BY_DRIVER:
                try {
                    obj = new Gson().fromJson(jsonObject.getString("notificationData"), ReportNotificationResponse.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

        return obj;
    }

}
