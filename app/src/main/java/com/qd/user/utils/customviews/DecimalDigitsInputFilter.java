package com.qd.user.utils.customviews;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DecimalDigitsInputFilter implements InputFilter {

    private Pattern mPattern;

    public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
        mPattern = Pattern.compile("\\d{0,3}?(\\.\\d{0,2})?");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        if (dest.length() > 2 && !dest.toString().contains(".") && !source.toString().equals(".")) {
            return "";
        } else {
            Matcher matcher = mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
        }
        return null;
    }

}
