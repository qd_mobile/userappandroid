package com.qd.user.utils.maps;

import com.google.android.gms.maps.model.LatLng;

class DistanceTimeMatrix {
    private static final String DISTANCE_TIME_MATRIX_API_BASE = "https://maps.googleapis.com/maps/api/distancematrix/";
    private static final String OUTPUT_FORMAT = "json";
    private static final String MODE = "driving";


    public String directionRequestUrl(LatLng origin, LatLng destination) {

        String request;
        request = DISTANCE_TIME_MATRIX_API_BASE +
                OUTPUT_FORMAT +
                "?origins=" + origin.latitude + "," + origin.longitude +
                "&destinations=" + destination.latitude + "," + destination.longitude +
                "&mode=" + MODE +
                "&key="
        // + API_KEY
        ;

        return request;

    }
}
