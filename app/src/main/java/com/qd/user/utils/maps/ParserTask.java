package com.qd.user.utils.maps;

import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParserTask extends
        AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

    private IDirectionsParserInterface mInterface;

    public ParserTask(IDirectionsParserInterface directionsParserInterface) {
        mInterface = directionsParserInterface;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(
            String... jsonData) {

        JSONObject jsonObject;
        List<List<HashMap<String, String>>> routes = null;

        try {
            jsonObject = new JSONObject(jsonData[0]);
            PathJSONParser parser = new PathJSONParser();
            routes = parser.parse(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
        List<LatLng> points;
        PolylineOptions polylineOptions = null;

        if (routes.size() > 0) {
            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<>();
                polylineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polylineOptions.addAll(points);
                polylineOptions.width(8);
                polylineOptions.color(Color.rgb(73, 206, 142));
            }
            mInterface.setPolyLineOptions(polylineOptions);
        }else {
            mInterface.showNoRouteFound();
        }
    }

}
