package com.qd.user.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PaginationListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager mLinearLayoutManager;
    private boolean isLoading;
    private boolean isLastPage;
    private int firstVisibleItemPosition;
    private int totalItemCount;
    private int visibleItemCount;
    private long thresholdItemCount = 3;
    private PaginationListenerCallbacks paginationListenerCallbacks;

    public PaginationListener(LinearLayoutManager layoutManager, PaginationListenerCallbacks callbacks) {
        mLinearLayoutManager = layoutManager;
        paginationListenerCallbacks = callbacks;
    }

    public PaginationListener(LinearLayoutManager layoutManager, PaginationListenerCallbacks callbacks, long thresholdItemCount) {
        mLinearLayoutManager = layoutManager;
        paginationListenerCallbacks = callbacks;
        this.thresholdItemCount = thresholdItemCount;

    }

    public boolean isLoading() {
        return isLoading;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public void setLastPageStatus(boolean isLastPage) {
        this.isLastPage = isLastPage;
    }

    public void setFetchingStatus(boolean isLoadingDone) {
        this.isLoading = isLoadingDone;
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0 && !isLoading && !isLastPage) {
            visibleItemCount = mLinearLayoutManager.getChildCount();//5
            totalItemCount = mLinearLayoutManager.getItemCount();//50
            firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();//41

            //43+5>=50-2
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount - thresholdItemCount) {
                isLoading = true;
                paginationListenerCallbacks.loadMoreItems();
            }
        }
    }

    public interface PaginationListenerCallbacks {
        void loadMoreItems();
    }

}