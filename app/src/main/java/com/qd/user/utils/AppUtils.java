package com.qd.user.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.qd.user.BuildConfig;
import com.qd.user.QuickDelivery;
import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.utils.ShakeAnimation.Animation;
import com.qd.user.utils.ShakeAnimation.Techniques;

import java.io.File;


public class AppUtils {

    private static final float DIM_AMOUNT = 0.9f;
    private static AppUtils appUtils;
    private static Dialog alertPermissionSetting;
    private ProgressDialog mDialog;
    private static String APP_IMAGE_FOLDER = Environment.getExternalStorageDirectory().toString() + "/.quickDelivery";


    /**
     * Returns instance of {@link AppUtils} to communicate and access required methods
     * @return AppUtils Instance
     */
    public static AppUtils getInstance() {
        if (appUtils == null) {
            appUtils = new AppUtils();
        }
        return appUtils;
    }

    /**
     * This method is used to show permission allow from Setting
     */
    static void showAllowPermissionFromSettingDialog(final Activity activity, CharSequence message, CharSequence title) {
        if (alertPermissionSetting != null && alertPermissionSetting.isShowing()) {
            alertPermissionSetting.dismiss();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setPositiveButton(activity.getString(R.string.action_turn_on),
                (dialog, id) -> {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", activity.getPackageName(), null));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    dialog.dismiss();
                });

        /*builder.setNegativeButton(activity.getString(R.string.s_cancel),
                (dialog, id) -> dialog.dismiss());*/

        alertPermissionSetting = builder.create();
        alertPermissionSetting.show();
    }

    @SuppressLint("HardwareIds")
    public String getDeviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    /**
     * method to dim the dialog background
     *
     * @param confirmationDialog Dialog whose background is to be altered
     */
    public void dimDialogBackground(Dialog confirmationDialog) {
        if (confirmationDialog.getWindow() != null) {
            WindowManager.LayoutParams lp = confirmationDialog.getWindow().getAttributes();
            lp.dimAmount = DIM_AMOUNT;
            confirmationDialog.getWindow().setAttributes(lp);
        }
    }

    /**
     * method to dim the dialog background
     *
     * @param confirmationDialog Dialog whose background is to be altered
     */
    public void dimDialogBackgroundWithBottomGravity(Dialog confirmationDialog) {
        if (confirmationDialog.getWindow() != null) {
            WindowManager.LayoutParams lp = confirmationDialog.getWindow().getAttributes();
            lp.dimAmount = 0.9f;
            lp.gravity = Gravity.BOTTOM;
            confirmationDialog.getWindow().setAttributes(lp);
        }
    }


    /**
     * Shows progress dialog in case of any network call or if any task is going on
     * in background
     *
     * @param context Context of activity
     */
    public void showProgressDialog(Context context) {
        hideProgressDialog();
        if (!((Activity) context).isFinishing()) {
            mDialog = new ProgressDialog(context, R.style.ProgressTheme);
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
            mDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            mDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.drawable_progress_loader));
            mDialog.show();
        }
    }
        /**
         * To shake layout on error
         *
         * @param textView for shaking.
         */
        public void shakeLayout(TextView textView){
            Animation.with(Techniques.Shake).duration(AppConstants.SHAKE_DURATION).repeat(1)
                    .playOn(textView);
        }



    /**
     * hide progress dialog if open
     */
    public void hideProgressDialog() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    /**
     * method to create action Alert Dialog
     */
    public void showAlertDialog(Context context, CharSequence message, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.s_yes), positive);
        if (negative != null)
            builder.setNegativeButton(context.getString(R.string.s_no), negative);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    public void openSuccessDialog(Context context, CharSequence message, final ISuccess iSuccess) {
        final Dialog successDialog = new Dialog(context, R.style.customDialog);
        successDialog.setContentView(R.layout.dialog_forgot_password);
        TextView tvMessage = successDialog.findViewById(R.id.tv_message);
        tvMessage.setText(message);
        ImageView ivTick = successDialog.findViewById(R.id.iv_resent_link);
        ivTick.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_password_changed));
        dimDialogBackground(successDialog);
        Button btnOk = successDialog.findViewById(R.id.btn_ok);
        successDialog.show();

        btnOk.setOnClickListener(v -> {
            successDialog.dismiss();
            iSuccess.onOkViewClicked();
        });

    }

    public void openConfirmationDialog(Context context, CharSequence message, final IConfirmationDialog iConfirmationDialog) {
        final Dialog confirmationDialog = new Dialog(context, R.style.customDialog);
        confirmationDialog.setContentView(R.layout.dialog_log_out);
        TextView tvMessage = confirmationDialog.findViewById(R.id.tv_message);
        tvMessage.setText(message);
        AppUtils.getInstance().dimDialogBackground(confirmationDialog);
        Button btnLogout = confirmationDialog.findViewById(R.id.btn_logout);
        Button btnCancel = confirmationDialog.findViewById(R.id.btn_cancel);
        btnLogout.setText(R.string.s_yes);
        btnCancel.setText(R.string.s_no);
        confirmationDialog.show();

        btnLogout.setOnClickListener(v -> {
            confirmationDialog.dismiss();
            iConfirmationDialog.onYesViewClicked();


        });
        btnCancel.setOnClickListener(v -> confirmationDialog.dismiss());
    }


    public interface IConfirmationDialog {

        void onYesViewClicked();
    }


    public String getFilePath() {
        File file = new File(APP_IMAGE_FOLDER);
        boolean isDirCreated = false;
        if (!file.exists()) {
            isDirCreated = file.mkdirs();
        }

        return (isDirCreated ? file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpeg" : null);
    }


   /* public boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        return (connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null) != null;
    }*/

    public void openNoServiceDialog(Context context, CharSequence message) {
        final Dialog logOutDialog = new Dialog(context, R.style.customDialog);
        logOutDialog.setContentView(R.layout.dialog_log_out);
        TextView tvMessage = logOutDialog.findViewById(R.id.tv_message);
        tvMessage.setText(message);
        AppUtils.getInstance().dimDialogBackground(logOutDialog);
        Button btnOk = logOutDialog.findViewById(R.id.btn_logout);
        btnOk.setText(R.string.s_ok);
        Button btnCancel = logOutDialog.findViewById(R.id.btn_cancel);
        btnCancel.setVisibility(View.GONE);

        logOutDialog.show();

        btnOk.setOnClickListener(v -> logOutDialog.dismiss());

        btnCancel.setOnClickListener(v -> logOutDialog.dismiss());
    }

    public interface ISuccess{

        void onOkViewClicked();
    }

    /**
     * method to get QD app version name
     *
     * @return versionName, if no exception and empty otherwise
     */
    public String getAppVersionName() {
        PackageManager pkgManager = QuickDelivery.getInstance().getPackageManager();
        String appPackageName = QuickDelivery.getInstance().getPackageName();

        try {
            if (BuildConfig.DEBUG)
                return pkgManager.getPackageInfo(appPackageName, 0).versionName + "-DEBUG";
            else
                return pkgManager.getPackageInfo(appPackageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
