package com.qd.user.utils;


import android.util.Log;

public class LogUtil {

    private static boolean ENABLE_INFO = false;
    private static boolean ENABLE_DEBUG = false;
    private static boolean ENABLE_ERROR = false;

//    private static final String PREPEND_TAG = "qd.";

    public static void isDebuggable(boolean flag) {
        ENABLE_INFO = flag;
        ENABLE_DEBUG = flag;
        ENABLE_ERROR = flag;
    }

    /*public static void info(String className, String methodName, String message) {
        if (ENABLE_INFO || (email.contains("quickdeliveryco.com"))) {
            Log.i(PREPEND_TAG + className, methodName + " -- " + message);
        }
    }*/

    public static void debug(String className, String methodName, String message) {
        if (ENABLE_DEBUG) {
            Log.d(className, methodName + " -- " + message);
        }
    }

    public static void error(String className, String methodName, String message) {
        if (ENABLE_ERROR) {
            Log.e(className, methodName + " -- " + message);
        }
    }

    public static void info(String className, String message) {
        if (ENABLE_INFO) {
            Log.i(className, " -- " + message);
        }
    }

    public static void debug(String className, String message) {
        if (ENABLE_DEBUG) {
            Log.d(className, " -- " + message);
        }
    }

    public static void error(String className, String message) {
        if (ENABLE_ERROR) {
            Log.e(className, " -- " + message);
        }
    }
}
