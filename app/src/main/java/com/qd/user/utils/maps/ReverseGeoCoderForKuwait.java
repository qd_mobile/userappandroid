package com.qd.user.utils.maps;

import com.google.android.gms.maps.model.LatLng;


public class ReverseGeoCoderForKuwait {
    private static final String API_BASE = "https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/PACIAddressSearch/MapServer/identify?";
    private static final String GEOMETRY_TYPE = "geometryType=esriGeometryPoint";
    private static final String SR = "sr=4326";
    private static final String LAYERS = "layers=1";
    private static final String TOLERANCE = "tolerance=10";
    private static final String MAP_EXTENT = "mapExtent=XMin: 5069581.550573585 YMin: 3310706.102544307 XMax: 5534770.219630809 YMax: 3526899.554510004 Spatial Reference: 102100 (3857)";
    private static final String IMAGE_DISPLAY = "imageDisplay=600,550,96";
    private static final String RETURN_GEOMETRY = "returnGeometry=false";
    private static final String F = "f=pjson";

    public String reverseGeoCodingUrl(String token, LatLng latLng) {
        String request;
        request = API_BASE + "geometry={y:" + latLng.latitude + ",x:" + latLng.longitude + ",Spatial Reference:4326}" + "&" +
                GEOMETRY_TYPE + "&" +
                SR + "&" +
                LAYERS + "&" +
                "token=" + token + "&" +
                TOLERANCE + "&" +
                MAP_EXTENT + "&" +
                IMAGE_DISPLAY + "&" +
                RETURN_GEOMETRY + "&" +
                F;

        return request;
    }

}
