package com.qd.user.utils;


import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.File;

public class Utils {

    public static void printJson(Object o) {
        printJson("", o);
    }

    public static void printJson(String msg, Object o) {
        try {
            String jsonStr = ((new Gson()).toJson(o));
            LogUtil.info("printJson", msg + "---- > " + jsonStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printJson(String className, String msg, Object o) {
        try {
            String jsonStr = ((new Gson()).toJson(o));
            LogUtil.debug(className, "Json---- > " + msg, jsonStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printJson(String className, String method, String msg, Object o) {
        try {
            String jsonStr = ((new Gson()).toJson(o));
            LogUtil.debug(className, method + " --Json---- > " + msg, jsonStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isNullOrEmpty(String emptyCheck) {
        return emptyCheck == null || emptyCheck.trim().isEmpty();
    }

    public static boolean deleteFileOrDir(@NonNull String path) {
        boolean result = true;
        LogUtil.info(Utils.class.getName(), path);
        File filePath = new File(path);

        if (filePath.exists()) {
            if (filePath.isDirectory()) {
                for (File child : filePath.listFiles()) {
                    result &= deleteFileOrDir(child);
                }
                result &= filePath.delete(); // Delete empty directory.
            } else if (filePath.isFile()) {
                result &= filePath.delete();
            }
            return result;
        } else {
            return false;
        }
    }

    public static boolean deleteFileOrDir(@NonNull File path) {
        return deleteFileOrDir(path.toString());
    }
}
