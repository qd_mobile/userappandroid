package com.qd.user.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateFormatter {

    private Date time;

    public static DateFormatter getInstance() {
        return new DateFormatter();
    }

    public String getFormattedDay(String date) {
        String day = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            spf.setTimeZone(TimeZone.getTimeZone("UTC"));
            day = new SimpleDateFormat("EEEE", Locale.getDefault()).format(spf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return day;
    }

    public String getFormattedDate(String date) {
        String day = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            spf.setTimeZone(TimeZone.getTimeZone("UTC"));
            day = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(spf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return day;
    }

    public String getFormattedDate(String date, String pattern) {
        String day = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            spf.setTimeZone(TimeZone.getTimeZone("UTC"));
            day = new SimpleDateFormat(pattern, Locale.getDefault()).format(spf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return day;
    }


    public String getFormattedTime(String date) {
        String time = null;
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
            spf.setTimeZone(TimeZone.getTimeZone("UTC"));
            time = new SimpleDateFormat("h:mm a", Locale.getDefault()).format(spf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }

    public String getFormattedDateInUtc(Date date) {
        String dateTime;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateTime = simpleDateFormat.format(date);
        return dateTime;
    }

    public Date getParsedDate(String date) {
        Date dateTime = null;

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            dateTime = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateTime;
    }

    public Date getDateInUtc(Date date) {
        String dateTime = null;
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateTime = spf.format(date);
        try {
            time = spf.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }
}
