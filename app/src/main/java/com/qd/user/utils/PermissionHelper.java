package com.qd.user.utils;


import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.qd.user.constants.AppConstants;

public class PermissionHelper {

    /**
     * `
     * A {@link Activity} object to get the context of the Activity from where it is called
     */
    private Activity mActivity;

    /**
     * A {@link IGetPermissionListener} object to sends call back to the Activity which implements it
     */
    private IGetPermissionListener mGetPermissionListener;

    public PermissionHelper(IGetPermissionListener getPermissionListener) {
        mGetPermissionListener = getPermissionListener;
    }

    /**
     * method to check permissions grantResult
     *
     * @return true if granted else false
     */
    public static boolean isPermissionGranted(int[] grantResults) {
        for (int i : grantResults) {
            if (i != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to check any permission. It will return true if permission granted
     * otherwise false
     *
     * @param requestCode is the code given for which permission is to be taken
     * @param context     of the activity
     * @param permissions contains the array of permissions to be taken
     * @return true if all the permissions granted otherwise false
     */
    public boolean hasPermission(Activity context, String[] permissions, int requestCode) {
        mActivity = context;
        boolean isAllGranted = true;

        //check for all devices
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                isAllGranted = false;

            }
        }

        if (!isAllGranted) {
            ActivityCompat.requestPermissions(context, permissions, requestCode);
            return false;
        } else
            return true;
    }

    /**
     * This method is used to set the result when {@link ActivityCompat.OnRequestPermissionsResultCallback}
     * gets the callback of the permission
     *
     * @param requestCode  code at which particular permission was asked
     * @param permissions  name of the permission taken from the user
     * @param grantResults gives the result whether user grants or denies the permission
     */
    public void setPermissionResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case AppConstants.PermissionConstants.REQUEST_IMAGE_PERMISSIONS:
                //If user denies the CAMERA permission with don't ask again checkbox,then this variable will be true
                boolean isRationalCamera = !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) &&
                        ContextCompat.checkSelfPermission(mActivity, permissions[0]) != PackageManager.PERMISSION_GRANTED;
                //If user denies the WRITE permission with don't ask again checkbox,then this variable will be true
                boolean isRationalStorage = !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[1]) &&
                        ContextCompat.checkSelfPermission(mActivity, permissions[1]) != PackageManager.PERMISSION_GRANTED;

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mGetPermissionListener.permissionsGiven(requestCode);

                } else if (isRationalCamera || isRationalStorage) {
                    AppUtils.showAllowPermissionFromSettingDialog(mActivity, "Please enable Quick Delivery to take photos and videos with this app", "Use Camera");
                }
                break;
            case AppConstants.PermissionConstants.REQUEST_STORAGE:
                //If user denies the WRITE permission with don't ask again checkbox,then this variable will be true
                boolean isStoragePermissionGiven = !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) &&
                        ContextCompat.checkSelfPermission(mActivity, permissions[0]) != PackageManager.PERMISSION_GRANTED;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mGetPermissionListener.permissionsGiven(requestCode);
                } else if (isStoragePermissionGiven) {
                    AppUtils.showAllowPermissionFromSettingDialog(mActivity, "Please enable storage permission to download your statement", "Storage Permission");
                }
                break;
            case AppConstants.PermissionConstants.REQUEST_LOCATION:
                boolean isFineLocationPermission = !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) &&
                        ContextCompat.checkSelfPermission(mActivity, permissions[0]) != PackageManager.PERMISSION_GRANTED;

                boolean isCoarseLocationPermission = !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[1]) &&
                        ContextCompat.checkSelfPermission(mActivity, permissions[1]) != PackageManager.PERMISSION_GRANTED;

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mGetPermissionListener.permissionsGiven(requestCode);
                } else if (isCoarseLocationPermission || isFineLocationPermission) {
                    AppUtils.showAllowPermissionFromSettingDialog(mActivity, "Please enable location permission to track your order", "Location Permission");
                } else {
                    AppUtils.showAllowPermissionFromSettingDialog(mActivity, "Please enable location permission to track your order", "Location Permission");
                }
                break;

            case AppConstants.PermissionConstants.REQUEST_READ_CONTACTS:
                boolean isReadContactsPermissionGiven = !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[0]) &&
                        ContextCompat.checkSelfPermission(mActivity, permissions[0]) != PackageManager.PERMISSION_GRANTED;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mGetPermissionListener.permissionsGiven(requestCode);
                } else if (isReadContactsPermissionGiven) {
                    AppUtils.showAllowPermissionFromSettingDialog(mActivity, "Please enable contacts permission to access your contacts", "Storage Permission");
                }
                break;
        }

    }


    /**
     * This interface is used to get the user permission callback to the activity or fragment who
     * implements it
     */
    public interface IGetPermissionListener {

        void permissionsGiven(int requestCode);

      /*  void CameraPermissionDenied();

        void StoragePermissionDenied();*/
    }
}
