package com.qd.user.utils.maps;

import com.google.android.gms.maps.model.LatLng;

public interface LatLngInterpolator {
    LatLng interpolate(float fraction, LatLng a, LatLng b);

    class LinearFixed implements LatLngInterpolator {
        @Override
        public LatLng interpolate(float fraction, LatLng a, LatLng b) {
            double lat = fraction * b.latitude + (1 - fraction) * a.latitude;
            double lng = fraction * b.longitude + (1 - fraction) * a.longitude;
            return new LatLng(lat, lng);
        }
    }
}

