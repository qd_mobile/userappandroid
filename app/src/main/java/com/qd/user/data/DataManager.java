package com.qd.user.data;

import android.content.Context;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qd.user.QuickDelivery;
import com.qd.user.data.api.ApiManager;
import com.qd.user.data.preference.AppSharedPreference;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.commomresponse.Result;
import com.qd.user.model.contactus.ContactUsResponse;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.additem.itemstype.ItemsTypeListResponse;
import com.qd.user.model.createorder.advancedsearch.SaveAddressResponse;
import com.qd.user.model.createorder.balance.PendingBalanceResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.createorder.flexible.FlexibleOrdersResponse;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.createorder.maps.autocomplete.PlacesAutoCompleteResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.createorder.promo.PromoCodeResponse;
import com.qd.user.model.createorder.response.CreateOrderResponse;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.createorder.servicetype.FlexibleSlotsResponse;
import com.qd.user.model.createorder.vehicle.VehicleCategoryResponse;
import com.qd.user.model.delete.DeleteSavedPlaceResponse;
import com.qd.user.model.editprofile.EditProfileResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;
import com.qd.user.model.kuwaitfinder.blocks.BlocksResponse;
import com.qd.user.model.kuwaitfinder.geocode.GeoCodeLocationResponse;
import com.qd.user.model.kuwaitfinder.geocoder.KuwaitReverseGeoCodingResposne;
import com.qd.user.model.kuwaitfinder.governorate.GovernorateResponse;
import com.qd.user.model.kuwaitfinder.neighbourhood.NeighbourhoodResponse;
import com.qd.user.model.kuwaitfinder.streets.StreetsResponse;
import com.qd.user.model.kuwaitfinder.token.TokenResponse;
import com.qd.user.model.myorders.OrdersResponse;
import com.qd.user.model.notifications.NotificationsListingResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.otpverification.OtpVerificationResponse;
import com.qd.user.model.payments.invoice.InvoiceListingResponse;
import com.qd.user.model.payments.invoicedetails.InvoiceDetailsResponse;
import com.qd.user.model.payments.personal.PaymentHistoryResponse;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;
import com.qd.user.model.payments.wallet.WalletTransactionHistoryResponse;
import com.qd.user.model.profile.ProfileResponse;
import com.qd.user.model.recievedorders.RecievedOrdersResponse;
import com.qd.user.model.socialresponse.socialsigninresponse.SocialLoginResponse;
import com.qd.user.model.vendor.uniquevendor.UniqueEmailResponse;
import com.qd.user.model.vendor.vendorsignup.VendorSignUpResponse;
import com.qd.user.model.vendor.vendorstatus.VendorStatusResponse;
import com.qd.user.ui.onboard.welcome.WelcomeFragment;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class DataManager {

    private static DataManager instance;
    private ApiManager apiManager;
    private AppSharedPreference mPrefManager;


    private DataManager(Context context) {
        //Initializing SharedPreference object
        mPrefManager = AppSharedPreference.getInstance(context);

        /*
        *initialize {@link ApiManager} class
        */
        apiManager = ApiManager.getInstance();
    }

    /**
     * Returns the single instance of {@link DataManager} if
     * {@link #init(Context)} is called first
     *
     * @return instance
     */
    public static DataManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Call init() before getInstance()");
        }
        return instance;
    }

    /**
     * Method used to create an instance of {@link DataManager}
     *
     * @param context of the application passed from the {@link QuickDelivery}
     * @return instance if it is null
     */
    public synchronized static DataManager init(Context context) {
        if (instance == null) {
            instance = new DataManager(context);
        }
        return instance;
    }


    public Call<CreateAccountResponse> hitLoginApi(HashMap<String, String> requestPayloadForLogin) {
        return apiManager.hitLoginApi(requestPayloadForLogin);
    }

    public Call<CreateAccountResponse> hitSignUpApi(JSONObject jsonObject) {
        return apiManager.hitSignUpApi(jsonObject);
    }

    public Call<CommonResponse> hitForgotPasswordApi(HashMap<String, String> responsePayload) {
         return apiManager.hitForgotPasswordApi(responsePayload);
    }

    public void doFbLogin(WelcomeFragment welcomeFragment) {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(welcomeFragment, Arrays.asList("public_profile", "email"/*, "user_location", "user_birthday"*/));
    }

    public Call<SocialLoginResponse> hitSocialLoginApi(HashMap<String, String> fbSignInObject) {
        return apiManager.hitSocialLoginApi(fbSignInObject);
    }

    public Call<TokenResponse> getTokenForKuwaitFinderApi(HashMap<String, String> requestPayload) {
        return apiManager.getTokenForKuwaitFinderApi(requestPayload);
    }

    public Call<GovernorateResponse> getGovernorate(String token) {
        return apiManager.getGovernorate(token);
    }

    public Call<NeighbourhoodResponse> getNeighbourhood(String url) {
        return apiManager.getNeighbourhood(url);
    }

    public Call<BlocksResponse> getBlocks(String blocksUrl) {
        return apiManager.getBlocks(blocksUrl);
    }

    public Call<StreetsResponse> getStreet(String streetUrl) {
        return apiManager.getStreet(streetUrl);
    }

    public Call<GeoCodeLocationResponse> getGeoCode(String geoCodeUrl) {
        return apiManager.getGeoCode(geoCodeUrl);
    }

    public Call<ItemsTypeListResponse> getItemTypes() {
        return apiManager.getItemTypes();
    }

    public void setUserName(String name) {
         mPrefManager.putString(AppSharedPreference.USER_NAME,name);

    }

    public void setCountryCode(String ccPersonal) {
        mPrefManager.putString(AppSharedPreference.COUNTRY_CODE,ccPersonal);

    }

    public void setPhoneNumber(String personal) {
        mPrefManager.putString(AppSharedPreference.PHONE_NUMBER,personal);

    }

    public void setEmailAddress(String email) {
        mPrefManager.putString(AppSharedPreference.EMAIL,email);

    }

    public String getUserName() {
        return mPrefManager.getString(AppSharedPreference.USER_NAME);

    }

    public String getPhoneNumber() {
        return mPrefManager.getString(AppSharedPreference.PHONE_NUMBER);

    }

    public String getEmail() {
        return mPrefManager.getString(AppSharedPreference.EMAIL);

    }

    public void setUserLoggedIn(boolean isUserLoggedIn) {
        mPrefManager.putBoolean(AppSharedPreference.IS_USER_LOGGED_IN,isUserLoggedIn);

    }
    public boolean isUserLoggedIn() {
        return mPrefManager.getBoolean(AppSharedPreference.IS_USER_LOGGED_IN);
    }

    public String getCountryCode() {
        return mPrefManager.getString(AppSharedPreference.COUNTRY_CODE);
    }

    public void setUserId(String userId) {
        mPrefManager.putString(AppSharedPreference.USER_ID,userId);

    }

    public String getUserId() {
        return mPrefManager.getString(AppSharedPreference.USER_ID);
    }

    public Call<EditProfileResponse> hitEditProfileApi(HashMap<String, String> editProfile) {
       return apiManager.hitEditProfileApi(editProfile);
    }

    public Call<CommonResponse> hitChangePasswordApi(HashMap<String, String> changePasswordObject) {
        return apiManager.hitChangePasswordApi(changePasswordObject);
    }

    public void setAccessToken(String authenticationToken) {
        mPrefManager.putString(AppSharedPreference.ACCESS_TOKEN,authenticationToken);
    }
    public String getAccessToken() {
        return mPrefManager.getString(AppSharedPreference.ACCESS_TOKEN);
    }

    public Call<CommonResponse> hitResetPasswordApi(HashMap<String, String> resetPasswordObject) {
        return apiManager.hitResetPasswordApi(resetPasswordObject);
    }

    public Call<SaveAddressResponse> saveAdressInFav(JSONObject saveAddressObject) {
        return apiManager.saveAddressInFav(saveAddressObject);
    }

    public Call<SavedAddressResponse> getSavedAddresses(String userId, String searchString) {
        return apiManager.getSavedAddresses(userId, searchString);
    }
    public Call<DeleteSavedPlaceResponse> hitDeleteSavedPlacesApi(String savedPlaceId) {
        return apiManager.hitDeleteSavedPlaceApi(savedPlaceId);
    }

    public Call<SaveAddressResponse> hitEditSaveAddressApi(JSONObject saveAddressObject) {
        return apiManager.hitEditSaveAddressApi(saveAddressObject);
    }

    public Call<OtpVerificationResponse> hitValidateOtpApi(HashMap<String, String> requestPayloadForVerification) {
        return apiManager.hitValidateOtpApi(requestPayloadForVerification);
    }

    public void clearPreference() {
        mPrefManager.clearAllPrefs();
    }


    public Call<VendorSignUpResponse> hitVendorSignUpApi(JSONObject becomeAVendorObject) {
        return apiManager.hitVendorSignUpApi(becomeAVendorObject);
    }

    public String getUserType() {
        return mPrefManager.getString(AppSharedPreference.USER_TYPE);
    }

    public String getCurrentStatus() {
        return mPrefManager.getString(AppSharedPreference.CURRENT_STATUS);
    }

    public void setUserType(String userType) {
        mPrefManager.putString(AppSharedPreference.USER_TYPE, userType);

    }

    public void setCurrentStatus(String currentStatus) {
        mPrefManager.putString(AppSharedPreference.CURRENT_STATUS, currentStatus);
    }

    public Call<VendorStatusResponse> hitVendorStatusApi(String id, String deviceId, String platform) {
        return apiManager.hitVendorStatusApi(id, deviceId, platform);
    }

    public void setVendorStatus(String status) {
        mPrefManager.putString(AppSharedPreference.VENDOR_STATUS, status);
    }

    public String getCashClientCurrentStatus() {
        return mPrefManager.getString(AppSharedPreference.CASH_CLIENT_CURRENT_STATUS);
    }

    public void setCashClientCurrentStatus(String cashClientCurrentStatus) {
        mPrefManager.putString(AppSharedPreference.CASH_CLIENT_CURRENT_STATUS, cashClientCurrentStatus);

    }

    public String getVendorStatus() {
        return mPrefManager.getString(AppSharedPreference.VENDOR_STATUS);
    }

    public Call<UniqueEmailResponse> hitIsVendorUniqueApi(HashMap<String, String> object) {
        return apiManager.hitIsVendorUniqueApi(object);
    }

    public Call<PlacesAutoCompleteResponse> getPlacesAutoCompleteResponse(String url) {
        return apiManager.getPlacesAutoCompleteResponse(url);
    }

    public Call<VehicleCategoryResponse> getVehicleCategories() {
        return apiManager.getVehicleCategories();

    }

    public Call<DirectionsApiResponse> getDirectionsApiResponse(String url) {
        return apiManager.getDirectionsApiResponse(url);
    }

    public Call<CreateOrderResponse> createOrder(JSONObject jsonObject) {
        return apiManager.createOrder(jsonObject);
    }

    public Call<CreateAccountResponse> hitSocialSignUpApi(HashMap<String, String> socialSignUpObject) {
        return apiManager.hitSocialSignUpApi(socialSignUpObject);
    }

    public Call<OrdersResponse> hitOrderHistoryApi(HashMap<String, Object> orderHistoryObject) {
        return apiManager.hitOrderHistoryApi(orderHistoryObject);
    }

    public void setRegistrationDateAndTime(String createdAt) {
        mPrefManager.putString(AppSharedPreference.CREATED_AT, createdAt);
    }

    public String getRegistrationDateAndTime() {
        return mPrefManager.getString(AppSharedPreference.CREATED_AT);
    }

    public Call<OrderDetailResponse> hitOrderDetailApi(String orderId) {
        return apiManager.hitOrderDetailApi(orderId);
    }

    public Call<FlexibleSlotsResponse> getTimeSlots() {
        return apiManager.getFlexibleSlotsApi();
    }

    public String getServiceType() {
        return mPrefManager.getString(AppSharedPreference.BUSINESS_TYPE);
    }

    public void setSignUpType(String signupType) {
        mPrefManager.putString(AppSharedPreference.SIGN_UP_TYPE, signupType);
    }

    public String getUserSignupType() {
        return mPrefManager.getString(AppSharedPreference.SIGN_UP_TYPE);
    }

    public void setServiceType(String businessType) {
        mPrefManager.putString(AppSharedPreference.BUSINESS_TYPE, businessType);
    }

    public void setUserVerified(boolean isUserVerified) {
        mPrefManager.putBoolean(AppSharedPreference.IS_USER_VERIFIED, isUserVerified);
    }

    public boolean isUserVerified() {
        return mPrefManager.getBoolean(AppSharedPreference.IS_USER_VERIFIED);
    }


    public Call<RecievedOrdersResponse> hitRecievedOrderApi(HashMap<String, Object> orderHistoryObject) {
        return apiManager.hitReceivedOrderApi(orderHistoryObject);
    }

    public String getBaseUrl() {
        return apiManager.getBaseUrl();
    }

    public void setBusinessAddress(String fullBusinessAddress) {
        mPrefManager.putString(AppSharedPreference.BUSINESS_ADDRESS,fullBusinessAddress);
    }

    public String getBusinessAddress() {
        return mPrefManager.getString(AppSharedPreference.BUSINESS_ADDRESS);
    }

    public Call<ContactUsResponse> hitContactUsApi(HashMap<String, Object> requestPayloadForContactus) {
        return apiManager.hitContactUsApi(requestPayloadForContactus);
    }

    public Call<OrderDetailResponse> getCurrentOrderStatus(String orderUniqueId) {
        return apiManager.getCurrentOrderStatus(orderUniqueId);
    }

    public Call<OrderCancellationResponse> cancelOrder(JSONObject request) {
        return apiManager.cancelOrder(request);
    }

    public Call<DriverAvailabilityResponse> checkDriverAvailability(JSONObject requestPayload) {
        return apiManager.checkDriverAvailability(requestPayload);
    }

    public String getBusinessName() {
        return mPrefManager.getString(AppSharedPreference.BUSINESS_NAME);
    }

    public void setBusinessName(String businessName) {
        mPrefManager.putString(AppSharedPreference.BUSINESS_NAME, businessName);
    }

    public String getCompanyProfile() {
        return mPrefManager.getString(AppSharedPreference.COMPANY_PROFILE);
    }

    public void setCompanyProfile(String companyProfile) {
        mPrefManager.putString(AppSharedPreference.COMPANY_PROFILE, companyProfile);
    }

    public String getCompanyContact() {
        return mPrefManager.getString(AppSharedPreference.BUSINESS_CONTACT);
    }

    public void setCompanyContact(String contact) {
        mPrefManager.putString(AppSharedPreference.BUSINESS_CONTACT, contact);
    }

    public String getBusinessFullAddress() {
        return mPrefManager.getString(AppSharedPreference.BUSINESS_FULL_ADDRESS);
    }

    public void setBusinessFullAddress(String fullAddress) {
        mPrefManager.putString(AppSharedPreference.BUSINESS_FULL_ADDRESS, fullAddress);
    }

    public String getVendorPaymentMode() {
        return mPrefManager.getString(AppSharedPreference.VENDOR_PAYMENT_MODE);
    }

    public void setVendorPaymentMode(String paymentMode) {
        mPrefManager.putString(AppSharedPreference.VENDOR_PAYMENT_MODE, paymentMode);
    }

    public Call<InvoiceListingResponse> getInvoiceListing(HashMap<String, Object> payloadForInvoice) {
        return apiManager.getInvoiceListing(payloadForInvoice);
    }

    public Call<InvoiceDetailsResponse> getInvoiceDetails(HashMap<String, Object> requestPayload) {
        return apiManager.getInvoiceDetails(requestPayload);
    }

    public Call<WalletTransactionHistoryResponse> getWalletTransactionHistory(HashMap<String, Object> requestPayload) {
        return apiManager.getWalletTransactionHistory(requestPayload);
    }

    public Call<PaymentHistoryResponse> getPersonalPaymentHistory(HashMap<String, Object> payload) {
        return apiManager.getPersonalPaymentHistory(payload);
    }

    public Call<CommonResponse> validateWalletAmount(String userId, BigDecimal totalDeliveryCharges) {
        return apiManager.validateWalletAmount(userId, totalDeliveryCharges);
    }

    public Call<WalletRechargeResponse> initializePayment(JSONObject requestPayloadForRecharge) {
        return apiManager.initializePayment(requestPayloadForRecharge);
    }

    public Call<VendorInfoResponse> getVendorInfo(String userId) {
        return apiManager.getVendorInfo(userId);
    }

    public Call<OptimizedDeliveryResponse> getDeliveryCharges(JSONObject jsonObject) {
        return apiManager.getDeliveryCharges(jsonObject);
    }

    public Call<CommonResponse> retryForDriver(String id) {
        return apiManager.retryForDriver(id);
    }

    public Call<PromoCodeResponse> applyPromoCode(HashMap<String, Object> requestPayloadForPromo) {
        return apiManager.applyPromoCode(requestPayloadForPromo);
    }

    public String getDeviceToken() {
        return mPrefManager.getString(AppSharedPreference.DEVICE_TOKEN);
    }

    public void setDeviceToken(String token) {
        mPrefManager.putString(AppSharedPreference.DEVICE_TOKEN, token);
    }

    public Call<FlexibleOrdersResponse> createFlexibleOrder(JSONObject jsonObject) {
        return apiManager.createFlexibleOrder(jsonObject);
    }

    public Call<VehicleEtaResponse> getAvailableVehicles(String orderType, double latitude, double longitude, String userId) {
        return apiManager.getAvailableVehicles(orderType, userId, latitude, longitude);
    }

    public Call<OptimizedDeliveryResponse> getEta(JSONObject jsonObject) {
        return apiManager.getEta(jsonObject);
    }

    public Call<PendingBalanceResponse> getPendingBalance(String userId) {
        return apiManager.getPendingBalanceResponse(userId);
    }

    public Call<CommonResponse> logOutUser(String userType, String userId, String deviceId) {
        return apiManager.logOutUser(userType, userId, deviceId);
    }

    public Call<OrderDetailResponse> hitReceivedOrderDetailApi(String orderId, String phoneNumber) {
        return apiManager.hitReceivedOrderDetailApi(orderId, phoneNumber);
    }

    public void setKuwaitFinderAccessToken(String token) {
        mPrefManager.putString(AppSharedPreference.KUWAIT_FINDER_TOKEN, token);
    }

    public String getKuwaitFinderToken() {
        return mPrefManager.getString(AppSharedPreference.KUWAIT_FINDER_TOKEN);
    }

    public Call<KuwaitReverseGeoCodingResposne> getAddressThroughReverseGeoCoding(String url) {
        return apiManager.getAddressThroughReverseGeoCoding(url);
    }

    public Call<NotificationsListingResponse> getNotificationsListing(HashMap<String, Object> requestPayload) {
        return apiManager.getNotificationsListing(requestPayload);
    }

    public void setWaitingTimeForCancellation(int driverWaitTime) {
        mPrefManager.putInt(AppSharedPreference.WAITING_TIME_FOR_CANCELLATION, driverWaitTime);
    }

    public int getWaitingTimeForRetry() {
        return mPrefManager.getInt(AppSharedPreference.WAITING_TIME_FOR_RETRY);
    }

    public void setWaitingTimeForRetry(int findingDriverWaitTime) {
        mPrefManager.putInt(AppSharedPreference.WAITING_TIME_FOR_RETRY, findingDriverWaitTime);
    }

    public Call<OrderCancellationResponse> approveOrderReturn(String elementId, String vendor) {
        return apiManager.approveOrderReturn(elementId, vendor);
    }

    public Call<ProfileResponse> getProfileInfo() {
        return apiManager.getProfileInfo();
    }

    public Call<CommonResponse> hitVersionApi(HashMap<String, String> versionCode) {
        return apiManager.hitVersionApi(versionCode);
    }

    public Call<CommonResponse> markNotificationAsRead(JSONObject request) {
        return apiManager.markNotificationAsRead(request);
    }

    public Call<CommonResponse> checkForOrderStatus(String id) {
        return apiManager.checkForDriverAvailability(id);
    }

    public Call<OrderCancellationResponse> autoCancelOrder(String id) {
        return apiManager.autoCancelOrder(id);
    }

    public boolean getNotificationStatus() {
        return mPrefManager.getBoolean(AppSharedPreference.NOTIFICATION_STATUS);
    }

    public void setNotificationStatus(boolean enableNotification) {
        mPrefManager.putBoolean(AppSharedPreference.NOTIFICATION_STATUS, enableNotification);
    }

    public Call<CommonResponse> enableOrDisableNotifications(String userId, boolean isChecked) {
        return apiManager.enableOrDisableNotifications(userId, isChecked);
    }

    public String getVersionName() {
        return mPrefManager.getString(AppSharedPreference.VERSION_NAME);
    }

    public void setVersionName(String versionName) {
        mPrefManager.putString(AppSharedPreference.VERSION_NAME, versionName);
    }

    public Call<CommonResponse> resendOtp(String type, String countryCode, String phoneNumber) {
        return apiManager.resendOtp(type, countryCode, phoneNumber);
    }

    public Call<CommonListResponse> getCancellationReasons() {
        return apiManager.getCancellationReasons();
    }

    public void setCancellationReasons(String reasons) {
        mPrefManager.putString(AppSharedPreference.CANCELLATION_REASONS, reasons);
    }

    public List<Result> getCancelReasons() {
        Type type = new TypeToken<List<Result>>() {
        }.getType();
        return new Gson().fromJson(mPrefManager.getString(AppSharedPreference.CANCELLATION_REASONS), type);
    }
}
