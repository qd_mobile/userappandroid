package com.qd.user.data.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSharedPreference {

    public static final String USER_ID = "user_id";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_NAME ="user_name" ;
    public static final String COUNTRY_CODE ="country_code" ;
    public static final String PHONE_NUMBER="phone_number";
    public static final String EMAIL ="email" ;
    public static final String IS_USER_LOGGED_IN = "isUserLogedIn";
    public static final String USER_TYPE = "userType";
    public static final String CASHCLIENT_STATUS = "cash_client_status";
    public static final String CURRENT_STATUS = "current_status";
    public static final String VENDOR_STATUS = "vendor_status";
    public static final String CASH_CLIENT_CURRENT_STATUS = "cash_client_current_status";
    public static final String CREATED_AT = "created_at";
    public static final String SIGN_UP_TYPE = "signup_type";
    public static final String BUSINESS_TYPE = "business_type";
    public static final String IS_USER_VERIFIED = "is_user_verified";
    public static final String BUSINESS_ADDRESS = "businessAddress";
    public static final String BUSINESS_NAME = "businessName";
    public static final String COMPANY_PROFILE = "companyProfile";
    public static final String BUSINESS_CONTACT = "businessContact";
    public static final String BUSINESS_FULL_ADDRESS = "businessFullAddress";
    public static final String VENDOR_PAYMENT_MODE = "vendorPaymentMode";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String KUWAIT_FINDER_TOKEN = "token";
    public static final String WAITING_TIME_FOR_RETRY = "Waiting time for retry";
    public static final String WAITING_TIME_FOR_CANCELLATION = "Waiting time for cancellation";
    public static final String NOTIFICATION_STATUS = "Notification Status";
    public static final String VERSION_NAME = "versionName";
    public static final String CANCELLATION_REASONS = "cancellationReasons";


    private static AppSharedPreference mAppSharedPreferenceInstance;
    private SharedPreferences sharedPref;
    private AppSharedPreference(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static
    AppSharedPreference getInstance(Context context) {
        if (mAppSharedPreferenceInstance== null) {
            synchronized(AppSharedPreference.class) {
                if (mAppSharedPreferenceInstance == null)
                    mAppSharedPreferenceInstance = new AppSharedPreference(context);
            }
        }
        return mAppSharedPreferenceInstance;
    }



    public int getInt(String key) {
       return sharedPref.getInt(key, 0);
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);         // Commit the edits!
        editor.apply();
    }


    public String getString(String key) {
        return sharedPref.getString(key, null);
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
    public void remove(String key) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return sharedPref.getBoolean(key, false);
    }

    public void clearAllPrefs() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }
}

