package com.qd.user.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.contactus.ContactUsResponse;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.additem.itemstype.ItemsTypeListResponse;
import com.qd.user.model.createorder.advancedsearch.SaveAddressResponse;
import com.qd.user.model.createorder.balance.PendingBalanceResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.createorder.flexible.FlexibleOrdersResponse;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.createorder.maps.autocomplete.PlacesAutoCompleteResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.createorder.promo.PromoCodeResponse;
import com.qd.user.model.createorder.response.CreateOrderResponse;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.createorder.servicetype.FlexibleSlotsResponse;
import com.qd.user.model.createorder.vehicle.VehicleCategoryResponse;
import com.qd.user.model.delete.DeleteSavedPlaceResponse;
import com.qd.user.model.editprofile.EditProfileResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;
import com.qd.user.model.kuwaitfinder.blocks.BlocksResponse;
import com.qd.user.model.kuwaitfinder.geocode.GeoCodeLocationResponse;
import com.qd.user.model.kuwaitfinder.geocoder.KuwaitReverseGeoCodingResposne;
import com.qd.user.model.kuwaitfinder.governorate.GovernorateResponse;
import com.qd.user.model.kuwaitfinder.neighbourhood.NeighbourhoodResponse;
import com.qd.user.model.kuwaitfinder.streets.StreetsResponse;
import com.qd.user.model.kuwaitfinder.token.TokenResponse;
import com.qd.user.model.myorders.OrdersResponse;
import com.qd.user.model.notifications.NotificationsListingResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.otpverification.OtpVerificationResponse;
import com.qd.user.model.payments.invoice.InvoiceListingResponse;
import com.qd.user.model.payments.invoicedetails.InvoiceDetailsResponse;
import com.qd.user.model.payments.personal.PaymentHistoryResponse;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;
import com.qd.user.model.payments.wallet.WalletTransactionHistoryResponse;
import com.qd.user.model.profile.ProfileResponse;
import com.qd.user.model.recievedorders.RecievedOrdersResponse;
import com.qd.user.model.socialresponse.socialsigninresponse.SocialLoginResponse;
import com.qd.user.model.vendor.uniquevendor.UniqueEmailResponse;
import com.qd.user.model.vendor.vendorsignup.VendorSignUpResponse;
import com.qd.user.model.vendor.vendorstatus.VendorStatusResponse;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiManager {

    private static final ApiManager instance = new ApiManager();
    private final ApiClient apiClient;
    private final KuwaitFinderApiClient kuwaitFinderApiClient;

    private ApiManager() {
        apiClient = getRetrofitService();
        kuwaitFinderApiClient = getRetrofitServiceForKuwaitFinderApi();
    }

    public static ApiManager getInstance() {
        return instance;
    }

    private static ApiClient getRetrofitService() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder requester = original.newBuilder()
                    .header(AppConstants.KEY_HEADER_CONTENT_TYPE, "application/json")
                    .method(original.method(), original.body());

            if (DataManager.getInstance().getAccessToken() != null) {
                requester.header("Authorization", "Bearer " + DataManager.getInstance().getAccessToken());
            }

            Request request = requester.build();
            return chain.proceed(request);
        });
        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(ApiClient.API_URL)
                .client(client)
                .build();

        return retrofit.create(ApiClient.class);

    }

    private KuwaitFinderApiClient getRetrofitServiceForKuwaitFinderApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder requester = original.newBuilder()
                    .header(AppConstants.KEY_HEADER_CONTENT_TYPE, "application/json")
                    .method(original.method(), original.body());

            Request request = requester.build();
            return chain.proceed(request);
        });
        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(KuwaitFinderApiClient.BASE_URL)
                .client(client)
                .build();

        return retrofit.create(KuwaitFinderApiClient.class);
    }

    public Call<CreateAccountResponse> hitLoginApi(HashMap<String, String> requestPayloadForLogin) {
        return apiClient.hitLoginApi(requestPayloadForLogin);
    }

    public Call<CreateAccountResponse> hitSignUpApi(JSONObject jsonObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        return apiClient.hitSignUpApi(body);
    }

    public Call<CommonResponse> hitForgotPasswordApi(HashMap<String, String> responsePayload) {
        return apiClient.hitForgotPasswordApi(responsePayload);
    }

    public Call<SocialLoginResponse> hitSocialLoginApi(HashMap<String, String> fbSignInObject) {
        return apiClient.hitSocialLoginApi(fbSignInObject);
    }

    public Call<EditProfileResponse> hitEditProfileApi(HashMap<String, String> editProfile) {
        return apiClient.hitEditProfileApi(editProfile);
    }

    public Call<CommonResponse> hitChangePasswordApi(HashMap<String, String> changePasswordObject) {
        return apiClient.hitChangePasswordApi(changePasswordObject);
    }

    public Call<CommonResponse> hitResetPasswordApi(HashMap<String, String> resetPasswordObject) {
        return apiClient.hitResetPasswordApi(resetPasswordObject);
    }

    public Call<TokenResponse> getTokenForKuwaitFinderApi(HashMap<String, String> requestPayload) {
        return kuwaitFinderApiClient.getTokenForKuwaitFinderApi(requestPayload);
    }

    public Call<GovernorateResponse> getGovernorate(String token) {
        return kuwaitFinderApiClient.getGovernorate(token);
    }

    public Call<NeighbourhoodResponse> getNeighbourhood(String url) {
        return kuwaitFinderApiClient.getNeighbourhood(url);
    }

    public Call<BlocksResponse> getBlocks(String blocksUrl) {
        return kuwaitFinderApiClient.getBlocks(blocksUrl);
    }

    public Call<StreetsResponse> getStreet(String streetUrl) {
        return kuwaitFinderApiClient.getStreet(streetUrl);
    }

    public Call<GeoCodeLocationResponse> getGeoCode(String geoCodeUrl) {
        return kuwaitFinderApiClient.getGeoCode(geoCodeUrl);
    }

    public Call<ItemsTypeListResponse> getItemTypes() {
        return apiClient.getItemTypes();
    }

    public Call<SaveAddressResponse> saveAddressInFav(JSONObject saveAddressObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), saveAddressObject.toString());
        return apiClient.saveAddressInFav(body);
    }

    public Call<SavedAddressResponse> getSavedAddresses(String userId, String searchString) {
        return apiClient.getSavedAddresses(userId, searchString);
    }
    public Call<DeleteSavedPlaceResponse> hitDeleteSavedPlaceApi(String savedPlaceId) {
        return apiClient.hitDeleteSavedPlaceApi(savedPlaceId);
    }

    public Call<SaveAddressResponse> hitEditSaveAddressApi(JSONObject saveAddressObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), saveAddressObject.toString());
        return apiClient.hitEditSaveAddressApi(body);
    }

    public Call<OtpVerificationResponse> hitValidateOtpApi(HashMap<String, String> requestPayloadForVerification) {
        return apiClient.hitValidateOtpApi(requestPayloadForVerification);
    }

    public Call<PlacesAutoCompleteResponse> getPlacesAutoCompleteResponse(String url) {
        return apiClient.getPlacesAutoCompleteResponse(url);
    }

    public Call<VehicleCategoryResponse> getVehicleCategories() {
        return apiClient.getVehicleCategories();
    }

    public Call<DirectionsApiResponse> getDirectionsApiResponse(String url) {
        return apiClient.getDirectionsApiResponse(url);
    }

    public Call<CreateOrderResponse> createOrder(JSONObject jsonObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        return apiClient.createOrder(body);
    }

    public Call<VendorSignUpResponse> hitVendorSignUpApi(JSONObject becomeAVendorObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), becomeAVendorObject.toString());
        return apiClient.hitVendorSignUpApi(body);
    }

    public Call<VendorStatusResponse> hitVendorStatusApi(String id, String deviceId, String platform) {
        return apiClient.hitVendorStatusApi(id, deviceId, platform);
    }

    public Call<UniqueEmailResponse> hitIsVendorUniqueApi(HashMap<String, String> object) {
        return apiClient.hitIsVendorUniqueApi(object);

    }

    public Call<CreateAccountResponse> hitSocialSignUpApi(HashMap<String, String> socialSignUpObject) {
        return apiClient.hitSocialSignUpApi(socialSignUpObject);
    }

    public Call<OrdersResponse> hitOrderHistoryApi(HashMap<String, Object> orderHistoryObject) {
        return apiClient.hitOrderHistoryApi(orderHistoryObject);
    }

    public Call<OrderDetailResponse> hitOrderDetailApi(String orderId) {
        return apiClient.hitOrderDetailApi(orderId);
    }

    public Call<FlexibleSlotsResponse> getFlexibleSlotsApi() {
        return apiClient.getFlexibleTimeSlotsApi();
    }

    public Call<RecievedOrdersResponse> hitReceivedOrderApi(HashMap<String, Object> orderHistoryObject) {
        return apiClient.hitReceivedOrderApi(orderHistoryObject);
    }

    public String getBaseUrl() {
        return apiClient.API_URL;
    }

    public Call<ContactUsResponse> hitContactUsApi(HashMap<String, Object> requestPayloadForContactus) {
        return apiClient.hitContactUsApi(requestPayloadForContactus);
    }

    public Call<OrderDetailResponse> getCurrentOrderStatus(String orderUniqueId) {
        return apiClient.getCurrentOrderStatus(orderUniqueId);
    }

    public Call<OrderCancellationResponse> cancelOrder(JSONObject request) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), request.toString());
        return apiClient.cancelOrder(body);
    }

    public Call<DriverAvailabilityResponse> checkDriverAvailability(JSONObject requestPayload) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), requestPayload.toString());
        return apiClient.checkDriverAvailability(body);
    }

    public Call<PromoCodeResponse> applyPromoCode(HashMap<String, Object> requestPayloadForPromo) {
        return apiClient.applyPromoCode(requestPayloadForPromo);
    }

    public Call<CommonResponse> retryForDriver(String id) {
        return apiClient.retryForDriver(id);
    }

    public Call<OptimizedDeliveryResponse> getDeliveryCharges(JSONObject object) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), object.toString());
        return apiClient.getDeliveryCharges(body);
    }

    public Call<InvoiceListingResponse> getInvoiceListing(HashMap<String, Object> payloadForInvoice) {
        return apiClient.getInvoiceListing(payloadForInvoice);
    }

    public Call<InvoiceDetailsResponse> getInvoiceDetails(HashMap<String, Object> requestPayload) {
        return apiClient.getInvoiceDetails(requestPayload);
    }

    public Call<WalletTransactionHistoryResponse> getWalletTransactionHistory(HashMap<String, Object> requestPayload) {
        return apiClient.getWalletTransactionHistory(requestPayload);
    }

    public Call<PaymentHistoryResponse> getPersonalPaymentHistory(HashMap<String, Object> payload) {
        return apiClient.getPersonalPaymentHistory(payload);
    }

    public Call<CommonResponse> validateWalletAmount(String userId, BigDecimal totalDeliveryCharges) {
        return apiClient.validateWalletAmount(userId, totalDeliveryCharges);
    }

    public Call<WalletRechargeResponse> initializePayment(JSONObject request) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), request.toString());
        return apiClient.initializePayment(body);
    }

    public Call<VendorInfoResponse> getVendorInfo(String userId) {
        return apiClient.getVendorInfo(userId);
    }

    public Call<FlexibleOrdersResponse> createFlexibleOrder(JSONObject jsonObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        return apiClient.createFlexibleOrder(body);
    }

    public Call<VehicleEtaResponse> getAvailableVehicles(String orderType, String userId, double latitude, double longitude) {
        if (latitude != 0.0d && longitude != 0.0d)
            return apiClient.getAvailableVehicles(orderType, userId, latitude, longitude);
        else return apiClient.getVehicleEta(orderType, userId);
    }

    public Call<OptimizedDeliveryResponse> getEta(JSONObject jsonObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        return apiClient.getEta(body);
    }

    public Call<PendingBalanceResponse> getPendingBalanceResponse(String userId) {
        return apiClient.getPendingBalance(userId);
    }

    public Call<CommonResponse> logOutUser(String userType, String userId, String deviceId) {
        return apiClient.logOutUser(userType, userId, deviceId);
    }

    public Call<OrderDetailResponse> hitReceivedOrderDetailApi(String orderId, String phoneNumber) {
        return apiClient.hitReceivedOrderDetailApi(orderId, phoneNumber);
    }

    public Call<KuwaitReverseGeoCodingResposne> getAddressThroughReverseGeoCoding(String url) {
        return kuwaitFinderApiClient.getAddressThroughReverseGeoCoding(url);
    }

    public Call<NotificationsListingResponse> getNotificationsListing(HashMap<String, Object> requestPayload) {
        return apiClient.getNotificationListing(requestPayload);
    }

    public Call<OrderCancellationResponse> approveOrderReturn(String elementId, String vendor) {
        return apiClient.approveOrderReturn(elementId, vendor);
    }

    public Call<ProfileResponse> getProfileInfo() {
        return apiClient.getProfileInfo();
    }

    public Call<CommonResponse> hitVersionApi(HashMap<String, String> versionCode) {
        return apiClient.hitVersionApi(versionCode);
    }

    public Call<CommonResponse> markNotificationAsRead(JSONObject request) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), request.toString());
        return apiClient.markNotificationAsRead(body);
    }

    public Call<CommonResponse> checkForDriverAvailability(String id) {
        return apiClient.checkForDriverAvailability(id);
    }

    public Call<OrderCancellationResponse> autoCancelOrder(String id) {
        return apiClient.autoCancelOrder(id);
    }

    public Call<CommonResponse> enableOrDisableNotifications(String userId, boolean isChecked) {
        return apiClient.enableOrDisableNotifications(userId, isChecked);
    }

    public Call<CommonResponse> resendOtp(String type, String countryCode, String phoneNumber) {
        return apiClient.resendOtp(type, countryCode, phoneNumber);
    }

    public Call<CommonListResponse> getCancellationReasons() {
        return apiClient.getCancellationReasons();
    }
}

