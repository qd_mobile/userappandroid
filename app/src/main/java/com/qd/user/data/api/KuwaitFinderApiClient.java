package com.qd.user.data.api;

import com.qd.user.model.kuwaitfinder.blocks.BlocksResponse;
import com.qd.user.model.kuwaitfinder.geocode.GeoCodeLocationResponse;
import com.qd.user.model.kuwaitfinder.geocoder.KuwaitReverseGeoCodingResposne;
import com.qd.user.model.kuwaitfinder.governorate.GovernorateResponse;
import com.qd.user.model.kuwaitfinder.neighbourhood.NeighbourhoodResponse;
import com.qd.user.model.kuwaitfinder.streets.StreetsResponse;
import com.qd.user.model.kuwaitfinder.token.TokenResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

interface KuwaitFinderApiClient {

    String BASE_URL = "https://kuwaitportal.paci.gov.kw/";

    @FormUrlEncoded
    @POST("arcgis/sharing/generateToken")
    Call<TokenResponse> getTokenForKuwaitFinderApi(@FieldMap HashMap<String, String> requestPayload);

    @GET("arcgisportal/rest/services/PACIAddressSearch/FeatureServer/3/query?where=1=1&outFields=*&returnGeometry=false&f=pjson")
    Call<GovernorateResponse> getGovernorate(@Query("token") String token);

    @GET
    Call<NeighbourhoodResponse> getNeighbourhood(@Url String url);

    @GET
    Call<BlocksResponse> getBlocks(@Url String blocksUrl);

    @GET
    Call<StreetsResponse> getStreet(@Url String streetUrl);

    @GET
    Call<GeoCodeLocationResponse> getGeoCode(@Url String geoCodeUrl);

    @GET
    Call<KuwaitReverseGeoCodingResposne> getAddressThroughReverseGeoCoding(@Url String url);
}
