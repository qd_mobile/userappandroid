package com.qd.user.data.api;

import com.qd.user.BuildConfig;
import com.qd.user.model.commomresponse.CommonListResponse;
import com.qd.user.model.commomresponse.CommonResponse;
import com.qd.user.model.contactus.ContactUsResponse;
import com.qd.user.model.createaccount.CreateAccountResponse;
import com.qd.user.model.createorder.DriverAvailabilityResponse;
import com.qd.user.model.createorder.additem.itemstype.ItemsTypeListResponse;
import com.qd.user.model.createorder.advancedsearch.SaveAddressResponse;
import com.qd.user.model.createorder.balance.PendingBalanceResponse;
import com.qd.user.model.createorder.distancetime.eta.VehicleEtaResponse;
import com.qd.user.model.createorder.flexible.FlexibleOrdersResponse;
import com.qd.user.model.createorder.location.DirectionsApiResponse;
import com.qd.user.model.createorder.maps.autocomplete.PlacesAutoCompleteResponse;
import com.qd.user.model.createorder.optimizedroute.OptimizedDeliveryResponse;
import com.qd.user.model.createorder.promo.PromoCodeResponse;
import com.qd.user.model.createorder.response.CreateOrderResponse;
import com.qd.user.model.createorder.savedaddress.SavedAddressResponse;
import com.qd.user.model.createorder.servicetype.FlexibleSlotsResponse;
import com.qd.user.model.createorder.vehicle.VehicleCategoryResponse;
import com.qd.user.model.delete.DeleteSavedPlaceResponse;
import com.qd.user.model.editprofile.EditProfileResponse;
import com.qd.user.model.info.vendor.VendorInfoResponse;
import com.qd.user.model.myorders.OrdersResponse;
import com.qd.user.model.notifications.NotificationsListingResponse;
import com.qd.user.model.ordercancellation.OrderCancellationResponse;
import com.qd.user.model.orderdetail.OrderDetailResponse;
import com.qd.user.model.otpverification.OtpVerificationResponse;
import com.qd.user.model.payments.invoice.InvoiceListingResponse;
import com.qd.user.model.payments.invoicedetails.InvoiceDetailsResponse;
import com.qd.user.model.payments.personal.PaymentHistoryResponse;
import com.qd.user.model.payments.wallet.WalletRechargeResponse;
import com.qd.user.model.payments.wallet.WalletTransactionHistoryResponse;
import com.qd.user.model.profile.ProfileResponse;
import com.qd.user.model.recievedorders.RecievedOrdersResponse;
import com.qd.user.model.socialresponse.socialsigninresponse.SocialLoginResponse;
import com.qd.user.model.vendor.uniquevendor.UniqueEmailResponse;
import com.qd.user.model.vendor.vendorsignup.VendorSignUpResponse;
import com.qd.user.model.vendor.vendorstatus.VendorStatusResponse;

import java.math.BigDecimal;
import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

interface ApiClient {
    String API_URL = BuildConfig.END_POINT; //Development Server

    @FormUrlEncoded
    @POST("auth/login")
    Call<CreateAccountResponse> hitLoginApi(@FieldMap HashMap<String, String> requestPayloadForLogin);

    @POST("cashclient/signup")
    Call<CreateAccountResponse> hitSignUpApi(@Body RequestBody jsonObject);

    @FormUrlEncoded
    @POST("auth/forgotpassword")
    Call<CommonResponse> hitForgotPasswordApi(@FieldMap HashMap<String, String> responsePayload);

    @FormUrlEncoded
    @POST("cashclient/socialSignup")
    Call<SocialLoginResponse> hitSocialLoginApi(@FieldMap HashMap<String, String> fbSignInObject);

    @GET("drivers/get-item-category")
    Call<ItemsTypeListResponse> getItemTypes();

    @FormUrlEncoded
    @POST("cashclient/edit-cash-client")
    Call<EditProfileResponse> hitEditProfileApi(@FieldMap HashMap<String, String> editProfile);

    @FormUrlEncoded
    @POST("auth/changepassword")
    Call<CommonResponse> hitChangePasswordApi(@FieldMap HashMap<String, String> changePasswordObject);

    @FormUrlEncoded
    @POST("auth/resetpassword")
    Call<CommonResponse> hitResetPasswordApi(@FieldMap HashMap<String, String> resetPasswordObject);

    @POST("cashclient/save-address")
    Call<SaveAddressResponse> saveAddressInFav(@Body RequestBody body);

    @FormUrlEncoded
    @POST("cashclient/get-saved-address")
    Call<SavedAddressResponse> getSavedAddresses(@Field("cashClientId") String userId, @Field("searchString") String searchString);

    @FormUrlEncoded
    @POST("cashclient/remove-saved-address")
    Call<DeleteSavedPlaceResponse> hitDeleteSavedPlaceApi(@Field("savedAddressId") String savedPlaceId);

    @POST("cashclient/edit-saved-address")
    Call<SaveAddressResponse> hitEditSaveAddressApi(@Body RequestBody body);

    @FormUrlEncoded
    @POST("auth/validateotp")
    Call<OtpVerificationResponse> hitValidateOtpApi(@FieldMap HashMap<String, String> requestPayloadForVerification);

    @GET()
    Call<PlacesAutoCompleteResponse> getPlacesAutoCompleteResponse(@Url String url);

    @GET("drivers/get-vehicle-category")
    Call<VehicleCategoryResponse> getVehicleCategories();

    @GET()
    Call<DirectionsApiResponse> getDirectionsApiResponse(@Url String url);

    @POST("orders/create-order")
    Call<CreateOrderResponse> createOrder(@Body RequestBody body);

    @POST("vendor/signup")
    Call<VendorSignUpResponse> hitVendorSignUpApi(@Body RequestBody body);

    @FormUrlEncoded
    @POST("cashClient/vendor-application-status")
    Call<VendorStatusResponse> hitVendorStatusApi(@Field("vendorId") String id, @Field("deviceId") String deviceId, @Field("platform") String userId);

    @FormUrlEncoded
    @POST("vendor/isUnique")
    Call<UniqueEmailResponse> hitIsVendorUniqueApi(@FieldMap HashMap<String, String> object);

    @FormUrlEncoded
    @POST("cashClient/newUserSocialSignup")
    Call<CreateAccountResponse> hitSocialSignUpApi(@FieldMap HashMap<String, String> socialSignUpObject);

    @FormUrlEncoded
    @POST("cashclient/cash-client-order-history")
    Call<OrdersResponse> hitOrderHistoryApi(@FieldMap HashMap<String, Object> orderHistoryObject);

    @FormUrlEncoded
    @POST("cashclient/cash-client-order-details")
    Call<OrderDetailResponse> hitOrderDetailApi(@Field("orderId") String orderId);

    @GET("cashclient/get-configuration")
    Call<FlexibleSlotsResponse> getFlexibleTimeSlotsApi();

    @FormUrlEncoded
    @POST("cashclient/cashClient-received-orders")
    Call<RecievedOrdersResponse> hitReceivedOrderApi(@FieldMap HashMap<String, Object> orderHistoryObject);

    @FormUrlEncoded
    @POST("career/get-in-touch")
    Call<ContactUsResponse> hitContactUsApi(@FieldMap HashMap<String, Object> requestPayloadForContactUs);

    @GET("orders/track-order/{orderId}")
    Call<OrderDetailResponse> getCurrentOrderStatus(@Path("orderId") String orderUniqueId);

    @POST("orders/cancel-order")
    Call<OrderCancellationResponse> cancelOrder(@Body RequestBody body);

    @POST("orders/check-driver-availability")
    Call<DriverAvailabilityResponse> checkDriverAvailability(@Body RequestBody body);

    @FormUrlEncoded
    @POST("promocode/apply-promo-code")
    Call<PromoCodeResponse> applyPromoCode(@FieldMap HashMap<String, Object> body);

    @FormUrlEncoded
    @POST("orders/retry-order")
    Call<CommonResponse> retryForDriver(@Field("orderId") String id);

    @POST("payments/optimize-drops")
    Call<OptimizedDeliveryResponse> getDeliveryCharges(@Body RequestBody body);

    @FormUrlEncoded
    @POST("vendor/get-invoices")
    Call<InvoiceListingResponse> getInvoiceListing(@FieldMap HashMap<String, Object> payloadForInvoice);

    @FormUrlEncoded
    @POST("vendor/get-invoice-details")
    Call<InvoiceDetailsResponse> getInvoiceDetails(@FieldMap HashMap<String, Object> requestPayload);

    @FormUrlEncoded
    @POST("payments/transaction-history")
    Call<WalletTransactionHistoryResponse> getWalletTransactionHistory(@FieldMap HashMap<String, Object> requestPayload);

    @FormUrlEncoded
    @POST("cashclient/payment-history")
    Call<PaymentHistoryResponse> getPersonalPaymentHistory(@FieldMap HashMap<String, Object> payload);

    @FormUrlEncoded
    @POST("payments/wallet-validation")
    Call<CommonResponse> validateWalletAmount(@Field("vendorId") String userId, @Field("amount") BigDecimal totalDeliveryCharges);

    @POST("payments/initialize-payment")
    Call<WalletRechargeResponse> initializePayment(@Body RequestBody requestPayloadForRecharge);

    @FormUrlEncoded
    @POST("vendor/get-vendor-configuration")
    Call<VendorInfoResponse> getVendorInfo(@Field("vendorId") String userId);

    @POST("orders/create-flexible-order")
    Call<FlexibleOrdersResponse> createFlexibleOrder(@Body RequestBody jsonObject);

    @FormUrlEncoded
    @POST("vendor/vehicle-category")
    Call<VehicleEtaResponse> getAvailableVehicles(@Field("orderType") String orderType, @Field("vendorId") String userId,
                                                  @Field("lat") double latitude, @Field("lng") double longitude);

    @POST("orders/get-eta")
    Call<OptimizedDeliveryResponse> getEta(@Body RequestBody body);

    @FormUrlEncoded
    @POST("vendor/vehicle-category")
    Call<VehicleEtaResponse> getVehicleEta(@Field("orderType") String orderType, @Field("vendorId") String userId);

    @FormUrlEncoded
    @POST("vendor/get-pending-balance")
    Call<PendingBalanceResponse> getPendingBalance(@Field("vendorId") String userId);

    @FormUrlEncoded
    @POST("auth/logout")
    Call<CommonResponse> logOutUser(@Field("userType") String userType, @Field("userId") String userId, @Field("deviceId") String deviceId);

    @FormUrlEncoded
    @POST("cashclient/get-recipient-order")
    Call<OrderDetailResponse> hitReceivedOrderDetailApi(@Field("orderId") String orderId, @Field("phoneNumber") String phoneNumber);

    @FormUrlEncoded
    @POST("cashclient/get-notifications")
    Call<NotificationsListingResponse> getNotificationListing(@FieldMap HashMap<String, Object> requestPayload);

    @FormUrlEncoded
    @POST("orders/appprove-order-return")
    Call<OrderCancellationResponse> approveOrderReturn(@Field("elementId") String elementId, @Field("approvedBy") String vendor);

    @GET("vendor/profile")
    Call<ProfileResponse> getProfileInfo();

    @FormUrlEncoded
    @POST("version/versionInApp")
    Call<CommonResponse> hitVersionApi(@FieldMap HashMap<String, String> versionCode);

    @POST("notification/mark-notification")
    Call<CommonResponse> markNotificationAsRead(@Body RequestBody body);

    @FormUrlEncoded
    @POST("drivers/order-status")
    Call<CommonResponse> checkForDriverAvailability(@Field("orderId") String id);

    @FormUrlEncoded
    @POST("orders/auto-cancel")
    Call<OrderCancellationResponse> autoCancelOrder(@Field("orderId") String id);

    @FormUrlEncoded
    @POST("cashclient/manage-notifications")
    Call<CommonResponse> enableOrDisableNotifications(@Field("vendorId") String userId, @Field("notificationStatus") boolean isChecked);

    @FormUrlEncoded
    @POST("auth/resendotp")
    Call<CommonResponse> resendOtp(@Field("type") String type, @Field("countryCode") String countryCode, @Field("phoneNumber") String phoneNumber);


    @GET("pages/cancellation-reasons")
    Call<CommonListResponse> getCancellationReasons();
}