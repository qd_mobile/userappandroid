package com.qd.user.socket;

import android.util.Log;

import com.qd.user.data.DataManager;

import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

public class SocketManager {

    private static final String TAG = SocketManager.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static SocketManager instance;
    private static Socket mSocket;

    public static SocketManager getInstance() {
        if (instance == null) {
            synchronized (LOCK) {
                instance = new SocketManager();
            }
        }
        return instance;
    }


    public static void initializeSocket() {
        if (mSocket == null) {
            try {
                IO.Options options = new IO.Options();
                options.transports = new String[]{WebSocket.NAME};
                options.reconnection = true;
                options.forceNew = true;
                options.secure = true;
                options.query = "token=" + DataManager.getInstance().getAccessToken() + "&platform=user";
                mSocket = IO.socket(SocketConstants.SOCKET_URL, options);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public void connectSocket() {
        if (mSocket != null && !mSocket.connected()) {
            setSocketConnectionListeners();
            setLocationConnectionListeners();
            mSocket.connect();
        }
    }


    public void disconnectSocket() {
        disconnectSocketListeners();
        if (mSocket != null && mSocket.connected()) {
            mSocket.disconnect();
        }
    }

    private void setLocationConnectionListeners() {
        mSocket.on(SocketConstants.EVENT_ORDER_STATUS, args -> Log.e(TAG, args[0].toString()));

        mSocket.on(SocketConstants.EVENT_ORDER_ACCEPTED_BY_DRIVER, args -> Log.e(TAG, args[0].toString()));

    }



    public void sendDataOnSocket(String event, JSONObject data) {
        if (mSocket != null) {
            mSocket.emit(event, data, (Ack) args -> Log.e(TAG, args[0].toString()));
        } else {
            initializeSocket();
            mSocket.connect();
            mSocket.emit(event, data, (Ack) args -> Log.e(TAG, args[0].toString()));
        }
    }


    public void disconnectSocketListeners() {
        if (mSocket != null && mSocket.connected()) {
            mSocket.off();
        }
    }

    public void offListeningEvent(String eventName) {
        if (mSocket != null)
            mSocket.off(eventName);
    }


    private void setSocketConnectionListeners() {
        mSocket.on(Socket.EVENT_CONNECT, args -> Log.e(TAG, "EVENT CONNECTED !!"));
        mSocket.on(Socket.EVENT_DISCONNECT, args -> Log.e(TAG, "EVENT DISCONNECT !!"));
        mSocket.on(Socket.EVENT_RECONNECTING, args -> Log.e(TAG, "EVENT RECONNECTING !!"));
        mSocket.on(Socket.EVENT_CONNECT_ERROR, args -> Log.e(TAG, "CONNECTION ERROR !!"));


    }

    public void listenToOrderStatus(final Emitter.Listener listener) {

        if (mSocket != null) {
            mSocket.off(SocketConstants.EVENT_ORDER_STATUS_FOR_APP);
            mSocket.on(SocketConstants.EVENT_ORDER_STATUS_FOR_APP, listener);
        }

    }

    public void listenToDriverCurrentDriverLocation(Emitter.Listener listener) {
        if (mSocket != null) {
            mSocket.off(SocketConstants.EVENT_DRIVER_LOCATION);
            mSocket.on(SocketConstants.EVENT_DRIVER_LOCATION, listener);
        }
    }

    public void listenForDriverAcceptance(Emitter.Listener listener) {
        if (mSocket != null) {
            mSocket.off(SocketConstants.EVENT_ORDER_ACCEPTED_BY_DRIVER);
            mSocket.on(SocketConstants.EVENT_ORDER_ACCEPTED_BY_DRIVER, listener);
        }
    }


}
