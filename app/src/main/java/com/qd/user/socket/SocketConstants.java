package com.qd.user.socket;


import com.qd.user.BuildConfig;

public class SocketConstants {

    static final String SOCKET_URL = BuildConfig.SOCKET_URL;

    public static final String KEY_DRIVER_ID = "driverId";
    public static final String EVENT_DRIVER_LOCATION = "driverLocation";
    public static final String KEY_TYPE = "type";
    public static final String KEY_ORDER_ID = "orderId";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String TYPE = "type";

    public static final String EVENT_ORDER_STATUS = "orderStatus";
    public static final String EVENT_ORDER_ACCEPTED_BY_DRIVER = "orderAcceptedByDriver";
    static final String EVENT_ORDER_STATUS_FOR_APP = "orderStatusForApp";

}
