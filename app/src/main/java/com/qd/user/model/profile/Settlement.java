
package com.qd.user.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Settlement {

    @SerializedName("noOfDays")
    @Expose
    private long noOfDays;
    @SerializedName("amount")
    @Expose
    private long amount;

    public long getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(long noOfDays) {
        this.noOfDays = noOfDays;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

}
