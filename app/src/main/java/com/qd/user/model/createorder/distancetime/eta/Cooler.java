
package com.qd.user.model.createorder.distancetime.eta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cooler {

    @SerializedName("ETA")
    @Expose
    private String eTA;

    public String getETA() {
        return eTA;
    }

    public void setETA(String eTA) {
        this.eTA = eTA;
    }

}
