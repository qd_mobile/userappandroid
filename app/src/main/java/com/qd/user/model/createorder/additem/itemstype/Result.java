
package com.qd.user.model.createorder.additem.itemstype;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Comparable<Result> {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("vehicleCategoryId")
    @Expose
    private Object vehicleCategoryId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("keyword")
    @Expose
    private String keyword;
    @SerializedName("suggestedVehicleCategory")
    @Expose
    private String suggestedVehicleCategory;
    @SerializedName("iconLink")
    @Expose
    private String iconLink;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("weightage")
    @Expose
    private int weightage;

    public int getWeightage() {
        return weightage;
    }

    public void setWeightage(int weightage) {
        this.weightage = weightage;
    }
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Long v;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getVehicleCategoryId() {
        return vehicleCategoryId;
    }

    public void setVehicleCategoryId(Object vehicleCategoryId) {
        this.vehicleCategoryId = vehicleCategoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSuggestedVehicleCategory() {
        return suggestedVehicleCategory;
    }

    public void setSuggestedVehicleCategory(String suggestedVehicleCategory) {
        this.suggestedVehicleCategory = suggestedVehicleCategory;
    }

    public String getIconLink() {
        return iconLink;
    }

    public void setIconLink(String iconLink) {
        this.iconLink = iconLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getV() {
        return v;
    }

    public void setV(Long v) {
        this.v = v;
    }

    @Override
    public int compareTo(Result o) {
        return keyword.compareTo(o.keyword);
    }
}
