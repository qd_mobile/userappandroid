package com.qd.user.model.orderstatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qd.user.model.orderdetail.DropLocation;

import java.util.ArrayList;

public class CurrentLocationResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("currentOrderStatus")
    @Expose
    private String currentOrderStatus;
    @SerializedName("currentLocationViaSocket")
    @Expose
    private CurrentLocationViaSocket currentLocationViaSocket;
    @SerializedName("dropLocation")
    @Expose
    private ArrayList<DropLocation> dropLocation;
    @SerializedName("ETA")
    @Expose
    private String ETA;
    @SerializedName("timeStamp")
    @Expose
    private Long timeStamp;

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }


    public ArrayList<DropLocation> getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(ArrayList<DropLocation> dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CurrentLocationViaSocket getCurrentLocationViaSocket() {
        return currentLocationViaSocket;
    }

    public void setCurrentLocationViaSocket(CurrentLocationViaSocket currentLocationViaSocket) {
        this.currentLocationViaSocket = currentLocationViaSocket;
    }

    public String getCurrentOrderStatus() {
        return currentOrderStatus;
    }

    public void setCurrentOrderStatus(String currentOrderStatus) {
        this.currentOrderStatus = currentOrderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getETA() {
        return ETA;
    }

    public void setETA(String ETA) {
        this.ETA = ETA;
    }
}
