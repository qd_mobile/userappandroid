
package com.qd.user.model.payments.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class OrderId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("orderUniqueId")
    @Expose
    private String orderUniqueId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

}
