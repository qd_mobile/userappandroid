
package com.qd.user.model.payments.invoicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickupLocation {

    @SerializedName("pickupDriverUniqueId")
    @Expose
    private String pickupDriverUniqueId;
    @SerializedName("pickupDriverName")
    @Expose
    private String pickupDriverName;

    public String getPickupDriverUniqueId() {
        return pickupDriverUniqueId;
    }

    public void setPickupDriverUniqueId(String pickupDriverUniqueId) {
        this.pickupDriverUniqueId = pickupDriverUniqueId;
    }

    public String getPickupDriverName() {
        return pickupDriverName;
    }

    public void setPickupDriverName(String pickupDriverName) {
        this.pickupDriverName = pickupDriverName;
    }

}
