
package com.qd.user.model.createorder.distancetime.eta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("ETA")
    @Expose
    private ETA eTA;
    @SerializedName("vehicleCategory")
    @Expose
    private List<VehicleCategory> vehicleCategory = null;

    public ETA getETA() {
        return eTA;
    }

    public void setETA(ETA eTA) {
        this.eTA = eTA;
    }

    public List<VehicleCategory> getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(List<VehicleCategory> vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

}
