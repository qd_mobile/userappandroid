
package com.qd.user.model.editprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Otp {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("generatedTime")
    @Expose
    private Integer generatedTime;
    @SerializedName("expiryTime")
    @Expose
    private Integer expiryTime;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getGeneratedTime() {
        return generatedTime;
    }

    public void setGeneratedTime(Integer generatedTime) {
        this.generatedTime = generatedTime;
    }

    public Integer getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Integer expiryTime) {
        this.expiryTime = expiryTime;
    }

}
