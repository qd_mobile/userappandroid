
package com.qd.user.model.createorder.vehicle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IconLink {

    @SerializedName("green")
    @Expose
    private String green;
    @SerializedName("black")
    @Expose
    private String black;
    @SerializedName("grey")
    @Expose
    private String grey;
    @SerializedName("white")
    @Expose
    private String white;

    @SerializedName("addItem")
    @Expose
    private String addItem;

    public String getAddItem() {
        return addItem;
    }

    public void setAddItem(String addItem) {
        this.addItem = addItem;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public String getBlack() {
        return black;
    }

    public void setBlack(String black) {
        this.black = black;
    }

    public String getGrey() {
        return grey;
    }

    public void setGrey(String grey) {
        this.grey = grey;
    }

    public String getWhite() {
        return white;
    }

    public void setWhite(String white) {
        this.white = white;
    }

}
