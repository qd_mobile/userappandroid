
package com.qd.user.model.ordercancellation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

class Item_ {

    @SerializedName("itemImage")
    @Expose
    private List<String> itemImage = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("itemPrice")
    @Expose
    private BigDecimal itemPrice;
    @SerializedName("note")
    @Expose
    private String note;

    public List<String> getItemImage() {
        return itemImage;
    }

    public void setItemImage(List<String> itemImage) {
        this.itemImage = itemImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
