
package com.qd.user.model.createaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class ApprovalStatusDates {

    @SerializedName("approvedDate")
    @Expose
    private String approvedDate;

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

}
