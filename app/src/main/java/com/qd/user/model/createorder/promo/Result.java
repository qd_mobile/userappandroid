
package com.qd.user.model.createorder.promo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("NoOfTimesUsed")
    @Expose
    private Long noOfTimesUsed;
    @SerializedName("isfrequency")
    @Expose
    private Boolean isfrequency;
    @SerializedName("noOfTimePublished")
    @Expose
    private Long noOfTimePublished;
    @SerializedName("applicableCashClient")
    @Expose
    private List<Object> applicableCashClient = null;
    @SerializedName("promocodeName")
    @Expose
    private String promocodeName;
    @SerializedName("promocodeKeyword")
    @Expose
    private String promocodeKeyword;
    @SerializedName("discount")
    @Expose
    private BigDecimal discount;
    @SerializedName("validStartDate")
    @Expose
    private String validStartDate;
    @SerializedName("validEndDate")
    @Expose
    private String validEndDate;
    @SerializedName("minimumDeliveryFee")
    @Expose
    private BigDecimal minimumDeliveryFee;
    @SerializedName("maximimUses")
    @Expose
    private Long maximimUses;
    @SerializedName("applicableFor")
    @Expose
    private String applicableFor;
    @SerializedName("frequencyPerUser")
    @Expose
    private Long frequencyPerUser;
    @SerializedName("discountType")
    @Expose
    private String discountType;
    @SerializedName("discountedAmount")
    @Expose
    private BigDecimal discountedAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getNoOfTimesUsed() {
        return noOfTimesUsed;
    }

    public void setNoOfTimesUsed(Long noOfTimesUsed) {
        this.noOfTimesUsed = noOfTimesUsed;
    }

    public Boolean getIsfrequency() {
        return isfrequency;
    }

    public void setIsfrequency(Boolean isfrequency) {
        this.isfrequency = isfrequency;
    }

    public Long getNoOfTimePublished() {
        return noOfTimePublished;
    }

    public void setNoOfTimePublished(Long noOfTimePublished) {
        this.noOfTimePublished = noOfTimePublished;
    }

    public List<Object> getApplicableCashClient() {
        return applicableCashClient;
    }

    public void setApplicableCashClient(List<Object> applicableCashClient) {
        this.applicableCashClient = applicableCashClient;
    }

    public String getPromocodeName() {
        return promocodeName;
    }

    public void setPromocodeName(String promocodeName) {
        this.promocodeName = promocodeName;
    }

    public String getPromocodeKeyword() {
        return promocodeKeyword;
    }

    public void setPromocodeKeyword(String promocodeKeyword) {
        this.promocodeKeyword = promocodeKeyword;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getValidStartDate() {
        return validStartDate;
    }

    public void setValidStartDate(String validStartDate) {
        this.validStartDate = validStartDate;
    }

    public String getValidEndDate() {
        return validEndDate;
    }

    public void setValidEndDate(String validEndDate) {
        this.validEndDate = validEndDate;
    }

    public BigDecimal getMinimumDeliveryFee() {
        return minimumDeliveryFee;
    }

    public void setMinimumDeliveryFee(BigDecimal minimumDeliveryFee) {
        this.minimumDeliveryFee = minimumDeliveryFee;
    }

    public Long getMaximimUses() {
        return maximimUses;
    }

    public void setMaximimUses(Long maximimUses) {
        this.maximimUses = maximimUses;
    }

    public String getApplicableFor() {
        return applicableFor;
    }

    public void setApplicableFor(String applicableFor) {
        this.applicableFor = applicableFor;
    }

    public Long getFrequencyPerUser() {
        return frequencyPerUser;
    }

    public void setFrequencyPerUser(Long frequencyPerUser) {
        this.frequencyPerUser = frequencyPerUser;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public BigDecimal getDiscountedAmount() {
        return discountedAmount;
    }

    public void setDiscountedAmount(BigDecimal discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

}
