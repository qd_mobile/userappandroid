
package com.qd.user.model.orderstatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qd.user.model.orderdetail.DropLocation;
import com.qd.user.model.orderdetail.Orderstatus;

import java.util.List;

public class OrderStatusResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("paymentDetails")
    @Expose
    private PaymentDetails paymentDetails;
    @SerializedName("specialPayments")
    @Expose
    private SpecialPayments specialPayments;
    @SerializedName("pickupInstruction")
    @Expose
    private String pickupInstruction;
    @SerializedName("isBulk")
    @Expose
    private Boolean isBulk;
    @SerializedName("isOrderInprogress")
    @Expose
    private Boolean isOrderInprogress;
    @SerializedName("isSettled")
    @Expose
    private Boolean isSettled;
    @SerializedName("isInvoiced")
    @Expose
    private Boolean isInvoiced;
    @SerializedName("reassignDriverId")
    @Expose
    private Object reassignDriverId;
    @SerializedName("isCancelled")
    @Expose
    private Boolean isCancelled;
    @SerializedName("cancellationTime")
    @Expose
    private String cancellationTime;
    @SerializedName("requestFrom")
    @Expose
    private String requestFrom;
    @SerializedName("requestFor")
    @Expose
    private String requestFor;
    @SerializedName("deliveryType")
    @Expose
    private String deliveryType;
    @SerializedName("pickupLocation")
    @Expose
    private List<PickupLocation> pickupLocation = null;
    @SerializedName("dropLocation")
    @Expose
    private List<com.qd.user.model.orderdetail.DropLocation> dropLocation = null;
    @SerializedName("vehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;
    @SerializedName("isScheduled")
    @Expose
    private Boolean isScheduled;
    @SerializedName("isLater")
    @Expose
    private Boolean isLater;
    @SerializedName("scheduledStartTime")
    @Expose
    private String scheduledStartTime;
    @SerializedName("scheduledEndTime")
    @Expose
    private String scheduledEndTime;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("vendorId")
    @Expose
    private String vendorId;
    @SerializedName("actualStartTime")
    @Expose
    private String actualStartTime;
    @SerializedName("actualEndTime")
    @Expose
    private String actualEndTime;
    @SerializedName("orderUniqueId")
    @Expose
    private String orderUniqueId;
    @SerializedName("orderStatus")
    @Expose
    private List<Orderstatus> orderStatus = null;
    @SerializedName("issue")
    @Expose
    private List<Object> issue = null;
    @SerializedName("location")
    @Expose
    private List<Object> location = null;
    @SerializedName("zoneId")
    @Expose
    private String zoneId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Long v;
    @SerializedName("driverId")
    @Expose
    private String driverId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public SpecialPayments getSpecialPayments() {
        return specialPayments;
    }

    public void setSpecialPayments(SpecialPayments specialPayments) {
        this.specialPayments = specialPayments;
    }

    public String getPickupInstruction() {
        return pickupInstruction;
    }

    public void setPickupInstruction(String pickupInstruction) {
        this.pickupInstruction = pickupInstruction;
    }

    public Boolean getIsBulk() {
        return isBulk;
    }

    public void setIsBulk(Boolean isBulk) {
        this.isBulk = isBulk;
    }

    public Boolean getIsOrderInprogress() {
        return isOrderInprogress;
    }

    public void setIsOrderInprogress(Boolean isOrderInprogress) {
        this.isOrderInprogress = isOrderInprogress;
    }

    public Boolean getIsSettled() {
        return isSettled;
    }

    public void setIsSettled(Boolean isSettled) {
        this.isSettled = isSettled;
    }

    public Boolean getIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(Boolean isInvoiced) {
        this.isInvoiced = isInvoiced;
    }

    public Object getReassignDriverId() {
        return reassignDriverId;
    }

    public void setReassignDriverId(Object reassignDriverId) {
        this.reassignDriverId = reassignDriverId;
    }

    public Boolean getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(Boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public String getCancellationTime() {
        return cancellationTime;
    }

    public void setCancellationTime(String cancellationTime) {
        this.cancellationTime = cancellationTime;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String getRequestFor() {
        return requestFor;
    }

    public void setRequestFor(String requestFor) {
        this.requestFor = requestFor;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public List<PickupLocation> getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(List<PickupLocation> pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public List<com.qd.user.model.orderdetail.DropLocation> getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(List<DropLocation> dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Boolean getIsScheduled() {
        return isScheduled;
    }

    public void setIsScheduled(Boolean isScheduled) {
        this.isScheduled = isScheduled;
    }

    public Boolean getIsLater() {
        return isLater;
    }

    public void setIsLater(Boolean isLater) {
        this.isLater = isLater;
    }

    public String getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(String scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public String getScheduledEndTime() {
        return scheduledEndTime;
    }

    public void setScheduledEndTime(String scheduledEndTime) {
        this.scheduledEndTime = scheduledEndTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getActualStartTime() {
        return actualStartTime;
    }

    public void setActualStartTime(String actualStartTime) {
        this.actualStartTime = actualStartTime;
    }

    public String getActualEndTime() {
        return actualEndTime;
    }

    public void setActualEndTime(String actualEndTime) {
        this.actualEndTime = actualEndTime;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

    public List<Orderstatus> getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(List<Orderstatus> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<Object> getIssue() {
        return issue;
    }

    public void setIssue(List<Object> issue) {
        this.issue = issue;
    }

    public List<Object> getLocation() {
        return location;
    }

    public void setLocation(List<Object> location) {
        this.location = location;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getV() {
        return v;
    }

    public void setV(Long v) {
        this.v = v;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

}
