
package com.qd.user.model.payments.personal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("paymentDetails")
    @Expose
    private PaymentDetails paymentDetails;
    @SerializedName("pickupLocation")
    @Expose
    private List<PickupLocation> pickupLocation = null;
    @SerializedName("dropLocation")
    @Expose
    private List<DropLocation> dropLocation = null;
    @SerializedName("scheduledStartTime")
    @Expose
    private String scheduledStartTime;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("orderUniqueId")
    @Expose
    private String orderUniqueId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public List<PickupLocation> getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(List<PickupLocation> pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public List<DropLocation> getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(List<DropLocation> dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(String scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
