
package com.qd.user.model.kuwaitfinder.streets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

    @SerializedName("objectid")
    @Expose
    private Long objectid;
    @SerializedName("streetenglish")
    @Expose
    private String streetenglish;
    @SerializedName("streetarabic")
    @Expose
    private String streetarabic;
    @SerializedName("streetnumber")
    @Expose
    private Long streetnumber;
    @SerializedName("alternativestreetarabic1")
    @Expose
    private Object alternativestreetarabic1;
    @SerializedName("alternativestreetarabic2")
    @Expose
    private Object alternativestreetarabic2;
    @SerializedName("alternativestreetarabic3")
    @Expose
    private Object alternativestreetarabic3;
    @SerializedName("alternativestreetarabic4")
    @Expose
    private Object alternativestreetarabic4;
    @SerializedName("alternativestreetenglish1")
    @Expose
    private Object alternativestreetenglish1;
    @SerializedName("alternativestreetenglish2")
    @Expose
    private Object alternativestreetenglish2;
    @SerializedName("alternativestreetenglish3")
    @Expose
    private Object alternativestreetenglish3;
    @SerializedName("alternativestreetenglish4")
    @Expose
    private Object alternativestreetenglish4;
    @SerializedName("blockarabic")
    @Expose
    private String blockarabic;
    @SerializedName("neighborhoodarabic")
    @Expose
    private String neighborhoodarabic;
    @SerializedName("neighborhoodenglish")
    @Expose
    private String neighborhoodenglish;
    @SerializedName("governoratearabic")
    @Expose
    private String governoratearabic;
    @SerializedName("governorateenglish")
    @Expose
    private String governorateenglish;
    @SerializedName("detailsarabic")
    @Expose
    private String detailsarabic;
    @SerializedName("detailsenglish")
    @Expose
    private String detailsenglish;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("blockenglish")
    @Expose
    private String blockenglish;
    @SerializedName("centroid_x")
    @Expose
    private Double centroidX;
    @SerializedName("centroid_y")
    @Expose
    private Double centroidY;
    @SerializedName("nhood_no")
    @Expose
    private Long nhoodNo;
    @SerializedName("gov_no")
    @Expose
    private Long govNo;

    public Long getObjectid() {
        return objectid;
    }

    public void setObjectid(Long objectid) {
        this.objectid = objectid;
    }

    public String getStreetenglish() {
        return streetenglish;
    }

    public void setStreetenglish(String streetenglish) {
        this.streetenglish = streetenglish;
    }

    public String getStreetarabic() {
        return streetarabic;
    }

    public void setStreetarabic(String streetarabic) {
        this.streetarabic = streetarabic;
    }

    public Long getStreetnumber() {
        return streetnumber;
    }

    public void setStreetnumber(Long streetnumber) {
        this.streetnumber = streetnumber;
    }

    public Object getAlternativestreetarabic1() {
        return alternativestreetarabic1;
    }

    public void setAlternativestreetarabic1(Object alternativestreetarabic1) {
        this.alternativestreetarabic1 = alternativestreetarabic1;
    }

    public Object getAlternativestreetarabic2() {
        return alternativestreetarabic2;
    }

    public void setAlternativestreetarabic2(Object alternativestreetarabic2) {
        this.alternativestreetarabic2 = alternativestreetarabic2;
    }

    public Object getAlternativestreetarabic3() {
        return alternativestreetarabic3;
    }

    public void setAlternativestreetarabic3(Object alternativestreetarabic3) {
        this.alternativestreetarabic3 = alternativestreetarabic3;
    }

    public Object getAlternativestreetarabic4() {
        return alternativestreetarabic4;
    }

    public void setAlternativestreetarabic4(Object alternativestreetarabic4) {
        this.alternativestreetarabic4 = alternativestreetarabic4;
    }

    public Object getAlternativestreetenglish1() {
        return alternativestreetenglish1;
    }

    public void setAlternativestreetenglish1(Object alternativestreetenglish1) {
        this.alternativestreetenglish1 = alternativestreetenglish1;
    }

    public Object getAlternativestreetenglish2() {
        return alternativestreetenglish2;
    }

    public void setAlternativestreetenglish2(Object alternativestreetenglish2) {
        this.alternativestreetenglish2 = alternativestreetenglish2;
    }

    public Object getAlternativestreetenglish3() {
        return alternativestreetenglish3;
    }

    public void setAlternativestreetenglish3(Object alternativestreetenglish3) {
        this.alternativestreetenglish3 = alternativestreetenglish3;
    }

    public Object getAlternativestreetenglish4() {
        return alternativestreetenglish4;
    }

    public void setAlternativestreetenglish4(Object alternativestreetenglish4) {
        this.alternativestreetenglish4 = alternativestreetenglish4;
    }

    public String getBlockarabic() {
        return blockarabic;
    }

    public void setBlockarabic(String blockarabic) {
        this.blockarabic = blockarabic;
    }

    public String getNeighborhoodarabic() {
        return neighborhoodarabic;
    }

    public void setNeighborhoodarabic(String neighborhoodarabic) {
        this.neighborhoodarabic = neighborhoodarabic;
    }

    public String getNeighborhoodenglish() {
        return neighborhoodenglish;
    }

    public void setNeighborhoodenglish(String neighborhoodenglish) {
        this.neighborhoodenglish = neighborhoodenglish;
    }

    public String getGovernoratearabic() {
        return governoratearabic;
    }

    public void setGovernoratearabic(String governoratearabic) {
        this.governoratearabic = governoratearabic;
    }

    public String getGovernorateenglish() {
        return governorateenglish;
    }

    public void setGovernorateenglish(String governorateenglish) {
        this.governorateenglish = governorateenglish;
    }

    public String getDetailsarabic() {
        return detailsarabic;
    }

    public void setDetailsarabic(String detailsarabic) {
        this.detailsarabic = detailsarabic;
    }

    public String getDetailsenglish() {
        return detailsenglish;
    }

    public void setDetailsenglish(String detailsenglish) {
        this.detailsenglish = detailsenglish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBlockenglish() {
        return blockenglish;
    }

    public void setBlockenglish(String blockenglish) {
        this.blockenglish = blockenglish;
    }

    public Double getCentroidX() {
        return centroidX;
    }

    public void setCentroidX(Double centroidX) {
        this.centroidX = centroidX;
    }

    public Double getCentroidY() {
        return centroidY;
    }

    public void setCentroidY(Double centroidY) {
        this.centroidY = centroidY;
    }

    public Long getNhoodNo() {
        return nhoodNo;
    }

    public void setNhoodNo(Long nhoodNo) {
        this.nhoodNo = nhoodNo;
    }

    public Long getGovNo() {
        return govNo;
    }

    public void setGovNo(Long govNo) {
        this.govNo = govNo;
    }

}
