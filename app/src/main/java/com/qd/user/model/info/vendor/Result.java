
package com.qd.user.model.info.vendor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("contact")
    @Expose
    private Contact contact;
    @SerializedName("fullBusinessAddress")
    @Expose
    private FullBusinessAddress fullBusinessAddress;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("vendorType")
    @Expose
    private String vendorType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("pendingBalance")
    @Expose
    private List<Object> pendingBalance = null;
    @SerializedName("invoice")
    @Expose
    private Boolean invoice;
    @SerializedName("paymentModes")
    @Expose
    private String paymentModes;
    @SerializedName("servicesOffered")
    @Expose
    private String servicesOffered;
    @SerializedName("settlement")
    @Expose
    private Settlement settlement;
    @SerializedName("previousCancellationCharge")
    @Expose
    private Integer previousCancellationCharge;
    @SerializedName("previousReturnCharges")
    @Expose
    private Integer previousReturnCharges;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public FullBusinessAddress getFullBusinessAddress() {
        return fullBusinessAddress;
    }

    public void setFullBusinessAddress(FullBusinessAddress fullBusinessAddress) {
        this.fullBusinessAddress = fullBusinessAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getPendingBalance() {
        return pendingBalance;
    }

    public void setPendingBalance(List<Object> pendingBalance) {
        this.pendingBalance = pendingBalance;
    }

    public Boolean getInvoice() {
        return invoice;
    }

    public void setInvoice(Boolean invoice) {
        this.invoice = invoice;
    }

    public String getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(String paymentModes) {
        this.paymentModes = paymentModes;
    }

    public String getServicesOffered() {
        return servicesOffered;
    }

    public void setServicesOffered(String servicesOffered) {
        this.servicesOffered = servicesOffered;
    }

    public Settlement getSettlement() {
        return settlement;
    }

    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    public Integer getPreviousCancellationCharge() {
        return previousCancellationCharge;
    }

    public void setPreviousCancellationCharge(Integer previousCancellationCharge) {
        this.previousCancellationCharge = previousCancellationCharge;
    }

    public Integer getPreviousReturnCharges() {
        return previousReturnCharges;
    }

    public void setPreviousReturnCharges(Integer previousReturnCharges) {
        this.previousReturnCharges = previousReturnCharges;
    }

}
