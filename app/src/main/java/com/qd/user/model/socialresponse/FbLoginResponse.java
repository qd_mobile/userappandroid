package com.qd.user.model.socialresponse;

import android.os.Parcel;
import android.os.Parcelable;

public class FbLoginResponse implements Parcelable {
    private String emailId;
    private String fbId;
    private String firstName;

    private String lastName;
    private String imgUrl;
    private String token;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private String phoneNumber;


    public FbLoginResponse() {

    }


    private FbLoginResponse(Parcel in) {
        emailId = in.readString();
        fbId = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        imgUrl = in.readString();
        token = in.readString();
        phoneNumber = in.readString();
    }

    public static final Creator<FbLoginResponse> CREATOR = new Creator<FbLoginResponse>() {
        @Override
        public FbLoginResponse createFromParcel(Parcel in) {
            return new FbLoginResponse(in);
        }

        @Override
        public FbLoginResponse[] newArray(int size) {
            return new FbLoginResponse[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emailId);
        dest.writeString(fbId);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(imgUrl);
        dest.writeString(token);
        dest.writeString(phoneNumber);
    }
}
