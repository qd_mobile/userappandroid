
package com.qd.user.model.info.vendor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Settlement {

    @SerializedName("noOfDays")
    @Expose
    private Integer noOfDays;
    @SerializedName("amount")
    @Expose
    private Integer amount;

    public Integer getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(Integer noOfDays) {
        this.noOfDays = noOfDays;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
