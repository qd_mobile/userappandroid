
package com.qd.user.model.createorder.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

class Item_ {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("itemPrice")
    @Expose
    private Long itemPrice;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("itemImage")
    @Expose
    private ArrayList<String> itemImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Long getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Long itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ArrayList<String> getItemImage() {
        return itemImage;
    }

    public void setItemImage(ArrayList<String> itemImage) {
        this.itemImage = itemImage;
    }

}
