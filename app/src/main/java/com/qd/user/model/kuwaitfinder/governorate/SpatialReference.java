
package com.qd.user.model.kuwaitfinder.governorate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpatialReference {

    @SerializedName("wkid")
    @Expose
    private Long wkid;
    @SerializedName("latestWkid")
    @Expose
    private Long latestWkid;

    public Long getWkid() {
        return wkid;
    }

    public void setWkid(Long wkid) {
        this.wkid = wkid;
    }

    public Long getLatestWkid() {
        return latestWkid;
    }

    public void setLatestWkid(Long latestWkid) {
        this.latestWkid = latestWkid;
    }

}
