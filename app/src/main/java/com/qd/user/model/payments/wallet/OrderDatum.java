
package com.qd.user.model.payments.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDatum {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("orderUniqueId")
    @Expose
    private String orderUniqueId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

}
