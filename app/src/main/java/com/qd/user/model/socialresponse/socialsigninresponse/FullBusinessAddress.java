
package com.qd.user.model.socialresponse.socialsigninresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FullBusinessAddress {

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("governorate")
    @Expose
    private String governorate;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("blockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("avenue")
    @Expose
    private String avenue;
    @SerializedName("houseOrbuilding")
    @Expose
    private String houseOrbuilding;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("apartmentOrOffice")
    @Expose
    private String apartmentOrOffice;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("lastUpdated")
    @Expose
    private String lastUpdated;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getHouseOrbuilding() {
        return houseOrbuilding;
    }

    public void setHouseOrbuilding(String houseOrbuilding) {
        this.houseOrbuilding = houseOrbuilding;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentOrOffice() {
        return apartmentOrOffice;
    }

    public void setApartmentOrOffice(String apartmentOrOffice) {
        this.apartmentOrOffice = apartmentOrOffice;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}
