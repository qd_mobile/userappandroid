
package com.qd.user.model.createaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class SessionLog {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("loginTime")
    @Expose
    private String loginTime;
    @SerializedName("logoutTime")
    @Expose
    private Object logoutTime;
    @SerializedName("loginStatus")
    @Expose
    private String loginStatus;
    @SerializedName("deviceToken")
    @Expose
    private Object deviceToken;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("arnToken")
    @Expose
    private String arnToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public Object getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Object logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public Object getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(Object deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getArnToken() {
        return arnToken;
    }

    public void setArnToken(String arnToken) {
        this.arnToken = arnToken;
    }

}
