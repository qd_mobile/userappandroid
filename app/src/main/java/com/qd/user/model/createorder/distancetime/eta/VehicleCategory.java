
package com.qd.user.model.createorder.distancetime.eta;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleCategory implements Parcelable {

    public static final Creator<VehicleCategory> CREATOR = new Creator<VehicleCategory>() {
        @Override
        public VehicleCategory createFromParcel(Parcel in) {
            return new VehicleCategory(in);
        }

        @Override
        public VehicleCategory[] newArray(int size) {
            return new VehicleCategory[size];
        }
    };
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("iconLink")
    @Expose
    private IconLink iconLink;
    @SerializedName("isActiveForCashClient")
    @Expose
    private boolean isActiveForCashClient;
    @SerializedName("isActiveForVendor")
    @Expose
    private boolean isActiveForVendor;
    @SerializedName("isActive")
    @Expose
    private boolean isActive;
    @SerializedName("weightage")
    @Expose
    private int weightage;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("keyword")
    @Expose
    private String keyword;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    private boolean isSelected;
    @SerializedName("ETA")
    @Expose
    private String eta;
    @SerializedName("__v")
    @Expose
    private long v;

    private VehicleCategory(Parcel in) {
        id = in.readString();
        isActiveForCashClient = in.readByte() != 0;
        isActiveForVendor = in.readByte() != 0;
        isActive = in.readByte() != 0;
        weightage = in.readInt();
        categoryName = in.readString();
        keyword = in.readString();
        description = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        isSelected = in.readByte() != 0;
        eta = in.readString();
        v = in.readLong();
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IconLink getIconLink() {
        return iconLink;
    }

    public void setIconLink(IconLink iconLink) {
        this.iconLink = iconLink;
    }

    public boolean isIsActiveForCashClient() {
        return isActiveForCashClient;
    }

    public void setIsActiveForCashClient(boolean isActiveForCashClient) {
        this.isActiveForCashClient = isActiveForCashClient;
    }

    public boolean isIsActiveForVendor() {
        return isActiveForVendor;
    }

    public void setIsActiveForVendor(boolean isActiveForVendor) {
        this.isActiveForVendor = isActiveForVendor;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getWeightage() {
        return weightage;
    }

    public void setWeightage(int weightage) {
        this.weightage = weightage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getV() {
        return v;
    }

    public void setV(long v) {
        this.v = v;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeByte((byte) (isActiveForCashClient ? 1 : 0));
        dest.writeByte((byte) (isActiveForVendor ? 1 : 0));
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeInt(weightage);
        dest.writeString(categoryName);
        dest.writeString(keyword);
        dest.writeString(description);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(eta);
        dest.writeLong(v);
    }
}
