
package com.qd.user.model.ordercancellation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

class PickupLocation {

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;
    @SerializedName("isElementInprogress")
    @Expose
    private Boolean isElementInprogress;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("governorate")
    @Expose
    private String governorate;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("blockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("avenue")
    @Expose
    private String avenue;
    @SerializedName("houseOrbuilding")
    @Expose
    private String houseOrbuilding;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("apartmentOrOffice")
    @Expose
    private String apartmentOrOffice;
    @SerializedName("recipientName")
    @Expose
    private String recipientName;
    @SerializedName("recipientMobileNo")
    @Expose
    private String recipientMobileNo;
    @SerializedName("recipientEmail")
    @Expose
    private String recipientEmail;
    @SerializedName("recipientImage")
    @Expose
    private String recipientImage;
    @SerializedName("driverNote")
    @Expose
    private String driverNote;
    @SerializedName("pickupStatus")
    @Expose
    private String pickupStatus;
    @SerializedName("pickupDriverId")
    @Expose
    private String pickupDriverId;
    @SerializedName("pickupDriverName")
    @Expose
    private String pickupDriverName;
    @SerializedName("pickupDriverEmail")
    @Expose
    private String pickupDriverEmail;
    @SerializedName("pickupDriverMobileNo")
    @Expose
    private String pickupDriverMobileNo;
    @SerializedName("pickupDriverImage")
    @Expose
    private String pickupDriverImage;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("tax")
    @Expose
    private BigDecimal tax;
    @SerializedName("tip")
    @Expose
    private BigDecimal tip;
    @SerializedName("discount")
    @Expose
    private BigDecimal discount;
    @SerializedName("pickupInstructions")
    @Expose
    private String pickupInstructions;
    @SerializedName("pickupPictures")
    @Expose
    private List<Object> pickupPictures = null;
    @SerializedName("pickupCharges")
    @Expose
    private BigDecimal pickupCharges;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("item")
    @Expose
    private List<Item> item = null;
    @SerializedName("elementUniqueId")
    @Expose
    private String elementUniqueId;
    @SerializedName("driverTrackingStatus")
    @Expose
    private List<DriverTrackingstatus> driverTrackingStatus = null;
    @SerializedName("confirmationStatus")
    @Expose
    private List<Object> confirmationStatus = null;
    @SerializedName("isReturn")
    @Expose
    private Boolean isReturn;
    @SerializedName("isReturnConfirmed")
    @Expose
    private Boolean isReturnConfirmed;
    @SerializedName("pickupDriverVehicle")
    @Expose
    private String pickupDriverVehicle;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Boolean getIsElementInprogress() {
        return isElementInprogress;
    }

    public void setIsElementInprogress(Boolean isElementInprogress) {
        this.isElementInprogress = isElementInprogress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getHouseOrbuilding() {
        return houseOrbuilding;
    }

    public void setHouseOrbuilding(String houseOrbuilding) {
        this.houseOrbuilding = houseOrbuilding;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentOrOffice() {
        return apartmentOrOffice;
    }

    public void setApartmentOrOffice(String apartmentOrOffice) {
        this.apartmentOrOffice = apartmentOrOffice;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientMobileNo() {
        return recipientMobileNo;
    }

    public void setRecipientMobileNo(String recipientMobileNo) {
        this.recipientMobileNo = recipientMobileNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientImage() {
        return recipientImage;
    }

    public void setRecipientImage(String recipientImage) {
        this.recipientImage = recipientImage;
    }

    public String getDriverNote() {
        return driverNote;
    }

    public void setDriverNote(String driverNote) {
        this.driverNote = driverNote;
    }

    public String getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(String pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public String getPickupDriverId() {
        return pickupDriverId;
    }

    public void setPickupDriverId(String pickupDriverId) {
        this.pickupDriverId = pickupDriverId;
    }

    public String getPickupDriverName() {
        return pickupDriverName;
    }

    public void setPickupDriverName(String pickupDriverName) {
        this.pickupDriverName = pickupDriverName;
    }

    public String getPickupDriverEmail() {
        return pickupDriverEmail;
    }

    public void setPickupDriverEmail(String pickupDriverEmail) {
        this.pickupDriverEmail = pickupDriverEmail;
    }

    public String getPickupDriverMobileNo() {
        return pickupDriverMobileNo;
    }

    public void setPickupDriverMobileNo(String pickupDriverMobileNo) {
        this.pickupDriverMobileNo = pickupDriverMobileNo;
    }

    public String getPickupDriverImage() {
        return pickupDriverImage;
    }

    public void setPickupDriverImage(String pickupDriverImage) {
        this.pickupDriverImage = pickupDriverImage;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getPickupInstructions() {
        return pickupInstructions;
    }

    public void setPickupInstructions(String pickupInstructions) {
        this.pickupInstructions = pickupInstructions;
    }

    public List<Object> getPickupPictures() {
        return pickupPictures;
    }

    public void setPickupPictures(List<Object> pickupPictures) {
        this.pickupPictures = pickupPictures;
    }

    public BigDecimal getPickupCharges() {
        return pickupCharges;
    }

    public void setPickupCharges(BigDecimal pickupCharges) {
        this.pickupCharges = pickupCharges;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public String getElementUniqueId() {
        return elementUniqueId;
    }

    public void setElementUniqueId(String elementUniqueId) {
        this.elementUniqueId = elementUniqueId;
    }

    public List<DriverTrackingstatus> getDriverTrackingStatus() {
        return driverTrackingStatus;
    }

    public void setDriverTrackingStatus(List<DriverTrackingstatus> driverTrackingStatus) {
        this.driverTrackingStatus = driverTrackingStatus;
    }

    public List<Object> getConfirmationStatus() {
        return confirmationStatus;
    }

    public void setConfirmationStatus(List<Object> confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }

    public Boolean getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Boolean isReturn) {
        this.isReturn = isReturn;
    }

    public Boolean getIsReturnConfirmed() {
        return isReturnConfirmed;
    }

    public void setIsReturnConfirmed(Boolean isReturnConfirmed) {
        this.isReturnConfirmed = isReturnConfirmed;
    }

    public String getPickupDriverVehicle() {
        return pickupDriverVehicle;
    }

    public void setPickupDriverVehicle(String pickupDriverVehicle) {
        this.pickupDriverVehicle = pickupDriverVehicle;
    }

}
