
package com.qd.user.model.createaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("ccPersonal")
    @Expose
    private String ccPersonal;
    @SerializedName("personal")
    @Expose
    private String personal;

    public String getCcPersonal() {
        return ccPersonal;
    }

    public void setCcPersonal(String ccPersonal) {
        this.ccPersonal = ccPersonal;
    }

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

}
