
package com.qd.user.model.savedplaces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("savedLocation")
    @Expose
    private SavedLocation savedLocation;
    @SerializedName("isFavourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("cashClientId")
    @Expose
    private String cashClientId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SavedLocation getSavedLocation() {
        return savedLocation;
    }

    public void setSavedLocation(SavedLocation savedLocation) {
        this.savedLocation = savedLocation;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCashClientId() {
        return cashClientId;
    }

    public void setCashClientId(String cashClientId) {
        this.cashClientId = cashClientId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}
