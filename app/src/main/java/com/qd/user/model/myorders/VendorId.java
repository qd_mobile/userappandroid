package com.qd.user.model.myorders;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("company")
    @Expose
    private Company company;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

}
