
package com.qd.user.model.kuwaitfinder.neighbourhood;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Field {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("length")
    @Expose
    private Long length;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

}
