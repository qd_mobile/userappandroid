
package com.qd.user.model.createaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Settlement {

    @SerializedName("noOfDays")
    @Expose
    private Long noOfDays;
    @SerializedName("amount")
    @Expose
    private Long amount;

    public Long getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(Long noOfDays) {
        this.noOfDays = noOfDays;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

}
