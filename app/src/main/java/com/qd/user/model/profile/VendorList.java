
package com.qd.user.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class VendorList {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("company")
    @Expose
    private Company company;
    @SerializedName("fullBusinessAddress")
    @Expose
    private FullBusinessAddress fullBusinessAddress;
    @SerializedName("geometry")
    @Expose
    private Geometry_ geometry;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("businessType")
    @Expose
    private String businessType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("tempPassword")
    @Expose
    private String tempPassword;
    @SerializedName("companyProfile")
    @Expose
    private String companyProfile;
    @SerializedName("businessAddress")
    @Expose
    private String businessAddress;
    @SerializedName("vendorType")
    @Expose
    private String vendorType;
    @SerializedName("otpVerified")
    @Expose
    private boolean otpVerified;
    @SerializedName("isCashClient")
    @Expose
    private boolean isCashClient;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("cashClientCurrentStatus")
    @Expose
    private String cashClientCurrentStatus;
    @SerializedName("isDeleted")
    @Expose
    private boolean isDeleted;
    @SerializedName("isCashClientDeleted")
    @Expose
    private boolean isCashClientDeleted;
    @SerializedName("createdByAdmin")
    @Expose
    private boolean createdByAdmin;
    @SerializedName("signupType")
    @Expose
    private String signupType;
    @SerializedName("cashClientOrderCount")
    @Expose
    private long cashClientOrderCount;
    @SerializedName("vendorOrderCount")
    @Expose
    private long vendorOrderCount;
    @SerializedName("pendingAmountFromQd")
    @Expose
    private BigDecimal pendingAmountFromQd;
    @SerializedName("payableAmountToQd")
    @Expose
    private BigDecimal payableAmountToQd;
    @SerializedName("walletAmount")
    @Expose
    private BigDecimal walletAmount;
    @SerializedName("isBalancePending")
    @Expose
    private boolean isBalancePending;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cashClientStatus")
    @Expose
    private String cashClientStatus;
    @SerializedName("vendorUniqueId")
    @Expose
    private String vendorUniqueId;
    @SerializedName("savedLocation")
    @Expose
    private List<Object> savedLocation = null;
    @SerializedName("documents")
    @Expose
    private List<Object> documents = null;
    @SerializedName("sessionLogs")
    @Expose
    private List<SessionLog> sessionLogs = null;
    @SerializedName("pendingBalance")
    @Expose
    private List<Object> pendingBalance = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private long v;
    @SerializedName("approvalStatusDates")
    @Expose
    private ApprovalStatusDates approvalStatusDates;
    @SerializedName("contact")
    @Expose
    private Contact_ contact;
    @SerializedName("invoice")
    @Expose
    private boolean invoice;
    @SerializedName("paymentModes")
    @Expose
    private String paymentModes;
    @SerializedName("servicesOffered")
    @Expose
    private String servicesOffered;
    @SerializedName("settlement")
    @Expose
    private Settlement settlement;
    @SerializedName("subscription")
    @Expose
    private Subscription subscription;
    @SerializedName("vendorLogo")
    @Expose
    private String vendorLogo;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("cancellationCount")
    @Expose
    private long cancellationCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public FullBusinessAddress getFullBusinessAddress() {
        return fullBusinessAddress;
    }

    public void setFullBusinessAddress(FullBusinessAddress fullBusinessAddress) {
        this.fullBusinessAddress = fullBusinessAddress;
    }

    public Geometry_ getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry_ geometry) {
        this.geometry = geometry;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(String companyProfile) {
        this.companyProfile = companyProfile;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public boolean isOtpVerified() {
        return otpVerified;
    }

    public void setOtpVerified(boolean otpVerified) {
        this.otpVerified = otpVerified;
    }

    public boolean isIsCashClient() {
        return isCashClient;
    }

    public void setIsCashClient(boolean isCashClient) {
        this.isCashClient = isCashClient;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCashClientCurrentStatus() {
        return cashClientCurrentStatus;
    }

    public void setCashClientCurrentStatus(String cashClientCurrentStatus) {
        this.cashClientCurrentStatus = cashClientCurrentStatus;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean isIsCashClientDeleted() {
        return isCashClientDeleted;
    }

    public void setIsCashClientDeleted(boolean isCashClientDeleted) {
        this.isCashClientDeleted = isCashClientDeleted;
    }

    public boolean isCreatedByAdmin() {
        return createdByAdmin;
    }

    public void setCreatedByAdmin(boolean createdByAdmin) {
        this.createdByAdmin = createdByAdmin;
    }

    public String getSignupType() {
        return signupType;
    }

    public void setSignupType(String signupType) {
        this.signupType = signupType;
    }

    public long getCashClientOrderCount() {
        return cashClientOrderCount;
    }

    public void setCashClientOrderCount(long cashClientOrderCount) {
        this.cashClientOrderCount = cashClientOrderCount;
    }

    public long getVendorOrderCount() {
        return vendorOrderCount;
    }

    public void setVendorOrderCount(long vendorOrderCount) {
        this.vendorOrderCount = vendorOrderCount;
    }

    public BigDecimal getPendingAmountFromQd() {
        return pendingAmountFromQd;
    }

    public void setPendingAmountFromQd(BigDecimal pendingAmountFromQd) {
        this.pendingAmountFromQd = pendingAmountFromQd;
    }

    public BigDecimal getPayableAmountToQd() {
        return payableAmountToQd;
    }

    public void setPayableAmountToQd(BigDecimal payableAmountToQd) {
        this.payableAmountToQd = payableAmountToQd;
    }

    public BigDecimal getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(BigDecimal walletAmount) {
        this.walletAmount = walletAmount;
    }

    public boolean isIsBalancePending() {
        return isBalancePending;
    }

    public void setIsBalancePending(boolean isBalancePending) {
        this.isBalancePending = isBalancePending;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCashClientStatus() {
        return cashClientStatus;
    }

    public void setCashClientStatus(String cashClientStatus) {
        this.cashClientStatus = cashClientStatus;
    }

    public String getVendorUniqueId() {
        return vendorUniqueId;
    }

    public void setVendorUniqueId(String vendorUniqueId) {
        this.vendorUniqueId = vendorUniqueId;
    }

    public List<Object> getSavedLocation() {
        return savedLocation;
    }

    public void setSavedLocation(List<Object> savedLocation) {
        this.savedLocation = savedLocation;
    }

    public List<Object> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Object> documents) {
        this.documents = documents;
    }

    public List<SessionLog> getSessionLogs() {
        return sessionLogs;
    }

    public void setSessionLogs(List<SessionLog> sessionLogs) {
        this.sessionLogs = sessionLogs;
    }

    public List<Object> getPendingBalance() {
        return pendingBalance;
    }

    public void setPendingBalance(List<Object> pendingBalance) {
        this.pendingBalance = pendingBalance;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getV() {
        return v;
    }

    public void setV(long v) {
        this.v = v;
    }

    public ApprovalStatusDates getApprovalStatusDates() {
        return approvalStatusDates;
    }

    public void setApprovalStatusDates(ApprovalStatusDates approvalStatusDates) {
        this.approvalStatusDates = approvalStatusDates;
    }

    public Contact_ getContact() {
        return contact;
    }

    public void setContact(Contact_ contact) {
        this.contact = contact;
    }

    public boolean isInvoice() {
        return invoice;
    }

    public void setInvoice(boolean invoice) {
        this.invoice = invoice;
    }

    public String getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(String paymentModes) {
        this.paymentModes = paymentModes;
    }

    public String getServicesOffered() {
        return servicesOffered;
    }

    public void setServicesOffered(String servicesOffered) {
        this.servicesOffered = servicesOffered;
    }

    public Settlement getSettlement() {
        return settlement;
    }

    public void setSettlement(Settlement settlement) {
        this.settlement = settlement;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public String getVendorLogo() {
        return vendorLogo;
    }

    public void setVendorLogo(String vendorLogo) {
        this.vendorLogo = vendorLogo;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public long getCancellationCount() {
        return cancellationCount;
    }

    public void setCancellationCount(long cancellationCount) {
        this.cancellationCount = cancellationCount;
    }

}
