
package com.qd.user.model.vendor.vendorsignup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("contact")
    @Expose
    private Contact contact;
    @SerializedName("fullBusinessAddress")
    @Expose
    private FullBusinessAddress fullBusinessAddress;
    @SerializedName("geometry")
    @Expose
    private Geometry_ geometry;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("businessType")
    @Expose
    private String businessType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("tempPassword")
    @Expose
    private String tempPassword;
    @SerializedName("companyProfile")
    @Expose
    private String companyProfile;
    @SerializedName("businessAddress")
    @Expose
    private String businessAddress;
    @SerializedName("vendorType")
    @Expose
    private String vendorType;
    @SerializedName("isCashClient")
    @Expose
    private Boolean isCashClient;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("cashClientCurrentStatus")
    @Expose
    private String cashClientCurrentStatus;
    @SerializedName("paymentModes")
    @Expose
    private List<Object> paymentModes = null;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("isCashClientDeleted")
    @Expose
    private Boolean isCashClientDeleted;
    @SerializedName("createdByAdmin")
    @Expose
    private Boolean createdByAdmin;
    @SerializedName("signupType")
    @Expose
    private String signupType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cashClientStatus")
    @Expose
    private String cashClientStatus;
    @SerializedName("CashClientCustomerId")
    @Expose
    private String cashClientCustomerId;
    @SerializedName("vendorUniqueId")
    @Expose
    private String vendorUniqueId;
    @SerializedName("savedLocation")
    @Expose
    private List<Object> savedLocation = null;
    @SerializedName("documents")
    @Expose
    private List<Object> documents = null;
    @SerializedName("sessionLogs")
    @Expose
    private List<SessionLog> sessionLogs = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public FullBusinessAddress getFullBusinessAddress() {
        return fullBusinessAddress;
    }

    public void setFullBusinessAddress(FullBusinessAddress fullBusinessAddress) {
        this.fullBusinessAddress = fullBusinessAddress;
    }

    public Geometry_ getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry_ geometry) {
        this.geometry = geometry;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(String companyProfile) {
        this.companyProfile = companyProfile;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public Boolean getIsCashClient() {
        return isCashClient;
    }

    public void setIsCashClient(Boolean isCashClient) {
        this.isCashClient = isCashClient;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCashClientCurrentStatus() {
        return cashClientCurrentStatus;
    }

    public void setCashClientCurrentStatus(String cashClientCurrentStatus) {
        this.cashClientCurrentStatus = cashClientCurrentStatus;
    }

    public List<Object> getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(List<Object> paymentModes) {
        this.paymentModes = paymentModes;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsCashClientDeleted() {
        return isCashClientDeleted;
    }

    public void setIsCashClientDeleted(Boolean isCashClientDeleted) {
        this.isCashClientDeleted = isCashClientDeleted;
    }

    public Boolean getCreatedByAdmin() {
        return createdByAdmin;
    }

    public void setCreatedByAdmin(Boolean createdByAdmin) {
        this.createdByAdmin = createdByAdmin;
    }

    public String getSignupType() {
        return signupType;
    }

    public void setSignupType(String signupType) {
        this.signupType = signupType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCashClientStatus() {
        return cashClientStatus;
    }

    public void setCashClientStatus(String cashClientStatus) {
        this.cashClientStatus = cashClientStatus;
    }

    public String getCashClientCustomerId() {
        return cashClientCustomerId;
    }

    public void setCashClientCustomerId(String cashClientCustomerId) {
        this.cashClientCustomerId = cashClientCustomerId;
    }

    public String getVendorUniqueId() {
        return vendorUniqueId;
    }

    public void setVendorUniqueId(String vendorUniqueId) {
        this.vendorUniqueId = vendorUniqueId;
    }

    public List<Object> getSavedLocation() {
        return savedLocation;
    }

    public void setSavedLocation(List<Object> savedLocation) {
        this.savedLocation = savedLocation;
    }

    public List<Object> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Object> documents) {
        this.documents = documents;
    }

    public List<SessionLog> getSessionLogs() {
        return sessionLogs;
    }

    public void setSessionLogs(List<SessionLog> sessionLogs) {
        this.sessionLogs = sessionLogs;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}
