
package com.qd.user.model.kuwaitfinder.neighbourhood;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

    @SerializedName("objectid")
    @Expose
    private int objectid;
    @SerializedName("neighborhoodarabic")
    @Expose
    private String neighborhoodarabic;
    @SerializedName("neighborhoodenglish")
    @Expose
    private String neighborhoodenglish;
    @SerializedName("nhood_no")
    @Expose
    private int nhoodNo;
    @SerializedName("gov_no")
    @Expose
    private int govNo;
    @SerializedName("governoratearabic")
    @Expose
    private String governoratearabic;
    @SerializedName("governorateenglish")
    @Expose
    private String governorateenglish;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("centroid_x")
    @Expose
    private Double centroidX;
    @SerializedName("centroid_y")
    @Expose
    private Double centroidY;

    public int getObjectid() {
        return objectid;
    }

    public void setObjectid(int objectid) {
        this.objectid = objectid;
    }

    public String getNeighborhoodarabic() {
        return neighborhoodarabic;
    }

    public void setNeighborhoodarabic(String neighborhoodarabic) {
        this.neighborhoodarabic = neighborhoodarabic;
    }

    public String getNeighborhoodenglish() {
        return neighborhoodenglish;
    }

    public void setNeighborhoodenglish(String neighborhoodenglish) {
        this.neighborhoodenglish = neighborhoodenglish;
    }

    public int getNhoodNo() {
        return nhoodNo;
    }

    public void setNhoodNo(int nhoodNo) {
        this.nhoodNo = nhoodNo;
    }

    public int getGovNo() {
        return govNo;
    }

    public void setGovNo(int govNo) {
        this.govNo = govNo;
    }

    public String getGovernoratearabic() {
        return governoratearabic;
    }

    public void setGovernoratearabic(String governoratearabic) {
        this.governoratearabic = governoratearabic;
    }

    public String getGovernorateenglish() {
        return governorateenglish;
    }

    public void setGovernorateenglish(String governorateenglish) {
        this.governorateenglish = governorateenglish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getCentroidX() {
        return centroidX;
    }

    public void setCentroidX(Double centroidX) {
        this.centroidX = centroidX;
    }

    public Double getCentroidY() {
        return centroidY;
    }

    public void setCentroidY(Double centroidY) {
        this.centroidY = centroidY;
    }

}
