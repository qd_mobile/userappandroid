
package com.qd.user.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("vendorList")
    @Expose
    private VendorList vendorList;

    public VendorList getVendorList() {
        return vendorList;
    }

    public void setVendorList(VendorList vendorList) {
        this.vendorList = vendorList;
    }

}
