
package com.qd.user.model.createorder.balance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("cancellationCharge")
    @Expose
    private double cancellationCharge;
    @SerializedName("returnCharges")
    @Expose
    private double returnCharges;
    @SerializedName("itemCharges")
    @Expose
    private double itemCharges;
    @SerializedName("configurations")
    @Expose
    private Configurations configurations;

    public double getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(double cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }

    public double getReturnCharges() {
        return returnCharges;
    }

    public void setReturnCharges(double returnCharges) {
        this.returnCharges = returnCharges;
    }

    public Configurations getConfigurations() {
        return configurations;
    }

    public void setConfigurations(Configurations configurations) {
        this.configurations = configurations;
    }

    public double getItemCharges() {
        return itemCharges;
    }

    public void setItemCharges(double itemCharges) {
        this.itemCharges = itemCharges;
    }
}
