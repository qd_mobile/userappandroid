
package com.qd.user.model.payments.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class Result {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("total")
    @Expose
    private long total;
    @SerializedName("page")
    @Expose
    private long page;
    @SerializedName("limit")
    @Expose
    private long limit;
    @SerializedName("vendorWalletAmount")
    @Expose
    private BigDecimal vendorWalletAmount;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public BigDecimal getVendorWalletAmount() {
        return vendorWalletAmount;
    }

    public void setVendorWalletAmount(BigDecimal vendorWalletAmount) {
        this.vendorWalletAmount = vendorWalletAmount;
    }

}
