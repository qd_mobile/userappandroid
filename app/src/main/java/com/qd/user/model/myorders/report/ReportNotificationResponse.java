
package com.qd.user.model.myorders.report;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportNotificationResponse implements Parcelable {

    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("fromIssueReport")
    @Expose
    private long fromIssueReport;
    @SerializedName("scheduledStartTime")
    @Expose
    private String scheduledStartTime;
    @SerializedName("deliveryCharge")
    @Expose
    private double deliveryCharge;
    @SerializedName("dropLocation")
    @Expose
    private String dropLocation;
    @SerializedName("itemPrice")
    @Expose
    private double itemPrice;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("orderUniqueId")
    @Expose
    private String orderUniqueId;
    @SerializedName("pickupLocation")
    @Expose
    private String pickupLocation;
    @SerializedName("elementId")
    @Expose
    private String elementId;
    @SerializedName("cancellationCharges")
    @Expose
    private double cancellationCharges;
    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("returnCharges")
    @Expose
    private double returnCharges;


    public static final Creator<ReportNotificationResponse> CREATOR = new Creator<ReportNotificationResponse>() {
        @Override
        public ReportNotificationResponse createFromParcel(Parcel in) {
            return new ReportNotificationResponse(in);
        }

        @Override
        public ReportNotificationResponse[] newArray(int size) {
            return new ReportNotificationResponse[size];
        }
    };

    protected ReportNotificationResponse(Parcel in) {
        orderType = in.readString();
        reason = in.readString();
        orderId = in.readString();
        fromIssueReport = in.readLong();
        scheduledStartTime = in.readString();
        deliveryCharge = in.readDouble();
        dropLocation = in.readString();
        itemPrice = in.readDouble();
        id = in.readString();
        orderUniqueId = in.readString();
        pickupLocation = in.readString();
        elementId = in.readString();
        cancellationCharges = in.readDouble();
        driverId = in.readString();
        returnCharges = in.readDouble();
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getFromIssueReport() {
        return fromIssueReport;
    }

    public void setFromIssueReport(long fromIssueReport) {
        this.fromIssueReport = fromIssueReport;
    }

    public String getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(String scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(String dropLocation) {
        this.dropLocation = dropLocation;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public double getReturnCharges() {
        return returnCharges;
    }

    public void setReturnCharges(double returnCharges) {
        this.returnCharges = returnCharges;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public double getCancellationCharges() {
        return cancellationCharges;
    }

    public void setCancellationCharges(double cancellationCharges) {
        this.cancellationCharges = cancellationCharges;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderType);
        dest.writeString(reason);
        dest.writeString(orderId);
        dest.writeLong(fromIssueReport);
        dest.writeString(scheduledStartTime);
        dest.writeDouble(deliveryCharge);
        dest.writeString(dropLocation);
        dest.writeDouble(itemPrice);
        dest.writeString(id);
        dest.writeString(orderUniqueId);
        dest.writeString(pickupLocation);
        dest.writeString(elementId);
        dest.writeDouble(cancellationCharges);
        dest.writeString(driverId);
        dest.writeDouble(returnCharges);
    }
}
