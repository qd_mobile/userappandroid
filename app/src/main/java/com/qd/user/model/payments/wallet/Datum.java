
package com.qd.user.model.payments.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class Datum {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("transactionFrom")
    @Expose
    private String transactionFrom;
    @SerializedName("transactionFor")
    @Expose
    private String transactionFor;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("vendorId")
    @Expose
    private String vendorId;
    @SerializedName("walletAmountBeforeTransaction")
    @Expose
    private BigDecimal walletAmountBeforeTransaction;
    @SerializedName("walletAmountAfterTransaction")
    @Expose
    private BigDecimal walletAmountAfterTransaction;
    @SerializedName("transactionStatus")
    @Expose
    private String transactionStatus;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private BigDecimal v;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("orderData")
    @Expose
    private List<OrderDatum> orderData = null;
    @SerializedName("reponseFromKnet")
    @Expose
    private String reponseFromKnet;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTransactionFrom() {
        return transactionFrom;
    }

    public void setTransactionFrom(String transactionFrom) {
        this.transactionFrom = transactionFrom;
    }

    public String getTransactionFor() {
        return transactionFor;
    }

    public void setTransactionFor(String transactionFor) {
        this.transactionFor = transactionFor;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public BigDecimal getWalletAmountBeforeTransaction() {
        return walletAmountBeforeTransaction;
    }

    public void setWalletAmountBeforeTransaction(BigDecimal walletAmountBeforeTransaction) {
        this.walletAmountBeforeTransaction = walletAmountBeforeTransaction;
    }

    public BigDecimal getWalletAmountAfterTransaction() {
        return walletAmountAfterTransaction;
    }

    public void setWalletAmountAfterTransaction(BigDecimal walletAmountAfterTransaction) {
        this.walletAmountAfterTransaction = walletAmountAfterTransaction;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public BigDecimal getV() {
        return v;
    }

    public void setV(BigDecimal v) {
        this.v = v;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<OrderDatum> getOrderData() {
        return orderData;
    }

    public void setOrderData(List<OrderDatum> orderData) {
        this.orderData = orderData;
    }

    public String getReponseFromKnet() {
        return reponseFromKnet;
    }

    public void setReponseFromKnet(String reponseFromKnet) {
        this.reponseFromKnet = reponseFromKnet;
    }

}
