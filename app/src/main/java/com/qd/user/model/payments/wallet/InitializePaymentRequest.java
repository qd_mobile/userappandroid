package com.qd.user.model.payments.wallet;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class InitializePaymentRequest implements Parcelable {

    public static final Creator<InitializePaymentRequest> CREATOR = new Creator<InitializePaymentRequest>() {
        @Override
        public InitializePaymentRequest createFromParcel(Parcel in) {
            return new InitializePaymentRequest(in);
        }

        @Override
        public InitializePaymentRequest[] newArray(int size) {
            return new InitializePaymentRequest[size];
        }
    };
    @SerializedName("vendorId")
    @Expose
    private String vendorId;
    @SerializedName("transactionFor")
    @Expose
    private String transactionFor;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("invoiceId")
    @Expose
    private String invoiceId;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    private Double price;

    private InitializePaymentRequest(Parcel in) {
        vendorId = in.readString();
        transactionFor = in.readString();
        invoiceId = in.readString();
        orderId = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
    }

    public InitializePaymentRequest() {

    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getTransactionFor() {
        return transactionFor;
    }

    public void setTransactionFor(String transactionFor) {
        this.transactionFor = transactionFor;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(vendorId);
        dest.writeString(transactionFor);
        dest.writeString(invoiceId);
        dest.writeString(orderId);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(price);
        }
    }
}
