
package com.qd.user.model.kuwaitfinder.blocks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

    @SerializedName("objectid")
    @Expose
    private Long objectid;
    @SerializedName("nhood_no")
    @Expose
    private Long nhoodNo;
    @SerializedName("blockarabic")
    @Expose
    private String blockarabic;
    @SerializedName("neighborhoodarabic")
    @Expose
    private String neighborhoodarabic;
    @SerializedName("neighborhoodenglish")
    @Expose
    private String neighborhoodenglish;
    @SerializedName("governoratearabic")
    @Expose
    private String governoratearabic;
    @SerializedName("governorateenglish")
    @Expose
    private String governorateenglish;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("blockenglish")
    @Expose
    private String blockenglish;
    @SerializedName("centroid_x")
    @Expose
    private Double centroidX;
    @SerializedName("centroid_y")
    @Expose
    private Double centroidY;
    @SerializedName("blockid")
    @Expose
    private String blockid;

    public Long getObjectid() {
        return objectid;
    }

    public void setObjectid(Long objectid) {
        this.objectid = objectid;
    }

    public Long getNhoodNo() {
        return nhoodNo;
    }

    public void setNhoodNo(Long nhoodNo) {
        this.nhoodNo = nhoodNo;
    }

    public String getBlockarabic() {
        return blockarabic;
    }

    public void setBlockarabic(String blockarabic) {
        this.blockarabic = blockarabic;
    }

    public String getNeighborhoodarabic() {
        return neighborhoodarabic;
    }

    public void setNeighborhoodarabic(String neighborhoodarabic) {
        this.neighborhoodarabic = neighborhoodarabic;
    }

    public String getNeighborhoodenglish() {
        return neighborhoodenglish;
    }

    public void setNeighborhoodenglish(String neighborhoodenglish) {
        this.neighborhoodenglish = neighborhoodenglish;
    }

    public String getGovernoratearabic() {
        return governoratearabic;
    }

    public void setGovernoratearabic(String governoratearabic) {
        this.governoratearabic = governoratearabic;
    }

    public String getGovernorateenglish() {
        return governorateenglish;
    }

    public void setGovernorateenglish(String governorateenglish) {
        this.governorateenglish = governorateenglish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBlockenglish() {
        return blockenglish;
    }

    public void setBlockenglish(String blockenglish) {
        this.blockenglish = blockenglish;
    }

    public Double getCentroidX() {
        return centroidX;
    }

    public void setCentroidX(Double centroidX) {
        this.centroidX = centroidX;
    }

    public Double getCentroidY() {
        return centroidY;
    }

    public void setCentroidY(Double centroidY) {
        this.centroidY = centroidY;
    }

    public String getBlockid() {
        return blockid;
    }

    public void setBlockid(String blockid) {
        this.blockid = blockid;
    }

}
