
package com.qd.user.model.createorder.flexible;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("pickupLocation")
    @Expose
    private PickupLocation pickupLocation;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("isApproved")
    @Expose
    private boolean isApproved;
    @SerializedName("isBulk")
    @Expose
    private boolean isBulk;
    @SerializedName("totalRequestedDrops")
    @Expose
    private long totalRequestedDrops;
    @SerializedName("totalApprovedDrops")
    @Expose
    private long totalApprovedDrops;
    @SerializedName("totalDeliveredDrops")
    @Expose
    private long totalDeliveredDrops;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("image")
    @Expose
    private List<Object> image = null;
    @SerializedName("bulkOrderTriggerUniqueId")
    @Expose
    private String bulkOrderTriggerUniqueId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private long v;

    public PickupLocation getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(PickupLocation pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIsApproved() {
        return isApproved;
    }

    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
    }

    public boolean isIsBulk() {
        return isBulk;
    }

    public void setIsBulk(boolean isBulk) {
        this.isBulk = isBulk;
    }

    public long getTotalRequestedDrops() {
        return totalRequestedDrops;
    }

    public void setTotalRequestedDrops(long totalRequestedDrops) {
        this.totalRequestedDrops = totalRequestedDrops;
    }

    public long getTotalApprovedDrops() {
        return totalApprovedDrops;
    }

    public void setTotalApprovedDrops(long totalApprovedDrops) {
        this.totalApprovedDrops = totalApprovedDrops;
    }

    public long getTotalDeliveredDrops() {
        return totalDeliveredDrops;
    }

    public void setTotalDeliveredDrops(long totalDeliveredDrops) {
        this.totalDeliveredDrops = totalDeliveredDrops;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public List<Object> getImage() {
        return image;
    }

    public void setImage(List<Object> image) {
        this.image = image;
    }

    public String getBulkOrderTriggerUniqueId() {
        return bulkOrderTriggerUniqueId;
    }

    public void setBulkOrderTriggerUniqueId(String bulkOrderTriggerUniqueId) {
        this.bulkOrderTriggerUniqueId = bulkOrderTriggerUniqueId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getV() {
        return v;
    }

    public void setV(long v) {
        this.v = v;
    }

}
