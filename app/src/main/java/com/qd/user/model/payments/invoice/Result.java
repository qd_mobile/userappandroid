
package com.qd.user.model.payments.invoice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class Result {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("total")
    @Expose
    private Long total;
    @SerializedName("page")
    @Expose
    private Long page;
    @SerializedName("limit")
    @Expose
    private Long limit;
    @SerializedName("pendingAmountFromQd")
    @Expose
    private BigDecimal pendingAmountFromQd;
    @SerializedName("payableAmountToQd")
    @Expose
    private BigDecimal payableAmountToQd;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public BigDecimal getPendingAmountFromQd() {
        return pendingAmountFromQd;
    }

    public void setPendingAmountFromQd(BigDecimal pendingAmountFromQd) {
        this.pendingAmountFromQd = pendingAmountFromQd;
    }

    public BigDecimal getPayableAmountToQd() {
        return payableAmountToQd;
    }

    public void setPayableAmountToQd(BigDecimal payableAmountToQd) {
        this.payableAmountToQd = payableAmountToQd;
    }

}
