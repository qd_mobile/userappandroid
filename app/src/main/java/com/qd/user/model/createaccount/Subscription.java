
package com.qd.user.model.createaccount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Subscription {

    @SerializedName("amount")
    @Expose
    private Object amount;
    @SerializedName("flag")
    @Expose
    private Boolean flag;

    public Object getAmount() {
        return amount;
    }

    public void setAmount(Object amount) {
        this.amount = amount;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

}
