
package com.qd.user.model.createorder.savedaddress;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("savedLocation")
    @Expose
    private SavedLocation savedLocation;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("isFavourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("isAlreadySelected")
    @Expose
    private Boolean isAlreadySelected;
    @SerializedName("_id")
    @Expose
    private String id;

    public Result() {

    }

    private Result(Parcel in) {
        createdAt = in.readString();
        byte tmpIsFavourite = in.readByte();
        isFavourite = tmpIsFavourite == 0 ? null : tmpIsFavourite == 1;
        byte tmpIsAlreadySelected = in.readByte();
        isAlreadySelected = tmpIsAlreadySelected == 0 ? null : tmpIsAlreadySelected == 1;
        id = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public Boolean getAlreadySelected() {
        return isAlreadySelected;
    }

    public void setAlreadySelected(Boolean alreadySelected) {
        isAlreadySelected = alreadySelected;
    }

    public SavedLocation getSavedLocation() {
        return savedLocation;
    }

    public void setSavedLocation(SavedLocation savedLocation) {
        this.savedLocation = savedLocation;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(createdAt);
        dest.writeByte((byte) (isFavourite == null ? 0 : isFavourite ? 1 : 2));
        dest.writeByte((byte) (isAlreadySelected == null ? 0 : isAlreadySelected ? 1 : 2));
        dest.writeString(id);
    }
}
