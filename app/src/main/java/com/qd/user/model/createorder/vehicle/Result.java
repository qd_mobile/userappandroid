
package com.qd.user.model.createorder.vehicle;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("iconLink")
    @Expose
    private IconLink iconLink;
    @SerializedName("isActiveForCashClient")
    @Expose
    private Boolean isActiveForCashClient;
    @SerializedName("isActiveForVendor")
    @Expose
    private Boolean isActiveForVendor;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("keyword")
    @Expose
    private String keyword;

    private Result(Parcel in) {
        id = in.readString();
        byte tmpIsActiveForCashClient = in.readByte();
        isActiveForCashClient = tmpIsActiveForCashClient == 0 ? null : tmpIsActiveForCashClient == 1;
        byte tmpIsActiveForVendor = in.readByte();
        isActiveForVendor = tmpIsActiveForVendor == 0 ? null : tmpIsActiveForVendor == 1;
        byte tmpIsActive = in.readByte();
        isActive = tmpIsActive == 0 ? null : tmpIsActive == 1;
        categoryName = in.readString();
        keyword = in.readString();
        weightage = in.readInt();
        isSelected = in.readByte() != 0;
        description = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            v = null;
        } else {
            v = in.readLong();
        }
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public int getWeightage() {
        return weightage;
    }

    public void setWeightage(int weightage) {
        this.weightage = weightage;
    }

    @SerializedName("weightage")
    @Expose
    private int weightage;



    private boolean isSelected;


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Long v;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IconLink getIconLink() {
        return iconLink;
    }

    public void setIconLink(IconLink iconLink) {
        this.iconLink = iconLink;
    }

    public Boolean getIsActiveForCashClient() {
        return isActiveForCashClient;
    }

    public void setIsActiveForCashClient(Boolean isActiveForCashClient) {
        this.isActiveForCashClient = isActiveForCashClient;
    }

    public Boolean getIsActiveForVendor() {
        return isActiveForVendor;
    }

    public void setIsActiveForVendor(Boolean isActiveForVendor) {
        this.isActiveForVendor = isActiveForVendor;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getV() {
        return v;
    }

    public void setV(Long v) {
        this.v = v;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeByte((byte) (isActiveForCashClient == null ? 0 : isActiveForCashClient ? 1 : 2));
        dest.writeByte((byte) (isActiveForVendor == null ? 0 : isActiveForVendor ? 1 : 2));
        dest.writeByte((byte) (isActive == null ? 0 : isActive ? 1 : 2));
        dest.writeString(categoryName);
        dest.writeString(keyword);
        dest.writeInt(weightage);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(description);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        if (v == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(v);
        }
    }
}
