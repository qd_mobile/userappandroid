
package com.qd.user.model.kuwaitfinder.geocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes implements Comparable<Attributes> {

    @SerializedName("objectid")
    @Expose
    private Integer objectid;
    @SerializedName("blockarabic")
    @Expose
    private String blockarabic;
    @SerializedName("housearabic")
    @Expose
    private String housearabic;
    @SerializedName("parcelarabic")
    @Expose
    private String parcelarabic;
    @SerializedName("civilid")
    @Expose
    private Integer civilid;
    @SerializedName("streetenglish")
    @Expose
    private String streetenglish;
    @SerializedName("streetarabic")
    @Expose
    private String streetarabic;
    @SerializedName("parcelenglish")
    @Expose
    private String parcelenglish;
    @SerializedName("houseenglish")
    @Expose
    private String houseenglish;
    @SerializedName("neighborhoodarabic")
    @Expose
    private String neighborhoodarabic;
    @SerializedName("neighborhoodenglish")
    @Expose
    private String neighborhoodenglish;
    @SerializedName("governoratearabic")
    @Expose
    private String governoratearabic;
    @SerializedName("governorateenglish")
    @Expose
    private String governorateenglish;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("blockenglish")
    @Expose
    private String blockenglish;
    @SerializedName("unit_no")
    @Expose
    private String unitNo;
    @SerializedName("floor_no")
    @Expose
    private String floorNo;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("buildingnamearabic")
    @Expose
    private Object buildingnamearabic;
    @SerializedName("buildingnameenglish")
    @Expose
    private Object buildingnameenglish;
    @SerializedName("buildingtypearabic")
    @Expose
    private Object buildingtypearabic;
    @SerializedName("buildingktypeenglish")
    @Expose
    private Object buildingktypeenglish;
    @SerializedName("neighborhoodid")
    @Expose
    private Integer neighborhoodid;
    @SerializedName("governorateid")
    @Expose
    private Integer governorateid;
    @SerializedName("ktm_x")
    @Expose
    private Double ktmX;
    @SerializedName("ktm_y")
    @Expose
    private Double ktmY;
    @SerializedName("utm_x")
    @Expose
    private Double utmX;
    @SerializedName("utm_y")
    @Expose
    private Double utmY;
    @SerializedName("wsphere_x")
    @Expose
    private Double wsphereX;
    @SerializedName("wsphere_y")
    @Expose
    private Double wsphereY;

    public Integer getObjectid() {
        return objectid;
    }

    public void setObjectid(Integer objectid) {
        this.objectid = objectid;
    }

    public String getBlockarabic() {
        return blockarabic;
    }

    public void setBlockarabic(String blockarabic) {
        this.blockarabic = blockarabic;
    }

    public String getHousearabic() {
        return housearabic;
    }

    public void setHousearabic(String housearabic) {
        this.housearabic = housearabic;
    }

    public String getParcelarabic() {
        return parcelarabic;
    }

    public void setParcelarabic(String parcelarabic) {
        this.parcelarabic = parcelarabic;
    }

    public Integer getCivilid() {
        return civilid;
    }

    public void setCivilid(Integer civilid) {
        this.civilid = civilid;
    }

    public String getStreetenglish() {
        return streetenglish;
    }

    public void setStreetenglish(String streetenglish) {
        this.streetenglish = streetenglish;
    }

    public String getStreetarabic() {
        return streetarabic;
    }

    public void setStreetarabic(String streetarabic) {
        this.streetarabic = streetarabic;
    }

    public String getParcelenglish() {
        return parcelenglish;
    }

    public void setParcelenglish(String parcelenglish) {
        this.parcelenglish = parcelenglish;
    }

    public String getHouseenglish() {
        return houseenglish;
    }

    public void setHouseenglish(String houseenglish) {
        this.houseenglish = houseenglish;
    }

    public String getNeighborhoodarabic() {
        return neighborhoodarabic;
    }

    public void setNeighborhoodarabic(String neighborhoodarabic) {
        this.neighborhoodarabic = neighborhoodarabic;
    }

    public String getNeighborhoodenglish() {
        return neighborhoodenglish;
    }

    public void setNeighborhoodenglish(String neighborhoodenglish) {
        this.neighborhoodenglish = neighborhoodenglish;
    }

    public String getGovernoratearabic() {
        return governoratearabic;
    }

    public void setGovernoratearabic(String governoratearabic) {
        this.governoratearabic = governoratearabic;
    }

    public String getGovernorateenglish() {
        return governorateenglish;
    }

    public void setGovernorateenglish(String governorateenglish) {
        this.governorateenglish = governorateenglish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBlockenglish() {
        return blockenglish;
    }

    public void setBlockenglish(String blockenglish) {
        this.blockenglish = blockenglish;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Object getBuildingnamearabic() {
        return buildingnamearabic;
    }

    public void setBuildingnamearabic(Object buildingnamearabic) {
        this.buildingnamearabic = buildingnamearabic;
    }

    public Object getBuildingnameenglish() {
        return buildingnameenglish;
    }

    public void setBuildingnameenglish(Object buildingnameenglish) {
        this.buildingnameenglish = buildingnameenglish;
    }

    public Object getBuildingtypearabic() {
        return buildingtypearabic;
    }

    public void setBuildingtypearabic(Object buildingtypearabic) {
        this.buildingtypearabic = buildingtypearabic;
    }

    public Object getBuildingktypeenglish() {
        return buildingktypeenglish;
    }

    public void setBuildingktypeenglish(Object buildingktypeenglish) {
        this.buildingktypeenglish = buildingktypeenglish;
    }

    public Integer getNeighborhoodid() {
        return neighborhoodid;
    }

    public void setNeighborhoodid(Integer neighborhoodid) {
        this.neighborhoodid = neighborhoodid;
    }

    public Integer getGovernorateid() {
        return governorateid;
    }

    public void setGovernorateid(Integer governorateid) {
        this.governorateid = governorateid;
    }

    public Double getKtmX() {
        return ktmX;
    }

    public void setKtmX(Double ktmX) {
        this.ktmX = ktmX;
    }

    public Double getKtmY() {
        return ktmY;
    }

    public void setKtmY(Double ktmY) {
        this.ktmY = ktmY;
    }

    public Double getUtmX() {
        return utmX;
    }

    public void setUtmX(Double utmX) {
        this.utmX = utmX;
    }

    public Double getUtmY() {
        return utmY;
    }

    public void setUtmY(Double utmY) {
        this.utmY = utmY;
    }

    public Double getWsphereX() {
        return wsphereX;
    }

    public void setWsphereX(Double wsphereX) {
        this.wsphereX = wsphereX;
    }

    public Double getWsphereY() {
        return wsphereY;
    }

    public void setWsphereY(Double wsphereY) {
        this.wsphereY = wsphereY;
    }

    @Override
    public int compareTo(Attributes o) {
        if (houseenglish.compareTo(o.houseenglish) == 0 && parcelenglish.compareTo(o.parcelenglish) == 0) {
            return 0;
        } else return -1;
    }

}
