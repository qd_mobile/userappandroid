
package com.qd.user.model.createorder.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateOrderRequest implements Parcelable {

    @SerializedName("requestFrom")
    @Expose
    private String requestFrom;
    @SerializedName("requestFor")
    @Expose
    private String requestFor;
    @SerializedName("ordererId")
    @Expose
    private String ordererId;
    @SerializedName("deliveryType")
    @Expose
    private String deliveryType;
    @SerializedName("vehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("pickupInstruction")
    @Expose
    private String pickupInstruction;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("isScheduled")
    @Expose
    private Boolean isScheduled;
    @SerializedName("islater")
    @Expose
    private Boolean islater;
    @SerializedName("paymentMode")
    @Expose
    private String paymentType;
    private String eta;
    @SerializedName("promoCodeId")
    @Expose
    private String promoCodeId;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("timeSlotId")
    @Expose
    private String timeSlotId;
    @SerializedName("scheduledStartTime")
    @Expose
    private String scheduledStartTime;
    @SerializedName("pickupLocation")
    @Expose
    private List<PickupLocation> pickupLocation = null;
    @SerializedName("dropOffArr")
    @Expose
    private List<DropOffArr> dropOffArr = null;
    @SerializedName("promoCode")
    @Expose
    private String promoCode;
    public static final Creator<CreateOrderRequest> CREATOR = new Creator<CreateOrderRequest>() {
        @Override
        public CreateOrderRequest createFromParcel(Parcel in) {
            return new CreateOrderRequest(in);
        }

        @Override
        public CreateOrderRequest[] newArray(int size) {
            return new CreateOrderRequest[size];
        }
    };
    @SerializedName("deliveryBySender")
    @Expose
    private boolean deliveryBySender;
    @SerializedName("itemDefault")
    @Expose
    private String itemDefault;

    public CreateOrderRequest() {

    }

    private CreateOrderRequest(Parcel in) {
        requestFrom = in.readString();
        requestFor = in.readString();
        ordererId = in.readString();
        deliveryType = in.readString();
        vehicleType = in.readString();
        pickupInstruction = in.readString();
        serviceType = in.readString();
        orderType = in.readString();
        byte tmpIsScheduled = in.readByte();
        isScheduled = tmpIsScheduled == 0 ? null : tmpIsScheduled == 1;
        byte tmpIslater = in.readByte();
        islater = tmpIslater == 0 ? null : tmpIslater == 1;
        paymentType = in.readString();
        eta = in.readString();
        promoCodeId = in.readString();
        platform = in.readString();
        timeSlotId = in.readString();
        scheduledStartTime = in.readString();
        pickupLocation = in.createTypedArrayList(PickupLocation.CREATOR);
        dropOffArr = in.createTypedArrayList(DropOffArr.CREATOR);
        promoCode = in.readString();
        itemDefault = in.readString();
        deliveryBySender = in.readByte() != 0;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public boolean getDeliveryBySender() {
        return deliveryBySender;
    }

    public void setDeliveryBySender(boolean deliveryBySender) {
        this.deliveryBySender = deliveryBySender;
    }

    public String getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(String promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String getRequestFor() {
        return requestFor;
    }

    public void setRequestFor(String requestFor) {
        this.requestFor = requestFor;
    }

    public String getOrdererId() {
        return ordererId;
    }

    public void setOrdererId(String ordererId) {
        this.ordererId = ordererId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getPickupInstruction() {
        return pickupInstruction;
    }

    public void setPickupInstruction(String pickupInstruction) {
        this.pickupInstruction = pickupInstruction;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Boolean getIsScheduled() {
        return isScheduled;
    }

    public void setIsScheduled(Boolean isScheduled) {
        this.isScheduled = isScheduled;
    }

    public Boolean getIslater() {
        return islater;
    }

    public void setIslater(Boolean islater) {
        this.islater = islater;
    }

    public String getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(String timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public String getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(String scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public List<PickupLocation> getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(List<PickupLocation> pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public List<DropOffArr> getDropOffArr() {
        return dropOffArr;
    }

    public void setDropOffArr(List<DropOffArr> dropOffArr) {
        this.dropOffArr = dropOffArr;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getItemDefault() {
        return itemDefault;
    }

    public void setItemDefault(String itemDefault) {
        this.itemDefault = itemDefault;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(requestFrom);
        parcel.writeString(requestFor);
        parcel.writeString(ordererId);
        parcel.writeString(deliveryType);
        parcel.writeString(vehicleType);
        parcel.writeString(pickupInstruction);
        parcel.writeString(serviceType);
        parcel.writeString(orderType);
        parcel.writeByte((byte) (isScheduled == null ? 0 : isScheduled ? 1 : 2));
        parcel.writeByte((byte) (islater == null ? 0 : islater ? 1 : 2));
        parcel.writeString(paymentType);
        parcel.writeString(eta);
        parcel.writeString(promoCodeId);
        parcel.writeString(platform);
        parcel.writeString(timeSlotId);
        parcel.writeString(scheduledStartTime);
        parcel.writeTypedList(pickupLocation);
        parcel.writeTypedList(dropOffArr);
        parcel.writeString(promoCode);
        parcel.writeString(itemDefault);
        parcel.writeByte((byte) (deliveryBySender ? 1 : 0));
    }
}
