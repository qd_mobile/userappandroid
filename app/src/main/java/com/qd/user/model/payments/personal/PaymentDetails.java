
package com.qd.user.model.payments.personal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class PaymentDetails {

    @SerializedName("deliveryCharge")
    @Expose
    private BigDecimal deliveryCharge;
    @SerializedName("returnCharges")
    @Expose
    private BigDecimal returnCharges;
    @SerializedName("cancellationCharges")
    @Expose
    private BigDecimal cancellationCharges;
    @SerializedName("isPromoDiscountPercentage")
    @Expose
    private Boolean isPromoDiscountPercentage;
    @SerializedName("isPromocode")
    @Expose
    private Boolean isPromoCode;
    @SerializedName("promoPercentage")
    @Expose
    private int promoPercentage;
    @SerializedName("promoAppliedAmount")
    @Expose
    private BigDecimal promoAppliedAmount;
    @SerializedName("previousItemCharges")
    @Expose
    private BigDecimal previousItemCharges;
    @SerializedName("grandTotalAfterDiscount")
    @Expose
    private BigDecimal grandTotalAfterDiscount;
    @SerializedName("deliveryChargeAfterDiscount")
    @Expose
    private BigDecimal deliveryChargeAfterDiscount;
    @SerializedName("previousCancellationCharges")
    @Expose
    private BigDecimal previousCancellationCharges;
    @SerializedName("previousReturnCharges")
    @Expose
    private BigDecimal previousReturnCharges;
    @SerializedName("grandTotal")
    @Expose
    private BigDecimal grandTotal;
    @SerializedName("totalItemCost")
    @Expose
    private BigDecimal totalItemCost;
    @SerializedName("totalItemCostCod")
    @Expose
    private BigDecimal totalItemCostCod;

    public BigDecimal getPreviousCancellationCharges() {
        return previousCancellationCharges;
    }

    public void setPreviousCancellationCharges(BigDecimal previousCancellationCharges) {
        this.previousCancellationCharges = previousCancellationCharges;
    }
    @SerializedName("totalCashOnDelivery")
    @Expose
    private BigDecimal totalCashOnDelivery;
    @SerializedName("totalOnlineAmount")
    @Expose
    private BigDecimal totalOnlineAmount;
    @SerializedName("totalAmountPaid")
    @Expose
    private BigDecimal totalAmountPaid;

    public BigDecimal getPreviousReturnCharges() {
        return previousReturnCharges;
    }

    public void setPreviousReturnCharges(BigDecimal previousReturnCharges) {
        this.previousReturnCharges = previousReturnCharges;
    }

    public BigDecimal getTotalItemCost() {
        return totalItemCost;
    }

    public void setTotalItemCost(BigDecimal totalItemCost) {
        this.totalItemCost = totalItemCost;
    }

    public BigDecimal getDeliveryChargeAfterDiscount() {
        return deliveryChargeAfterDiscount;
    }
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;

    public void setDeliveryChargeAfterDiscount(BigDecimal deliveryChargeAfterDiscount) {
        this.deliveryChargeAfterDiscount = deliveryChargeAfterDiscount;
    }

    public BigDecimal getGrandTotalAfterDiscount() {
        return grandTotalAfterDiscount;
    }

    public void setGrandTotalAfterDiscount(BigDecimal grandTotalAfterDiscount) {
        this.grandTotalAfterDiscount = grandTotalAfterDiscount;
    }

    public BigDecimal getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(BigDecimal deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public BigDecimal getReturnCharges() {
        return returnCharges;
    }

    public void setReturnCharges(BigDecimal returnCharges) {
        this.returnCharges = returnCharges;
    }

    public BigDecimal getCancellationCharges() {
        return cancellationCharges;
    }

    public void setCancellationCharges(BigDecimal cancellationCharges) {
        this.cancellationCharges = cancellationCharges;
    }

    public Boolean getIsPromoDiscoutPercentage() {
        return isPromoDiscountPercentage;
    }

    public void setIsPromoDiscoutPercentage(Boolean isPromoDiscountPercentage) {
        this.isPromoDiscountPercentage = isPromoDiscountPercentage;
    }

    public int getPromoPercentage() {
        return promoPercentage;
    }

    public void setPromoPercentage(int promoPercentage) {
        this.promoPercentage = promoPercentage;
    }

    public BigDecimal getPromoAppliedAmount() {
        return promoAppliedAmount;
    }

    public void setPromoAppliedAmount(BigDecimal promoAppliedAmount) {
        this.promoAppliedAmount = promoAppliedAmount;
    }

    public BigDecimal getTotalCashOnDelivery() {
        return totalCashOnDelivery;
    }

    public void setTotalCashOnDelivery(BigDecimal totalCashOnDelivery) {
        this.totalCashOnDelivery = totalCashOnDelivery;
    }

    public BigDecimal getTotalOnlineAmount() {
        return totalOnlineAmount;
    }

    public void setTotalOnlineAmount(BigDecimal totalOnlineAmount) {
        this.totalOnlineAmount = totalOnlineAmount;
    }

    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Boolean getPromoCode() {
        return isPromoCode;
    }

    public void setPromoCode(Boolean promoCode) {
        isPromoCode = promoCode;
    }

    public BigDecimal getTotalItemCostCod() {
        return totalItemCostCod;
    }

    public void setTotalItemCostCod(BigDecimal totalItemCostCod) {
        this.totalItemCostCod = totalItemCostCod;
    }

    public BigDecimal getPreviousItemCharges() {
        return previousItemCharges;
    }

    public void setPreviousItemCharges(BigDecimal previousItemCharges) {
        this.previousItemCharges = previousItemCharges;
    }
}
