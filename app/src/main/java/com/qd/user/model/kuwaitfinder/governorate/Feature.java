
package com.qd.user.model.kuwaitfinder.governorate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feature implements Comparable<Feature> {

    @SerializedName("attributes")
    @Expose
    private Attributes attributes;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public int compareTo(Feature o) {
        return 0;
    }
}
