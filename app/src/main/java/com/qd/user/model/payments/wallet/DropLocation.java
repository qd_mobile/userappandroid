
package com.qd.user.model.payments.wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class DropLocation {

    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

}
