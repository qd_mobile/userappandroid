package com.qd.user.model.validationerror;

public class ValidationErrors {
    private String errorMessage;
    private int errorCode;
    private int validationMessage;


    public ValidationErrors(int errorCode,String errorMessage) {
        this.errorMessage = errorMessage;
        this.errorCode=errorCode;
    }

    public ValidationErrors(int errorCode, int validationMessage) {
        this.validationMessage = validationMessage;
        this.errorCode = errorCode;
    }



    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getValidationMessage() {
        return validationMessage;
    }

    public void setValidationMessage(int validationMessage) {
        this.validationMessage = validationMessage;
    }
}
