
package com.qd.user.model.orderdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class Item {

    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("itemPrice")
    @Expose
    private BigDecimal itemPrice;
    @SerializedName("itemDescription")
    @Expose
    private String itemDescription;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("itemImage")
    @Expose
    private List<String> itemImage;

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getItemImage() {
        return itemImage;
    }

    public void setItemImage(List<String> itemImage) {
        this.itemImage = itemImage;
    }

}
