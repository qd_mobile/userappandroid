
package com.qd.user.model.ordercancellation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

class DropLocation {

    @SerializedName("geometry")
    @Expose
    private Geometry_ geometry;
    @SerializedName("isElementInprogress")
    @Expose
    private Boolean isElementInprogress;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("governorate")
    @Expose
    private String governorate;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("blockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("avenue")
    @Expose
    private String avenue;
    @SerializedName("houseOrbuilding")
    @Expose
    private String houseOrbuilding;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("apartmentOrOffice")
    @Expose
    private String apartmentOrOffice;
    @SerializedName("recipientName")
    @Expose
    private String recipientName;
    @SerializedName("recipientMobileNo")
    @Expose
    private String recipientMobileNo;
    @SerializedName("recipientEmail")
    @Expose
    private String recipientEmail;
    @SerializedName("recipientImage")
    @Expose
    private String recipientImage;
    @SerializedName("driverNote")
    @Expose
    private String driverNote;
    @SerializedName("deliveryStatus")
    @Expose
    private String deliveryStatus;
    @SerializedName("deliveryDriverId")
    @Expose
    private String deliveryDriverId;
    @SerializedName("deliveryDriverName")
    @Expose
    private String deliveryDriverName;
    @SerializedName("deliveryDriverEmail")
    @Expose
    private String deliveryDriverEmail;
    @SerializedName("deliveryDriverMobileNo")
    @Expose
    private String deliveryDriverMobileNo;
    @SerializedName("deliveryDriverImage")
    @Expose
    private String deliveryDriverImage;
    @SerializedName("isCancelled")
    @Expose
    private Boolean isCancelled;
    @SerializedName("isReturn")
    @Expose
    private Boolean isReturn;
    @SerializedName("isReturnConfirmed")
    @Expose
    private Boolean isReturnConfirmed;
    @SerializedName("isReturnReceived")
    @Expose
    private Boolean isReturnReceived;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("tax")
    @Expose
    private BigDecimal tax;
    @SerializedName("tip")
    @Expose
    private BigDecimal tip;
    @SerializedName("discount")
    @Expose
    private BigDecimal discount;
    @SerializedName("dropInstructions")
    @Expose
    private String dropInstructions;
    @SerializedName("deliveryPictures")
    @Expose
    private List<Object> deliveryPictures = null;
    @SerializedName("deliveryCharge")
    @Expose
    private BigDecimal deliveryCharges;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("item")
    @Expose
    private List<Item_> item = null;
    @SerializedName("elementUniqueId")
    @Expose
    private String elementUniqueId;
    @SerializedName("driverTrackingStatus")
    @Expose
    private List<DriverTrackingstatus_> driverTrackingStatus = null;
    @SerializedName("confirmationStatus")
    @Expose
    private List<Object> confirmationStatus = null;
    @SerializedName("reasonForCancellation")
    @Expose
    private String reasonForCancellation;
    @SerializedName("digitalSignature")
    @Expose
    private String digitalSignature;
    @SerializedName("deliveryDriverVehicle")
    @Expose
    private String deliveryDriverVehicle;

    public Geometry_ getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry_ geometry) {
        this.geometry = geometry;
    }

    public Boolean getIsElementInprogress() {
        return isElementInprogress;
    }

    public void setIsElementInprogress(Boolean isElementInprogress) {
        this.isElementInprogress = isElementInprogress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getHouseOrbuilding() {
        return houseOrbuilding;
    }

    public void setHouseOrbuilding(String houseOrbuilding) {
        this.houseOrbuilding = houseOrbuilding;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentOrOffice() {
        return apartmentOrOffice;
    }

    public void setApartmentOrOffice(String apartmentOrOffice) {
        this.apartmentOrOffice = apartmentOrOffice;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientMobileNo() {
        return recipientMobileNo;
    }

    public void setRecipientMobileNo(String recipientMobileNo) {
        this.recipientMobileNo = recipientMobileNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientImage() {
        return recipientImage;
    }

    public void setRecipientImage(String recipientImage) {
        this.recipientImage = recipientImage;
    }

    public String getDriverNote() {
        return driverNote;
    }

    public void setDriverNote(String driverNote) {
        this.driverNote = driverNote;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getDeliveryDriverId() {
        return deliveryDriverId;
    }

    public void setDeliveryDriverId(String deliveryDriverId) {
        this.deliveryDriverId = deliveryDriverId;
    }

    public String getDeliveryDriverName() {
        return deliveryDriverName;
    }

    public void setDeliveryDriverName(String deliveryDriverName) {
        this.deliveryDriverName = deliveryDriverName;
    }

    public String getDeliveryDriverEmail() {
        return deliveryDriverEmail;
    }

    public void setDeliveryDriverEmail(String deliveryDriverEmail) {
        this.deliveryDriverEmail = deliveryDriverEmail;
    }

    public String getDeliveryDriverMobileNo() {
        return deliveryDriverMobileNo;
    }

    public void setDeliveryDriverMobileNo(String deliveryDriverMobileNo) {
        this.deliveryDriverMobileNo = deliveryDriverMobileNo;
    }

    public String getDeliveryDriverImage() {
        return deliveryDriverImage;
    }

    public void setDeliveryDriverImage(String deliveryDriverImage) {
        this.deliveryDriverImage = deliveryDriverImage;
    }

    public Boolean getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(Boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public Boolean getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Boolean isReturn) {
        this.isReturn = isReturn;
    }

    public Boolean getIsReturnConfirmed() {
        return isReturnConfirmed;
    }

    public void setIsReturnConfirmed(Boolean isReturnConfirmed) {
        this.isReturnConfirmed = isReturnConfirmed;
    }

    public Boolean getIsReturnReceived() {
        return isReturnReceived;
    }

    public void setIsReturnReceived(Boolean isReturnReceived) {
        this.isReturnReceived = isReturnReceived;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getDropInstructions() {
        return dropInstructions;
    }

    public void setDropInstructions(String dropInstructions) {
        this.dropInstructions = dropInstructions;
    }

    public List<Object> getDeliveryPictures() {
        return deliveryPictures;
    }

    public void setDeliveryPictures(List<Object> deliveryPictures) {
        this.deliveryPictures = deliveryPictures;
    }

    public BigDecimal getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(BigDecimal deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Item_> getItem() {
        return item;
    }

    public void setItem(List<Item_> item) {
        this.item = item;
    }

    public String getElementUniqueId() {
        return elementUniqueId;
    }

    public void setElementUniqueId(String elementUniqueId) {
        this.elementUniqueId = elementUniqueId;
    }

    public List<DriverTrackingstatus_> getDriverTrackingStatus() {
        return driverTrackingStatus;
    }

    public void setDriverTrackingStatus(List<DriverTrackingstatus_> driverTrackingStatus) {
        this.driverTrackingStatus = driverTrackingStatus;
    }

    public List<Object> getConfirmationStatus() {
        return confirmationStatus;
    }

    public void setConfirmationStatus(List<Object> confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }

    public String getReasonForCancellation() {
        return reasonForCancellation;
    }

    public void setReasonForCancellation(String reasonForCancellation) {
        this.reasonForCancellation = reasonForCancellation;
    }

    public String getDigitalSignature() {
        return digitalSignature;
    }

    public void setDigitalSignature(String digitalSignature) {
        this.digitalSignature = digitalSignature;
    }

    public String getDeliveryDriverVehicle() {
        return deliveryDriverVehicle;
    }

    public void setDeliveryDriverVehicle(String deliveryDriverVehicle) {
        this.deliveryDriverVehicle = deliveryDriverVehicle;
    }

}
