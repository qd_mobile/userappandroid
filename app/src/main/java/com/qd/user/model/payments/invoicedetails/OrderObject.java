
package com.qd.user.model.payments.invoicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderObject {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("metadata")
    @Expose
    private List<Total> metadata = null;

    public List<Total> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<Total> metadata) {
        this.metadata = metadata;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


}
