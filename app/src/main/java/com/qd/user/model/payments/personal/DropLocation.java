
package com.qd.user.model.payments.personal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DropLocation {

    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

}
