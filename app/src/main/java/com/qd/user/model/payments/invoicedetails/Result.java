
package com.qd.user.model.payments.invoicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("vendorId")
    @Expose
    private String vendorId;
    @SerializedName("invoiceStartDate")
    @Expose
    private String invoiceStartDate;
    @SerializedName("invoiceEndDate")
    @Expose
    private String invoiceEndDate;
    @SerializedName("invoiceDueDate")
    @Expose
    private String invoiceDueDate;
    @SerializedName("invoiceNumber")
    @Expose
    private String invoiceNumber;
    @SerializedName("totalItemCost")
    @Expose
    private BigDecimal totalItemCost;
    @SerializedName("totalDeliveryCharges")
    @Expose
    private BigDecimal totalDeliveryCharges;
    @SerializedName("totalReturnCharges")
    @Expose
    private BigDecimal totalReturnCharges;
    @SerializedName("totalCancellationCharges")
    @Expose
    private BigDecimal totalCancellationCharges;
    @SerializedName("totalAmount")
    @Expose
    private BigDecimal totalAmount;
    @SerializedName("totalNoOfOrders")
    @Expose
    private int totalNoOfOrders;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("orderObjects")
    @Expose
    private List<OrderObject> orderObjects = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getInvoiceStartDate() {
        return invoiceStartDate;
    }

    public void setInvoiceStartDate(String invoiceStartDate) {
        this.invoiceStartDate = invoiceStartDate;
    }

    public String getInvoiceEndDate() {
        return invoiceEndDate;
    }

    public void setInvoiceEndDate(String invoiceEndDate) {
        this.invoiceEndDate = invoiceEndDate;
    }

    public String getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(String invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public BigDecimal getTotalItemCost() {
        return totalItemCost;
    }

    public void setTotalItemCost(BigDecimal totalItemCost) {
        this.totalItemCost = totalItemCost;
    }

    public BigDecimal getTotalDeliveryCharges() {
        return totalDeliveryCharges;
    }

    public void setTotalDeliveryCharges(BigDecimal totalDeliveryCharges) {
        this.totalDeliveryCharges = totalDeliveryCharges;
    }

    public BigDecimal getTotalReturnCharges() {
        return totalReturnCharges;
    }

    public void setTotalReturnCharges(BigDecimal totalReturnCharges) {
        this.totalReturnCharges = totalReturnCharges;
    }

    public BigDecimal getTotalCancellationCharges() {
        return totalCancellationCharges;
    }

    public void setTotalCancellationCharges(BigDecimal totalCancellationCharges) {
        this.totalCancellationCharges = totalCancellationCharges;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalNoOfOrders() {
        return totalNoOfOrders;
    }

    public void setTotalNoOfOrders(int totalNoOfOrders) {
        this.totalNoOfOrders = totalNoOfOrders;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<OrderObject> getOrderObjects() {
        return orderObjects;
    }

    public void setOrderObjects(List<OrderObject> orderObjects) {
        this.orderObjects = orderObjects;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}
