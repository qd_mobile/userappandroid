
package com.qd.user.model.createorder.optimizedroute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OptimizedDeliveryRequest {

    @SerializedName("dropLocation")
    @Expose
    private List<DropLocation> dropLocation = null;
    @SerializedName("origin")
    @Expose
    private Origin origin;
    @SerializedName("vehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;
    @SerializedName("orderType")
    @Expose
    private String orderType;
    @SerializedName("vendorId")
    @Expose
    private String vendorId;
    @SerializedName("isOptimized")
    @Expose
    private Boolean isOptimized;

    public Boolean getOptimize() {
        return isOptimized;
    }

    public void setOptimize(Boolean optimize) {
        isOptimized = optimize;
    }

    public List<DropLocation> getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(List<DropLocation> dropLocation) {
        this.dropLocation = dropLocation;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
