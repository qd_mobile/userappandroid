
package com.qd.user.model.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("vendorId")
    @Expose
    private String vendorId;
    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("adminId")
    @Expose
    private String adminId;
    @SerializedName("isRead")
    @Expose
    private boolean isRead;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("triggerId")
    @Expose
    private String triggerId;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("notificationType")
    @Expose
    private String notificationType;
    @SerializedName("notificaitonTitle")
    @Expose
    private String notificaitonTitle;
    @SerializedName("notificationMessage")
    @Expose
    private String notificationMessage;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("orderData")
    @Expose
    private List<OrderDatum> orderData = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(String triggerId) {
        this.triggerId = triggerId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificaitonTitle() {
        return notificaitonTitle;
    }

    public void setNotificaitonTitle(String notificaitonTitle) {
        this.notificaitonTitle = notificaitonTitle;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<OrderDatum> getOrderData() {
        return orderData;
    }

    public void setOrderData(List<OrderDatum> orderData) {
        this.orderData = orderData;
    }

}
