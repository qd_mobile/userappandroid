
package com.qd.user.model.payments.invoicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("paymentDetails")
    @Expose
    private PaymentDetails paymentDetails;
    @SerializedName("pickupLocation")
    @Expose
    private List<PickupLocation> pickupLocation = null;
    @SerializedName("orderUniqueId")
    @Expose
    private String orderUniqueId;
    @SerializedName("totalDrop")
    @Expose
    private int totalDrop;
    @SerializedName("scheduledStartTime")
    @Expose
    private String scheduledStartTime;
    @SerializedName("driverVehicle")
    @Expose
    private String driverVehicle;

    public String getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(String scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public List<PickupLocation> getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(List<PickupLocation> pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

    public int getTotalDrop() {
        return totalDrop;
    }

    public void setTotalDrop(int totalDrop) {
        this.totalDrop = totalDrop;
    }

    public String getDriverVehicle() {
        return driverVehicle;
    }

    public void setDriverVehicle(String driverVehicle) {
        this.driverVehicle = driverVehicle;
    }

}
