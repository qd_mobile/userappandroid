package com.qd.user.model.createorder.additem;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class ItemDetails implements Parcelable {

    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("itemPrice")
    @Expose
    private BigDecimal itemPrice;
    @SerializedName("itemDescription")
    @Expose
    private String itemDescription;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("itemImage")
    @Expose
    private List<String> itemImage = null;
    public static final Creator<ItemDetails> CREATOR = new Creator<ItemDetails>() {
        @Override
        public ItemDetails createFromParcel(Parcel in) {
            return new ItemDetails(in);
        }

        @Override
        public ItemDetails[] newArray(int size) {
            return new ItemDetails[size];
        }
    };
    private boolean isForEdit;
    private int pos;

    private ItemDetails(Parcel in) {
        itemType = in.readString();
        itemDescription = in.readString();
        note = in.readString();
        itemImage = in.createStringArrayList();
    }

    public ItemDetails() {

    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public boolean isForEdit() {
        return isForEdit;
    }

    public void setForEdit(boolean forEdit) {
        isForEdit = forEdit;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getItemImage() {
        return itemImage;
    }

    public void setItemImage(List<String> itemImage) {
        this.itemImage = itemImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemType);
        dest.writeString(itemDescription);
        dest.writeString(note);
        dest.writeStringList(itemImage);
        dest.writeByte((byte) (isForEdit ? 1 : 0));
        dest.writeInt(pos);
    }
}