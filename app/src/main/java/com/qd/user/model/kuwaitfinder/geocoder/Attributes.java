
package com.qd.user.model.kuwaitfinder.geocoder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

    @SerializedName("objectid")
    @Expose
    private String objectid;
    @SerializedName("Nhood_No")
    @Expose
    private String nhoodNo;
    @SerializedName("blockarabic")
    @Expose
    private String blockarabic;
    @SerializedName("neighborhoodarabic")
    @Expose
    private String neighborhoodarabic;
    @SerializedName("neighborhoodenglish")
    @Expose
    private String neighborhoodenglish;
    @SerializedName("governoratearabic")
    @Expose
    private String governoratearabic;
    @SerializedName("governorateenglish")
    @Expose
    private String governorateenglish;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("blockenglish")
    @Expose
    private String blockenglish;
    @SerializedName("centroid_x")
    @Expose
    private String centroidX;
    @SerializedName("centroid_y")
    @Expose
    private String centroidY;
    @SerializedName("blockid")
    @Expose
    private String blockid;
    @SerializedName("shape")
    @Expose
    private String shape;
    @SerializedName("st_area(shape)")
    @Expose
    private String stAreaShape;
    @SerializedName("st_length(shape)")
    @Expose
    private String stLengthShape;
    @SerializedName("Gov_no")
    @Expose
    private String govNo;

    private String street;

    private String addressName;

    public String getObjectid() {
        return objectid;
    }

    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }

    public String getNhoodNo() {
        return nhoodNo;
    }

    public void setNhoodNo(String nhoodNo) {
        this.nhoodNo = nhoodNo;
    }

    public String getBlockarabic() {
        return blockarabic;
    }

    public void setBlockarabic(String blockarabic) {
        this.blockarabic = blockarabic;
    }

    public String getNeighborhoodarabic() {
        return neighborhoodarabic;
    }

    public void setNeighborhoodarabic(String neighborhoodarabic) {
        this.neighborhoodarabic = neighborhoodarabic;
    }

    public String getNeighborhoodenglish() {
        return neighborhoodenglish;
    }

    public void setNeighborhoodenglish(String neighborhoodenglish) {
        this.neighborhoodenglish = neighborhoodenglish;
    }

    public String getGovernoratearabic() {
        return governoratearabic;
    }

    public void setGovernoratearabic(String governoratearabic) {
        this.governoratearabic = governoratearabic;
    }

    public String getGovernorateenglish() {
        return governorateenglish;
    }

    public void setGovernorateenglish(String governorateenglish) {
        this.governorateenglish = governorateenglish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBlockenglish() {
        return blockenglish;
    }

    public void setBlockenglish(String blockenglish) {
        this.blockenglish = blockenglish;
    }

    public String getCentroidX() {
        return centroidX;
    }

    public void setCentroidX(String centroidX) {
        this.centroidX = centroidX;
    }

    public String getCentroidY() {
        return centroidY;
    }

    public void setCentroidY(String centroidY) {
        this.centroidY = centroidY;
    }

    public String getBlockid() {
        return blockid;
    }

    public void setBlockid(String blockid) {
        this.blockid = blockid;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getStAreaShape() {
        return stAreaShape;
    }

    public void setStAreaShape(String stAreaShape) {
        this.stAreaShape = stAreaShape;
    }

    public String getStLengthShape() {
        return stLengthShape;
    }

    public void setStLengthShape(String stLengthShape) {
        this.stLengthShape = stLengthShape;
    }

    public String getGovNo() {
        return govNo;
    }

    public void setGovNo(String govNo) {
        this.govNo = govNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }
}
