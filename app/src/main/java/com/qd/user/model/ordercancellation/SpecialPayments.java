
package com.qd.user.model.ordercancellation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpecialPayments {

    @SerializedName("amountPaidAtPickup")
    @Expose
    private Long amountPaidAtPickup;
    @SerializedName("amountCollectedAtDrop")
    @Expose
    private Long amountCollectedAtDrop;

    public Long getAmountPaidAtPickup() {
        return amountPaidAtPickup;
    }

    public void setAmountPaidAtPickup(Long amountPaidAtPickup) {
        this.amountPaidAtPickup = amountPaidAtPickup;
    }

    public Long getAmountCollectedAtDrop() {
        return amountCollectedAtDrop;
    }

    public void setAmountCollectedAtDrop(Long amountCollectedAtDrop) {
        this.amountCollectedAtDrop = amountCollectedAtDrop;
    }

}
