
package com.qd.user.model.createorder.request;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PickupLocation implements Parcelable {

    @SerializedName("governorate")
    @Expose
    private String governorate;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("blockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("avenue")
    @Expose
    private String avenue;
    @SerializedName("houseOrbuilding")
    @Expose
    private String houseOrbuilding;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("apartmentOrOffice")
    @Expose
    private String apartmentOrOffice;
    @SerializedName("recipientName")
    @Expose
    private String recipientName;
    @SerializedName("recipientEmail")
    @Expose
    private String recipientEmail;
    @SerializedName("recipientMobileNo")
    @Expose
    private String recipientMobileNo;
    @SerializedName("driverNote")
    @Expose
    private String driverNote;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("geometry")
    @Expose
    private Geometry geometry;
    @SerializedName("isFavourite")
    @Expose
    private boolean isFavourite;

    public static final Creator<PickupLocation> CREATOR = new Creator<PickupLocation>() {
        @Override
        public PickupLocation createFromParcel(Parcel in) {
            return new PickupLocation(in);
        }

        @Override
        public PickupLocation[] newArray(int size) {
            return new PickupLocation[size];
        }
    };

    public PickupLocation() {

    }

    protected PickupLocation(Parcel in) {
        governorate = in.readString();
        area = in.readString();
        service = in.readString();
        name = in.readString();
        blockNumber = in.readString();
        street = in.readString();
        avenue = in.readString();
        houseOrbuilding = in.readString();
        fullAddress = in.readString();
        floor = in.readString();
        apartmentOrOffice = in.readString();
        recipientName = in.readString();
        recipientEmail = in.readString();
        recipientMobileNo = in.readString();
        driverNote = in.readString();
        paymentMode = in.readString();
        isFavourite = in.readByte() != 0;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getHouseOrbuilding() {
        return houseOrbuilding;
    }

    public void setHouseOrbuilding(String houseOrbuilding) {
        this.houseOrbuilding = houseOrbuilding;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentOrOffice() {
        return apartmentOrOffice;
    }

    public void setApartmentOrOffice(String apartmentOrOffice) {
        this.apartmentOrOffice = apartmentOrOffice;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientMobileNo() {
        return recipientMobileNo;
    }

    public void setRecipientMobileNo(String recipientMobileNo) {
        this.recipientMobileNo = recipientMobileNo;
    }

    public String getDriverNote() {
        return driverNote;
    }

    public void setDriverNote(String driverNote) {
        this.driverNote = driverNote;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(governorate);
        dest.writeString(area);
        dest.writeString(service);
        dest.writeString(name);
        dest.writeString(blockNumber);
        dest.writeString(street);
        dest.writeString(avenue);
        dest.writeString(houseOrbuilding);
        dest.writeString(fullAddress);
        dest.writeString(floor);
        dest.writeString(apartmentOrOffice);
        dest.writeString(recipientName);
        dest.writeString(recipientEmail);
        dest.writeString(recipientMobileNo);
        dest.writeString(driverNote);
        dest.writeString(paymentMode);
        dest.writeByte((byte) (isFavourite ? 1 : 0));
    }
}
