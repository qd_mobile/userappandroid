
package com.qd.user.model.kuwaitfinder.governorate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes implements Comparable<Attributes> {

    @SerializedName("objectid")
    @Expose
    private int objectid;
    @SerializedName("gov_no")
    @Expose
    private int govNo;
    @SerializedName("governoratearabic")
    @Expose
    private String governoratearabic;
    @SerializedName("governorateenglish")
    @Expose
    private String governorateenglish;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("centroid_x")
    @Expose
    private Double centroidX;
    @SerializedName("centroid_y")
    @Expose
    private Double centroidY;

    public int getObjectid() {
        return objectid;
    }

    public void setObjectid(int objectid) {
        this.objectid = objectid;
    }

    public int getGovNo() {
        return govNo;
    }

    public void setGovNo(int govNo) {
        this.govNo = govNo;
    }

    public String getGovernoratearabic() {
        return governoratearabic;
    }

    public void setGovernoratearabic(String governoratearabic) {
        this.governoratearabic = governoratearabic;
    }

    public String getGovernorateenglish() {
        return governorateenglish;
    }

    public void setGovernorateenglish(String governorateenglish) {
        this.governorateenglish = governorateenglish;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getCentroidX() {
        return centroidX;
    }

    public void setCentroidX(Double centroidX) {
        this.centroidX = centroidX;
    }

    public Double getCentroidY() {
        return centroidY;
    }

    public void setCentroidY(Double centroidY) {
        this.centroidY = centroidY;
    }

    @Override
    public int compareTo(Attributes o) {
        return governorateenglish.compareTo(o.governorateenglish);
    }
}
