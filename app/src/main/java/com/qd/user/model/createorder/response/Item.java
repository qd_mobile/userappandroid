
package com.qd.user.model.createorder.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class Item {

    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("itemPrice")
    @Expose
    private double itemPrice;
    @SerializedName("itemDescription")
    @Expose
    private String itemDescription;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("itemImage")
    @Expose
    private List<String> itemImage;

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getItemImage() {
        return itemImage;
    }

    public void setItemImage(List<String> itemImage) {
        this.itemImage = itemImage;
    }

}
