
package com.qd.user.model.socialresponse.socialsigninresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Geometry_ {

    @SerializedName("coordinates")
    @Expose
    private List<Object> coordinates = null;

    public List<Object> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Object> coordinates) {
        this.coordinates = coordinates;
    }

}
