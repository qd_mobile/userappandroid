
package com.qd.user.model.createorder.distancetime.eta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ETA {

    @SerializedName("COOLER")
    @Expose
    private List<Cooler> cooler = null;
    @SerializedName("VAN")
    @Expose
    private List<Van> van = null;
    @SerializedName("BIKE")
    @Expose
    private List<Bike> bike = null;
    @SerializedName("CAR")
    @Expose
    private List<Car> car = null;

    public List<Cooler> getCooler() {
        return cooler;
    }

    public void setCooler(List<Cooler> cooler) {
        this.cooler = cooler;
    }

    public List<Van> getVan() {
        return van;
    }

    public void setVan(List<Van> van) {
        this.van = van;
    }

    public List<Bike> getBike() {
        return bike;
    }

    public void setBike(List<Bike> bike) {
        this.bike = bike;
    }

    public List<Car> getCar() {
        return car;
    }

    public void setCar(List<Car> car) {
        this.car = car;
    }

}
