
package com.qd.user.model.vendor.vendorstatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qd.user.model.createaccount.FullBusinessAddress;

import java.math.BigDecimal;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("cashClientCurrentStatus")
    @Expose
    private String cashClientCurrentStatus;
    @SerializedName("cashClientStatus")
    @Expose
    private String cashClientStatus;
    @SerializedName("pendingOrders")
    @Expose
    private boolean pendingOrders;
    @SerializedName("fullBusinessAddress")
    @Expose
    private FullBusinessAddress fullBusinessAddress;
    @SerializedName("orderCurrentStatus")
    @Expose
    private String orderCurrentStatus;
    @SerializedName("servicesOffered")
    @Expose
    private String serviceOffered;
    @SerializedName("notificationCount")
    @Expose
    private int notificationCount;

    @SerializedName("paymentModes")
    @Expose
    private String paymentModes;

    public String getPaymentModes() {
        return paymentModes;
    }

    public void setPaymentModes(String paymentModes) {
        this.paymentModes = paymentModes;
    }

    public boolean isEnableNotification() {
        return enableNotification;
    }
    @SerializedName("enableNotification")
    @Expose
    private boolean enableNotification;

    @SerializedName("walletAmount")
    @Expose
    private BigDecimal walletAmount;

    public BigDecimal getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(BigDecimal walletAmount) {
        this.walletAmount = walletAmount;
    }


    public String getOrderCurrentStatus() {
        return orderCurrentStatus;
    }

    public void setOrderCurrentStatus(String orderCurrentStatus) {
        this.orderCurrentStatus = orderCurrentStatus;
    }

    public FullBusinessAddress getFullBusinessAddress() {
        return fullBusinessAddress;
    }

    public void setFullBusinessAddress(FullBusinessAddress fullBusinessAddress) {
        this.fullBusinessAddress = fullBusinessAddress;
    }

    public String getServiceOffered() {
        return serviceOffered;
    }

    public void setServiceOffered(String serviceOffered) {
        this.serviceOffered = serviceOffered;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCashClientCurrentStatus() {
        return cashClientCurrentStatus;
    }

    public void setCashClientCurrentStatus(String cashClientCurrentStatus) {
        this.cashClientCurrentStatus = cashClientCurrentStatus;
    }

    public String getCashClientStatus() {
        return cashClientStatus;
    }

    public void setCashClientStatus(String cashClientStatus) {
        this.cashClientStatus = cashClientStatus;
    }

    public boolean isPendingOrders() {
        return pendingOrders;
    }

    public void setPendingOrders(boolean pendingOrders) {
        this.pendingOrders = pendingOrders;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public boolean getEnableNotification() {
        return enableNotification;
    }

    public void setEnableNotification(boolean enableNotification) {
        this.enableNotification = enableNotification;
    }
}
