
package com.qd.user.model.createorder.balance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Configurations {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("findingDriverWaitTime")
    @Expose
    private int findingDriverWaitTime;
    @SerializedName("autoCancellation")
    @Expose
    private int autoCancellation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getFindingDriverWaitTime() {
        return findingDriverWaitTime;
    }

    public void setFindingDriverWaitTime(int findingDriverWaitTime) {
        this.findingDriverWaitTime = findingDriverWaitTime;
    }

    public int getAutoCancellation() {
        return autoCancellation;
    }

    public void setAutoCancellation(int autoCancellation) {
        this.autoCancellation = autoCancellation;
    }

}
