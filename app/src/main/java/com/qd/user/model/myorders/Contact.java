package com.qd.user.model.myorders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("personal")
    @Expose
    private String personal;
    @SerializedName("ccPersonal")
    @Expose
    private String ccPersonal;

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public String getCcPersonal() {
        return ccPersonal;
    }

    public void setCcPersonal(String ccPersonal) {
        this.ccPersonal = ccPersonal;
    }

}