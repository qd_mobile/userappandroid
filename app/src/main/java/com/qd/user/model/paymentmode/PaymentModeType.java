package com.qd.user.model.paymentmode;

public class PaymentModeType {
    private String paymentType;
    private int paymentIcon;
    private boolean isActive;

    public PaymentModeType(String paymentType, int paymentIcon, boolean isActive) {
        this.paymentType = paymentType;
        this.paymentIcon = paymentIcon;
        this.isActive = isActive;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getPaymentIcon() {
        return paymentIcon;
    }

    public void setPaymentIcon(int paymentIcon) {
        this.paymentIcon = paymentIcon;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
