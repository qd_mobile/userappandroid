package com.qd.user.model.commomresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Result {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("isDescriptionMandatory")
    @Expose
    private Boolean isDescriptionMandatory;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Boolean getDescriptionMandatory() {
        return isDescriptionMandatory;
    }

    public void setDescriptionMandatory(Boolean descriptionMandatory) {
        isDescriptionMandatory = descriptionMandatory;
    }

    @SerializedName("walletAmount")
    @Expose
    private BigDecimal walletAmount;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;

    @SerializedName("enableNotification")
    @Expose
    private boolean enableNotification;

    public boolean isEnableNotification() {
        return enableNotification;
    }

    public void setEnableNotification(boolean enableNotification) {
        this.enableNotification = enableNotification;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(BigDecimal walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
