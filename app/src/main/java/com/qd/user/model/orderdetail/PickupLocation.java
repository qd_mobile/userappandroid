
package com.qd.user.model.orderdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PickupLocation {

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;
    @SerializedName("isElementInprogress")
    @Expose
    private Boolean isElementInprogress;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("governorate")
    @Expose
    private String governorate;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("fullAddress")
    @Expose
    private String fullAddress;
    @SerializedName("blockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("avenue")
    @Expose
    private String avenue;
    @SerializedName("houseOrbuilding")
    @Expose
    private String houseOrbuilding;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("apartmentOrOffice")
    @Expose
    private String apartmentOrOffice;
    @SerializedName("recipientName")
    @Expose
    private String recipientName;
    @SerializedName("recipientMobileNo")
    @Expose
    private String recipientMobileNo;
    @SerializedName("recipientEmail")
    @Expose
    private String recipientEmail;
    @SerializedName("recipientImage")
    @Expose
    private String recipientImage;
    @SerializedName("driverNote")
    @Expose
    private String driverNote;
    @SerializedName("pickupStatus")
    @Expose
    private String pickupStatus;
    @SerializedName("pickupDriverId")
    @Expose
    private String pickupDriverId;
    @SerializedName("pickupDriverName")
    @Expose
    private String pickupDriverName;
    @SerializedName("pickupDriverEmail")
    @Expose
    private String pickupDriverEmail;
    @SerializedName("pickupDriverVehicle")
    @Expose
    private String pickupDriverVehicle;
    @SerializedName("pickupDriverMobileNo")
    @Expose
    private String pickupDriverMobileNo;
    @SerializedName("pickupDriverImage")
    @Expose
    private String pickupDriverImage;
    @SerializedName("pickupPictures")
    @Expose
    private List<String> pickupPictures = null;
    @SerializedName("confirmationStatus")
    @Expose
    private List<String> confirmationStatus = null;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("tip")
    @Expose
    private Integer tip;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("pickupInstructions")
    @Expose
    private String pickupInstructions;

    public String getPickupDriverVehicle() {
        return pickupDriverVehicle;
    }
    @SerializedName("pickupCharges")
    @Expose
    private Integer pickupCharges;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("item")
    @Expose
    private List<Item> item = null;
    @SerializedName("driverTrackingStatus")
    @Expose
    private List<DriverTrackingstatus> driverTrackingStatus = null;

    public void setPickupDriverVehicle(String pickupDriverVehicle) {
        this.pickupDriverVehicle = pickupDriverVehicle;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Boolean getIsElementInprogress() {
        return isElementInprogress;
    }

    public void setIsElementInprogress(Boolean isElementInprogress) {
        this.isElementInprogress = isElementInprogress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getHouseOrbuilding() {
        return houseOrbuilding;
    }

    public void setHouseOrbuilding(String houseOrbuilding) {
        this.houseOrbuilding = houseOrbuilding;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartmentOrOffice() {
        return apartmentOrOffice;
    }

    public void setApartmentOrOffice(String apartmentOrOffice) {
        this.apartmentOrOffice = apartmentOrOffice;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientMobileNo() {
        return recipientMobileNo;
    }

    public void setRecipientMobileNo(String recipientMobileNo) {
        this.recipientMobileNo = recipientMobileNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientImage() {
        return recipientImage;
    }

    public void setRecipientImage(String recipientImage) {
        this.recipientImage = recipientImage;
    }

    public String getDriverNote() {
        return driverNote;
    }

    public void setDriverNote(String driverNote) {
        this.driverNote = driverNote;
    }

    public String getPickupStatus() {
        return pickupStatus;
    }

    public void setPickupStatus(String pickupStatus) {
        this.pickupStatus = pickupStatus;
    }

    public String getPickupDriverId() {
        return pickupDriverId;
    }

    public void setPickupDriverId(String pickupDriverId) {
        this.pickupDriverId = pickupDriverId;
    }

    public String getPickupDriverName() {
        return pickupDriverName;
    }

    public void setPickupDriverName(String pickupDriverName) {
        this.pickupDriverName = pickupDriverName;
    }

    public String getPickupDriverEmail() {
        return pickupDriverEmail;
    }

    public void setPickupDriverEmail(String pickupDriverEmail) {
        this.pickupDriverEmail = pickupDriverEmail;
    }

    public String getPickupDriverMobileNo() {
        return pickupDriverMobileNo;
    }

    public void setPickupDriverMobileNo(String pickupDriverMobileNo) {
        this.pickupDriverMobileNo = pickupDriverMobileNo;
    }

    public String getPickupDriverImage() {
        return pickupDriverImage;
    }

    public void setPickupDriverImage(String pickupDriverImage) {
        this.pickupDriverImage = pickupDriverImage;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getTip() {
        return tip;
    }

    public void setTip(Integer tip) {
        this.tip = tip;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getPickupInstructions() {
        return pickupInstructions;
    }

    public void setPickupInstructions(String pickupInstructions) {
        this.pickupInstructions = pickupInstructions;
    }

    public List<String> getPickupPictures() {
        return pickupPictures;
    }

    public void setPickupPictures(List<String> pickupPictures) {
        this.pickupPictures = pickupPictures;
    }

    public Integer getPickupCharges() {
        return pickupCharges;
    }

    public void setPickupCharges(Integer pickupCharges) {
        this.pickupCharges = pickupCharges;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public List<DriverTrackingstatus> getDriverTrackingStatus() {
        return driverTrackingStatus;
    }

    public void setDriverTrackingStatus(List<DriverTrackingstatus> driverTrackingStatus) {
        this.driverTrackingStatus = driverTrackingStatus;
    }

    public List<String> getConfirmationStatus() {
        return confirmationStatus;
    }

    public void setConfirmationStatus(List<String> confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }

}
