
package com.qd.user.model.kuwaitfinder.geocoder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("layerId")
    @Expose
    private long layerId;
    @SerializedName("layerName")
    @Expose
    private String layerName;
    @SerializedName("displayFieldName")
    @Expose
    private String displayFieldName;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("attributes")
    @Expose
    private Attributes attributes;

    public long getLayerId() {
        return layerId;
    }

    public void setLayerId(long layerId) {
        this.layerId = layerId;
    }

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    public String getDisplayFieldName() {
        return displayFieldName;
    }

    public void setDisplayFieldName(String displayFieldName) {
        this.displayFieldName = displayFieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

}
