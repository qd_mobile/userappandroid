
package com.qd.user.model.kuwaitfinder.geocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GeoCodeLocationResponse {

    @SerializedName("exceededTransferLimit")
    @Expose
    private Boolean exceededTransferLimit;
    @SerializedName("features")
    @Expose
    private List<Feature> features = null;
    @SerializedName("fields")
    @Expose
    private List<Field> fields = null;
    @SerializedName("objectIdFieldName")
    @Expose
    private String objectIdFieldName;

    public Boolean getExceededTransferLimit() {
        return exceededTransferLimit;
    }

    public void setExceededTransferLimit(Boolean exceededTransferLimit) {
        this.exceededTransferLimit = exceededTransferLimit;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public String getObjectIdFieldName() {
        return objectIdFieldName;
    }

    public void setObjectIdFieldName(String objectIdFieldName) {
        this.objectIdFieldName = objectIdFieldName;
    }

}
