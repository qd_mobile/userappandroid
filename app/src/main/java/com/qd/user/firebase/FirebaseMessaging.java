package com.qd.user.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Parcelable;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.qd.user.R;
import com.qd.user.constants.AppConstants;
import com.qd.user.data.DataManager;
import com.qd.user.model.createorder.response.Result;
import com.qd.user.ui.createorder.CreateOrderActivity;
import com.qd.user.ui.home.HomeActivity;
import com.qd.user.ui.onboard.OnBoardActivity;
import com.qd.user.utils.notifications.HandlePushNotifications;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class FirebaseMessaging extends FirebaseMessagingService {

    private int mUniqueId;
    private String type;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //handling notification
        Log.d("notification", remoteMessage.toString() + "------------------");
        sendNotification(remoteMessage.getData(), remoteMessage.getNotification());

    }

    //This method is only generating push notification
    private void sendNotification(Map<String, String> data, RemoteMessage.Notification notificationData) {
        mUniqueId = 0;
        JSONObject obj = null;

        String image = null;

        Log.e("", "sendNotification: " + data + "----------------\n\n\n" + notificationData);
        //Generating unique id
        generateUniqueId();


        try {
            obj = new JSONObject(data.get("data"));
            // obj = new JSONObject(jsonObject.getString("notificationData"));
            if (obj.has("image"))
                image = obj.getString("image");
            type = obj.getString("type");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (type) {
            case AppConstants.NotificationType.ORDER_ACCEPTED_BY_DRIVER:
                Intent resultantIntent = new Intent(this, CreateOrderActivity.class);
                resultantIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultantIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                resultantIntent.putExtra(AppConstants.ORDER_ID, ((Result) HandlePushNotifications.getInstance().parseNotificationData(obj)).getId());
                resultantIntent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.TRACK_ORDER);
                startActivity(resultantIntent);
                generateUniqueId();

                break;
            case AppConstants.NotificationType.ISSUE_REPORTED_BY_DRIVER:
                Intent reportIntent = new Intent(this, OnBoardActivity.class);
                reportIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                reportIntent.putExtra(AppConstants.ORDER_DATA, ((Parcelable) HandlePushNotifications.getInstance().parseNotificationData(obj)));
                reportIntent.putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.APPROVE_RETURN);
                startActivity(reportIntent);

                break;

            default:
                Intent intent;
                if (type.equals(AppConstants.NotificationType.PROMOTIONAL_NOTIFICATION))
                    intent = new Intent(this, HomeActivity.class).putExtra(AppConstants.ACTION_NAVIGATION, AppConstants.Navigation.NOTIFICATION);
                else
                    intent = new Intent(this, OnBoardActivity.class);

                intent.putExtra(AppConstants.NOTIFICATION_DATA, data.get("data"));

                // Create the TaskStackBuilder and add the intent, which inflates the back stack
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addNextIntentWithParentStack(intent);
                // Get the PendingIntent containing the entire back stack
                PendingIntent pendingIntent =
                        stackBuilder.getPendingIntent(mUniqueId, PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationManager notificationManager = getNotificationManager();


                if (notificationManager != null) {
                    String id = "channel" + mUniqueId;
                    //Check for oreo (Making notification channel)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel(id, data.get("title"), importance);
                        mChannel.setDescription(data.get("body"));
                        mChannel.setShowBadge(true);
                        mChannel.enableLights(true);
                        mChannel.setLightColor(ContextCompat.getColor(this, R.color.colorPrimary));
                        mChannel.enableVibration(true);
                        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                        notificationManager.createNotificationChannel(mChannel);

                    }

                    NotificationCompat.Builder notificationBuilder;

                    if (image != null && !image.trim().isEmpty()) {
                        notificationBuilder =
                                new NotificationCompat.Builder(this, id)
                                        .setSmallIcon(getNotificationIcon())
                                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.push_notification))
                                        .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                                        .setContentText(data.get("body"))
                                        .setContentTitle(data.get("title"))
                                        .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(getBitmapFromUrl(image)))
                                        .setAutoCancel(true).setChannelId(id)
                                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                        .setContentIntent(pendingIntent);

                    } else {
                        notificationBuilder =
                                new NotificationCompat.Builder(this, id)
                                        .setSmallIcon(getNotificationIcon())
                                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.push_notification))
                                        .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                                        .setContentText(data.get("body"))
                                        .setContentTitle(data.get("title"))
                                        .setAutoCancel(true).setChannelId(id)
                                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                        .setContentIntent(pendingIntent);

                    }

                    //Set Notification for other devices

                    //.setContentIntent(pendingIntent).setNumber(Integer.parseInt(AppSharedPrefs.getInstance(this).getString(AppSharedPrefs.PREF_KEY.BATCH_COUNT, "0")));
                    // Set a message count to associate with this notification in the long-press menu.
                    // Create a notification and set a number to associate with it.

                    Notification notification = notificationBuilder.build();

                    notification.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                    notification.flags |=
                            Notification.FLAG_AUTO_CANCEL; //Do not clear  the notification
                    notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
                    notification.defaults |= Notification.DEFAULT_VIBRATE;//Vibration

                    notificationManager.notify(mUniqueId, notification);

                }

        }

    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        DataManager.getInstance().setDeviceToken(s);
    }

    //method to get notificationManager
    private NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }


    /**
     * This method is used to generate unique id for the notifications
     */
    private void generateUniqueId() {
        mUniqueId = (int) System.currentTimeMillis();
    }


    /**
     * This method is used to set the notification icon on the push notifications for marshmallow
     * or oreo devices
     *
     * @return drawable on the basis of the type of device
     */
    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.push_notification : R.drawable.push_notification;
    }

    /*
     *To get a Bitmap image from the URL received
     * */
    private Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
