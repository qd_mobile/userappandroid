package com.qd.user;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.NonNull;

import com.qd.user.data.DataManager;
import com.qd.user.utils.LocaleManager;
import com.qd.user.utils.LogUtil;

public class QuickDelivery extends Application {

    private static QuickDelivery mApplicationInstance;
    private Activity mCurrentActivity;


    public QuickDelivery() {
        super();
        mApplicationInstance = this;
    }

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.onAttach(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.onAttach(base, LocaleManager.getLanguage(base)));
    }

    public static QuickDelivery getInstance() {
        return mApplicationInstance;
    }

    public void setCurrentActivity(Activity currentActivity) {
        mCurrentActivity = currentActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DataManager.init(getApplicationContext());

        /*
         * Enable Log only for Debug Build Variant
         */
        LogUtil.isDebuggable(BuildConfig.DEBUG);
    }

}
