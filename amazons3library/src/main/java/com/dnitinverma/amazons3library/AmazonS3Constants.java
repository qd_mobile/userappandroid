package com.dnitinverma.amazons3library;

import com.amazonaws.regions.Regions;

class AmazonS3Constants {
    static final String AMAZON_POOL_ID = "eu-west-1:1ebf9e5e-0bf1-44ba-b582-4a40a0d1c1b4";
    static final String BUCKET = "quickdelivery";
    static final String AMAZON_SERVER_URL = "https://quickdelivery.s3-eu-west-1.amazonaws.com/";
    static final String END_POINT = "s3-eu-west-1.amazonaws.com";
    static final Regions REGIONS = Regions.EU_WEST_1;
}